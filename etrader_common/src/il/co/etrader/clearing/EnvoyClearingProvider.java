package il.co.etrader.clearing;


import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.WireBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.SQLException;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingProvider;
import com.anyoption.common.clearing.ClearingProviderConfig;
import com.anyoption.common.util.ClearingUtil;


/**
 * Implement the communication with Envoy.
 * Debit Processing Gateway
 *
 * @author Eliran
 */
public class EnvoyClearingProvider extends ClearingProvider {
	private static final Logger log = Logger.getLogger(EnvoyClearingProvider.class);

	// request types
    private static final int FNC_ENROLL = 1;
    private static final int FNC_PURCHASE = 2;
    private static final int FNC_REFUND = 3;

    // online methods account number parsing place
    private static final int POSITION_MONETA = 5;
    private static final int POSITION_WEBMONEY = 3;

    public static final int REFUND_DEST_SERVICE = 1;
    public static final int REFUND_DEST_BANK = 2;

    private Hashtable<String, String> headers;
    private boolean demo;

	public void authorize(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");

	}

	public void bookback(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");
	}

	public void capture(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");

	}

	public void enroll(ClearingInfo info) throws ClearingException {
		request((EnvoyInfo)info, FNC_ENROLL);
	}

	public boolean isSupport3DEnrollement() {
		return false;
	}

	public void purchase(ClearingInfo info) throws ClearingException {
		request((EnvoyInfo)info, FNC_PURCHASE);
	}

	public void withdraw(ClearingInfo info) throws ClearingException {
		request((EnvoyInfo)info, FNC_REFUND);
	}

	 /**
     * Process a request through this provider.
     *
     * @param info thransaction info
     * @param authorize authorize or capture request to make
     * @throws ClearingException
     */
    private void request(EnvoyInfo info, int type) throws ClearingException {
        if (log.isTraceEnabled()) {
            log.trace(info.toString());
        }
        try {

        	Document request = createRequest();

            switch (type) {
	            case FNC_REFUND:
	            	addRefundFunction(request, info);
	            	break;
            default:
                throw new ClearingException("Unkown request type");
            }
            String requestXML = ClearingUtil.transformDocumentToString(request);
            if (log.isTraceEnabled()) {
                log.trace(requestXML);
            }

            String responseXML = ClearingUtil.executePOSTRequest(url, requestXML, false, true, headers,"UTF-8");

            if (log.isTraceEnabled()) {
                log.trace(responseXML);
            }
            parsePayToBankResponse(responseXML, info);

        } catch (Throwable t) {
            throw new ClearingException("Transaction failed.", t);
        }
    }

    /**
     * Create Envoy request XML document.
     *
     * @return New Document with the Envoy XML structure.
     */
    private Document createRequest() {
        Document request = null;
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            request = docBuilder.newDocument();

            Element root = request.createElement("soap:Envelope");
            request.appendChild(root);
            ClearingUtil.setAttribute(request, "", "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            ClearingUtil.setAttribute(request, "", "xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
            ClearingUtil.setAttribute(request, "", "xmlns:soap", "http://schemas.xmlsoap.org/soap/envelope/");

        } catch (Exception e) {
            log.log(Level.ERROR, "Failed to create request document.", e);
        }
        return request;
    }

    /**
     * Add "refund" function to the request.
     *
     * @param request
     * @param info
     * @return Wire Card function name.
     */
    private void addRefundFunction(Document request, EnvoyInfo info) {
        Element root = request.getDocumentElement();
        String path = "soap:Body/payToBankAccountV2";
        ClearingUtil.setAttribute(request, path, "xmlns", "http://merchantapi.envoyservices.com");

        ClearingUtil.setNodeValue(root, path + "/auth/username/*", username);
        ClearingUtil.setNodeValue(root, path + "/auth/password/*", password);
        ClearingUtil.setNodeValue(root, path + "/requestReference/*", info.getRequestReference());

        path += "/paymentInstructions/paymentInstructionV2/";
        String pd = path + "paymentDetails/";
        String bd = path + "bankDetails/";

        // paymentDetails

        if (REFUND_DEST_SERVICE == info.getRefundDestination()){
        	ClearingUtil.setNodeValue(root, pd + "countryCode/*", info.getRefundCountryCode());
        	ClearingUtil.setNodeValue(root, pd + "payee/*", info.getUserName());
        }else if (REFUND_DEST_BANK == info.getRefundDestination()){
        	ClearingUtil.setNodeValue(root, pd + "countryCode/*", info.getCountryA2());
        	ClearingUtil.setNodeValue(root, pd + "payee/*", info.getAccount().getBeneficiaryName());
        }
        ClearingUtil.setNodeValue(root, pd + "sourceCurrency/*", info.getCurrencySymbol());
        ClearingUtil.setNodeValue(root, pd + "sourceAmount/*", info.getFullAmountStr());
        ClearingUtil.setNodeValue(root, pd + "targetCurrency/*", info.getCurrencySymbol());
        ClearingUtil.setNodeValue(root, pd + "targetAmount/*", info.getFullAmountStr());
        ClearingUtil.setNodeValue(root, pd + "sourceOrTarget/*", "T");
        ClearingUtil.setNodeValue(root, pd + "merchantReference/*", String.valueOf(info.getUserId()));
        ClearingUtil.setNodeValue(root, pd + "paymentReference/*", String.valueOf(info.getTransactionId()));
//        ClearingUtil.setNodeValue(root, pd + "additionalInfo1/*", );
//        ClearingUtil.setNodeValue(root, pd + "additionalInfo2/*", );
//        ClearingUtil.setNodeValue(root, pd + "additionalInfo3/*", );
        String email = info.getEmail();
        if (null != email){
        	ClearingUtil.setNodeValue(root, pd + "email/*", email);
        }

        // bankDetails
        if (REFUND_DEST_SERVICE == info.getRefundDestination()){
        	ClearingUtil.setNodeValue(root, bd + "accountNumber/*", info.getAccountNumber());
        }else if (REFUND_DEST_BANK == info.getRefundDestination()){
	        WireBase account = info.getAccount();

	        if (account.getAccountNum() != null){
	        	ClearingUtil.setNodeValue(root, bd + "accountNumber/*", account.getAccountNum());
	        }

	        ClearingUtil.setNodeValue(root, bd + "bankName/*", account.getBankName());

	        if (account.getBankCode() != null){
	        	ClearingUtil.setNodeValue(root, bd + "bankCode/*", account.getBankCode());
	        }
	        if (account.getBranch() != null){
	        	ClearingUtil.setNodeValue(root, bd + "branchCode/*", account.getBranch());
	        }
	        if (account.getBranchAddress() != null){
	        	ClearingUtil.setNodeValue(root, bd + "branchAddress/*", account.getBranchAddress());
	        }
	        if (account.getAccountType() != null){
	        	ClearingUtil.setNodeValue(root, bd + "accountType/*", account.getAccountType());
	        }
	        if (account.getCheckDigits() != null){
	        	ClearingUtil.setNodeValue(root, bd + "checkDigits/*", account.getCheckDigits());
	        }
	        if (account.getIban() != null){
	        	ClearingUtil.setNodeValue(root, bd + "iban/*", account.getIban());
	        }
	        if (account.getSwift() != null){
	        	ClearingUtil.setNodeValue(root, bd + "swift/*", account.getSwift());
	        }
        }
    }

    /**
     * Parse the response.
     *
     * @param xml the XML to parse
     * @param info the transaction info to fill the result values
     * @throws Exception
     */
    private static void parsePayToBankResponse(String xml, EnvoyInfo info) throws Exception {
        Document resp = ClearingUtil.parseXMLToDocument(xml);
        String rootPath = "soap:Body/payToBankAccountV2Response/payToBankAccountV2Result";
        Element result = (Element) ClearingUtil.getNode(resp.getDocumentElement(), rootPath);

        String statusCode = ClearingUtil.getElementValue(result, "statusCode/*");
        info.setResult(statusCode);
        info.setMessage(ClearingUtil.getElementValue(result, "statusMessage/*"));

        String path = "paymentInstructions/paymentResultV2/";

        String epacsReference   = ClearingUtil.getElementValue(result, path + "epacsReference/*");
        //////////////////////////////
        info.setAuthNumber(epacsReference);
        info.setProviderTransactionId(epacsReference);

//        String redirectUrl = ClearingUtil.getElementValue(result, "RedirectURL/*");

       	info.setSuccessful(Integer.parseInt(statusCode) >= 0);

//        if (null != redirectUrl) {  // passed in the initial response
//        	redirectUrl = URLDecoder.decode(redirectUrl,"UTF-8");
//        }
    }

    /**
     * Set clearing provider configuration.
     *
     * @param key
     * @param props
     * @throws ClearingException
     */
    public void setConfig(ClearingProviderConfig config) throws ClearingException {
        super.setConfig(config);

        headers = new Hashtable<String, String>();
        headers.put("Content-Type", "text/xml");

        demo = false;
        if (null != config.getProps() && !config.getProps().trim().equals("")) {
            String[] ps = config.getProps().split(",");
            for (int i = 0; i < ps.length; i++) {
                String[] p = ps[i].split("=");
                if (p[0].equals("demo")) {
                    demo = p[1].equalsIgnoreCase("true");
                } else {
                    log.warn("Unknown property " + ps[i]);
                }
            }
        }
        if (log.isInfoEnabled()) {
            log.info("demo: " + demo);
        }
    }

    /**
     * Process a notification through this provider.
     *
     * @param xml the notification xml
     * @param info thransaction info
     * @throws ClearingException
     */
    public EnvoyInfo handleNotifications(String requestXmlString, long loginId) throws ClearingException {
    	EnvoyInfo info = null;
    	int notificationType = getNotificationType(requestXmlString);
//    	String status;

//    	log.info("handleNotifications - without wait");
        log.info("handleNotifications - before wait");
        try {
       		// wait for 10 sec to make sure that result from envoy 1 click will come first.
       		synchronized(this) {
       			Thread.sleep(10000);
       		}
		} catch (InterruptedException e) {
			log.info("envoyNotify - waiting failed");
		}
		log.info("handleNotifications - after wait");

    	log.info("notificationType is  " + notificationType);

        try {
        	switch (notificationType) {
		        case ConstantsBase.ENVOY_PAY_IN_NOTIFICATION_ID:
		        	info = parsePayInNotification(requestXmlString, loginId);

		        	if (null != info){
		        		confirmPayInNotification(info);
		        	}
		        	break;
		        case ConstantsBase.ENVOY_PAY_OUT_NOTIFICATION_ID:
		        	info = parsePayOutNotification(requestXmlString);
		        	break;
//		        case ConstantsBase.ENVOY_PAY_OUT_REVERSAL_NOTIFICATION_ID:
//		        	info = parsePayOutReversalNotification(requestXmlString);
//		        	break;
//		        case ConstantsBase.ENVOY_PAY_IN_REVERSAL_NOTIFICATION_ID:
//		        	info = parsePayInReversalNotification(requestXmlString);
//		        	break;
			    default:
			    	log.info("Unknown payment notification type");
			    	break;
        	}

//        	if (null != info){
//        		status = ConstantsBase.ENVOY_NOTIFY_SUCCESS;
//        	} else {
//        		status = ConstantsBase.ENVOY_NOTIFY_ERROR;
//        	}
//
//        	log.info("status is  " + status);
//
//	    	// Send success/error respond
//        	HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
//        	response.setContentType("text/xml");
//			response.setCharacterEncoding("utf-8");
//			response.setHeader("Cache-Control", "no-cache");
//
//			try {
//				response.getWriter().write(status);
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Error! Can't write reply!!.", e);
//			}
//
//			context.responseComplete();

     	   if (null != info){
    		   try{
    			   log.info("finish handeling Envoy transaction" );
    			   TransactionsManagerBase.finishEnvoyTransaction(info,notificationType, loginId);
    		   }catch (Exception e) {
    			   log.error("Problem with finishEnvoyTransaction ",e);
    		   }
    	   }

        } catch (Throwable t) {
            throw new ClearingException("Notification failed.", t);
        }
        return info;
    }

    /**
     * Get the notification id from requestXmlString
     *
     * @param requestXmlString the notification XML
     * @return the notification type
     * @throws Exception
     */
    private static int getNotificationType(String requestXmlString) {
    	int notificationType = 0;

	    if (requestXmlString.indexOf("<" + ConstantsBase.ENVOY_PAY_IN_NOTIFICATION + " ") != -1){
	    	notificationType = ConstantsBase.ENVOY_PAY_IN_NOTIFICATION_ID;
	    } else if (requestXmlString.indexOf("<" + ConstantsBase.ENVOY_PAY_OUT_NOTIFICATION + " ") != -1){
	    	notificationType = ConstantsBase.ENVOY_PAY_OUT_NOTIFICATION_ID;
	    } else if (requestXmlString.indexOf("<" + ConstantsBase.ENVOY_PAY_OUT_REVERSAL_NOTIFICATION+ " ") != -1){
	    	notificationType = ConstantsBase.ENVOY_PAY_OUT_REVERSAL_NOTIFICATION_ID;
	    } else if (requestXmlString.indexOf("<" + ConstantsBase.ENVOY_PAY_IN_REVERSAL_NOTIFICATION+ " ") != -1){
	    	notificationType = ConstantsBase.ENVOY_PAY_IN_REVERSAL_NOTIFICATION_ID;
	    }

    	return notificationType;
    }

    private void confirmPayInNotification(EnvoyInfo info) throws Exception {
    	log.info("handlePayInNotification ");
    	try {
    		//  Send Pay-In Confirmation Request
	    	Document requset = createRequest();
	    	log.info("before getEpacsReference " + info.getEpacsReference());

	    	addPayInConfirmation(requset, info.getEpacsReference());
	    	log.info("before transformDocumentToString ");
	        String requsetXML = ClearingUtil.transformDocumentToString(requset);

	        log.info("addPayInConfirmation: XML =====");
	        String responseXML = ClearingUtil.executePOSTRequest(url, requsetXML, false, true, headers,"UTF-8");

	        if (log.isTraceEnabled()) {
	            log.trace(requsetXML);
	        }
	        parsePayInConfirmationResponse(responseXML, info);
    	} catch (Throwable t) {
            throw new ClearingException("handlePayInNotification failed.", t);
        }
    }

    /**
     * Add PayIn Confirmation to the request.
     *
     * @param request
     * @param status
     * @return Wire Card function name.
     */
    private void addPayInConfirmation(Document request, String epacsReference) {
        Element root = request.getDocumentElement();
        String path = "soap:Body/payInConfirmation";
        ClearingUtil.setAttribute(request, path, "xmlns", "http://merchantapi.envoyservices.com");

        ClearingUtil.setNodeValue(root, path + "/auth/username/*", username);
        ClearingUtil.setNodeValue(root, path + "/auth/password/*", password);
        ClearingUtil.setNodeValue(root, path + "/epacsReference/*", epacsReference);
    }

   /**
     * Parse the Pay-In Notification.
     *
     * @param xml the XML to parse
     * @return EnvoyInfo
     * @throws Exception
     */
    private static EnvoyInfo parsePayInNotification(String xml, long loginId) throws Exception {
		EnvoyInfo info = null;

    	try{
	        Document resp = ClearingUtil.parseXMLToDocument(xml);
	        String rootPath = "soap:Body/PaymentNotification/payment";
	        Element result = (Element) ClearingUtil.getNode(resp.getDocumentElement(), rootPath);

	        String uniqueReference = ClearingUtil.getElementValue(result, "uniqueReference/*");
	        log.info("parsePayInNotification:uniqueReference is  " + uniqueReference);
	        if (null != uniqueReference){
		        String epacsReference = ClearingUtil.getElementValue(result, "epacsReference/*");
		        log.info("parsePayInNotification:epacsReference is  " + epacsReference);
		        Transaction tran = null;
		        UserBase user = null;
		        boolean isCurrencyVerified = false;
		        tran = TransactionsManagerBase.getTransactionByEpacsRef(epacsReference);
//		        if (null != tran){
//		        	log.info("parsePayInNotification:tran.id is  " + tran.getId());
//		        	user = UsersManagerBase.getUserById(tran.getUserId());
////		        	tran.setXorIdCapture(epacsReference);
//		        } else {
		        if (null == tran){
		        	long amount = CommonUtil.calcAmount(ClearingUtil.getElementValue(result, "appliedAmount/*"));
		        	log.info("parsePayInNotification:amount is  " + amount);
		        	Pattern p = Pattern.compile("\\d+");
		    	    Matcher m = p.matcher(uniqueReference);
		    	    long userId = -1;
		    	    //Assumption: only 1 number can be in uniquereference
    	    		if (m.find()) {
    	    	    	userId = Long.parseLong(m.group());
    	    	    }
		        	log.info("parsePayInNotification:userId is  " + userId);
		        	String appliedCurrency = ClearingUtil.getElementValue(result, "appliedCurrency/*");
		        	String transferType = ClearingUtil.getElementValue(result, "transferType/*");
		        	user = UsersManagerBase.getUserById(userId);
		        	boolean isOnlineMethod = false;


		        	if (null != user ){
		        		// insert banking transaction - or online that returned status open
			        	String paymentTypeName = null;
			        	tran = new Transaction();

			        	// check that currency matches the user one
			        	if (user.getCurrency().getCode().equals(appliedCurrency)){
			        		tran.setStatusId(TransactionsManagerBase.TRANS_STATUS_IN_PROGRESS);
			        		isCurrencyVerified = true;
			        	}else{
			        		tran.setStatusId(TransactionsManagerBase.TRANS_STATUS_FAILED);
			        		tran.setComments("Applied currency doesn't match user's currency");
			        		log.error("Envoy Pay in notification -  Applied currency doesn't match user's currency for user id: " + userId);
			        	}

			        	if (transferType.trim().equals(ConstantsBase.TRANSFER_TYPE_ONLINE)){
			        		paymentTypeName = ClearingUtil.getElementValue(result, "refererType/*");

			        		tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_ENVOY_ONLINE_DEPOSIT);
			        		isOnlineMethod = true;
			        	}else{
			        		paymentTypeName = ClearingUtil.getElementValue(result, "bankName/*");

			        		tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_ENVOY_BANKING_DEPOSIT);
			        	}

		        		if (!CommonUtil.isParameterEmptyOrNull(paymentTypeName)){
		        			long paymentMethodId = TransactionsManagerBase.handlePaymentMethodId(paymentTypeName);

			        		// if the online methos is already exists in PAYMENT_METHODS table insert it's id
			        		// else insert a new payment method.
			        		if (paymentMethodId > 0){
			        			// Parse online method account num
			        			if (isOnlineMethod){
			        				parseOnlineMethodAccountNum(paymentMethodId, result, tran);
			        			}
			        		}
			        		tran.setPaymentTypeId(paymentMethodId);
		        		}

			        	tran.setAmount(amount);
		    		   	tran.setXorIdAuthorize(epacsReference);
		     		   	tran.setAuthNumber(epacsReference);
		     		   	tran.setDescription(null);
		       		   	tran.setTimeSettled(null);
		     		   	tran.setUtcOffsetSettled(null);
		     		   	tran.setLoginId(loginId);
		     		   	TransactionsManagerBase.insertDirectDepositEnvoy(tran, user);
		        	} else {
		        		log.error("Error! user id " + userId + " wasn't found");
		        		throw new ClearingException();
		        	}

		        }else{
		        	log.error("Error! There is already a transaction with epacs " + epacsReference);
	        		throw new ClearingException();
		        }

		        if (isCurrencyVerified){
		        	info = new EnvoyInfo(user, tran);
		        	info.setEpacsReference(epacsReference);
		        	info.setUniqueReference(uniqueReference);
		        	info.setNotificationType(ConstantsBase.ENVOY_PAY_IN_NOTIFICATION_ID);
		        }

	        } else {
	        	log.error("Error! uniqueReference is null");
	        }

    	} catch (Exception e) {
    		log.error("Error! parsePayInNotification failed", e);
    	}
    	return info;
    }

    /**
     * Parse the Pay-In Confirmation Response.
     *
     * @param xml the XML to parse
     * @param info the transaction info to fill the result values
     * @throws Exception
     */
    private static void parsePayInConfirmationResponse(String xml, EnvoyInfo info) throws Exception {
    	try{
	        Document resp = ClearingUtil.parseXMLToDocument(xml);
	        String rootPath = "soap:Body/payInConfirmationResponse/payInConfirmationResult";
	        Element result = (Element) ClearingUtil.getNode(resp.getDocumentElement(), rootPath);

	        info.setXmlString(xml);

	        String statusCode = ClearingUtil.getElementValue(result, "statusCode/*");
	        info.setResult(statusCode);
	        info.setMessage(ClearingUtil.getElementValue(result, "statusMessage/*"));

	        info.setUniqueReference(ClearingUtil.getElementValue(result, "payment/uniqueReference/*"));

	        String epacsReference = ClearingUtil.getElementValue(result, "payment/epacsReference/*");
	        info.setAuthNumber(epacsReference);
	        info.setProviderTransactionId(epacsReference);

	        info.setPostingDate(ClearingUtil.getElementValue(result, "payment/postingDate/*"));
	        info.setBankCurrency(ClearingUtil.getElementValue(result, "payment/bankCurrency/*"));
	        info.setBankAmount(ClearingUtil.getElementValue(result, "payment/bankAmount/*"));
	        info.setAppliedCurrency(ClearingUtil.getElementValue(result, "payment/appliedCurrency/*"));
	        info.setAppliedAmount(ClearingUtil.getElementValue(result, "payment/appliedAmount/*"));
	        info.setCountryA2(ClearingUtil.getElementValue(result, "payment/countryCode/*"));
	        info.setBankInformation(ClearingUtil.getElementValue(result, "payment/bankInformation/*"));

	        info.setSuccessful(Integer.parseInt(statusCode) >= 0);
    	} catch (Exception e) {
    		log.error("Error! parsePayInConfirmationResponse Failed!");
    	}

    }

    /**
     * Parse the PayOut Notification.
     *
     * @param xml the XML to parse
     * @return EnvoyInfo
     * @throws Exception
     */
    private static EnvoyInfo parsePayOutNotification(String xml) throws Exception {
    	EnvoyInfo info = null;

    	try{
    		log.info("parsePayOutNotification");
	        Document resp = ClearingUtil.parseXMLToDocument(xml);
	        String rootPath = "soap:Body/PaymentOutNotification/paymentDetails";
	        Element result = (Element) ClearingUtil.getNode(resp.getDocumentElement(), rootPath);

	        String merchantPaymentReference =
	        	ClearingUtil.getElementValue(result, "originalPaymentInfo/merchantPaymentReference/*");
	        log.info("merchantPaymentReference = " + merchantPaymentReference);
	        if (null != merchantPaymentReference){
	    		long tranId = Long.parseLong(merchantPaymentReference);
	    		log.info("resolved tranId = " + tranId);
	    		info = getEnvoyInfoByTranId(tranId);

	    		log.info("info tran id = " + info.getTransactionId());

	    		if (null != info){
		        	info.setNotificationType(ConstantsBase.ENVOY_PAY_OUT_NOTIFICATION_ID);
			        info.setEpacsReference(ClearingUtil.getElementValue(result, "paymentResult/epacsReference/*"));
			        info.setXmlString(xml);
			        info.setSuccessful(true);
		        }
	        }
    	} catch (Exception e) {
    		log.error("Error! parsePayOutNotification failed");
    	}
    	return info;
    }

    /**
     * Parse the PayOutReversal Notification.
     *
     * @param xml the XML to parse
     * @return EnvoyInfo
     * @throws Exception
     */
    private static EnvoyInfo parsePayOutReversalNotification(String xml) throws Exception {
    	EnvoyInfo info = null;

    	try{
	        Document resp = ClearingUtil.parseXMLToDocument(xml);
	        String rootPath = "soap:Body/PaymentOutReversalNotification/reversalInfo";
	        Element result = (Element) ClearingUtil.getNode(resp.getDocumentElement(), rootPath);

	        String merchantPaymentReference =
	        	ClearingUtil.getElementValue(result, "originalPaymentInfo/merchantPaymentReference/*");

	        if (null != merchantPaymentReference){
	    		long tranId = Long.parseLong(merchantPaymentReference);
	    		info = getEnvoyInfoByTranId(tranId);

	    		if (null != info){
		        	info.setNotificationType(ConstantsBase.ENVOY_PAY_OUT_NOTIFICATION_ID);
			        info.setEpacsReference(ClearingUtil.getElementValue(result, "debit/epacsReference/*"));
			        info.setXmlString(xml);
			        info.setSuccessful(false);
			        info.setResult("-1");
            		info.setMessage("PayOut was reverse.");
		        }
	        }
    	} catch (Exception e) {
    		log.error("Error! parsePayOutNotification failed");
    	}
    	return info;
    }

    /**
     * Parse the PayOutReversal Notification.
     *
     * @param xml the XML to parse
     * @return EnvoyInfo
     * @throws Exception
     */
    private static EnvoyInfo parsePayInReversalNotification(String xml) throws Exception {
    	EnvoyInfo info = null;

    	try{
	        Document resp = ClearingUtil.parseXMLToDocument(xml);
	        String rootPath = "soap:Body/PaymentInReversalNotification/reversalInfo";
	        Element result = (Element) ClearingUtil.getNode(resp.getDocumentElement(), rootPath);

	        String epacsReference =
	        	ClearingUtil.getElementValue(result, "credit/epacsReference/*");

	        if (null != epacsReference){
//	    		Transaction tran = TransactionsManagerBase.getTransactionByEpacsRef(epacsReference);
//	    		info = getEnvoyInfoByTranId(tran.getId());
//
//	    		if (null != info){
//		        	info.setNotificationType(ConstantsBase.ENVOY_PAY_IN_NOTIFICATION_ID);
//			        info.setEpacsReference(ClearingUtil.getElementValue(result, "debit/epacsReference/*"));
//			        info.setXmlString(xml);
//			        info.setSuccessful(false);
//			        info.setResult("-1");
//            		info.setMessage("PayIn was reverse.");
//		        }
	        	Transaction tran = new Transaction();

	        	tran.setStatusId(TransactionsManagerBase.TRANS_STATUS_IN_PROGRESS);
	        	tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_ENVOY_BANKING_DEPOSIT);
//	        	tran.setAmount(amount);
    		   	tran.setXorIdAuthorize(epacsReference);
     		   	tran.setAuthNumber(epacsReference);
     		   	tran.setDescription(null);
       		   	tran.setTimeSettled(null);
     		   	tran.setUtcOffsetSettled(null);
//     		   	TransactionsManagerBase.insertDirectDepositEnvoy(tran, user);
	        }
    	} catch (Exception e) {
    		log.error("Error! parsePayOutNotification failed");
    	}
    	return info;
    }

    private static EnvoyInfo getEnvoyInfoByTranId(long tranId) throws SQLException{
    	EnvoyInfo info = null;

    	log.info("wewe tranId is  " + tranId);
    	Transaction tran = TransactionsManagerBase.getTransaction(tranId);

    	if (null != tran){
    		log.info("wewe tran.userId is  " + tran.getUserId());
    		UserBase user = UsersManagerBase.getUserById(tran.getUserId());

	        if (null != user){
	        	log.info("wewe User is  " + user.getId());
	        	info = new EnvoyInfo(user, tran);
	        }
    	}
        return info;
    }

    public static void parseOnlineMethodAccountNum(long paymentMethodId, Element result, Transaction tran){
    	String methodInfo = ClearingUtil.getElementValue(result, "bankInformation/*");

		if (paymentMethodId == TransactionsManagerBase.PAYMENT_TYPE_MONETA){
			String[] tokens = methodInfo.split(",",99);
			tran.setEnvoyAccountNum(tokens[POSITION_MONETA].trim());
		}else if (paymentMethodId == TransactionsManagerBase.PAYMENT_TYPE_WEB_MONEY){
			String[] tokens = methodInfo.split(",",99);
			tran.setEnvoyAccountNum(tokens[POSITION_WEBMONEY].trim());
		}
    }

	@Override
	public void cancel(ClearingInfo info) throws ClearingException {
		// TODO Auto-generated method stub
		log.info("NOT IMPLEMENTED !!!");
	}
}
