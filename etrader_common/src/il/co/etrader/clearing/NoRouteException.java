//package il.co.etrader.clearing;
//
//import com.anyoption.common.clearing.ClearingException;
//
//public class NoRouteException extends ClearingException {
//    private static final long serialVersionUID = 1L;
//
//    public NoRouteException() {
//        super();
//    }
//
//    public NoRouteException(String message) {
//        super(message);
//    }
//
//    public NoRouteException(String message, Throwable t) {
//        super(message, t);
//    }
//}