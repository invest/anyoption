package il.co.etrader.clearing.jobs;

import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.clearing.ACHInfo;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;
import com.anyoption.common.managers.TransactionsManagerBase.TransactionSource;
import com.anyoption.common.util.AppsflyerEventSender;
import com.anyoption.common.util.ClearingUtil;

/**
 * ACH "capture" deposit.
 * Go over the pending ACH transactions and check transaction status via ACH API
 *
 * @author Kobi
 */
public class ACHDepositJob extends JobUtil {
    private static Logger log = Logger.getLogger(ACHDepositJob.class);

    public static StringBuffer report = new StringBuffer();
    public static ReportSummary summery = new ReportSummary();
    public static String lastCurrencyCode = null;
    public static String skinName;
    public static String classId;

	/**
	 * Goes over the ACH pending transactions in the db from more than the last 48 hours
	 * And make TransactionStatus call
	 */
	private static void captureTransactions() {
		report = new StringBuffer("<br><b>ACH deposit transactions:</b><table border=\"1\">");
		report.append("<tr><td>User id</td><td>Transaction id</td><td>Status</td>" +
					  "<td>Description</td><td>Amount</td><td>Currency</td></tr>");
		Connection conn1 = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String utcOffset = ConstantsBase.EMPTY_STRING;

		String transactionId = getPropertyByFile("transaction.id", ConstantsBase.EMPTY_STRING);

		try {
			conn1 = getConnection();
            ClearingManager.loadClearingProviders(conn1);
            PopulationsManagerBase.loadPopulatioHandlers(conn1);
			String sql =
            	"SELECT " +
                    "t.user_id userid, " +
                    "t.id tid, " +
                    "curr.code, "+
                    "t.amount amount, " +
                    "w.user_name writer, " +
                    "u.user_name user_name, " +
                    "t.fee_cancel, " +
                    "u.skin_id, " +
                    "u.utc_offset, " +
                    "u.currency_id, " +
                    "s.name, " +
                    "u.class_id " +
            	"FROM " +
                    "transactions t, " +
                    "writers w, " +
                    "users u, " +
                    "skins s, " +
                    "currencies curr, " +
                    "limits l " +
            	"WHERE " +
                    "t.user_id = u.id AND " +
                    "t.writer_id = w.id AND " +
                    "s.id = u.skin_id AND " +
                    "curr.id = u.currency_id AND " +
                    "u.limit_id = l.id AND " +
                    "u.class_id <> 0 AND " +
                    "u.skin_id <> 1 AND " +
                    "t.status_id = ? AND ";

                	// Add an option to run job with specific transcation id
                    if (CommonUtil.isParameterEmptyOrNull(transactionId)) {
                       	sql +=
                            "t.type_id = ? AND " +
                       		"t.time_created < sysdate - (48/24) ";

                        if (log.isInfoEnabled()) {
                            log.info("No transaction.id specified");
                        }
                    } else {
                    		sql +=
                    			"t.id = " + transactionId + " AND t.type_id = ? ";
                    }

                sql +=
                	"ORDER BY " +
                    	"curr.id, userid ,tid";

			pstmt = conn1.prepareStatement(sql);

			pstmt.setLong(1, TransactionsManagerBase.TRANS_STATUS_STARTED);
			pstmt.setLong(2, TransactionsManagerBase.TRANS_TYPE_ACH_DEPOSIT);

			rs = pstmt.executeQuery();

			while (rs.next()) {
                ACHInfo info = new ACHInfo();
                String currentCurrencyCode = rs.getString("code");
                info.setCurrencySymbol(currentCurrencyCode);
                info.setCurrencyId(rs.getInt("currency_id"));
                info.setUserId(rs.getLong("userid"));
                info.setUserName(rs.getString("user_name"));
                info.setSkinId(rs.getLong("skin_id"));
                info.setTransactionType(TransactionsManagerBase.TRANS_TYPE_ACH_DEPOSIT);
                info.setTransactionId(rs.getInt("tid"));
                info.setAmount(rs.getLong("amount"));
                utcOffset = rs.getString("utc_offset"); //save user utc offset
                skinName = rs.getString("name");
                classId = rs.getString("class_id");

                log.info("Going to call transactionStatusCall: " + info.toString());
                try {
                	ClearingManager.ACHStatusReq(info);
                } catch (ClearingException ce) {
                    log.log(Level.ERROR, "Failed to capture ACH transaction: " + info.toString(), ce);
                    info.setResult("999");
                    info.setMessage(ce.getMessage());
                }

                // currency code display
                if ( null == lastCurrencyCode || !lastCurrencyCode.equals(currentCurrencyCode) ) {
                	lastCurrencyCode = currentCurrencyCode;
                    report.append("<tr><td colspan=\"7\" align=\"center\" valign=\"middle\"><b>").append(lastCurrencyCode).append("</b></td></tr>");
                }

                if (info.isSuccessful()) {
                    report.append("<tr>");
                } else if (!info.isSuccessful() &&
            			(info.getStatus().equals("Rejected") || info.getStatus().equals("Returned"))) { // Failed
               		report.append("<tr style=\" color: #FF0000; font-weight: bold;\">");
                } else {  // Pending
                	report.append("<tr style=\" color: #C24641; font-weight: bold;\">");
                }

                report.append("<td>").append(info.getUserId()).append("</td><td>");
                report.append(info.getTransactionId()).append("</td><td>");
                report.append(info.getResult()).append("</td><td>");
                report.append(info.getMessage()).append("</td><td>");
                report.append(ClearingUtil.formatAmount(info.getAmount())).append("</td><td>");
                report.append(currentCurrencyCode).append("</td></tr>\n");
                updateTransaction(info, utcOffset, info.isSuccessful());
                summery.addTransaction(rs.getString("writer"), "ACH", info.getAmount(), currentCurrencyCode, info.isSuccessful());
             }
        } catch (Exception e) {
            log.log(Level.ERROR, "Error while checking ACH transactions.", e);
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                pstmt.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                conn1.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
        }
        report.append("</table>");
    }


   /**
	 * Update transaction after send it via PayPal
	 * @param info ClearingInfo instance of the current trx that captured
	 * @param hasFee true in case we need to create fee transaction
	 * @param feeAmount fee amount to take
	 * @param utcOffset user utcOffset
	 * @throws SQLException
	 */
	public static void updateTransaction(ACHInfo info, String utcOffset, boolean isSuccessful) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		Transaction tran = null;
		boolean tranSuccessfullAndUpdate = false;
        try {
        	conn = getConnection();
        	String updateSql;
        	if (isSuccessful) { //handle success from provider

            	try {
            		Transaction t = new Transaction();
            		t.setId(info.getTransactionId());
        			PopulationsManagerBase.deposit(conn, info.getUserId(), true, Writer.WRITER_ID_AUTO, t);
        		} catch (Exception e) {
        			log.warn("Problem with population succeed deposit event " + e);
        		}

                try {
                    TransactionsManagerBase.afterTransactionSuccess(conn, info.getTransactionId(), info.getUserId(), info.getAmount(), Writer.WRITER_ID_AUTO, 0L);
                } catch (Exception e) {
                	log.error("Problem processing bonuses after success transaction", e);
                }

        		try {
        			conn.setAutoCommit(false);
    				UsersDAOBase.addToBalance(conn, info.getUserId(), info.getAmount());
    				GeneralDAO.insertBalanceLog(conn, Writer.WRITER_ID_AUTO, info.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, info.getTransactionId(), ConstantsBase.LOG_BALANCE_ACH_DEPOSIT, utcOffset);
    				TransactionsDAOBase.updatePurchase(conn, info.getTransactionId(), TransactionsManagerBase.TRANS_STATUS_SUCCEED,
    								null, info.getResult() + info.getMessage(), null, utcOffset, Writer.WRITER_ID_AUTO);
    				conn.commit();
                    try {
                        UserBase user = new UserBase();
                        tran = new Transaction();
                        tran.setAmount(info.getAmount());
                        tran.setTimeCreated(new Date());
                        user.setSkinName(skinName);
                        user.setClassId(new Long(classId));
                        user.setCurrencyId(Long.valueOf(info.getCurrencyId()));
                        user.setUserName(info.getUserName());
                        CommonUtil.alertOverDepositLimitByEmail(tran, user);
                        tranSuccessfullAndUpdate = true;
                    } catch (Exception e) {
                        log.error("Exception in success ACH deposit Email! ", e);
                    }
            	} catch (Exception e) {
            		log.error("Exception in success deposit Transaction! ", e);
    				try {
    					conn.rollback();
    				} catch (Throwable it) {
    					log.error("Can't rollback.", it);
    				}
            	} finally {
                    try {
                        conn.setAutoCommit(true);
                    } catch (Exception e) {
                        log.error("Can't set back to autocommit.", e);
                    }
        		}
        	} else if (!isSuccessful &&
        			(info.getStatus().equals("Rejected") || info.getStatus().equals("Returned"))) { //handle failed from provider
        		updateSql =
                    "UPDATE " +
                        "transactions " +
                    "SET " +
                        "time_settled = sysdate, " +
                        "status_id = ? , " +
      	 				"comments = comments || ? || '|' || ? , " +
      	 				"description = ? , " +
        			    "utc_offset_settled = ? " +
    			    "WHERE " +
    			  		"id = ? ";

       				 ps = conn.prepareStatement(updateSql);
					 ps.setInt(1, TransactionsManagerBase.TRANS_STATUS_FAILED);
					 ps.setString(2, info.getResult());
					 ps.setString(3, info.getMessage());
					 ps.setString(4, ConstantsBase.ERROR_FAILED_CAPTURE);
					 ps.setString(5, utcOffset);
					 ps.setLong(6, info.getTransactionId());
					 ps.executeUpdate();
        	} else {
        		updateSql =
                    "UPDATE " +
	                    "transactions " +
	                "SET " +
	  	 				"comments = comments || ? || '|' || ?  " +
				    "WHERE " +
				  		"id = ? ";

       				 ps = conn.prepareStatement(updateSql);
					 ps.setString(1, info.getStatus());
					 ps.setString(2, info.getMessage());
					 ps.setLong(3, info.getTransactionId());
					 ps.executeUpdate();
        	}
        	UserBase user = new UserBase();
        	user.setUserName(info.getUserName());
        	TransactionsManagerBase.afterDepositAction(user, tran, TransactionSource.TRANS_FROM_JOB);
        } catch (Exception e) {
			log.log(Level.ERROR, "ERROR! Can't update transaction!. ", e);
		} finally {
			try {
        		conn.close();
        	} catch (Exception e) {
        		log.log(Level.ERROR, "Can't close: conn2", e);
        	}
		}
    	if (tranSuccessfullAndUpdate) {
    		// Reward plan
    		int countRealDeposit = 0;
    		try {
            	countRealDeposit = TransactionsManagerBase.countRealDeposit(info.getUserId());
            	if (countRealDeposit == ConstantsBase.NUM_OF_REAL_SUCCESS_DEPOSIT) { // check if this first deposit 
            		RewardUserTasksManager.rewardTasksHandler(TaskGroupType.FIRST_TIME_DEPOSIT_AMOUNT, info.getUserId(),  
            				info.getAmount(), info.getTransactionId(), BonusManagerBase.class, (int) Writer.WRITER_ID_AUTO);
            	}
            } catch (Exception e) {
                log.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + " RewardUserTasksManager exception after success transaction", e);
            }
    		
    		try {
    			if (countRealDeposit == ConstantsBase.NUM_OF_REAL_SUCCESS_DEPOSIT) {
    				new AppsflyerEventSender(info.getTransactionId(), ConstantsBase.SERVER_PIXEL_TYPE_FIRST_DEPOSIT).run();
    			}
    		} catch (Exception e1) {
    			log.error("cant run appsflyer event sender", e1);
    		}
    	}  
	}


    /**
     * Capture PayPal Withdrawal Job.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }
        propFile = args[0];

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the job.");
        }

        captureTransactions();

        String reportBody = "<html><body>" +
            summery.getSummery() +
            report.toString() +
            "</body></html>";

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Sending report email: " + reportBody);
        }

        // send email
        Hashtable<String, String> server = new Hashtable<String, String>();
        server.put("url", getPropertyByFile("email.url"));
        server.put("auth", getPropertyByFile("email.auth"));
        server.put("user", getPropertyByFile("email.user"));
        server.put("pass", getPropertyByFile("email.pass"));
        server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", getPropertyByFile("email.subject"));
        email.put("to", getPropertyByFile("email.to"));
        email.put("from", getPropertyByFile("email.from"));
        email.put("body", reportBody);
        sendEmail(server, email);

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Job completed.");
        }
    }
}