package il.co.etrader.clearing.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.clearing.CDPayInfo;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.DeltaPayInfo;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawEntryResult;
import com.anyoption.common.payments.WithdrawUtil;
import com.anyoption.common.util.ClearingUtil;

import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

public class CupWithdrawalJob  extends JobUtil {
    public static int RUN_CREDIT = 2;  // (credit/bookBack)
    public static int DONT_RUN = 0;

    private static Logger log = Logger.getLogger(CupWithdrawalJob.class);
    public static StringBuffer report = new StringBuffer();
    //public static ReportSummary summery = new ReportSummary ();
    
    public static void main(String[] args) throws Exception {
        if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }
        propFile = args[0];

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the job.");
        }
        process();
        
        String reportBody = "<html><body>" +
                //summery.getSummery() +
                report.toString() +
                "</body></html>";
        
        // send email
        Hashtable<String, String> server = new Hashtable<String, String>();
        server.put("url", getPropertyByFile("email.url"));
        server.put("auth", getPropertyByFile("email.auth"));
        server.put("user", getPropertyByFile("email.user"));
        server.put("pass", getPropertyByFile("email.pass"));
        server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", getPropertyByFile("email.subject"));
        email.put("to", getPropertyByFile("email.to"));
        email.put("from", getPropertyByFile("email.from"));
        email.put("body", reportBody);
        sendEmail(server, email);

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Job completed.");
        }
    }
    
public static void process() throws Exception{
	
		report = new StringBuffer("<br><b>Capture withdraw transactions:</b><table border=\"1\">");
		report.append("<tr><td>User id</td><td>Transaction id</td><td>Provider</td><td>Status</td>" +
					  "<td>Description</td><td>Amount</td><td>Currency</td><td>Type</td></tr>");
	
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String countBeforeCUPproperty = getPropertyByFile("countBeforeCUP");
		boolean countBeforeCUP = (countBeforeCUPproperty!=null && countBeforeCUPproperty.equals("true"));
		try {

			conn = getConnection();
			ClearingManager.loadClearingProviders(conn);

			String sql = getSql();

			pstmt = conn.prepareStatement(sql);

			pstmt.setLong(1, TransactionsManagerBase.TRANS_STATUS_APPROVED);
			pstmt.setLong(2, ConstantsBase.ACCOUNTING_APPROVED_YES);
			pstmt.setLong(3, TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW);
			rs = pstmt.executeQuery();

			while (rs.next()) {

				long userId = rs.getLong("userid");
				long amount = rs.getLong("amount");
				//long originalAmount = amount;
				String writer = rs.getString("writer");
		        boolean hasFee = rs.getInt("fee_cancel") == JobTransactionUtil.TRANSACTION_HAS_FEE;
		        long feeAmount = rs.getLong("cc_fee");
		        String utcOffset = rs.getString("utc_offset"); //save user utc offset
		        
		        ClearingInfo ci = createClearingInfo(rs, amount);
				HashMap<Long,Transaction> updateTrxList = new HashMap<Long,Transaction>(); // <withdrawalTrxId, internalTrxKey>		        
		        if (hasFee) {
		        	amount = amount - feeAmount;
		        	Transaction feeTransaction = JobTransactionUtil.insertFeeTransaction(conn, ci, feeAmount, utcOffset);
		        	updateTrxList.put(feeTransaction.getId(), feeTransaction);
					report.append("<tr>");
			        report.append("<td>").append(ci.getUserId()).append("</td><td>");
			        report.append(feeTransaction.getId()).append("</td><td>");
			        report.append("&nbsp;--&nbsp;").append("</td><td>");
			        report.append("&nbsp;--&nbsp;").append("</td><td>");
			        report.append("Fee amount").append("</td><td>");
			        report.append(ClearingUtil.formatAmount(feeAmount)).append("</td><td>");
			        report.append(ci.getCurrencySymbol()).append("</td><td>");
			        report.append("CC Withdraw Fee").append("</td></tr>\n");
		        }

		        Long providersByPriority = TransactionsDAOBase.getProvidersPriorityForUser(conn, userId);
		        
				ArrayList<Transaction> cdPayDepositsList = TransactionsDAOBase.getDepositListForProvider(conn, userId, ClearingManager.CDPAY_PROVIDER_ID, countBeforeCUP);
				Long cdPayAmount = calculateSum(cdPayDepositsList);

				ArrayList<Transaction> deltapayDepositsList = TransactionsDAOBase.getDepositListForProvider(conn, userId, ClearingManager.DELTAPAY_CHINA_PROVIDER_ID, countBeforeCUP);
				Long deltapayAmount = calculateSum(deltapayDepositsList);
				

				boolean splitSuccess = false;
				try{
					//TODO refactor when more then 2 providers
					if(amount > cdPayAmount && amount > deltapayAmount && amount <=(cdPayAmount+deltapayAmount) ) {
						// distribute the withdrawal between CUP providers
						
						if(providersByPriority == ClearingManager.CDPAY_PROVIDER_ID){
							ci.setAmount(cdPayAmount);
							boolean a = splitTransactionSingleProvider(ci, conn, cdPayDepositsList, updateTrxList, ClearingManager.CDPAY_PROVIDER_ID, utcOffset, writer);
							ci.setAmount(amount - cdPayAmount);
							boolean b = splitTransactionSingleProvider(ci, conn, deltapayDepositsList, updateTrxList, ClearingManager.DELTAPAY_CHINA_PROVIDER_ID, utcOffset, writer);
							splitSuccess = a&&b;
						} else {
							ci.setAmount(deltapayAmount);
							boolean a = splitTransactionSingleProvider(ci, conn, deltapayDepositsList,updateTrxList,  ClearingManager.DELTAPAY_CHINA_PROVIDER_ID, utcOffset, writer);
							ci.setAmount(amount - deltapayAmount);
							boolean b = splitTransactionSingleProvider(ci, conn, cdPayDepositsList, updateTrxList, ClearingManager.CDPAY_PROVIDER_ID,  utcOffset, writer);
							splitSuccess = a&&b;
						}
						
					} else if ( amount <= cdPayAmount && amount >= deltapayAmount   ){
						ci.setAmount(amount);
						splitSuccess = splitTransactionSingleProvider(ci, conn, cdPayDepositsList, updateTrxList, ClearingManager.CDPAY_PROVIDER_ID, utcOffset, writer);
					} else if( amount <= deltapayAmount && amount >= cdPayAmount) {
						ci.setAmount(amount);
						splitSuccess = splitTransactionSingleProvider(ci, conn, deltapayDepositsList, updateTrxList, ClearingManager.DELTAPAY_CHINA_PROVIDER_ID,  utcOffset, writer);
					} else if( amount < cdPayAmount && amount < deltapayAmount) {
						if(providersByPriority == ClearingManager.CDPAY_PROVIDER_ID){
							ci.setAmount(amount);
							splitSuccess = splitTransactionSingleProvider(ci, conn, cdPayDepositsList, updateTrxList, ClearingManager.CDPAY_PROVIDER_ID, utcOffset, writer);
						} else {
							ci.setAmount(amount);
							splitSuccess = splitTransactionSingleProvider(ci, conn, deltapayDepositsList, updateTrxList, ClearingManager.DELTAPAY_CHINA_PROVIDER_ID, utcOffset, writer);
						}
					}
				}catch(Exception e) {
					log.debug("Capture of transaction"+ci.getTransactionId()+" failed");
					log.debug(e.getMessage());
					splitSuccess = false;
				}

				//ci.setAmount(originalAmount);
				updateTransaction(ci, utcOffset, splitSuccess, updateTrxList.keySet().toString());
				if(splitSuccess) {
					report.append("<tr bgcolor=\"#eeeeeeee\">");
				} else {
		       		report.append("<tr style=\" color: #C24641; font-weight: bold;\">");
		        }
		        report.append("<td>").append(ci.getUserId()).append("</td><td>");
		        report.append(ci.getTransactionId()).append("</td><td>");
		        report.append("&nbsp").append("</td><td>");
		        report.append("&nbsp").append("</td><td>");
		        report.append("Splitted to:").append(updateTrxList.keySet().toString()).append("</td><td>");
		        report.append(ClearingUtil.formatAmount(ci.getAmount())).append("</td><td>");
		        report.append(ci.getCurrencySymbol()).append("</td><td>");
		        report.append("CUP Withdraw").append("</td></tr>\n");
			}
		} catch (SQLException e) {
			log.debug(e.getMessage());
		} finally {
			  try {
	                rs.close();
	                pstmt.close();
	                conn.close();	                
	            } catch (Exception e) {
	                log.log(Level.ERROR, "Can't close", e);
	            }
		}
		report.append("</table>");
    }

	public static boolean isProviderFirstPriority(ArrayList<Long> providersByPriority, Long provider_id) {
		if(providersByPriority  == null ) {
			return false;
		}
		if( providersByPriority.size() == 0 ) {
			return false;
		}
		
		return ( providersByPriority.get(0)  == provider_id) ;
	}
	
public static ClearingInfo createClearingInfo(ResultSet rs, long amount) throws SQLException {
	ClearingInfo info = new ClearingInfo();
    info.setTransactionId(rs.getInt("tid"));
    String currentCurrencyCode = getPropertyByFile("currency");
    if (currentCurrencyCode.equals("?currency?")) {
    	currentCurrencyCode = rs.getString("code");
    }
    info.setCurrencySymbol(currentCurrencyCode);
    info.setCurrencyId(rs.getInt("currency_id"));
    info.setAuthNumber(rs.getString("auth_number"));
    info.setAmount(rs.getLong("amount"));
    info.setUserId(rs.getLong("userid"));
    info.setUserName(rs.getString("user_name"));
    info.setSkinId(rs.getLong("skin_id"));
    if (CommonUtil.isHebrewSkin(info.getSkinId())) {
    	info.setCardUserId(rs.getString("idnum"));
    } else {  // Ao users(no id number)
    	info.setCardUserId(ConstantsBase.NO_ID_NUM);
    }
    info.setCountryId(rs.getLong("country_id"));
    info.setCountryA2(rs.getString("a2"));
    info.setIp(rs.getString("ip"));
    info.setTransactionType(TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
    info.setTransactionType(rs.getLong("transaction_type_id"));
    
    info.setProviderId(rs.getLong("clearing_provider_id"));
    info.setAmount(amount);

    return info;
}
    
    static long calculateSum(ArrayList<Transaction> t){
    	long a = 0;
    	for(Transaction t1: t){
    		a += t1.getCreditAmount();
    	}
    	return a;
    }
    
    static boolean splitTransactionSingleProvider(ClearingInfo info, Connection conn, ArrayList<Transaction> depositsList , HashMap<Long,Transaction> updateTrxList, long providerId, String utcOffset, String writer) throws Exception{
    	// the amount to split
		long creditAmount = info.getAmount();
		boolean success = true;

		for (Transaction depositTransaction : depositsList) {
			// available in this deposit
			long availableAmount = depositTransaction.getCreditAmount();
			long toCredit = Math.min(availableAmount, creditAmount);
			creditAmount -= availableAmount;
			
			Transaction internalTransaction = JobTransactionUtil.createInternalTrx(info, utcOffset, toCredit, providerId, depositTransaction.getId());
			if (null != internalTransaction) {
				ClearingInfo innerInfo = null;
				if(providerId == ClearingManager.DELTAPAY_CHINA_PROVIDER_ID) {
					innerInfo =  creatDeltaPayInternalInfo(info, internalTransaction, depositTransaction);
				} else if( providerId == ClearingManager.CDPAY_PROVIDER_ID) {
					innerInfo =  createCDPayInternalInfo(info, internalTransaction, depositTransaction);
					ClearingManager.setCDPayDepositDetails((CDPayInfo)innerInfo, ClearingManager.CDPAY_PROVIDER_ID);
				}
				
				boolean res = captureWithdrawalSingleTrx(innerInfo, writer, info.getTransactionId(), conn, info.getCurrencySymbol(), utcOffset, false, 0);
				updateTrxList.put(internalTransaction.getId(), depositTransaction);				
				if (res) {
					long credit_amount = internalTransaction.getAmount();
					// fix for deltapay single bookback 
					if(providerId == ClearingManager.DELTAPAY_CHINA_PROVIDER_ID) {
						credit_amount = depositTransaction.getAmount();
					} 
					updateCreditAmount(credit_amount, depositTransaction.getId());
				} else {
					success = false;
				}
			} else { 
				creditAmount += availableAmount;
			}
			
			if (creditAmount <= 0) {
				break;
			}
		}
		
		return success;
    }
    
    /**
	 * withdrawal action to 1 trx.
	 * @param type withdrawal type(cft/credit)
	 * @param info ClearingInfo instance of the trx
	 * @param currentCurrencyCode currency code of the trx
	 * @param utcOffset trx offset
	 * @param hasFee in case we need to take fee
	 * @param feeAmount fee amount
	 * @param conn1 db connection
	 * @param rs ResultSet instance
	 * @return true in case the process completed successfully
	 * @throws SQLException
	 */
	public static boolean captureWithdrawalSingleTrx(ClearingInfo info, String writer, Long mainTransaction, Connection conn1, String currentCurrencyCode,
			String utcOffset, boolean hasFee1, long feeAmount) throws SQLException {

		String providerName = "";
		String withdrawalType = "";
		boolean captureRes = true;
	
		withdrawalType = "(Credit)";
		

        if (log.isEnabledFor(Level.INFO)) {

        		log.log(Level.INFO, "Going to capture transaction" + withdrawalType + ": " + info.toString());
        }
        try {

        		if( info instanceof DeltaPayInfo) {
        			ClearingManager.setDeltaPayChinaBookBack((DeltaPayInfo)info);
        		}

        		if( info instanceof CDPayInfo){
        			ClearingManager.setCDPayChinaBookBack((CDPayInfo)info);
        		}

        } catch (ClearingException ce) {
            log.log(Level.ERROR, "Failed to capture withdrawal transaction" + withdrawalType + ": " + info.toString(), ce);
            info.setResult("999");
            info.setMessage(ce.getMessage());
            captureRes = false;
        }

        // currency code display
        //report.append("<tr><td colspan=\"7\" align=\"center\" valign=\"middle\"><b>").append(currentCurrencyCode).append("</b></td></tr>");

        if (info.isSuccessful()) {
            report.append("<tr>");
        } else {
       		report.append("<tr style=\" color: #C24641; font-weight: bold;\">");
        }

        providerName = ClearingManager.getProviderName(info.getProviderId());

        report.append("<td>").append(info.getUserId()).append("</td><td>");

        String actionType = "";
        String splittedId = "";  // display splittedId for internal credit trx
       	splittedId = "(splittedTrx:" + String.valueOf(mainTransaction) + ")";

        report.append(info.getTransactionId()).append(splittedId).append("</td><td>");
        report.append(providerName).append("</td><td>");
        report.append(info.getResult()).append("</td><td>");
        report.append(info.getMessage()).append("</td><td>");
        report.append(ClearingUtil.formatAmount(info.getAmount())).append("</td><td>");
        report.append(currentCurrencyCode).append("</td><td>");

    	actionType = "CUP Credit(internal)";

        report.append(actionType).append("</td></tr>\n");

        if (info.isSuccessful()) {
        	captureRes = true;
        } else {
        	captureRes = false;
        }

        JobTransactionUtil.updateTransaction(info, utcOffset, info.isSuccessful(), false, null, 0);

        return captureRes;
    }
	
	

	public static void updateCreditAmount(long amount, long depositTrxId) {

		Connection con = null;
		PreparedStatement ps2 = null;

		try {
			con = getConnection();
			
			String sql2 = "UPDATE " +
						  	 "transactions " +
						  "SET " +
						  	 "credit_amount = credit_amount + ? " +
						  "WHERE " +
						  	 "id = ? ";

			ps2 = con.prepareStatement(sql2);
			ps2.setLong(1, amount);
			ps2.setLong(2, depositTrxId);
			ps2.executeUpdate();
			con.commit();

		} catch (Exception e) {
			log.log(Level.ERROR, "ERROR! Can't update transaction after credit. ", e);
			try {
				con.rollback();
			} catch (Exception er) {
				log.log(Level.ERROR, "Can't rollBack! ", er);
			}
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException e) {
				log.error("Can't set back to autocommit.", e);
			}
        	try {
        		ps2.close();
        	} catch (Exception ep2) {
        		log.log(Level.ERROR, "Can't close: PreparedStatement2!", ep2);
        	}
        	try {
        		con.close();
        	} catch (Exception ec) {
        		log.log(Level.ERROR, "Can't close: connection!", ec);
			}
		}
	}

    
    
    public static void updateTransaction(ClearingInfo info, String utcOffset, boolean isSuccessful, String internalTransactions) throws SQLException {

		Connection conn = null;
		PreparedStatement ps = null;
        try {
        	conn = getConnection();
            String updateSql;
        	if (isSuccessful) { //handle success from provider
              	//update transaction status to success and amount to the amount after fee (incase there was fee)
        		updateSql =
                    "UPDATE " +
                        "transactions " +
                    "SET " +
                        "time_settled = sysdate, " +
                        "status_id =  ?, ";
				updateSql += "comments =  ? || '|' || ? || '|' || ? || '| SplittedTrx to: ' || ? , ";
				updateSql += "amount =  ?, " + "utc_offset_settled = ?, " + "clearing_provider_id = ? ";
				updateSql += ",is_credit_withdrawal = 1 ";
				updateSql += "WHERE " + "id = ? ";

				ps = conn.prepareStatement(updateSql);
				ps.setInt(1, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
				ps.setString(2, info.getResult());
				ps.setString(3, info.getMessage());
				ps.setString(4, info.getUserMessage());
				ps.setString(5, internalTransactions);
				ps.setLong(6, info.getAmount());
				ps.setString(7, utcOffset);
				ps.setLong(8, info.getProviderId());
				ps.setLong(9, info.getTransactionId());

				//!!!										   !!!
				//!!CRITICAL SECTION - must be atomic statement!!!
				//!!!										   !!!
				//method assumption - make one time functionality for each transaction.
				UserBase user = UsersDAOBase.getUserById(conn, info.getUserId());
				WithdrawEntryResult finalWithdrawEntryResult = WithdrawUtil.finalWithdrawEntry(new WithdrawEntryInfo(user, info.getTransactionId()));
				
        		conn.setAutoCommit(false); //in order to support rollback once and update or an insert was not succesfull


        	} else { //handle failed from provider
        		//update transaction status to failed
        		updateSql =
                    "UPDATE " +
                        "transactions " +
                    "SET " +
                        "time_settled = sysdate, " +
                        "status_id = ? , " +
      	 				"comments = ? || '|' || ? || '|' || ? || '|' || ? || '|SplittedTrx to:' || ? , " +
      	 				"description = ? , ";
   				updateSql +=  "utc_offset_settled = ?, " + "clearing_provider_id = ? " + "WHERE " + "id = ? ";

       				 ps = conn.prepareStatement(updateSql);
					 ps.setInt(1, TransactionsManagerBase.TRANS_STATUS_FAILED);
					 ps.setString(2, info.getResult());
					 ps.setString(3, info.getMessage());
					 ps.setString(4, info.getUserMessage());
					 ps.setString(5, info.getProviderTransactionId());
					 ps.setString(6, internalTransactions);
					 ps.setString(7, ConstantsBase.ERROR_FAILED_CAPTURE);
					 ps.setString(8, utcOffset);
					 ps.setLong(9, info.getProviderId());
					 ps.setLong(10, info.getTransactionId());

        	}
        	ps.executeUpdate();
        	conn.commit();
        } catch (Exception e) {
			log.log(Level.ERROR, "ERROR! Can't update transaction!. ", e);
			try {
				conn.rollback();
			} catch (Exception er) {
				log.log(Level.ERROR, "Can't rollBack! ", er);
			}
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				log.error("Can't set back to autocommit.", e);
			}
			try {
        		conn.close();
        	} catch (Exception e) {
        		log.log(Level.ERROR, "Can't close: conn2", e);
        	}
		}
	}

    
    /**
	 * Add splitted trx to the report and to summary
	 * @param info ClearingInfo instance of the original trx
	 * @param currentCurrencyCode currency code
	 */
	public static void addSplittedTrxToReport(ClearingInfo info, String currentCurrencyCode, String writer) {
        if (info.isSuccessful()) {
            report.append("<tr>");
        } else {
            report.append("<tr style=\" color: #FF0000; font-weight: bold;\">");
        }

        String providerName = ClearingManager.getProviderName(info.getProviderId());

        report.append("<td>").append(info.getUserId()).append("</td><td>");
        String splittedStr = "(splittedTrx)";
        report.append(info.getTransactionId()).append(splittedStr).append("</td><td>");
        report.append(providerName).append("</td><td>");
        report.append(info.getResult()).append("</td><td>");
        report.append(info.getMessage()).append("</td><td>");
        report.append(ClearingUtil.formatAmount(info.getAmount())).append("</td><td>");
        report.append(currentCurrencyCode).append("</td><td>");
        report.append("Credit").append("</td></tr>\n");

        //summery.addTransaction(writer, providerName, info.getAmount(), currentCurrencyCode, info.isSuccessful());
	}

	public static DeltaPayInfo creatDeltaPayInternalInfo(ClearingInfo info, Transaction trx, Transaction depoTrx) {
		DeltaPayInfo innerInfo = new DeltaPayInfo();
		innerInfo.setTransactionId(trx.getId());
		innerInfo.setRedirect(getPropertyByFile("redirect"));
		innerInfo.setAffiliate(getPropertyByFile("affiliate"));
		innerInfo.setTransactionId(trx.getId());
		innerInfo.setCcn(info.getCcn());
		innerInfo.setExpireMonth(info.getExpireMonth());
		innerInfo.setExpireYear(info.getExpireYear());
		innerInfo.setCvv(info.getCvv());
		innerInfo.setCcTypeId(info.getCcTypeId());
		innerInfo.setOwner(info.getOwner());
        innerInfo.setCurrencySymbol(info.getCurrencySymbol());
        innerInfo.setCurrencyId(info.getCurrencyId());
        innerInfo.setAmount(trx.getAmount());
        innerInfo.setUserId(trx.getUserId());
        innerInfo.setSkinId(info.getSkinId());
        innerInfo.setCountryId(info.getCountryId());
        innerInfo.setCountryA2(info.getCountryA2());
        innerInfo.setIp(trx.getIp());
        innerInfo.setTransactionType(trx.getTypeId());
        innerInfo.setCardUserId(info.getCardUserId());

        innerInfo.setOriginalDepositId(depoTrx.getId());
        innerInfo.setProviderDepositId(depoTrx.getXorIdCapture());
        innerInfo.setAuthNumber(depoTrx.getAuthNumber());
        innerInfo.setProviderId(depoTrx.getClearingProviderId());

        return innerInfo;
	}
	
	public static CDPayInfo createCDPayInternalInfo(ClearingInfo info, Transaction trx, Transaction depoTrx) {

		CDPayInfo innerInfo = new CDPayInfo();
		innerInfo.setTransactionId(trx.getId());
		innerInfo.setCcn(info.getCcn());
		innerInfo.setExpireMonth(info.getExpireMonth());
		innerInfo.setExpireYear(info.getExpireYear());
		innerInfo.setCvv(info.getCvv());
		innerInfo.setCcTypeId(info.getCcTypeId());
		innerInfo.setOwner(info.getOwner());
        innerInfo.setCurrencySymbol(info.getCurrencySymbol());
        innerInfo.setCurrencyId(info.getCurrencyId());
        innerInfo.setAmount(trx.getAmount());
        innerInfo.setUserId(trx.getUserId());
        innerInfo.setSkinId(info.getSkinId());
        innerInfo.setCountryId(info.getCountryId());
        innerInfo.setCountryA2(info.getCountryA2());
        innerInfo.setIp(trx.getIp());
        innerInfo.setTransactionType(trx.getTypeId());
        innerInfo.setCardUserId(info.getCardUserId());
        innerInfo.setOrderId(new Long(depoTrx.getXorIdCapture()));
        innerInfo.setOriginalDepositId(depoTrx.getId());
        innerInfo.setProviderDepositId(depoTrx.getXorIdCapture());
        innerInfo.setAuthNumber(depoTrx.getAuthNumber());
        innerInfo.setProviderId(depoTrx.getClearingProviderId());

        return innerInfo;
	}

    
    static String getSql() {
    	String sql =
            	"SELECT " +
                    "t.user_id userid, " +
                    "t.id tid, " +
                    "curr.code, "+
                    "t.amount amount, " +
                    "t.auth_number auth_number, " +
                    "w.user_name writer, " +
                    "u.user_name user_name, " +
                    "t.fee_cancel, " +
                    "s.default_xor_id, " +
                    "u.skin_id, " +
                    "u.utc_offset, " +
                    "u.currency_id, " +
                    "l.cc_fee, " +
                    "cntr.id AS country_id, " +
                    "cntr.a2 , " +
                    "t.ip, " +
                    "t.is_credit_withdrawal, " +
                    "t.deposit_reference_id, " +
                    "t.clearing_provider_id, " +
                    "t.type_id transaction_type_id, " +
                    "t.internals_amount " +
            	"FROM " +
                    "transactions t, " +
                    "writers w, " +
                    "users u, " +
                    "skins s, " +
                    "currencies curr, " +
                    "limits l, " +
                    "countries cntr " +
            	"WHERE " +
                    "t.user_id = u.id AND " +
                    "t.writer_id = w.id AND " +
                    "s.id = u.skin_id AND " +
                    "curr.id = u.currency_id AND " +
                    "u.limit_id = l.id AND " +
                    "u.country_id = cntr.id AND " +
                    "u.class_id <> 0 AND " +
                    "t.status_id = ? AND " +
                    "t.is_accounting_approved = ? AND " +
                    "t.type_id = ? AND " +
               		"t.time_created < sysdate " +
    			"ORDER BY " +
                	"curr.id, userid ,tid";
                return sql;
    }
}
