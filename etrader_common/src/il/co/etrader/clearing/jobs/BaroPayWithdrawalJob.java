//package il.co.etrader.clearing.jobs;
//
//import il.co.etrader.bl_managers.ClearingManager;
//import il.co.etrader.bl_managers.TransactionsManagerBase;
//import il.co.etrader.bl_managers.UsersManagerBase;
//import il.co.etrader.bl_vos.Template;
//import il.co.etrader.bl_vos.UserBase;
//import il.co.etrader.bl_vos.Writer;
//import il.co.etrader.clearing.BaroPayInfo;
//import il.co.etrader.dao_managers.SkinsDAOBase;
//import il.co.etrader.dao_managers.TemplatesDAO;
//import il.co.etrader.dao_managers.TransactionsDAOBase;
//import il.co.etrader.dao_managers.UsersDAOBase;
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;
//import il.co.etrader.util.JobUtil;
//import il.co.etrader.util.SendTemplateEmail;
//
//import java.math.BigDecimal;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Hashtable;
//
//import org.apache.log4j.Level;
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Transaction;
//import com.anyoption.common.beans.base.BaroPayResponse;
//import com.anyoption.common.beans.base.Skin;
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.daos.BaroPayDAOBase;
//import com.anyoption.common.daos.LanguagesDAOBase;
//import com.anyoption.common.util.ClearingUtil;
//
///**
// * BaroPay "capture" withdrawal.
// * Go over the pending BaroPay transactions and "capture" them via BaroPay API
// * @author EranL
// *
// */
//public class BaroPayWithdrawalJob extends JobUtil {
//    private static Logger log = Logger.getLogger(BaroPayWithdrawalJob.class);
//
//    public static int TRANSACTION_HAS_FEE = 0;
//    public static int NUMBER_OF_HOURS_IN_A_DAY = 24;
//    public static StringBuffer report = new StringBuffer();
//    public static ReportSummary summery = new ReportSummary();
//    public static String lastCurrencyCode = null;
//
//	/**
//	 * Goes over the approved (by accounting) transactions in the db from the previosday and make a "capture" request
//	 * for each of them
//	 */
//	private static void captureWithdrawalTransactions() {
//		report = new StringBuffer("<br><b>Capture BaroPay withdraw transactions:</b><table border=\"1\">");
//		report.append("<tr><td>User id</td><td>Transaction id</td><td>Status</td>" +
//					  "<td>Description</td><td>Amount</td><td>Currency</td></tr>");
//		Connection conn1 = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		String utcOffset = ConstantsBase.EMPTY_STRING;
//
//		String transactionId = getPropertyByFile("transaction.id", ConstantsBase.EMPTY_STRING);
//
//		try {
//			conn1 = getConnection();
//            ClearingManager.loadClearingProviders(conn1);
//			String sql =
//		            	" SELECT " +
//		                "	t.user_id userid, " +
//		                "	t.id tid, " +
//		                "	curr.code, "+
//		                "	t.amount amount, " +
//		                "	w.user_name writer, " +
//		                "	u.user_name user_name, " +
//		                "	t.fee_cancel, " +
//		                "	u.skin_id, " +
//		                "	u.utc_offset, " +
//		                "	u.currency_id, " +
//		                "	u.city_name, " +
//		                "	l.bank_wire_fee, " +
//		                "	t.rate, " +
//		                "   br.* " +
//		            	" FROM " +
//		                "	transactions t, " +
//		                "	writers w, " +
//		                "	users u, " +
//		                "	skins s, " +
//		                "	currencies curr, " +
//		                "	limits l, " +
//		                "   baropay_request br " +
//		            	" WHERE " +
//		                "	t.user_id = u.id AND " +
//		                "	t.writer_id = w.id AND " +
//		                "	s.id = u.skin_id AND " +
//		                "	curr.id = u.currency_id AND " +
//		                "	u.limit_id = l.id AND " +
//		                "   t.id = br.transaction_id AND " +
//		                "	u.class_id <> 0 AND " +
//		                "	t.status_id = ? AND " +                    
//		                "	t.is_accounting_approved = ? AND " +
//		                "	t.type_id = ? ";
//
//                	// Add an option to run job with specific transcation id
//                    if (!CommonUtil.isParameterEmptyOrNull(transactionId)) {                        
//                           log.info("Going to capture trxId:" + transactionId);
//                           sql += 	" AND t.id = " + transactionId;
//                    }
//                sql +=
//                	"	ORDER BY " +
//                    "		curr.id, userid ,tid";
//
//			pstmt = conn1.prepareStatement(sql);
//
//			pstmt.setLong(1, TransactionsManagerBase.TRANS_STATUS_APPROVED);
//			pstmt.setLong(2, ConstantsBase.ACCOUNTING_APPROVED_YES);
//			pstmt.setLong(3, TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW);
//
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//                BaroPayInfo info = new BaroPayInfo();
//                String currentCurrencyCode = rs.getString("code");
//                info.setCurrencySymbol(currentCurrencyCode);
//                info.setCurrencyId(rs.getInt("currency_id"));
//                info.setUserId(rs.getLong("userid"));
//                info.setUserName(rs.getString("user_name"));
//                info.setSkinId(rs.getLong("skin_id"));
//                info.setTransactionType(TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW);
//                info.setTransactionId(rs.getInt("tid"));
//                info.setAmount(rs.getLong("amount"));
//                info.getBaroPayRequest().setId(rs.getInt("id"));
//                info.getBaroPayRequest().setTransactionId(rs.getInt("transaction_id"));
//                info.getBaroPayRequest().setBankName(rs.getString("bank_name"));
//                info.getBaroPayRequest().setAccountOwner(rs.getString("account_owner"));
//                info.getBaroPayRequest().setAccountNumber(rs.getString("account_number"));
//                info.setStatusWithdrawURL(getPropertyByFile("status.withdraw.url"));
//                utcOffset = rs.getString("utc_offset"); //save user utc offset
//
//                //set fee indicaor and amount in case transaction has fee
//                boolean hasFee = rs.getInt("fee_cancel") == TRANSACTION_HAS_FEE;
//                long feeAmount = rs.getLong("bank_wire_fee");
//                long amount = rs.getLong("amount");
//                long amountAfterFee = amount - feeAmount;
//                if (hasFee) {
//                	info.setAmount(amountAfterFee);
//                }
//
//                // set PP Email. Note: there is an option to request N withdrawals per currency
////                MassPayItem[] emails = new MassPayItem[1];
////            	emails[0] = info.new MassPayItem();
////            	emails[0].setAmount(info.getAmount());
////            	emails[0].setEmail(rs.getString("paypal_email"));
////            	emails[0].setTransactionId(rs.getInt("tid"));
////
////            	// Handle TRY currency convertion to USD (not supported by PayPal)
////   			 	if (info.getCurrencyId() == ConstantsBase.CURRENCY_TRY_ID) {
////   			 		info.setCurrencySymbol("USD");
////   			 		emails[0].setAmount(Math.round(emails[0].getAmount() * rs.getDouble("rate")));
////   			 	}
////
////                info.setEmails(emails);
//
//                log.info("Going to capture BaroPay transaction: " + info.toString());
//                // call Mass pay method
//                try {
//                	ClearingManager.setBaroPayBookBack(info);
//                	info.setSuccessful(true);
//                } catch (ClearingException ce) {
//                    log.log(Level.ERROR, "Failed to capture BaroPay transaction: " + info.toString(), ce);
//                    info.setResult("999");
//                    info.setMessage(ce.getMessage());
//                    info.setSuccessful(false);
//                }
//                BaroPayResponse baroPayResponse = new BaroPayResponse();
//		        long providerTransactionId = 0;
//		        if (null != info.getProviderTransactionId()) {
//		        	providerTransactionId = Long.valueOf(info.getProviderTransactionId());
//		        }
//		        baroPayResponse.setPaymentId(providerTransactionId);
//		        int status = 1;
//		        if (null != info.getResult()) {
//		        	status = Integer.valueOf(info.getResult());
//		        }
//		        baroPayResponse.setStatus(status);
//		        baroPayResponse.setDescription(info.getMessage());			      
//		        baroPayResponse.setTransactionId(rs.getInt("tid"));
//		        baroPayResponse.setXmlResponse(info.getXmlResponse());			        
//		        BaroPayDAOBase.insertBaroPayResponse(conn1, baroPayResponse);
//
//                // currency code display
//                if ( null == lastCurrencyCode || !lastCurrencyCode.equals(currentCurrencyCode) ) {
//                	lastCurrencyCode = currentCurrencyCode;
//                    report.append("<tr><td colspan=\"7\" align=\"center\" valign=\"middle\"><b>").append(lastCurrencyCode).append("</b></td></tr>");
//                }
//
//                if (info.isSuccessful()) {
//                    report.append("<tr>");
//                } else {
//               		report.append("<tr style=\" color: #FF0000; font-weight: bold;\">");
//                }
//
//                report.append("<td>").append(info.getUserId()).append("</td><td>");
//                report.append(info.getTransactionId()).append("</td><td>");
//                report.append(info.getResult()).append("</td><td>");
//                report.append(info.isSuccessful() ? info.getMessage() : info.getUserMessage()).append("</td><td>");
//                report.append(ClearingUtil.formatAmount(info.getAmount())).append("</td><td>");
//                report.append(currentCurrencyCode).append("</td></tr>\n");
//
//                updateTransaction(info, hasFee, feeAmount, utcOffset, info.isSuccessful());
//
//                summery.addTransaction(rs.getString("writer"), "BaroPay", info.getAmount(), currentCurrencyCode, info.isSuccessful());
//
//               	// Send email to user
//                if(info.isSuccessful()) {
//                   	String ls = System.getProperty("line.separator");
//    				log.info("Going to send BaroPay withdrawal email: " + ls +
//    								"userId: " + info.getUserId() + ls +
//    								"trxId: " + info.getTransactionId() + ls);
//    		        try {
//    		        	   Hashtable<String, String>emailProperties = new Hashtable<String, String>();
//    				       long langId = SkinsDAOBase.getById(conn1, (int)info.getSkinId()).getDefaultLanguageId();
//    				       String langCode = LanguagesDAOBase.getCodeById(conn1, langId);
//    				       log.debug("skinId: " + info.getSkinId() + " , langId: " + langId + ", langCode: " + langCode);
//    				       mailTemplate = getEmailTemplate(ConstantsBase.TEMPLATE_WITHDRAW_BAROPAY_SUCCEED,
//    				    		   langCode, String.valueOf(info.getSkinId()));
//
//    				       emailProperties = new Hashtable<String, String>();
//    				       HashMap<String,String> params = new HashMap<String, String>();
//
//    				       UserBase uBase = new UserBase();
//    				       uBase.setUserName(info.getUserName());
//    				       UsersDAOBase.getByUserName(conn1,uBase.getUserName(),uBase, true);
//    				      
//
//   				    	   emailProperties.put("subject", properties.getProperty("withdrawal.user.email.kr"));
//
//    				       String currencySym = properties.getProperty(CommonUtil.getCurrencySymbol(info.getCurrencyId()));
//    				       String amountTxt = currencySym + ClearingUtil.formatAmount(info.getAmount());
//    				       params.put(SendTemplateEmail.PARAM_USER_ID,String.valueOf(uBase.getId()));
//    		   		       params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME,uBase.getFirstName());
//    		   		       params.put(SendTemplateEmail.PARAM_USER_LAST_NAME, uBase.getLastName());
//    		   		       params.put(SendTemplateEmail.PARAM_USER_ADDRESS_NUMBER, uBase.getStreetNo());
//    		   		       params.put(SendTemplateEmail.PARAM_USER_ADDRESS_STREET, uBase.getStreet());
//    		   		       params.put(SendTemplateEmail.PARAM_USER_ADDRESS_CITY, rs.getString("city_name"));
//    		   		       params.put(SendTemplateEmail.PARAM_USER_ADDRESS_ZIP_CODE, uBase.getZipCode());
//    		   		       params.put(SendTemplateEmail.PARAM_TRANSACTION_AMOUNT, amountTxt);
//    		   		       params.put(SendTemplateEmail.PARAM_TRANSACTION_BANK_NAME, info.getBaroPayRequest().getBankName());
//    		   		       params.put(SendTemplateEmail.PARAM_USER_FULL_NAME, uBase.getFirstName() + " " + uBase.getLastName());
//    		   		       params.put(SendTemplateEmail.PARAM_TRANSFER_AMOUNT, amountTxt);
//    		   		       
////    		   		       params.put(SendTemplateEmail.PARAM_PAYPAL_EMAIL, info.getEmails()[0].getEmail());
//    		   		       Date date = new Date();
//    		   		       params.put(SendTemplateEmail.PARAM_DATE_DMMYYYY, new SimpleDateFormat("dd.MM.yy").format(date));
//
//    		   		       if (null != uBase.getGender()) {
//	    		   				String genderTxt = uBase.getGenderForTemplateEnHeUse();
//    		   					genderTxt += ".en";
//	    		   				params.put(SendTemplateEmail.PARAM_GENDER_DESC_EN_OR_HE, properties.getProperty(genderTxt));
//    		   		       }
//
//    					   try {
//    						   emailProperties.put("body",getEmailBody(params));
//    					   } catch (Exception e) {
//    						   log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
//    						   System.exit(1);
//    					   }
//
//    					   // send email for tracking
//						   String[] splitTo = properties.getProperty("succeed.email.to").split(";");
//						   for (int i = 0; i < splitTo.length; i++) {
//							    emailProperties.put("to", splitTo[i]);
//								sendSingleEmail(properties, emailProperties, langCode);
//						   }
//
//						   // send email to user
//    					   emailProperties.put("to", uBase.getEmail());
//    					   sendSingleEmail(properties,emailProperties, langCode);
//    					   
//    					   // add to user MailBox
//						   if (uBase.getSkinId() != Skin.SKIN_ETRADER) {
//							   Template t = TemplatesDAO.get(conn1, Template.BAROPAY_WITHDRAWAL_SUCCEED_ID);
//							   UsersManagerBase.SendToMailBox(uBase, t, Writer.WRITER_ID_AUTO, langId, info.getTransactionId(), conn1, 0);
//						   }
//
//    		        } catch (Exception e) {
//    		        	log.error("Error, Can't send baroPay withdrawal email! " + e);
//    				}
//                } else {
//                	log.info("trx failed, baroPay withdrawal email not sent!");
//                }
//             }
//        } catch (Exception e) {
//            log.log(Level.ERROR, "Error while capturing baroPay transactions.", e);
//        } finally {
//            try {
//                rs.close();
//            } catch (Exception e) {
//                log.log(Level.ERROR, "Can't close", e);
//            }
//            try {
//                pstmt.close();
//            } catch (Exception e) {
//                log.log(Level.ERROR, "Can't close", e);
//            }
//            try {
//                conn1.close();
//            } catch (Exception e) {
//                log.log(Level.ERROR, "Can't close", e);
//            }
//        }
//        report.append("</table>");
//    }
//
//	/**
//	 * This method create a fee transaction and sets it according to the clearing info
//	 * @param info - Clearing info instance
//	 * @return New fee transaction
//	 */
//	private static Transaction getFeeTransaction(ClearingInfo info) {
//		Transaction t = new Transaction();
//    	t.setUserId(info.getUserId());
//    	t.setTypeId(TransactionsManagerBase.TRANS_TYPE_HOMO_FEE);
//    	t.setTimeCreated(new Date());
//    	t.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
//    	t.setWriterId(Writer.WRITER_ID_AUTO);
//    	t.setIp("IP NOT FOUND!");
//    	t.setTimeSettled(new Date());
//    	t.setProcessedWriterId(Writer.WRITER_ID_AUTO);
//    	t.setReferenceId(new BigDecimal(info.getTransactionId()));
//    	return t;
//	}
//
//   /**
//	 * Update transaction after send it via baroPay
//	 * @param info ClearingInfo instance of the current trx that captured
//	 * @param hasFee true in case we need to create fee transaction
//	 * @param feeAmount fee amount to take
//	 * @param utcOffset user utcOffset
//	 * @throws SQLException
//	 */
//	public static void updateTransaction(ClearingInfo info, boolean hasFee, long feeAmount, String utcOffset, boolean isSuccessful) throws SQLException {
//
//		Connection conn = null;
//		PreparedStatement ps = null;
//        try {
//        	conn = getConnection();
//            String updateSql;
//        	if (isSuccessful) { //handle success from provider
//              	//update transaction status to success and amount to the amount after fee (incase there was fee)
//        		updateSql =
//                    "UPDATE " +
//                        "transactions " +
//                    "SET " +
//                        "time_settled = sysdate, " +
//                        "status_id = ?, " +
//        				"comments = ? || '|' || ? || '|' || ? , " +
//        				"amount =  ?, " +
//               			"utc_offset_settled = ?, " +
//               			"xor_id_capture = ? " +
//               		"WHERE " +
//               			"id = ? ";
//
//				 ps = conn.prepareStatement(updateSql);
//				 ps.setInt(1, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
//				 ps.setString(2, info.getResult());
//				 ps.setString(3, info.getMessage());
//				 ps.setString(4, info.getUserMessage());
//				 ps.setLong(5, info.getAmount());
//				 ps.setString(6, utcOffset);
//				 ps.setString(7, info.getProviderTransactionId());
//				 ps.setLong(8, info.getTransactionId());
//
//				 conn.setAutoCommit(false); //in order to support rollback once and update or an insert was not succesfull
//
//               	 //insert new fee transaction
//               	 if (hasFee) {
//                	Transaction feeTransaction = getFeeTransaction(info); //creating the fee transaction accoring to the orig transaction
//                	feeTransaction.setAmount(feeAmount);
//                	feeTransaction.setUtcOffsetCreated(utcOffset);
//                	feeTransaction.setUtcOffsetSettled(utcOffset);
//    				TransactionsDAOBase.insert(conn, feeTransaction);
//    				if (log.isInfoEnabled()) {
//    					String ls = System.getProperty("line.separator");
//    					log.info("fee transaction inserted: " + ls + feeTransaction.toString());
//    				}
//
//    				//update Transaction reference
//    				TransactionsDAOBase.updateRefId(conn, info.getTransactionId(), feeTransaction.getId());
//               	}
//        	} else { //handle failed from provider
//        		//update transaction status to failed
//        		updateSql =
//                    "UPDATE " +
//                        "transactions " +
//                    "SET " +
//                        "time_settled = sysdate, " +
//                        "status_id = ? , " +
//      	 				"comments = ? || '|' || ? || '|' || ? , " +
//      	 				"description = ? , " +
//        			    "utc_offset_settled = ? " +
//    			    "WHERE " +
//    			  		"id = ? ";
//
//       				 ps = conn.prepareStatement(updateSql);
//					 ps.setInt(1, TransactionsManagerBase.TRANS_STATUS_FAILED);
//					 ps.setString(2, info.getResult());
//					 ps.setString(3, info.getMessage());
//					 ps.setString(4, info.getUserMessage());
//					 ps.setString(5, ConstantsBase.ERROR_FAILED_CAPTURE);
//					 ps.setString(6, utcOffset);
//					 ps.setLong(7, info.getTransactionId());
//        	}
//        	ps.executeUpdate();
//        	conn.commit();
//        } catch (Exception e) {
//			log.log(Level.ERROR, "ERROR! Can't update transaction!. ", e);
//			try {
//				conn.rollback();
//			} catch (Exception er) {
//				log.log(Level.ERROR, "Can't rollBack! ", er);
//			}
//		} finally {
//			try {
//				conn.setAutoCommit(true);
//			} catch (SQLException e) {
//				log.error("Can't set back to autocommit.", e);
//			}
//			try {
//        		conn.close();
//        	} catch (Exception e) {
//        		log.log(Level.ERROR, "Can't close: conn2", e);
//        	}
//		}
//	}
//
//
//    /**
//     * Capture BaroPay Withdrawal Job.
//     *
//     * @param args
//     */
//    public static void main(String[] args) {
//        if (null == args || args.length < 1) {
//            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
//            return;
//        }
//        propFile = args[0];
//
//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.INFO, "Starting the job.");
//        }
//
//        captureWithdrawalTransactions();
//
//        String reportBody = "<html><body>" +
//            summery.getSummery() +
//            report.toString() +
//            "</body></html>";
//
//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.INFO, "Sending report email: " + reportBody);
//        }
//
//        // send email
//        Hashtable<String, String> server = new Hashtable<String, String>();
//        server.put("url", getPropertyByFile("email.url"));
//        server.put("auth", getPropertyByFile("email.auth"));
//        server.put("user", getPropertyByFile("email.user"));
//        server.put("pass", getPropertyByFile("email.pass"));
//        server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));
//
//        Hashtable<String, String> email = new Hashtable<String, String>();
//        email.put("subject", getPropertyByFile("email.subject"));
//        email.put("to", getPropertyByFile("email.to"));
//        email.put("from", getPropertyByFile("email.from"));
//        email.put("body", reportBody);
//        sendEmail(server, email);
//
//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.INFO, "Job completed.");
//        }
//    }
//}