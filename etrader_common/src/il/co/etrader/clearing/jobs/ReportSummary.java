package il.co.etrader.clearing.jobs;

import java.util.Enumeration;
import java.util.Hashtable;

import com.anyoption.common.util.ClearingUtil;

/**
 * Helper class that wrap the report summery.
 *
 * @author Tony
 */
public class ReportSummary {
    private Hashtable<String, SenderSummary> senders;

    /**
     * Create report summery wrapper.
     */
    public ReportSummary() {
        senders = new Hashtable<String, SenderSummary>();
    }

    /**
     * Add transaction to the report summery.
     *
     * @param sender the sender of the transaction
     * @param provider the provider processed the transaction
     * @param amount the transaction amount
     * @param currency the transaction currency
     * @param successful if the transaction was successful
     */
    public void addTransaction(String sender, String provider, long amount, String currency, boolean successful) {
        getSender(sender).addTransaction(provider, amount, currency, successful);
    }

    /**
     * Gets the wrapper for specified sender. Create it and add it
     * to the senders if needed.
     *
     * @param sender the sender name we need wrapper for
     * @return <code>SenderSummary</code> for the specified sender.
     */
    SenderSummary getSender(String sender) {
        SenderSummary ss = senders.get(sender);
        if (null == ss) {
            ss = new SenderSummary(sender);
            senders.put(sender, ss);
        }
        return ss;
    }

    /**
     * Create the summery part of the report.
     *
     * @return HTML representing the summery part of the report.
     */
    public String getSummery() {
        StringBuffer sb = new StringBuffer("<b>Capture Summary:</b><br><table border=\"1\">");
        sb.append("<tr><td>Sender</td><td>Provider</td><td>#Transactions</td><td>#Errors</td><td>Totals</td></tr>");
        SenderSummary ss = null;
        for (Enumeration<String> e = senders.keys(); e.hasMoreElements();) {
            String key = e.nextElement();
            ss = senders.get(key);
            ss.getSummery(sb);
        }
        sb.append("</table>");
        return sb.toString();
    }

    /**
     * Wrapper of the summery info for single sender.
     *
     * @author Tony
     */
    class SenderSummary {
        private String senderName;
        private Hashtable<String, ProviderSummary> providers;

        /**
         * Create new sender summery.
         *
         * @param sender the name of the sender
         */
        SenderSummary(String sender) {
            senderName = sender;
            providers = new Hashtable<String, ProviderSummary>();
        }

        /**
         * Add transaction to the sender summery.
         *
         * @param provider the provider processed the transaction
         * @param amount the transaction amount
         * @param currency the transaction currency
         * @param successful if the transaction was successful
         */
        void addTransaction(String provider, long amount, String currency, boolean successful) {
            getProvider(provider).addTransaction(amount, currency, successful);
        }

        /**
         * Gets the wrapper for specified provider. Create it and add it
         * to the providers if needed.
         *
         * @param provider the provider name we need wrapper for
         * @return <code>ProviderSummary</code> for the specified provider.
         */
        ProviderSummary getProvider(String provider) {
            ProviderSummary ps = providers.get(provider);
            if (null == ps) {
                ps = new ProviderSummary(provider);
                providers.put(provider, ps);
            }
            return ps;
        }

        /**
         * Append to the summery <code>StringBuffer</code> the info about the
         * represented sender.
         *
         * @param sb the buffer in which to append the summery
         */
        void getSummery(StringBuffer sb) {
            ProviderSummary ps = null;
            for (Enumeration<String> e = providers.keys(); e.hasMoreElements();) {
                String key = e.nextElement();
                ps = providers.get(key);
                ps.getSummery(sb);
            }
        }

        /**
         * Wrapper of the summery info for single provider.
         *
         * @author Tony
         */
        class ProviderSummary {
            private String providerName;
            private int transactionsCount;
            private int failCount;
            private Hashtable<String, CurrencySummary> currencies;

            /**
             * Create new provider summery.
             *
             * @param provider the name of the provider
             */
            ProviderSummary(String provider) {
                providerName = provider;
                transactionsCount = 0;
                failCount = 0;
                currencies = new Hashtable<String, CurrencySummary>();
            }

            /**
             * Add transaction to the sender summery.
             *
             * @param provider the provider processed the transaction
             * @param amount the amount of the transaction
             * @param currency the currency of the transaction
             * @param successful if the transaction was successful
             */
            void addTransaction(long amount, String currency, boolean successful) {
                getCurrency(currency).addTransaction(amount);
                transactionsCount++;
                if (!successful) {
                    failCount++;
                }
            }

            /**
             * Gets the wrapper for specified currency. Create it and add it
             * to the currencies if needed.
             *
             * @param currency the currency we need wrapper for
             * @return <code>CurrencySummary</code> for the specified currency.
             */
            CurrencySummary getCurrency(String currency) {
                CurrencySummary cs = currencies.get(currency);
                if (null == cs) {
                    cs = new CurrencySummary();
                    currencies.put(currency, cs);
                }
                return cs;
            }

            /**
             * Append to the summery <code>StringBuffer</code> the info about the
             * represented provider.
             *
             * @param sb the buffer in which to append the summery
             */
            void getSummery(StringBuffer sb) {
                sb.append("<tr><td>").append(senderName);
                sb.append("</td><td>").append(providerName);
                sb.append("</td><td>").append(transactionsCount);
                sb.append("</td><td>").append(failCount);
                sb.append("</td><td>");
                boolean first = true;
                CurrencySummary cs = null;
                for (Enumeration<String> e = currencies.keys(); e.hasMoreElements();) {
                    String key = e.nextElement();
                    cs = currencies.get(key);
                    if (first) {
                        first = false;
                    } else {
                        sb.append(" | ");
                    }
                    sb.append(ClearingUtil.formatAmount(cs.getAmount())).append(" ").append(key);
                }
                sb.append("</td></tr>");
            }

            /**
             * Object wrapping <code>long</code> to be used to sum the amounts in
             * one currency.
             *
             * @author Tony
             */
            class CurrencySummary {
                long amount;

                /**
                 * Create new currency amount wrapper.
                 */
                CurrencySummary() {
                    amount = 0;
                }

                /**
                 * Add amount to the currency.
                 *
                 * @param amount the amount to add
                 */
                void addTransaction(long amount) {
                    this.amount += amount;
                }

                /**
                 * @return The currenct amount for the currency.
                 */
                long getAmount() {
                    return amount;
                }
            }
        }
    }
}