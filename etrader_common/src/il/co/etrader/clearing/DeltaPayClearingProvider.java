///**
// *
// */
//package il.co.etrader.clearing;
//
//
//import java.io.IOException;
//import java.net.URLEncoder;
//import java.util.Hashtable;
//
//import javax.xml.parsers.ParserConfigurationException;
//
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.xml.sax.SAXException;
//
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.clearing.ClearingProvider;
//import com.anyoption.common.util.ClearingUtil;
//
///**
// * @author AviadH
// *
// */
//public class DeltaPayClearingProvider extends ClearingProvider {
//	private static Logger log = Logger.getLogger(DeltaPayClearingProvider.class);
//
//	@Override
//	public void authorize(ClearingInfo info) throws ClearingException {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void bookback(ClearingInfo info) throws ClearingException {
//		 if (log.isTraceEnabled()) {
//	            log.trace(info.toString());
//	        }
//	        try {
//	        	url = "https://backoffice.vixipay.com/api/transact.php";
//	        	DeltaPayInfo deltaInfo = (DeltaPayInfo) info;
//	        	log.info("Enter to deltaPay BookBack for transaction:" + deltaInfo);
//	            String body =
//	                "affiliate=" + URLEncoder.encode(deltaInfo.getAffiliate(),"UTF-8") +
//	                "&paymethod=" + URLEncoder.encode("Credit Card","UTF-8") +
//	                "&processing_mode=" + URLEncoder.encode("refund","UTF-8") +
//	                "&redirect=" + URLEncoder.encode(deltaInfo.getRedirect(),"UTF-8") +
//	                "&customer_id=" + URLEncoder.encode("NA","UTF-8") +
//	                "&order_id=" + URLEncoder.encode(Long.toString(deltaInfo.getOriginalDepositId()),"UTF-8") +
//	                "&amount=" + URLEncoder.encode(deltaInfo.getFullAmountStr(),"UTF-8") +
//	                "&currency=" + URLEncoder.encode(deltaInfo.getCurrencySymbol(),"UTF-8") +
//	                "&reference_transaction_no=" + URLEncoder.encode(deltaInfo.getAuthNumber(),"UTF-8") +
//	                "&terminal_name=" + URLEncoder.encode("ANYOALOGCNY-1","UTF-8");
//
//	            String response = ClearingUtil.executePOSTRequest(url, body);
//	            parseResponse(response, deltaInfo);
//	        } catch (Throwable t) {
//	            throw new ClearingException("Transaction failed.", t);
//	        }
//	}
//
//	@Override
//	public void capture(ClearingInfo info) throws ClearingException {
//		// TODO Auto-generated method stub
//	}
//
//	@Override
//	public void enroll(ClearingInfo info) throws ClearingException {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public boolean isSupport3DEnrollement() {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	@Override
//	public void purchase(ClearingInfo info) throws ClearingException {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void withdraw(ClearingInfo info) throws ClearingException {
//		//	TODO Auto-generated method stub
//	}
//
//	/**
//     * Parse DeltaPayResult
//     *
//     * @param result the result XML
//     * @return <code>Hashtable</code> with result values.
//     * @throws ParserConfigurationException
//     * @throws SAXException
//     * @throws IOException
//     */
//    private  void parseResponse(String response, DeltaPayInfo info) throws ParserConfigurationException, SAXException, IOException {
//    	// Patch for response with two <html> parts
//    	if(StringUtils.countMatches(response.toLowerCase(), "<html>")>1){
//    		int k = response.lastIndexOf("<html>");
//    		response = response.substring(k);
//    	}
//        Document respDoc = ClearingUtil.parseXMLToDocument(response);
//        Element root = (Element) ClearingUtil.getNode(respDoc.getDocumentElement(), "");
//        Hashtable<String, String> params = ClearingUtil.getListNode(root, "input", "name", "value");
//        info.setResult(params.get("status"));
//        info.setAuthNumber(params.get("transaction_no"));
//        info.setMessage(params.get("description"));
//        info.setSuccessful(params.get("status").equals("REFUND REQUEST"));
//
//        if (log.isDebugEnabled()) {
//            log.debug(info.toString());
//        }
//    }
//
//	@Override
//	public void cancel(ClearingInfo info) throws ClearingException {
//		// TODO Auto-generated method stub
//		log.info("NOT IMPLEMENTED !!!");
//	}
//}
