/**
 *
 */
package il.co.etrader.clearing;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.WireBase;
import il.co.etrader.util.ConstantsBase;

import java.math.BigDecimal;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.managers.LanguagesManagerBase;

/**
 * Envoy clearing info class
 * @author Eliran
 *
 */
public class EnvoyInfo extends ClearingInfo {

	private final String MERCHANT_REFERENCE = "AYP";
	private final String MERCHANT_ID_TEST = "000203AYP";
	private final String MERCHANT_ID_LIVE = "000266AYP";
	private final String ONE_CLICK_URL_TEST = "https://test.envoytransfers.com";
	private final String ONE_CLICK_URL_LIVE = "https://www.envoytransfers.com";
	private static final Logger log = Logger.getLogger(EnvoyInfo.class);
    // for Envoy

	private String redirectUrl;
    private String merchantId;
    private String customerRef;
    private String language;
    private String amountStr;
    private String requestReference;
    private String xmlString;
    private String service;
    private Transaction envoyTran;

    // withdraw info
	private WireBase account;
	private String accountNumber;
	private int refundDestination;
	private String refundCountryCode;

	// Notification info
	private String uniqueReference;
	private String epacsReference;
	private int notificationType;
	private String userOffset;

	// PayIn notification info
	private String postingDate;
	private String bankCurrency;
	private String bankAmount;
	private String appliedCurrency;
	private String appliedAmount;
	private String bankInformation;
	private int transferTypeId;
	private String refererType;

	public EnvoyInfo() {
    }

	public EnvoyInfo(UserBase user, Transaction t) throws SQLException{

    	super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId());
    	FacesContext context = FacesContext.getCurrentInstance();
    	ApplicationDataBase ap = (ApplicationDataBase)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);

    	if (ap.getIsLive()){
	        redirectUrl = ONE_CLICK_URL_LIVE;
	    	merchantId = MERCHANT_ID_LIVE;
	    	log.debug("Live parameters marchent id = " + merchantId);
		}else {
			redirectUrl = ONE_CLICK_URL_TEST;
	    	merchantId = MERCHANT_ID_TEST;
	    	log.debug("Test parameters marchent id = " + merchantId);
		}
        customerRef = MERCHANT_REFERENCE + currencySymbol.substring(0, 2) + getReferencedUserId();
        language = LanguagesManagerBase.getCodeById(user.getLanguageId());
        userOffset = user.getUtcOffset();
        service = "";
        envoyTran = t;
    }

	/**
	 * @return the appliedAmount
	 */
	public String getAppliedAmount() {
		return appliedAmount;
	}

	/**
	 * @param appliedAmount the appliedAmount to set
	 */
	public void setAppliedAmount(String appliedAmount) {
		this.appliedAmount = appliedAmount;
	}

	/**
	 * @return the appliedCurrency
	 */
	public String getAppliedCurrency() {
		return appliedCurrency;
	}

	/**
	 * @param appliedCurrency the appliedCurrency to set
	 */
	public void setAppliedCurrency(String appliedCurrency) {
		this.appliedCurrency = appliedCurrency;
	}

	/**
	 * @return the bankAmount
	 */
	public String getBankAmount() {
		return bankAmount;
	}

	/**
	 * @param bankAmount the bankAmount to set
	 */
	public void setBankAmount(String bankAmount) {
		this.bankAmount = bankAmount;
	}

	/**
	 * @return the bankCurrency
	 */
	public String getBankCurrency() {
		return bankCurrency;
	}

	/**
	 * @param bankCurrency the bankCurrency to set
	 */
	public void setBankCurrency(String bankCurrency) {
		this.bankCurrency = bankCurrency;
	}

	/**
	 * @return the bankInformation
	 */
	public String getBankInformation() {
		return bankInformation;
	}

	/**
	 * @param bankInformation the bankInformation to set
	 */
	public void setBankInformation(String bankInformation) {
		this.bankInformation = bankInformation;
	}

	/**
	 * @return the epacsReference
	 */
	public String getEpacsReference() {
		return epacsReference;
	}

	/**
	 * @param epacsReference the epacsReference to set
	 */
	public void setEpacsReference(String epacsReference) {
		this.epacsReference = epacsReference;
	}

	/**
	 * @return the postingDate
	 */
	public String getPostingDate() {
		return postingDate;
	}

	/**
	 * @param postingDate the postingDate to set
	 */
	public void setPostingDate(String postingDate) {
		this.postingDate = postingDate;
	}

	/**
	 * @return the uniqueReference
	 */
	public String getUniqueReference() {
		return uniqueReference;
	}

	/**
	 * @param uniqueReference the uniqueReference to set
	 */
	public void setUniqueReference(String uniqueReference) {
		this.uniqueReference = uniqueReference;
	}

	/**
	 * @return the redirectUrl
	 */
	public String getRedirectUrl() {
		return redirectUrl;
	}
	/**
	 * @param redirectUrl the redirectUrl to set
	 */
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	/**
	 * Implement toString function
	 */
	   public String toString() {
	        String ls = System.getProperty("line.separator");
	        String superToStr = super.toString();  // base toString
	        return ls + superToStr + ls + "EnvoyInfo:" + ls +


	                "email: " + email + ls +

	        		"redirectUrl: " + redirectUrl + ls;
	    }

	/**
	 * @return the customerRef
	 */
	public String getCustomerRef() {
		return customerRef;
	}

	/**
	 * @param customerRef the customerRef to set
	 */
	public void setCustomerRef(String customerRef) {
		this.customerRef = customerRef;
	}

	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getReferencedUserId(){
		String userIdString = String.valueOf(userId);

		while (userIdString.length() < ConstantsBase.USER_ID_REFERENCE_DIGITS_NUMBER){
			userIdString = "0" + userIdString;
		}

		return userIdString;
	}

	/**
	 * @return the account
	 */
	public WireBase getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(WireBase account) {
		this.account = account;
	}

	/**
	 * @return the withdarwAmount
	 */
	public String getAmountStr() {
		return String.valueOf(amount/100);
	}

	/**
	 * @return the withdarwAmount
	 */
	public String getFullAmountStr() {
		return String.valueOf(new BigDecimal(amount).divide(new BigDecimal(100)));
	}

	/**
	 * @param withdarwAmount the withdarwAmount to set
	 */
	public void setAmountStr(String withdarwAmount) {
		this.amountStr = withdarwAmount;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the requestReference
	 */
	public String getRequestReference() {
		return requestReference;
	}

	/**
	 * @param requestReference the requestReference to set
	 */
	public void setRequestReference(String requestReference) {
		this.requestReference = requestReference;
	}

	/**
	 * @return the xmlString
	 */
	public String getXmlString() {
		return xmlString;
	}

	/**
	 * @param xmlString the xmlString to set
	 */
	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}

	/**
	 * @return the notificationType
	 */
	public int getNotificationType() {
		return notificationType;
	}

	/**
	 * @param notificationType the notificationType to set
	 */
	public void setNotificationType(int notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * @return the userOffset
	 */
	public String getUserOffset() {
		return userOffset;
	}

	/**
	 * @param userOffset the userOffset to set
	 */
	public void setUserOffset(String userOffset) {
		this.userOffset = userOffset;
	}

	/**
	 * @return the refererType
	 */
	public String getRefererType() {
		return refererType;
	}

	/**
	 * @param refererType the refererType to set
	 */
	public void setRefererType(String refererType) {
		this.refererType = refererType;
	}

	/**
	 * @return the transferTypeId
	 */
	public int getTransferTypeId() {
		return transferTypeId;
	}

	/**
	 * @param transferTypeId the transferTypeId to set
	 */
	public void setTransferTypeId(int transferTypeId) {
		this.transferTypeId = transferTypeId;
	}

	/**
	 * @return the service
	 */
	public String getService() {
		return service;
	}

	/**
	 * @param service the service to set
	 */
	public void setService(String service) {
		this.service = service;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the refundDestination
	 */
	public int getRefundDestination() {
		return refundDestination;
	}

	/**
	 * @param refundDestination the refundDestination to set
	 */
	public void setRefundDestination(int refundDestination) {
		this.refundDestination = refundDestination;
	}

	/**
	 * @return the envoyTran
	 */
	public Transaction getEnvoyTran() {
		return envoyTran;
	}

	/**
	 * @param envoyTran the envoyTran to set
	 */
	public void setEnvoyTran(Transaction envoyTran) {
		this.envoyTran = envoyTran;
	}

	/**
	 * @return the withdrawCountryCode
	 */
	public String getRefundCountryCode() {
		return refundCountryCode;
	}

	/**
	 * @param withdrawCountryCode the withdrawCountryCode to set
	 */
	public void setRefundCountryCode(String withdrawCountryCode) {
		this.refundCountryCode = withdrawCountryCode;
	}


}
