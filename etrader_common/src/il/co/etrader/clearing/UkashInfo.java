//package il.co.etrader.clearing;
//
//import il.co.etrader.bl_managers.ClearingManager;
//import il.co.etrader.bl_vos.UserBase;
//import il.co.etrader.util.CommonUtil;
//
//import com.anyoption.common.beans.Transaction;
//import com.anyoption.common.clearing.ClearingInfo;
//
///**
// * Ukash clearing info class
// * @author Eran
// *
// */
//public class UkashInfo extends ClearingInfo {
//
//    // for Ukash
//	private String payeeName;
//    private String payeePhone;
//    private String voucherNumber;
//    private String voucherValue;
//    private String voucherCurrency;
//    private String userCurrencyCode;
//    private boolean differentCurrencies;
//    private int status;
//    private final int minAmount = 100;
//
//    public UkashInfo() {
//    	providerId = ClearingManager.UKASH_PROVIDER_ID;
//    }
//
//    public UkashInfo(UserBase user, Transaction t){
//    	super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId());
//    	providerId = ClearingManager.UKASH_PROVIDER_ID;
//    	setDetailsByUser(user);
//    }
//
//    public UkashInfo(UserBase user){
//    	providerId = ClearingManager.UKASH_PROVIDER_ID;
//    	setDetailsByUser(user);
//    }
//
//    private void setDetailsByUser(UserBase user) {
//    	payeeName = user.getFirstName() + " " + user.getLastName();
//        payeePhone = user.getMobilePhone();
//        if (CommonUtil.IsParameterEmptyOrNull(payeePhone)){ // for German users who don't have a mobile phone
//        	if (!CommonUtil.IsParameterEmptyOrNull(user.getLandLinePhone())){
//        		payeePhone = user.getLandLinePhone();
//        	} else {
//        		payeePhone = "123456789";
//        	}
//        }
//    }
//
//
//	/**
//	 * Implement toString function
//	 */
//	   public String toString() {
//	        String ls = System.getProperty("line.separator");
//	        String superToStr = super.toString();  // base toString
//	        return ls + superToStr + ls + "Ukash Info:" + ls +
//	        	"payeeName:" + payeeName + ls +
//		    	"payeePhone:" + payeePhone + ls +
//		    	"voucherNumber:" + voucherNumber + ls +
//		    	"voucherValue:" + voucherValue + ls +
//		    	"voucherCurrency:" + voucherCurrency + ls +
//	        	"userCurrencyCode:" + userCurrencyCode;
//	    }
//
//	/**
//	 * @return the payeeName
//	 */
//	public String getPayeeName() {
//		return payeeName;
//	}
//
//	/**
//	 * @param payeeName the payeeName to set
//	 */
//	public void setPayeeName(String payeeName) {
//		this.payeeName = payeeName;
//	}
//
//	/**
//	 * @return the payeePhone
//	 */
//	public String getPayeePhone() {
//		return payeePhone;
//	}
//
//	/**
//	 * @param payeePhone the payeePhone to set
//	 */
//	public void setPayeePhone(String payeePhone) {
//		this.payeePhone = payeePhone;
//	}
//
//	/**
//	 * @return the voucherNumber
//	 */
//	public String getVoucherNumber() {
//		return voucherNumber;
//	}
//
//	/**
//	 * @param voucherNumber the voucherNumber to set
//	 */
//	public void setVoucherNumber(String voucherNumber) {
//		this.voucherNumber = voucherNumber;
//	}
//
//	/**
//	 * @return the voucherValue
//	 */
//	public String getVoucherValue() {
//		return voucherValue;
//	}
//
//	/**
//	 * @param voucherValue the voucherValue to set
//	 */
//	public void setVoucherValue(String voucherValue) {
//		this.voucherValue = voucherValue;
//	}
//
//	/**
//	 * @return the voucherCurrency
//	 */
//	public String getVoucherCurrency() {
//		return voucherCurrency;
//	}
//
//	/**
//	 * @param voucherCurrency the voucherCurrency to set
//	 */
//	public void setVoucherCurrency(String voucherCurrency) {
//		this.voucherCurrency = voucherCurrency;
//	}
//
//	/**
//	 * @return the userCurrencyCode
//	 */
//	public String getUserCurrencyCode() {
//		return userCurrencyCode;
//	}
//
//	/**
//	 * @param userCurrencyCode the userCurrencyCode to set
//	 */
//	public void setUserCurrencyCode(String userCurrencyCode) {
//		this.userCurrencyCode = userCurrencyCode;
//	}
//
//	/**
//	 * @return the differentCurrencies
//	 */
//	public boolean isDifferentCurrencies() {
//		return differentCurrencies;
//	}
//
//	/**
//	 * @param differentCurrencies the differentCurrencies to set
//	 */
//	public void setDifferentCurrencies(boolean differentCurrencies) {
//		this.differentCurrencies = differentCurrencies;
//	}
//
//	/**
//	 * @return the status
//	 */
//	public int getStatus() {
//		return status;
//	}
//
//	/**
//	 * @param status the status to set
//	 */
//	public void setStatus(int status) {
//		this.status = status;
//	}
//
//	/**
//	 * @return the ukashMinAmount
//	 */
//	public int getMinAmount() {
//		return minAmount;
//	}
//}