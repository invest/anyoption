package mailTaxJob;

import il.co.etrader.bl_vos.UserBase;

import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

public class MailTaxJob {

	private static Logger log = Logger.getLogger(MailTaxJob.class);
	private static Template emailTemplate;
	// The server properties
	private static Hashtable<String, String> serverProperties;
	// The specific email Properties
	private static Hashtable<String, String> emailProperties;
	public static String USER_NAME;
	public static String USER_TAX_AMOUNT;
	public static String USER_EMAIL;
	public static String newLine = "\n";
	public static String MID_YEAR_TAX_TEMPLATE = "midYearTax.html";
	public static String END_YEAR_TAX_TEMPLATE = "endYearTax.html";

	public static void main(String[] args) throws Exception {
		Utils.propFile = args[0];
		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Starting the Mail Tax Job.");
		}
		//Tax Mail Job
		SendTaxEmail();

		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Tax Mail Job completed.");
		}
	}

	/**
	 * @throws Exception
	 *
	 */
	private static void SendTaxEmail() throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<UserBase> users = new ArrayList<UserBase>();

		try {

			conn = Utils.getConnection();
			String sql ="SELECT " +
							"u.id, u.first_name, u.email, th.tax " +
						"FROM " +
							"users u,tax_history th " +
						"WHERE " +
							"u.id = th.user_id " +
							"AND th.period = ? " +
							"AND th.year = ? " +
							"AND th.tax > 0 ";
			//run on specific user
			if (null != Utils.getProperty("user.id")) {
					sql+=	"AND u.id = ? ";
			}

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, Utils.getProperty("tax.period"));
			pstmt.setString(2, Utils.getProperty("tax.year"));
			if (null != Utils.getProperty("user.id")){
				pstmt.setString(3, Utils.getProperty("user.id"));
			}
			rs = pstmt.executeQuery();

			while(rs.next()) {
				UserBase tmp = new UserBase();
				tmp.setId(rs.getLong("ID"));
				tmp.setFirstName(rs.getString("FIRST_NAME"));
				tmp.setEmail(rs.getString("EMAIL"));
				tmp.setTaxBalance(rs.getLong("TAX"));
				users.add(tmp);
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get users " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		//String template = MID_YEAR_TAX_TEMPLATE;
		//if(Utils.getProperty("tax.period").equalsIgnoreCase("2")){
		String template = END_YEAR_TAX_TEMPLATE;
		//}

		log.log(Level.DEBUG,"going to send " + template + " emails ");

		sendEmailForUsersList(users , template);

		log.log(Level.DEBUG,"finished to send emails");

	}


	/** Send Mail for the selected users list
	 * @param users
	 * @throws Exception
	 */
	private static void sendEmailForUsersList(ArrayList<UserBase> users, String htmlName) throws Exception {

		// put all users params
		HashMap<String, Object> params = new HashMap<String, Object>();

		try {
			initEmail(htmlName);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
		}

		for (UserBase user: users){
			USER_NAME =  user.getFirstName();
			USER_TAX_AMOUNT = displayAmount(user.getTaxBalance());
			if (Utils.getProperty("send.to").equalsIgnoreCase("Live")){
				USER_EMAIL = user.getEmail();
			} else {
				USER_EMAIL = Utils.getProperty("send.to");
			}
			params.put("userFirstName", USER_NAME);
			params.put("userTaxBalance", USER_TAX_AMOUNT);
			if(htmlName.equalsIgnoreCase(END_YEAR_TAX_TEMPLATE)){
				params.put("year", Utils.getProperty("tax.year"));
			}
			emailProperties.put("body", getEmailBody(params));
			emailProperties.put("to",USER_EMAIL);
			Utils.sendEmail(serverProperties,emailProperties);
			log.info(newLine + "User ID: " + user.getId() + newLine + "User Name: " + USER_NAME + newLine + "Email: " + USER_EMAIL + newLine + "Tax is: " +USER_TAX_AMOUNT +newLine );
			emailProperties.remove("to");
		}

	}

	/**
	 * Initialize Email configuration
	 * @param name
	 * @throws Exception
	 */
	private static void initEmail(String name) throws Exception {
		// get the emailTemplate
		emailTemplate = getEmailTemplate(name);

		// set the server properties
		serverProperties = new Hashtable<String, String>();
		serverProperties = new Hashtable<String, String>();
		serverProperties.put("url",Utils.getProperty("email.url"));
		serverProperties.put("user",Utils.getProperty("email.user"));
		serverProperties.put("pass",Utils.getProperty("email.pass"));
		serverProperties.put("auth","true");
		// Set the email properties
		emailProperties = new Hashtable<String, String>();
		emailProperties.put("subject",Utils.getProperty("etrader.midYearTax.subject"));
		emailProperties.put("from",Utils.getProperty("email.from.etrader"));
	}

	/**
	 * Get Email Template
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	private static Template getEmailTemplate(String fileName) throws Exception {
		try {
			Properties props = new Properties();
			props.setProperty("file.resource.loader.path",Utils.getProperty("template.path"));
			props.setProperty("resource.loader","file");
			props.setProperty("file.resource.loader.class","org.apache.velocity.runtime.resource.loader.FileResourceLoader");
			props.setProperty("file.resource.loader.cache","true");
			VelocityEngine v = new VelocityEngine();
			v.init(props);
			return v.getTemplate(fileName,"UTF-8");
		} catch (Exception ex) {
			log.fatal("ERROR! Cannot find template : " + ex.getMessage());
			throw ex;
		}
	}

	/**
	 * Get Email template and put a params
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public static String getEmailBody(HashMap params) throws Exception {

		// set the context and the parameters
		VelocityContext context = new VelocityContext();

		// get all the params and add them to the context
		String paramName = "";
		log.debug("Adding parrams");

		for (Iterator keys = params.keySet().iterator(); keys.hasNext();) {
			paramName = (String) keys.next();
			context.put(paramName,params.get(paramName));
		}
		StringWriter sw = new StringWriter();
		emailTemplate.merge(context,sw);

		return sw.toString();
	}

	/**
	 * Display amount with currency
	 * @param amount
	 * @return
	 */
	public static String displayAmount(long amount) {
		DecimalFormat sd = new DecimalFormat("###,###,##0.00");
		double amountDecimal = amount;
		amountDecimal /= 100;
		return sd.format(amountDecimal);
	}
}

