package mailTaxJob;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.MissingResourceException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import oracle.jdbc.driver.OracleDriver;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
/**
 * console utils
 */
public class Utils {
	private static final Logger log = Logger.getLogger(Utils.class);

	private static boolean registered=false;
    private static Properties properties=null;
    public static String propFile;
    public static String EMAIL_USER = "";
	public static String EMAIL_PASS = "";

	public static final String CURRENCY_ILS = "ILS";
	public static final String CURRENCY_USD = "USD";
	public static final String CURRENCY_TRY = "TRY";
	public static final String CURRENCY_RUB = "RUB";
	public static final String CURRENCY_EUR = "EUR";

    public static String UNIX_ERROR_STREAM = "runError: ";
    public static String UNIX_GEO_IP_LOOKUP_COMMAND = "geoiplookup ";
    public static String UNIX_GEO_IP_LOOKUP_RES_COUNTRY = ",";
    public static String UNIX_GEO_IP_LOOKUP_RES_ERROR_COUNTRY = "--";

    public static Connection getConnection() throws SQLException{
   	 	if (!registered) {
   	 		DriverManager.registerDriver(new OracleDriver());
   	 		registered=true;
   	 	}
   	 	log.debug("getting db connection");

   	 	return DriverManager.getConnection(getProperty("db.url"),getProperty("db.user"),getProperty("db.pass"));

   }

    public static String getProperty(String key) {

		if (properties == null) {
			properties = new Properties();
			try {
				properties.load(new FileInputStream(propFile));
			} catch (Exception ex) {
				log.fatal("failed to load properties "+ex.toString());
				System.exit(1);
			}
		}

		if (key == null || key.trim().equals(""))
			return "";

		String text = null;
		try {
			text = properties.getProperty(key);
		} catch (MissingResourceException e) {
			return "";
		}

		return text;

	}

    public static void sendEmail(Hashtable server, Hashtable msg) {
		Properties props = new Properties();
		EMAIL_USER = (String) server.get("user");
		EMAIL_PASS = (String) server.get("pass");
		props.put("mail.smtp.host", (String) server.get("url"));
		//props.put("mail.smtp.auth", "true");
		String subject = (String) msg.get("subject");
		String to = (String) msg.get("to");
		String from = (String) msg.get("from");
		String body = (String) msg.get("body");
		//  log.debug(body);     //For Test

		String[] splitTo = to.split(";");
		for (int i = 0; i < splitTo.length; i++) {
			sendSingleEmail(props, subject, splitTo[i], from, body, server);
		}
	}

    private static void sendSingleEmail(Properties props, String subject, String to, String from, String body, Hashtable server) {
		Session session = Session.getInstance(props, new ForcedAuthenticator());
		Message mess = new MimeMessage(session);

		try {
			// Set Headers
			mess.setHeader("From", from);
			mess.setHeader("To", to);
			mess.setHeader("Content-Type", "text/html; charset=iso-8859-1");
			mess.setHeader("Content-Transfer-Encoding", "base64");

			mess.setFrom(new InternetAddress(from));
			mess.setSubject(subject);
			mess.setContent(body, "text/html; charset=utf-8");

			// Address[] address = { new InternetAddress(to) };
			// Transport trans = session.getTransport("smtp");
			// if (null != server.get("auth") && ((String)
			// server.get("auth")).equals("true")) {
			// trans.connect((String) server.get("url"), EMAIL_USER,
			// EMAIL_PASS);
			// } else {
			// trans.connect();
			// }
			Transport.send(mess);

		} catch (AddressException ae) {
			log.log(Level.FATAL, "Error Sending Mail to: " + to + "body: " + body, ae);
		} catch (MessagingException me) {
			log.log(Level.FATAL, "Error Sending Mail to: " + to + "body: " + body, me);
		}
	}

	static class ForcedAuthenticator extends Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(EMAIL_USER,EMAIL_PASS);

		}
	}

	public static String displayAmount(double amount) {
		DecimalFormat sd = new DecimalFormat("###,###,##0.00");
		String out = "";
		out = sd.format(amount);

		return out;
	}

/*    *//**
     * Get contry code by ip with unix command
     * @param ip  for getting country code
     * @return country code
     *//*
    public static String getCountryCodeByIp(String ip) {

   	 String code = null;
   	 String command = UNIX_GEO_IP_LOOKUP_COMMAND + ip;
   	 String unixRes = runUnixCommand(command);

   	 if ( IsParameterEmptyOrNull(unixRes) || unixRes.indexOf(UNIX_ERROR_STREAM) > -1 ) {   // error with running command
   		 log.warn("Error with running unix geoiplookup  command, res: " + unixRes);
   		 return null;
   	 } else {  // get country code from string result
   		 	int index = unixRes.indexOf(UNIX_GEO_IP_LOOKUP_RES_COUNTRY);  // search for string in the result e.g:
   		 	if ( index > -1  ) {														// GeoIP Country Edition: US, United States
   		 		try {
		    		 	code = unixRes.substring(index-2, index);
		    		 	if ( IsParameterEmptyOrNull(code) ||
		    		 			code.equalsIgnoreCase(UNIX_GEO_IP_LOOKUP_RES_ERROR_COUNTRY)) {   // error to extract code

		    		 		log.warn("Error to extract countryCode with running unix geoiplookup command, res: " + unixRes);
		    		 		return null;
		    		 	}
   		 		} catch (IndexOutOfBoundsException e) {
   		 			log.warn("Error to extract countryCode with running unix geoiplookup command, res: " + e);
   		 			return null;
					}
   		 	}
   	 }
   	 return code;
    }*/

    /**
     * Run unix command
     * @param command  unix command to run
     * @return
     */
     public static String runUnixCommand(String command) {

	       String s = "";
	       String res = "";

	        try {
	        		log.info("going to run unix command: " + command);
		            Process p = Runtime.getRuntime().exec(command);

		            BufferedReader stdInput = new BufferedReader(new
		                 InputStreamReader(p.getInputStream()));

		            BufferedReader stdError = new BufferedReader(new
		                 InputStreamReader(p.getErrorStream()));

		            // read the output from the command
			         while ((s = stdInput.readLine()) != null) {
			        	 log.info(s);
			             res += s;
			         }

			         // read any errors from the attempted command
			         while ((s = stdError.readLine()) != null) {
			            	res = UNIX_ERROR_STREAM + s;
			            	log.info(s);
			            	break;
			         }

	        } catch (IOException e) {
	        		log.info("Exception running unix command: " + e);
		            res = UNIX_ERROR_STREAM;
		    }

	        return res;
    }

 	/**
 	 * Check if some variable is empty or not.
 	 *
 	 * @param par
 	 *            The parameter that need to check if he is empty or not
 	 */
 	public static boolean IsParameterEmptyOrNull(String par) {
 		if( (par == null || par.length() == 0) ) {
 			return true;
 		}
 		return false;
 	}

    /**
     * Get country name by country code
     * @param country code
     * @param countryCodeLength - can be 2 (for a2) or 3 (for a3)
     * @return country name
     */
    public static String getCountryNameByCode(String countryCode, int countryCodeLength) {
    	String countryName = null;

    	Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

    	try{
    		conn = getConnection();

    		String sql =
				"SELECT " +
	            	"c.COUNTRY_NAME country_name " +
	            "FROM " +
	            	"countries c " +
	            "WHERE ";

    		switch (countryCodeLength) {
				case 2:
					sql += "c.a2 = ? ";
					break;
				case 3:
					sql += "c.a3 = ? ";
					break;
				default:
					log.log(Level.FATAL,"Can't find country name for code with length " + countryCodeLength );
					return null;
				}


    		pstmt = conn.prepareStatement(sql);
    		pstmt.setString(1, countryCode);

    		rs = pstmt.executeQuery();

    		if (rs.next()){
    			countryName = rs.getString("country_name");
    		}

    	} catch (Exception e) {
			log.log(Level.FATAL,"Can't find country name by code " + countryCode  + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

   	 	return countryName;
    }


}