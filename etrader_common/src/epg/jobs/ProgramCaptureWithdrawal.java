package epg.jobs;

import java.sql.Connection;
import java.util.TreeSet;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;
import org.jboss.logging.Logger;
import com.anyoption.common.managers.BaseBLManager;

/**
 * @author LioR SoLoMoN
 *
 */
public class ProgramCaptureWithdrawal extends JobUtil {
	private static final Logger logger = Logger.getLogger(ProgramCaptureWithdrawal.class);
	private static final String LOG_PREFIX_MSG = "";
	
	public static void main(String[] args) {
		// take propertis file
		if (args.length != 1 || args == null) {
			logger.error(LOG_PREFIX_MSG + "ERROR! file not found. Proper Usage is: java program filename.");
			return;
		}
		logger.info(LOG_PREFIX_MSG + " START JOB.");		
		
		propFile = args[0];
		String transactionIds = getPropertyByFile("transaction.id", ConstantsBase.EMPTY_STRING);
		Connection connection = null;
		try {
			connection = getConnection();
			ClearingManager.loadClearingProviders(connection);
			TreeSet<SummaryEPGCaptureWithdrawal> reportRecords = EPGCaptureWithdrawalJobManager.doJob(connection, transactionIds);
			buildReport();
		} catch (Exception e) {
			logger.error("");
		} finally {
			BaseBLManager.closeConnection(connection);
		}
		
		logger.info(LOG_PREFIX_MSG + " END JOB.");
	}

	private static void buildReport() {
		// TODO Auto-generated method stub
		
	}
}
