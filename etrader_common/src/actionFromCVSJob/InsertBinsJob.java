package actionFromCVSJob;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.JobUtil;

public class InsertBinsJob extends JobUtil {
	private static Logger log = Logger.getLogger(InsertBinsJob.class);

	private static int conCounter = 0;

	public static void main(String[] args) {

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the job.");
        }
        propFile = args[0];
        //read the users from CSV file
        String fileName = "f:\\bins.csv";
        log.info("fileName = " + fileName);

        String[] users = CommonUtil.getUsersListFromCSV(fileName,"\n");
        ArrayList<String> errorBins = new ArrayList<String>();
		PreparedStatement ps = null;
        int i = 0;
        String startBin = "";
		String endBin = "";

		try{
        	Connection con = getConnection();
//        	//bonus withdraw is activated only for users that didn't use their bonus
        	//userIds are taken from sql inside the function
        	//insertBonusWithdraw(con);


			String sql=
				"insert into bins2(FROM_BIN,TO_BIN,CARD_TYPE,COUNTRY_NAME,CARD_PAYMENT_TYPE) " +
				"values(?,?,?,?,?) ";

			ps = con.prepareStatement(sql);


        	for(String bin:users){

        		try{
	        		i++;

	        		String[] binFields = bin.split(",",99);

	        		startBin = binFields[0];
	        		endBin = binFields[0];

	        		while (startBin.length() < 6 ){
	        			startBin += "0";
	        			endBin += "9";
	        		}
	        		ps.setString(1, startBin.substring(0, 6));
	           		ps.setString(2, endBin.substring(0, 6));
	           		ps.setString(3, binFields[1].trim());
	           		ps.setString(4, binFields[2].trim());
	           		ps.setString(5, binFields[3].trim());

	        		log.info("insert bin num: " + i + " - " + startBin.substring(0, 6));
	        		ps.executeUpdate();

	        		if (++conCounter == 500){
	        			con.close();
	            		con = getConnection();
	            		ps = con.prepareStatement(sql);
	            		conCounter = 0;
	            		log.info("reset connection");
	        		}
        		}catch (Exception e) {
        			log.error("Inserting failed, bin: " + startBin.substring(0, 6) ,e);
        			errorBins.add(startBin.substring(0, 6));
				}

        	}
        	con.close();
        }catch(SQLException se){
        	log.fatal("Exception during performing action :"+se.getMessage());
        	se.printStackTrace();
        }


        if (errorBins.size() > 0){
        	log.log(Level.ERROR, "------------------------------------------");
        	log.log(Level.ERROR, "Users with Errors are:");
        	for (String binStr: errorBins){
        		log.log(Level.ERROR, binStr);
        	}
        	log.log(Level.ERROR, "------------------------------------------");
        }

        if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Job completed.");
	    }
        if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Sending report...");
	    }
    }


}

