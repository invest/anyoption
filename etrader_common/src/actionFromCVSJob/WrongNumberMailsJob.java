package actionFromCVSJob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.CountryDAOBase;
import com.anyoption.common.daos.LanguagesDAOBase;

import il.co.etrader.dao_managers.SkinsDAOBase;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;
import il.co.etrader.util.SendTemplateEmail;


public class WrongNumberMailsJob extends JobUtil {
	private static Logger log = Logger.getLogger(WrongNumberMailsJob.class);
	private static Hashtable<String, String> emailProperties;
	private static String unSuccessfullLog = new String("");

	public static void main(String[] args) {

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the job.");
        }
        propFile = args[0];
        //read the users from CSV file
        ArrayList<String> errorUsers = new ArrayList<String>();
    	HashMap<String, String> countrySupportPhones = new HashMap<String, String>();
        String supportPhone;

        try{
        	Connection con = getConnection();

        	 // get all countries and build countries list
    		ArrayList<Country> cList = CountryDAOBase.getAll(con);
			for (Country c : cList) {
				// set support data
				if (!c.getSupportPhone().equals(ConstantsBase.COUNTRY_EMPTY_VALUE) &&
						!c.getSupportFax().equals(ConstantsBase.COUNTRY_EMPTY_VALUE)) {
					countrySupportPhones.put(String.valueOf(c.getId()), c.getSupportPhone());
				}
			}

        	PreparedStatement ps = null;
    	    ResultSet rs = null;


    	    // Need to fix sql before running - example: there is no ia.reaction_id = 10 anymore
    	        String sql = " "
//                        " select " +
//                            " pu.user_id, " +
//                            " u.user_name, " +
//                            " u.skin_id, " +
//                    		" u.first_name , " +
//                    		" u.country_id, " +
//                    		" u.email,  " +
//                    		" u.gender, " +
//                    		" u.IS_CONTACT_BY_EMAIL " +
//						" from  issue_actions ia, population_users pu, users u " +
//						" where entry_type_id = 5 " +
//							" and pu.entry_type_action_id = ia.id " +
//							" and ia.reaction_id = 10 " +
//							" and ia.action_time < to_date('14/02/2010-17:00','dd/mm/yyyy-HH24:MI') " +
//							" and u.class_id > 0 " +
//							" and pu.user_id = u.id " +
//							" and u.IS_CONTACT_BY_EMAIL  = 1" +
//						" order by ia.action_time desc"
    	        	;

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			long userId = 0;
			String userFirstName = null;
			String username = null;
			String email = null;
			long skinId = 0;
			String countryIdStr = null;
			long langId =0 ;
			String langCode = null;
			String subject = null;
			boolean isContactByEmail = false;
			String genderTxt = null;
			String gender;
			int count = 0;

        	while(rs.next()){
        		supportPhone = "";
        		userId = rs.getLong("user_id");
        		userFirstName = rs.getString("first_name");
        		username = rs.getString("user_name");
        		email = rs.getString("email");
        		skinId = rs.getLong("skin_id");
        		gender = rs.getString("gender");
        		countryIdStr = rs.getString("country_id");
        		isContactByEmail = (rs.getInt("IS_CONTACT_BY_EMAIL")==1)?true:false;

//       		 handle support phone for anyoption
        		if ( skinId != Skin.SKIN_ETRADER ) {
             		// get support phone
            		supportPhone = countrySupportPhones.get(countryIdStr);
        		}

        		log.info("trying to send mail to user "+ userId + " name: " + userFirstName + " username: " + username + " email: " + email);
        		try {

			       langId = SkinsDAOBase.getById(con, (int)skinId).getDefaultLanguageId();
			       langCode = LanguagesDAOBase.getCodeById(con, langId);
			       mailTemplate = getEmailTemplate("wrongNumberMailUser_" + langCode + ".html", null, null);

			       emailProperties = new Hashtable<String, String>();

			       subject = properties.getProperty("subject." + langCode);
			       emailProperties.put("subject",subject);

			       if (gender.equals("M")) {
			    	   genderTxt = properties.getProperty("template.des.gender.male");
					} else {
						genderTxt = properties.getProperty("template.des.gender.female");
					}



			       HashMap<String,String> params = new HashMap<String, String>();
	   		       params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME,userFirstName);
	   		       params.put(SendTemplateEmail.PARAM_USER_NAME, username);
	   			   params.put(SendTemplateEmail.PARAM_SUPPORT_PHONE, supportPhone);

				   try {
					   emailProperties.put("body",getEmailBody(params));
				   } catch (Exception e) {
					   log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
					   System.exit(1);
				   }

				   emailProperties.put("to","eliran@etrader.co.il");
				   if (skinId == 1){
					   emailProperties.put("from",properties.getProperty("email.from.etrader"));
				   } else {
					   emailProperties.put("from",properties.getProperty("email.from.anyoption"));
					   if (skinId == Skin.SKIN_CHINESE) {
						   emailProperties.put("from",properties.getProperty("email.from.anyoption.zh"));
					   }
				   }

				   if( isContactByEmail){
					   // send email only to users with IS_CONTACT_BY_EMAIL=1
					   sendSingleEmail(properties, emailProperties, langCode);
					   log.info("Mail " + (++count) + " sent to userID:"+userId);
				   } else {
					   unSuccessfullLog += userId +";";
				   }
				} catch (Exception e) {
					log.error("didn't send mail to  user "+ userId,e);
					errorUsers.add(String.valueOf(userId));
				}
        	}
        	con.close();
        }catch(SQLException se){
        	log.fatal("Exception during performing action :"+se.getMessage());
        	se.printStackTrace();
        }

        if (errorUsers.size() > 0){
        	log.log(Level.ERROR, "Users with Errors are:");
        	for (String user: errorUsers){
        		log.log(Level.ERROR, user);
        	}
        }

        if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Job completed.");
	    }
        if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Sending report...");
	    }

    }


}

