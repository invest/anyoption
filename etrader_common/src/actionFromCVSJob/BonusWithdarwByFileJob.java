package actionFromCVSJob;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.daos.CurrenciesDAOBase;
import com.anyoption.common.daos.GeneralDAO;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.dao_managers.WritersDAO;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

public class BonusWithdarwByFileJob extends JobUtil{

	private static final Logger log = Logger
			.getLogger(BonusWithdarwByFileJob.class);
	/**
	 * @param args
	 * @throws Exception 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, Exception, SQLException {
		// TODO Auto-generated method stub
		
		propFile = args[0];
		HashMap<Long, Long> hm = new HashMap<Long, Long>();
		ArrayList<UserBase> list = new ArrayList<UserBase>();
		ArrayList<BonusUsers> buList = new ArrayList<BonusUsers>(); 
		PreparedStatement ps = null;
		ResultSet rs = null;
		Double AmountToWithdraw = 0.0;
		String usersFromFile ="";
		Connection con = null;		
		usersFromFile = CommonUtil.getUsersListAmountFromTXT("f:\\users.txt",",",hm);
		usersFromFile = usersFromFile.substring(0, usersFromFile.length()-1);
		
		String sqlGetUsers = " select u.* from users u where u.id in (" + usersFromFile + ")";
		
		String sqlGetBonusUsers =    " SELECT bu.* " +
									 " FROM bonus_users bu " +
									 " WHERE bu.user_id in (" + usersFromFile + ")" +
									 " and bu.bonus_state_id in ( 2 , 3 ) " +
									 " and to_char(bu.time_activated ,'yyyymmdd') < 20120901 ";
		
		if(!CommonUtil.isParameterEmptyOrNull(usersFromFile)){
			try{
		
			con = getConnection();
			ps = con.prepareStatement(sqlGetUsers);
			rs = ps.executeQuery();
			while(rs.next()){
				UserBase user = new UserBase();
				user.setId(rs.getLong("id"));
				user.setBalance((rs.getLong("balance")));
				user.setCurrencyId(rs.getLong("currency_id"));
				user.setUtcOffset(rs.getString("utc_offset"));
				user.setContactId(rs.getLong("contact_id"));
				list.add(user);
			}
			
		}finally{
			con.close();
			ps.close();
			rs.close();
		}
			
		try{
			con = getConnection();
			ps = con.prepareStatement(sqlGetBonusUsers);
			rs = ps.executeQuery();
			while(rs.next()){
				BonusUsers bu = new BonusUsers();
				bu.setId(rs.getLong("id"));
				buList.add(bu);
			}
		} finally {
			con.close();
			ps.close();
			rs.close();
		}
		
		for(UserBase user : list ){
			if(AmountToWithdraw != 0){
				AmountToWithdraw = 0.0 ;
			}
			AmountToWithdraw = Double.parseDouble(hm.get(user.getId()).toString())/100;
			Double balance = Double.valueOf(String.valueOf(user.getBalance()))/100;
			if(AmountToWithdraw > 0 && balance > 0 ){
				if(AmountToWithdraw >= balance){
					AmountToWithdraw = balance;
				}
				try{
					con = getConnection();
					insertBonusWithdraw(con, user, CommonUtil.calcAmount(AmountToWithdraw));
					insertIssueToUsers(con, user);
					log.debug("Success Bonus Withdraw on user: " + user.getId());
				} catch (Exception e){
					log.error("problem withdraw bonus from balance from user: " + user.getId());
				} finally {
					con.close();
				}
			}else{
				log.debug("No Amount To Withdrawa on user: " + user.getId() + " amount : " + AmountToWithdraw +
						"users balance : " + user.getBalance() );
			}
		}
		
		for(BonusUsers bu : buList){
			try{
				con = getConnection();
				updateBonusWageringParam(con, 0, bu.getId());
				log.debug("update bonus user id :"  + bu.getId());
			} finally {
				con.close();
			}
		}
		
		log.debug("Finished Job Succesfully");
		}else {
			log.error("Can't find file or users from file");
		}
		log.debug("Finished Job Succesfully");
	}
	
	public static boolean insertBonusWithdraw(Connection con, UserBase user, Long amount ) throws Exception {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Insert Bonus Withdraw: " + ls + "userId: "
					+ user.getId() + ls + amount + ls);
		}


		try {

			con.setAutoCommit(false);

			Transaction tran = new Transaction();
			tran.setAmount(amount);
			tran.setCurrency(new Currency( CurrenciesDAOBase.getById(con, Integer.parseInt(user.getCurrencyId().toString()))));
			tran.setComments("Bonus funds were withdrawn as wagering wasn't reached in 3 months.");
			tran.setCreditCardId(null);
			tran.setDescription("log.balance.bonus.withdraw");
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(Writer.WRITER_ID_AUTO);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setUtcOffsetSettled(user.getUtcOffset());
			tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW);
			tran.setUserId(user.getId());
			tran.setWriterId(Writer.WRITER_ID_AUTO);
			tran.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
			tran.setChequeId(null);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);

			
			UsersDAOBase.addToBalance(con, user.getId(), -amount);//change the balance
			TransactionsDAOBase.insert(con, tran);
			GeneralDAO.insertBalanceLog(con, Writer.WRITER_ID_AUTO, tran.getUserId(),
					ConstantsBase.TABLE_TRANSACTIONS, tran.getId(),
					ConstantsBase.LOG_BALANCE_BONUS_WITHDRAW, user.getUtcOffset());
			

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, "Bonus Withdraw insert successfully! " + ls
						+ tran.toString());
			}

			con.commit();
		} catch (SQLException e) {
			log.log(Level.ERROR, "insertBonusWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
		}
		return true;
	}

	public static void insertIssueToUsers(Connection con, UserBase user) throws SQLException{
		Issue issue = new Issue();
		IssueAction issueAction = new IssueAction();

		// Filling issue fields
		issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_BONUS));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issue.setUserId(user.getId());
		issue.setContactId(user.getContactId());
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		// Filling action fields
		issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_COMMENT));
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_COMMENT));
		issueAction.setWriterId(Writer.WRITER_ID_AUTO);
		issueAction.setSignificant(false);
		issueAction.setActionTime(new Date());
		issueAction.setActionTimeOffset(WritersDAO.getWriterUtcOffset(con, Writer.WRITER_ID_AUTO));
		issueAction.setComments("Bonus was taken back from customer balance. It’s been more than 3 months from the day that he received it and customer didn’t complete the wagering.");

		IssuesDAOBase.insertIssue(con, issue);
		issueAction.setIssueId(issue.getId());
		IssuesDAOBase.insertAction(con, issueAction);
		//IssuesManagerBase.insertIssue(issue,issueAction,user.getUtcOffset(),ConstantsBase.WRITER_ID_AUTO,user, 0);
	}

	public static void updateBonusWageringParam(Connection con, long wageringParam, long userId) throws SQLException{
		PreparedStatement ps = null;
		String sql =" UPDATE " +
				        "bonus_users " +
				    " SET " +
				        " wagering_parameter = 0 , " +
				        " time_updated = current_date ," +
				        " bonus_state_id = " + ConstantsBase.BONUS_STATE_DONE + " , " +
				        " time_done = current_date " +
				    " WHERE " +
				        "id = ?";
		try {
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.executeQuery();
		} finally {
			ps.close();
		}		
	}
}
