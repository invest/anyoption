package actionFromCVSJob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.bl_vos.TransactionBin;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.JobUtil;

public class updateTransactionBinsTableJob extends JobUtil {

	/**
	 * @author oshikl
	 * @param args
	 * @throws SQLException 
	 * @throws CryptoException 
	 * @throws NumberFormatException 
	 */
	public static void main(String[] args) throws SQLException, NumberFormatException, CryptoException {
		
		propFile = args[0];
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		Connection conn = null;
		ArrayList<TransactionBin> list =  new ArrayList<TransactionBin>();
		String sqlUpdate = " UPDATE transactions_bins tb set tb.time_last_success = ? , tb.time_last_failed_reroute = ?  WHERE tb.from_bin = ? ";
		String selectSql = " select  bins.bin, " +
							" CASE WHEN all_s_tran.time_created is null  THEN null else   all_s_tran.time_created END last_sucsess , " +
							" CASE WHEN failed_reroute.time_failed is null  THEN null  else failed_reroute.time_failed  END  time_failed_reroute  " +
							" from  (  select cc.bin " +
							" from credit_cards cc " +
							" group by cc.bin  ) bins " +
							" left join  ( select max(t1.id) as id ,  max(t1.time_created) as time_created , cc.bin " +
							" from transactions t1, credit_cards cc " +
							" where t1.type_id = 1 and t1.credit_card_id = cc.id and t1.status_id in (2,7) " +
							" group by cc.bin) all_s_tran on all_s_tran.bin = bins.bin  " +
							" left join ( select max(t2.id) as id , cc2.bin, max(t2.time_created) as time_failed " +
							" from transactions t2, credit_cards cc2 " +
							" where t2.type_id = 1 and t2.id = t2.rerouting_transaction_id and " +
							" t2.credit_card_id = cc2.id " +
							" group by cc2.bin ) failed_reroute on failed_reroute.bin = bins.bin ";
				
		try {
			conn = getConnection();
			ps = conn.prepareStatement(selectSql);
			rs = ps.executeQuery();
			while (rs.next()){
				TransactionBin tb = new TransactionBin();
				tb.setBin(rs.getLong("bin"));
				tb.setTimeLastSuccess(rs.getDate("last_sucsess"));
				tb.setTimeLastFailedReroute(rs.getDate("time_failed_reroute"));
				list.add(tb);				
				}
			} finally {
				conn.close();
			}
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sqlUpdate);
			
			final int batchSize = 1000;
			int count = 0;
			
			for(TransactionBin tb : list){
				if(tb.getBin() > 0 ){
					ps.setTimestamp(1, CommonUtil.convertToTimeStamp(tb.getTimeLastSuccess()));
					ps.setTimestamp(2, CommonUtil.convertToTimeStamp(tb.getTimeLastFailedReroute()));
					ps.setLong(3, tb.getBin());
					ps.addBatch();
					
					
					if(++count % batchSize == 0) {
				        ps.executeBatch();
				    }
				}
			}	
			ps.executeBatch(); 
			ps.close();				
		} finally {
			conn.close();
			System.out.println("Job Finished");
		}		
		}
	}
