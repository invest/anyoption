package actionFromCVSJob;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;


public class ChangeUsersSkinJob extends JobUtil {
	private static Logger log = Logger.getLogger(ChangeUsersSkinJob.class);

	public static String newLine = "\n";

	private static HashMap<Long, String> supportPhone = new HashMap<Long, String>();

	public static void main(String[] args) {

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the job.");
        }
        propFile = args[0];
        long fromSkinId = Long.valueOf(getPropertyByFile("from.skin.id", "0"));
        long toSkinId = Long.valueOf(properties.getProperty("to.skin.id"));
        String user = properties.getProperty("user");
        Connection con = null;
        try {
	    	con = getConnection();
	    	ArrayList<UserBase> users = new ArrayList<UserBase>();
	    	if (null != user) {
	    		UserBase u = new UserBase();
	    		UsersDAOBase.getByUserId(con, Long.valueOf(user), u, true);
	    		if (u.getSkinId() != fromSkinId) {
	    			log.info("not good user skin id not equal to from skin id");
	    			return;
	    		}
	    		users.add(u);
	    	} else {
	    		users = UsersDAOBase.getBaseUsersBySkinId(con, fromSkinId);
	    	}

	    	//change users skin
	    	int updateUsers = UsersDAOBase.updateUsersSkin(con, fromSkinId, toSkinId, user);
	    	if (updateUsers == 0) {
                log.info("no users was updated");
    			return;
	    	}
            log.info(updateUsers + " users was updated");


	    	//add issue action to users
	    	for (UserBase u:users) {
	    		insertIssueMoveSkin(con, u.getId());
	    	}
            log.info("issue was sent");
		} catch (Exception e) {
            log.info("cant change users skin", e);
		} finally {
            if (null != con) {
                try {
                    con.close();
                } catch (SQLException e) {
                    log.info("cant close db con", e);
                }
            }
        }


        if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Job completed.");
	    }
    }

	public static void insertIssueMoveSkin(Connection con, long userId)throws SQLException{
	    Issue issue = new Issue();
	   	IssueAction issueAction = new IssueAction();

	   	issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_MOVE_SKIN));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issue.setUserId(userId);
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_MEDIUM));
		// Filling action fields
		issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_COMMENT));
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_COMMENT));
		issueAction.setWriterId(Writer.WRITER_ID_AUTO);
		issueAction.setSignificant(false);
		issueAction.setActionTime(new Date());
		issueAction.setActionTimeOffset("GMT+00:00");
		issueAction.setComments("User's skin was changed from AR to EN.");
		try{
//			if (log.isEnabledFor(Level.INFO)) {
//		        log.log(Level.INFO, "insert issue for "+user);
//		    }
			issue.setUserId(userId);
			issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
			// Insert Issue
			IssuesDAOBase.insertIssue(con, issue);

			issueAction.setIssueId(issue.getId());
			issueAction.setWriterId(Writer.WRITER_ID_AUTO);
			// Insert Issue action
			IssuesDAOBase.insertAction(con, issueAction);
		} catch(Exception e){
			log.fatal("Exception performing action on user :" + userId, e);
		}
	 }
}

