package actionFromCVSJob;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.MailBoxTemplate;
import com.anyoption.common.beans.MailBoxUser;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlerFactory;
import com.anyoption.common.daos.CurrenciesDAOBase;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.daos.LanguagesDAOBase;
import com.anyoption.common.managers.CurrenciesManagerBase;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.MailBoxTemplatesDAOBase;
import il.co.etrader.dao_managers.MailBoxUsersDAOBase;
import il.co.etrader.dao_managers.SkinsDAOBase;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;
import mailTaxJob.Utils;

public class ActionJob extends JobUtil {
	private static Logger log = Logger.getLogger(ActionJob.class);

	private static ArrayList<BonusReportLine> bonusReportData = new ArrayList<BonusReportLine>();
	private static BonusReportLine reportLine;
	private static Hashtable<String, String> serverProperties;
	private static Hashtable<String, String> emailProperties;
	private static Template mailTemplate;
	private static String EMAIL_USER = "";
	private static String EMAIL_PASS = "";
	private static long i = 1;
	private static int conCounter = 0;
	private static MailBoxTemplate mailBoxTemplate;
	private static long mailboxTemplateId;
	private static Template emailTemplate;
	public static String BONUS_TEMPLATE_FILE;
	private static Boolean isNextInvestOnUs;
	private static long issueDay;
	private static long issueMonth;
	private static long issueYear;
	private static long issueHourOfDay;
	private static long issueMinute;
	private static String issueComment;
	private static String CURRENCY_ILS;
	private static String CURRENCY_USD;
	private static String CURRENCY_EUR;
	private static String CURRENCY_GBP;
	private static String CURRENCY_TRY;
	private static String CURRENCY_RUB;
	private static String CURRENCY_CNY;
	private static String CURRENCY_KRW;
	private static String templateSendTo;
	private static String reportSendTo;
	private static HashMap<String, String> subjectTemplate = new HashMap<String, String>();
	private static long EMPTY_VAR = 0;
	
	//params
	private static String USER_FIRST_NAME;
	private static String USER_EMAIL;
	
	//send mail
	private static String emailUrl;
	private static String emailUser;
	private static String emailPass;
	private static String templateFile;
	private static HashMap<Long, String> emailFromHM;
	private static HashMap<Long, String> supportPhone;
	private static boolean isNeedToSendInternalMail;
	private static HashMap<Long, String> bonusAmountPerCurrencyHM; // only for display in templates.

	public static void main(String[] args) {

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the job.");
        }
        propFile = args[0];
        //read the users from CSV file
//        String[] users = CommonUtil.getUsersListFromCSV("d:\\users.txt",";");
        
        // bonus report.
        String reportName = "bonus_report_";
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HH_mm");
        GregorianCalendar gc = new GregorianCalendar();
        Date dateReport = gc.getTime();
		String filename = reportName + fmt.format(dateReport)+ ".csv";
		File file;
		String htmlBuffer = null;
		java.io.Writer output = null;
		String errorStr = null;
		File attachment = null;
		String env = JobUtil.getPropertyByFile(ConstantsBase.CURRENT_SYSTEM);
		String filePath = filename;
		boolean isHaveData = false;
		if (!env.equals("local")){
			filePath = JobUtil.getPropertyByFile("file.path") + filename;
			
		}
		
		file = new File(filePath);
		try {
			OutputStream os = new FileOutputStream(file);
			// Representation (decimal) number for utf-8 encoding by byte order mark (BOM)
			os.write(239);
		    os.write(187);
		    os.write(191);
			output = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
			output.append("userID");
			output.append(",");
			output.append("bonusEndDate");
			output.append(",");
			output.append("userFirstName");
			output.append(",");
			output.append("bonusAmount");
			output.append(",");
			output.append("sumInvQualify");
			output.append(",");
			output.append("emailAddress");
			output.append(",");
			output.append("subjectTemplate");
			output.append(",");
			output.append("skinId");
			output.append("\n");
		} catch (Exception e) {
			errorStr = "There was a problem to initial your report " + e.toString();
			e.printStackTrace();
		}
        
        CURRENCY_ILS = JobUtil.getPropertyByFile("currency.ils");
        CURRENCY_USD = JobUtil.getPropertyByFile("currency.usd");
        CURRENCY_EUR = JobUtil.getPropertyByFile("currency.eur");
        CURRENCY_GBP = JobUtil.getPropertyByFile("currency.gbp");
        CURRENCY_TRY = JobUtil.getPropertyByFile("currency.try");
        CURRENCY_RUB = JobUtil.getPropertyByFile("currency.rub");
        CURRENCY_CNY = JobUtil.getPropertyByFile("currency.cny");
        CURRENCY_KRW = JobUtil.getPropertyByFile("currency.krw");
        templateSendTo = JobUtil.getPropertyByFile("send.to");
        reportSendTo = JobUtil.getPropertyByFile("report.send.to");
        subjectTemplate.put("iw", StringEscapeUtils.escapeCsv(JobUtil.getPropertyByFile("anyoption.subject.iw")));
        subjectTemplate.put("es", StringEscapeUtils.escapeCsv(JobUtil.getPropertyByFile("anyoption.subject.es")));
        subjectTemplate.put("fr", StringEscapeUtils.escapeCsv(JobUtil.getPropertyByFile("anyoption.subject.fr")));
        subjectTemplate.put("en", StringEscapeUtils.escapeCsv(JobUtil.getPropertyByFile("anyoption.subject.en")));
        subjectTemplate.put("tr", StringEscapeUtils.escapeCsv(JobUtil.getPropertyByFile("anyoption.subject.tr")));
        subjectTemplate.put("de", StringEscapeUtils.escapeCsv(JobUtil.getPropertyByFile("anyoption.subject.de")));
        subjectTemplate.put("ru", StringEscapeUtils.escapeCsv(JobUtil.getPropertyByFile("anyoption.subject.ru")));
        subjectTemplate.put("zh", StringEscapeUtils.escapeCsv(JobUtil.getPropertyByFile("anyoption.subject.zh")));
        subjectTemplate.put("it", StringEscapeUtils.escapeCsv(JobUtil.getPropertyByFile("anyoption.subject.it")));
        emailUrl = JobUtil.getPropertyByFile("email.url");
        emailUser = JobUtil.getPropertyByFile("email.user");
        emailPass = JobUtil.getPropertyByFile("email.pass");
        emailFromHM = new HashMap<Long, String>();
        emailFromHM.put(Skin.SKIN_ETRADER, JobUtil.getPropertyByFile("email.from.etrader"));
        emailFromHM.put(Skin.SKIN_CHINESE, JobUtil.getPropertyByFile("email.from.anyoption.zh"));
        emailFromHM.put((long) Skin.SKINS_DEFAULT, JobUtil.getPropertyByFile("email.from.anyoption"));
        templateFile = JobUtil.getPropertyByFile("template.path");
        bonusAmountPerCurrencyHM = new HashMap<Long, String>();
        bonusAmountPerCurrencyHM.put(ConstantsBase.CURRENCY_ILS_ID, JobUtil.getPropertyByFile("bonus.currency.ils"));
        bonusAmountPerCurrencyHM.put(ConstantsBase.CURRENCY_USD_ID, JobUtil.getPropertyByFile("bonus.currency.usd"));
        bonusAmountPerCurrencyHM.put(ConstantsBase.CURRENCY_EUR_ID, JobUtil.getPropertyByFile("bonus.currency.eur"));
        bonusAmountPerCurrencyHM.put(ConstantsBase.CURRENCY_GBP_ID, JobUtil.getPropertyByFile("bonus.currency.gbp"));
        bonusAmountPerCurrencyHM.put(ConstantsBase.CURRENCY_TRY_ID, JobUtil.getPropertyByFile("bonus.currency.try"));
        bonusAmountPerCurrencyHM.put(ConstantsBase.CURRENCY_RUB_ID, JobUtil.getPropertyByFile("bonus.currency.rub"));
        bonusAmountPerCurrencyHM.put(ConstantsBase.CURRENCY_CNY_ID, JobUtil.getPropertyByFile("bonus.currency.cny"));
        for (Long currencyId : bonusAmountPerCurrencyHM.keySet()) {
        	String amount = bonusAmountPerCurrencyHM.get(currencyId);
        	if (!CommonUtil.isParameterEmptyOrNull(amount)) {
        		bonusAmountPerCurrencyHM.put(currencyId, displayAmount(Long.valueOf(amount), currencyId, true));
        	}
        }
        
        // dynamic parameters //
        long chunkNumFrom = Long.valueOf(JobUtil.getPropertyByFile("chunk.num.from.id", "0"));
        long chunkNumTo = Long.valueOf(JobUtil.getPropertyByFile("chunk.num.to.id", "0"));
		isNextInvestOnUs = Boolean.valueOf(JobUtil.getPropertyByFile("is.next.invest.on.us", "false"));
		BONUS_TEMPLATE_FILE = JobUtil.getPropertyByFile("bonus.name.template.file");
		mailboxTemplateId = Long.valueOf(JobUtil.getPropertyByFile("mailbox.template.id", "0"));
		issueDay = Long.valueOf(JobUtil.getPropertyByFile("issue.day", "0"));
		issueMonth = Long.valueOf(JobUtil.getPropertyByFile("issue.month", "0"));
		issueMonth = issueMonth - 1; // Calendar month working with (-1) for each month.
		issueYear = Long.valueOf(JobUtil.getPropertyByFile("issue.year", "0"));
		issueHourOfDay = Long.valueOf(JobUtil.getPropertyByFile("issue.hour.of.day", "0"));
		issueMinute = Long.valueOf(JobUtil.getPropertyByFile("issue.minute", "0"));
		issueComment = JobUtil.getPropertyByFile("issue.comment");
        long bonusId = Long.valueOf(JobUtil.getPropertyByFile("bonus.id", "0"));
        Boolean isNeedInsertViewData = Boolean.valueOf(JobUtil.getPropertyByFile("is.need.insert.view.data", "false"));
        long sumDeposits = Long.valueOf(JobUtil.getPropertyByFile("sum.deposits", "0")); // if you want to insert a new sum deposits, change from zero
        if (isNeedInsertViewData) {
        	boolean res = insertUsersGrantBonus(bonusId);
        	if (!res) { //Failed to get SQL 
        		return;
        	}
        }
        ArrayList<String> users = selectUsersGrantBonus(chunkNumFrom, chunkNumTo, bonusId);
        
        ArrayList<String> errorUsers = new ArrayList<String>();
        supportPhone = new HashMap<Long, String>();
        isNeedToSendInternalMail = false; //send template by id we get from properties file.
        if (mailboxTemplateId == 0) {
			isNeedToSendInternalMail = true; //send template by query.
		}
        
        try{
        	Connection con = getConnection();
//        	//bonus withdraw is activated only for users that didn't use their bonus
        	//userIds are taken from sql inside the function
        	//insertBonusWithdraw(con);
        	
        	createSupportPhone(con);
        	if (mailboxTemplateId != 0) {
        		createTemplates(con, mailboxTemplateId);
        	} 
        	
        	for(String userIdStr:users){

		    	boolean isInsertBonus = true;
        		boolean res = false;
        		long userId = Long.parseLong(userIdStr);
        		String errMsg = "";

        		////////////////////////
        		// dynamic parameters //
        		////////////////////////
        		long bonusAmount = 0; // if want to take original amount from bouns insert 0
        		boolean isCheckUserLastBonus = false; // if you want to disable the check for wehther this user got bonus in the last 30 days… so… say false
        		long bonusDaysPeriod = 0; // if want to take default period from bouns insert 0
        		long wageringParameter = 0; // if want to take default wageringParameter from bouns insert 0
        		long roundedBalanceBonusAmount = 0; // if bonus shoud round up the users balace, change from zero
        		// for 'Instant next invest on us' bonus change to: sumDeposits = 0;
        		if (isNextInvestOnUs) {
        			sumDeposits = 0;
        		}

		   		UserBase user = new UserBase();
			   	try {
			   		UsersDAOBase.getByUserId(con, userId, user,true);
			    } catch (SQLException e) {
			    	errMsg = " getUserById Failed, error code " + e.getErrorCode();
			   		user = null;
			   	}

			    if (null != user){
			    	if (roundedBalanceBonusAmount > 0){
			    		bonusAmount = roundedBalanceBonusAmount - user.getBalance();

			    		if (bonusAmount <= 0){
			    			isInsertBonus = false;
			    			errMsg = " didnt get bonus due to negative nonus amount";
			    		}
			    	}

			    	if (isInsertBonus){
			    		res = insertBonusToUser(con,user,bonusId,bonusAmount,isCheckUserLastBonus,bonusDaysPeriod, wageringParameter, sumDeposits, isNeedToSendInternalMail);
			    		BonusUsers bonusUser = new BonusUsers();
                        bonusUser = BonusDAOBase.getLastBonusUserByBonusId(con, user.getId(), bonusId);
			    		if (res) {
			    			updateUsersGrantBonus(con, userId, bonusId, bonusUser.getId());
			    			long currencyId = user.getCurrencyId();
                            String sumInvQualify = displayAmount(bonusUser.getSumInvQualify(), currencyId, false);
                            String userBonusAmount = displayAmount(bonusUser.getBonusAmount(), currencyId, false);
			    			if (mailboxTemplateId != 0) {
			    				// Send internal mail
				    			MailBoxUser email = new MailBoxUser();
								email.setTemplateId(mailBoxTemplate.getId());
								email.setUserId(user.getId());
								email.setWriterId(ConstantsBase.WEB_CONST);
								email.setSenderId(mailBoxTemplate.getSenderId());
								email.setIsHighPriority(mailBoxTemplate.getIsHighPriority());
								email.setPopupTypeId(mailBoxTemplate.getPopupTypeId());
								email.setSubject(mailBoxTemplate.getSubject());
								HashMap<String,String> parameters = new HashMap<String, String>();
								parameters.put("sumInvQualify", sumInvQualify);
								parameters.put("bonusAmount", userBonusAmount);
								if (parameters != null) {
									email.setParameters(parameters);
								}
								MailBoxUsersDAOBase.insert(con, email);
			    			}
			    			
			    			long langId = SkinsDAOBase.getById(con, (int)user.getSkinId()).getDefaultLanguageId();
                            String langCode = LanguagesDAOBase.getCodeById(con, langId);
                            log.debug("skinId: " + user.getSkinId() + " , langId: " + langId + ", langCode: " + langCode);
                            Date date = bonusUser.getEndDate();
                            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                            long skinId = user.getSkinId();
                            if (skinId != Skin.SKIN_ETRADER) {
                                formatter = new SimpleDateFormat("dd.MM.yy");   // for AO format
                            }
                            String endDate = formatter.format(date);
                            USER_FIRST_NAME = user.getFirstName();
                            if (templateSendTo.equalsIgnoreCase("Live")){
                                USER_EMAIL = user.getEmail();
                            } else {
                                USER_EMAIL = templateSendTo;
                            }
                            String supportPhoneTxt = supportPhone.get(user.getCountryId());
                    		if (supportPhoneTxt == null || supportPhoneTxt.length() == 0) {
                    			supportPhoneTxt = "+44-2080997262"; // default
                    		}
                            
			    			isHaveData = true;
			    			output.append(String.valueOf(userId));
							output.append(",");
							output.append(String.valueOf(endDate));
							output.append(",");
							output.append(String.valueOf(USER_FIRST_NAME));
							output.append(",");
							output.append(StringEscapeUtils.escapeCsv(String.valueOf(userBonusAmount)));
							output.append(",");
							output.append(StringEscapeUtils.escapeCsv(String.valueOf(sumInvQualify)));
							output.append(",");
							output.append(String.valueOf(USER_EMAIL));
							output.append(",");
							output.append(subjectTemplate.get(langCode));
							output.append(",");
							output.append(String.valueOf(skinId));
							output.append("\n");
							
							//send external mail.
							if (user.getContactByEmail()) {
				    			if (!BONUS_TEMPLATE_FILE.equals("0")) {
				    				log.log(Level.DEBUG,"going to send " + BONUS_TEMPLATE_FILE + " emails ");
		                            sendEmail(user , BONUS_TEMPLATE_FILE, langCode, endDate, sumInvQualify, userBonusAmount, supportPhoneTxt);
		                            log.log(Level.DEBUG,"finished to send email.");
				    			}
	                        }
			    		}
			    	}
			    }

        		if (++conCounter == 500){
        			con.close();
            		con = getConnection();
            		conCounter = 0;
            		log.info("reset connection");
        		}

        		if (!res){
        			errorUsers.add(userIdStr + errMsg);
        		} else {
//	        		 if we want to change the action performed - just add the methods and put it here
	        		insertIssueForMail(userIdStr,con,user.getContactByEmail());
//	        		insertBonusDeposit(con,Long.parseLong(userIdStr));
        		}

        	}
        	con.close();
        }catch(SQLException se){
        	log.fatal("Exception during performing action :"+se.getMessage());
        	se.printStackTrace();
        } catch (Exception e) {
            log.fatal("Exception during performing action :"+e.getMessage());
            e.printStackTrace();
        }


        if (errorUsers.size() > 0){
        	log.log(Level.ERROR, "------------------------------------------");
        	log.log(Level.ERROR, "Users with Errors are:");
        	for (String userIdStr: errorUsers){
        		log.log(Level.ERROR, userIdStr);
        	}
        	log.log(Level.ERROR, "------------------------------------------");
        }
        
        try {
			output.flush();
			output.close();
			if (!isHaveData){
				errorStr = "No data was Found.";
			}
		} catch (IOException e) {
			errorStr = "There was a problem in processing your report " + e.toString();
			if (env.equals("live")){
				reportSendTo = reportSendTo + ";qa@etrader.co.il";
			}
		}
        if (errorStr != null) {
        	htmlBuffer = errorStr + "<br/><br/>";
        } else {
            attachment = new File(filePath);
            htmlBuffer = "The report is attached to this mail";
        }
        
        // set the server properties
        serverProperties = new Hashtable<String, String>();
        serverProperties.put("url", emailUrl);
        serverProperties.put("user", emailUser);
        serverProperties.put("pass", emailPass);
        serverProperties.put("auth","true");
        serverProperties.put("contenttype", "text/html; charset=UTF-8");
        // Set the email properties
        emailProperties = new Hashtable<String, String>();
        emailProperties.put("subject", filename);
        emailProperties.put("from",JobUtil.getPropertyByFile("email.from.anyoption"));
        emailProperties.put("to", reportSendTo);
        emailProperties.put("body", htmlBuffer);
        File []attachments={attachment};
        CommonUtil.sendEmail(serverProperties, emailProperties, attachments);
        if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Sending report...");
	    }

        if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Job completed.");
	    }
        
//        sendBonusReport();
    }

	/**
     * Add issue for users from the list
     *
     * @param user - userId
     * @param con -  connection
     */
public static void insertIssueForMail(String user,Connection con, boolean isContactByMail)throws SQLException{
	    Issue issue = new Issue();
	   	IssueAction issueAction = new IssueAction();

	   	issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_GENERAL));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issue.setUserId(Long.parseLong(user));
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		// Filling action fields
		issueAction.setChannelId("3"); //EMAIL
		if (isContactByMail){
			issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
		}else{
			issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_COMMENT));
		}
		issueAction.setWriterId(Writer.WRITER_ID_AUTO);
		issueAction.setSignificant(false);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.DATE,(int) issueDay);
		cal.set(Calendar.MONTH,(int) issueMonth);
		cal.set(Calendar.YEAR, (int) issueYear);
		cal.set(Calendar.HOUR_OF_DAY, (int) issueHourOfDay);
		cal.set(Calendar.MINUTE, (int) issueMinute);
		issueAction.setActionTime(cal.getTime());
		issueAction.setActionTimeOffset("GMT+00:00");
		issueAction.setComments(issueComment);
		try{
//			if (log.isEnabledFor(Level.INFO)) {
//		        log.log(Level.INFO, "insert issue for "+user);
//		    }
			issue.setUserId(Long.parseLong(user));
			issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
			// Insert Issue
			IssuesDAOBase.insertIssue(con, issue);

			issueAction.setIssueId(issue.getId());
			issueAction.setWriterId(Writer.WRITER_ID_AUTO);
			// Insert Issue action
			IssuesDAOBase.insertAction(con, issueAction);
		}catch(Exception e){
			log.fatal("Exception performing action on user :"+user);
			e.printStackTrace();
		}
//		finally{
//			con.commit();
//		}
	 }

/**
 * calculate how much to deposit for the bonus (100 shekels - balance)
 *
 * @param user - userId
 */
public static long calculateBonusAmount(long userId) throws SQLException{
	long bonusAmount = 0;
	Connection con = getConnection();
	String sql = "select balance from users where id=?";
	PreparedStatement ps = con.prepareStatement(sql);
	ps.setLong(1, userId);
	ResultSet rs = ps.executeQuery();
	NumberFormat numberFormat = NumberFormat.getInstance();
	numberFormat.setMaximumFractionDigits(2);
	if (rs.next()) {
		bonusAmount = 100*100-rs.getLong("BALANCE"); // only users with less then 100 shekels will be passed to the job
		reportLine.setUserId(String.valueOf(userId));
		reportLine.setBalanceBeforeDeposit(String.valueOf(numberFormat.format(rs.getLong("BALANCE")/(double)100)));
		reportLine.setDepositAmount(String.valueOf(numberFormat.format(bonusAmount/(double)100)));

	}
	rs.close();ps.close();
	con.close();
	return bonusAmount;
}
/**
 * get Currency obj by userId
 *
 * @param user - userId
 * @param con -  connection
 */
public static Currency getCurrencyByUserId(Connection con,long userId)throws SQLException{
	Currency currency = new Currency();
	long currencyId = 0;
	String sql = "select currency_id from users where id=?";

	PreparedStatement ps = con.prepareStatement(sql);
	ps.setLong(1, userId);
	ResultSet rs = ps.executeQuery();
	if(rs.next()){
		currencyId = rs.getLong("currency_id");
	}
	currency = CurrenciesDAOBase.getById(con, (int)currencyId);
	rs.close();ps.close();
	return currency;
}
/**
 * insert bonus deposit for the user
 *
 * @param user - userId
 * @param con -  connection
 */
public static void insertBonusDeposit(Connection con, long userId) throws SQLException {

	reportLine = new BonusReportLine();

	long bonusAmount = calculateBonusAmount(userId);
		try {

			Transaction tran = null;
			con.setAutoCommit(false);
			tran = new Transaction();
			tran.setAmount(bonusAmount);
			tran.setCurrency(getCurrencyByUserId(con,userId));
			tran.setComments("");//TODO - what comments?
			tran.setCreditCardId(null);
			tran.setDescription("log.balance.bonus.deposit");
			tran.setIp("IP NOT FOUND!");
			tran.setProcessedWriterId(Writer.WRITER_ID_AUTO);
			tran.setChequeId(null);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated("GMT+00:00");
			tran.setUtcOffsetSettled("GMT+00:00");
			tran.setTypeId(12); // TRANS_TYPE_BONUS_DEPOSIT
			tran.setUserId(userId);
			tran.setWriterId(Writer.WRITER_ID_AUTO);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(2);//TRANS_STATUS_SUCCEED
			tran.setReceiptNum(null);

			UsersDAOBase.addToBalance(con, userId, bonusAmount);
			TransactionsDAOBase.insert(con, tran);
			GeneralDAO.insertBalanceLog(con, Writer.WRITER_ID_AUTO,
					tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran
							.getId(), ConstantsBase.LOG_BALANCE_BONUS_DEPOSIT, "GMT+00:00");
			con.commit();

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, "Bonus deposit inserted successfully!");
			}
		bonusReportData.add(reportLine);

	} catch (SQLException e) {
		log.log(Level.ERROR, "insertBonusDeposit: ", e);
		try {
			con.rollback();
		} catch (SQLException ie) {
			log.log(Level.ERROR, "Can't rollback.", ie);
		}
		throw e;
	} finally {
		con.setAutoCommit(true);
	}
}

private static void sendBonusReport(){
	HashMap<String, Object> params = new HashMap<String, Object>();
	params.put("reportData",bonusReportData);
	emailProperties = new Hashtable<String, String>();
	serverProperties = new Hashtable<String, String>();
	serverProperties.put("auth", "true");
	serverProperties.put("url",JobUtil.getPropertyByFile("email.url"));
	serverProperties.put("user",JobUtil.getPropertyByFile("email.user"));
	serverProperties.put("pass",JobUtil.getPropertyByFile("email.pass"));
	serverProperties.put("contenttype","text/html;charset=utf-8;");

	try {
		mailTemplate = getReportEmailTemplate();

	} catch (Exception e) {
		log.fatal("!!! ERROR >> INIT report email: " + e.getMessage());
	}
	emailProperties.put("subject",JobUtil.getPropertyByFile("bonusReport.subject"));
	try {
		emailProperties.put("body",getEmailBody(params));
	} catch (Exception e) {
		log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
	}

	emailProperties.put("to",JobUtil.getPropertyByFile("bonusReport.sendTo"));
	emailProperties.put("from",JobUtil.getPropertyByFile("bonusEmail.from"));
	JobUtil.sendEmail(serverProperties,emailProperties);
}

public static String getEmailBody(HashMap params) throws Exception {
	// set the context and the parameters
	VelocityContext context = new VelocityContext();
	// get all the params and add them to the context
	String paramName = "";
	log.debug("Adding parrams");
	for (Iterator keys = params.keySet().iterator(); keys.hasNext();) {
		paramName = (String) keys.next();
		context.put(paramName,params.get(paramName));
	}
	StringWriter sw = new StringWriter();
	emailTemplate.merge(context,sw);
	return sw.toString();
}
private static Template getReportEmailTemplate() throws Exception {
	try {
		Properties props = new Properties();
		props.setProperty("file.resource.loader.path","C:/DimaDev/head/etrader_common/src/actionFromCVSJob");
		//props.setProperty("file.resource.loader.path","/usr/local/successMailJob/");
		props.setProperty("resource.loader","file");
		props.setProperty("file.resource.loader.class","org.apache.velocity.runtime.resource.loader.FileResourceLoader");
		props.setProperty("file.resource.loader.cache","true");
		Velocity.init(props);
			return Velocity.getTemplate("bonusReport.html","UTF-8");
	} catch (Exception ex) {
		log.fatal("ERROR! Cannot find Report template : " + ex.getMessage());
		throw ex;
	}
}
static class ForcedAuthenticator extends Authenticator {
	public PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(EMAIL_USER,EMAIL_PASS);

	}
}

public static void insertBonusWithdraw(Connection con) throws SQLException {

	reportLine = new BonusReportLine();
	NumberFormat numberFormat = NumberFormat.getInstance();
	numberFormat.setMaximumFractionDigits(2);
	// sql gives us user IDs and the amount of bonus deposit
	String getUsersSql ="select t.user_id,t.amount from transactions t,users u " +
	" where "+
	" t.time_settled > to_date('21/06/2009 11:00:00','DD/MM/YYYY HH24:MI:SS') "+
	" and t.time_settled < to_date('21/06/2009 12:00:00','DD/MM/YYYY HH24:MI:SS') "+
	" and u.id=t.user_id and u.skin_id=1 "+
	" and t.type_id=12 and user_id not in ( "+
	" select t1.user_id from investments i,transactions t1 "+
	" where i.time_created > to_date('21/06/2009','DD/MM/YYYY') "+
	" and i.user_id=t1.user_id " +
	" and t1.type_id=12 " +
	" and t1.time_settled > to_date('21/06/2009 11:00:00','DD/MM/YYYY HH24:MI:SS') "+
	" and t1.time_settled < to_date('21/06/2009 12:00:00','DD/MM/YYYY HH24:MI:SS') "+
	" ) and t.user_id not in( "+
	"  select t2.user_id from transactions t2 where "+
	"  t2.type_id=13 and "+
	"  t2.time_settled > to_date('21/06/2009 11:00:00','DD/MM/YYYY HH24:MI:SS') "+
	" ) order by t.user_id ";
	Connection con2 = getConnection();
	Statement ps = con2.createStatement();

	ResultSet rs = ps.executeQuery(getUsersSql);
	while(rs.next()){
		long userId = rs.getLong("user_id");
		long bonusAmount = rs.getLong("amount");
		log.info(userId + " " + bonusAmount);
		try {
			Transaction tran = new Transaction();
			tran.setAmount(bonusAmount);
			tran.setCurrency(getCurrencyByUserId(con,userId));
			tran.setComments(""); // NEED COMMENTS
			tran.setCreditCardId(null);
			tran.setDescription("log.balance.bonus.withdraw");
			tran.setIp("IP NOT FOUND!");
			tran.setProcessedWriterId(Writer.WRITER_ID_AUTO);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated("GMT+00:00");
			tran.setUtcOffsetSettled("GMT+00:00");
			tran.setTypeId(13); // TRANS_TYPE_BONUS_WITHDRAW
			tran.setUserId(userId);
			tran.setWriterId(Writer.WRITER_ID_AUTO);
			tran.setStatusId(2); // TRANS_STATUS_SUCCEED
			tran.setChequeId(null);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);

			UsersDAOBase.addToBalance(con, userId, -(bonusAmount));
			TransactionsDAOBase.insert(con, tran);
			GeneralDAO.insertBalanceLog(con,
					Writer.WRITER_ID_AUTO, tran.getUserId(),
					ConstantsBase.TABLE_TRANSACTIONS, tran.getId(),
					ConstantsBase.LOG_BALANCE_BONUS_WITHDRAW, "GMT+00:00");
			con.commit();

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, "Bonus withdraw inserted successfully!");
			}
			reportLine = new BonusReportLine();
			reportLine.setDepositAmount(String.valueOf(numberFormat.format(bonusAmount/(double)100))+"");
			reportLine.setBalanceBeforeDeposit("100");
			reportLine.setUserId(userId+"");
			bonusReportData.add(reportLine);

		} catch (SQLException e) {
			log.log(Level.ERROR, "insertBonusWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
		}
	} // end of while on users
	ps.close();rs.close();con2.close();
}

	private static boolean insertBonusToUser(Connection con, UserBase user, long bonusId,long bonusAmount, boolean checkUserLastBonus,long bonusDaysPeriod, long wageringParameter, long sumDeposits, boolean isNeedToSendInternalMail){
		 boolean res = false;
		 boolean isUserShouldGetBonus = true;
		 long userId = user.getId();
		 BonusUsers bonusUser = new BonusUsers();

		 bonusUser.setBonusId(bonusId);
		 bonusUser.setUserId(userId);
		 bonusUser.setWriterId(Writer.WRITER_ID_AUTO);

		 if (sumDeposits > 0){
			 bonusUser.setSumDeposits(sumDeposits);
		 }

	     if (null != user && user.getId() > 0){

		     if (checkUserLastBonus){
		    	 // grant bonus to user only if he didn't got one in lomit period
		    	 BonusUsers lastUserBonus = null;

		    	 try {
					lastUserBonus = BonusDAOBase.getLastBonusForUser(con, user.getId());
				 } catch (SQLException e) {
					log.error("Error: problem with getting last bonus for user " + userId + e);
					return false;
				 }

		    	 if (null != lastUserBonus){
						Date lastUserBonusTime = lastUserBonus.getStartDate();

						GregorianCalendar gc = new GregorianCalendar();
						gc.add(Calendar.DATE, -ConstantsBase.BONUS_GRANT_LIMIT_PERIOD);
						Date grantBonusLimitDate = gc.getTime();

						if (lastUserBonusTime.after(grantBonusLimitDate)){
							isUserShouldGetBonus = false;
						}
		    	 }

			 } else {
				 isUserShouldGetBonus = true;
			 }

	    	 if (isUserShouldGetBonus){
	    		 try {
		     			Currency userCurrency = CurrenciesDAOBase.getById(con, user.getCurrencyId().intValue());
		     			user.setCurrency(userCurrency);
		     			bonusUser.setCurrency(userCurrency);

		     			res = insertBonusUser(bonusUser, user, Writer.WRITER_ID_AUTO, con,bonusAmount,bonusDaysPeriod, wageringParameter, isNeedToSendInternalMail);

		 	    	 } catch (SQLException e1) {
		 	    		log.error("Error: Granting bonus to user " + userId + " Failed." + e1);
		 	    		return false;
		 	    	 }
	    	 }else{
	    		 log.error("Error: user " + userId + " already got a bonus lately.");
	    		 return false;
	    	 }

	     }
	     if (res){
		   log.info("bonus " + i + " inserted for user " + userId);
		   ++i;
	     }else {
	       log.error("Error: Granting bonus to user " + userId + " Failed.");
	     }

	     return res;
	}


	 /**
     * Insert bonus to user
     * @param bUser BonusUser instnace
	 * @param wageringParameter TODO
	 * @param currencyId user currency
     * @return
     * @throws SQLException
     */
    public static boolean insertBonusUser(BonusUsers bUser, UserBase user,long writerId,Connection con,long bonusAmount, long bonusDaysPeriod, long wageringParameter, boolean isNeedToSendInternalMail) throws SQLException {
    	boolean res = false;

        	//  get Bonus data for insert
        	long bonusId = bUser.getBonusId();
			Bonus b = BonusDAOBase.getBonusById(con, bonusId);
			BonusCurrency bc = BonusDAOBase.getBonusCurrency(con,bonusId, user.getCurrencyId());

			if (0 != bonusAmount){
				bc.setBonusAmount(bonusAmount);
			}

			if (0 != bonusDaysPeriod){
				b.setDefaultPeriod(bonusDaysPeriod);
			}

			if (0 != wageringParameter){
				b.setWageringParameter(wageringParameter);
			}
			if (isNextInvestOnUs) {
				bUser.setSumDeposits(bc.getSumDeposits()); // we forced the SumDeposits to be taken from the bonus currency object
			}


			if (null != b && null != bc) {
				BonusHandlerBase bh = BonusHandlerFactory.getInstance(b.getTypeId());

				if (null != bh){
					try {
						con.setAutoCommit(false);
						res = bh.bonusInsert(con, bUser, b, bc, user.getId(), user.getCurrencyId(), writerId, 0, 0, isNeedToSendInternalMail);
						con.commit();

			    	} catch (Exception e) {
			    		log.error("Exception Insert new bonus id:" + b.getId() + " to user id: " + user.getId(), e);
						try {
							con.rollback();
						} catch (Throwable it) {
							log.error("Can't rollback.", it);
						}
			            SQLException sqle = new SQLException();
			            sqle.initCause(e);
			            throw sqle;
			    	} finally {
			            try {
			                con.setAutoCommit(true);
			            } catch (Exception e) {
			            	log.error("Can't set back to autocommit.", e);
			            }
					}
				}
			}

        return res;
    }
    
    public static void createTemplates(Connection con, long id) {
    	log.info("Going to create skin templates");
    	PreparedStatement pstmt = null;
    	ResultSet rs = null;
		try {		
			String sql = " SELECT " +
							" * " +
						 " FROM " +
						 	" MAILBOX_TEMPLATES " +
						 " WHERE " +
						 	" id = " + id +  " ";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				mailBoxTemplate = MailBoxTemplatesDAOBase.getVO(rs);
			}


		} catch (Exception e) {
			log.log(Level.ERROR, "Error getting mailBoxTemplate.", e);
	    } finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
	    }
    }
    
    /** Send Mail for the selected users list
     * @param users
     * @throws Exception
     */
    private static void sendEmailForUsersList(UserBase user, String htmlName, long sumBalance, String langCode, String endDate, BonusUsers bonusUsers) throws Exception {
        // put all users params
        HashMap<String, Object> params = new HashMap<String, Object>();
        try {
            initEmail(htmlName, user, langCode);
        } catch (Exception e) {
            log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
        }
        long currencyId = user.getCurrencyId();
        String sumInvQualify = displayAmount(bonusUsers.getSumInvQualify(), currencyId, false);
        String userBonusAmount = displayAmount(bonusUsers.getBonusAmount(), currencyId, false);
        USER_FIRST_NAME = user.getFirstName();
        if (JobUtil.getPropertyByFile("send.to").equalsIgnoreCase("Live")){
            USER_EMAIL = user.getEmail();
        } else {
            USER_EMAIL = JobUtil.getPropertyByFile("send.to");
        }
        params.put("bonusEndDate", endDate);
        params.put("userFirstName", USER_FIRST_NAME);
        params.put("bonusAmount", userBonusAmount);
        params.put("sumInvQualify", sumInvQualify);
        emailProperties.put("body", getEmailBody(params));
        emailProperties.put("to",USER_EMAIL);
        Utils.sendEmail(serverProperties,emailProperties);          
        emailProperties.remove("to");   
    }
    
    /**
     * Initialize Email configuration
     * @param name
     * @throws Exception
     */
    private static void initEmail(String name, UserBase user, String langCode) throws Exception {
        // get the emailTemplate
        emailTemplate = getEmailTemplate(name, user, langCode);

        // set the server properties
        serverProperties = new Hashtable<String, String>();
        serverProperties.put("url", emailUrl);
        serverProperties.put("user", emailUser);
        serverProperties.put("pass", emailPass);
        serverProperties.put("auth","true");
        // Set the email properties
        emailProperties = new Hashtable<String, String>();
        emailProperties.put("subject", subjectTemplate.get(langCode));
        long skinId = user.getSkinId();
        switch ((int) skinId) {
		case Skin.SKIN_ETRADER_INT:
			emailProperties.put("from", emailFromHM.get(Skin.SKIN_ETRADER));
			break;
		case (int) Skin.SKIN_CHINESE:
		case (int) Skin.SKIN_CHINESE_VIP:
			emailProperties.put("from", emailFromHM.get(Skin.SKIN_CHINESE));
			break;
		default:
			emailProperties.put("from", emailFromHM.get((long) Skin.SKINS_DEFAULT));
			break;
		}
    }
    
    /**
     * Get Email Template
     * @param fileName
     * @return
     * @throws Exception
     */
    private static Template getEmailTemplate(String fileName, UserBase user, String langCode) throws Exception {
        try {
            String templatePath = templateFile;
            templatePath += langCode + "_" + user.getSkinId() + "/" ;
            Properties props = new Properties();
            props.setProperty("file.resource.loader.path", templatePath);
            props.setProperty("resource.loader","file");
            props.setProperty("file.resource.loader.class","org.apache.velocity.runtime.resource.loader.FileResourceLoader");
            props.setProperty("file.resource.loader.cache","true");
            VelocityEngine v = new VelocityEngine();
            v.init(props);
            return v.getTemplate(fileName,"UTF-8");
        } catch (Exception ex) {
            log.fatal("ERROR! Cannot find template : " + ex.getMessage());
            throw ex;
        }
    }
    
    private static boolean insertUsersGrantBonus(long bonusId) {
		PreparedStatement ps = null;
		String fileName = JobUtil.getPropertyByFile("sql.file");
		String usersForBonus = fileToString(fileName);
		if (CommonUtil.isParameterEmptyOrNull(usersForBonus)) {
			log.fatal("Can't find SQL file");
			return false;
		}
    	try {
    		Connection conn = getConnection();
    		String sql="INSERT into USERS_GRANT_BONUS(id, user_id, is_grant_bonus, bonus_id, time_created, bonus_user_id) " +
	    					" SELECT " +
	    						" seq_users_grant_bonus.nextval, " +
	    						" users_for_bonus.id, " +
	    						" 0, " +
	    						" ?, " +
	    						" sysdate, " +
	    						" 0 " +
	    					" FROM " +
	    						" ( " + usersForBonus + " ) users_for_bonus ";
    		ps = conn.prepareStatement(sql);
    		ps.setLong(1, bonusId);
    		ps.executeUpdate();
    		conn.close();
    	} catch(SQLException se){
	    	log.fatal("Exception during performing action :", se);	    	
	    	se.printStackTrace();
	    	return false;
	    } catch (Exception e) {
	        log.fatal("Exception during performing action :", e);
	        e.printStackTrace();
	        return false;
	    } finally {
			closeStatement(ps);
		}
    	return true;
    }


    
    private static void updateUsersGrantBonus(Connection conn, long userId, long bonusId, long bonusUserId) {
		PreparedStatement ps = null;
    	try {
    		String sql="UPDATE " +
    					" users_grant_bonus " +
    				" SET " +
    					" is_grant_bonus = 1, " +
    					" bonus_user_id = ? " +
    				" WHERE " +
    					" bonus_id = ? " +
    					" AND user_id = ? " +
    					" AND bonus_user_id = 0 ";
    		ps = conn.prepareStatement(sql);
    		ps.setLong(1, bonusUserId);
    		ps.setLong(2, bonusId);
			ps.setLong(3, userId);
    		ps.executeUpdate();
    	} catch(SQLException se){
	    	log.fatal("Exception during performing action :"+se.getMessage());
	    	se.printStackTrace();
	    } catch (Exception e) {
	        log.fatal("Exception during performing action :"+e.getMessage());
	        e.printStackTrace();
	    } finally {
			closeStatement(ps);
		}
    }
    
    private static ArrayList<String> selectUsersGrantBonus(long chunkNumFrom,long chunkNumTo, long bonusId) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> users = new ArrayList<String>();
    	try {
    		Connection conn = getConnection();
    		String sql="SELECT " +
    					" * " +
    				" FROM " +
    					" users_grant_bonus " +
    				" WHERE " +
    					" is_grant_bonus = 0 " +
    					" AND bonus_id = ? ";
    					
    		if (chunkNumFrom != EMPTY_VAR && chunkNumTo != EMPTY_VAR) {
    			 sql += " AND id >= ? " +
    				    " AND id <= ? ";
    		}
    		ps = conn.prepareStatement(sql);
    		ps.setLong(1, bonusId);
    		if (chunkNumFrom != EMPTY_VAR && chunkNumTo != EMPTY_VAR) {
    			ps.setLong(2, chunkNumFrom);
        		ps.setLong(3, chunkNumTo);
    		}
    		rs = ps.executeQuery();
			while (rs.next()) {
				users.add(rs.getString("user_id"));
			}
    		conn.close();
    	} catch(SQLException se){
	    	log.fatal("Exception during performing action :"+se.getMessage());
	    	se.printStackTrace();
	    } catch (Exception e) {
	        log.fatal("Exception during performing action :"+e.getMessage());
	        e.printStackTrace();
	    } finally {
			closeStatement(ps);
		}
    	return users;
    }
    
    /**
	 * Display amount with currency
	 * @param amount
	 * @return
	 */
	public static String displayAmount(long amount, long currencyId, boolean isDisplayWithoutDecimalPoint) {
		Currency currency = null;
		try {
			Connection conn = getConnection();
			currency = CurrenciesDAOBase.getById(conn, (int)currencyId);
			conn.close();
		} catch (Exception e) {
	        log.fatal("Exception during performing action :"+e.getMessage());
	        e.printStackTrace();
	    }
		return displayAmount(amount, true, currency, isDisplayWithoutDecimalPoint);
	}
	
	/**
	 * DisplayAmount function
     *
	 * @param amount amount to display
	 * @param showCurrency if needed to show currency symbol
	 * @param currency currency instance
	 * @return
	 */
	public static String displayAmount(long amount, boolean showCurrency, Currency currency, boolean isDisplayWithoutDecimalPoint) {
		String amountFormat = CurrenciesManagerBase.getAmountFormat(currency, false);
		if (isDisplayWithoutDecimalPoint) {
			amountFormat = amountFormat.substring(0, amountFormat.length() - 3);
		}
		DecimalFormat sd = new DecimalFormat(amountFormat);
		double amountDecimal = amount;
		String symbol = "";
		amountDecimal /= 100;
		String out = "";
		if (showCurrency) {
			symbol = getCurrencySymbol(currency.getId());
			if (currency.getIsLeftSymbolBool()) {
				out = symbol + sd.format(amountDecimal);
			} else {
				out = sd.format(amountDecimal) + symbol;
			}
		} else {
			out = sd.format(amountDecimal);
		}
		return out;
	}
	
	public static String getCurrencySymbol(long currencyId) {
		String curr = ConstantsBase.EMPTY_STRING;
		switch ((int)currencyId) {
		case (int)ConstantsBase.CURRENCY_ILS_ID:
			curr = CURRENCY_ILS;
			break;
		case (int)ConstantsBase.CURRENCY_USD_ID:
			curr = CURRENCY_USD;
			break;
		case (int)ConstantsBase.CURRENCY_EUR_ID:
			curr = CURRENCY_EUR;
			break;
		case (int)ConstantsBase.CURRENCY_GBP_ID:
			curr = CURRENCY_GBP;
			break;
		case (int)ConstantsBase.CURRENCY_TRY_ID:
			curr = CURRENCY_TRY;
			break;
		case (int)ConstantsBase.CURRENCY_RUB_ID:
			curr = CURRENCY_RUB;
			break;
        case (int)ConstantsBase.CURRENCY_CNY_ID:
            curr = CURRENCY_CNY;
            break;
        case (int)ConstantsBase.CURRENCY_KRW_ID:
            curr = CURRENCY_KRW;
            break;
		default:
			break;
		}
		return curr;
	}
	
	/**
	 * Send Mail to user
	 * @param user
	 * @param htmlName
	 * @param langCode
	 * @param endDate
	 * @param sumInvQualify
	 * @param userBonusAmount
	 * @throws Exception
	 */
    private static void sendEmail(UserBase user, String htmlName, String langCode, String endDate, String sumInvQualify, String userBonusAmount, String supportPhoneTxt) throws Exception {
        // put all user params
        HashMap<String, Object> params = new HashMap<String, Object>();
        try {
            initEmail(htmlName, user, langCode);
        } catch (Exception e) {
            log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
        }
        long currencyId = user.getCurrencyId();
        params.put("bonusEndDate", endDate);
        params.put("userFirstName", USER_FIRST_NAME);
        params.put("bonusAmount", userBonusAmount);
        params.put("sumInvQualify", sumInvQualify);
        params.put("currency", getCurrencySymbol(currencyId));
        params.put("userName", user.getUserName());
        params.put("supportPhone", supportPhoneTxt);
        params.put("userLastName", user.getLastName());
        params.put("bonusAmountPerCurrency", bonusAmountPerCurrencyHM.get(currencyId));
        emailProperties.put("body", getEmailBody(params));
        emailProperties.put("to",USER_EMAIL);
        Utils.sendEmail(serverProperties,emailProperties);          
        emailProperties.remove("to");   
    }
    
    /**
     * create support phone by country
     * @param con
     */
    private static void createSupportPhone(Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;		
		try {			
			String sql =" SELECT " +
							" * " +
						" FROM " +
							" countries ";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				supportPhone.put(new Long(rs.getLong("id")), rs.getString("SUPPORT_PHONE"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get countries " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}			
		}		
	}
}

