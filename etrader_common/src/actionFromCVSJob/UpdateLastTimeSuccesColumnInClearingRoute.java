package actionFromCVSJob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.bl_vos.ClearingRoute;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.JobUtil;

public class UpdateLastTimeSuccesColumnInClearingRoute extends JobUtil {

	/**
	 * @author oshikl
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		
		propFile = args[0];
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		ArrayList<ClearingRoute> list =  new ArrayList<ClearingRoute>();
		String sql1 = " SELECT cr.id as cr_id , s.last_success as time_created , s.bin " +
					  " FROM (select max(t.time_created) as last_success,  cc.bin ,t.clearing_provider_id " +
					  " FROM transactions t , credit_cards cc " +
					  " WHERE t.type_id = 1 " +
					  " AND t.is_rerouted != 1 " +
					  " AND t.status_id in ( 2 , 7 ) " +
					  " AND t.credit_card_id = cc.id " +
					  " GROUP BY cc.bin , t.clearing_provider_id ) s, clearing_routes cr " +
					  " WHERE s.bin = cr.start_bin " +
					  " AND cr.deposit_clearing_provider_id = s.clearing_provider_id "; 
//				
//				" SELECT  max (t.time_created) as time_created, cc.bin  , min(cr.id) as cr_id " +
//					  " FROM transactions t , credit_cards cc, clearing_routes cr " +
//					  " WHERE " +
//					  " t.type_id = 1  and cc.bin = cr.start_bin " +
//					  " and t.credit_card_id = cc.id  and t.status_id in( 2 , 7 ) and t.is_rerouted != 1 " +
//					  " GROUP BY cc.bin";
		
		String sql2 = " SELECT  max(  t.time_created ) as time_created , min(cc.bin) as bin  , cr.id as cr_id " +
					  " FROM transactions t , credit_cards cc , clearing_routes cr, users u " +
					  " WHERE t.type_id = 1 and t.credit_card_id = cc.id " +
					  " and ( cr.country_id is null or   cr.country_id = cc.country_id  ) " +
					  " and   ( cr.currency_id is null or  cr.currency_id = u.currency_id) " +
					  " and ( cr.cc_type is null or cr.cc_type = cc.type_id ) " +
					  " and cr.start_bin is null " +
					  " and t.user_id = u.id and t.status_id in( 2 , 7 ) " +
					  " and t.is_rerouted != 1 " +
					  " GROUP BY cr.id ";
		
		String sqlUpdate = " UPDATE clearing_routes SET time_last_success = ? WHERE id = ? ";
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql1);
			rs = ps.executeQuery();
			while (rs.next()){
				ClearingRoute cr = new ClearingRoute();
				cr.setId(rs.getLong("cr_id"));
				cr.setTimelastSuccess(rs.getDate("time_created"));			
				list.add(cr);				
				}
			} finally {
				conn.close();
			}
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql2);
			rs = ps.executeQuery();
			while (rs.next()){
				ClearingRoute cr = new ClearingRoute();
				cr.setId(rs.getLong("cr_id"));
				cr.setTimelastSuccess(rs.getDate("time_created"));			
				list.add(cr);				
				}
			} finally {
				conn.close();
			}
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sqlUpdate);
			
			for(ClearingRoute cr : list){
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(cr.getTimelastSuccess()));
				ps.setLong(2, cr.getId());
				ps.addBatch();
			}
			ps.executeBatch(); 
							
		} finally {			
			ps.close();
			rs.close();
			conn.close();
			System.out.println("Job Finished");
		}		
		}
	}
