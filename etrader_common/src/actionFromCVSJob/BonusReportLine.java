package actionFromCVSJob;

public class BonusReportLine {
	private String userId;
	private String depositAmount;
	private String balanceBeforeDeposit;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDepositAmount() {
		return depositAmount;
	}
	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}
	public String getBalanceBeforeDeposit() {
		return balanceBeforeDeposit;
	}
	public void setBalanceBeforeDeposit(String balanceBeforeDeposit) {
		this.balanceBeforeDeposit = balanceBeforeDeposit;
	}

}
