package email.job;

import il.co.etrader.util.JobUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.util.CommonUtil;

/**
 * @author liors
 *
 */
class Program extends JobUtil {
	private static Logger log = Logger.getLogger(Program.class);	

	public static void main(String[] args) {
		// take propertis file
		if (args.length != 1 || args == null) {
			log.error(Util.LOG_PREFIX_MSG
					+ "ERROR! file not found. Proper Usage is: java program filename.");
			return;
		}
		log.info(Util.LOG_PREFIX_MSG + " START reset password JOB.");		
		propFile = args[0];
		
		// create lists
		Connection con = null;
        try {
        	con = getConnection();        	
        	Util.createSupportPhone(con); 
        	Util.createSupportEmail(con);
        } catch (Exception e) {
        	log.error(Util.LOG_PREFIX_MSG
					+ " ERROR! cant create support phones");         	
        } finally {
			BaseBLManager.closeConnection(con);
		}
		
        // get additional information for template        
		String templateName = getPropertyByFile("template.name");				
		ArrayList<User> users = new ArrayList<User>();				
		if (!CommonUtil.isParameterEmptyOrNull(templateName)) {
			Connection connection = null;
			try {
				connection = getConnection();
				String sqlFileName = JobUtil.getPropertyByFile("sql.file");
				String sql = fileToString(sqlFileName); 
				if (CommonUtil.isParameterEmptyOrNull(sql)) {
					log.fatal("Can't find SQL file");
					return;
				}				
				users = Util.getUsers(connection, sql);				
			} catch (SQLException e) {
				log.error(Util.LOG_PREFIX_MSG
						+ "ERROR! SQL exception get users ", e);   		
			} catch (Exception e) {
				log.error(Util.LOG_PREFIX_MSG
						+ "ERROR! exceptions get users ", e);   
			} finally {
				BaseBLManager.closeConnection(connection);
			}
		}
		
		try {
			Util.sendEmails(users, templateName);
		} catch (Exception e) {
			log.error(Util.LOG_PREFIX_MSG
					+ "ERROR! exceptions send emails ", e);			
		}
		
		log.info(Util.LOG_PREFIX_MSG + " END reset password JOB.");
		
	}
}
