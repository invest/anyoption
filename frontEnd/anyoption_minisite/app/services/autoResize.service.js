/* service::autoResizeService */
(function() {
	'use strict';

	angular.module('commonServices').service('autoResizeService', autoResizeService);

	// autoResizeService.$inject = [];
	function autoResizeService() {
		var _this = this;
		_this.elements = [];
		
		_this.register = register;
		_this.unregister = unregister;
		_this.clean = clean;
		_this.onResizeRegister = onResizeRegister;
		_this.onResizeRegister();
		
		
		function register(el) {
			var index = _this.elements.indexOf(el);
			
			if (index == -1) {
				_this.elements.push(el);
				onResize();
			}
		}
		
		function unregister(el) {
			var index = _this.elements.indexOf(el);
			
			if (index > -1) {
				_this.elements.splice(index, 1);
			}
		}
		
		function clean() {
			_this.elements.length = 0;
		}
	
		function onResizeRegister() {
			$(window).resize(function() {onResize();});
		}
		
		function onResize() {
			//this timeout is for the auto scroll on keyboard show problem - it was scrolling too much
			setTimeout(function() {
				if (_this.elements.length > 0) {
					var $window = $(window);
					var viewport_height = $window.height();
					var viewport_width = $window.width();
					
					$.each(_this.elements, function(index) {
						this.height(viewport_height + this.offsetHeight);
						this.width(viewport_width + this.offsetWidth);
					})
				}
			}, 300)
		}
	}
})();