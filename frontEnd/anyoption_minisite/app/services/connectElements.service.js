/* service::connectElementsService */
(function() {
	'use strict';

	angular.module('commonServices').service('connectElementsService', connectElementsService);

	//connectElementsService.$inject = [];
	function connectElementsService() {
		var _this = this;
		_this.elements = [];
		
		_this.register = register;
		_this.unregister = unregister;
		_this.clean = clean;
		_this.onResizeRegister = onResizeRegister;
		_this.onResizeRegister();
		_this.calcSVG = calcSVG;
		
		
		function register(el) {
			var index = _this.elements.indexOf(el);
			
			if (index == -1) {
				_this.elements.push(el);
				setTimeout(function() {
					_this.calcSVG(el);
				},500)
			}
		}
		
		function unregister(el) {
			var index = _this.elements.indexOf(el);
			
			if (index > -1) {
				_this.elements.splice(index, 1);
			}
		}
		
		function clean() {
			_this.elements.length = 0;
		}
	
		function onResizeRegister() {
			$(window).resize(function() {onResize();});
		}
		
		function onResize() {
			if (_this.elements.length > 0) {
				for (var i = _this.elements.length - 1; i >= 0; i--) {
					_this.calcSVG(_this.elements[i]);
				}
			}
		}
		
		function calcSVG(element) {
			element.find('._svg-selector').remove();
			
			var circleRadius = 30;
			var borderWidth = 3;
			var arrowWidth = 30;
			var arrowHeight = 20;
			var arrowIn = 8;
			var minSVGWidth = 60;
			var arrowOffset = 20;
			
				
			var fromEl = element.find('.'+element.from);
			var toEl = element.find('.'+element.to);
			
			var toElTop = toEl.offset().top + (toEl.height() / 2);
			var fromElTop = fromEl.offset().top + (fromEl.height() / 2);
			var svgHeight = toElTop - fromElTop + (arrowHeight / 2) + borderWidth;
			
			var svgWidth = toEl.offset().left - fromEl.offset().left;
			var widthDiff = svgWidth;
			var svgLeft = 0;
			if (element.dir != 'reverse') {
				if (svgWidth < 0) {//to element is smoller
					svgWidth *= -1;
					svgLeft = fromEl.offset().left - svgWidth;
				} else {
					svgLeft = toEl.offset().left - svgWidth;
				}
				svgWidth += minSVGWidth;
				svgLeft -= minSVGWidth;
			} else {
				if (svgWidth < 0) {//to element is smoller
					svgWidth *= -1;
					svgLeft = toEl.offset().left + toEl.width();
				} else {
					svgLeft = fromEl.offset().left + fromEl.width();
				}
				svgLeft -= svgWidth;
				svgWidth += minSVGWidth;
			}
			
			var svg = $('<svg class="_svg-selector svg-dashed-el ' + element.dir + '" width="' + svgWidth + 'px" height="'+ svgHeight +'px" xmlns="http://www.w3.org/2000/svg"></svg');
			svg.css({top: fromElTop, left: svgLeft})
			
			var shapeLine = document.createElementNS("http://www.w3.org/2000/svg", "path");
			shapeLine.setAttribute('class','_svg-selector-line svg-shape-line');
			shapeLine.setAttributeNS(null, "stroke-dasharray", "10, 10");
			svg.append(shapeLine);
			
			
			//start
			var start_x = svgWidth;
			var start_y = borderWidth;
			if (widthDiff > -0) {
				start_x -= widthDiff;
			}
			var line = "M" + start_x + " " + start_y;
			
			//line to
			var line_1_x = circleRadius + borderWidth;
			var line_1_y = borderWidth;
			if (start_x > line_1_x) {
				line += " L" + line_1_x + " " + line_1_y;
			}
			
			//1/4 circle
			var circle_1_start_x = line_1_x;
			var circle_1_start_y = line_1_y;
			var circle_1_end_x = line_1_x - circleRadius;
			var circle_1_end_y = line_1_y + circleRadius;
			var circle_1_middle_x = circle_1_end_x;
			var circle_1_middle_y = circle_1_start_y;
			line += " C " + circle_1_start_x + " " + circle_1_start_y + ", " + circle_1_middle_x + " " + circle_1_middle_y + ", " + circle_1_end_x + " " + circle_1_end_y + " ";
			
			//line to
			var line_2_x = borderWidth;
			var line_2_y = svgHeight - (circleRadius + borderWidth + (arrowHeight/2));
			if (line_2_y > circle_1_end_y) {
				line += " L" + line_2_x + " " + line_2_y;
			}
			
			//1/4 circle
			var circle_2_start_x = line_2_x;
			var circle_2_start_y = line_2_y;
			var circle_2_end_x = line_2_x + circleRadius;
			var circle_2_end_y = line_2_y + circleRadius;
			var circle_2_middle_x = line_2_x;
			var circle_2_middle_y = circle_2_end_y;
			line += " C " + circle_2_start_x + " " + circle_2_start_y + ", " + circle_2_middle_x + " " + circle_2_middle_y + ", " + circle_2_end_x + " " + circle_2_end_y + " ";
			
			//line to
			var line_3_x = svgWidth - arrowWidth + arrowIn - arrowOffset;
			var line_3_y = circle_2_end_y;
			if (widthDiff < 0) {
				line_3_x += widthDiff;
			}
			if (line_3_x > circle_2_end_x) {
				line += " L" + line_3_x + " " + line_3_y;
			} else {
				line_3_x = circle_2_end_x;
			}
			
			shapeLine.setAttributeNS(null, "d", line);
			
			
			var shapeArrow = document.createElementNS("http://www.w3.org/2000/svg", "path");
			shapeArrow.setAttribute('class','svg-shape-arrow');
			svg.append(shapeArrow);
			
			//start
			var arrow_start_x = line_3_x;
			var arrow_start_y = line_3_y;
			var line = "M" + arrow_start_x + " " + arrow_start_y;
			
			//line to
			var arrow_line_1_x = arrow_start_x - arrowIn;
			var arrow_line_1_y = arrow_start_y - (arrowHeight/2);
			line += " L" + arrow_line_1_x + " " + arrow_line_1_y;
			
			//line to
			var arrow_line_2_x = arrow_line_1_x + arrowWidth;
			var arrow_line_2_y = arrow_line_1_y + (arrowHeight/2);
			line += " L" + arrow_line_2_x + " " + arrow_line_2_y;
			
			//line to
			var arrow_line_3_x = arrow_line_2_x - arrowWidth;
			var arrow_line_3_y = arrow_line_2_y + (arrowHeight/2);
			line += " L" + arrow_line_3_x + " " + arrow_line_3_y;
			
			//line to
			line += " L" + arrow_start_x + " " + arrow_start_y + " Z";
			
			shapeArrow.setAttributeNS(null, "d", line);
			
			
			element.append(svg);			
		}
	}
})();