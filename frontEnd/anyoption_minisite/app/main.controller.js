/* controller::MainController */
(function() {
	'use strict';

	angular.module('mainApp').controller('MainController', MainController);

	MainController.$inject = ['utilsService', 'redirectService', '$state'];
	function MainController(utilsService, redirectService, $state) {
		var _this = this;
		_this._u = utilsService;
		_this.getMsg = utilsService.getMsg;
		_this.getSkin = _this._u.getSkin;
		
		_this.disclaimer = _this._u.getMsg('disclaimer', {_noTrust: true, 'clickEvent': 'mCtrl._u.openGeneralTerms(3)'});
		_this.faq_trading_text11 = _this._u.getMsg('faq.trading.text11', {_noTrust: true, 'clickEvent': 'mCtrl._u.openGeneralTerms(1)'});
		_this.faq_trading_text12 = _this._u.getMsg('faq.trading.text12', {_noTrust: true, 'clickEvent': 'mCtrl._u.openGeneralTerms(1)'});
		_this.faq_trading_text17 = _this._u.getMsg('faq.trading.text17', {_noTrust: true, 'clickEvent': 'mCtrl._u.openGeneralTerms(1)'});
		_this.faq_deposits_withdrawals_text1 = _this._u.getMsg('faq.deposits-withdrawals.text1', {_noTrust: true, 'ui-sref-link': {link: 'ln.banking'}});
		_this.faq_deposits_withdrawals_text4 = _this._u.getMsg('faq.deposits-withdrawals.text4', {_noTrust: true, 'ui-sref-link': {link: 'ln.banking'}});
		_this.faq_my_account_test3 = _this._u.getMsg('faq.my-account.test3', {_noTrust: true, 'ui-sref-link': {link: 'ln.banking'}});

		_this.counterItUp = counterItUp;
		_this.headerScroll = headerScroll;
		_this.dashIt = dashIt;
		_this.openGeneralTerms = _this._u.openGeneralTerms;

		
		function counterItUp() {
			$('._counter').counterUp({
				delay: 10,
				time: 1000
			});
		}
		
		function headerScroll(viewport_top) {
			var $header = $('header');
			if (viewport_top > 0) {
				$header.addClass('no-tranaparent');
			} else {
				$header.removeClass('no-tranaparent');
			}
		}
		
		function dashIt($element) {
			var path = $element.find('._svg-selector-line').get(0);
			drawPath(path, {
				duration: 2000
			});
		}
		
		$(window).focus(function() {
			console.log($state);
			if ($state.current.name == "ln.how-to-download-app") {
				location.reload();
			}
		});
	}
})();