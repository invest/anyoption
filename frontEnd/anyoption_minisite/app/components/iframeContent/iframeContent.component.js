/*component::IframeContentController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('iframeContent', {
			templateUrl: 'app/components/iframeContent/iframeContent.component.html',
			controller: IframeContentController,
			bindings: {
				info: '<',
				onResolve: '&',
				onReject: '&'
			}
		});
		
	IframeContentController.$inject = ['utilsService'];
	function IframeContentController(utilsService) {
		var _this = this;
		_this._u = utilsService;

		_this.$onInit = function() {
			$('#the-frame').attr({'srcdoc': _this.info.form});
			
			checkIframe();
		}
		
		function checkIframe() {
			var iframeUrl = '';
			try{
				iframeUrl = $('#the-frame').contents().get(0).location.href;
			} catch(e){}
			
			if (iframeUrl.toLowerCase().indexOf(_this.info.checkFor) >= 0) {
				_this.onResolve({data: getUrlValues(iframeUrl)});
			} else {
				setTimeout(function() {
					checkIframe();
				},500);
			}
		}
	}
})();