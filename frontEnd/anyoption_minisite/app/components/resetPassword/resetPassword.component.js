/*component::ResetPasswordController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('resetPassword', {
			templateUrl: 'app/components/resetPassword/resetPassword.component.html',
			controller: ResetPasswordController,
			bindings: {
				onLogin: '&',
				info: '<'
			}
		});
		
	ResetPasswordController.$inject = ['utilsService', 'userService'];
	function ResetPasswordController(utilsService, userService) {
		var _this = this;
		_this._u = utilsService;
		
		var AOETUsername = getAOCookie('AOETUsername');
		if (!isUndefined(AOETUsername)) {
			AOETUsername = AOETUsername.replace(/\"/g, '');
		}
		_this.form = {
			userName: AOETUsername,
			remember: true
		}
		
		_this.submit = submit;
		
		
		function submit(_form) {
//			userService.login(_this, _form)
//				.then(function(data) {
//					_this.onLogin({
//						data: data
//					})
//				})
		}
	}
})();