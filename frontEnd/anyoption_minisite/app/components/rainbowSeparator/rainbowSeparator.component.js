(function() {
	'use strict';
	
	angular.module('mainApp')
		.component('rainbowSeparator', {
			templateUrl: 'app/components/rainbowSeparator/rainbowSeparator.component.html',
		});
})();
