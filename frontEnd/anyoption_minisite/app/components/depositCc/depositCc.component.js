
/*component::DepositCcController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('depositCcForm', {
			templateUrl: 'app/components/depositCc/depositCc.component.html',
			controller: DepositCcController,
			bindings: {
				onDeposit: '&'
			}
		});
		
	DepositCcController.$inject = ['utilsService', 'depositCcService', 'skinCurrenciesService', 'userService', 'popUpService'];
	function DepositCcController(utilsService, depositCcService, skinCurrenciesService, userService, popUpService) {
		this.$onInit = function() {
			userService.checkFirstDeposit()
				.then(function(data) {
					_this.isFirstDeposit = data;
				})

			skinCurrenciesService.get()
				.then(function(data) {
					_this.currencies = data.currencies;
					_this.predefinedAmounts = data.predefinedDepositAmounts;
					
					_this.formSelect.curency = _this.currencies[searchJsonKeyInArray(_this.currencies, 'id', _this._u.getCurrency().id)];
					_this.form.amount = data.predefinedDepositAmounts[_this._u.getCurrency().id];
				})
		}
		
		var _this = this;
		
		_this._u = utilsService;
		
		_this.form = {}
		
		//some cool stuff are going on here
		if (isLocal) {_this.form = _this._u.defaultDepositLocal();}
		
		_this.formSelect = {
			curency: {}
		}
		
		_this.isFirstDeposit = true;
		
		_this.shortYear = _this._u.settings.shortYear;
		_this.shortYearFuture = _this._u.settings.shortYearFuture;
		_this.yearMinus18 = new Date().getFullYear() - 18;
		
		var popupProperties = {
			success: {
				contentClass: 'successful-deposit',
				headerText: _this._u.getMsg('successful-deposit'),
				bodyTextList: [
					{
						text: _this._u.getMsg('popUp-successful-deposit')
					}, {
						className: 'important-text',
						text: ''
					}
				],
				button: {
					className: 'blue',
					text: _this._u.getMsg('start-trading')
				},
				footer: {
					text: _this._u.getMsg('back-to-site')
				}
			},
			unsuccess: {
				contentClass: 'unsuccessful-deposit',
				headerText: _this._u.getMsg('unsuccessful-deposit'),
				bodyTextList: [{text: _this._u.getMsg('popUp-unsuccessful-deposit')}],
				button: {
					className: 'blue',
					text: _this._u.getMsg('try-again')
				},
				footer: {
					text: _this._u.getMsg('contact-us')
				}
			},
			processing: {
				contentClass: 'processing-deposit',
				headerText: _this._u.getMsg('popUp-processing'),
				bodyTextList: [{text: _this._u.getMsg('please-wait')}],
			}
		}
		
		_this.getUser = userService.getUser;
		
		_this.updateDefaultDepositAmount = updateDefaultDepositAmount;
		_this.submit = submit;
		_this.openPopup = openPopup;
		
		
		function updateDefaultDepositAmount(_form) {
			if (!_form.amount.$touched) {
				_this.form.amount = _this.predefinedAmounts[_this.formSelect.curency.id];
			}
		}
		
		function submit(_form) {
			_this.openPopup('processing');
			
			depositCcService.insertFirstDeposit(_this, _form)
				.then(function(data) {
					afterDeposit(data);
				})
				.catch(function(data) {
					popUpService.close('processing');
					if (!$.isEmptyObject(data)) {
						_this.openPopup('unsuccess');
					}
				})
		}
		
		function afterDeposit(data) {
			popUpService.close('processing');
			if (data.clearingProvider && data.clearingProvider != '') {
				popupProperties.success.bodyTextList[0].text = _this._u.getMsg('popUp-successful-deposit-provider', {provider: data.clearingProvider});
			}
			
			popupProperties.success.bodyTextList[1].text = _this._u.getMsg('transaction-id') + ' ' + data.transactionId;
			_this.openPopup('success');
			
			_this.onDeposit({
				data: data
			})
		}

		function openPopup(type) {
			popUpService.open({
				component: 'general-popup',
				data: popupProperties[type]
			})
			.then(function (data) {
				if (type == 'success') {
					if (data.action == 'button') {
						_this._u.goTo('ln.how-to-download-app');
						_this._u.downloadApp();
					} else {
						_this._u.goTo('ln.how-to-download-app');
					}
				} else if (type == 'unsuccess') {
					if (data.action == 'footer-link') {
						location.href = "mailTo:" + _this._u.settings.supportEmail;
					}
				}
			})
			.catch(function() {})
		}
	}
})();