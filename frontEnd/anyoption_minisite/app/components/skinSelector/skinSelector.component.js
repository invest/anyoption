/*component::SkinSelectorController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('skinSelector', {
			templateUrl: 'app/components/skinSelector/skinSelector.component.html',
			controller: SkinSelectorController,
			bindings: {
				onSkinSelect: '&'
			}
		});
		
	SkinSelectorController.$inject = ['utilsService', 'skinSelectorService', 'translationService', 'redirectService', '$state', 'countriesService'];
	function SkinSelectorController(utilsService, skinSelectorService, translationService, redirectService, $state, countriesService) {
		var _this = this;
		_this._u = utilsService;
		_this.resultList = [];
		_this.menuOpen = false;
		_this.inAction = false;
		
		_this.toggleSelector = toggleSelector;
		_this.closeSeletor = closeSeletor;
		_this.selectSkin = selectSkin;
		
		
		function getList() {
			_this.inAction = true;
			skinSelectorService.get()
				.then(function(data) {
					_this.resultList = data;
					_this.inAction = false;
				})
		}
		
		function toggleSelector() {
			_this.menuOpen = !_this.menuOpen;
			getList();
		}
		
		function closeSeletor() {
			_this.menuOpen = false;
		}
		
		function selectSkin(skin) {
			_this._u.setSkin(skin);
			countriesService.resetCountries();
			
			skinSelectorService.skinUpdate(skin.id)
				.then(function() {
					translationService.getTransations()
						.then(function(data) {
							$state.go($state.current.name, {ln: _this._u.getSkin().locale});
							
							_this.onSkinSelect(_this._u.getSkin());
						});
				})
		}
	}
})();