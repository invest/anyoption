/*component::IframePostController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('iframePost', {
			templateUrl: 'app/components/iframePost/iframePost.component.html',
			controller: IframePostController,
			bindings: {
				info: '<',
				onResolve: '&',
				onReject: '&'
			}
		});
		
	IframePostController.$inject = ['utilsService'];
	function IframePostController(utilsService) {
		var _this = this;
		_this._u = utilsService;
		_this.keys = [];

		_this.$onInit = function() {
			_this.keys = [];
			for (var key in _this.info.postData) {
				_this.keys.push(encodeURI(key));
			}
			
			//because for some reason on submit to iframe do not work on ng-form
			$('#the-form').attr('action', _this.info.url);
			
			checkInputlength();
		}
		
		function checkInputlength() {
			if ($('#the-form > input').length == _this.keys.length) {
				$('#the-form').submit();
				checkIframe();
			} else {
				setTimeout(function() {
					checkInputlength()
				}, 500);
			}
		}
		
		function checkIframe() {
			var iframeUrl = '';
			try{
				iframeUrl = $('#the-frame').contents().get(0).location.href;
			} catch(e){}
			if (iframeUrl.toLowerCase().indexOf("action") >= 0) {
				_this.onResolve({data: getUrlValues(iframeUrl)});
			} else {
				setTimeout(function() {
					checkIframe();
				},500);
			}
		}
		
	}
})();