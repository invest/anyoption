(function() {
	'use strict';
	
	angular.module('mainApp')
		.component('globalErrors', {
			templateUrl: 'app/components/globalErrors/globalErrors.component.html',
			bindings: {
				errors: '='
			}
		});
})();
