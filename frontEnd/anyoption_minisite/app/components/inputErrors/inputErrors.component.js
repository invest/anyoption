(function() {
	'use strict';
	
	angular.module('mainApp')
		.component('inputErrors', {
			templateUrl: 'app/components/inputErrors/inputErrors.component.html',
			controller: InputErrorsController,
			bindings: {
				field: '='
			}
		});
		
	InputErrorsController.$inject = ['$element', '$scope', 'translationService'];
	function InputErrorsController($element, $scope, translationService) {
		var _this = this;
		this.$onInit = function() {
			_this.errorFlag = null;
			
			_this.getMsg = translationService.getMsg;
			
			$scope.inputCopy = _this.field;
			setTimeout(function() {//because otherwise $scope.inputCopy.$viewValue is NaN ?!?!?
				_this.msgParams = {
					minlength: _this.field.$$attr.minlength,
					maxlength: _this.field.$$attr.maxlength
				};
				
				var listOfeleToWatch;
				if (typeof $scope.inputCopy.$viewValue == 'object') {
					listOfeleToWatch = '[inputCopy.$touched, inputCopy.$invalid, inputCopy.$$parentForm.$submitted]';
				} else {
					listOfeleToWatch = '[inputCopy.$touched, inputCopy.$invalid, inputCopy.$viewValue, inputCopy.$$parentForm.$submitted]';
				}
				
				$scope.$watch(listOfeleToWatch, function (newValue, oldValue) {
					if (newValue != oldValue) {
						hasError();
						setClass();
					}
				});
			}, 0)
		};
		
		
		
		function hasError() {
			if (_this.field) {
				if (_this.field.$valid) {
					return _this.errorFlag = false;
				} else if ((_this.field.$$parentForm.$submitted && _this.field.$invalid) || (_this.field.$touched && _this.field.$dirty && _this.field.$invalid)) {
					return _this.errorFlag = true;
				}
			}
			return _this.errorFlag = null;
		}
		
		function setClass() {
			$element.removeClass('incorrect correct');
			switch(_this.errorFlag) {
				case true: $element.addClass('incorrect');break;
				case false: $element.addClass('correct');break;
				default: return '';
			}
		}
	}
})();
