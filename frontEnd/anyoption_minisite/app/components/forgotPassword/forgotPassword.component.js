/*component::ForgotPasswordController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('forgotPassword', {
			templateUrl: 'app/components/forgotPassword/forgotPassword.component.html',
			controller: ForgotPasswordController,
			bindings: {
				onReset: '&',
				info: '<'
			}
		});
		
	ForgotPasswordController.$inject = ['utilsService', 'userService'];
	function ForgotPasswordController(utilsService, userService) {
		var _this = this;
		_this._u = utilsService;
		
		var AOETUsername = getAOCookie('AOETUsername');
		if (!isUndefined(AOETUsername)) {
			AOETUsername = AOETUsername.replace(/\"/g, '');
		}
		_this.form = {
			userName: AOETUsername
		}
		
		_this.submit = submit;
		
		
		function submit(_form) {
//			userService.login(_this, _form)
//				.then(function(data) {
//					_this.onLogin({
//						data: data
//					})
//				})
		}
	}
})();