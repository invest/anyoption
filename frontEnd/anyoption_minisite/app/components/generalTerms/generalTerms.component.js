/*component::GeneralTermsController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('generalTerms', {
			templateUrl: 'app/components/generalTerms/generalTerms.component.html',
			controller: GeneralTermsController,
			bindings: {
				info: '<',
				onResolve: '&',
				onReject: '&'
			}
		});
		
	GeneralTermsController.$inject = ['utilsService', 'generalTermsService'];
	function GeneralTermsController(utilsService, generalTermsService) {
		var _this = this;
		_this._u = utilsService;
		_this.filesList = [];

		_this.get = get;
		
		_this.$onInit = function() {
			_this.get();
		}
		
		
		
		function get() {
			var request = {
				platformIdFilter: 2,
				skinIdFilter: _this._u.getSkin().id,
				termsType: _this.info.termsType
			}
			generalTermsService.get(request)
				.then(function(data) {
					_this.filesList = data;
	

					
					// if (!isUndefined($rootScope.$state.params.id)) {
						// $timeout(function() {
							// goToByScroll($rootScope.$state.params.id, -100);
						// }, 1000)
					// }
					// if (_this.params.scrollTo) {
						// $timeout(function() {
							// goToByScroll(_this.params.scrollTo, 0, '.scroll-content-gt');
						// }, 1000)
					// }
				})
		}
	}
})();