﻿/*mainApp module*/
(function() {
	'use strict';
	
	angular.module('mainApp', [
		'ui.router',
		'ngSanitize',
		'ngAnimate',
		'toastr',
		'commonDirectives',
		'commonFilters',
		'commonServices'
	])
	.run(appRun)
	.config(appConfing)


	appRun.$inject = ['$rootScope', '$state', 'mainMenuService', 'settingsService'];
	function appRun($rootScope, $state, mainMenuService, settingsService) {
		
		$rootScope.$state = $state;
		
		$rootScope.$on('$stateChangeStart', stateChangeStart);
		$rootScope.$on('$stateChangeSuccess', stateChangeSuccess);
		
		function stateChangeStart(event, toState, toParams, fromState, fromParams) {
			$rootScope.stateLoaded = false;
			if (!isUndefined($rootScope.targetState)) {
				if (settingsService.settings.loggedIn) {
					if (toState.needLogin < 1) {
						event.preventDefault(); 
					}
				} else {
					if (toState.needLogin > 1) {
						event.preventDefault(); 
					}
				}
			}
			$rootScope.stateParams = toParams;
			$rootScope.targetState = toState;
			//change browser title
			if (!isUndefined(toState.metadataKey)) {
				$rootScope.metadataTitle = toState.metadataKey;
			} else {
				$rootScope.metadataTitle = 'index';
			}
		}

		function stateChangeSuccess(event, toState, toParams, fromState, fromParams) {
			if (fromState.url == '^' || fromState.url == '/') {
				$rootScope.afterRefresh = true;
			} else {
				$rootScope.afterRefresh = false;
			}
			
			$rootScope.bodyClass = toState.name.replace('ln.', '').replace('.', '_');

			//redirect links that should not be accessed
			if (!isUndefined($rootScope.$state.current.redirectTo)) {
				$rootScope.$state.go($rootScope.$state.current.redirectTo, {ln: $rootScope.$state.params.ln});
			}

			mainMenuService.close();
			
			var body = $("html, body");
			if (!isUndefined($rootScope.goToAndScroll_id) && $rootScope.goToAndScroll_id != '') {
				setTimeout(function() {
					body.stop().animate({scrollTop: $("#" + $rootScope.goToAndScroll_id).offset().top}, '500', 'swing');
					$rootScope.goToAndScroll_id = '';
				}, 250);
			} else {
				body.stop().animate({scrollTop:0}, '500', 'swing');
			}
			$rootScope.ngLoaded = true;
			$rootScope.stateLoaded = true;
		}
	}

	appConfing.$inject = ['$httpProvider', '$stateProvider', '$urlRouterProvider', '$locationProvider'];
	function appConfing($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider) {
		getTrackingParams();
		//$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
		delete $httpProvider.defaults.headers.post['Content-Type'];
		
		$httpProvider.defaults.withCredentials = true;
		
		//TODO remove after AO web is in NG
		switch (decodeURIComponent(window.location.pathname).toLowerCase()) {
			case "/register-binary-options-platform"://en,cs,pl,pt
			case "/cuenta-negociacion-opciones-binarias"://es
			case "/register-binary-options-platform"://it
			case "/registrieren-für-binäroptionen-plattform"://de
			case "/inscription-plateforme-options-binaires"://fr
			case "/registrera-binär-optioner-plattform"://sv
			case "/registreer-binaire-opties-platform"://nl
			case "/register_v2"://affiliate
			window.redirectOldLinks = 'ln.open-account';
		}

		$urlRouterProvider
			.otherwise('/check/');

		$stateProvider
			.state('ln', {
				url: '/:ln',
				templateUrl: 'app/layout/template.html',
				'abstract': true,
				needLogin: 1,//0 - no, 1 - doesn't matter, 2-yes
				controller: 'MainController',
				controllerAs: 'mCtrl',
				resolve: {
					initPrepService: initPrepService
				}
			})
			.state('ln.index', {
				url: '/',
				views: {
					'main': {
						templateUrl: 'app/views/index.html'
					}
				},
				needLogin: 0,
			})
			.state('ln.open-account', {
				url: '/open-account',
				views: {
					'main': {
						templateUrl: 'app/views/open-account.html'
					}
				},
				needLogin: 0,
			})
			.state('ln.forgot-password', {
				url: '/forgot-password',
				views: {
					'main': {
						templateUrl: 'app/views/forgot-password.html'
					}
				},
				needLogin: 0,
			})
			.state('ln.reset-password', {
				url: '/reset-password',
				views: {
					'main': {
						templateUrl: 'app/views/reset-password.html'
					}
				},
				needLogin: 0,
			})
			.state('ln.trading', {
				url: '/trading',
				'abstract': true,
				redirectTo: 'ln.trading.our-platforms',
				needLogin: 0,
			})
			.state('ln.trading.our-platforms', {
				url: '/our-platforms',
				views: {
					'main@ln': {
						templateUrl: 'app/views/our-platforms.html'
					}
				},
				needLogin: 0,
			})
			.state('ln.trading.strategy', {
				url: '/strategy',
				views: {
					'main@ln': {
						templateUrl: 'app/views/strategy.html'
					}
				},
				needLogin: 0,
			})
			.state('ln.banking', {
				url: '/banking',
				views: {
					'main': {
						templateUrl: 'app/views/banking.html'
					}
				},
				needLogin: 0,
			})
			.state('ln.info', {
				url: '/info',
				views: {
					'main': {
						templateUrl: 'app/views/about-us.html'
					}
				},
				needLogin: 0,
			})
			.state('ln.info.faq', {
				url: '/faq',
				views: {
					'main@ln': {
						templateUrl: 'app/views/faq.html'
					}
				},
				needLogin: 0,
			})
			.state('ln.info.security', {
				url: '/security',
				views: {
					'main@ln': {
						templateUrl: 'app/views/security.html'
					}
				},
				needLogin: 0,
			})
			.state('ln.how-to-download-app', {
				url: '/how-to-download-app',
				views: {
					'main': {
						templateUrl: 'app/views/how-to-download-app.html'
					}
				},
				needLogin: 1,
			})
			.state('ln.deposit', {
				url: '/deposit',
				views: {
					'main': {
						templateUrl: 'app/views/deposit-with-cc.html'
					}
				},
				needLogin: 2,
			})


		// enable html5Mode for pushstate ('#'-less URLs)
		// if (settings.isLive) {
			// $locationProvider.html5Mode(true).hashPrefix('!');
		// }

		$httpProvider.interceptors.push('myHttpInterceptor');
	}
})();
