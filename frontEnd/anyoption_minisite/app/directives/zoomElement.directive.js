/* directive::zoomElement */
(function() {
	'use strict';
	
	angular.module('commonDirectives').directive('zoomElement', zoomElement);

	function zoomElement() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.bind('click', function() {
					if (attrs.toggleTarget) {
						element.parent('.' + attrs.toggleTarget).toggleClass(attrs.zoomElement);
						$("html, body").stop().animate({scrollTop: element.offset().top - ($(window).height() / 2) + (element.height() / 2) }, '500', 'swing');
						
						if ($('body').hasClass('no-scroll')) {
							document.ontouchmove = function (e) {
								return true;
							}
						} else {
							document.ontouchmove = function (e) {
								e.preventDefault();
							}
						}
						
						$('body').toggleClass('no-scroll');
					} else {
						element.toggleClass(attrs.zoomElement);
					}
				});
			}
		}
	}
})();