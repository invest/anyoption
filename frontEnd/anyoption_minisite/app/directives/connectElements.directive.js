/* directive::connectElements */
(function() {
	'use strict';

	angular.module('commonDirectives').directive('connectElements', connectElements);

	connectElements.$inject = ['connectElementsService'];
	function connectElements(connectElementsService) {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				element.from = attrs.from;
				element.to = attrs.to;
				element.dir = attrs.dir;
				
				connectElementsService.register(element);
				
				scope.$on('$destroy', function() {
					connectElementsService.unregister(element);
				})
			}
		}
	}
})();
/*
<svg width="200px" height="400px">
	<path fill="none" stroke="#a8b3b9" stroke-width="3" stroke-dasharray="10" d="M33 3 C 53 3, 3 3, 3 53 L3 303 C 3 303, 3 353, 53 353"/>
	<path class="path" fill="#faaa1b" d="M43 338 L83 353 L43 368 L53 353 L43 338 Z"/>
</svg>
*/