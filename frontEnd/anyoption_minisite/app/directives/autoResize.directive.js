/* directive::autoResize */
(function() {
	'use strict';

	angular.module('commonDirectives').directive('autoResize', autoResize);

	autoResize.$inject = ['autoResizeService'];
	function autoResize(autoResizeService) {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				var offsetWidth = parseInt(attrs.offsetWidth);
				var offsetHeight = parseInt(attrs.offsetHeight);
				
				element.offsetWidth = ($.isNumeric(offsetWidth)) ? offsetWidth : 0;
				element.offsetHeight = ($.isNumeric(offsetHeight)) ? offsetHeight : 0;
				
				autoResizeService.register(element);
				
				scope.$on('$destroy', function() {
					autoResizeService.unregister(element);
				})
			}
		}
	}
})();