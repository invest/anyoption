﻿/*mainApp module*/
(function() {
	'use strict';
	
	angular.module('mainApp', [
		'ui.router',
		'ngSanitize',
		'ngAnimate',
		'toastr',
		'ngScrollbars',
		'commonDirectives',
		'commonFilters',
		'commonServices'
	])
	.run(appRun)
	.config(appConfing)


	appRun.$inject = ['$rootScope', '$state', 'mainMenuService', 'settingsService'];
	function appRun($rootScope, $state, mainMenuService, settingsService) {
		$rootScope.$state = $state;
		
		$rootScope.$on('$stateChangeStart', stateChangeStart);
		$rootScope.$on('$stateChangeSuccess', stateChangeSuccess);
		
		function stateChangeStart(event, toState, toParams, fromState, fromParams) {
			$rootScope.stateLoaded = false;
			if (!isUndefined($rootScope.targetState)) {
				if (settingsService.settings.loggedIn) {
					if (toState.needLogin < 1) {
						event.preventDefault(); 
					}
				} else {
					if (toState.needLogin > 1) {
						event.preventDefault(); 
					}
				}
			}
			$rootScope.stateParams = toParams;
			$rootScope.targetState = toState;
			//change browser title
			if (!isUndefined(toState.metadataKey)) {
				$rootScope.metadataTitle = toState.metadataKey;
			} else {
				$rootScope.metadataTitle = 'index';
			}
		}

		function stateChangeSuccess(event, toState, toParams, fromState, fromParams) {
			if (fromState.url == '^' || fromState.url == '/') {
				$rootScope.afterRefresh = true;
			} else {
				$rootScope.afterRefresh = false;
			}
			
			$rootScope.bodyClass = toState.name.replace('ln.', '').replace('.', '_');

			//redirect links that should not be accessed
			if (!isUndefined($rootScope.$state.current.redirectTo)) {
				$rootScope.$state.go($rootScope.$state.current.redirectTo, {ln: $rootScope.$state.params.ln});
			}

			mainMenuService.close();
			
			var body = $("html, body");
			var scrtollTo = 0;
			var inView = $("#in-view");
			if (inView.length > 0) {
				scrtollTo = inView.offset().top - 30;
			}
			body.stop().animate({scrollTop:scrtollTo}, 500, 'swing');
			$rootScope.ngLoaded = true;
			$rootScope.stateLoaded = true;
		}
	}

	appConfing.$inject = ['$httpProvider', '$stateProvider', '$urlRouterProvider', '$locationProvider'];
	function appConfing($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider) {
		getTrackingParams();
		//$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
		delete $httpProvider.defaults.headers.post['Content-Type'];
		
		$httpProvider.defaults.withCredentials = true;

		$urlRouterProvider
			.otherwise('/check/');

		$stateProvider
			.state('ln', {
				url: '/:ln',
				views: {
					'uiView': {
						templateUrl: 'app/layout/template.html',
						controller: 'MainController',
						controllerAs: 'mCtrl',
						resolve: {
							initPrepService: initPrepService
						}
					}
				},
				'abstract': true,
				needLogin: 1,//0 - no, 1 - doesn't matter, 2-yes
			})
			.state('ln.index', {
				url: '/',
				views: {
					'main': {
						templateUrl: 'app/views/index.html'
					}
				},
				needLogin: 1,
			})
			.state('ln.academy', {
				url: '/academy',
				views: {
					'main': {
						templateUrl: 'app/layout/template-academy.html',
						controller: 'AcademyController',
						controllerAs: 'AcCtrl'
					}
				},
				needLogin: 1,
			})
			.state('ln.academy.intro', {
				url: '/intro',
				views: {
					'academy': {
						templateUrl: 'app/views/academy/intro.html'
					}
				},
				needLogin: 1,
			})
			.state('ln.academy.trading-videos', {
				url: '/trading-videos',
				views: {
					'academy': {
						templateUrl: 'app/views/academy/trading-videos.html'
					}
				},
				needLogin: 1,
			})
			.state('ln.academy.trading-videos.beginners', {
				url: '/beginners',
				views: {
					'academy@ln.academy': {
						templateUrl: 'app/views/academy/trading-videos-beginners.html'
					}
				},
				needLogin: 1,
			})
			.state('ln.academy.trading-videos.intermediate', {
				url: '/intermediate',
				views: {
					'academy@ln.academy': {
						templateUrl: 'app/views/academy/trading-videos-intermediate.html'
					}
				},
				needLogin: 1,
			})
			.state('ln.academy.trading-videos.advanced', {
				url: '/advanced',
				views: {
					'academy@ln.academy': {
						templateUrl: 'app/views/academy/trading-videos-advanced.html'
					}
				},
				needLogin: 1,
			})
			.state('ln.academy.our-products', {
				url: '/our-products',
				views: {
					'academy': {
						templateUrl: 'app/views/academy/our-products.html'
					}
				},
				needLogin: 1,
			})
			.state('ln.academy.e-learning', {
				url: '/e-learning',
				views: {
					'academy': {
						templateUrl: 'app/views/academy/e-learning.html'
					}
				},
				needLogin: 1,
			})
			.state('ln.academy.e-learning.e-books', {
				url: '/e-books',
				views: {
					'academy@ln.academy': {
						templateUrl: 'app/views/academy/e-books.html'
					}
				},
				needLogin: 1,
			})
			.state('ln.academy.e-learning.e-cources', {
				url: '/e-cources',
				views: {
					'academy@ln.academy': {
						templateUrl: 'app/views/academy/e-cources.html'
					}
				},
				needLogin: 1,
			})
			.state('ln.academy.articles', {
				url: '/articles',
				views: {
					'academy': {
						templateUrl: 'app/views/academy/articles.html',
						controller: 'ArticlesController',
						controllerAs: 'ArCtrl'
					}
				},
				needLogin: 1,
			})
			.state('ln.academy.articles.view', {
				url: '/:articleId',
				views: {
					'academy@ln.academy': {
						templateUrl: 'app/views/academy/articles-view.html',
						controller: 'ArticlesViewController',
						controllerAs: 'ArVCtrl',
						resolve: {
							articlePrepService: articlePrepService 
						}
					}
				},
				needLogin: 1,
			})
			.state('ln.asset-index', {
				url: '/asset-index',
				views: {
					'main': {
						templateUrl: 'app/views/asset-index-page.html'
					}
				},
				needLogin: 1,
			})
			.state('ln.dynamics', {
				url: '/dynamics',
				views: {
					'main': {
						templateUrl: 'app/views/dynamics.html',
						controller: 'DynamicsController',
						controllerAs: 'DyCtrl',
					}
				},
				needLogin: 1,
			})
			.state('ln.dynamics.asset', {
				url: '/:marketId',
				views: {
					'main': {
						templateUrl: 'app/views/dynamics.html'
					}
				},
				needLogin: 1,
			})


		// enable html5Mode for pushstate ('#'-less URLs)
		// if (settings.isLive) {
			// $locationProvider.html5Mode(true).hashPrefix('!');
		// }

		$httpProvider.interceptors.push('myHttpInterceptor');
		
	}
	
	angular.bootstrap(document.getElementById("bootstrapId"), ['aoApp']);
})();
