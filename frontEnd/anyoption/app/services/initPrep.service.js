initPrepService.$inject = ['$q', 'settingsService', 'translationService', 'userService', 'redirectService'];
function initPrepService($q, settingsService, translationService, userService, redirectService) {
	var deferred = $q.defer();
	
	if (!settingsService.finished) {
		settingsService.get()
			.then(function() {
				$q.all([
					userService.get({ifLogged: true}),
					translationService.getTransations()
				])
				.then(function(data) {
					redirectService.redirect();
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
			})
			.catch(function(data) {
				deferred.reject(data);
			})
	} else {
		deferred.resolve({});
	}
	
	return deferred.promise;
}