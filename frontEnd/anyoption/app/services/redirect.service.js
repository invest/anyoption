/* service::redirectService */
(function() {
	'use strict';

	angular.module('commonServices').service('redirectService', redirectService);

	redirectService.$inject = ['settingsService', 'userService', '$state', '$rootScope'];
	function redirectService(settingsService, userService, $state, $rootScope) {
		var _this = this;
		_this.redirect = redirect;
		
		function redirect() {
			if (settingsService.settings.loggedIn) {
				setAOCookie('id_ln', settingsService.getSkin().id, 365);
//TODO this should be the logic for what to show after login/register
//				if ($rootScope.targetState.needLogin < 1) {
//					$state.go('ln.deposit', {ln: settingsService.skin.locale});
//				} else {
//					var stateName = $rootScope.targetState.name;
//					if (stateName == '') {
//						stateName = 'ln.index';
//					}
//					$state.go($rootScope.targetState.name, {ln: settingsService.skin.locale});
//				}
			} else {
				setAOCookie('id_ln', settingsService.getSkin().id, 365);

				//TODO remove after AO in NG
				if (!isUndefined(window.redirectOldLinks) && window.redirectOldLinks != '') {
					$state.go(window.redirectOldLinks, {ln: settingsService.getSkin().locale});
				} else if (isUndefined($rootScope.targetState.needLogin) || $rootScope.targetState.needLogin > 1 || $rootScope.stateParams.ln == 'check') {
					$state.go('ln.index', {ln: settingsService.getSkin().locale});
				}
			}
		}
	}
})();