/* controller::MainController */
(function() {
	'use strict';

	angular.module('mainApp').controller('MainController', MainController);

	MainController.$inject = ['utilsService', 'redirectService', '$state'];
	function MainController(utilsService, redirectService, $state) {
		var _this = this;
		_this._u = utilsService;
		_this.getMsg = utilsService.getMsg;
		_this.getSkin = _this._u.getSkin;
		
		
		//TODO - this will be removed once we refactor EVERTHING MUGHAHAHA
		if (requestURINoContextPath.search('trading-academy') > -1 && !$state.includes('ln.academy')) {
			_this._u.goTo('ln.academy');
		}
		else if (requestURINoContextPath.search('assetIndex') > -1 && !$state.includes('asset-index')) {
			_this._u.goTo('ln.asset-index');
		}
		else if (requestURINoContextPath.search('dynamics') > -1 && !$state.includes('dynamics')) {
			_this._u.goTo('ln.dynamics');
		}
	}
})();