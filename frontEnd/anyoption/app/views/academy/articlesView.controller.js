/* controller::ArticlesViewController */
(function() {
	'use strict';

	angular.module('mainApp').controller('ArticlesViewController', ArticlesViewController);

	ArticlesViewController.$inject = ['utilsService', 'articlePrepService'];
	function ArticlesViewController(utilsService, articlePrepService) {
		var _this = this;
		var _u = utilsService;
		
		_this.article = articlePrepService;
	}
})();