/* service::academyVideoService */
(function() {
	'use strict';

	angular.module('mainApp').service('academyVideoService', academyVideoService);

	academyVideoService.$inject = ['utilsService', 'popUpService', 'popupLogicService', 'userService'];
	function academyVideoService(utilsService, popUpService, popupLogicService, userService) {
		var _u = utilsService;
		var _this = this;

		//video id: access level (1,2,3)
		_this.contentList = {
			ourProducts:[
				{id: 9, type: 'video', access: 1, mteOrdId: 1, product: 'binr', url: '../jsp/trade_binary_options.jsf'},
				{id: 10, type: 'video', access: 1, mteOrdId: 2, product: 'binr', url: '../jsp/optionPlus.jsf'},
				{id: 35, type: 'video', access: 1, mteOrdId: 3, product: 'binr', url: '../jsp/oneTouch.jsf'},
				{id: 25, type: 'video', access: 1, mteOrdId: 4, product: 'binr', url: '../jsp/trade_binary_options.jsf'}
			],
			beginners: [
				{id: 13, type: 'video', access: 1, mteOrdId: 1, product: 'vinr'},
				{id: 5, type: 'video', access: 1, mteOrdId: 2, product: 'vinr'},
				{id: 6, type: 'video', access: 1, mteOrdId: 3, product: 'vinr'},
				{id: 51, type: 'video', access: 1, mteOrdId: 4, product: 'vinr'}
			],
			intermediate: [
				{type: 'video', id: 'c_1', access: 2, mteOrdId: 1, product: 'binr'},
				{type: 'video', id: 'c_2', access: 2, mteOrdId: 2, product: 'binr'},
				{type: 'video', id: 'c_3', access: 2, mteOrdId: 3, product: 'binr'},
				{type: 'video', id: 'c_4', access: 2, mteOrdId: 4, product: 'binr'}
			],
			advanced: [
				{id: 7, type: 'video', access: 3, mteOrdId: 5, product: 'vinr'},
				{id: 8, type: 'video', access: 3, mteOrdId: 6, product: 'vinr'},
				{id: 23, type: 'video', access: 3, mteOrdId: 7, product: 'vinr'},
				{id: 22, type: 'video', access: 3, mteOrdId: 8, product: 'vinr'},
				{id: 36, type: 'video', access: 3, mteOrdId: 9, product: 'vinr'},
				{id: 40, type: 'video', access: 3, mteOrdId: 10, product: 'vinr'},
				{id: 43, type: 'video', access: 3, mteOrdId: 11, product: 'vinr'},
				{id: 44, type: 'video', access: 3, mteOrdId: 12, product: 'vinr'}
			],
			ebooks: [
				{id: 'b_1', type: 'flipBook', folder: 'master_the_market', access: 1},
				{id: 'b_2', type: 'flipBook', folder: '10_commandments', access: 2},
				{id: 'b_3', type: 'flipBook', folder: '5_top_tr_tips', access: 2},
				{id: 'b_4', type: 'flipBook', folder: 'the_mindset_of_successful_traders', access: 2},
			],
			ecourses: [
				{id: 'l_1', type: 'link', url: 'https://www.anyoption.com/blog/binary-options-ecourse/introduction-to-binary-options-trading/', access: 1},
				{id: 'l_2', type: 'link', url: 'https://www.anyoption.com/blog/binary-options-ecourse/psychology-of-a-successful-trader/', access: 1},
				{id: 'l_3', type: 'link', url: 'https://www.anyoption.com/blog/binary-options-ecourse/types-of-underlying-assets/', access: 1},
				{id: 'l_4', type: 'link', url: 'https://www.anyoption.com/blog/binary-options-ecourse/risk-management/', access: 1},
				{id: 'l_5', type: 'link', url: 'https://www.anyoption.com/blog/binary-options-ecourse/market-analysis-overview/', access: 1},
				{id: 'l_6', type: 'link', url: 'https://www.anyoption.com/blog/binary-options-ecourse/fundamental-analysis/', access: 1},
				{id: 'l_7', type: 'link', url: 'https://www.anyoption.com/blog/binary-options-ecourse/technical-analysis/', access: 1},
				{id: 'l_8', type: 'link', url: 'https://www.anyoption.com/blog/binary-options-ecourse/fundamental-vs-technical-analysis/', access: 1},
				{id: 'l_9', type: 'link', url: 'https://www.anyoption.com/blog/binary-options-ecourse/in-depth-trading-strategies-and-tactics/', access: 1},
				{id: 'l_10', type: 'link', url: 'https://www.anyoption.com/blog/binary-options-ecourse/economic-calendar-and-trading-signals/', access: 1},
				{id: 'l_11', type: 'link', url: 'https://www.anyoption.com/blog/binary-options-ecourse/forex-binary-options/', access: 1},
				{id: 'l_12', type: 'link', url: 'https://www.anyoption.com/blog/binary-options-ecourse/trading-platform-trade-types/', access: 1},
				{id: 'l_13', type: 'link', url: 'https://www.anyoption.com/blog/binary-options-ecourse/regulations-in-the-industry/', access: 1},
			]
		}
		
		$.each(_this.contentList, function(index, el) {
			el.map(function(current) {
				current.randomOrder = Math.floor(Math.random() * (el.length * 2)) + 1;
				return current;
			});
		});
		
		_this.open = open;
		
		
		
		function open(content) {
			var component = '';
			var action = 'popup';
			if (userService.getUserVODStatus() == 1 && userService.getUserVODStatus() < content.access) {
				component = 'register-login';
				action = 'general-popup';
			} else if ((userService.getUserVODStatus() == 2 && userService.getUserVODStatus() < content.access) 
					|| (userService.getUserVODStatus() == 3 && userService.getUserVODStatus() < content.access)) {
				component = 'deposit-general';
				action = 'general-popup';
			} else {
				if (content.type == "video") {
					component = 'academy-video';
				} else {
					action = content.type;
				}
			}
			
			if (action == 'general-popup') {
				popupLogicService.openGeneralPopup({
					component: component,
					info: {
						showDeposit: (content.access == 3) ? true : false
					}
				})
				.then(function(data) {
					open(content);
				})
				.catch(function() {})
			} else if (action == 'popup') {
				popUpService.open({
					component: 'academy-video',
					data: {video: content}
				})
				.catch(function() {})
			} else if (action == 'link') {
				var win = window.open(content.url, '_blank');
				win.focus();
			} else if (action == 'flipBook') {
				var win = window.open('eBook/eBook-flipbook.html?book=' + content.folder + '&ln=' + _u.getSkin().locale, '_blank');
				win.focus();
			}
		}
	}
})();