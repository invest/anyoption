/* service::articlesService */
(function() {
	'use strict';

	angular.module('commonServices').service('articlesService', articlesService);

	articlesService.$inject = ['$q', '_http', 'utilsService'];
	function articlesService($q, _http, utilsService) {
		var _u = utilsService;
		var _this = this;
		
		_this.homePageArticles = [];
		for (var i = 0; i <= 13; i++) {
			_this.homePageArticles.push({});
		}
		_this.homePageTopics = [];
		
		var requestFinished = {
			homeArticles: false,
			homeTopics: false
		};
		
		_this.getHomePageArticles = getHomePageArticles;
		_this.getTopics = getTopics;
		_this.searchArticle = searchArticle;
		_this.getArticle = getArticle;


		
		function getHomePageArticles() {
			if (!requestFinished.homeArticles) {
				var request = _u.getMethodRequest();
				request.articleLocale = _u.getSkin().locale;
				requestFinished.homeArticles = true;
				
				return _http.get('common', 'AcademyArticlesServices/getHomePageArticles', request, 'homePageArticles')
					.then(function(data) {
						if (data != null) {
							data.map(function(current) {
								_this.homePageArticles[current.homePagePosition] = {
									id: current.id,
									imageUrl: current.imageUrl,
									author: current.translations[0].author,
									title: current.translations[0].title
								}
							});
						}

						return _this.homePageArticles;
					})
			} else {
				return $q.resolve(_this.homePageArticles);
			}
		}
		
		function getTopics() {
			if (!requestFinished.topics) {
				var request = _u.getMethodRequest();
				request.articleLocale = _u.getSkin().locale;
				requestFinished.topics = true;
				
				return _http.get('common', 'AcademyArticlesServices/getTopics', request, 'topics')
					.then(function(data) {
						_this.homePageTopics = data;

						return _this.homePageTopics;
					})
			} else {
				return $q.resolve(_this.homePageTopics);
			}
		}
		
		function searchArticle(topics) {
			var request = _u.getMethodRequest();
			request.articleLocale = _u.getSkin().locale;
			request.topicIds = topics;
			
			return _http.get('common', 'AcademyArticlesServices/searchArticle', request, 'searchedArticles')
				.then(function(data) {
					var searchedArticles = [];
					
					if (data != null) {
						data.map(function(current) {
							searchedArticles.push({
								id: current.id,
								imageUrl: current.imageUrl,
								author: current.translations[0].author,
								title: current.translations[0].title
							});
						});
					}

					return searchedArticles;
				})
		}
		
		function getArticle(id) {
			var request = _u.getMethodRequest();
			request.articleLocale = _u.getSkin().locale;
			request.article = {id: id};
			
			return _http.get('common', 'AcademyArticlesServices/getArticle', request, 'article')
				.then(function(data) {
					return {
						title: data.translations[0].title,
						subtitle: data.translations[0].subtitle,
						author: data.translations[0].author,
						html: data.translations[0].html
					} 
				})
		}
	
	}
})();

articlePrepService.$inject = ['$stateParams', 'articlesService'];
function articlePrepService($stateParams, articlesService) {
	return articlesService.getArticle($stateParams.articleId);
}