/* controller::AcademyController */
(function() {
	'use strict';

	angular.module('mainApp').controller('AcademyController', AcademyController);

	AcademyController.$inject = ['utilsService', 'academyVideoService', 'articlesService', '$timeout'];
	function AcademyController(utilsService, academyVideoService, articlesService, $timeout) {
		var _this = this;
		var _u = utilsService;
		_this.homePageArticles = [];
		
		_this.showComplateRegister = false;
		
		_this.contentList = academyVideoService.contentList;
		
		articlesService.getHomePageArticles()
			.then(function(data) {
				_this.homePageArticles = data;
			})

		$.getScript("//mte-media.com/admin2/js/mte.js");
		
		_this.openContent = academyVideoService.open;
		_this.setComplateRegister = setComplateRegister;
		
		function setComplateRegister() {
			_this.showComplateRegister = true;
			
			$timeout(function() {
				_this.showComplateRegister = false;
			}, 2000);
		}
	}
})();