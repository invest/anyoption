/* controller::ArticlesController */
(function() {
	'use strict';

	angular.module('mainApp').controller('ArticlesController', ArticlesController);

	ArticlesController.$inject = ['utilsService', 'articlesService'];
	function ArticlesController(utilsService, articlesService) {
		var _this = this;
		var _u = utilsService;
		_this.homePageArticles = [];
		_this.homePageArticlesSearched = [];
		_this.homePageTopics = [];
		_this.homePageTopicsSelected = [];

		
		articlesService.getHomePageArticles()
			.then(function(data) {
				_this.homePageArticles = data;
			})
			
		articlesService.getTopics()
			.then(function(data) {
				_this.homePageTopics = data;
			})
			
		_this.isTopicSelected = isTopicSelected;
		_this.selectTopic = selectTopic;
		
		
		function isTopicSelected(id) {
			return _this.homePageTopicsSelected.indexOf(id) > -1;
		}
		
		function selectTopic(id) {
			var index = _this.homePageTopicsSelected.indexOf(id);
			if (index > -1) {
				_this.homePageTopicsSelected.splice(index, 1);
			} else {
				_this.homePageTopicsSelected.push(id);
			}
			
			if (_this.homePageTopicsSelected.length == 0) {
				_this.homePageArticlesSearched.length = 0;
			} else {
				return articlesService.searchArticle(_this.homePageTopicsSelected)
					.then(function(data) {
						_this.homePageArticlesSearched = data;
					})
			}
		}
		
	}
})();