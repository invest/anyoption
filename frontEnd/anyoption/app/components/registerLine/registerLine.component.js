/*component::RegisterController*/
(function() {
	'use strict';

	angular.module('mainApp')
	.component('registerLineForm', {
		templateUrl: 'app/components/registerLine/registerLine.component.html',
		controller: RegisterController,
		bindings: {
			info: '<',
			onRegister: '&',
			onClose: '&'
		}
	});
		
	RegisterController.$inject = ['utilsService', 'userService', 'countriesService', 'popUpService'];
	function RegisterController(utilsService, userService, countriesService, popUpService) {
		this.$onInit = function() {
			countriesService.getSortedCountries()
				.then(function(data) {
					_this.countries = data;
					
					_this.formSelect.country = countriesService.defaultCountry;
				})
		}
		
		var _this = this;
		_this._u = utilsService;
		
		_this.reneralTerms = _this._u.getMsg('accept-terms', {_noTrust: true, 'clickEvent': '$ctrl._u.openGeneralTerms(1)'});
		
		_this.form = {
			terms: false
		}
		//a little less input filling for us
		if (isLocal) {_this.form = _this._u.defaultRegisterLocal();}
		
		_this.formSelect = {
			country: {}
		}
		
		
		_this.submit = submit;
		_this.insertAttempt = insertAttempt;
		
		
		function submit(_form) {
			userService.register(_this, _form)
				.then(function(data) {
					_this.onRegister(data);
				});
		}
		
		function insertAttempt(_form) {
			return userService.insertRegisterAttempt(_this, _form);
		}
	}
})();
