/*component::DepositProcesssingController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('depositProcesssing', {
			templateUrl: 'app/components/depositProcesssing/depositProcesssing.component.html',
			controller: DepositProcesssingController,
			bindings: {
				onResolve: '&'
			}
		});
		
	DepositProcesssingController.$inject = ['utilsService'];
	function DepositProcesssingController(utilsService) {
		var _this = this;
		_this._u = utilsService;
	}
})();