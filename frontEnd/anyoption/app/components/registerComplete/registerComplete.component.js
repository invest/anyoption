/*component::RegisterCompleteController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('registerComplete', {
			templateUrl: 'app/components/registerComplete/registerComplete.component.html',
			controller: RegisterCompleteController,
			bindings: {
				info: '<',
				onResolve: '&',
				onReject: '&'
			}
		});
		
	RegisterCompleteController.$inject = ['utilsService'];
	function RegisterCompleteController(utilsService) {
		var _this = this;
		_this._u = utilsService;
	}
})();