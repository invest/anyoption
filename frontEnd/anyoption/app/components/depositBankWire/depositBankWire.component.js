/*component::DepositBankWireController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('depositBankWire', {
			templateUrl: 'app/components/depositBankWire/depositBankWire.component.html',
			controller: DepositBankWireController,
			bindings: {
				info: '<',
				onResolve: '&',
				onReject: '&'
			}
		});
		
	DepositBankWireController.$inject = ['utilsService', 'bankWireService'];
	function DepositBankWireController(utilsService, bankWireService) {
		var _this = this;
		_this._u = utilsService;
		
		_this.activeTab = 1;
		
		_this.bankWireInfo = bankWireService.getBankWireInfo();
		_this.switchTab = switchTab;
		
		
		function switchTab(tab) {
			_this.activeTab = tab;
		}
	}
})();