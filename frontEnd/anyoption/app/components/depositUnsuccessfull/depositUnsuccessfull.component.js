/*component::DepositSuccessfullController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('depositUnsuccessfull', {
			templateUrl: 'app/components/depositUnsuccessfull/depositUnsuccessfull.component.html',
			controller: DepositSuccessfullController,
			bindings: {
				info: '<',
				onResolve: '&',
				onReject: '&',
			}
		});
		
	DepositSuccessfullController.$inject = ['utilsService'];
	function DepositSuccessfullController(utilsService) {
		var _this = this;
		_this._u = utilsService;
	}
})();