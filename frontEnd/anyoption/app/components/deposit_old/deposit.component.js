/*component::DepositController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('deposit', {
			templateUrl: 'app/components/deposit/deposit.component.html',
			controller: DepositController,
			bindings: {
				info: '<',
				onResolve: '&',
				onReject: '&'
			}
		});
		
	DepositController.$inject = ['utilsService'];
	function DepositController(utilsService) {
		var _this = this;
		_this._u = utilsService;
		
	}
})();