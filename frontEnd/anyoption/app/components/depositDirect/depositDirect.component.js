/*component::DepositDirectController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('depositDirect', {
			templateUrl: 'app/components/depositDirect/depositDirect.component.html',
			controller: DepositDirectController,
			bindings: {
				info: '<',
				onDeposit: '&',
				onClose: '&'
			}
		});
		
	DepositDirectController.$inject = ['utilsService', 'depositDirectService', 'skinCurrenciesService', 'userService', 'popUpService'];
	function DepositDirectController(utilsService, depositDirectService, skinCurrenciesService, userService, popUpService) {
		this.$onInit = function() {
			userService.checkFirstDeposit()
				.then(function(data) {
					_this.isFirstDeposit = data;
				})

			skinCurrenciesService.get()
				.then(function(data) {
					_this.currencies = data.currencies;
					_this.predefinedAmounts = data.predefinedDepositAmounts;
					
					_this.formSelect.curency = _this.currencies[searchJsonKeyInArray(_this.currencies, 'id', _this._u.getCurrency().id)];
					_this.form.amount = data.predefinedDepositAmounts[_this._u.getCurrency().id];
				})
				
			_this.form.paymentType = _this.info.type;
		}
		
		var _this = this;
		_this._u = utilsService;
		
		_this.form = {}
		//some cool stuff are going on here
		if (isLocal) {_this.form = _this._u.defaultDepositLocal();}
		
		_this.formSelect = {
			curency: {}
		}
		
		_this.isFirstDeposit = true;
		
		_this.shortYear = _this._u.settings.shortYear;
		_this.shortYearFuture = _this._u.settings.shortYearFuture;
		_this.yearMinus18 = new Date().getFullYear() - 18;
		
		_this.updateDefaultDepositAmount = updateDefaultDepositAmount;
		_this.submit = submit;
	
		function updateDefaultDepositAmount(_form) {
			if (!_form.amount.$touched) {
				_this.form.amount = _this.predefinedAmounts[_this.formSelect.curency.id];
			}
		}
		
		function submit(_form) {
			popUpService.open({component: 'deposit-processsing'});
			
			depositDirectService.insertFirstDeposit(_this, _form)
				.then(function(data) {
					afterDeposit(data);
				})
				.catch(function(data) {
					popUpService.close('deposit-processsing');
					
					if (!$.isEmptyObject(data)) {
						popUpService.open({component: 'deposit-unsuccessfull'})
							.catch(function(data) {
								_this.onClose();
							})
					}
				})
		}
		
		function afterDeposit(data) {
			popUpService.close('deposit-processsing');
			
			popUpService.open({
				component: 'deposit-successfull',
				data: {
					transactionId: data.transactionId,
					// clearingProvider: data.clearingProvider,
					// provider: (data.clearingProvider && data.clearingProvider != '') ? '-provider' : ''
				}
			})
			.then(function() {
				_this.onDeposit({
					data: data
				})
			})
			.catch(function() {})
		}
	}
})();