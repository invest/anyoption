/*component::DepositOtherController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('depositOther', {
			templateUrl: 'app/components/depositOther/depositOther.component.html',
			controller: DepositOtherController,
			bindings: {
				info: '<',
				onDeposit: '&'
			}
		});
		
	DepositOtherController.$inject = ['utilsService', 'depositDirectService'];
	function DepositOtherController(utilsService, depositDirectService) {
		this.$onInit = function() {
			depositDirectService.getPermissions()
				.then(function(data) {
					_this.permissions = data;
				})
		}
		
		var _this = this;
		_this._u = utilsService;
		
		
		_this.activeTab = 1;
		
		_this.switchTab = switchTab;
		
		
		function switchTab(tab) {
			_this.activeTab = tab;
		}
	}
})();