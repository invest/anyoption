/*component::RegisterController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('registerForm', {
			templateUrl: 'app/components/register/register.component.html',
			controller: RegisterController,
			bindings: {
				info: '<',
				onRegister: '&',
				onClose: '&'
			}
		});
		
	RegisterController.$inject = ['utilsService', 'userService', 'countriesService', 'popUpService', 'SingleSelect'];
	function RegisterController(utilsService, userService, countriesService, popUpService, SingleSelect) {
		this.$onInit = function() {
			countriesService.getSortedCountries()
				.then(function(data) {
					_this.countries = data;
					
					_this.formSelect.country = countriesService.defaultCountry;
					// var phoneCodeList = [];
					// for(var i = 0; i < data.length; i++){
					// 	phoneCodeList.push({
					// 		id: data[i].id,
					// 		suffixOptionName: '+' + data[i].phoneCode,
					// 		suffixDisplayName: false,
					// 		name: '+' + data[i].phoneCode,
					// 		displayName: data[i].displayName,
					// 		phoneCode: data[i].phoneCode,
					// 		flagCode: data[i].a2.toLowerCase()
					// 	});
					// }
					// _this.formSelect.country.fillOptions(phoneCodeList);
					// _this.formSelect.country.setById(countriesService.defaultCountry.id);
				})
		}
		
		var _this = this;
		_this._u = utilsService;
		
		_this.reneralTerms = _this._u.getMsg('accept-terms', {_noTrust: true, 'clickEvent': '$ctrl._u.openGeneralTerms(1)'});
		
		_this.form = {
			terms: false
		}
		//a little less input filling for us
		if (isLocal) {_this.form = _this._u.defaultRegisterLocal();}
		
		_this.formSelect = {
			country: {}
			// country: new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: ''}})
		}
		
		_this.submit = submit;
		_this.insertAttempt = insertAttempt;
		
		
		function submit(_form) {
			userService.register(_this, _form)
				.then(function(data) {
					popUpService.open({
						component: 'register-complete',
						data: {
							showDeposit: _this.info.showDeposit
						}
					})
					.then(function(data) {
						_this.onRegister({
							data: data
						});
					})
					.catch(function(data) {
						//if user closed the popup but did not have the option to deposit - 
						if (!_this.info.showDeposit) {
							_this.onRegister({
								data: data
							});
						} else {
							_this.onClose({
								data: data
							});
						}
					});
				});
		}
		
		function insertAttempt(_form) {
			return userService.insertRegisterAttempt(_this, _form);
		}
	}
})();
