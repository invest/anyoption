/*component::GeneralPopupController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('generalPopup', {
			templateUrl: 'app/components/generalPopup/generalPopup.component.html',
			controller: GeneralPopupController,
			bindings: {
				info: '<',
				onResolve: '&',
				onReject: '&'
			}
		});
		
	GeneralPopupController.$inject = ['utilsService'];
	function GeneralPopupController(utilsService) {
		var _this = this;
		_this._u = utilsService;

		_this.action = action;
		
		function action(action) {
			_this.onResolve({data: {action: action}});
		}
	}
})();