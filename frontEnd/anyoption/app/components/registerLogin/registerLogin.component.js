/*component::RegisterLoginController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('registerLogin', {
			templateUrl: 'app/components/registerLogin/registerLogin.component.html',
			controller: RegisterLoginController,
			bindings: {
				onResolve: '&',
				onReject: '&',
				info: '<'
			}
		});
		
	RegisterLoginController.$inject = ['utilsService'];
	function RegisterLoginController(utilsService) {
		var _this = this;
		_this._u = utilsService;
		
		_this.activeTab = 1;
		
		_this.switchTab = switchTab;
		
		
		function switchTab(tab) {
			_this.activeTab = tab;
		}
	}
})();