/*component::DepositSuccessfullController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('depositSuccessfull', {
			templateUrl: 'app/components/depositSuccessfull/depositSuccessfull.component.html',
			controller: DepositSuccessfullController,
			bindings: {
				info: '<',
				onResolve: '&'
			}
		});
		
	DepositSuccessfullController.$inject = ['utilsService'];
	function DepositSuccessfullController(utilsService) {
		var _this = this;
		_this._u = utilsService;
	}
})();