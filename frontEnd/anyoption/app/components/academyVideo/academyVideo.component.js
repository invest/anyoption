/*component::AcademyVideoController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('academyVideo', {
			templateUrl: 'app/components/academyVideo/academyVideo.component.html',
			controller: AcademyVideoController,
			bindings:{
				info: '<',
				onResolve: '&',
				onReject: '&'
			}
		});
		
	AcademyVideoController.$inject = ['utilsService'];
	function AcademyVideoController(utilsService) {
		var _this = this;
		_this._u = utilsService;

		_this.$onInit = function() {
			_this.title = _this._u.getMsg('academy.video.title.' + _this.info.video.id);
			_this.text = _this._u.getMsg('academy.video.long.' + _this.info.video.id);
			
			put_course({
				target_id:'mte_course', 
				ref:'5ac7430',
				settings: 'Embed', 
				language: _this._u.getSkin().locale, 
				product: _this.info.video.product, 
				startWith: _this.info.video.mteOrdId, 
				// lsns: _this.info.video.mteOrdId.toString(),
				playOne:true,
				reset: true,
				demo: (isUndefined(_this.info.video.demo)) ? '' : _this.info.video.demo()
			})
			$(document).on("VIDEO_ENDED", function() {_this.onResolve()});
		}
	}
})();
