
/*component::AssetIndexController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('assetIndex', {
			templateUrl: 'app/components/assetIndex/assetIndex.component.html',
			controller: AssetIndexController
		});
		
	AssetIndexController.$inject = ['assetIndexService', 'utilsService'];
	function AssetIndexController(assetIndexService, utilsService) {
		var _this = this;
		_this._u = utilsService;
		_this.assetIndexMarkets = [];
		_this.assetIndexGeneral = {};
		
		_this.$onInit = function() {
			assetIndexService.get()
				.then(function(data) {
					_this.assetIndexMarkets = data.assetIndexMarkets;
					_this.assetIndexGeneral = data.assetIndexGeneral;
				})
		}
	}
})();
