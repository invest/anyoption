/*component::DepositGeneralController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('depositGeneral', {
			templateUrl: 'app/components/depositGeneral/depositGeneral.component.html',
			controller: DepositGeneralController,
			bindings: {
				onResolve: '&',
				onReject: '&',
				info: '<'
			}
		});
		
	DepositGeneralController.$inject = ['utilsService'];
	function DepositGeneralController(utilsService) {
		var _this = this;
		_this._u = utilsService;
		
		_this.activeTab = 1;
		
		_this.switchTab = switchTab;
		
		
		function switchTab(tab) {
			_this.activeTab = tab;
		}
	}
})();