/*component::IframeUrlController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('iframeUrl', {
			templateUrl: 'app/components/iframeUrl/iframeUrl.component.html',
			controller: IframeUrlController,
			bindings: {
				info: '<',
				onResolve: '&',
				onReject: '&'
			}
		});
		
	IframeUrlController.$inject = ['utilsService'];
	function IframeUrlController(utilsService) {
		var _this = this;
		_this._u = utilsService;

		_this.$onInit = function() {
			$('#the-frame').attr('src', _this.info.url);
			checkIframe();
		}
		
		function checkIframe() {
			var iframeUrl = '';
			try{
				iframeUrl = $('#the-frame').contents().get(0).location.href;
			} catch(e){}
			
			if (iframeUrl.toLowerCase().indexOf(_this.info.checkFor) >= 0) {
				_this.onResolve({data: getUrlValues(iframeUrl)});
			} else {
				setTimeout(function() {
					checkIframe();
				},500);
			}
		}
	}
})();