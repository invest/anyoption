/*component::LoginRegisterController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('loginRegister', {
			templateUrl: 'app/components/loginRegister/loginRegister.component.html',
			controller: LoginRegisterController,
			bindings: {
				info: '<',
				onResolve: '&',
				onReject: '&'
			}
		});
		
	LoginRegisterController.$inject = ['utilsService'];
	function LoginRegisterController(utilsService) {
		var _this = this;
		_this._u = utilsService;
		
	}
})();