/*component::LoginController*/
(function() {
	'use strict';

	angular.module('mainApp')
		.component('loginForm', {
			templateUrl: 'app/components/login/login.component.html',
			controller: LoginController,
			bindings: {
				onLogin: '&',
				info: '<'
			}
		});
		
	LoginController.$inject = ['utilsService', 'userService'];
	function LoginController(utilsService, userService) {
		var _this = this;
		_this._u = utilsService;
		
		var AOETUsername = getAOCookie('AOETUsername');
		if (!isUndefined(AOETUsername)) {
			AOETUsername = AOETUsername.replace(/\"/g, '');
		}
		_this.form = {
			userName: AOETUsername,
			remember: true
		}
		
		_this.submit = submit;
		
		
		function submit(_form) {
			userService.login(_this, _form)
				.then(function(data) {
					_this.onLogin({
						data: data
					})
				})
		}
	}
})();