/* service::onScrollService */
(function() {
	'use strict';

	angular.module('commonServices').service('onScrollService', onScrollService);

	// onScrollService.$inject = [];
	function onScrollService() {
		var _this = this;
		_this.elements = [];
		
		_this.register = register;
		_this.unregister = unregister;
		_this.clean = clean;
		_this.onResizeRegister = onResizeRegister;
		_this.onResizeRegister();


		function register(el) {
			var index = _this.elements.indexOf(el);
			
			if (index == -1) {
				_this.elements.push(el);
			}
		}
		
		function unregister(el) {
			var index = _this.elements.indexOf(el);
			
			if (index > -1) {
				_this.elements.splice(index, 1);
			}
		}
		
		function clean() {
			_this.elements.length = 0;
		}
	
		function onResizeRegister() {
			$(window).scroll(function() {onScroll();});
		}
		
		function onScroll() {
			var $window = $(window);
			var viewport_top = $window.scrollTop();
			
			if (_this.elements.length > 0) {
				$.each(_this.elements, function(index) {
					var $elem = this;
					
					if ($elem.onScrollCallBack != '') {
						$elem.onScrollCallBack({viewport_top: viewport_top});
					}
				})
			}
		}
	}
})();