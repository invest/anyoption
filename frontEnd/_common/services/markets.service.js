/* service::marketsService */
(function() {
	'use strict';

	angular.module('commonServices').service('marketsService', marketsService);

	marketsService.$inject = ['$q', '_http', 'utilsService'];
	function marketsService($q, _http, utilsService) {
		var _u = utilsService;
		var _this = this;
		
		// _this.binaryMarketsCount = 0;
		
		_this.marketGroups = [];//array of objects
		_this.markets = [];//array of objects
		
		_this.dynamicsMarkets = [];//array of ids
		_this.binaryMarkets = [];//array of ids
		
		_this.getGroups = getGroups;
		_this.getMarkets = getMarkets;
		_this.getDynamicsMarkets = getDynamicsMarkets;
		_this.getBinaryMarkets = getBinaryMarkets;
		_this.getMarketInfo = getMarketInfo;

		
		function getGroups() {
			if (_this.marketGroups.length == 0) {
				var request = _u.getMethodRequest();
				
				return _http.get('jsonNew', 'getMarketGroups', request, 'groups')
					.then(function(data) {
						_this.marketGroups = data;
						
						for (var i = 0; i < _this.marketGroups.length; i++) {
							// _this.marketGroups[i].index = i;
							// _this.marketGroups[i].filterInactive = {active: true};
							// _this.marketGroups[i].showInactiveStates = false;
							
							for (var j = 0; j < _this.marketGroups[i].markets.length; j++) {
								// if (_this.marketGroups[i].id != 100 && _this.marketGroups.[i].id != 99) {
									// _this.marketGroups[i].markets[j].binaryIndex = _this.binaryMarketsCount;
									// _this.binaryMarketsCount++;
								// }
								// _this.marketGroups[i].markets[j].index = j;
								_this.markets[_this.marketGroups[i].markets[j].id] = _this.marketGroups[i].markets[j];
								
								if (_this.marketGroups[i].id == 100) {
									_this.dynamicsMarkets.push(_this.marketGroups[i].markets[j].id);
								} else if (_this.marketGroups[i].id < 99) {
									_this.binaryMarkets.push(_this.marketGroups[i].markets[j]);
								}
							}
						}
						
						return _this.marketGroups;
					})
			} else {
				return $q.resolve(_this.marketGroups);
			}
		}
		
		function getMarkets() {
			if (_this.markets.length == 0) {
				return getGroups()
					.then(function(data) {
						return _this.markets;
					})
			} else {
				return $q.resolve(_this.markets);
			}
		}
		
		function getDynamicsMarkets() {
			if (_this.dynamicsMarkets.length == 0) {
				return getGroups()
					.then(function(data) {
						return _this.dynamicsMarkets;
					})
			} else {
				return $q.resolve(_this.dynamicsMarkets);
			}
		}
		
		function getBinaryMarkets() {
			if (_this.binaryMarkets.length == 0) {
				return getGroups()
					.then(function(data) {
						return _this.binaryMarkets;
					})
			} else {
				return $q.resolve(_this.binaryMarkets);
			}
		}

		function getMarketInfo(marketId) {
			if (_this.markets.length == 0) {
				return getGroups()
					.then(function(data) {
						return _this.markets[marketId];
					})
			} else {
				return $q.resolve(_this.markets[marketId]);
			}
		}
	}
})();
