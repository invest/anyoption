/* service::userService */
(function() {
	'use strict';

	angular.module('commonServices').service('userService', userService);

	userService.$inject = ['$q', '_http', 'utilsService'];
	function userService($q, _http, utilsService) {
		var _u = utilsService;
		var _this = this;
		_this.user = {};
		_this.userRegulation = {};
		_this.hasFirstDeposit = null;
		_this.registerAttemptId = null;
		
		_this.get = get;//getUser
		_this.register = register;
		_this.login = login;
		_this.logout = logout;
		_this.setUser = setUser;
		_this.setUserRegulation = setUserRegulation;
		_this.setUserFDId = setUserFDId;
		_this.getUser = getUser;
		_this.getUserRegulation = getUserRegulation;
		_this.getUserVODStatus = getUserVODStatus;
		_this.clearUser = clearUser;
		_this.checkFirstDeposit = checkFirstDeposit;
		_this.setFirstDeposit = setFirstDeposit;
		_this.clearUser();
		_this.insertRegisterAttempt = insertRegisterAttempt;
		

		function get(request) {
			if (request.ifLogged && _u.settings.loggedIn) {
				var request = _u.mergeUMR({
					skinId: _u.getSkin().id,
					encrypt: true
				});
				
				return _http.get('jsonNew', 'getUser', request)
					.then(function(data) {
						setUser(data.user);
						setUserRegulation(data.userRegulation);
						
						_u.setCurrencyFromUser(data.user);
						
						//TODO remove after all in ng
						refreshHeaderAO();
						
						return data;
					})
			} else {
				return $q.resolve({});
			}
		}
		
		function register(scope, _form) {
			var countryId = scope.formSelect.country.id;
			//Check is the country is a SingleSelect - TODO - fix the minisite to use SingleSelect as well
			if(scope.formSelect.country.getId && typeof scope.formSelect.country.getId == 'function'){
				countryId = scope.formSelect.country.getId();
			}
			
			var request = _u.mergeMR({
				register: {
					userName: scope.form.email,
					password: scope.form.password,
					password2: scope.form.password,
					email: scope.form.email,
					firstName: scope.form.firstName,
					lastName: scope.form.lastName,
					countryId: countryId,
					mobilePhone: scope.form.mobilePhone,
					skinId: _u.getSkin().id,
					combinationId: _u.settings.combinationId,
					dynamicParam: _u.settings.dynamicParam,
					httpReferer: _u.settings.httpReferer,
					platformId: 0,
					aff_sub1: _u.settings.aff_sub1,
					aff_sub2: _u.settings.aff_sub2,
					aff_sub3: _u.settings.aff_sub3,
					//probably useless stuff
					timeFirstVisit: 'Oct 28, 2014 9:46:38 AM',
					calcalistGame: false,
					stateId: 0,
					languageId: 0,
					currencyId: 0,
					registerAttemptId: _this.registerAttemptId,
					contactId: 0,
					cityId: 0,
					contactByEmail: true,
					contactBySms: true,
					terms: true,
					deviceUniqueId: ''
				}
			})
			
			return _http.set('jsonNew', 'insertUser', request, scope, _form)
				.then(function(data) {
					setUser(data.user);
					setUserRegulation(data.userRegulation);
					
					_u.settings.loggedIn = true;
					
					_u.setCurrencyFromUser(data.user);
					
					//TODO remove after all in ng
					refreshHeaderAO();
					
					return data;
				})

		}

		function login(scope, _form) {
			var request = _u.mergeUMR({
				userName: scope.form.userName,
				password: scope.form.password,
				login: {
					combinationId: 0,
					dynamicParam: '',
					affSub1: '',
					affSub2: '',
					affSub3: ''
				}
			})
			
			return _http.set('jsonNew', 'getUser', request, scope, _form)
				.then(function(data) {
					setUser(data.user);
					setUserRegulation(data.userRegulation);
					
					_u.settings.loggedIn = true;
					
					if (scope.form.remember) {
						setAOCookie('AOETUsername', scope.form.userName, 365);
					}
					
					_u.setCurrencyFromUser(data.user);
					
					//TODO remove after all in ng
					refreshHeaderAO();
					
					return data;
				})

		}

		function logout() {
			var request = _u.getMethodRequest();
			
			return _http.get('json', 'logoutUser', request)
				.then(function(data) {
					clearUser();
					
					//TODO remove after all in ng
					refreshHeaderAO();
					
					return data;
				})
		}
	
		function setUser(data) {
			_this.user = data;
			if (data.id) {
				_u.settings.loggedIn = true;
			}
		}
		
		function setUserRegulation(data) {
			_this.userRegulation = data;
		}
		
		//called after every deposit
		function setUserFDId(data) {
			if (data.isFirstDeposit) {
				_this.user.firstDepositId = data.transactionId;
				setFirstDeposit(true);
			}
			//TODO remove after all in ng
			refreshHeaderAO();
		}
		
		function getUser() {
			return _this.user;
		}
		
		function getUserRegulation() {
			return (_this.userRegulation) ? _this.userRegulation : {};
		}
		
		function getUserVODStatus() {
			var vod_status = 1;
			if (_u.settings.loggedIn) {
				vod_status = 2;
				if (_this.user.firstDepositId != 0) {
					vod_status = 3;	
				}
			}
			
			return vod_status;
		}
		
		function clearUser() {
			_this.user = {}
			_this.userRegulation = {}
		}
	
		function checkFirstDeposit() {
			if (isUndefined(_this.hasFirstDeposit)) {
				var request = _u.mergeMR({
					userId: getUser().id
				});
				
				return _http.get('json', 'getFirstEverDepositCheck', request)
					.then(function(data) {
						_this.hasFirstDeposit = !data.firstEverDeposit;
						return data.firstEverDeposit;
					})
			} else {
				return $q.resolve(!_this.hasFirstDeposit);
			}
		}
		
		function setFirstDeposit(isFirst) {
			_this.hasFirstDeposit = isFirst;
		}
	
		function insertRegisterAttempt(scope, _form) {
			if ((!isUndefined(scope.form.email) && validateEmailRegEx(scope.form.email)) || 
				(!isUndefined(scope.form.mobilePhone) && scope.form.mobilePhone.length >= 7)) {
				var countryId = scope.formSelect.country.id;
				//Check is the country is a SingleSelect - TODO - fix the minisite to use SingleSelect as well
				if(scope.formSelect.country.getId && typeof scope.formSelect.country.getId == 'function'){
					countryId = scope.formSelect.country.getId();
				}
				var request = _u.mergeMR({
					email: scope.form.email,
					firstName: scope.form.firstName,
					lastName: scope.form.lastName,
					mobilePhone: (scope.form.mobilePhone != '') ? scope.formSelect.country.phoneCode + scope.form.mobilePhone : '',
					countryId: countryId,
					marketingStaticPart: '',
					etsMId: '',
					mId: settings.mId,
					httpReferer: settings.httpReferer,
					combinationId: settings.combinationId,
					dynamicParameter: settings.dynamicParam
				})
				
				return _http.get('json', 'insertRegisterAttempt', request)
					.then(function(data) {
						_this.registerAttemptId = data.registerAttemptId;
						return data;
					})
			}
		}
	}
})();