/* service::onScreenService */
(function() {
	'use strict';

	angular.module('commonServices').service('onScreenService', onScreenService);

	//onScreenService.$inject = [];
	function onScreenService() {
		var _this = this;
		_this.elements = [];
		
		_this.register = register;
		_this.unregister = unregister;
		_this.clean = clean;
		_this.onScrollRegister = onScrollRegister;
		_this.onScrollRegister();
		
		
		function register(el) {
			var index = _this.elements.indexOf(el);
			
			if (index == -1) {
				_this.elements.push(el);
			}
		}
		
		function unregister(el) {
			var index = _this.elements.indexOf(el);
			
			if (index > -1) {
				_this.elements.splice(index, 1);
			}
		}
		
		function clean() {
			_this.elements.length = 0;
		}
	
		function onScrollRegister() {
			$(window).scroll(function() {checkOnScreen();});
			$(window).resize(function() {checkOnScreen();});
		}
		
		function checkOnScreen() {
			if (_this.elements.length > 0) {
				var $window = $(window);
				var viewport_top = $window.scrollTop();
				var viewport_height = $window.height();
				var viewport_bottom = viewport_top + viewport_height;

				for (var i = _this.elements.length - 1; i >= 0; i--) {
					var $elem = _this.elements[i];
					var top = $elem.offset().top;
					var height = $elem.height();
					var bottom = top + height;
					
					//check if element ever show on page
					if (viewport_bottom > top) {
						if (!$elem.hasClass('os-shown')) {
							$elem.addClass('os-shown');
							
							if ($elem.onFirstShowCallBack != '') {
								$elem.onFirstShowCallBack({data: $elem});
							}
							
							if (isUndefined($elem.forceStay) || !$elem.forceStay) {
								_this.unregister($elem);
							}
						}
					}

					if (!isUndefined($elem.forceStay) && $elem.forceStay) {
						//check if element has been scrolled in view and down
						if (viewport_bottom > top) {
							$elem.addClass('os-in-and-down');
						} else {
							$elem.removeClass('os-in-and-down');
						}
						//check if element is curently in view
						if ((viewport_bottom > top) && (viewport_top < bottom)) {
							if (!$elem.hasClass('os-in')) {
								$elem.addClass('os-in');
								
								if ($elem.onShowCallBack != '') {
									$elem.onShowCallBack();
								}
							}
						} else {
							if ($elem.hasClass('os-in')) {
								$elem.removeClass('os-in');

								if ($elem.onHideCallBack != '') {
									$elem.onHideCallBack();
								}
							}
						}
					}
				}
			}
		}
	}
})();