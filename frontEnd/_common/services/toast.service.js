/* service::toastService */
(function() {
	'use strict';

	angular.module('commonServices').service('toastService', toastService);

	toastService.$inject = ['$log', 'translationService', 'toastr', 'toastrConfig'];
	function toastService($log, translationService, toastr, toastrConfig) {
		var _this = this;
		
		_this.showToast = showToast;
		
		toastrConfig.positionClass = 'toast-top-center'
	/*
		params = {
			type: {
				0: error,
				1: success,
				2: info,
				3: waring
			},
			msg: 'free text',
			title: 'free text'
		} 
	*/
		function showToast(params) {
			switch(params.type) {
				case 0:
					var title = (params.title) ? params.title : translationService.getMsg('error');
					toastr.error(params.msg, title, {positionClass: 'toast-top-center'});
					$log.error(params.msg);
					break;
				case 1: 
					toastr.success(params.msg, params.title);
					break;
				case 3: 
					toastr.info(params.msg, params.title);
					break;
				case 4: 
					toastr.warning(params.msg, params.title);
					$log.warning(params.msg);
					break;
			}
		}
	}
})();