/* service::bankWireService */
(function() {
	'use strict';
		
	angular.module('commonServices').service('bankWireService', bankWireService);

	bankWireService.$inject = ['userService'];
	function bankWireService(userService) {
		var _this = this;
		
		_this.getBankWireInfo = getBankWireInfo; 

		
		function getBankWireInfo() {
			var defaultInfo = -1;
			var currencyInfo = -1;
			
			bankWireInfo.map(function(current, index) {
				if (current.currencies.indexOf('all') > -1) {
					defaultInfo = index;
				} else if (current.currencies.indexOf(userService.getUser().currencyId) > -1) {
					currencyInfo = index;
				}
			});
			
			return bankWireInfo[(currencyInfo > -1) ? currencyInfo : defaultInfo];
		}
	}
})();
