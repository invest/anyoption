(function() {
	'use strict';

	angular.module('commonServices').service('_http', _http);

	_http.$inject = ['$q', '$http', 'utilsService'];
	function _http($q, $http, utilsService) {
		var _u = utilsService;
		var _this = this;
		
		_this.get = get;
		_this.set = set;


		function get(servicePath, serviceName, request, returnProperty) {
			if (!request) {request = {};}
			
			return $http.post(getPath(servicePath) + serviceName, request)
				.then(function(data) {
					return _u.handleErrors({data: data});
				})
				.then(function(data) {
					if (returnProperty) {
						return data[returnProperty];
					} else {
						return data;
					}
				})
		}
		
		function set(servicePath, serviceName, request, scope, _form) {
			if (_u.isOkToSubmit(scope, _form)) {
				return $http.post(getPath(servicePath) + serviceName, request)
					.then(function(data) {
						return _u.handleErrors({data: data, form: _form, scope: scope});
					})
					.then(function(data) {
						scope.inAction = false;
						return data;
					})
					.catch(function(data) {
						scope.inAction = false;
						return $q.reject(data);
					})
			} else {
				return $q.reject({});
			}
		}
	
		function getPath(servicePath) {
			switch(servicePath) {
				case 'json': return _u.settings.jsonLink;
				case 'jsonNew': return _u.settings.jsonLinkNew;
				case 'common': return _u.settings.commonServiceLink;
				case 'msg': return _u.settings.msgsPath;
				default: return servicePath;
			}
		}
	}
})();