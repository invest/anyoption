/* service::dynamicsService */
(function() {
	'use strict';

	angular.module('commonServices').service('dynamicsService', dynamicsService);

	dynamicsService.$inject = ['_http', 'utilsService', 'marketsService'];
	function dynamicsService(_http, utilsService, marketsService) {
		var _u = utilsService;
		var _this = this;
		
		_this.marketsInfo = {};//updated date for ls and history {info:{},opps:[],temp:{}}
		_this.chartsMarkets = [100000, 100001, 100002, 100003];//0-3 : id of markets loaded in charts
		_this.chartsMarketsManual = [true, false, false, false];//0-3 : is the market selected by the user or by the system
		
		_this.fillInvestmentsInfo = fillInvestmentsInfo;
		_this.fillChartDataInfo = fillChartDataInfo;

		
		function fillInvestmentsInfo(data) {
			for (var i = 0; i < data.length; i++) {
				var investment = data[i];
				var marketId = investment.marketId;
				var oppId = investment.opportunityId;
				var marketInfo = _this.marketsInfo[marketId];
				
				if (investment.opportunityTypeId == opportunityTypeId.dynamics) {
					if (isUndefined(marketInfo) || marketInfo.opps.length == 0) {
						if (isUndefined(marketInfo)) {
							marketInfo = createMarketObj();
						}
					
						//move all data to a temp place for use after ls is connected
						if (isUndefined(marketInfo.temp)) {
							marketInfo.temp = {
								investments: []
							};
						}
						
						marketInfo.temp.investments.push(investment);
					} else {
						//find which opp is this info for
						var oppArrayIndex = searchJsonKeyInArray(marketInfo.opps, 'oppId', oppId);
						
						if (oppArrayIndex != -1) {
							var invIndex = searchJsonKeyInArray(marketInfo.opps[oppArrayIndex].investments, 'id', investment.id);
							
							if (invIndex == -1) {
								investment = calculateInvProfit(marketInfo.opps[oppArrayIndex], investment);
								investment.timeCreated = adjustFromUTCToLocal(new Date(investment.timeCreated));
								marketInfo.opps[oppArrayIndex].investments.push(investment);
							}
						}
					}
				}
			}
		}
		
		function fillChartDataInfo(data) {
			var marketId = data.marketId;
			var oppId = data.opportunityId;
			
			if (marketId > 0) {
				var marketInfo = _this.marketsInfo[marketId];
				
				_this.chartsMarkets[data.boxId] = marketId;
				
				//if there is no update for this opp
				if (isUndefined(_this.marketsInfo[marketId]) || _this.marketsInfo[marketId].opps.length == 0) {
					_this.marketsInfo[marketId] = createMarketObj();
				
					//move all data to a temp place for use after ls is connected
					_this.marketsInfo[marketId].temp = data;
				
					//set basic info about the market
					marketsService.getMarketInfo(marketId)
						.then(function(data) {
							_this.marketsInfo[marketId].info = {
								marketId: marketId,
								marketName: data.displayName,
								decimalPoint: data.decimalPoint,
								currentOppId: 0,
								currentOppArrayIndex: -1
							}
						})
				} else {
					//find which opp is this info for
					var oppArrayIndex = searchJsonKeyInArray(_this.marketsInfo[marketId].opps, 'oppId', oppId);
					$scope.addHistoryToOpp(marketId, oppArrayIndex, data);
				}
				
				if (data.boxId == 0 && _this.marketsInfo[marketId].opps[_this.marketsInfo[marketId].info.currentOppArrayIndex].investments.length > 0) {
					//TODO NOW
					$timeout(function() {
						angular.element($('#DynamicsOpenMarkets')).scope().selectOpenOption(_this.marketsInfo[marketId]);
					}, 500);
				}
			}
		}
		
		function createMarketObj() {
			//opps:{oppId:number,ls:{},timeLeft:{},times:{},investments:[{}],chartPoints:[{}]}
			return {
				info: {},
				opps: [],
				temp: null
			};
		}
		
		function calculateInvProfit(oppObj, investment) {
			var invEndDate = oppObj.times.invEndDate;
			
			investment.if_correct_profit = getProfit({
				dir: (investment.typeId == 7) ? true : false,
				percent: investment.price,
				amount: _u.amountToFloat(investment.amount)
			});
			
			investment.if_correct_profitAmount = investment.if_correct_profit + amountToFloat(investment.amount);
			investment.if_correct_profitAmountDsp = formatAmount(investment.if_correct_profitAmount, 2);
			// investment.amountDsp = formatAmount(investment.amount, 2, true); - this will filter
			
			var tempEnd = milSecToMinFormat(invEndDate - investment.timeCreated - _u.settings.serverOffset);
			investment.timePercent = 100 - ((100 / (oppObj.times.timeChart * 60)) * (tempEnd.seconds + (oppObj.times.timeForClosing * 60) + (_u.settings.serverOffset / 1000)));
			
			if (investment.timePercent > 100) {
				investment.timePercent = 100;
			}
			
			return investment;
		}
	}
})();
