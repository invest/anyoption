/* service::generalTermsService */
(function() {
	'use strict';

	angular.module('commonServices').service('generalTermsService', generalTermsService);

	generalTermsService.$inject = ['_http', 'utilsService'];
	function generalTermsService(_http, utilsService) {
		var _u = utilsService;
		var _this = this;
		
		_this.get = get;

		
		function get(req) {
			var request = _u.mergeMR(req);
			
			return _http.get('jsonNew', 'getTermsFiles', request)
				.then(function(data) {
					data.filesList.sort(function(a, b) {
						return parseInt(a.orderNum) - parseInt(b.orderNum);
					});
					
					for (var i = 0; i < data.filesList.length; i++) {
						data.filesList[i].html = multipleReplace(replaceParamsGT, data.filesList[i].html);
						
						var linkTitle = data.filesList[i].title;
						if (!linkTitle) {
							$('<div>' + data.filesList[i].html + '</div>').find(".general_terms_h2:eq(0)").each(function(index, el) {
								linkTitle = el.innerHTML;
							});
							if (!linkTitle) {
								$('<div>' + data.filesList[i].html + '</div>').find(".general_terms_h1:eq(0)").each(function(index, el) {
									linkTitle = el.innerHTML;
								});
							}
						}
						if (linkTitle) {
							data.filesList[i].menuTitle = linkTitle;
						}
					}
					
					return data.filesList;
				})
		}
	}
})();