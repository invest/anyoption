/* service::additionalFieldsService */
(function() {
	'use strict';

	angular.module('commonServices').service('additionalFieldsService', additionalFieldsService);

	additionalFieldsService.$inject = ['_http', '$q', 'utilsService', 'userService'];
	function additionalFieldsService(_http, $q, utilsService, userService) {
		var _u = utilsService;
		var _this = this;
		
		_this.update = update;
		
		
		function update(request, scope, _form) {
			if (isUndefined(request.isFirstDeposit) || request.isFirstDeposit) {
				return _http.set('jsonNew', 'updateUserAdditionalFields', request, scope, _form)
					.then(function(data) {
						userService.setUser(_u.merge(userService.getUser(), request.user));
						
						return data;
					})
			} else {
				return $q.resolve({});
			}
		}
	}
})();