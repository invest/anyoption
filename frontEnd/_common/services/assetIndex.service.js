/* service::assetIndexService */
(function() {
	'use strict';

	angular.module('commonServices').service('assetIndexService', assetIndexService);

	assetIndexService.$inject = ['_http', 'utilsService'];
	function assetIndexService(_http, utilsService) {
		var _u = utilsService;
		var _this = this;
		_this.lastConfig = {
			groupName: '',
			displayGroupNameKey: '',
			opportunityTypeId: ''
		};
		_this.assetIndexMarkets = [];//type 1: group, type 2: country, type 3: market
		_this.cols = [];
		
		_this.get = get;

		
		function get(req) {
			var request = _u.getMethodRequest();
			
			return _http.get('jsonNew', 'getAssetIndexMarketsPerSkin', request)
				.then(function(data) {
					_this.assetIndexGeneral = {
						tooltipData: data.tooltipData,
						tooltipDataUnderTable: data.tooltipDataUnderTable,
						assetIndexFormulasBySkin: data.assetIndexFormulasBySkin
					}

					data.marketList.map(function(marketRow) {
						if (marketRow.opportunityTypeId == 1) {
							if (_this.lastConfig.groupName != marketRow.market.groupName) {
								if (!isUndefined(marketRow.market.groupName)) {
									_this.assetIndexMarkets.push({
										title: _u.getMsg(marketRow.market.groupName),
										type: 1
									});
									_this.lastConfig.groupName = marketRow.market.groupName;
								}
							}
							
							if (_this.lastConfig.displayGroupNameKey != marketRow.market.displayGroupNameKey) {
								if (!isUndefined(marketRow.market.displayGroupNameKey)) {
									_this.assetIndexMarkets.push({
										title: _u.getMsg(marketRow.market.displayGroupNameKey),
										type: 2
									});
									_this.lastConfig.displayGroupNameKey = marketRow.market.displayGroupNameKey;
								}
							}
						} else {
							if (_this.lastConfig.opportunityTypeId != marketRow.opportunityTypeId) {
								_this.assetIndexMarkets.push({
									title: _u.getMsg('type-name-' + marketRow.opportunityTypeId),
									type: 1
								});
								_this.lastConfig.opportunityTypeId = marketRow.opportunityTypeId;
							}
						}
						
						var temp = {
							type: 3,
							opportunityType: marketRow.opportunityTypeId,
							id: marketRow.market.id,
							marketGroupId: marketRow.marketGroupId,
							title: marketRow.market.displayName,
							description: marketRow.marketAssetIndexInfo.marketDescription,
							descriptionPage: marketRow.marketAssetIndexInfo.marketDescriptionPage,
							additionalText: marketRow.marketAssetIndexInfo.additionalText,
							symbol: marketRow.market.feedName,
							tradingTime: '',
							newMarket: marketRow.market.newMarket,
							is24h7: marketRow.marketAssetIndexInfo.is24h7,
							formulas: []
						}

						var startDay = -1;
						var endDay = -1;
						for (var d = 0; d < 7; d++) {
							if (marketRow.marketAssetIndexInfo.tradingDays[d] == 1 && startDay == -1) {
								startDay = d;
							}
							if (marketRow.marketAssetIndexInfo.tradingDays[d] == 1) {
								endDay = d;
							}
						}
						
						var orgStartDate = new Date(marketRow.marketAssetIndexInfo.startTime);
						var startDate = new Date(new Date(orgStartDate.setDate(orgStartDate.getDate() - orgStartDate.getDay() + startDay)).getTime() - (orgStartDate.getTimezoneOffset() * 60 * 1000));//crazy shit
						
						var orgEndDate = new Date(marketRow.marketAssetIndexInfo.endTime);
						var endDate = new Date(new Date(orgEndDate.setDate(orgEndDate.getDate() - orgEndDate.getDay() + endDay)).getTime() - (orgEndDate.getTimezoneOffset() * 60 * 1000));//crazy shit

						temp.tradingTime += _u.getMsg('day-' + startDate.getDay()) + ' - ' + _u.getMsg('day-' + endDate.getDay());
						temp.tradingTime += ' ' + startDate.getHours() + ':' + (startDate.getMinutes() < 10 ? '0' + startDate.getMinutes() : startDate.getMinutes()) + '-' + endDate.getHours() + ':' + (endDate.getMinutes() < 10 ? '0' + endDate.getMinutes() : endDate.getMinutes());
						
						for (var t = 0; t < marketRow.marketAssetIndexInfo.expiryFormulaCalculations.length; t++) {
							if (t == 0) {
								temp.formulas.push(marketRow.marketAssetIndexInfo.expiryFormulaCalculations[t]);
								temp.formulas[temp.formulas.length - 1].sameNextRow = false;
								temp.formulas[temp.formulas.length - 1].formulaIndex = searchJsonKeyInArray(data.assetIndexFormulasBySkin, 'formulaId', marketRow.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId);
							} else if (marketRow.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryTypeId == 2) {
								temp.formulas.push(marketRow.marketAssetIndexInfo.expiryFormulaCalculations[t]);
								temp.formulas[temp.formulas.length - 1].sameNextRow = false;
								if (marketRow.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId == marketRow.marketAssetIndexInfo.expiryFormulaCalculations[0].expiryFormulaId) {
									temp.formulas[temp.formulas.length - 1].sameNextRow = true;
									temp.formulas[0].sameNextRow = true;
								}
								temp.formulas[temp.formulas.length - 1].formulaIndex = searchJsonKeyInArray(data.assetIndexFormulasBySkin, 'formulaId', marketRow.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId);
							} else if (marketRow.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryTypeId == 3) {
								temp.formulas[temp.formulas.length - 1].expiryTypeId = 3;
							}
						}
						
						_this.assetIndexMarkets.push(temp);
					});

					return {
						assetIndexMarkets: _this.assetIndexMarkets,
						assetIndexGeneral: _this.assetIndexGeneral
					}
				})
		}
	}
})();