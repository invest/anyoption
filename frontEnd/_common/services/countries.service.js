/* service::countriesService */
(function() {
	'use strict';

	angular.module('commonServices').service('countriesService', countriesService);

	countriesService.$inject = ['$q', '_http', 'utilsService'];
	function countriesService($q, _http, utilsService) {
		var _u = utilsService;
		var _this = this;
		_this.ipDetectedCountryId = 0;
		_this.skinDefautlCointryId = 0;
		_this.defaultCountry = {};
		_this.sortedCountries = [];
		
		_this.getSortedCountries = getSortedCountries;
		_this.resetCountries = resetCountries; 

		
		function getSortedCountries() {
			if (_this.sortedCountries.length == 0) {
				var request = _u.getMethodRequest();
				
				return _http.get('jsonNew', 'getSortedCountries', request)
					.then(function(data) {
						_this.ipDetectedCountryId = data.ipDetectedCountryId;
						_this.skinDefautlCointryId = data.skinDefautlCointryId;
						
						var countryId;
						if (isUndefined(data.countriesMap[data.ipDetectedCountryId])) {
							countryId = data.skinDefautlCointryId;
						} else if (data.ipDetectedCountryId != 0) {
							countryId = data.ipDetectedCountryId;
						} else if (data.skinDefautlCointryId != 0) {
							countryId = data.skinDefautlCointryId;
						}
						_this.defaultCountry = data.countriesMap[countryId];
						
						angular.forEach(data.countriesMap, function(value, key) {
							_this.sortedCountries.push(angular.merge(value, {
								id: key
							}));
						});

						return _this.sortedCountries;
					})
			} else {
				return $q.resolve(_this.sortedCountries);
			}
		}
		
		function resetCountries() {
			_this.defaultCountry = {};
			_this.sortedCountries = [];
		}
	}
})();