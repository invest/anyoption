/* service::handleErrorsService */
(function() {
	'use strict';

	angular.module('commonServices').service('handleErrorsService', handleErrorsService);

	handleErrorsService.$inject = ['$q', '$log', 'toastService', 'translationService', '$rootScope', 'settingsService'];
	function handleErrorsService($q, $log, toastService, translationService, $rootScope, settingsService) {
		var _this = this;
		
		//utils
		var _u = {
			settings: settingsService.settings,
			errorCodes: settingsService.errorCodes,
			showToast: toastService.showToast,
			getMsg: translationService.getMsg
		}
		
		_this.handleErrors = handleErrors;

		/*
			params = {
				data: object,
				scope: object,
				form: object,
				options: {
					noError: boolean
				}
			}
		*/
		function handleErrors(params) {
			var data = params.data;
			var scope = params.scope;
			var form = params.form;
			var errorCode = data.errorCode;
			
			var deferred = $q.defer();
			
			var hasScope = true;
			if (scope && scope._errorArray) {
				scope._errorArray.length = 0;
			} else if (scope) {
				scope._errorArray = [];
			} else {
				hasScope = false;
			}
			
			if (errorCode == _u.errorCodes.success) {
				deferred.resolve(data);
			} else {
				if (errorCode == _u.errorCodes.network_error || errorCode == _u.errorCodes.server_error) {
					_u.showToast({
						type: 0,
						msg: _u.getMsg('error-' + errorCode)
					})
				} else if (errorCode == _u.errorCodes.session_expired) {
					$rootScope.$state.go('ln.login', {ln: _u.settings.ln.locale});
				} else {
					if (data.userMessages && data.userMessages.length > 0) {
						for (var i = 0; i < data.userMessages.length; i++) {
							if (form && data.userMessages[i].field != '' && form[data.userMessages[i].field]) {
								form[data.userMessages[i].field].$setValidity('serverError', false);
								form[data.userMessages[i].field].serverError = data.userMessages[i].message;
							} else {
								pushToVeiw(data.userMessages[i]);
							}
						}
					} else {
						pushToVeiw({
							message: _u.getMsg('error-' + errorCode)
						});
					}
				}
				deferred.reject(data);
			}
			return deferred.promise;
			
			function pushToVeiw(msg) {
				if (msg.message != '') {
					if (hasScope && form) {
						scope._errorArray.push(msg);
					} else {
						_u.showToast({
							type: 0,
							msg: msg.message
						})
					}
				}
			}
		}
	}
})();