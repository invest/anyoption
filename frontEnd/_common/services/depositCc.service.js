/* service::depositCcService */
(function() {
	'use strict';

	angular.module('commonServices').service('depositCcService', depositCcService);

	depositCcService.$inject = ['_http', 'utilsService', 'additionalFieldsService', 'userService', '$q', 'popUpService'];
	function depositCcService(_http, utilsService, additionalFieldsService, userService, $q, popUpService) {
		var _u = utilsService;
		var _this = this;
		
		_this.insertCard = insertCard;
		_this.insertDepositCard = insertDepositCard;
		_this.insertFirstDeposit = insertFirstDeposit;
		_this.handleTreeDResponce = handleTreeDResponce;
		_this.finish3dTransaction = finish3dTransaction;
		
		
		function insertCard(request, scope, _form) {
			return _http.set('jsonNew', 'insertCard', request, scope, _form);
		}
		
		function insertDepositCard(request, scope, _form) {
			return _http.set('jsonNew', 'insertDepositCard', request, scope, _form);
		}
		
		function insertFirstDeposit(scope, _form) {
			var birthDayUpdate = scope.form.birthDayUpdate;
			var birthMonthUpdate = scope.form.birthMonthUpdate;
			var birthYearUpdate = scope.form.birthYearUpdate;
			var currencyId = scope.formSelect.curency.id;
			//Check is the country is a SingleSelect - TODO - fix the minisite to use SingleSelect as well
//			if(scope.formSelect.birthDayUpdate.getId && typeof scope.formSelect.birthDayUpdate.getId == 'function'){
//				birthDayUpdate = scope.formSelect.birthDayUpdate.getId();
//				birthMonthUpdate = scope.formSelect.birthMonthUpdate.getId();
//				birthYearUpdate = scope.formSelect.birthYearUpdate.getId();
//				currencyId = scope.formSelect.currencyId.getId();
//			}

			var UpdateUserMethodRequest = _u.mergeUMR({
				isFirstDeposit: scope.isFirstDeposit,
				user: {
					id: userService.getUser().id,
					currencyId: currencyId,
					currency: scope.formSelect.curency,
					street: scope.form.street,
					cityName: scope.form.cityName,
					zipCode: scope.form.zipCode,
					birthDayUpdate: birthDayUpdate,
					birthMonthUpdate: birthMonthUpdate,
					birthYearUpdate: birthYearUpdate
				}
			})

			return additionalFieldsService.update(UpdateUserMethodRequest, scope, _form)
				.then(function(data) {
					if (scope.isFirstDeposit) {
						_u.setCurrency(scope.formSelect.curency);
						scope.isFirstDeposit = false;
					}

					var expMonth = scope.form.expMonth;
					var expYear = scope.form.expYear;
					//Check is the country is a SingleSelect - TODO - fix the minisite to use SingleSelect as well
//					if(scope.formSelect.expMonth.getId && typeof scope.formSelect.expMonth.getId == 'function'){
//						expMonth = scope.formSelect.expMonth.getId();
//						expYear = scope.formSelect.expYear.getId();
//					}
					
					var ccType = detectCCtype(scope.form.ccNumber, true);
					var CardMethodRequest = _u.mergeUMR({
						card: {
							typeId: (isNaN(ccType) ? 0 : ccType),
							ccNumber: scope.form.ccNumber.replace(/-/g, '').replace(/ /g, ''),
							ccPass: scope.form.ccPass,
							expMonth: expMonth,
							expYear: expYear,
							holderName: scope.form.firstName + ' ' + scope.form.lastName,
							holderId: '@"No-Id"'
						}
					})
					
					return insertCard(CardMethodRequest, scope, _form);
				})
				.then(function(data) {
					var CcDepositMethodRequest;
					if (data.error != null) {
						CcDepositMethodRequest = _u.mergeUMR({
							amount: scope.form.amount,
							cardId: data.card.id,
							error: data.error
						})
					} else { // firstNewCardChanged
						CcDepositMethodRequest = _u.mergeUMR({
							amount: scope.form.amount,
							cardId: data.card.id,
							ccPass: scope.form.ccPass
						})
					}
					
					return insertDepositCard(CcDepositMethodRequest, scope, _form);
				})
				.then(function(data) {
					return handleTreeDResponce(data)
						.then(function(data) {
							userService.setUserFDId(data);
							
							return data;
						})
				})
		}
	
		function handleTreeDResponce(data) {
			if (data.threeDParams) {
				if (!isUndefined(data.threeDParams.acsUrl)) {
					return popUpService.open({
						component: 'iframe-post',
						data: {
							url: data.threeDParams.acsUrl,
							postData: data.threeDParams,
						}
					})
					.then(function(data) {
						data = {
							amount: data.a,
							balance: data.b,
							clearingProvider: data.c,
							dollarAmount: data.da,
							errorCode: data.e,
							isFirstDeposit: data.fd,
							transactionId: data.t,
						}

						return data;
					})
					.catch(function() {})
				} else {
					return popUpService.open({
						component: 'iframe-content',
						data: {
							form: data.threeDParams.form,
							checkFor: document.domain,
						}
					})
					.then(function(data) {
						return finish3dTransaction();
					})
					.catch(function() {})
				}
			} else {
				return $q.resolve(data);
			}
		}
		
		function finish3dTransaction() {
			var request = _u.getUserMethodRequest();
			
			return _http.get('json', 'finish3dTransaction', request);
		}
	}
})();