/* service::mainMenuService */
(function() {
	'use strict';

	angular.module('commonServices').service('mainMenuService', mainMenuService);

	// mainMenuService.$inject = [];
	function mainMenuService() {
		var _this = this;
		_this.hamburger;
		
		_this.open = open;
		_this.close = close;
		
		
		function open(element) {
			if (element) {
				_this.hamburger = element;
			}
			
			_this.hamburger.toggleClass('open');
			$('nav').toggleClass('open');
			
			headerAction();
		}
		
		function close() {
			if (!isUndefined(_this.hamburger)) {
				_this.hamburger.removeClass('open');
				$('nav').removeClass('open');
				headerAction();
			}
		}
		
		function headerAction() {
			if ($('nav').hasClass('open')) {
				$('header').addClass('menu-oppend');
			} else {
				$('header').removeClass('menu-oppend');
			}
		}
	}
})();