/* service::depositDirectService */
(function() {
	'use strict';

	angular.module('commonServices').service('depositDirectService', depositDirectService);

	depositDirectService.$inject = ['_http', 'utilsService', 'additionalFieldsService', 'userService', '$q', 'popUpService'];
	function depositDirectService(_http, utilsService, additionalFieldsService, userService, $q, popUpService) {
		var _u = utilsService;
		var _this = this;
		
		_this.getPermissions = getPermissions;
		_this.insertDirectDeposit = insertDirectDeposit;
		_this.finishDirectDeposit = finishDirectDeposit;
		_this.insertFirstDeposit = insertFirstDeposit;
		
		
		function getPermissions() {
			return $q.resolve( {
				direct24: hasDirect24(),
				eps: hasEps(),
				giropay: hasGiropay()
			})
		}
		
		function insertDirectDeposit(request, scope, _form) {
			return _http.set('json', 'insertDirectDeposit', request, scope, _form);
		}
		
		function finishDirectDeposit(params) {
			var request = _u.mergeUMR({
				inatecTransactionId: params.xid,
				cs: params.cs
			});
			
			return _http.get('json', 'finishDirectDeposit', request);
		}
		
		function insertFirstDeposit(scope, _form) {
			var UpdateUserMethodRequest = _u.mergeUMR({
				isFirstDeposit: scope.isFirstDeposit,
				user: {
					id: userService.getUser().id,
					currencyId: scope.formSelect.curency.id,
					currency: scope.formSelect.curency,
					street: scope.form.street,
					cityName: scope.form.cityName,
					zipCode: scope.form.zipCode,
					birthDayUpdate: scope.form.birthDayUpdate,
					birthMonthUpdate: scope.form.birthMonthUpdate,
					birthYearUpdate: scope.form.birthYearUpdate
				}
			})

			return additionalFieldsService.update(UpdateUserMethodRequest, scope, _form)
				.then(function(data) {
					if (scope.isFirstDeposit) {
						_u.setCurrency(scope.formSelect.curency);
						scope.isFirstDeposit = false;
					}
					
					var InatecDepositMethodRequest = _u.mergeUMR({
						depositAmount: scope.form.amount,
						accountNum: scope.form.accountNum,
						bankSortCode: scope.form.bankSortCode,
						beneficiaryName: scope.form.beneficiaryName,
						paymentTypeParam: scope.form.paymentType
					});
					
					return insertDirectDeposit(InatecDepositMethodRequest, scope, _form);
				})
				.then(function(data) {
					if (data.success) {
						return data;
					} else {
						return popUpService.open({
							component: 'iframe-url',
							data: {
								url: data.redirectUrl,
								checkFor: 'txid',
							}
						})
						.then(function(data) {
							return finishDirectDeposit(data)
								.then(function(data) {
									userService.setUserFDId(data);
									return data;
								})
						})
						.catch(function() {})
					}
				})
		}
	
		
		function hasDirect24() {
			var countryIds = [80,15,201,145];
			if (_u.getSkin().isRegulated && userService.getUserRegulation().isWcApproval 
				&& _u.getCurrency().id == currencyConst.eur && countryIds.indexOf(userService.getUser().countryId) > -1) {
				return true;
			} else {
				return false;
			}
		}
		
		function hasGiropay() {
			if (_u.getCurrency().id == currencyConst.eur && userService.getUser().countryId == 80) {
				return true;
			} else {
				return false;
			}
		}
		
		function hasEps() {
			if (_u.getSkin().isRegulated && userService.getUserRegulation().isWcApproval
				&& _u.getCurrency().id == currencyConst.eur && userService.getUser().countryId == 15) {
				return true;
			} else {
				return false;
			}
		}
	}
})();