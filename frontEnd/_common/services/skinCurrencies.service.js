/* service::skinCurrenciesService */
(function() {
	'use strict';

	angular.module('commonServices').service('skinCurrenciesService', skinCurrenciesService);

	skinCurrenciesService.$inject = ['$q', '_http', 'utilsService'];
	function skinCurrenciesService($q, _http, utilsService) {
		var _u = utilsService;
		var _this = this;
		_this.results = {
			currencies: [],
			predefinedDepositAmounts: {}
		}
		
		_this.get = get;
		
		
		function get() {
			if (_this.results.currencies.length == 0) {
				var request = _u.getMethodRequest();
				
				return _http.get('json', 'getSkinCurrencies', request)
					.then(function(data) {
						data.currency = data.currency.map(function(current) {
							current.name = current.symbol;

							//TODO renive when single select receive parameters
							current.displayName = current.symbol;
							return current;
						});

						_this.results = {
							currencies: data.currency,
							predefinedDepositAmounts: data.predefinedDepositAmountMap
						}
						
						return _this.results;
					})
			} else {
				return $q.resolve(_this.results);
			}
		}
	}
})();
