/* service::popupLogicService */
(function() {
	'use strict';
		
	angular.module('commonServices').service('popupLogicService', popupLogicService);

	popupLogicService.$inject = ['popUpService', 'utilsService', 'userService', '$q'];
	function popupLogicService(popUpService, utilsService, userService, $q) {
		var _u = utilsService;
		var _this = this;
		
		_this.openGeneralPopup = openGeneralPopup;


		function openGeneralPopup(params) {
			//if you want to open register/login popup but you are already logged in
			if (_u.settings.loggedIn && params.component == 'register-login') {
				return $q.resolve({user: userService.getUser()});
			}
			
			return popUpService.open({
				component: params.component,
				data: params.info
			})
		}
	}
})();
