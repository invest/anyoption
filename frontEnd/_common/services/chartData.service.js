/* service::chartDataService */
(function() {
	'use strict';

	angular.module('commonServices').service('chartDataService', chartDataService);

	chartDataService.$inject = ['_http', 'utilsService'];
	function chartDataService(_http, utilsService) {
		var _u = utilsService;
		var _this = this;

		_this.get = get;

		
		function get(params) {
			var request = _u.mergeUMR({
				box: params.boxId, 
				marketId: params.marketId, 
				opportunityId: 0, 
				opportunityTypeId: opportunityTypeId.dynamics
			});
				
			return _http.get('jsonNew', 'getChartDataCommon', request)
				.then(function(data) {
					data.boxId = params.boxId;
					
					return data;
				})

		}
	}
})();
