/* service::utilsService */
(function() {
	'use strict';
		
	angular.module('commonServices').service('utilsService', utilsService);

	utilsService.$inject = ['toastService', 'translationService', 'settingsService', 'handleErrorsService', '$stateParams', '$rootScope', 'popUpService'];
	function utilsService(toastService, translationService, settingsService, handleErrorsService, $stateParams, $rootScope, popUpService) {
		var _this = this;

		_this.monthsList = [];
		_this.device = detectDevice();
		//from toastr
		_this.showToast = toastService.showToast;
		//from translationService
		_this.getMsg = translationService.getMsg;
		//from settingsService
		_this.settings = settingsService.settings;
		_this.getSkin = settingsService.getSkin;
		_this.setSkin = settingsService.setSkin;
		_this.getCurrency = settingsService.getCurrency;
		_this.setCurrency = settingsService.setCurrency;
		_this.setCurrencyFromUser = settingsService.setCurrencyFromUser;
		_this.errorCodes = settingsService.errorCodes;
		_this.skinMap = settingsService.skinMap;
		//from handleErrorsService
		_this.handleErrors = handleErrorsService.handleErrors;
		// from $stateParams
		_this.$stateParams = $rootScope.stateParams;
		_this.targetState = $rootScope.targetState;
		//new
		_this.goTo = goTo;
		_this.redirect = redirect;
		_this.isOkToSubmit = isOkToSubmit;
		_this.Range = Range;
		_this.merge = merge;
		_this.mergeMR = mergeMR;//merge with methodRequest
		_this.mergeUMR = mergeUMR;//merge with userMethodRequest
		_this.getMonths = getMonths;
		_this.getMethodRequest = getMethodRequest;
		_this.getUserMethodRequest = getUserMethodRequest;
		_this.downloadApp = downloadApp;
		_this.openGeneralTerms = openGeneralTerms;
		_this.scrollToById = scrollToById;
		_this.goToAndScroll = goToAndScroll;
		_this.amountToFloat = amountToFloat;
		
		_this.defaultRegisterLocal = defaultRegisterLocal;
		_this.defaultDepositLocal = defaultDepositLocal;

		
		//dest = {'string', object}
		function goTo(dest, params) {
			$rootScope.$state.go(dest, params);
		}

		function redirect(link) {
			window.location = link;
		}
		//check if form is valid and not already submited
		function isOkToSubmit(scope, form) {
			if (form.$error && form.$error.serverError) {
				for (var i = form.$error.serverError.length - 1; i >= 0; i--) {
					form.$error.serverError[i].$setValidity('serverError', true);
				}
			}
			if (!scope.inAction) {
				if (scope._errorArray) {
					scope._errorArray.length = 0;
				}
				if (form.$valid) {
					scope.inAction = true;
					return true;
				} else {
//					toastService.showToast({
//						type: 0,
//						msg: translationService.getMsg('error-required-all')
//					})
					return false;
				}
			} else {
				toastService.showToast({
					type: 3,
					msg: translationService.getMsg('warning-inProgress')
				})
				return false;
			}
		}
		//get list of numbers from one number to another
		function Range(start, end) {
			var result = [];
			if (start < end) {
				for (var i = start; i <= end; i++) {
					if (i < 10) {
						result.push('0' + i);
					}
					else {
						result.push(i);
					}
				}
			} else {
				for (var i = start; i >= end; i--) {
					if (i < 10) {
						result.push('0' + i);
					}
					else {
						result.push(i);
					}
				}
			}
			return result;
		};

		function merge(obj1, obj2) {
			return angular.merge({}, obj1, obj2);
		}
		
		function mergeMR(obj) {
			return merge(_this.getMethodRequest(), obj);
		}
		
		function mergeUMR(obj) {
			return merge(_this.getUserMethodRequest(), obj);
		}
		
		function getMonths() {
			if (_this.monthsList.length == 0) {
				for (var i = 1; i <= 12; i++) {
					_this.monthsList.push({
						id: (i < 10) ? '0' + i : i,
						displayName: _this.getMsg('month-' + i)
					})
				}
			}
			return _this.monthsList;
		}
		
		function getMethodRequest() {
			var methodRequest = {
				skinId: settingsService.getSkin().id,
				utcOffset: new Date().getTimezoneOffset(), 
				writerId: settingsService.settings.writerId, 
				locale: ''
			}
			
			return methodRequest;
		}
		
		function getUserMethodRequest() {
			var userMethodRequest = {
				encrypt: false, 
				isLogin: false, 
				afterDeposit: false
			}
			angular.merge(userMethodRequest, _this.getMethodRequest());
			
			return userMethodRequest;
		}
	
		function downloadApp() {
			var device = _this.device;
			var params = '?pid='+_this.settings.utm_source+'&c='+_this.settings.campaginid+'&af_siteid='+_this.settings.combinationId+'&af_sub1='+_this.settings.dynamicParam+'&af_sub2='+_this.settings.utm_medium+'&af_sub3='+_this.settings.utm_content;
			
			if (device == 'android') {
				setTimeout(function(){window.location.href = staticSettings.appsflyerandroid + params;},200);
			} else if(device == 'ios') {
				setTimeout(function(){window.location.href = staticSettings.appsflyerios + params;},200);
			}
		}
		
		function openGeneralTerms(type) {
			var headerText;
			if (type == 2) {
				headerText = 'general-terms';
			} else if (type == 3) {
				headerText = 'risk-disclosure';
			}
			
			popUpService.open({
				component: 'general-terms',
				data: {
					termsType: type,
					headerText: (headerText)? _this.getMsg(headerText) : ''
				}
			})
			.catch(function() {})
		}
		
		function scrollToById(id, adjust, container) {
			adjust = (!isUndefined(adjust)) ? adjust : 0;
			if (!container) {
				container = 'html,body';
			} else {
				adjust -= $('body').scrollTop();
			}
			$(container).last().animate({scrollTop: $("#"+id).offset().top + adjust}, 'swing');
		}
		
		function goToAndScroll(state, id) {
			$rootScope.goToAndScroll_id = id;
			$rootScope.$state.go(state);
		}
		
		function amountToFloat(amount) {
			//TODO currencyDecimalPoint this is from js_vars
			return (amount/Math.pow(10, currencyDecimalPoint));
		}
		//auto fill registration form for local deploys
		function defaultRegisterLocal() {
			return {
				firstName: 'af firstName',
				lastName: 'af lastName',
				email: 'autoFill-' +  Math.ceil(Math.random()*1000000) + '@anyoption.com',
				mobilePhone: 1234567,
				password: 123456,
				terms: true
			}
		}
		//auto fill deposit form for local deploys
		function defaultDepositLocal() {
			return {
				ccNumber: '4200000000000000',
				firstName: 'firstName',
				lastName: 'lastName',
				expMonth: '01',
				expYear: parseInt(new Date().getFullYear().toString().substr(-2)) + 2,
				ccPass: 123,
				street: 'Home sweet home',
				cityName: 'Sofia',
				zipCode: 1758,
				birthDayUpdate: '01',
				birthMonthUpdate: '01',
				birthYearUpdate: 1994,
				accountNum: 'DE07444488880123456789',
				bankSortCode: 'GENODETT488',
				beneficiaryName: 'Firstname Lastname'
			}
		}
	}
})();
