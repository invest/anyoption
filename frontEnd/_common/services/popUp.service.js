/* service::popUpService */
(function() {
	'use strict';

	angular.module('commonServices').service('popUpService', popUpService);

	popUpService.$inject = ['$compile', '$q', '$rootScope', '$document'];
	function popUpService($compile, $q, $rootScope, $document) {
		var _this = this;
		/*
			keep all open popups
			[{
				component: 'string',
				htmlElement: object,
				scope: object
			}]
		*/
		var popUpStack = [];
		var popUpWaiting = [];//keep all popups that wnats to get opent while there is already an open one
		var popupHtml;
		
		_this.open = open;
		_this.close = close;

		/*
			params = {
				component: 'string',
				data: {},
				backgroundClass: 'popup-transparent-bgr'
			}
		*/
		function open(params) {
			var deferred = $q.defer();
			var scope = $rootScope.$new();
			scope.tagName = params.component; 
			if (!params.data) {
				params.data = {}
			}
			
			angular.extend(scope, {
				data: angular.extend(params.data, {
					scrollbarConfig: {
						autoHideScrollbar: false,
						theme: '3d-dark',
						advanced:{
							updateOnContentResize: true
						},
						scrollInertia: 500
					}
				})
			})
			
			scope.resolve = function(data, scope) {
				close({component: scope.tagName});
				deferred.resolve(data);
			}
			scope.reject = function(data, scope) {
				close({component: scope.tagName});
				deferred.reject(data);
			}
			
			
			if (searchJsonKeyInArray(popUpStack, 'component', params.component) == -1) {
				var zIndex = 101 + popUpStack.length;
				
				var bgrClass = params.backgroundClass ? params.backgroundClass : 'anyoption-ng popUp _popupHolder';
				popupHtml = angular.element("<div class='" + bgrClass + "' style='z-index:" + zIndex + "' id='popupHolder' click-out-popup='reject(data, this)'></div>");
				
				var componentHtml = angular.element("<" + params.component + " on-resolve='resolve(data, this)' on-reject='reject(data, this)' class='popup-component' info='data'></" + params.component + ">");
				
				popupHtml.append(componentHtml);
				
				angular.element('body').append($compile(popupHtml)(scope));
				
				angular.element('body').addClass('no-scroll');
				
				angular.element('#container').addClass('blur-bgr');
				
				popUpStack.push({
					component: params.component,
					htmlElement: popupHtml,
					scope: scope
				})
				
				if (params.closeElement) {
					close({
						component: params.closeElement
					})
				}
			}
			
			return deferred.promise;
		}
		
		/*params = {component: 'string'}*/
		function close(params) {
			var index = popUpStack.length - 1;
			if (params && params.component) {
				var stackIndex = searchJsonKeyInArray(popUpStack, 'component', params.component);
				if (stackIndex > -1) {
					index = stackIndex;
				}
			}
			
			if (index != -1 && popUpStack.length > 0) {
				popUpStack[index].htmlElement.remove();
				//$document.off('click', popUpStack[index].scope.parseClick);
				popUpStack[index].scope.$destroy();
				popUpStack.splice(index, 1);
				
				if (popUpStack.length == 0) {
					angular.element('#container').removeClass('blur-bgr');
					angular.element('body').removeClass('no-scroll');
				}
			}
		}

	}
})();