/* service::uploadService */
(function() {
	'use strict';

	angular.module('commonServices').service('uploadService', uploadService);

	uploadService.$inject = ['$q', 'Upload', 'utilsService'];
	function uploadService($q, Upload, utilsService) {
		var _u = utilsService;
		var _this = this;
		
		_this.upload = upload;
		
		function upload(file, data, croppedDataUrl) {
			var deferred = $q.defer();
			if (file) {
				var dataPost = {file: file}
				if (croppedDataUrl) {
					dataPost = Upload.dataUrltoBlob(croppedDataUrl, file.name);
				}
				if (data) {
					angular.extend(dataPost, data);
				}
				
				var request = {
					url: _u.settings.jsonPath + 'files/upload',
					data: dataPost
				}
				
				Upload.upload(request)
					.then(function(data) {
						return _u.handleErrors({data: data});
					})
					.then(function(data) {
						deferred.resolve(data);
					})
					.catch(function(data) {
						deferred.reject(data);
					})
			} else {
				deferred.reject({});
			}
			
			return deferred.promise;
		}
	}
})();