/* service::investmentsService */
(function() {
	'use strict';

	angular.module('commonServices').service('investmentsService', investmentsService);

	investmentsService.$inject = ['_http', 'utilsService', '$q'];
	function investmentsService(_http, utilsService, $q) {
		var _u = utilsService;
		var _this = this;
		_this.openInvList = [];//all open investments
		
		_this.getOpened = getOpened;

		
		function getOpened() {
			if (_this.openInvList.length == 0) {
				var request = _u.mergeUMR({
					isSettled: false,
					startRow: 0
				});
				
				return _http.get('jsonNew', 'getUserInvestments', request, 'investments')
					.then(function(data) {
						data.map(function(current) {
							return {
								marketId: current.marketId,
								marketName: current.asset,
								id: current.id,//inv id
								amount: current.amount,
								amountDsp: current.amountTxt,
								copyopType: current.copyopType,
								level: current.currentLevel,
								eventLevel: current.eventLevel,
								expiryLevel: current.expiryLevel,
								oddsLose: current.oddsLose,
								oddsWin: current.oddsWin,
								price: current.price,
								opportunityId: current.opportunityId,
								opportunityTypeId: current.opportunityTypeId,
								optionPlusFee: current.optionPlusFee,
								scheduledId: current.scheduledId,
								typeId: current.typeId,
								timeCreated: adjustFromUTCToLocal(new Date(current.timeCreated)),
								timeEstClosing: adjustFromUTCToLocal(new Date(current.timeEstClosing))
							};
						});
						
						_this.openInvList = data;
						
						return data;
					})
			} else {
				return $q.resolve(_this.openInvList);
			}
		}
		
	}
})();
