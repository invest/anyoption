/* service::skinSelectorService */
(function() {
	'use strict';

	angular.module('commonServices').service('skinSelectorService', skinSelectorService);

	skinSelectorService.$inject = ['$q', '_http', 'utilsService'];
	function skinSelectorService($q, _http, utilsService) {
		var _u = utilsService;
		var _this = this;
		_this.visibleSkins = []
		
		_this.get = get;
		_this.skinUpdate = skinUpdate;

		
		function get() {
			if (_this.visibleSkins.length == 0) {
				var request = _u.getMethodRequest();
				
				return _http.get('json', 'getVisibleSkins', request, 'visibleSkins')
					.then(function(data) {
						for(var i = 0; i < data.length; i++) {
							_this.visibleSkins.push(_u.skinMap[data[i].skinId]);
						}
						return _this.visibleSkins;
					})
			} else {
				return $q.resolve(_this.visibleSkins);
			}
		}
		
		function skinUpdate(skinId) {
			var request = _u.mergeUMR({
				skinId: skinId
			})
			return _http.get('json', 'skinUpdate', request)
		}
	}
})();
