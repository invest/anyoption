/* service::marqueeService */
(function() {
	'use strict';

	angular.module('commonServices').service('marqueeService', marqueeService);

	//marqueeService.$inject = [];
	function marqueeService() {
		var _this = this;
		_this.elements = [];
		_this.lastScreenSize = 0;
		
		_this.register = register;
		_this.unregister = unregister;
		_this.clean = clean;
		_this.onResizeRegister = onResizeRegister;
		_this.onResizeRegister();
		
		
		function register(el) {
			var index = _this.elements.indexOf(el);
			
			if (index == -1) {
				_this.elements.push(el);
				onResize();
			}
		}
		
		function unregister(el) {
			var index = _this.elements.indexOf(el);
			
			if (index > -1) {
				_this.elements.splice(index, 1);
				_this.lastScreenSize = 0;
			}
		}
		
		function clean() {
			_this.elements.length = 0;
		}
	
		function onResizeRegister() {
			$(window).resize(function() {onResize();});
		}
		
		function onResize() {
			if (_this.elements.length > 0) {
				$.each(_this.elements, function(index) {
					var $elem = this;
					var width = $elem.width();
					
					if (_this.lastScreenSize != width) {
						_this.lastScreenSize = width;
						
						$elem.html('<div class="in">' + $elem.orgHtml + '</div>');
						
						var inWidth = $elem.find('.in').width();
						
						if (width >= inWidth) {
							$elem.removeClass($elem.className);
							$elem.html($elem.orgHtml);
						} else {
							$elem.html('<div class="in">' + $elem.orgHtml + '</div><div class="in">' + $elem.orgHtml + '</div>');
							$elem.addClass($elem.className);
						}
					}
				})
			}
		}
	}
})();