/* service::tradingService */
(function() {
	'use strict';

	angular.module('commonServices').service('tradingService', tradingService);

	tradingService.$inject = ['_http', 'utilsService', 'marketsService'];
	function tradingService(_http, utilsService, marketsService) {
		var _u = utilsService;
		var _this = this;
		
		_this.fillInvestmentsInfo = fillInvestmentsInfo;
		_this.fillChartDataInfo = fillChartDataInfo;

		
		function fillInvestmentsInfo(marketsInfoObj, data, oppTypeId) {
			for (var i = 0; i < data.length; i++) {
				var investment = data[i];
				var marketId = investment.marketId;
				var oppId = investment.opportunityId;
				
				if (investment.opportunityTypeId == oppTypeId) {
					if (isUndefined(marketsInfoObj[marketId]) || marketsInfoObj[marketId].opps.length == 0) {
						if (isUndefined(marketsInfoObj[marketId])) {
							marketsInfoObj[marketId] = createMarketObj();
						}
					
						//move all data to a temp place for use after ls is connected
						if (isUndefined(marketsInfoObj[marketId].temp)) {
							marketsInfoObj[marketId].temp = {
								investments: []
							};
						}
						
						marketsInfoObj[marketId].temp.investments.push(investment);
					} else {
						//find which opp is this info for
						var oppArrayIndex = searchJsonKeyInArray(marketsInfoObj[marketId].opps, 'oppId', oppId);
						
						if (oppArrayIndex != -1) {
							var invIndex = searchJsonKeyInArray(marketsInfoObj[marketId].opps[oppArrayIndex].investments, 'id', investment.id);
							
							if (invIndex == -1) {
								investment = calculateInvProfit(marketsInfoObj[marketId].opps[oppArrayIndex], investment);
								investment.timeCreated = adjustFromUTCToLocal(new Date(investment.timeCreated));
								marketsInfoObj[marketId].opps[oppArrayIndex].investments.push(investment);
							}
						}
					}
				}
			}
		}
		
		function fillChartDataInfo(marketsInfoObj, data) {
			var marketId = data.marketId;
			var oppId = data.opportunityId;
			
			if (marketId > 0) {
				//TODO now
				_this.chartsMarkets[data.boxId] = marketId;
				
				//if there is no update for this opp
				if (isUndefined(marketsInfoObj[marketId]) || marketsInfoObj[marketId].opps.length == 0) {
					marketsInfoObj[marketId] = createMarketObj();
				
					//move all data to a temp place for use after ls is connected
					marketsInfoObj[marketId].temp = data;
				
					//set basic info about the market
					marketsService.getMarketInfo(marketId)
						.then(function(data) {
							marketsInfoObj[marketId].info = {
								marketId: marketId,
								marketName: data.displayName,
								decimalPoint: data.decimalPoint,
								currentOppId: 0,
								currentOppArrayIndex: -1
							}
						})
				} else {
					//find which opp is this info for
					var oppArrayIndex = searchJsonKeyInArray(marketsInfoObj[marketId].opps, 'oppId', oppId);
					//TODO now
					$scope.addHistoryToOpp(marketId, oppArrayIndex, data);
				}
				
				//TODO NOW
				if (data.boxId == 0 && marketsInfoObj[marketId].opps[marketsInfoObj[marketId].info.currentOppArrayIndex].investments.length > 0) {
					$timeout(function() {
						angular.element($('#DynamicsOpenMarkets')).scope().selectOpenOption(marketsInfoObj[marketId]);
					}, 500);
				}
			}
		}
		
		function createMarketObj() {
			//opps:{oppId:number,ls:{},timeLeft:{},times:{},investments:[{}],chartPoints:[{}]}
			return {
				info: {},
				opps: [],
				temp: null
			};
		}
		
		function calculateInvProfit(oppObj, investment) {
			var invEndDate = oppObj.times.invEndDate;
			
			investment.if_correct_profit = getProfit({
				dir: (investment.typeId == 7) ? true : false,
				percent: investment.price,
				amount: _u.amountToFloat(investment.amount)
			});
			
			investment.if_correct_profitAmount = investment.if_correct_profit + amountToFloat(investment.amount);
			investment.if_correct_profitAmountDsp = formatAmount(investment.if_correct_profitAmount, 2);
			// investment.amountDsp = formatAmount(investment.amount, 2, true); - this will filter
			
			var tempEnd = milSecToMinFormat(invEndDate - investment.timeCreated - _u.settings.serverOffset);
			investment.timePercent = 100 - ((100 / (oppObj.times.timeChart * 60)) * (tempEnd.seconds + (oppObj.times.timeForClosing * 60) + (_u.settings.serverOffset / 1000)));
			
			if (investment.timePercent > 100) {
				investment.timePercent = 100;
			}
			
			return investment;
		}
	}
})();
