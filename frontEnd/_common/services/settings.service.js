/* service::settingsService */
(function() {
	'use strict';

	angular.module('commonServices').service('settingsService', settingsService);

	settingsService.$inject = ['$q', '$http', '$state', '$rootScope'];
	function settingsService($q, $http, $state, $rootScope) {
		var _this = this;
		
		_this.finished = false;
		_this.settings = staticSettings;
		_this.errorCodes = errorCodeMap;
		_this.skinMap = skinMap;
		_this.skin = {}
		_this.curency = {}
		
		_this.get = get;
		_this.setSkin = setSkin;
		_this.getSkin = getSkin;
		_this.getCurrency = getCurrency;
		_this.setCurrency = setCurrency;
		_this.setCurrencyFromUser = setCurrencyFromUser;

		
		function get() {
			var deferred = $q.defer();
			
			var urlLnId = parseInt(searchJsonKeyInJson(skinMap, 'locale', $rootScope.$state.params.ln));
			var skinIdCookie = getAOCookie('id_ln');
			var locale = '';
			var skinIdTemp = 16;
			
			if (!isUndefined(skinIdCookie) && skinIdCookie != '') {
				skinIdTemp = skinIdCookie;
			} else {
				skinIdTemp = 0;
				if (urlLnId > -1) {
					locale = $rootScope.$state.params.ln;
				}
			}
			
			var request = {
				skinId: skinIdTemp,
				locale: locale
			}
			
			$http.post(staticSettings.jsonLinkNew + 'defineCopyopWebSkin', request)
				.then(function(data) {
					if (!isUndefined(window.userLogin)) {
						data.loggedIn = userLogin;
					}
					_this.finished = true;
					
					_this.settings.loggedIn = data.loggedIn;
					//stuff not sure where to put
					//TODO this one comes from js_vars
					_this.settings.supportFaxNumber = window.faxNumberMsg;
					_this.settings.supportEmail = '';
					_this.settings.serverTime = data.currentTime;
					_this.settings.serverOffset = data.currentTime - new Date().getTime();
					_this.settings.isLocal = isLocal;
					
					if (data.loggedIn) {
						setSkin(skinMap[data.skinId]);
					} else {
						setSkin(checkSkin(skinMap[data.skinId]));
					}
					
					replaceParamsGT['${complaint_form}'] = replaceParamsGT['${complaint_form}'].replace('{locale}', skinMap[data.skinId].locale);
					
					setCurrency({
						id: data.currencyId,
						symbol: data.currencySymbol,
						isLeftSymbol: data.currencyLeftSymbol,
					})
					
					//TODO - following line is AR skin hack
					//$rootScope.isRtl = true;
					$rootScope.rtlChecked = true;
					
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
			
			return deferred.promise;
		}
		
		function checkSkin(skin) {
			var index = searchJsonKeyInJson(skinMap, 'locale', $rootScope.stateParams.ln);
			
			//TODO TODO TODO - this is temporary until all AO is in angular
			//it is for LPs
			var forecedSkin = getUrlValue_ng('s');
			if (forecedSkin != '') {
				index = searchJsonKeyInJson(skinMap, 'id', forecedSkin);
			}
			
			if (skin.isRegulated && index > -1) {
				if (!skinMap[index].isRegulated) {
					var index = searchJsonKeyInJson(skinMap, 'id', skinMap[index].rgulatedSkinId);
				}
			}
			
			if (index > -1 && $rootScope.stateParams.ln != skin.locale) {
				return skinMap[index];
			} else {
				var id_ln = getAOCookie('id_ln');
				var index = searchJsonKeyInJson(skinMap, 'id', id_ln);
				if (index == -1 || isUndefined(id_ln) || id_ln == '' || id_ln == -1) {
					return skin;
				} else {
					return skinMap[index];
				}
			}
		}
		
		function getSkin() {
			return _this.skin;
		}
		
		function setSkin(skin) {
			_this.skin = skin;
			setAOCookie('id_ln', _this.skin.id, 365);
		}
		
		function getCurrency() {
			return _this.curency;
		}
		
		function setCurrency(curency) {
			$.extend(curency,{
				//TODO this comes from js_vars
				minFirstDeposit: window.minFirstDeposit
			});
			
			_this.curency = curency;
		}
		
		function setCurrencyFromUser(user) {
			setCurrency({
				id: user.currency.id,
				symbol: user.currency.defaultSymbol,
				isLeftSymbol: (user.currency.isLeftSymbol == 1) ? true : false,
			});
		}
	}
})();
