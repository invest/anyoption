/* service::translationService */
(function() {
	'use strict';

	angular.module('commonServices').service('translationService', translationService);

	translationService.$inject = ['$q', '$sce', '$http', 'settingsService', '$compile', '$rootScope'];
	function translationService($q, $sce, $http, settingsService, $compile, $rootScope) {
		var _this = this;
		var settings = settingsService.settings;
		var skinMap = settingsService.skinMap;
		
		_this.msgs = {};
		
		_this.getTransations = getTransations;
		_this.getMsg = getMsg;
		
		
		function getTransations() {
			$rootScope.stateLoaded = false;
			
			return getTransactionsFile(false, true)
				.then(function() {
					if (settingsService.skin.isRegulated) {
						return getTransactionsFile(true, false)
							.then(function() {
								$rootScope.stateLoaded = true;
								return _this.msgs;
							})
					} else {
						return _this.msgs;
					}
				})
		}
		
		function getTransactionsFile(getRegulated, init) {
			var fileName = settings.msgsFileName + skinMap[skinMap[settingsService.skin.id].skinIdTexts].locale;
			if (getRegulated) {
				 fileName += settings.msgsEUsufix;
			}
			fileName += settings.msgsExtension;
			
			return $http.post(settings.msgsPath + fileName, {})
				.then(function(data) {
					if (init) {
						_this.msgs = {}
					}
					
					angular.merge(_this.msgs, data);
					settings.supportEmail = getMsg('support-mail');
					return {};
				})
		}
		
		
		//to replace internal link _noTrust: true is required
		//.getMsg('MESSAGE-KEY', {_noTrust: true, 'ui-sref-PROPERTY': {link: 'STATE', params: {id: 1, testIt: true}}})
		//.getMsg('accept-terms', {_noTrust: true, 'ui-sref-clickEvent': {link: 'ln.banking', params: {id: 1, testIt: true}}})
		function getMsg(key, params) {
			if (!isUndefined(_this.msgs)) {
				if (typeof key != 'object') {
					if (_this.msgs.hasOwnProperty(key)) {
						if (!isUndefined(params)) {
							if (!isUndefined(params._noTrust) && params._noTrust) {
								return msgsParam(_this.msgs[key], params);
							} else {
								return $sce.trustAsHtml(msgsParam(_this.msgs[key], params));
							}
							// return $sce.parseAsResourceUrl($scope.msgsParam($scope.msgs[key], params));
							// return $scope.msgsParam($scope.msgs[key], params);
						} else {
							return _this.msgs[key];
						}
					} else {
						return "?msgs[" + key + "]?";
					}
				} else {
					if (_this.msgs.hasOwnProperty(key[0])) {
						if (_this.msgs[key[0]].hasOwnProperty(key[1])) {
							if (typeof params != 'undefined') {
								return $sce.trustAsHtml(msgsParam(_this.msgs[key[0]][key[1]], params));
							} else {
								return _this.msgs[key[0]][key[1]];
							}
						} else {
							return "?msgs[" + key[0] + '.' + key[1] + "]?";
						}
					} else {
						return "?msgs[" + key[0] + "]?";
					}
				}
			}
		}
		
		//get msg with param
		function msgsParam(msg, params){
			var compile = false;
			for (var key in params) {
				if (params.hasOwnProperty(key)) {
					if (key.startsWith('ui-sref-')) {
						compile = true;
						var property = key.replace('ui-sref-', '');
						
						var href = params[key].link;
						href += '({ln:\"' + settingsService.getSkin().locale + '\"';
						
						for (var param in params[key].params) {
							if (params[key].params.hasOwnProperty(param)) {
								href += ',' + param + ':\"' + params[key].params[param] + '\"';
							}
						}
						href += '})';
						
						msg = msg.replace(new RegExp('{' + property + '}', 'g'), href);
					} else if (params[key] && params[key].replace) {
						msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key].replace(new RegExp('\\$', 'g'), '$$$$')); //Fix for IE and $0, $1, etc. problems with RegEx
					} else {
						msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key]);
					}
				}
			}
			return msg;
		}
	}
})();