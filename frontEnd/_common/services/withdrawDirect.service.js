/* service::withdrawDirectService */
(function() {
	'use strict';

	angular.module('commonServices').service('withdrawDirectService', withdrawDirectService);

	withdrawDirectService.$inject = ['_http', 'utilsService', 'additionalFieldsService', 'userService'];
	function withdrawDirectService(_http, utilsService, additionalFieldsService, userService) {
		var _u = utilsService;
		var _this = this;
		
		_this.getPermissions = getPermissions;
		
		
		function getPermissions() {
			var request = {
				userId: userService.getUser().id
			}
			
			return _http.get('json', 'getPermissionsToDisplay', request)
				.then(function(data) {
					return {
						direct24: data.displayDirect24,
						eps: data.displayEPS,
						giropay: data.displayGiropay,
						ideal: data.displayIdeal
					}
				});
		}
		
	}
})();