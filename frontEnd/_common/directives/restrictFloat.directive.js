/* directive::restrictFloat */
(function() {
	'use strict';

	angular.module('commonDirectives').directive('restrictFloat', restrictFloat);

	function restrictFloat() {
		return {
			require: 'ngModel',
			link: function(scope, elem, attr, ngModel) {
				ngModel.$parsers.unshift(function (value) {
					var val = value.replace(',', '.').split('.');
					value = val[0].replace(regEx.digits_reverse, '');
					if (!isUndefined(val[1])) {
						value += '.' + val[1].replace(regEx.digits_reverse, '');
					}
					elem[0].value = value;
					ngModel.$setViewValue(value);
					return value;
				});
			}
		}
	}
})();