/* directive::formatCcNum */
(function() {
	'use strict';
		
	angular.module('commonDirectives').directive('formatCcNum', formatCcNum);

	function formatCcNum() {
		return {
			require: 'ngModel',
			link: function(scope, elem, attr, ngModel) {
				ngModel.$parsers.unshift(function (value) {
					value = value.replace(regEx.digits_reverse, '');

					var newValue = '';
					for (var i = 1; i <= value.length; i++) {
						if (i > 16) {
							break;
						}
						newValue += (i % 4 == 0 && i < value.length) ? value[i-1] + '    -    ' : value[i-1];
					}
					elem[0].value = newValue;
					if (!isUndefined(elem[0].setSelectionRange)) {
						var strLength = elem[0].value.length;
							setTimeout(function() {
								elem[0].setSelectionRange(strLength, strLength);
							}, 0);
					}
					ngModel.$setViewValue(newValue);
					return newValue;
				});
			}
		}
	}
})();