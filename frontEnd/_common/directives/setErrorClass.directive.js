/* directive::setErrorClass */
(function() {
	'use strict';

	angular.module('commonDirectives').directive('setErrorClass', setErrorClassDirective);

	function setErrorClassDirective() {
		var directive = {
			restrict: 'A',
			scope: {
				input: "=setErrorClass"
			},
			link: link,
		};
		return directive;

		function link(scope, element, attrs) {
			scope.errorFlag = null;	

			
			setTimeout(function() {
				var listOfeleToWatch;
				if (typeof scope.input.$viewValue == 'object') {
					listOfeleToWatch = '[input.$touched, input.$invalid, input.$$parentForm.$submitted]';
				} else {
					listOfeleToWatch = '[input.$touched, input.$invalid, input.$viewValue, input.$$parentForm.$submitted]';
				}

				scope.$watch(listOfeleToWatch, function (newValue, oldValue) {
					if (newValue != oldValue) {
						hasError();
						setClass();
					}
				});
			},0)
			
			function hasError() {
				if (scope.input) {
					if (scope.input.$valid) {
						return scope.errorFlag = false;
					} else if ((scope.input.$$parentForm.$submitted && scope.input.$invalid) || (scope.input.$touched && scope.input.$dirty && scope.input.$invalid)) {
						return scope.errorFlag = true;
					}
				}
				return scope.errorFlag = null;
			}
			
			function setClass() {
				element.removeClass('incorrect correct');
				switch(scope.errorFlag) {
					case true: element.addClass('incorrect');break;
					case false: element.addClass('correct');break;
					default: return '';
				}
			}
		}
	}
})();