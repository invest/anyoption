/* directive::autoFocus */
(function() {
	'use strict';

	angular.module('commonDirectives').directive('expiryDate', expiryDate);

	//expiryDate.$inject = [];
	function expiryDate() {
		return {
			require: "ngModel",
			scope: {
				expMonth : "=expiryDate"
			},
			link: function(scope, element, attributes, ngModel) {
				ngModel.$validators.expiryDate = function(value) {
					var date = new Date();
					return !(date.getFullYear() == (value + 2000) && date.getMonth() >= (parseInt(scope.expMonth)-1));
				};

				scope.$watch("expMonth", function() {
					ngModel.$validate();
				});
			}
		};
	}
})();