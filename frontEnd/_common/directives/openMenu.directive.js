/* directive::openMenu */
(function() {
	'use strict';
	
	angular.module('commonDirectives').directive('openMenu', openMenu);

	openMenu.$inject = ['mainMenuService'];
	function openMenu(mainMenuService) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.bind('click', function() {
					mainMenuService.open(element);
				});
			}
		}
	}
})();