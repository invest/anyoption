/* directive::validEmail */
(function() {
	'use strict';
		
	angular.module('commonDirectives').directive('validEmail', validEmail);

	function validEmail() {
		return {
			require: 'ngModel',
			link: function(scope, elem, attr, ngModel) {
				ngModel.$parsers.unshift(function (value) {
					ngModel.$setValidity('validEmail', regEx.email.test(value));
					return value;
				});
			}
		}
	}
})();