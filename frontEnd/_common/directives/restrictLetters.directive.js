/* directive::restrictLetters */
(function() {
	'use strict';
	
	angular.module('commonDirectives').directive('restrictLetters', restrictLetters);

	function restrictLetters() {
		return {
			require: 'ngModel',
			link: function(scope, elem, attr, ngModel) {
				ngModel.$parsers.unshift(function (value) {
					var regEx_letters_reverse = new XRegExp((regEx.lettersOnly+'').replace(/\//g,'').replace('^[','[^').replace('+$',''),'g');
					value = value.replace(regEx_letters_reverse, '');
					elem[0].value = value;
					ngModel.$setViewValue(value);
					return value;
				});
			}
		}
	}
})();