/* directive::animatedTabs */
(function() {
	'use strict';

	angular.module('commonDirectives').directive('animatedTabs', animatedTabs);

	// animatedTabs.$inject = [];
	function animatedTabs() {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				var amimatedElement = element.find('._animated-element');
				
				var defaultOppened = parseInt(attrs.animatedTabs);
				var targetLis = $('.' + attrs.animatedTabsTarget + '>li');
				var lis = element.find('>li');
				
				if (defaultOppened > 0 && defaultOppened <= lis.length) {
					selectOne($(lis[defaultOppened - 1]));
				}
				
				lis.off('click');

				lis.on('click', function(event) {
					selectOne($(this));
				})
				
				function selectOne(elementClicked) {
					amimatedElement.css({
						left: elementClicked.position().left,
						width: elementClicked.outerWidth(true)
					})
					
					lis.removeClass('active');
					elementClicked.addClass('active');
					
					targetLis.slideUp();
					
					var index = lis.index(elementClicked);
					if (index > -1) {
						$(targetLis[index]).slideDown();
					}
				}
			}
		}
	}
})();