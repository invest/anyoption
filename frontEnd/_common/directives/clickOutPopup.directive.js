/* directive::clickOut */
(function() {
	'use strict';

	angular.module('commonDirectives').directive('clickOutPopup', clickOutPopup);

	clickOutPopup.$inject = ['$document', '$parse', 'utilsService'];
	function clickOutPopup($document, $parse, utilsService) {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				var container;
				var clickOutHandler;
				
				setTimeout(function() {
					container = element.find('*[click-out-popup-cont]');
					// clickOutHandler = $parse(container.attr('click-out-popup-cont'));
					if (container.length > 0) {
						clickOutHandler = $parse(attrs.clickOutPopup);
						element.on('click', parseClick);
					}
				}, 0);
				
				function parseClick(event) {
					if (!document.contains(event.target) || container[0].contains(event.target) || angular.element(event.target).closest('.mCSB_scrollTools').length > 0 || event.target.files) {
						return;
					} else{
						clickOutHandler(scope, {$event: event});
						scope.$apply();
					}
				}
			}
		}
	}
})();