/* directive::eighteenPlus */
(function() {
	'use strict';

	angular.module('commonDirectives').directive('eighteenPlus', eighteenPlus);

	//eighteenPlus.$inject = [];
	function eighteenPlus() {
		return {
			require: "ngModel",
			scope: {
				day : "=epDay",
				month : "=epMonth"
			},
			link: function(scope, element, attributes, ngModel) {
				ngModel.$validators.eighteenPlus = function(value) {
					if (isUndefined(value)) {
						return true;
					}
					var date = new Date();
					var birthday = new Date();
					if (scope.day) {
						birthday.setDate(parseInt(scope.day));
					}
					if (scope.month) {
						birthday.setMonth(parseInt(scope.month) - 1);
					}
					if (value) {
						birthday.setYear(parseInt(value) + 18);
					}
					return (date.getTime() > birthday.getTime());
				};

				scope.$watch('[day,month]', function() {
					ngModel.$validate();
				});
			}
		};
	}
})();