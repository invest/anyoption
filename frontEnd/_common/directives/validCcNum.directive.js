/* directive::validCcNum */
(function() {
	'use strict';
		
	angular.module('commonDirectives').directive('validCcNum', validCcNum);

	function validCcNum() {
		return {
			require: 'ngModel',
			link: function(scope, elem, attr, ngModel) {
				ngModel.$parsers.unshift(function (value) {
					ngModel.$setValidity('validCcNum', !isNaN(detectCCtype(value, true)));
					return value;
				});
			}
		}
	}
})();