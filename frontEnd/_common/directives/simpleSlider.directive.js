/* directive::simpleSlider */
(function() {
	'use strict';
	
	angular.module('commonDirectives').directive('simpleSlider', simpleSlider);

	// simpleSlider.$inject = [];
	function simpleSlider() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				setTimeout(function() {
					var currentPage = 0;
					var pages = 0;
					var elementWidth = element.width();
					var totalWidth = 0;
					
					var ul = element.find('._SL-main');
					var dots = element.find('._SL-dots');
					var scroller = element.find('._SL-scroler');
					var leftArrow = element.find('._SL-left').on('click', function(event) {changePage('prev');});
					var rightArrow = element.find('._SL-right').on('click', function(event) {changePage('next');});
					
					
					ul.width(50000);

					var lisInView = 0;
					ul.find('li').each(function(index, el) {
						totalWidth += $(el).outerWidth(true);
						if (totalWidth <= (elementWidth + 10)) {
							lisInView++;
						}
					});
					
					ul.width(totalWidth);
					
					var initialScroll = scroller.scrollLeft();
					
					pages = Math.ceil(ul.find('li').length / lisInView);
					
					for (let i = 0; i < pages; i++) {
						$('<i></i>').appendTo(dots).on('click', function(event) {
							changePage(i);
						});
					}
					setVisibility();
					
					

					function changePage(page) {
						if (page == 'next') {
							currentPage++;
							if (currentPage >= pages) {
								currentPage = (pages - 1);
							}
						} else if (page == 'prev') {
							currentPage--;
							if (currentPage < 0) {
								currentPage = 0;
							}
						} else {
							currentPage = page;
						}
						
						var scrollTo = 0;
						// $(ul.find('li')[currentPage * lisInView]).position().left + parseInt($(ul.find('li')[currentPage * lisInView]).css('marginLeft'));
						for (var i = 0; i < (currentPage * lisInView); i++) {
							scrollTo += $(ul.find('li')[i]).outerWidth(true);
						}
						
						var dir = '';
						if (scroller.closest('[dir]').attr('dir') == 'rtl') {
							dir = 'rtl';
						}
						if (dir == 'rtl') {
							scrollTo += parseInt($(ul.find('li')[currentPage * lisInView]).css('marginRight'));
						} else {
							scrollTo += parseInt($(ul.find('li')[currentPage * lisInView]).css('marginLeft'));
						}
						if (initialScroll != 0) { //rtl fix
							scrollTo = (totalWidth - elementWidth) - scrollTo;
						} else if (dir == 'rtl') {
							scrollTo = (-1)*scrollTo;
						}
						
						scroller.stop().animate({scrollLeft: scrollTo}, 1000, 'easeOutExpo');
						setVisibility();
					}
					
					function setVisibility() {
						var displayLeftArrow = '';
						var displayRighArrow = '';
						var displayDots = '';
						
						if (pages == 1) {
							displayLeftArrow = 'none';
							displayRighArrow = 'none';
							displayDots = 'none';
						} else if (currentPage == 0) {
							displayLeftArrow = 'none';
						} else if (currentPage == (pages - 1)) {
							displayRighArrow = 'none';
						}
						
						leftArrow.css({display: displayLeftArrow});
						rightArrow.css({display: displayRighArrow});
						dots.css({display: displayDots});
						
						dots.find('i').removeClass('active');
						$(dots.find('i')[currentPage]).addClass('active');
					}
				}, 0);
				
			}
		}
	}
})();