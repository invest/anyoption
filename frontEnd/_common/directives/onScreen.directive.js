/* directive::onScreen */
(function() {
	'use strict';

	angular.module('commonDirectives').directive('onScreen', onScreen);

	onScreen.$inject = ['onScreenService'];
	function onScreen(onScreenService) {
		return {
			restrict: 'A',
			scope: {
				onFirstShow: '&',
				onShow: '&',
				onHide: '&'
			},
			link: function (scope, element, attrs) {
				element.onFirstShowCallBack = scope.onFirstShow;
				element.onShowCallBack = scope.onShow;
				element.onHideCallBack = scope.onHide;
				
				//force not to unsubscribe after element in view
				element.forceStay = attrs.forceStay;
				onScreenService.register(element);
				
				scope.$on('$destroy', function() {
					onScreenService.unregister(element);
				})
			}
		}
	}
})();