/* directive::serverError */
(function() {
	'use strict';
	
	angular.module('commonDirectives').directive('serverError', serverErrorDirective);

	function serverErrorDirective() {
		var directive = {
			restrict: 'A',
			require: 'ngModel',
			link: link,
		};
		return directive;

		function link(scope, element, attrs, ngModel) {
			element.on('keydown', clearServerError);
			
			function clearServerError() {
				ngModel.hasServerError = false;
				ngModel.$setValidity('serverError', true);
			}
		}
	}
})();