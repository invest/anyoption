/* directive::toggleClass */
(function() {
	'use strict';
	
	angular.module('commonDirectives').directive('toggleClass', toggleClass);

	function toggleClass() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.bind('click', function() {
					if (attrs.toggleTarget) {
						element.parent('.' + attrs.toggleTarget).toggleClass(attrs.toggleClass);
					} else {
						element.toggleClass(attrs.toggleClass);
					}
				});
			}
		}
	}
})();