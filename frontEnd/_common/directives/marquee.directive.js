/* directive::marquee */
(function() {
	'use strict';

	angular.module('commonDirectives').directive('marquee', marquee);

	marquee.$inject = ['marqueeService'];
	function marquee(marqueeService) {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				element.addClass('marquee-main');
				
				element.className = attrs.marquee;
				element.orgHtml = element.html();
				
				marqueeService.register(element);
				
				scope.$on('$destroy', function() {
					marqueeService.unregister(element);
				})
			}
		}
	}
})();