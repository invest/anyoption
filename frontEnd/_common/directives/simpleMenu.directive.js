/* directive::simpleMenu */
(function() {
	'use strict';
	
	angular.module('commonDirectives').directive('simpleMenu', simpleMenu);

	simpleMenu.$inject = ['$document'];
	function simpleMenu($document) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.find('li').each(function() {
					if ($(this).find(' > ul').length > 0) {
						var li = $(this);

						li.find(' > a').on('mouseover', function(event) {
							li.addClass('ao-opened');
						})
						li.on('mouseleave', function(event) {
							li.removeClass('ao-opened');
						})
					}
					
				});
				
				function parseClick(event) {
					if (clickedElement.find(' > a')[0].contains(event.target)) {
						return;
					} else{
						clickedElement.removeClass('ao-opened');
						clickedElement = '';
						$document.off('click', parseClick);
						scope.$apply();
					}
				}
			}
		}
	}
})();