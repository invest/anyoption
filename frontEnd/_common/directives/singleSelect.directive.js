/* directive::simpleSlider */
(function() {
	'use strict';
	
	angular.module('commonDirectives').directive('singleSelect', ['$document', '$timeout', '$rootScope', function($document, $timeout, $rootScope) {
		return {
			restrict: 'E',
			require: '?ngModel',
			template: '<ng-include src="template()"></ng-include>',
			//templateUrl: 'app/components/singleSelect/single-select.html',
			scope: {
				ngDisabled: '=',
				
			},
			link: function(scope, element, attrs, ngModel) {
				scope.templateName = '';
				scope.template = function(){
					scope.templateName = attrs.template;
					return scope.templateName ? 'app/components/singleSelect/single-select-' + scope.templateName + '.html' : 'app/components/singleSelect/single-select.html';
				}
				
				scope.getMsgs = $rootScope.getMsgs;
				
				ngModel.$overrideModelOptions({
					allowInvalid: true
				});
				ngModel.$isEmpty = function(value){
					if(!value.model || !value.model.model || !value.model.model.name || value.model.model.id === -1){
						return true;
					}
					return false;
				}
				ngModel.$formatters.push(function(model){
					return {model: model};
				});
				ngModel.$parsers.push(function(viewValue){
					return viewValue.model;
				});
				ngModel.$render = function(){
					scope.singleSelect = ngModel.$viewValue.model;
				};
				scope.selectOption = function(option){
					scope.toggleOpen();
					scope.updateModel(option);
				}
				scope.updateModel = function(option){
					if(scope.ngDisabled){
						return;
					}
					ngModel.$modelValue.setModel(option);
					ngModel.$setViewValue({model: ngModel.$modelValue.clone()});
					ngModel.$commitViewValue();
					ngModel.$render();
				}
				
				scope.toggleOpen = function(forceClose){
					ngModel.$setTouched();
					if(!scope.singleSelect.visible && !forceClose){
						if(scope.ngDisabled){
							return;
						}
						scope.singleSelect.visible = true;
						element.on('transitionend', toggleOpenTransitionEndHandler);
						$document.bind('mousedown', documentClickBind);
					}else{
						scope.singleSelect.visible = false;
						scope.singleSelect.filterText = '';
						element.off('transitionend', toggleOpenTransitionEndHandler);
						$document.unbind('mousedown', documentClickBind);
					}
				}
				
				function toggleOpenTransitionEndHandler() {
					element.find('[data-filter]:eq(0)').focus();
				}
				
				function documentClickBind(event){
					if(!element[0].contains(event.target)){
						scope.$apply(function(){
							scope.toggleOpen(true);
						});
					}
				}
				
				$(element[0]).on('selectstart', function(){
					return false;
				});
				$(element[0]).on('keydown', function(e){
					if(scope.singleSelect.isSmart){
						return;
					}
					if(scope.ngDisabled){
						return;
					}
					if(e.keyCode == 38){//Up key
						scope.$apply(function(){
							var option = ngModel.$modelValue.getPreviousOption();
							if(option){
								scope.updateModel(option);
								scope.updateScroll(option);
							}
						});
						return false;
					}else if(e.keyCode == 40){//Down key
						scope.$apply(function(){
							var option = ngModel.$modelValue.getNextOption()
							if(option){
								scope.updateModel(option);
								scope.updateScroll(option);
							}
						});
						return false;
					}else if(e.keyCode == 13){//Enter
						scope.$apply(function(){
							scope.toggleOpen();
						});
						return false;
					}else if(e.keyCode == 27){//Escape
						scope.$apply(function(){
							scope.toggleOpen(true);
						});
						return false;
					}else if(e.key && !e.ctrlKey){
						scope.$apply(function(){
							var option = ngModel.$modelValue.getNextOptionByKey(e.key);
							if(option){
								scope.updateModel(option);
								scope.updateScroll(option);
								return false;
							}
						});
					}
				});
				
				scope.$watch(function(){
					return scope.singleSelect ? scope.singleSelect.options : null;
				}, function(){
					scope.updateSize();
				}, true);
				
				scope.updateSize = function(){
					$timeout(function(){
						var width;
						if($(element[0]).find('[data-options-box]:eq(0)') && $(element[0]).find('[data-options-box]:eq(0)').length > 0){
							var rect = $(element[0]).find('[data-options-box]:eq(0)')[0].getBoundingClientRect();
							if(rect.width){
								width = rect.width;
							}else{
								width = rect.right - rect.left;
							}
						}
						if(width > 0){
							$(element[0]).width(Math.ceil(width));
						}
					});
				}
				
				scope.updateScroll = function(option){
					$timeout(function(){
						var optionEl = $(element[0]).find('[data-options-box] .single-select-options > span:eq(' + option.index + ')');
						if(optionEl.position().top < 0 || (optionEl.position().top + optionEl.outerHeight() > optionEl.offsetParent().height())){
							optionEl.offsetParent().scrollTop(optionEl.offsetParent().scrollTop() + optionEl.position().top);
						}
						optionEl = null;
					});
				}
			}
		};
	}]);
})();