/* directive::onScroll */
(function() {
	'use strict';

	angular.module('commonDirectives').directive('onScroll', onScroll);

	onScroll.$inject = ['onScrollService'];
	function onScroll(onScrollService) {
		return {
			restrict: 'A',
			scope: {
				onScroll: '&'
			},
			link: function (scope, element, attrs) {
				element.onScrollCallBack = scope.onScroll;

				onScrollService.register(element);
				
				scope.$on('$destroy', function() {
					onScrollService.unregister(element);
				})
			}
		}
	}
})();