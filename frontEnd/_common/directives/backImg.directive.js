(function() {
	'use strict';
		
	angular.module('commonDirectives').directive('backImg', backImg);

	function backImg() {
		return function(scope, element, attrs){
			var url = attrs.backImg;
			if (url != '') {
				element.css({
					'background-image': 'url(' + url +')'
				});
			}
		};
	}
})();