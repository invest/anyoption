/* directive::accordionMenu */
(function() {
	'use strict';
	
	angular.module('commonDirectives').directive('accordionMenu', accordionMenu);

	// accordionMenu.$inject = [];
	function accordionMenu() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.find('li').each(function() {
					if ($(this).find(' > ul').length > 0) {
						var li = $(this);
						//remove other click events
						li.find(' > a').off('click');
						
						li.find(' > a').on('click', function(event) {
							if (li.hasClass('opened')) {
								li.find('> ul').slideUp();
							} else {
								li.find('> ul').slideDown();
							}
							li.toggleClass('opened');
						})
					}
					
				});
			}
		}
	}

	angular.module('commonDirectives').directive('accordionMenuExpandAll', accordionMenuExpandAll);

	// accordionMenuExpandAll.$inject = [];
	function accordionMenuExpandAll() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.on('click', function(event) {
					var lis = element.parents('.' + attrs.accordionMenuExpandAll).find('[accordion-menu] > li');
					
					if (element.hasClass('opened')) {
						lis.removeClass('opened');
						lis.find(' > ul').slideUp();
					} else {
						lis.addClass('opened');
						lis.find(' > ul').slideDown();
					}
					element.toggleClass('opened');
				})
			}
		}
	}
})();