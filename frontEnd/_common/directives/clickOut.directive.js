/* directive::clickOut */
(function() {
	'use strict';

	angular.module('commonDirectives').directive('clickOut', clickOut);

	clickOut.$inject = ['$document', '$parse', 'utilsService'];
	function clickOut($document, $parse, utilsService) {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				var clickOutHandler = $parse(attrs.clickOut);

				scope.$parent.parseClick = parseClick;
				
				setTimeout(function() {
					$document.on('click', parseClick);
					if (utilsService.device == 'ios') {
						$('body').addClass('cursor');
					}
				}, 0);
				
				function parseClick(event) {
					if (element[0].contains(event.target) || angular.element(event.target).closest('.mCSB_scrollTools').length > 0 || event.target.files) {
						return;
					} else{
						clickOutHandler(scope, {$event: event});
						$('body').removeClass('cursor');
						$document.off('click', parseClick);
						scope.$apply();
					}
				}
			}
		}
	}
})();