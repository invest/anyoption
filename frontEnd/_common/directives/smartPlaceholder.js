/* directive::smartPlaceholder */



angular.module('commonDirectives').directive('smartPlaceholder', ['$compile', function($compile) {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, element, attrs) {
			var posTopSmallDefault = '3px';
			//Wrap the element
			var wrapper = angular.element('<div class="smart-placeholder-wrapper"><div class="smart-placeholder-text">{{placeholderText}}</div><i></i></div>');
			var smartPlaceholderText = wrapper.find('.smart-placeholder-text');
			$compile(wrapper)(scope);
			element.after(wrapper);
			wrapper.find('i').before(element);
			
			var posTopSmall = posTopSmallDefault;
			
			//Watch for changes
			scope.$watch(function(){return scope.$eval(attrs.smartPlaceholder);}, function(newValue){
				smartPlaceholderText.text(newValue);
			});
			scope.$watch(function(){return scope.$eval(attrs.placeholderTop);}, function(top){
				posTopSmall = top ? top : posTopSmallDefault;
			});
			
			scope.$watch(function(){return element.val() ? true : false;}, function(forceClear){
				togglePlaceholder(forceClear);
			});			
			
			//Handle focus/blur
			element.on('focus', function(e){
				togglePlaceholder(true);
				$(wrapper).addClass('focused');
			});
			element.on('blur', function(e){
				togglePlaceholder();
				$(wrapper).removeClass('focused');
			});

			function togglePlaceholder(forceClear){
				setTimeout(function(){
					var clear = false;
					if(forceClear){
						clear = true;
					}else{
						if(element[0].value == ''){
							clear = false;
						}else{
							clear = true;
						}
					}
					if(clear){
						$(wrapper).find('.smart-placeholder-text').addClass('small');
						$(wrapper).find('.smart-placeholder-text').css('top', posTopSmall);
					}else{
						var posTop = parseInt(element.css('padding-top')) + parseInt(element.css('margin-top'));
						$(wrapper).find('.smart-placeholder-text').removeClass('small');
						$(wrapper).find('.smart-placeholder-text').css('top', posTop);
					}
				}, 0);
			}
			
			//Cleanup
			scope.$on("$destroy", function() {
				wrapper.after(element);
				wrapper.remove();
			});
		}
	};
}]);