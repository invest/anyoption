/* directive::restrictDigits */
(function() {
	'use strict';

	angular.module('commonDirectives').directive('restrictDigits', restrictDigits);

	function restrictDigits() {
		return {
			require: 'ngModel',
			link: function(scope, elem, attr, ngModel) {
				ngModel.$parsers.unshift(function (value) {
					value = value.replace(regEx.digits_reverse, '');
					elem[0].value = value;
					ngModel.$setViewValue(value);
					return value;
				});
			}
		}
	}
})();