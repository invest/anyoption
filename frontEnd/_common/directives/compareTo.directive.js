/* directive::compareTo */
(function() {
	'use strict';
		
	angular.module('commonDirectives').directive('compareTo', compareTo);

	function compareTo() {
		return {
			require: "ngModel",
			scope: {
				otherModelValue: "=compareTo"
			},
			link: function(scope, element, attributes, ngModel) {
				ngModel.$validators.compareTo = function(value) {
					return value == scope.otherModelValue;
				};

				scope.$watch("otherModelValue", function() {
					ngModel.$validate();
				});
			}
		};
	}
})();