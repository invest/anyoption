﻿/*general function*/
function getAOCookie(c_name){
	var c_value = document.cookie;
	var c_start = c_value.indexOf(" " + c_name + "=");
	if (c_start == -1) {
		c_start = c_value.indexOf(c_name + "=");
	}
	if (c_start == -1) {
		c_value = null;
	} else {
		c_start = c_value.indexOf("=", c_start) + 1;
		var c_end = c_value.indexOf(";", c_start);
		if (c_end == -1) {
			c_end = c_value.length;
		}
		c_value = unescape(c_value.substring(c_start,c_end));
	}
	return c_value;
}

function setAOCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;// + "path=/";
}

function searchJsonKeyInArray(arr, key, val) {
	if (!isUndefined(arr)) {
		for (var i = 0; i < arr.length; i++) {
			if (arr[i][key] == val) {
				return i;
			}
		}
	}
	return -1;
}

function searchJsonKeyInJson(obj, key, val) {
	if (!isUndefined(obj)) {
		for (var el in obj) {
			if (obj[el][key] == val) {
				return el;
			}
		}
	}
	return -1;
}

function isUndefined(el) {
	return (typeof el == 'undefined' || el == null);
}

function detectDevice() {
	if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
		return 'ios';
	} else if (/Android/i.test(navigator.userAgent)) {
		return 'android';
	} else {
		return '';
	}
}

function chunk(arr, size) {
	var newArr = [];
	for (var i=0; i<arr.length; i+=size) {
		newArr.push(arr.slice(i, i+size));
	}
	return newArr;
}

function getUrlValue_ng(key){
	var params = (window.location.search == '') ? staticSettings.url_params : window.location.search;
	var query = params.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		if(key == pair[0]){
			return decodeURIComponent(pair[1]);
		}
	}
	return '';
}

function getUrlValues(url) {
	var params = (window.location.search == '') ? staticSettings.url_params : window.location.search;
	if (!isUndefined(url)) {
		params = url.split('?');
		if (params.length > 1) {
			params = params[1];
		} else {
			return {};
		}
	}
	var query = params.substring(1);
	var vars = query.split('&');
	
	var rtn = {};
	
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		rtn[pair[0]] = pair[1];
	}
	
	return rtn;
}

function getTrackingParams() {
	var combinationId = getUrlValue_ng('combid');
	staticSettings.combinationId = (combinationId == '') ? 0 : combinationId;
	staticSettings.dynamicParam = getUrlValue_ng('dp');
	staticSettings.httpReferer = getAOCookie('hr');
	if (staticSettings.httpReferer == null) {
		staticSettings.httpReferer = document.referrer;
	}
	var mId = getAOCookie('smt')
	staticSettings.mId = (mId == null) ? '' : mId;
	
	staticSettings.utm_source = getUrlValue_ng('utm_source');
	staticSettings.campaginid = getUrlValue_ng('utm_campaign'); 
	staticSettings.utm_medium = getUrlValue_ng('utm_medium');
	staticSettings.utm_content = getUrlValue_ng('utm_content');
	staticSettings.aff_sub1 = getUrlValue_ng('aff_sub1').substr(0, 20);
	staticSettings.aff_sub2 = getUrlValue_ng('aff_sub2').substr(0, 20);
	staticSettings.aff_sub3 = '';
	if (getUrlValue_ng('gclid') != '') {
		staticSettings.aff_sub3 = 'gclid_' + getUrlValue_ng('gclid').substr(0, 30);
	}
}

function showPassowrd(id, el) {
	var input = $('#' + id);
	if (input.length > 0) {
		var type = input[0].type;
		
		if (type == 'password') {
			input[0].type = 'text';
			$(el).addClass('vissible');
		} else {
			input[0].type = 'password';
			$(el).removeClass('vissible');
		}
	}
}

//detect credit card type
function detectCCtype(card, fullCheck){
	var cardTypes = {
		unknown: 'unknown',
		visa: 2,
		mastercard: 1,
		solo: 'solo',
		switch_type: 'switch',
		maestro: 6,
		amex: 5,
		discover: 'discover',
		diners: 3,
		isracard: 4,
		allcards: 'allcards',
		cup: 7
	}
	card = card.toString().replace(/-/g,'').replace(/ /g,'');
	//visa - starts with 4
	var part = parseInt(card.substr(0, 1));
	if(part == 4){
		if(fullCheck){
			if((card.length == 13 || card.length == 16) && luhnValidate(card)){
				return cardTypes.visa;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.visa;
	}
	//Mastercard - starts with 51,52,53,54,55
	var part = parseInt(card.substr(0, 2));
	if(part >= 51 && part <= 55){
		if(fullCheck){
			if(card.length == 16 && luhnValidate(card)){
				return cardTypes.mastercard;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.mastercard;
	}
	//Solo - starts with 6334,6767
	var part = parseInt(card.substr(0, 4));
	if(part == 6334 || part == 6767){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 19 && luhnValidate(card)){
				return cardTypes.solo;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.solo;
	}
	//Switch - starts with 4903,4905,4911,4936,6333,6759 or 564182,633110
	var part_arr = [4903,4905,4911,4936,6333,6759];
	var part2_arr = [564182,633110];
	var part = parseInt(card.substr(0, 4));
	var part2 = parseInt(card.substr(0, 6));
	if(part_arr.indexOf(part) > -1 || part2_arr.indexOf(part2) > -1){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 19 && luhnValidate(card)){
				return cardTypes.switch_type;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.switch_type;
	}
	//Maestro - starts with 50,56,57,58,59,60,62,63,64,67,90
	var part_arr = [50,56,57,58,59,60,62,63,64,67,90];
	var part = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 18 && luhnValidate(card)){
				return cardTypes.maestro;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.maestro;
	}
	//Amex - starts with 34,37
	var part_arr = [34,37];
	var part = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length == 15 && luhnValidate(card)){
				return cardTypes.amex;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.amex;
	}
	//Discover - starts with 6011
	var part_arr = [6011];
	var part = parseInt(card.substr(0, 4));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length == 16 && luhnValidate(card)){
				return cardTypes.discover;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.discover;
	}
	//Diners - starts with 300,301,302,303,304,305 or 36
	var part_arr = [300,301,302,303,304,305];
	var part2_arr = [36];
	var part = parseInt(card.substr(0, 3));
	var part2 = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1 || part2_arr.indexOf(part2) > -1){
		if(fullCheck){
			if(card.length == 14 && luhnValidate(card)){
				return cardTypes.diners;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.diners;
	}
	//CUP - starts with 62
	var part_arr = [62];
	var part = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 19 && luhnValidate(card)){
				return cardTypes.cup;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.diners;
	}
	//Isracard - complicated :)
	if(fullCheck){
		var cardIsr = card;
		if(cardIsr.length == 8){
			cardIsr = "0" + cardIsr;
		}
		else if(cardIsr.length > 9){
			cardIsr = cardIsr.substr(0, 9);
		}
		
		if(cardIsr.length == 9){
			var diff = "987654321",
				sum = 0,
				a = 0,
				b = 0,
				c = 0;
			for(var i=1;i<9;i++){
				sum += parseInt(diff.substr(i,1))*parseInt(cardIsr.substr(i,1));
			}
			if(sum % 11 == 0){
				return cardTypes.isracard;
			}
		}
	}
	//allcard
	if(card.length >= 12 && card.length <= 20 && luhnValidate(card)){
		return cardTypes.allcards;
	}
	return cardTypes.unknown;
}

// The Luhn algorithm is basically a CRC type
// system for checking the validity of an entry.
// All major credit cards use numbers that will
// pass the Luhn check. Also, all of them are based
// on MOD 10.

function luhnValidate(value) {
  // accept only digits, dashes or spaces
	if (/[^0-9-\s]+/.test(value)) return false;
 
	// The Luhn Algorithm. It's so pretty.
	var nCheck = 0, nDigit = 0, bEven = false;
	value = value.replace(/\D/g, "");
 
	for (var n = value.length - 1; n >= 0; n--) {
		var cDigit = value.charAt(n),
			  nDigit = parseInt(cDigit, 10);
 
		if (bEven) {
			if ((nDigit *= 2) > 9) nDigit -= 9;
		}
 
		nCheck += nDigit;
		bEven = !bEven;
	}
 
	return (nCheck % 10) == 0;
}
function drawPath(path, options) {
    options = options || {}
    var duration = options.duration || 5000
    var easing = options.easing || 'ease-in-out'
    var reverse = options.reverse || false
    var undraw = options.undraw || false
    var callback = options.callback || null
    var length = options.length || path.getTotalLength()

    var dashOffsetStates = [length, 0]
    if (reverse) {
        dashOffsetStates = [length, 2 * length]
    }
    if (undraw) {
        dashOffsetStates.reverse()
    }

    // Clear any previous transition
    path.style.transition = path.style.WebkitTransition = 'none';

    var dashArray = path.style.strokeDasharray || path.getAttribute("stroke-dasharray");

    if (dashArray != '') {
        var dashLength = dashArray.split(/[\s,]/).map(function (a) {
            return parseFloat(a) || 0
        }).reduce(function (a, b) {
            return a + b
        })
        var dashCount = length / dashLength + 1
        var a = new Array(Math.ceil(dashCount)).join(dashArray + " ")
        path.style.strokeDasharray = a + '0' + ' ' + length
    } else {
        path.style.strokeDasharray = length + ' ' + length;
    }
    path.style.strokeDashoffset = dashOffsetStates[0];
    path.getBoundingClientRect();
    path.style.transition = path.style.WebkitTransition =
        'stroke-dashoffset ' + duration + 'ms ' + easing;
    path.style.strokeDashoffset = dashOffsetStates[1]
    setTimeout(function() {
    	if (options.reverse) {
    		path.style.strokeDasharray = dashArray //set back original dash array
    	}
    	if (callback) {
    		callback()
    	}
    }, duration)
}

function multipleReplace(map, string) {
	$.each(map, function(k, v) {
		string = string.split(k).join(v);
	});
	return string;
}

(function() {
    var lastTime = 0;
    var vendors = ['webkit', 'moz'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame =
          window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

function fisherYates( array ){
	var count = array.length,
		randomnumber,
		temp;
	while( count ){
		randomnumber = Math.random() * count-- | 0;
		temp = array[count];
		array[count] = array[randomnumber];
		array[randomnumber] = temp
	}
	return array;
}
function adjustFromUTCToLocal(toAdj) {//from tradePage.js
	var adjM = toAdj.getMinutes() - new Date().getTimezoneOffset();
	var adjH = 0;
	if (adjM >= 0) {
		adjH = Math.floor(adjM / 60);
		adjM = adjM % 60;
	} else {
		adjH = Math.floor(Math.abs(adjM) / 60);
		if (Math.abs(adjM) % 60 != 0) {
			adjH += 1;
			adjM = 60 - Math.abs(adjM) % 60;
		} else {
			adjM = Math.abs(adjM) % 60;
		}
		adjH = -adjH;
	}
	toAdj.setHours(toAdj.getHours() + adjH);
	toAdj.setMinutes(adjM);
	return toAdj;
}
function milSecToMin(milSec) {
	return milSec / 60000;
}
function milSecToMinFormat(milSec) {
	if (milSec < 0) {
		milSec = 0;
	}
	var tmp = {};
	var sec = tmp.seconds = Math.floor(milSec / 1000);
	
	var min = 0;
	if (sec > 60) {
		min = Math.floor(sec / 60);
		sec = sec - (min * 60);
	}
	
	var hours = 0;
	if (min > 60) {
		hours = Math.floor(min / 60);
		min = min - (hours * 60);
	}
	
	secDsp = (sec < 10) ? '0' + sec : sec;
	minDsp = (min < 10) ? '0' + min : min;

	if (hours > 0) {
		tmp.formatted = hours + ':';
		tmp.formattedNoZero = hours + ':';
	} else {
		tmp.formatted = '';
		tmp.formattedNoZero = '';
	}
	
	tmp.formatted += minDsp + ':' + secDsp;
	tmp.formattedNoZero += min + ':' + sec;
	tmp.min = min;
	tmp.hours = hours;
	return tmp;
}
function validateEmailRegEx(email) {
	var emailValidationRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return emailValidationRegEx.test(email);
}
//TODO remove afre all in ng
function refreshHeaderAO() {
	try {
		document.getElementById('header').contentWindow.location.reload(true);
	}
	catch(e){};
}

//Polyfill for IE
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position){
      return this.substr(position || 0, searchString.length) === searchString;
  };
}
