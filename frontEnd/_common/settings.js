﻿//Empty line in case of BOM
var isLive = {isLive};
var isLocal = {isLocal};
var staticSettings = {
	domain: window.location.host,
	jsonLink: '{jsonLink}',
	jsonLinkNew: '{jsonLinkNew}',
	commonServiceLink: '{commonServiceLink}',
	msgsPath: 'app/msgs/',
	msgsFileName: 'MessageResources_',
	msgsEUsufix: '_EU',
	msgsExtension: '.json',
	writerId: 1,
	defaultSkinId: 16,
	defaultLanguage: 16,
	loggedIn: false,
	url_params: window.location.search,
	sessionTimeout: (1000*60*31),//31 min
	isLive: isLive,
	url_params: window.location.search || (window.location.hash.indexOf('?') >= 0 ? window.location.hash.substring(window.location.hash.indexOf('?')) : ''),
	shortYearFormat: new Date().getFullYear().toString().slice(-2),
	appsflyerios: '{appsflyerios}',
	appsflyerandroid: '{appsflyerandroid}',
};

staticSettings.shortYear = Number(staticSettings.shortYearFormat);
staticSettings.shortYearFuture = staticSettings.shortYear + 20;

var skinMap = {
	'1': {
		id: 1,
		subdomain:'www',
		locale: 'iw',
		isRegulated: false,
		isAllowed: false,
		skinIdTexts: staticSettings.defaultLanguage,
		regEx_letters: "^['a-zA-Zא-ת ]+$",
		name: ''
	},
	'2': {
		id: 2,
		subdomain:'www',
		locale: 'en',
		isRegulated: false,
		rgulatedSkinId: 16,
		isAllowed: false,
		skinIdTexts: 2,
		regEx_letters: "^['a-zA-Z ]+$",
		name: 'English'
	},
	'3': {
		id: 3,
		subdomain:'tr',
		locale: 'tr',
		isRegulated: false,
		isAllowed: false,
		skinIdTexts: staticSettings.defaultLanguage,
		regEx_letters: "^['a-zA-ZİıÖöÜüÇçĞğŞş ]+$",
		name: 'Türkçe'
	},
	'4': {
		id: 4,
		subdomain:'ar',
		locale: 'ar',
		isRegulated: false,
		isAllowed: false,
		skinIdTexts: staticSettings.defaultLanguage,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'5': {
		id: 5,
		subdomain:'es',
		locale: 'es',
		isRegulated: false,
		rgulatedSkinId: 18,
		isAllowed: false,
		skinIdTexts: 5,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$",
		name: 'Español'
	},
	'8': {
		id: 8,
		subdomain:'de',
		locale: 'de',
		isRegulated: false,
		rgulatedSkinId: 19,
		isAllowed: true,
		skinIdTexts: 8,
		regEx_letters: "^['a-zA-ZäÄöÖüÜß ]+$",
		name: 'Deutsch'
	},
	'9': {
		id: 9,
		subdomain:'it',
		locale: 'it',
		isRegulated: false,
		rgulatedSkinId: 20,
		isAllowed: true,
		skinIdTexts: 9,
		regEx_letters: "^['a-zA-ZÀàÁáÈèÉéÌìÍíÒòÓóÙùÚú ]+$",
		name: 'Italiano'
	},
	'10': {
		id: 10,
		subdomain:'ru',
		locale: 'ru',
		isRegulated: false,
		isAllowed: true,
		skinIdTexts: staticSettings.defaultLanguage,
		regEx_letters: "^['a-zA-ZА-Яа-яЁё ]+$",
		name: 'Русский'
	},
	'12': {
		id: 12,
		subdomain:'fr',
		locale: 'fr',
		isRegulated: false,
		rgulatedSkinId: 21,
		isAllowed: true,
		skinIdTexts: 12,
		regEx_letters: "^['a-zA-Zéèàùâêîôûëïç ]+$",
		name: 'Français'
	},
	'13': {
		id: 13,
		subdomain:'en-us',
		locale: 'en-us',
		isRegulated: false,
		isAllowed: false,
		skinIdTexts: 13,
		regEx_letters: "^['a-zA-Z ]+$",
		name: 'English'
	},
	'14': {
		id: 14,
		subdomain:'es-us',
		locale: 'es-us',
		isRegulated: false,
		isAllowed: false,
		skinIdTexts: 14,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$",
		name: 'Español'
	},
	'15': {
		id: 15,
		subdomain:'sg',
		locale: 'sg',
		isRegulated: false,
		isAllowed: true,
		skinIdTexts: staticSettings.defaultLanguage,
		regEx_letters: "^['a-zA-Z一-龠 ]+$",
		name: '中文'
	},
	'16': {
		id: 16,
		subdomain:'www',
		locale: 'en',
		isRegulated: true,
		isAllowed: true,
		skinIdTexts: 16,
		regEx_letters: "^['a-zA-Z ]+$",
		name: 'English'
	},
	'17': {
		id: 17,
		subdomain:'kr',
		locale: 'kr',
		isRegulated: false,
		isAllowed: true,
		skinIdTexts: staticSettings.defaultLanguage,
		regEx_letters: "^['a-zA-Z\u1100-\u11FF\uAC00-\uD7AF\u3130-\u318F ]+$",
		name: '한국'
	},
	'18': {
		id: 18,
		subdomain:'es',
		locale: 'es',
		isRegulated: true,
		isAllowed: true,
		skinIdTexts: 18,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$",
		name: 'Español'
	},
	'19': {
		id: 19,
		subdomain:'de',
		locale: 'de',
		isRegulated: true,
		isAllowed: true,
		skinIdTexts: 19,
		regEx_letters: "^['a-zA-ZäÄöÖüÜß ]+$",
		name: 'Deutsch'
	},
	'20': {
		id: 20,
		subdomain:'www1',
		locale: 'it',
		isRegulated: true,
		isAllowed: true,
		skinIdTexts: 20,
		regEx_letters: "^['a-zA-ZÀàÁáÈèÉéÌìÍíÒòÓóÙùÚú ]+$",
		name: 'Italiano'
	},
	'21': {
		id: 21,
		subdomain:'fr',
		locale: 'fr',
		isRegulated: true,
		isAllowed: true,
		skinIdTexts: 21,
		regEx_letters: "^['a-zA-Zéèàùâêîôûëïç ]+$",
		name: 'Français'
	},
	'22': {
		id: 22,
		subdomain:'hk',
		locale: 'hk',
		isRegulated: false,
		isAllowed: false,
		skinIdTexts: staticSettings.defaultLanguage,
		regEx_letters: "^['a-zA-Z一-龠 ]+$",
		name: '中文'
	},
	'23': {
		id: 23,
		subdomain:'nl',
		locale: 'nl',
		isRegulated: true,
		isAllowed: true,
		skinIdTexts: 23,
		regEx_letters: "^['a-zA-Zéëï ]+$",
		name: 'Nederlands'
	},
	'24': {
		id: 24,
		subdomain:'se',
		locale: 'sv',
		isRegulated: true,
		isAllowed: true,
		skinIdTexts: 24,
		regEx_letters: "^['a-zA-ZÅÄÖåäö ]+$",
		name: 'Svenska'
	},
	'25': {
		id: 25,
		subdomain:'www',
		locale: 'en',
		isRegulated: false,
		isAllowed: true,
		skinIdTexts: staticSettings.defaultLanguage,
		regEx_letters: "^['a-zA-Z ]+$",
		name: 'English'
	},
	'26': {
		id: 26,
		subdomain:'es',
		locale: 'es',
		isRegulated: false,
		isAllowed: true,
		skinIdTexts: staticSettings.defaultLanguage,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$",
		name: 'Español'
	},
	'27': {
		id: 27,
		subdomain:'cs',
		locale: 'cs',
		isRegulated: true,
		isAllowed: true,
		skinIdTexts: 27,
		regEx_letters: "^['a-zA-Z ]+$",
		name: 'Čeština'
	},
	'28': {
		id: 28,
		subdomain:'pl',
		locale: 'pl',
		isRegulated: true,
		isAllowed: true,
		skinIdTexts: 28,
		regEx_letters: "^['a-zA-Z ]+$",
		name: 'Polski'
	},
	'29': {
		id: 29,
		subdomain:'pt',
		locale: 'pt',
		isRegulated: true,
		isAllowed: true,
		skinIdTexts: 29,
		regEx_letters: "^['a-zA-Z ]+$",
		name: 'Português'
	},
	'30': {
		id: 30,
		subdomain:'ar',
		locale: 'ar',
		isRegulated: false,
		isAllowed: true,
		skinIdTexts: 30,
		regEx_letters: "^['a-zA-Z ]+$",
		name: 'Português'
	}
};

var errorCodeMap = {
	success: 0,
	withdrawal_close: 110,
	invalid_input: 200,
	invalid_account: 201,
	dup_request: 202,
	missing_param: 203,
	login_failed: 205,
	inv_validation: 206,
	general_validation: 207,
	validation_without_field: 208,
	validation_low_balance: 209,
	transaction_failed: 210,
	cc_not_supported: 211,
	invalid_captch: 212,
	no_cash_for_niou: 213,
	regulation_missing_questionnaire: 300,
	regulation_suspended: 301,
	regulation_suspended_documents: 302,
	user_not_regulated: 303,
	regulation_user_restricted: 304,
	reg_suspended_quest_incorrect: 305,
	regulation_user_pep_prohibited: 307,
	regulation_user_is_treshold_block: 308,

	questionnaire_not_full: 400,
	questionnaire_not_supported: 401,
	questionnaire_already_done: 402,
	session_expired: 419,
	opp_not_exists: 500,
	file_too_large: 600,
	user_removed: 800,
	user_blocked: 801,
	unknown: 999,

	et_regulation_additional_info: 700,
	not_accepted_terms: 701,
	et_regulation_knowledge_confirm: 702,
	et_regulation_capital_questionnaire_not_done: 703,
	et_regulation_suspended_low_score: 704,
	et_regulation_suspended_low_treshold: 705,
	et_regulation_suspended_medium_treshold: 706,

	copyop_max_copiers: 1001,

	bubbles_invalid_signature: 3000,
	
	network_error: 10000,
	server_error: 10001
};

var userRegulationBase = {
	regulation_user_registered: 0, 
	regulation_wc_approval: 1,
	regulation_first_deposit: 2,
	regulation_mandatory_questionnaire_done: 3,
	regulation_all_documents_received: 5,
	regulation_all_documents_support_approval: 6,
	regulation_control_approved_user: 7,
	
	et_regulation_knowledge_question_user: 103,
	et_capital_market_questionnaire: 104,
	et_regulation_done: 105,
	
	et_regulation_low_score_group_calc: 9,
	et_regulation_medium_score_group_calc: 17,
	
	et_regulation_unsuspended_score_group_id: 0,
	et_regulation_high_score_group_id: 1,
	et_regulation_medium_score_group_id: 2,
	et_regulation_low_score_group_id: 3,	
	
	ao_regulation_restricted_score_group_id: 4,
	ao_regulation_restricted_low_x_score_group_id: 5,
	ao_regulation_restricted_high_y_score_group_id: 6,

	not_suspended: 0,
	suspended_by_world_check: 1,
	suspended_mandatory_questionnaire_not_filled: 2,
	suspended_due_documents: 3,
	suspended_manually: 4,
	suspended_mandatory_questionnaire_filled_incorrect: 5,

	non_prohibited: 0,
	auto_prohibited: 1,
	writer_prohibited: 2,
	approved_by_compliance: 3,
	false_classification: 4,

	et_suspend_low_score_group: 10,
	et_suspend_treshehold_low_score_group: 11,
	et_suspend_treshehold_medium_score_group: 12,
	
	regulation_user_version: 2, 

	days_from_qualified_time: 30
};

var fileStatus = {
	not_selected: 0,
	requested: 1,
	in_progress: 2,
	done: 3,
	invalid: 4,
	not_needed: 5
};

var fileType = {
	user_id_copy: 1,
	passport: 2,
	cc_copy: 4,
	bankwire_confirmation: 12,
	withdraw_form: 13,
	power_of_attorney: 16,
	account_closing: 17,
	cc_holder_id: 18,
	ssn: 20,
	driver_license: 21,
	utility_bill: 22,
	cc_copy_front: 23,
	cc_copy_back: 24
};

var balanceCallTypes = {
	total: 1,//CALL_TOTAL_AMOUNT_BALANCE
	open_amount: 2,//CALL_OPEN_INVESTMENTS_AMOUNT_BALANCE
	settled_amount: 3,//CALL_SETTLED_INVESTMENTS_AMOUNT_BALANCE
	settled_return: 4//CALL_SETTLED_INVESTMENTS_RETURN_BALANCE
};

var optimoveEvents = {
	login: 1,
	pageVisit: 3,
	loss: 5,
	win: 6,
	deposit: 7,
	withdraw: 8,
	trade: 9
};

var opportunityTypeId = {
	dynamics: 7
};

var regEx = {
	lettersAndNumbers: /^[0-9a-zA-Z]+$/,
	nickname: /^[a-zA-Z0-9!@#$%\^:;"']{1,11}$/,
	nickname_reverse: /[^a-zA-Z0-9!@#$%\^:;"']/g,
	phone : /^\d{7,20}$/,
	digits: /^\d+$/,
	digits_reverse : /\D/g,
	email: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	englishLettersOnly: /^['a-zA-Z ]+$/,
	lettersOnly: "^['\\p{L} ]+$"
};

var replaceParamsGT = {
	'${assets}': '<asset-index></asset-index>',
	'${index_page}': '.',
	'${privacy_link}': './privacy',
	'${complaint_form}': './../anyoption/app/img/pdf/complaint-form/complaint-form-{locale}.pdf'
};

var currencyConst = {
	all: 0,//CURRENCY_ALL_ID
	ils: 1,//CURRENCY_ILS_ID
	usd: 2,//CURRENCY_USD_ID
	eur: 3,//CURRENCY_EUR_ID
	gbp: 4,//CURRENCY_GBP_ID
	'try': 5,//CURRENCY_TRY_ID
	rub: 6,//CURRENCY_RUB_ID
	cny: 7,//CURRENCY_CNY_ID
	kr: 8,//CURRENCY_KR_ID
	sek: 9,//CURRENCY_SEK_ID
	aud: 10,//CURRENCY_AUD_ID
	zar: 11,//CURRENCY_ZAR_ID
	czk: 12,
	pln: 13
};
var payment_type = {
	direct24: 2,//PAYMENT_TYPE_DIRECT24
	giropay: 3,//PAYMENT_TYPE_GIROPAY
	eps: 4//PAYMENT_TYPE_EPS
};

var bankWireInfo = [
	{
		//Regulated EUR 
		currencies: [3],
		accountName: 'Ouroboros Derivatives Trading Limited',
		accountNumber: 992970605,
		iban: 'DE62500100600992970605',
		swift: 'PBNKDEFFXXX',
		bankName: 'Postbank Frankfurt',
		address: 'Frankfurt am Main, 60288, Germany',
		limitations: 'bankwire-limitations-msg-eur'
	},{
		//Regulated USD 
		currencies: ['all'],
		accountName: 'Ouroboros Derivatives Trading Limited',
		accountNumber: 901660661,
		iban: 'DE45590100660901660661',
		swift: 'PBNKDEFF590',
		bankName: 'Postbank Frankfurt',
		address: 'Frankfurt am Main, 60288, Germany',
		limitations: 'bankwire-limitations-msg-usd'
	}
];
