/* factory::SingleSelect */
(function() {
	'use strict';
	
	angular.module('mainApp').service('SingleSelect', ['$timeout', '$rootScope', function($timeout, $rootScope){
		var SingleSelect = function(params){
			var that = this;
			
			if(!params){
				params = {};
			}
			
			this.visible = false;
			
			this.externalModel = params.externalModel;
			if(this.externalModel){
				if(this.externalModel.bindById !== false){
					this.externalModel.bindById = true;
				}
				if(this.externalModel.bindByIdInt !== false){
					this.externalModel.bindByIdInt = true;
				}
			}
			this.validateExternalModel = function(){
				if(this.externalModel && typeof this.externalModel.objCallback == 'function' && (typeof this.externalModel.objCallback() != 'undefined' && this.externalModel.objCallback() != null)){
					return true;
				}
				return false;
			}
			$timeout(function(){
				$rootScope.$watch(function(){
					return that.validateExternalModel() ? that.externalModel.objCallback()[that.externalModel.key] : '';
				}, function(newValue, oldValue){
					if(!that.validateExternalModel()){
						return;
					}
					if(that.externalModel.bindById){
						if(that.model && that.externalModel.objCallback()[that.externalModel.key] == that.model.id){
							return;
						}
						that.model = null;
						for(var i = 0; i < that.options.length; i++){
							if(that.externalModel.objCallback()[that.externalModel.key] == that.options[i].id){
								that.model = (typeof that.model == 'string') ? that.options[i] + '' : that.options[i];
							}
						}
					}else{
						if(that.model && that.externalModel.objCallback()[that.externalModel.key] == that.model){
							return;
						}
						that.model = null;
						for(var i = 0; i < that.options.length; i++){
							if(that.externalModel.objCallback()[that.externalModel.key] == that.options[i]){
								that.model = that.options[i];
							}
						}
					}
				}, true);
			});
			
			this.isSmart = params.isSmart;
			this.noSort = params.noSort;
			this.sortById = params.sortById;
			this.sortByNameInt = params.sortByNameInt;
			this.mutateCallback = params.mutateCallback;
			this.mutateOnlyAggregate = params.mutateOnlyAggregate;
			this.iconCallback = params.iconCallback;
			
			this.iconClosed = params.iconClosed;
			this.iconOpen = params.iconOpen;
			
			this.filterText = '';
			this.filterCallback = params.filterCallback;
			
			this.filterPlaceholder = params.filterPlaceholder;
			
			this.model = null;
			this.options = [];
			this.aggregateOption = params.aggregateOption;
			
			this.setModel = function(option){
				this.model = option;
				if(this.externalModel && typeof this.externalModel.objCallback == 'function'){
					if(this.externalModel.bindById){
						this.externalModel.objCallback()[this.externalModel.key] = this.externalModel.bindByIdInt ? parseInt(this.model.id) : (this.model.id ? this.model.id : null);
					}else{
						this.externalModel.objCallback()[this.externalModel.key] = this.model;
					}
				}
			}
			
			if(this.aggregateOption){
				this.aggregateOption.isAggregate = true;
				this.options.push(this.aggregateOption);
				this.setModel(this.aggregateOption);
			}
			
			this.fillOptions = function(options, setModelToFirst){
				this.options = [];
				if(this.aggregateOption){
					this.options.push(this.aggregateOption);
				}
				if(options && Array.isArray(options)){
					if (options.length > 0 && typeof options[0] != 'object') {
						var tmpOption = [];
						for(var i = 0; i < options.length; i++){
							tmpOption.push({
								id: options[i],
								name: options[i].toString()
							});
						}
						options = tmpOption;
					}

					for(var i = 0; i < options.length; i++){
						this.options.push(options[i]);
					}
				}else if(options){
					for(var el in options){
						if(options.hasOwnProperty(el)){
							var name = options[el];
							this.options.push({id: el, name: name});
						}
					}
				}
				if(setModelToFirst && this.options.length > 0){
					this.setModel(this.options[0]);
				}
				this.filter();
			}
			
			this.setFilterCallback = function(callback){
				this.filterCallback = callback;
			}
			
			
			this.filteredOptions = [];
			this.filter = function(){
				that.filteredOptions = that.options.filter(that.filterFunction);
				if(!that.noSort){
					that.filteredOptions.sort(that.sortFunction);
				}
			}
			this.filterFunction = function(option){
				var filterTextMatch = true;
				if(that.mutateCallback && (!that.mutateOnlyAggregate || option.isAggregate)){
					filterTextMatch = that.mutateCallback(option.name).search(new RegExp(that.filterText, "i")) != -1;
				}else{
					filterTextMatch = option.name.search(new RegExp(that.filterText, "i")) != -1;
				}
				if(that.filterCallback && typeof that.filterCallback == 'function' && !option.isAggregate){
					var filterCallbackResult = that.filterCallback(option);
					if(!filterCallbackResult){
						if(that.model == option){
							//TODO - perhaps find the first option that would not be filtered by filterCallback and set the model to that option
							if(that.aggregateOption){
								that.model = that.aggregateOption;
							}else{
								that.model = null;
							}
						}
					}
					return filterTextMatch && filterCallbackResult;
				}
				return filterTextMatch;
			}
			
			this.sortFunction = function(a, b){
				if(a == that.aggregateOption){
					return -1;
				}
				if(b == that.aggregateOption){
					return 1;
				}
				if(that.sortById){
					return parseInt(a.id) - parseInt(b.id);
				}
				if(that.sortByNameInt){
					return parseInt(a.name) - parseInt(b.name);
				}
				var aName = a.name;
				var bName = b.name
				if(that.mutateCallback && (!that.mutateOnlyAggregate || a == that.aggregateOption)){
					aName = that.mutateCallback(aName);
				}
				if(that.mutateCallback && (!that.mutateOnlyAggregate || b == that.aggregateOption)){
					bName = that.mutateCallback(bName);
				}
				aName = aName.trim();
				bName = bName.trim();
				if(aName < bName){
					return -1;
				}else if(aName > bName){
					return 1;
				}else{
					return 0;
				}
			}
			
			this.sort = function(option){
				if(that.noSort){
					return '';
				}
				if(that.sortById){
					return option == that.aggregateOption ? -1000 : parseInt(option.id);
				}
				if(that.sortByNameInt){
					return option == that.aggregateOption ? -1000 : parseInt(option.name);
				}
				if(that.mutateCallback && (!that.mutateOnlyAggregate || option == that.aggregateOption)){
					return option == that.aggregateOption ? '' : that.mutateCallback(option.name).trim();
				}
				return option == that.aggregateOption ? '' : option.name.trim();
			}
			
			if(params.options){
				this.fillOptions(params.options, params.setModelToFirst);
			}
			
			this.getTextValue = function(){
				if(this.mutateCallback && (!this.mutateOnlyAggregate || this.options[i] == this.aggregateOption)){
					this.model != null ? this.mutateCallback(this.model.name) : '';
				}
				return this.model != null ? this.model.name : '';
			}
			
			this.getId = function(){
				if(!this.model){
					return null;
				}
				return this.model.id;
			}
			
			this.getOptionById = function(id){
				var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
				if(!val){
					return null;
				}
				return val;
			}
			
			this.getElNameById = function(id){
				var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
				if(!val){
					return '';
				}
				if(this.mutateCallback && (!this.mutateOnlyAggregate || this.options[i] == this.aggregateOption)){
					this.mutateCallback(val.name);
				}
				return val.name;
			}
			
			this.setById = function(id){
				this.setModel(this.getOptionById(id));
			}
			
			this.getPreviousOption = function(){
				if(this.model.index == 0){
					return null;
				}
				for(var i = 0; i < this.options.length; i++){
					if(this.options[i].index == this.model.index - 1){
						return this.options[i];
					}
				}
				return null;
			}
			this.getNextOption = function(){
				if(this.model.index == this.options.length - 1){
					return null;
				}
				for(var i = 0; i < this.options.length; i++){
					if(this.options[i].index == this.model.index + 1){
						return this.options[i];
					}
				}
				return null;
			}
			this.getNextOptionByKey = function(key){
				var firstMatch = null;
				var nextMatch = null;
				for(var i = 0; i < this.options.length; i++){
					if(this.options[i].index != this.model.index){
						var name = this.options[i].name;
						if(this.mutateCallback && (!this.mutateOnlyAggregate || this.options[i] == this.aggregateOption)){
							name = this.mutateCallback(name);
						}
						if(name && name.substring(0, 1).localeCompare(key, undefined, {sensitivity: 'base'}) == 0){
							if(this.options[i].index < this.model.index && (firstMatch == null || firstMatch.index > this.options[i].index)){
								firstMatch = this.options[i];
							}
							if(this.options[i].index > this.model.index && (nextMatch == null || nextMatch.index > this.options[i].index)){
								nextMatch = this.options[i];
							}
						}
					}
				}
				if(nextMatch){
					return nextMatch;
				}else if(firstMatch){
					return firstMatch;
				}
				return null;
			}
			
			this.clone = function(){
				var clone = new SingleSelect(cloneObj(params));
				var options = [];
				var modelIndex = 0;
				for(var i = 0; i < this.options.length; i++){
					if(this.options[i] == this.model){
						modelIndex = i;
					}
					if(!this.options[i].isAggregate){
						options.push(angular.copy(this.options[i]));
					}
				}
				clone.fillOptions(options);
				clone.setModel(clone.options[modelIndex]);
				return clone;
			}
		}
		
		return {
			InstanceClass: SingleSelect
		}
	}]);
})();