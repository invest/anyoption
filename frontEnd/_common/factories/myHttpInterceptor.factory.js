﻿/* factory::myHttpInterceptor */
(function() {
	'use strict';
	
	angular.module('mainApp').factory('myHttpInterceptor', myHttpInterceptor);
		
	myHttpInterceptor.$inject = ['$q'];
	function myHttpInterceptor($q) {
		return {
			'response': function(response) {
				if (response.config.method == 'POST') {
					if (angular.isObject(response.data)) {
						return response.data;
					} else {
						return $q.resolve({
							errorCode: 10001,
							data: response
						});
					}
				} else {
					return response;
				}
			},
			'responseError': function(rejection) {
				if (rejection.config && rejection.config.method == 'POST') {
					return $q.resolve({
						errorCode: 10000,
						data: rejection
					});
				} else {
					return rejection;
				}
			}
		}
	}
})();