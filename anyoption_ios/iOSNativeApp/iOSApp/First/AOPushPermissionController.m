//
//  AOPushPermissionController.m
//  iOSApp
//
//  Created by mac_user on 1/23/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOPushPermissionController.h"

@implementation AOPushPermissionController {
    
    __weak IBOutlet UIImageView *imgViewPushDescr;
    __weak IBOutlet NSLayoutConstraint *lcPushDescrWidth;
    __weak IBOutlet NSLayoutConstraint *lcPushDescrHeight;
    __weak IBOutlet UIImageView *imgViewLogo;
    __weak IBOutlet UIView *separator_first;
    __weak IBOutlet UIView *separator_second;
    __weak IBOutlet NSLayoutConstraint *lcTitleLabelTop;
    __weak IBOutlet NSLayoutConstraint *lcDescrLabelHeight;
    __weak IBOutlet NSLayoutConstraint *lcDescrLabelWidth;
    __weak IBOutlet NSLayoutConstraint *lcDescrLabelTop;
    __weak IBOutlet NSLayoutConstraint *lcImageViewTop;
    __weak IBOutlet NSLayoutConstraint *lcButtonBottom;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    
    [self buildLogoTitleView];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    titleLabel.textColor = [theme pushTitleLabelTextColor];
    titleLabel.font = [theme pushTitleLabelFont];
    descriptionLabel.font = [theme pushTitleDescriptionLabelFont];
    
#ifdef ETRADER
    imgViewLogo.image = [UIImage imageNamed:@"etrader_logo_blue_red_arrow"];
    imgViewPushDescr.image = [UIImage imageNamed:@"allow_notifications_background"];
    [imgViewPushDescr removeConstraint:lcPushDescrWidth];
    [imgViewPushDescr removeConstraint:lcPushDescrHeight];
    
    if (IS_IPHONE && ![AOUtils isRetina4])
    {
        lcTitleLabelTop.constant = 2.0;
        lcDescrLabelTop.constant = 8.0;
        lcImageViewTop.constant = 2.0;
        lcButtonBottom.constant = 15.0;
    }
    else
    {
        lcTitleLabelTop.constant = 20.0;
        lcDescrLabelTop.constant = 30.0;
    }
    
    [descriptionLabel removeConstraints:@[lcDescrLabelWidth, lcDescrLabelHeight]];
    descriptionLabel.preferredMaxLayoutWidth = 320.0 - 2 * 20.0;
#elif defined COPYOP
    imgViewLogo.image = [UIImage imageNamed:@"logo_title_bar"];
    imgViewLogo.contentMode = UIViewContentModeScaleAspectFill;
    imgViewPushDescr.image = [UIImage imageNamed:@"push_notify"];
    [imgViewPushDescr removeConstraint:lcPushDescrWidth];
    [imgViewPushDescr removeConstraint:lcPushDescrHeight];
    lcDescrLabelTop.constant = -70.0;
    [descriptionLabel removeConstraints:@[lcDescrLabelWidth, lcDescrLabelHeight]];
    descriptionLabel.preferredMaxLayoutWidth = 320.0 - 2 * 20.0;
    lcImageViewTop.constant = 30.0;
    [self.view removeConstraint:lcButtonBottom];
    
    NSDictionary *vars = @{@"lbl" : descriptionLabel,
                           @"btn" : okButton};
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[btn]-(>=5)-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:vars]];
    NSLayoutConstraint *lcDist = [NSLayoutConstraint constraintWithItem:okButton
                                                              attribute:NSLayoutAttributeTop
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:descriptionLabel
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1.0
                                                               constant:30.0];
    lcDist.priority = 999;
    [self.view addConstraint:lcDist];
    
    if (IS_IPHONE && ![AOUtils isRetina4])
    {
        lcTitleLabelTop.constant = 2.0;
        lcImageViewTop.constant = 2.0;
    }
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#e4e1e1"];
#endif
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.navigationItem.titleView = self.logoTitleView;
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = nil;
    separator_first.backgroundColor = M_SETTINGS_COLOR;
    separator_second.backgroundColor = M_SETTINGS_COLOR;
    
    [self refreshLocalizedData];
}

-(void)refreshLocalizedData {
    titleLabel.text = AOLocalizedString(@"notifications");
    descriptionLabel.text = AOLocalizedString(@"notificationsMessage");
    [descriptionLabel sizeToFit];
    descriptionLabel.textColor = M_TEXTCOLOR;
    
    NSString *okString = AOLocalizedString(@"alert.button");
    
    [okButton setTitle:okString forState:UIControlStateNormal];
    [okButton setTitle:okString forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)okPressed:(id)sender {    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"firstLogin"]){
        [MAINVC loadPage:@"Main" pageName:@"introductionController" root:YES options:nil];
    }
    else
    {
        //[MAINVC loadPage:@"Account" pageName:@"accountController" root:YES options:@{@"noMenu":@YES}];
        [RESIDENT goTo:@"login" withOptions:@{@"noMenu":@YES}];
    }
}

@end
