//
//  AOUpdateViewController.m
//  iOSApp
//
//  Created by mac_user on 1/22/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//
#import "AOBlueButton.h"
#import "AOUpdateViewController.h"
#import "AORequestService.h"

@implementation AOUpdateViewController{
    
    __weak IBOutlet AOBlueButton *notNowButton;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    updateButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    updateButton.titleLabel.minimumScaleFactor = 0.5;
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    self.view.backgroundColor = [theme viewsBackgroundColor];
    updateText.textColor = M_TEXTCOLOR;
    
    self.navigationController.navigationBarHidden = NO;
    if (self.isCritical)
        notNowButton.hidden = YES;
}

-(void)viewDidAppear:(BOOL)animated{
    
    
    [super viewDidAppear:animated];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = nil;
    
    [self refreshLocalizedData];
}

-(void)refreshLocalizedData {
    if(_critical) updateText.text = AOLocalizedString(@"mustUpdateMsg");
    else updateText.text = AOLocalizedString(@"mustUpdateMsg");
    
    [self buildTitleViewWithText:AOLocalizedString(@"updateVersion") andImage:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)updateClicked:(id)sender{
#ifdef ANYOPTION
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.com/app/anyoption"]];
#elif ETRADER
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.com/app/etrader"]];
#else 
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.com/app/copyop"]];
#endif
    
}

- (IBAction)notNowTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
