//
//  AOCheckUpdateMethodRequest.m
//  iOSApp
//
//  Created by mac_user on 1/22/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOCheckUpdateMethodRequest.h"

@implementation AOCheckUpdateMethodRequest

-(id)init{
    if(self = [super init]){
        self.serviceMethod = @"checkForUpdate";
    }
    return self;
}

-(NSMutableArray*)keys
{
    NSMutableArray *keyArray = [[NSMutableArray alloc]
            initWithObjects:@"apkVersion",
            @"apkName",
            @"combinationId",
            @"c2dmRegistrationId",
            @"dynamicParam",
            @"mId",
            @"isFirstOpening",
            @"osTypeId",
            @"appsflyerId",
            @"deviceFamily",
            @"aff_sub1",
            @"aff_sub2",
            @"aff_sub3",
            nil];
    
    [keyArray addObjectsFromArray:[super keys]];
    
    return keyArray;
}

@end
