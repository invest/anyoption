//
//  AOCheckUpdateMethodRequest.h
//  iOSApp
//
//  Created by mac_user on 1/22/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOMethodRequest.h"

@interface AOCheckUpdateMethodRequest : AOMethodRequest

@property int apkVersion;
@property (copy, nonatomic) NSString *apkName;
@property (strong, nonatomic) NSNumber *combinationId;
@property (copy, nonatomic) NSString *c2dmRegistrationId;
@property (copy, nonatomic) NSString *dynamicParam;
@property NSInteger osTypeId;
@property (copy, nonatomic) NSString *appsflyerId;
@property (copy, nonatomic) NSString *deviceFamily;

@property (copy, nonatomic) NSString *mId;
@property NSNumber *isFirstOpening;

@property (copy, nonatomic) NSString *aff_sub1;
@property (copy, nonatomic) NSString *aff_sub2;
@property (copy, nonatomic) NSString *aff_sub3;

-(NSMutableArray*)keys;

@end
