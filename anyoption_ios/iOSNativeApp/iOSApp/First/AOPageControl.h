//
//  AOPageControl.h
//  iOSApp
//
//  Created by mac_user on 2/26/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AOPageControl : UIView {
    int lastSelectedPage;
}

@property (nonatomic) int pageCount;
@property (nonatomic) int selectedPage;

@end
