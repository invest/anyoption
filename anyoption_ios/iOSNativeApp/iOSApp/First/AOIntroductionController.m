//
//  AOIntroductionController.m
//  iOSApp
//
//  Created by mac_user on 1/23/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOIntroductionController.h"
#import "AOPageControl.h"
#import "AOPushPermissionController.h"
#import "AOGreenButton.h"

#ifdef COPYOP
#import "COModalPopupViewController.h"
#import "COLanguageSelectorViewController.h"
#import "UIFont+Roboto.h"
#import "AOFlippedButton.h"
#endif

@implementation AOIntroductionController{
    UIView* pickerWrapper;
    BOOL isFirstSlide;
}

#ifdef COPYOP
static const NSInteger kPageCount = 4;
#elif ETRADER
static const NSInteger kPageCount = 3;
#else
static const NSInteger kPageCount = 1;
#endif

-(void)dealloc{
    // NSLog(@"AOIntroductionController dealloc");
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	pickerWrapper = [[UIView alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height,languagePicker.frame.size.width, 50)];
    pickerWrapper.backgroundColor = [UIColor whiteColor];
    pickerWrapper.hidden=YES;
    UIButton* doneButton = [[UIButton alloc] initWithFrame:CGRectMake(pickerWrapper.frame.size.width-100, 0, 100, 50)];
    doneButton.backgroundColor = [UIColor clearColor];
    [doneButton setTitle:AOLocalizedString(@"done") forState:UIControlStateNormal];
    [doneButton setTitleColor:M_TEXTCOLOR forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(didTapOverlayButton:) forControlEvents:UIControlEventTouchUpInside];
    [pickerWrapper addSubview:doneButton];
    [self.view addSubview:pickerWrapper];
    [self.view bringSubviewToFront:languagePicker];
    languages = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"languages" ofType:@"plist"]];
    dataSource = [AOUtils allAvailableLanguages];
    
    isFirstSlide = YES;
    
    
#ifdef ANYOPTION
    pageControl.hidden = YES;
#endif
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [MAINVC setLockOverlay:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults boolForKey: @"keyPushNotification"]) {
        [defaults setBool:YES forKey:@"keyPushNotification"];
        
        [TrackingManager trackEvent:TrackingEventTypeInstall options:nil];
        
        [MAINVC loadPage:@"Main" pageName:@"pushPermissionController" root:YES options:nil];
        return;
    }
    
    self.navigationItem.titleView = self.logoTitleView;

    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = nil;
    
    _scrollView.frame = scrollViewContainer.bounds;
    CGFloat width = scrollViewContainer.frame.size.width;
    
    [_scrollView setContentSize:CGSizeMake(width*kPageCount, scrollViewContainer.frame.size.height)];
    
    NSArray *nibNames;
#ifdef COPYOP
    nibNames = @[@"introView", @"introView2", @"introView4", @"introView3"];
#elif ETRADER
     nibNames = @[@"introView", @"introView2", @"introView3"];
#else
    nibNames = @[@"introViewAO"];
#endif
    
    
    for(int i=0;i<kPageCount;i++) {
        CGFloat left = IS_RIGHT_TO_LEFT ? ((kPageCount - i - 1) * width) : (i * width);
        CGRect frame = CGRectMake(left,0, scrollViewContainer.frame.size.width,scrollViewContainer.frame.size.height);
        AOIntroView *introView = [[AOIntroView alloc] init];
        [introView setNib:nibNames[i] andFrame:frame];
        [_scrollView addSubview:introView];
        

        if(i==0){
#ifndef ANYOPTION
            w1LangButton = (UIButton*)[introView viewWithTag:1];
            [w1LangButton addTarget:self action:@selector(didTapLangButton) forControlEvents:UIControlEventTouchUpInside];
            w1FirstLabel = (UILabel*)[introView viewWithTag:11];
            w1SecondLabel = (UILabel*)[introView viewWithTag:12];
            w1ThirdLabel = (UILabel*)[introView viewWithTag:13];
            w1NextButton = (UIButton*)[introView viewWithTag:21];
            [w1NextButton addTarget:self action:@selector(didTapFirstNextButton) forControlEvents:UIControlEventTouchUpInside];
#else 
            w1LangButton = (UIButton*)[introView viewWithTag:1];
            [w1LangButton addTarget:self action:@selector(didTapLangButton) forControlEvents:UIControlEventTouchUpInside];
            w1FirstLabel = (UILabel*)[introView viewWithTag:11];
            w1SecondLabel = (UILabel*)[introView viewWithTag:12];
            w1NextButton = (UIButton*)[introView viewWithTag:21];
            [w1NextButton addTarget:self action:@selector(didTapOpenAccountButton) forControlEvents:UIControlEventTouchUpInside];
            
            w3LoginButton = (UIButton*)[introView viewWithTag:22];
            [w3LoginButton addTarget:self action:@selector(didTapLoginButton) forControlEvents:UIControlEventTouchUpInside];

#endif
        }
        else if(i==1){
            w2LangButton = (UIButton*)[introView viewWithTag:1];
            [w2LangButton addTarget:self action:@selector(didTapLangButton) forControlEvents:UIControlEventTouchUpInside];
            w2FirstLabel = (UILabel*)[introView viewWithTag:11];
            w2SecondLabel = (UILabel*)[introView viewWithTag:12];
            w2NextButton = (UIButton*)[introView viewWithTag:21];
            [w2NextButton addTarget:self action:@selector(didTapSecondNextButton) forControlEvents:UIControlEventTouchUpInside];
        }
#ifdef COPYOP
        else if (i == 2)
        {
            w4LangButton = (UIButton*)[introView viewWithTag:1];
            [w4LangButton addTarget:self action:@selector(didTapLangButton) forControlEvents:UIControlEventTouchUpInside];
            w4FirstLabel = (UILabel*)[introView viewWithTag:11];
            w4SecondLabel = (UILabel*)[introView viewWithTag:12];
            w4NextButton = (UIButton*)[introView viewWithTag:21];
            [w4NextButton addTarget:self action:@selector(didTapFourthNextButton) forControlEvents:UIControlEventTouchUpInside];
        }
#endif
         if(i==kPageCount - 1 ){
            w3LangButton = (UIButton*)[introView viewWithTag:1];
            [w3LangButton addTarget:self action:@selector(didTapLangButton) forControlEvents:UIControlEventTouchUpInside];
            w3FirstLabel = (UILabel*)[introView viewWithTag:11];
            w3SecondLabel = (UILabel*)[introView viewWithTag:12];
            w3OpenAccountButton = (UIButton*)[introView viewWithTag:21];
            [w3OpenAccountButton addTarget:self action:@selector(didTapOpenAccountButton) forControlEvents:UIControlEventTouchUpInside];
            w3LoginButton = (UIButton*)[introView viewWithTag:22];
            [w3LoginButton addTarget:self action:@selector(didTapLoginButton) forControlEvents:UIControlEventTouchUpInside];
            w3TradeButton = (UIButton*)[introView viewWithTag:23];
            [w3TradeButton addTarget:self action:@selector(didTapTradeButton) forControlEvents:UIControlEventTouchUpInside];
            
            id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
            w3TryWithoutAccountButton = (UIButton *)[introView viewWithTag:24];
            [w3TryWithoutAccountButton addTarget:self action:@selector(didTapTryWithoutAccountButton) forControlEvents:UIControlEventTouchUpInside];
            [w3TryWithoutAccountButton setTitleColor:[theme loginCommercialTextColor] forState:UIControlStateNormal];
            w3TryWithoutAccountButton.titleLabel.font = [theme tryTheAppFont];
            w3TryWithoutAccountButton.titleLabel.adjustsFontSizeToFitWidth = YES;
            [w3TryWithoutAccountButton setTitle:AOLocalizedString(@"tryWithoutAccount") forState:UIControlStateNormal];
#ifdef COPYOP
            w3TryWithoutAccountButton.hidden = YES;
#endif
        }
    }

    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;

    UIColor *textColor = [theme introsTextColor];
    w1FirstLabel.textColor = w1SecondLabel.textColor = w1ThirdLabel.textColor =
    w2FirstLabel.textColor = w2SecondLabel.textColor =
    w3FirstLabel.textColor = w3SecondLabel.textColor = textColor;
    
    w4FirstLabel.textColor = w4SecondLabel.textColor = textColor;
    
    [w1LangButton setTitleColor:textColor forState:UIControlStateNormal];
    [w2LangButton setTitleColor:textColor forState:UIControlStateNormal];
    [w3LangButton setTitleColor:textColor forState:UIControlStateNormal];
    [w4LangButton setTitleColor:textColor forState:UIControlStateNormal];
    
    w1LangButton.alpha = w2LangButton.alpha = w3LangButton.alpha = w1FirstLabel.alpha = w1SecondLabel.alpha = w1ThirdLabel.alpha = w2FirstLabel.alpha = w2SecondLabel.alpha = w3FirstLabel.alpha = w3SecondLabel.alpha = 0.77;
    w4LangButton.alpha = w4FirstLabel.alpha = w4SecondLabel.alpha = 0.77;
    
    NSArray *firstLabels = @[w1FirstLabel
#ifndef ANYOPTION
                             , w2FirstLabel, w3FirstLabel
#endif
                             ];
    
    if (w4FirstLabel)
    {
        firstLabels = [firstLabels arrayByAddingObject:w4FirstLabel];
    }
    NSArray *secondLabels = @[w1SecondLabel
#ifndef ANYOPTION
                              , w2SecondLabel, w3SecondLabel
#endif
];
    if (w4SecondLabel)
    {
        secondLabels = [secondLabels arrayByAddingObject:w4SecondLabel];
    }
    for (UILabel *level1Label in firstLabels)
    {
        level1Label.font = [theme introLabel1Font];
    }
    for (UILabel *level2Label in secondLabels)
    {
#ifndef ANYOPTION
        level2Label.font = [theme introLabel2Font];
#else
        level2Label.font = [theme introLabel3Font];
#endif
    }
    if(w1ThirdLabel){
        for (UILabel *level3Label in @[w1ThirdLabel])
        {
            level3Label.font = [theme introLabel3Font];
        }
    }
    
    self.view.backgroundColor = [theme introsBackgroundColor];
    languagePicker.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.9];
    
    [pageControl setPageCount:kPageCount];
    
    [pageControl setSelectedPage:(IS_RIGHT_TO_LEFT ? kPageCount - 1 : 0)];
    
    if (IS_RIGHT_TO_LEFT)
    {
        [_scrollView scrollRectToVisible:CGRectMake((kPageCount - 1) * width, 0, width, scrollViewContainer.frame.size.height) animated:NO];
    }
    
#ifdef ETRADER
    [self buildTitleViewWithText:@"" andImage:[UIImage imageNamed:@"etrader_logo_blue_red_arrow"]];
#else
    [self buildLogoTitleView];
#endif
    pickerBottomConstraint.constant = -languagePicker.frame.size.height;
    
    [self refreshLocalizedData];
    
    if (IS_LOGGED) {
        [self setupIfLogged];
    }
    
#ifdef COPYOP
    [self buildLogo];
    
    if (!IS_LOGGED)
    {
        AOFlippedButton *btnLang = [AOFlippedButton buttonWithType:UIButtonTypeCustom];
        btnLang.forceFlip = YES;
        NSString *title;
        if ([defaults objectForKey:@"currentLang"])
        {
            title = AOLocalizedString([languages objectForKey:[defaults objectForKey:@"currentLang"]]);
        }
        else
        {
            title = @"English";
        }
        if ([title length] > 3)
        {
            title = [title substringToIndex:3];
        }
        title = [title uppercaseStringWithLocale:[NSLocale currentLocale]];
        UIImage *image = [self p_arrowDownImage];
        [btnLang setTitle:title forState:UIControlStateNormal];
        [btnLang setImage:image forState:UIControlStateNormal];
        btnLang.titleLabel.font = [UIFont robotoFontRegularWithSize:13.0];
        [btnLang setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        UIEdgeInsets insets = UIEdgeInsetsMake(10.0, 0.0, 0.0, 0.0);
        [btnLang setTitleEdgeInsets:insets];
        [btnLang setImageEdgeInsets:insets];
        [btnLang sizeToFit];
        CGRect fr = btnLang.frame;
        fr.size.height = CGRectGetHeight(self.navigationController.navigationBar.bounds);
        btnLang.frame = fr;
        [btnLang addTarget:self action:@selector(btnLangPressed:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithCustomView:btnLang];
        [btn setTitlePositionAdjustment:UIOffsetMake(0.0, 20.0) forBarMetrics:UIBarMetricsDefault];
        self.navigationItem.leftBarButtonItem = btn;
    }
    else
    {
        self.navigationItem.leftBarButtonItem = nil;
    }
#endif
    if (self.navOptions[@"selPage"])
    {
        [self scrollToPage:(int)[self.navOptions[@"selPage"] integerValue] animated:NO];
    }
}

- (UIImage *) p_arrowDownImage
{
    CGSize size = CGSizeMake(16.0, 10.0);
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);

    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(ctx, 1.0);
    [[UIColor blackColor] setStroke];
    CGContextMoveToPoint(ctx, 2.0, 2.0);
    CGContextAddLineToPoint(ctx, size.width / 2.0, size.height - 2.0);
    CGContextAddLineToPoint(ctx, size.width - 2.0, 2.0);
    CGContextDrawPath(ctx, kCGPathStroke);
    
    UIImage *readyImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return readyImage;
}

#ifdef COPYOP
- (void) btnLangPressed:(id)sender
{
    COLanguageSelectorViewController *langVc = [[UIStoryboard storyboardWithName:@"CopyOP" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"LangSelector"];
    
    NSArray *_langDataSource = [AOUtils allAvailableLanguages];
    langVc.datasource = _langDataSource;
    COModalPopupViewController *popupVc = [[COModalPopupViewController alloc] initWithContentViewController:langVc];
    
    popupVc.didTapOutsideHandler =  popupVc.closeButtonPressedHandler = langVc.selectionHandler = ^
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        [MAINVC loadPage:@"Main" pageName:@"introductionController" root:YES options:@{@"selPage": @([self currentPage])}];
    };
    [popupVc presentInViewController:self animated:YES completion:nil];
}
#endif

-(void)refreshLocalizedData{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if([userDefaults objectForKey:@"currentLang"]) {
        [w1LangButton setTitle:[self getLanguageNameWithId:[userDefaults objectForKey:@"currentLang"]] forState:UIControlStateNormal];
        [w2LangButton setTitle:[self getLanguageNameWithId:[userDefaults objectForKey:@"currentLang"]] forState:UIControlStateNormal];
        [w3LangButton setTitle:[self getLanguageNameWithId:[userDefaults objectForKey:@"currentLang"]] forState:UIControlStateNormal];
    } else {
        [w1LangButton setTitle:@"English" forState:UIControlStateNormal];
        [w2LangButton setTitle:@"English" forState:UIControlStateNormal];
        [w3LangButton setTitle:@"English" forState:UIControlStateNormal];
    }
    
#ifdef ETRADER
    w1FirstLabel.text = AOLocalizedString(@"wellcomeMail.subject");
    w1SecondLabel.text = AOLocalizedString(@"safeSecureReliable");
#elif ANYOPTION
    w1FirstLabel.text = AOLocalizedString(@"wellcomeMail.subject");
    NSString* rowFirst = AOLocalizedString(@"easyExcitingTrading");
    
    NSString *rowFirstFirstLetter = [[rowFirst substringToIndex:1] uppercaseString];

    rowFirst = [rowFirstFirstLetter stringByAppendingString:[rowFirst substringFromIndex:1]];
    
    w1SecondLabel.text = [NSString stringWithFormat:@"%@\n%@\n%@",rowFirst,AOLocalizedString(@"safeSecureReliable"),AOLocalizedString(@"home.page.slide.2.3")] ;
#else
    w1FirstLabel.text = AOLocalizedString(@"welcomeScreen1Header");
    w1SecondLabel.text = AOLocalizedString(@"welcomeScreen1Paragraph");
#endif
    
#if defined(ETRADER) || defined(COPYOP)
    w1ThirdLabel.text = @"";
#else
    w1ThirdLabel.text = AOLocalizedString(@"home.page.slide.2.3");
#endif
    
#ifdef COPYOP
    w2FirstLabel.text = AOLocalizedString(@"welcomeScreen2Header");
    w2SecondLabel.text = AOLocalizedString(@"welcomeScreen2Paragraph");
    
    w4FirstLabel.text = AOLocalizedString(@"welcomeScreen3Header");
    w4SecondLabel.text = AOLocalizedString(@"welcomeScreen3Paragraph");
    
    w3FirstLabel.text = AOLocalizedString(@"welcomeScreen4Header");
    w3SecondLabel.text = AOLocalizedString(@"welcomeScreen4Paragraph");
#elif ETRADER
    w2FirstLabel.text = AOLocalizedString(@"oneClickPurchase");
    w2SecondLabel.text = AOLocalizedString(@"easyExcitingTrading");
    
    w3FirstLabel.text = AOLocalizedString(@"fastPayouts");
    w3SecondLabel.text = AOLocalizedString(@"only24HoursWithdrawal");
#endif
    
    NSString *nextTitle = AOLocalizedString(@"mobile.button.next");
#ifdef COPYOP
    nextTitle = [nextTitle uppercaseStringWithLocale:[NSLocale currentLocale]];
#endif
    [w1NextButton setTitle:nextTitle forState:UIControlStateNormal];
    [w2NextButton setTitle:nextTitle forState:UIControlStateNormal];
    [w4NextButton setTitle:nextTitle forState:UIControlStateNormal];
#ifdef COPYOP
    [w3OpenAccountButton setTitle:[AOLocalizedString(@"startNow") uppercaseStringWithLocale:[NSLocale currentLocale]] forState:UIControlStateNormal];
    [w3LoginButton setTitle:[AOLocalizedString(@"header.login.up") uppercaseStringWithLocale:[NSLocale currentLocale]] forState:UIControlStateNormal];
#else
    [w3OpenAccountButton setTitle:AOLocalizedString(@"menuStrip.openaccount") forState:UIControlStateNormal];
    [w3LoginButton setTitle:AOLocalizedString(@"header.login") forState:UIControlStateNormal];
    [w3TryWithoutAccountButton setTitle:AOLocalizedString(@"tryWithoutAccount") forState:UIControlStateNormal];
#endif
    [w3TradeButton setTitle:AOLocalizedString(@"live.login.trade") forState:UIControlStateNormal];
    [languagePicker reloadAllComponents];
    
    
#ifdef ANYOPTION
    [w1NextButton setTitle:AOLocalizedString(@"menuStrip.openaccount") forState:UIControlStateNormal];
#endif
}

- (void)setupIfLogged {
    w1LangButton.hidden = YES;
    w2LangButton.hidden = YES;
    w3LangButton.hidden = YES;
    w3LoginButton.hidden = YES;
    w3OpenAccountButton.hidden = YES;
    w3TryWithoutAccountButton.hidden = YES;
    
    w3TradeButton.hidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didTapLangButton{
    if(isAnimating) return;
    isAnimating = YES;
    [UIView animateWithDuration:0.5 animations:^{
       
        languagePicker.hidden = NO;
        pickerBottomConstraint.constant = 0;
        [languagePicker layoutIfNeeded];
    } completion:^(BOOL finished) {
        pickerWrapper.frame = CGRectMake(0.0, languagePicker.frame.origin.y-50, languagePicker.frame.size.width, 50);
         pickerWrapper.hidden=NO;
        overlayButton.hidden = NO;
        isAnimating = NO;
    }];
}
-(IBAction)didTapOverlayButton:(id)sender{
    if(isAnimating) return;
    isAnimating = YES;
    pickerWrapper.hidden=YES;
    [UIView animateWithDuration:0.3 animations:^{
        pickerBottomConstraint.constant = -languagePicker.frame.size.height;
        [languagePicker layoutIfNeeded];
    } completion:^(BOOL finished) {
        overlayButton.hidden = YES;
        languagePicker.hidden = NO;
        isAnimating = NO;
    }];
}

-(void)didTapFirstNextButton{
    [self scrollToPage:1];
    
    [self changeNotificationPermission];
}
-(void)didTapSecondNextButton{
    [self scrollToPage:2];
}

- (void) didTapFourthNextButton{
    [self scrollToPage:3];
}

- (void)didTapTradeButton {
    [MAINVC setLockOverlay:NO];
    //[RESIDENT goTo:@"home" withOptions:nil];
    [MAINVC loadPage:@"Trade" pageName:@"Trade" root:YES options:nil];
}

- (void) didTapTryWithoutAccountButton
{
    [DLG tryWithoutAccount];
}

-(void)didTapOpenAccountButton{
    //[MAINVC loadPage:@"Account" pageName:@"accountController" root:YES options:@{@"noMenu":@YES, @"account":@YES}];
    [RESIDENT goTo:@"login" withOptions:@{@"account":@YES}];
}
-(void)didTapLoginButton{
  [RESIDENT goTo:@"login" withOptions:nil];
}

- (void) scrollToPage:(int)page animated:(BOOL)animated
{
    if (IS_RIGHT_TO_LEFT)
    {
        page = kPageCount - 1 - page;
    }
    CGRect frame = _scrollView.frame;
    frame.origin.x = page * frame.size.width;
    frame.origin.y = 0;
    [pageControl setSelectedPage:page];
    if (animated)
    {
        [UIView animateWithDuration:0.5 animations:^{
            [_scrollView scrollRectToVisible:frame animated:NO];
        } completion:^(BOOL finished) {
            
        }];
    }
    else
    {
        [_scrollView scrollRectToVisible:frame animated:NO];
    }
}

-(void)scrollToPage:(int)page{
    [self scrollToPage:page animated:YES];
}

- (NSInteger) currentPage
{
    NSInteger page = (NSInteger)(_scrollView.contentOffset.x / _scrollView.frame.size.width);
#ifndef ETRADER
    return page;
#else
    return kPageCount - 1 - page;
#endif
}


#pragma mark scrollview delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int page = scrollView.contentOffset.x/scrollView.frame.size.width;
    [pageControl setSelectedPage:page];
    
    if (isFirstSlide) {
        [self changeNotificationPermission];
    }
    
    isFirstSlide = NO;
}

#pragma mark UIPickerView delegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
	return [self getLanguageNameWithId:[dataSource objectAtIndex:row]];
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
	return dataSource.count;
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
	return 1;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSString *selectedLang = [dataSource objectAtIndex:row];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:selectedLang forKey:@"currentLang"];
    [defaults synchronize];
    [DLG user].skinId = [AOUtils skinIdFromCurrentLang:selectedLang];
    /**/
    long sId = [DLG user].skinId;
    [defaults setInteger:sId forKey:@"currentSkinIdForLockedPopUp"];
    [defaults synchronize];
    /**/
    [self refreshLocalizedData];
    [w1LangButton setTitle:[self getLanguageNameWithId:[dataSource objectAtIndex:row]] forState:UIControlStateNormal];
    [w2LangButton setTitle:[self getLanguageNameWithId:[dataSource objectAtIndex:row]] forState:UIControlStateNormal];
    [w3LangButton setTitle:[self getLanguageNameWithId:[dataSource objectAtIndex:row]] forState:UIControlStateNormal];
}
-(NSString*)getLanguageNameWithId:(NSString*)idString{
    return AOLocalizedString([languages objectForKey:idString]);
}

-(void)changeNotificationPermission {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (!([NSNumber numberWithBool:[[defaults objectForKey:@"firstRun"] boolValue]])) {
        return;
    }
    
    UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    
    BOOL allowed;
    
    if (!(types & UIRemoteNotificationTypeAlert)) {
        allowed = NO;
    } else {
        allowed = YES;
    }
    
    [TrackingManager trackEvent:TrackingEventTypeNotificationPermissionChanged options:@{@"allowed": @(allowed)}];
}

@end
