//
//  AOIntroViewPage4.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 10/20/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOIntroViewPage4.h"

@implementation AOIntroViewPage4

- (void) awakeFromNib
{
    [super awakeFromNib];
    
#ifdef COPYOP
    [self viewWithTag:111].hidden = YES;
#endif
}

- (UIImage *) backgroundImage
{
    return [UIImage imageNamed:@"start_sreen_image_3"];
}

- (CGFloat) backgroundImageTopOffset
{
    return 15.0;
}

@end
