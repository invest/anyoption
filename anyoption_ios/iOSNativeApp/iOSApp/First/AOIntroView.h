//
//  AOIntroView.h
//  iOSApp
//
//  Created by mac_user on 2/4/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

@interface AOIntroView : UIView {
}

@property (nonatomic,weak) IBOutlet UIView *view;

-(void)setNib:(NSString*)nibName andFrame:(CGRect)frame;

@end
