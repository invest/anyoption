//
//  AOIntroductionController.h
//  iOSApp
//
//  Created by mac_user on 1/23/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOBaseViewController.h"
#import "AOIntroView.h"

@class AOPageControl;

@interface AOIntroductionController : AOBaseViewController <UIScrollViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate> {
    __weak IBOutlet UIView *scrollViewContainer;
    __weak IBOutlet UIScrollView *_scrollView;
    __weak IBOutlet AOPageControl *pageControl;
    
    __weak UIButton *w1LangButton;
    __weak UILabel *w1FirstLabel;
    __weak UILabel *w1SecondLabel;
    __weak UILabel *w1ThirdLabel;
    __weak UIButton *w1NextButton;
    
    __weak UIButton *w2LangButton;
    __weak UILabel *w2FirstLabel;
    __weak UILabel *w2SecondLabel;
    __weak UIButton *w2NextButton;
    
    __weak UIButton *w3LangButton;
    __weak UILabel *w3FirstLabel;
    __weak UILabel *w3SecondLabel;
    __weak UIButton *w3OpenAccountButton;
    __weak UIButton *w3LoginButton;
    __weak UIButton *w3TradeButton;
    __weak UIButton *w3TryWithoutAccountButton;
    
    __weak UIButton *w4LangButton;
    __weak UILabel *w4FirstLabel;
    __weak UILabel *w4SecondLabel;
    __weak UIButton *w4NextButton;
    
    __weak IBOutlet UIPickerView *languagePicker;
    __weak IBOutlet NSLayoutConstraint *pickerBottomConstraint;
    __weak IBOutlet UIButton *overlayButton;
    
    NSArray *dataSource;
    NSDictionary *languages;
    
    BOOL isAnimating;
}

@end
