//
//  AOPushPermissionController.h
//  iOSApp
//
//  Created by mac_user on 1/23/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOBaseViewController.h"

@interface AOPushPermissionController : AOBaseViewController {
    IBOutlet UILabel *titleLabel;
    IBOutlet UILabel *descriptionLabel;
    IBOutlet UIButton *okButton;
}

-(void)refreshLocalizedData;

@end
