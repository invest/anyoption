//
//  AOIntroView.m
//  iOSApp
//
//  Created by mac_user on 2/4/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOIntroView.h"

@implementation AOIntroView

-(void)dealloc{
    // NSLog(@"AOIntroView dealloc");
}

-(void)setNib:(NSString*)nibName andFrame:(CGRect)frame{
    [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    self.frame = frame;
    _view.frame = self.bounds;
    [self addSubview:_view];
}

/*
-(void)setText:(NSString*)text{
    UIView *labelContainerView = [self viewWithTag:1];
    UILabel *label = (UILabel*)[self viewWithTag:2];
    
    label.text = text;
    
    [label layoutIfNeeded];
    
    CGFloat labelHeight = label.frame.size.height+80;
    [AOUtils replaceConstraintWithAttribute:NSLayoutAttributeHeight withConstant:labelHeight onView:labelContainerView];
}
*/

@end
