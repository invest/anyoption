//
//  AOPageControl.m
//  iOSApp
//
//  Created by mac_user on 2/26/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOPageControl.h"

@implementation AOPageControl

-(void)awakeFromNib{
    _selectedPage = -1;
    lastSelectedPage = -1;
}

-(void)setSelectedPage:(int)selectedPage{
    UIImageView *lastSelectedImageView = nil;
    if(lastSelectedPage!=-1)lastSelectedImageView = (UIImageView*)[self.subviews objectAtIndex:lastSelectedPage];
    if(lastSelectedImageView) lastSelectedImageView.highlighted = NO;
    
    UIImageView *selectedImageView = (UIImageView*)[self.subviews objectAtIndex:selectedPage];
    selectedImageView.highlighted = YES;
    lastSelectedPage = selectedPage;
}

-(void)setPageCount:(int)pageCount {
    for(UIImageView *imageView in self.subviews){
        [imageView removeFromSuperview];
    }
    
    CGFloat space = 4;
    CGFloat xoffset = 0;
    CGFloat imageWidth = 0;
    CGFloat totalWidth = -1;
    for(int i=0;i<pageCount;i++){
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.highlightedImage = [UIImage imageNamed:@"pagecontrol-active.png"];
        imageView.image = [UIImage imageNamed:@"pagecontrol-inactive.png"];
        if(i==_selectedPage) imageView.highlighted = YES;
        
        if(totalWidth==-1) {
            imageWidth = imageView.image.size.width;
            totalWidth = imageWidth*pageCount+space*(pageCount-1);
            xoffset = self.frame.size.width/2-totalWidth/2;
        }

        imageView.frame = CGRectMake(xoffset,0, imageWidth,imageView.image.size.height);
        xoffset += imageWidth+space;
        [self addSubview:imageView];
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
