//
//  AOUpdateViewController.h
//  iOSApp
//
//  Created by mac_user on 1/22/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOBaseViewController.h"

@class AppDelegate;

@interface AOUpdateViewController : AOBaseViewController {
    IBOutlet UILabel *updateText;
    IBOutlet UIButton *updateButton;
}

@property (nonatomic,assign, getter = isCritical) BOOL critical;

-(void)refreshLocalizedData;

@end
