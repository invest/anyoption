//
//  ViewController.h
//  iOSApp
//
//  Created by  on 20/05/14.
//  Copyright (c) 2013 Anyoption. All rights reserved.
//

#import "AONavigationController.h"
#import "MainMenuViewController.h"
#import "AORootMainViewController.h"
#import "AOStatusBar.h"

@class AppDelegate,AOAccountController,AOUpdateViewController;

@interface AOMainViewControllerIPad : AORootMainViewController <UIGestureRecognizerDelegate> {
    AOUpdateViewController *updateViewController;
}
@property (strong, nonatomic) AOMainPopupViewController* popupVC;
- (void) setupSplitView;


@end