//
//  AORegQuestionnaireViewController.h
//  iOSApp
//
//  Created by  on 8/14/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AOBaseViewController.h"
#import "AORegQuestionnaire.h"



enum {
    AOQuestionnaireMandatoryController = 1,
    AOQuestionnaireOptionalController = 2
};

@interface AORegQuestionnaireViewController : AOBaseViewController <UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegateFlowLayout>
{
    
    AORegQuestionnaire* questionnaire;
    
    //NSMutableArray *dataSource;
  
    
    NSInteger selectedQuestionIndex;
    NSInteger selectedAnswerIndex;
    
    //__weak IBOutlet UICollectionView *_collectionView;
    
    
    __weak IBOutlet UITableView *questionsTableView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
}

@end
