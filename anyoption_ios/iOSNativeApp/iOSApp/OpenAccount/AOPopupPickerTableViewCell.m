//
//  AOPopupPickerTableViewCell.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/19/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AOPopupPickerTableViewCell.h"

@interface AOPopupPickerTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcBottomPadding;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcTopPadding;

@end

@implementation AOPopupPickerTableViewCell

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    self.lblTitle.textColor = [theme textColor];
    self.lblTitle.font = [theme sectionTextFont];
    
    _topBottomPadding = self.lcTopPadding.constant;
    
    [self p_updateTitle];
    [self p_updatePadding];
    
    self.lblTitle.highlightedTextColor = [theme popupPickerCellTitleHighlightedColor];
    
    UIColor *selColor = [theme popupPickerCellSelectionBackgroundColor];
    if (selColor != nil)
    {
        self.selectedBackgroundView = ({
            UIView *view = [[UIView alloc] init];
            view.backgroundColor = selColor;
            view;
        });
    }
}

GENERATE_SETTER(setTitle, NSString *, title, p_updateTitle);
GENERATE_SETTER(setTopBottomPadding, CGFloat, topBottomPadding, p_updatePadding);

- (void) p_updateTitle
{
    self.lblTitle.text = self.title;
}

- (void) p_updatePadding
{
    self.lcTopPadding.constant = self.lcBottomPadding.constant = self.topBottomPadding;
}

+ (CGFloat) defaultTopBottomPadding
{
    return 15.0;
}

@end
