//
//  AORegQuestionnaireTableViewCell.h
//  iOSApp
//
//  Created by  on 8/14/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

static NSString* const AORegCellId = @"AORegQuestionCell";

#import <UIKit/UIKit.h>

@interface AORegQuestionnaireTableViewCell : UITableViewCell

@property (nonatomic, copy) NSString *questionText;
@property (nonatomic, copy) NSString *answerText;
@property (nonatomic, copy) NSString *errorText;
@property (nonatomic, assign) BOOL showsTopSeparator;
@property (nonatomic, assign) BOOL showsBottomSeparator;
@property (nonatomic, assign) BOOL showsTextInput;
@property (nonatomic, copy) NSString *textInputPlaceholder;
- (void) showKeyboard;
@property (nonatomic, copy) void (^textInputChangedChander)(AORegQuestionnaireTableViewCell *cell, NSString *text);
@property (nonatomic, copy) void (^textInputDidEndEditing)(AORegQuestionnaireTableViewCell *cell);
@end
