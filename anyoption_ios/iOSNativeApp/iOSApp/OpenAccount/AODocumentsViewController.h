//
//  AODocumentsViewController.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/31/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AOBaseViewController.h"

@interface AODocumentsViewController : AOBaseViewController

@end
