//
//  AORegQuestionnaireViewController.m
//  iOSApp
//
//  Created by  on 8/14/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//




#import "AORegQuestionnaireViewController.h"
#import "AORequestService.h"
#import "AOUserMethodRequest.h"
#import "AOUpdateUserQuestionnaireAnswersRequest.h"
#import "UIView+Disabler.h"
#import "AOUpdateQuestionnaireDoneRequest.h"
#import "AOQuestionnaireAnswerCheckBox.h"
#import "UIAlertView+Blocks.h"
#import "AOGetQuestionnaireMethodRequest.h"
#import "NSString+Utils.h"
#import "AOQuestionnaireFooterView.h"
#import "AORegQuestionnaireTableViewCell.h"
#import "AORegQuestionnaireSectionHeaderView.h"
#import "AOBlueButton.h"
#import "AoRegQuestionnaireCheckboxTableViewCell.h"
#import "AOPopupPickerViewController.h"
#import "AOConstants.h"

@interface AORegQuestionnaireViewController ()

@property (weak, nonatomic) IBOutlet AOBlueButton *btnNext;

@end

@implementation AORegQuestionnaireViewController{
    int questionnaireStep;
    BOOL _nextButtonPressed;
}

static NSString * const kSectionHeader = @"SectionHeader";
static NSString * const kCheckboxCellIdentifier = @"AORegCheckboxCell";
static NSInteger kFirstPageNumQuestions = 5;

static const NSInteger kQuestionAgree1Id = 90;
static const NSInteger kQuestionAgree2Id = 91;
static const NSInteger kQuestionAgree2bId = 93;

static const NSInteger kYesIndex = 0;
static const NSInteger kNoIndex = 1;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#ifdef COPYOP
    self.btnNext.options = @{@"height" : @43.0};
#endif
    
    questionsTableView.backgroundColor = [UIColor clearColor];
    questionsTableView.separatorColor = [UIColor colorWithRed:208.0/255.0 green:224.0/255.0 blue:231.0/255.0 alpha:1.0];
    
    questionnaireStep = 1;
     if ([self.navOptions objectForKey:@"questionnaireStep"]){
         questionnaireStep = [[self.navOptions objectForKey:@"questionnaireStep"] intValue];
     }
    
    if (questionnaireStep == 1)
    {
        [self p_buildTableHeaderView];
        
        NSString *nextTitle;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"currentLang"] isEqualToString:@"it"])
        {
            nextTitle = AOLocalizedString(@"next");
        }
        else
        {
            nextTitle = AOLocalizedString(@"mobile.button.next");
        }
#ifdef COPYOP
        nextTitle = [nextTitle uppercaseStringWithLocale:[NSLocale currentLocale]];
#endif
        [self.btnNext setTitle:nextTitle forState:UIControlStateNormal];
    }
    else
    {
        NSString *tradeNowTitle = AOLocalizedString(@"mobile.header.trade");
#ifdef COPYOP
        tradeNowTitle = [tradeNowTitle uppercaseStringWithLocale:[NSLocale currentLocale]];
#endif
        [self.btnNext setTitle:tradeNowTitle forState:UIControlStateNormal];
    }
    
    questionsTableView.tableFooterView.backgroundColor = [UIColor clearColor];
    
    [questionsTableView registerNib:[UINib nibWithNibName:@"AORegQuestionnaireSectionHeaderView" bundle:[NSBundle mainBundle]]
 forHeaderFooterViewReuseIdentifier:kSectionHeader];
  
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification  object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    self.view.backgroundColor = [theme viewsBackgroundColor];
    
    selectedQuestionIndex = -1;
}

- (void) keyboardWillShow:(NSNotification *)notification
{
    CGFloat keyboardFrameY = CGRectGetMinY([[notification userInfo][UIKeyboardFrameEndUserInfoKey] CGRectValue]);
    questionsTableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, keyboardFrameY, 0.0);
}

- (void) keyboardWillBeHidden:(NSNotification *)notification
{
    questionsTableView.contentInset = UIEdgeInsetsZero;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self buildTitleViewWithText:AOLocalizedString(@"CMS.mypage_side_menu.label.questionnaire") andImage:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
   // if ([DLG userRegulation].approvedRegulationStep < 3)
 
    
    [self getQuestionnaire];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) p_buildTableHeaderView
{
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [theme regTableHeaderBackgroundColor];
    headerView.translatesAutoresizingMaskIntoConstraints = NO;
    
    UILabel *lblTop = [[UILabel alloc] init];
    lblTop.numberOfLines = 0;
    lblTop.translatesAutoresizingMaskIntoConstraints = NO;
    lblTop.text = AOLocalizedString(@"regQuestionnaireBeforeYouCanTrade");
    lblTop.font = [theme sectionTextFont];
    lblTop.textColor = [theme textColor];
    
    UILabel *lblBottom = [[UILabel alloc] init];
    lblBottom.translatesAutoresizingMaskIntoConstraints = NO;
    lblBottom.numberOfLines = 0;
    
    NSString *bottomText = AOLocalizedString(@"regQuestionnaireNote");
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:bottomText
                                                                            attributes:@{NSForegroundColorAttributeName : [theme textColor],
                                                                                         NSFontAttributeName : [theme sectionTextFont]}];
    
    NSRange colonRange = [bottomText rangeOfString:@":"];
    if (colonRange.location != NSNotFound)
    {
        NSRange noteRange = NSMakeRange(0, colonRange.location + 1);
        [str setAttributes:@{NSFontAttributeName : [UIFont fontWithName:APP_FONT_BOLD size: 12.0],
                             NSForegroundColorAttributeName : [theme textColor]}
                     range:noteRange];
    }
    lblBottom.attributedText = str;
    
    [headerView addSubview:lblTop];
    [headerView addSubview:lblBottom];
    
    NSDictionary *vars = NSDictionaryOfVariableBindings(headerView, lblTop, lblBottom);
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20@999-[lblTop]-20@999-|"
                                                                        options:0
                                                                        metrics:nil
                                                                          views:vars]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20@999-[lblBottom]-20@999-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:vars]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20@999-[lblTop]-8@999-[lblBottom]-20@999-|"
                                                                       options:NSLayoutFormatAlignAllCenterX
                                                                       metrics:nil
                                                                         views:vars]];
    
    [headerView addConstraint:[NSLayoutConstraint constraintWithItem:headerView
                                                           attribute:NSLayoutAttributeWidth
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:0.0
                                                            constant:CGRectGetWidth(questionsTableView.bounds)]];
    [headerView setNeedsLayout];
    [headerView layoutIfNeeded];

    CGFloat height = [headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect bounds = headerView.bounds;
    bounds.size.height = height;
    headerView.bounds = bounds;
    
    questionsTableView.tableHeaderView = headerView;
}

- (IBAction) btnNextPressed:(id)sender
{
    _nextButtonPressed = YES;
    
    [self.view endEditing:YES];
    
    if ([self validate])
    {
        NSInteger count = [self tableView:questionsTableView numberOfRowsInSection:0];
        for (NSInteger i = 0; i < count; i++)
        {
            NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:0];
            if ([self isProfessionOtherCellAtIndexPath:path])
            {
                [self sendUserAnswersWithBlock:nil];
            }
        }
        
        if (questionnaireStep == 1)
        {
            [RESIDENT goTo:@"questionnaire" withOptions:@{@"questionnaireStep" : @2, @"isRoot" : @NO}];
        }
        else
        {
            [self submitQuestionnaire:nil];
        }
    }
}

- (BOOL) validate
{
    NSMutableArray *errorRows = [NSMutableArray array];
    NSInteger count = [self tableView:questionsTableView numberOfRowsInSection:0];
    for (NSInteger i = 0; i < count; i++)
    {
        NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:0];
        NSDictionary *question = questionnaire.questions[[self questionIndexFromIndexPath:path]];
        
        if (![question[@"mandatory"] boolValue])
        {
            continue;
        }
        
        NSDictionary *userAnswer = [questionnaire userAnswerForQuestionId:[question[@"id"] integerValue]];
        
        if (userAnswer == nil || userAnswer[@"answerId"] == [NSNull null])
        {
            [errorRows addObject:path];
        }
        else if ([self isCheckboxCellAtIndexPath:path])
        {
            NSArray *allAnswers = question[@"answers"];
            NSUInteger index = [allAnswers indexOfObjectPassingTest:^BOOL(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
                return [obj[@"id"] intValue] == [userAnswer[@"answerId"] intValue];
            }];
            NSAssert(index != NSNotFound, @"this should not be possible!");
            
            NSDictionary *userAns = allAnswers[index];
            if (![userAns[@"name"] hasSuffix:@".yes"] && ![userAns[@"questionId"] isEqual:@93]) //93 is specail Are you a polititian question
            {
                [errorRows addObject:path];
            }
        }
        else if ([self isProfessionOtherCellAtIndexPath:path])
        {
            NSString *text = userAnswer[@"textAnswer"] == [NSNull null] ? @"" : userAnswer[@"textAnswer"];
            if ([[text trimWhitespaces] length] == 0)
            {
                [errorRows addObject:path];
            }
        }
    }
    
    if ([errorRows count] > 0)
    {
        [questionsTableView reloadRowsAtIndexPaths:errorRows withRowAnimation:UITableViewRowAnimationAutomatic];
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)startLoading{
    activityIndicator.hidden = NO;
    [activityIndicator startAnimating];
    [self.view addDisablerWithColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.1] andTag:2914963];//Number of Saruman's soldiers
}
-(void)stopLoading{
    [activityIndicator stopAnimating];
    activityIndicator.hidden = YES;
    [self.view removeDisablerWithTag:2914963];
}


- (void) configureCell:(AORegQuestionnaireTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSInteger questionIndex = [self questionIndexFromIndexPath:indexPath];
    NSDictionary *question = questionnaire.questions[questionIndex];
    cell.questionText = AOLocalizedString(question[@"name"]);
    
    NSDictionary *userAnswer = [questionnaire userAnswerForQuestionId:[question[@"id"] integerValue]];
    if (userAnswer == nil || userAnswer[@"answerId"] == [NSNull null])
    {
        cell.answerText = @"";
        cell.errorText = ((_nextButtonPressed && [question[@"mandatory"] boolValue]) ? AOLocalizedString(@"error.mandatory") : @"");
    }
    else
    {
        NSArray *allAnswers = question[@"answers"];
        NSUInteger index = [allAnswers indexOfObjectPassingTest:^BOOL(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            return [obj[@"id"] intValue] == [userAnswer[@"answerId"] intValue];
        }];
        NSAssert(index != NSNotFound, @"this should not be possible!");
        
        NSDictionary *userAns = allAnswers[index];
        if ([self isProfessionOtherCellAtIndexPath:indexPath])
        {
            cell.answerText = userAnswer[@"textAnswer"] == [NSNull null] ? @"" : userAnswer[@"textAnswer"];
        }
        else
        {
            cell.answerText = AOLocalizedString(userAns[@"name"]);
        }
        cell.errorText = @"";
    }
    
    cell.showsTopSeparator = NO;
    cell.showsBottomSeparator = YES;
    
    __typeof__(self) __weak myself = self;
    if ([self isProfessionOtherCellAtIndexPath:indexPath])
    {
        cell.showsTextInput = YES;
        cell.textInputChangedChander = ^(AORegQuestionnaireTableViewCell *theCell, NSString *text)
        {
            NSAssert([userAnswer isKindOfClass:[NSMutableDictionary class]], @"expecting NSMutableDictionary!");
            [(NSMutableDictionary *)userAnswer setObject:text forKey:@"textAnswer"];
        };
        cell.textInputDidEndEditing = ^(AORegQuestionnaireTableViewCell *theCell)
        {
            if (myself)
            {
                __typeof__(myself) __strong sself = myself;
                [sself sendUserAnswersWithBlock:nil];
            }
        };
        cell.textInputPlaceholder = AOLocalizedString(@"CMS.reg_opt_quest.text.question.profession");
        
        NSString *textAnswer = userAnswer[@"textAnswer"] == [NSNull null] ? @"" : userAnswer[@"textAnswer"];
        if (_nextButtonPressed && [question[@"mandatory"] boolValue] && [[textAnswer trimWhitespaces] length] == 0)
        {
            cell.errorText = AOLocalizedString(@"error.mandatory");
        }
        else
        {
            cell.errorText = @"";
        }
    }
    else
    {
        cell.showsTextInput = NO;
        cell.textInputChangedChander = nil;
        cell.textInputDidEndEditing = nil;
    }
}

- (void) configureCheckboxCell:(AoRegQuestionnaireCheckboxTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSInteger questionIndex = [self questionIndexFromIndexPath:indexPath];
    NSDictionary *question = questionnaire.questions[questionIndex];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.questionText = AOLocalizedString(question[@"name"]);
    
    NSUInteger index = [questionnaire.userAnswers indexOfObjectPassingTest:^BOOL(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        return [obj[@"answerId"] isKindOfClass:[NSNumber class]] && [obj[@"questionId"] integerValue] == [question[@"id"] integerValue];
    }];
    if (index != NSNotFound)
    {
        NSDictionary *userAnswer = questionnaire.userAnswers[index];
        NSInteger index = [question[@"answers"] indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            return [obj[@"id"] integerValue] == [userAnswer[@"answerId"] integerValue];
        }];
        BOOL isYes = NO;
        if (index != NSNotFound)
        {
            isYes = [question[@"answers"][index][@"name"] hasSuffix:@".yes"];
        }
        cell.checked = isYes;
    }
    else
    {
        BOOL checkedByDefault = NO;
        if ([question[@"id"] integerValue] == kQuestionAgree1Id || [question[@"id"] integerValue] == kQuestionAgree2Id || [question[@"id"] integerValue] == kQuestionAgree2bId)
        {
            NSUInteger index = [questionnaire.userAnswers indexOfObjectPassingTest:^BOOL(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
                return [obj[@"answerId"] isKindOfClass:[NSNumber class]] && [obj[@"questionId"] integerValue] == [question[@"id"] integerValue];
            }];
            
            if (index == NSNotFound || questionnaire.userAnswers[index][@"answerId"] == [NSNull null] || questionnaire.userAnswers[index][@"answerId"] == nil)
            {
                checkedByDefault = YES;
                [questionnaire setUserAnswerId:kYesIndex forQuestionId:questionIndex];
            }
        }
        
        cell.checked = checkedByDefault;
    }
    
    if (!_nextButtonPressed)
    {
        cell.errorMessage = @"";
    }
    else
    {
        cell.errorMessage = ((cell.checked || ![question[@"mandatory"] boolValue]) ? @"" : AOLocalizedString(@"error.mandatory"));
    }
    
    __typeof__(self) __weak myself = self;
    
    cell.checkedChanged = ^(AoRegQuestionnaireCheckboxTableViewCell *theCell) {
        if (myself)
        {
            __typeof__(myself) __strong sself = myself;
            NSInteger answerIndex = theCell.checked ? kYesIndex : kNoIndex;
            NSInteger questionIndex = [sself questionIndexFromIndexPath:indexPath];
            [sself->questionnaire setUserAnswerId:answerIndex forQuestionId:questionIndex];
            [sself sendUserAnswersWithBlock:nil];
            
            [sself->questionsTableView beginUpdates];
            [sself configureCheckboxCell:theCell atIndexPath:indexPath];
            [sself->questionsTableView endUpdates];
        }
    };
    
    cell.linkPressed = ^(AoRegQuestionnaireCheckboxTableViewCell *theCell) {
        if (myself)
        {
            [MAINVC loadPage:@"Account" pageName:@"Terms" root:NO options:@{@"countryId": @([DLG user].countryId)}];
        }
    };
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
}

#pragma mark request sample code

-(void)getQuestionnaire{
    
    AOGetQuestionnaireMethodRequest *request = [[AOGetQuestionnaireMethodRequest alloc] init];
    request.serviceMethod =@"getUserSingleQuestionnaire";
    request.afterDeposit = [NSNumber numberWithBool:NO];
    request.isLogin = [NSNumber numberWithBool:NO];
    [self startLoading];
    [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary *resultDictionary) {
        
        
        questionnaire = [[AORegQuestionnaire alloc] initWithDictionary:resultDictionary];
        
        [questionsTableView reloadData];

        
        
        // [self sendUserAnswersWithBlock:nil];
        
        [self stopLoading];
    } failed:^(NSDictionary *errDictionary) {
        // NSLog(@"failed: %@", errDictionary);
        [self stopLoading];
    }];
}

-(IBAction)submitQuestionnaire:(id)sender {
    
    
    [self startLoading];
    [self sendUserAnswersWithBlock:^(BOOL success){
        
        if (success) {
            AOUpdateQuestionnaireDoneRequest* request = [[AOUpdateQuestionnaireDoneRequest alloc] init];
            request.serviceMethod = @"updateSingleQuestionnaireDone";
            
            
            
            [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary *resultDictionary) {
                
                
                AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                NSMutableDictionary *userRegulation = [[resultDictionary objectForKey:@"userRegulation"] mutableCopy];
                [AOUtils validateDictionary:userRegulation withObject:delegate.userRegulation];
                [delegate.userRegulation setValuesForKeysWithDictionary:userRegulation];
                [self stopLoading];
                
                NSLog(@"resultDictionary: \n\n %@", userRegulation[@"pepState"]);
                
                if([userRegulation[@"pepState"] intValue] == 1 || [userRegulation[@"pepState"] intValue] == 2) {
                    [[MAINVC popupVC] showPopup:AOPEPRestricted withData:nil animated:YES completion:nil];
                }
                
                
                [RESIDENT goTo:@"home" withOptions:nil];

                RESIDENT.mainMenuSelectedIndex = MainMenuIndexHome;
                
                [RESIDENT checkRegulationKnowledge:AORegulationPEPActionOther];
                
                /*
                    
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:AOLocalizedString(@"questionnaire.mandatory.thankyou")
                                                               delegate:nil
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:AOLocalizedString(@"questionnaire.fill"),AOLocalizedString(@"optionPlus.button.not.now"), nil];
                
                alert.cancelButtonIndex=-1;
                
                alert.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex)
                {
                    if (buttonIndex == alertView.firstOtherButtonIndex)
                    {
                        //dataSource=[NSMutableArray arrayWithCapacity:0];
                       // [_collectionView reloadData];
                       // [self getQuestionnaire];
                    }
                    else
                    {
                        [RESIDENT goTo:@"home" withOptions:nil];
                        RESIDENT.mainMenuSelectedIndex = MainMenuIndexHome;
                    }
                };
                
                [alert show];
                    
               */
                
            } failed:^(NSDictionary *errDict) {
                //check for errorCode 304/305 ?
                
                if ([(NSError*)errDict[@"error"] code] == ERROR_CODE_REGULATION_USER_RESTRICTED ) {
                    
                    [[MAINVC popupVC] showPopup:AORegulationStatePopupRestricted withData:nil animated:YES completion:nil];
                    

                    
                    
                  
                }
                else if([(NSError*)errDict[@"error"] code] == ERROR_CODE_REG_SUSPENDED_QUEST_INCORRECT){
                                        [[MAINVC popupVC] showPopup:AORegulationStatePopupBlocked withData:nil animated:YES completion:nil];
                }
                else {
                    SHOW_DEFAULT_ERROR_NOTIFICATION();
                }
               
                
                
                 [RESIDENT goTo:@"home" withOptions:@{@"checkRegulationKnowledge":@YES}];
                
                
                
            }];
        }
        
        [self stopLoading];
    }];
}


-(void)sendUserAnswersWithBlock:(void (^)(BOOL success))completion {
    
    
    
    AOUpdateUserQuestionnaireAnswersRequest *request = [[AOUpdateUserQuestionnaireAnswersRequest alloc] init];
    request.encrypt = [NSNumber numberWithBool:YES];
    request.userAnswers = questionnaire.userAnswers;
    
    [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary *resultDictionary) {
        // NSLog(@"initial updateUserQuestionnaireAnswers success: %@", resultDictionary);
        
        AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        NSMutableDictionary *userRegulation = [[resultDictionary objectForKey:@"userRegulation"] mutableCopy];
        [AOUtils validateDictionary:userRegulation withObject:delegate.userRegulation];
        [delegate.userRegulation setValuesForKeysWithDictionary:userRegulation];
        if(completion){
            completion(YES);
        }
    } failed:^(NSDictionary *resultDictionary) {
        if(completion){
            completion(NO);
        }
    }];
    
}

#pragma mark -textField


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    
   // [self setUserMultipleAnswerWithUserAnswerId:(int)textField.tag andTextAnswer:textField.text andAnswerId:[NSNumber numberWithInt:202]];
    
    
    return YES;
}

#pragma mark Table View DS/DEL

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return questionnaireStep==1?kFirstPageNumQuestions:questionnaire.questions.count-kFirstPageNumQuestions;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    
    if ([self isCheckboxCellAtIndexPath:indexPath])
    {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        return;
    }

    AOPopupPickerViewController *pick = [self.storyboard instantiateViewControllerWithIdentifier:@"popupPickerVc"];
    
    selectedQuestionIndex = [self questionIndexFromIndexPath:indexPath];
    NSDictionary *question = [questionnaire.questions objectAtIndex:selectedQuestionIndex];
    NSArray *answers = question[@"answers"];
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:[answers count]];
    for (NSDictionary *answer in answers)
    {
        [items addObject:AOLocalizedString(answer[@"name"])];
    }
    
    pick.items = items;
    [pick showInViewController:MAINVC animated:YES];
    
    pick.selectionHandler = ^(AOPopupPickerViewController *picker, NSInteger selectedIndex)
    {
        if (selectedIndex != -1)
        {
            selectedAnswerIndex = selectedIndex;
            
            [questionnaire setUserAnswerId:selectedAnswerIndex forQuestionId:selectedQuestionIndex];
            
            [self sendUserAnswersWithBlock:nil];
        }
        
        AORegQuestionnaireTableViewCell *cell = (AORegQuestionnaireTableViewCell *)[questionsTableView cellForRowAtIndexPath:indexPath];
        [questionsTableView beginUpdates];
        [self configureCell:cell atIndexPath:indexPath];
        [questionsTableView endUpdates];
        
        [questionsTableView deselectRowAtIndexPath:indexPath animated:YES];
        
        [picker hideAnimated:YES completion:^{
            if ([self isProfessionOtherCellAtIndexPath:indexPath])
            {
                AORegQuestionnaireTableViewCell *cell = (AORegQuestionnaireTableViewCell *)[questionsTableView cellForRowAtIndexPath:indexPath];
                [cell showKeyboard];
            }
        }];
    };
}

- (NSInteger) questionIndexFromIndexPath:(NSIndexPath *)indexPath
{
    if (questionnaireStep == 1)
    {
        return indexPath.row;
    }
    else
    {
        return kFirstPageNumQuestions + indexPath.row;
    }
}

- (NSIndexPath *) indexPathFromQuestionIndex:(NSInteger)questionIndex
{
    if (questionnaireStep == 1)
    {
        return [NSIndexPath indexPathForRow:questionIndex inSection:0];
    }
    else
    {
        return [NSIndexPath indexPathForRow:questionIndex - kFirstPageNumQuestions inSection:0];
    }
}

- (BOOL) isCheckboxCellAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger questionIndex = [self questionIndexFromIndexPath:indexPath];
    return questionIndex >= [questionnaire.questions count] - 4; //magic
}

- (BOOL) isProfessionOtherCellAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger questionIndex = [self questionIndexFromIndexPath:indexPath];
    NSDictionary *question = questionnaire.questions[questionIndex];
    NSInteger questionId = [question[@"id"] integerValue];
    if (questionId == kProfessionQuesionId)
    {
        NSDictionary *userAnswer = [questionnaire userAnswerForQuestionId:questionId];
        if (userAnswer != nil && userAnswer[@"answerId"] != [NSNull null])
        {
            if ([userAnswer[@"answerId"] integerValue] == kProfessionAnswerOtherId)
            {
                return YES;
            }
        }
    }
    return NO;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self isCheckboxCellAtIndexPath:indexPath])
    {
        AoRegQuestionnaireCheckboxTableViewCell *cell = (AoRegQuestionnaireCheckboxTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kCheckboxCellIdentifier forIndexPath:indexPath];
        [self configureCheckboxCell:cell atIndexPath:indexPath];
        return cell;
    }
    else
    {
        AORegQuestionnaireTableViewCell *cell = (AORegQuestionnaireTableViewCell*)[tableView dequeueReusableCellWithIdentifier:AORegCellId forIndexPath:indexPath];
        
        [self configureCell:cell atIndexPath:indexPath];
        
        return cell;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![self isCheckboxCellAtIndexPath:indexPath])
    {
        static AORegQuestionnaireTableViewCell *cell = nil;
        
        if (cell == nil)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:AORegCellId];
        }
        
        [self configureCell:cell atIndexPath:indexPath];
        
        [cell setNeedsLayout];
        [cell layoutIfNeeded];
        
        CGSize size = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height + 1.0;
    }
    else
    {
        static AoRegQuestionnaireCheckboxTableViewCell *checkCell = nil;

        if (checkCell == nil)
        {
            checkCell = [tableView dequeueReusableCellWithIdentifier:kCheckboxCellIdentifier];
        }
        
        [self configureCheckboxCell:checkCell atIndexPath:indexPath];
        
        [checkCell setNeedsLayout];
        [checkCell layoutIfNeeded];
        
        CGSize size = [checkCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height + 1.0;
    }
}

- (void) p_configureSectionHeaderView:(AORegQuestionnaireSectionHeaderView *)headerView inSection:(NSInteger)section
{
    if (questionnaireStep == 1)
    {
        headerView.text = AOLocalizedString(@"regQuestionnaireEconomicBackground");
    }
    else
    {
        headerView.text = AOLocalizedString(@"regQuestionnaireTradingExperience");
    }
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    AORegQuestionnaireSectionHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kSectionHeader];
    [self p_configureSectionHeaderView:headerView inSection:section];
    return headerView;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    static AORegQuestionnaireSectionHeaderView *header;
    if (header == nil)
    {
        header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kSectionHeader];
    }
    [self p_configureSectionHeaderView:header inSection:section];
    CGSize size = [header systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

@end
