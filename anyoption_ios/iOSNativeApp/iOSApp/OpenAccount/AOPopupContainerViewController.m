//
//  AOPopupContainerViewController.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 9/1/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AOPopupContainerViewController.h"

@interface AOPopupContainerViewController ()

@end

@implementation AOPopupContainerViewController
{
    UIViewController *_contentVc;
}

- (id) initWithContentViewController:(UIViewController *)viewController
{
    if ((self = [super init]))
    {
        _contentVc = viewController;
    }
    
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [_contentVc.view layoutIfNeeded];
    
    _contentVc.view.center = CGPointMake(CGRectGetWidth(self.view.bounds) / 2.0, CGRectGetHeight(self.view.bounds) / 2.0);
    _contentVc.view.autoresizingMask = 0;
    
    [self.view addSubview:_contentVc.view];
    [self addChildViewController:_contentVc];
    
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.45];
}

- (void) showInParentViewController:(UIViewController *)parentViewController animated:(BOOL)animated
{
    self.view.frame = parentViewController.view.bounds;
    
    [parentViewController.view addSubview:self.view];
    [parentViewController addChildViewController:self];
    
    if (animated)
    {
        self.view.alpha = 0.0;
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.view.alpha = 1.0;
                         } completion:^(BOOL finished) {
                             
                         }];
    }
}

- (void) hideAnimated:(BOOL)animated completion:(dispatch_block_t)completion
{
    void (^compl)() = ^
    {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        
        if (completion)
        {
            completion();
        }
    };
    
    if (!animated)
    {
        compl();
    }
    else
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.view.alpha = 0.0;
                         } completion:^(BOOL finished) {
                             compl();
                         }];
    }
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self.view];
    if (!CGRectContainsPoint(_contentVc.view.frame, location))
    {
        if (self.didTapOutsideHandler)
        {
            self.didTapOutsideHandler();
        }
    }
}

@end
