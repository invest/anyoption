//
//  AOPopupPickerViewController.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/19/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AOPopupPickerViewController : UIViewController

@property (nonatomic, strong) NSArray *items;

@property (nonatomic, assign) NSInteger selectedIndex;

@property (nonatomic, copy) void (^selectionHandler)(AOPopupPickerViewController *picker, NSInteger selectedItemIndex);

- (void) showInViewController:(UIViewController *)parentViewController animated:(BOOL)animated;
- (void) hideAnimated:(BOOL)animated completion:(dispatch_block_t)completion;

@property (nonatomic, assign) CGFloat cellTopBottomPadding;

@end
