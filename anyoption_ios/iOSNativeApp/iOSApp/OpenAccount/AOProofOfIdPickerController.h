//
//  AOProofOfIdPickerController.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 9/1/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AOProofOfIdPickerController : UIViewController

@property (nonatomic, strong) NSArray *items; // of NSString
@property (nonatomic, copy) void (^selectionHandler)(NSInteger selectedIndex);

@end
