//
//  AODocumentsViewController.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/31/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AODocumentsViewController.h"

#import "AODocumentTableViewCell.h"
#import "AOProofOfIdPickerController.h"
#import "UIViewController+Helpers.h"

#ifndef COPYOP
#import "AOPopupContainerViewController.h"
#else
#import "COModalPopupViewController.h"
#import "COActionPickerViewController.h"
#endif

#import "AOUserMethodRequest.h"
#import "AORequestService.h"
#import "AOCreditCard.h"
#import "AOPopupPickerViewController.h"

@import AssetsLibrary;

typedef NS_ENUM(NSInteger, AODocFileType)
{
    AODocFileTypeId = 1,
    AODocFileTypePassport = 2,
    AODocFileTypeDriverLicense = 21,
    AODocFileTypeUtilityBill = 22,
    AODocFileTypeCCFront = 23,
    AODocFileTypeCCBack = 24,
};

typedef NS_ENUM(NSInteger, AODocFileStatus)
{
    AODocFileStatusRequested = 1,
    AODocFileStatusInProgress = 2,
    AODocFileStatusDone = 3,
    AODocFileStatusInvalid = 4,
    AODocFileStatusNotNeeded = 5
};

@interface AODocumentsViewController () <UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate
#ifndef COPYOP
    , UIActionSheetDelegate
#endif
>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblFooterTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewExclamation;
@property (weak, nonatomic) IBOutlet UILabel *lblUploadStatus;
@property (weak, nonatomic) IBOutlet UIView *vUploadStatus;

#ifdef COPYOP

@property (nonatomic, strong) COActionPickerViewController *avatarPicker;

#endif

@end

@implementation AODocumentsViewController
{
    NSDictionary *_docs;
    NSDictionary *_ccDocs;
    
    AODocFileType _currSelectedFileType;
    AOCreditCard *_currSelectedCard;
    
    NSArray *_creditCards;
    
    NSLayoutConstraint *_lcStatusHeight;
    
    BOOL _creditCardsFetched;
}

static NSString * const kDocumentCellIdentifier = @"DocumentCellIdentifier";

static const NSInteger kRowBill = 0;
static const NSInteger kRowId = 1;
static const NSInteger kRowCard = 2;
static const NSInteger kRowCardFront = 3;
static const NSInteger kRowCardBack = 4;

static NSString * const kStatusMsgIdKey = @"statusMsgId";

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    _currSelectedFileType = -1;
    
    [self p_collectData];
    
//    _blocked = YES;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    // customize header
    self.lblHeaderTitle.preferredMaxLayoutWidth = CGRectGetWidth(self.view.bounds) - 2.0 * 15.0;
    
    // customize footer
    self.lblFooterTitle.preferredMaxLayoutWidth = CGRectGetWidth(self.view.bounds) - 2.0 * 15.0;

#ifdef ANYOPTION
    self.imgViewExclamation.tintColor = [UIColor grayColor];
    self.imgViewExclamation.image = [self.imgViewExclamation.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
#endif
    
    self.lblUploadStatus.attributedText = [[NSAttributedString alloc] initWithString:AOLocalizedString(@"docsUploadSuccess")
                                                                          attributes:@{NSFontAttributeName : [UIFont fontWithName:APP_FONT size:12.0],
                                                                                       NSForegroundColorAttributeName : [UIColor grayColor]}];
    
    _lcStatusHeight = [NSLayoutConstraint constraintWithItem:self.vUploadStatus
                                                   attribute:NSLayoutAttributeHeight
                                                   relatedBy:NSLayoutRelationEqual
                                                      toItem:nil
                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                  multiplier:0.0
                                                    constant:0.0];
    [self.vUploadStatus addConstraint:_lcStatusHeight];
    
    [self p_updateUI];
}

- (BOOL) isBlocked
{
    return [self.navOptions[@"isBlocked"] boolValue];
}

- (AODocFileStatus) p_statusForCard:(AOCreditCard *)card
{
    NSString *key = [NSString stringWithFormat:@"%@", @(card.id)];
    NSAssert(_ccDocs[key], @"card not found!");
    
    NSDictionary *front = _ccDocs[key][[NSString stringWithFormat:@"%@", @(AODocFileTypeCCFront)]];
    NSDictionary *back = _ccDocs[key][[NSString stringWithFormat:@"%@", @(AODocFileTypeCCBack)]];
    
    if (front == nil || back == nil)
    {
        return AODocFileStatusRequested;
    }
    
    NSArray *priorities = [self p_ccPriorities];
    
    AODocFileStatus statFront = [front[kStatusMsgIdKey] integerValue];
    AODocFileStatus statBack = [back[kStatusMsgIdKey] integerValue];
    
    NSInteger frontIndex = [priorities indexOfObject:@(statFront)];
    NSInteger backIndex = [priorities indexOfObject:@(statBack)];

    if (frontIndex == NSNotFound || backIndex == NSNotFound)
    {
        return AODocFileStatusRequested;
    }
    
    if (frontIndex <= backIndex)
    {
        return statFront;
    }
    else
    {
        return statBack;
    }
}

- (NSArray *) p_ccPriorities
{
    return @[@(AODocFileStatusRequested), @(AODocFileStatusInProgress), @(AODocFileStatusInvalid), @(AODocFileStatusDone), @(AODocFileStatusNotNeeded)];
}

- (void) p_selectAppropriateCard
{
    if ([_creditCards count] == 1)
    {
        _currSelectedCard = [_creditCards firstObject];
    }
    else
    {
        if ([_creditCards count] == 0)
        {
            _currSelectedCard = nil;
        }
        else
        {
            NSMutableArray *cards = [NSMutableArray arrayWithArray:_creditCards];
            NSArray *priorities = [self p_ccPriorities];
            [cards sortUsingComparator:^NSComparisonResult(AOCreditCard *card1, AOCreditCard *card2)
             {
                 NSString *key1 = [NSString stringWithFormat:@"%@", @(card1.id)];
                 NSString *key2 = [NSString stringWithFormat:@"%@", @(card2.id)];
                 
                 if (_ccDocs[key1] != nil && _ccDocs[key2] != nil)
                 {
                     AODocFileStatus stat1 = [self p_statusForCard:card1];
                     AODocFileStatus stat2 = [self p_statusForCard:card2];
                     
                     NSInteger index1 = [priorities indexOfObject:@(stat1)];
                     NSInteger index2 = [priorities indexOfObject:@(stat2)];
                     
                     if (index1 == index2)
                     {
                         return NSOrderedSame;
                     }
                     else if (index1 < index2)
                     {
                         return NSOrderedAscending;
                     }
                     else
                     {
                         return NSOrderedDescending;
                     }
                 }
                 else if (_ccDocs[key1] == nil && _ccDocs[key2] == nil)
                 {
                     return NSOrderedSame;
                 }
                 else if (_ccDocs[key1] == nil)
                 {
                     return NSOrderedAscending;
                 }
                 else
                 {
                     return NSOrderedDescending;
                 }
             }];
            _currSelectedCard = [cards firstObject];
        }
    }
}

- (NSDictionary *) p_appropriateIdFileInfo
{
    NSMutableArray *idDocs = [NSMutableArray arrayWithCapacity:3];
    id docPassport = _docs[[NSString stringWithFormat:@"%@", @(AODocFileTypePassport)]];
    if (docPassport != nil)
    {
        [idDocs addObject:docPassport];
    }
    id docId = _docs[[NSString stringWithFormat:@"%@", @(AODocFileTypeId)]];
    if (docId != nil)
    {
        [idDocs addObject:docId];
    }
    id docDriversLicense = _docs[[NSString stringWithFormat:@"%@", @(AODocFileTypeDriverLicense)]];
    if (docDriversLicense != nil)
    {
        [idDocs addObject:docDriversLicense];
    }
    if ([idDocs count] > 0)
    {
        NSArray *priorities = @[@(AODocFileStatusDone), @(AODocFileStatusInProgress), @(AODocFileStatusInvalid), @(AODocFileStatusRequested), @(AODocFileStatusNotNeeded)];
        [idDocs sortUsingComparator:^NSComparisonResult(NSDictionary *docInfo1, NSDictionary *docInfo2)
         {
             AODocFileStatus stat1 = [docInfo1[kStatusMsgIdKey] integerValue];
             AODocFileStatus stat2 = [docInfo2[kStatusMsgIdKey] integerValue];
             NSInteger index1 = [priorities indexOfObject:@(stat1)];
             NSInteger index2 = [priorities indexOfObject:@(stat2)];
             
             if (index1 == NSNotFound || index2 == NSNotFound)
             {
                 if (index1 == NSNotFound && index2 == NSNotFound)
                 {
                     return NSOrderedSame;
                 }
                 else if (index1 == NSNotFound)
                 {
                     return NSOrderedDescending;
                 }
                 else
                 {
                     return NSOrderedAscending;
                 }
             }
             else
             {
                 if (index1 == index2)
                 {
                     return NSOrderedSame;
                 }
                 else if (index1 < index2)
                 {
                     return NSOrderedAscending;
                 }
                 else
                 {
                     return NSOrderedDescending;
                 }
             }
         }];
        return [idDocs firstObject];
    }
    return nil;
}

#warning this is taken from AODepositMenu, move to the facade later
-(void) getCards
{
    AOUserMethodRequest *request = [[AOUserMethodRequest alloc] init];
    request.serviceMethod = @"getAvailableCreditCards";
    
    [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary *resultDictionary) {
        // NSLog(@"getAvailableCreditCards success: %@",resultDictionary);

        NSMutableArray *ccArray = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in [resultDictionary objectForKey:@"options"]){
            AOCreditCard *creditCard = [[AOCreditCard alloc] init];
            [creditCard setValuesForKeysWithDictionary:dict];
            [ccArray addObject:creditCard];
        }

        _creditCardsFetched = YES;

        _creditCards = [NSArray arrayWithArray:ccArray];
        
        [self p_selectAppropriateCard];
        
        [self.tableView reloadData];
        [self.tableView layoutIfNeeded];
    } failed:^(NSDictionary *errDictionary) {
        // NSLog(@"getAvailableCreditCards error: %@",errDictionary);
    }];
}

- (void) p_collectData
{
    [self.facade getDocumentsWithCompletion:^(NSDictionary *docs, NSDictionary *ccDocs, AOResponse *response) {
        if (response.error != nil)
        {
            [response.error show];
            
            return;
        }
        else
        {
            _docs = docs;
            _ccDocs = ccDocs;
            
            if (!_creditCardsFetched)
            {
                [self getCards];
            }
            else
            {
                [self.tableView reloadData];
                [self.tableView layoutIfNeeded];
            }
        }
    }];
}

- (void) p_updateUI
{
    if (![self isViewLoaded])
    {
        return;
    }
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    if (![self isBlocked])
    {
        // update the header text
        NSString *pleaseUpload = AOLocalizedString(@"docsPleaseUpload");
        NSArray *boldRanges = nil;
        NSString *processed = [AOUtils processString:pleaseUpload tag:@"b" ranges:&boldRanges];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:processed
                                          
                                                                                attributes:@{NSForegroundColorAttributeName : [theme textColor],
                                                                                             NSFontAttributeName : [theme sectionTextFont]}];
        
        for (NSValue *range in boldRanges)
        {
            [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:APP_FONT_BOLD size: 12.0] range:[range rangeValue]];
        }
        
        self.lblHeaderTitle.attributedText = str;
        
        // update the footer text
        NSString *docsRequired = AOLocalizedString(@"docsFooterRequired");
        NSAttributedString *docsRequiredAttr = [[NSAttributedString alloc] initWithString:docsRequired
                                                                               attributes:@{NSFontAttributeName : [theme sectionTextFont],
                                                                                            NSForegroundColorAttributeName : [theme textColor]}];
        NSString *docsNote = [NSString stringWithFormat:@"\n\n%@", AOLocalizedString(@"docsFooterNote")];
        NSArray *noteRanges = nil;
        NSString *noteProcessed = [AOUtils processString:docsNote tag:@"b" ranges:&noteRanges];
        NSMutableAttributedString *noteAttr = [[NSMutableAttributedString alloc] initWithString:noteProcessed
                                                                                     attributes:@{NSForegroundColorAttributeName : [theme docsFooterNoteTextColor],
                                                                                                  NSFontAttributeName : [theme sectionTextFont]}];
        for (NSValue *range in noteRanges)
        {
            [noteAttr addAttribute:NSFontAttributeName value:[UIFont fontWithName:APP_FONT_BOLD size:12.0] range:[range rangeValue]];
        }
        
        NSMutableAttributedString *allFooter = [[NSMutableAttributedString alloc] init];
        [allFooter appendAttributedString:docsRequiredAttr];
        [allFooter appendAttributedString:noteAttr];
        
        self.lblFooterTitle.attributedText = allFooter;
    }
    else
    {
        // update header
        NSString *blockedStr = AOLocalizedString(@"docsAutomaticallyBlocked");
        NSAttributedString *blockedAttr = [[NSAttributedString alloc] initWithString:blockedStr
                                                                          attributes:@{NSFontAttributeName : [theme sectionTextFont],
                                                                                       NSForegroundColorAttributeName : [theme regSectionTextColor]}];
        NSString *inOrderStr = [NSString stringWithFormat:@"\n\n%@", AOLocalizedString(@"docsInOrderToContinue")];
        NSArray *boldRanges = nil;
        NSString *processed = [AOUtils processString:inOrderStr tag:@"b" ranges:&boldRanges];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:processed
                                                                                attributes:@{NSForegroundColorAttributeName : [theme textColor],
                                                                                             NSFontAttributeName : [theme sectionTextFont]}];
        
        for (NSValue *range in boldRanges)
        {
            [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:APP_FONT_BOLD size: 12.0] range:[range rangeValue]];
        }
        
        NSMutableAttributedString *allHeader = [[NSMutableAttributedString alloc] init];
        [allHeader appendAttributedString:blockedAttr];
        [allHeader appendAttributedString:str];
        
        self.lblHeaderTitle.attributedText = allHeader;
        
        // update footer
        NSString *docsRequired = AOLocalizedString(@"docsFooterRequired");
        NSAttributedString *docsRequiredAttr = [[NSAttributedString alloc] initWithString:docsRequired
                                                                               attributes:@{NSFontAttributeName : [theme sectionTextFont],
                                                                                            NSForegroundColorAttributeName : [theme textColor]}];
        self.lblFooterTitle.attributedText = docsRequiredAttr;
    }
    
    UIView *headerView = self.tableView.tableHeaderView;
    [headerView layoutIfNeeded];
    CGSize headerPreferredSize = [headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    CGRect fr = headerView.bounds;
    fr.size.height = headerPreferredSize.height;
    headerView.bounds = fr;
    
    [self p_updateFooterHeight];
    
    [self buildTitleViewWithText:[self screenTitle] andImage:nil];
}

- (void) p_updateFooterHeight
{
    UIView *footerView = self.tableView.tableFooterView;
    [footerView layoutIfNeeded];
    CGSize footerPreferredSize = [footerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    CGRect footerFr = footerView.frame;
    footerFr.size.height = footerPreferredSize.height;
    footerView.frame = footerFr;
}

- (void) handlePhotoPickerInput:(NSInteger)selectedIndex
{
    BOOL fromCamera = (selectedIndex == 0);
    [self takePhotoFromCamera:fromCamera];
}

- (void) takePhotoFromCamera:(BOOL)fromCamera
{
    if (fromCamera)
    {
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [[[UIAlertView alloc] initWithTitle:@"Camera not available!" message:nil delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil] show];
            return;
        }
    }
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
//    picker.allowsEditing = YES;
    picker.sourceType = (fromCamera ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary);
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        [popover presentPopoverFromRect:self.view.bounds inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        [self presentViewController:picker animated:YES completion:NULL];
    }
//    [self presentViewController:picker animated:YES completion:NULL];
}

- (NSString *) screenTitle
{
    if ([self isBlocked])
    {
        return AOLocalizedString(@"docsAccountBlockedTitle");
    }
    else
    {
        return AOLocalizedString(@"docsUploadDocumentsTitle");
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self buildTitleViewWithText:[self screenTitle] andImage:nil];
}

#pragma mark - UITableView methods

- (NSString *) cellTitleAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case kRowBill:
            return AOLocalizedString(@"docsUtilityBill");
        case kRowId:
            return AOLocalizedString(@"docsProofOfId");
        case kRowCard:
            return AOLocalizedString(@"CMS.documents.files.cc");
        case kRowCardFront:
            return AOLocalizedString(@"docsFrontShort");
        case kRowCardBack:
            return AOLocalizedString(@"docsBackShort");
        default:
            NSAssert(nil, @"unhandled case!");
            return nil;
    }
}

+ (NSString *) stringFromFileStatus:(AODocFileStatus)status
{
    switch (status)
    {
        case AODocFileStatusDone:
            return AOLocalizedString(@"docsStatusApproved");
        case AODocFileStatusInProgress:
            return AOLocalizedString(@"docsStatusWaiting");
        case AODocFileStatusInvalid:
            return AOLocalizedString(@"docsStatusInvalid");
        case AODocFileStatusNotNeeded:
            return @"";
        case AODocFileStatusRequested:
            return AOLocalizedString(@"docsStatusMissing");
        default:
            return @"";
    }
}

+ (UIColor *) colorForFileStatus:(AODocFileStatus)status
{
    switch (status)
    {
        case AODocFileStatusDone:
            return [UIColor greenColor];
        case AODocFileStatusInProgress:
            return [UIColor grayColor];
        case AODocFileStatusInvalid:
            return [UIColor redColor];
        case AODocFileStatusNotNeeded:
            return [UIColor blackColor];
        case AODocFileStatusRequested:
            return [UIColor redColor];
        default:
            return [UIColor blackColor];
    }
}

- (void) p_showCardPicker
{
    AOPopupPickerViewController *pick = [self.storyboard instantiateViewControllerWithIdentifier:@"popupPickerVc"];

    NSMutableArray *items = [NSMutableArray arrayWithCapacity:[_creditCards count]];
    for (AOCreditCard *card in _creditCards)
    {
        NSString *text = [NSString stringWithFormat:@"%@ (%@)",AOLocalizedString([card.type objectForKey:@"name"]),[AOUtils formatCardNumberShort:card.ccNumber]];
        [items addObject:text];
    }
    
    pick.items = items;
    [pick showInViewController:MAINVC animated:YES];
    
    pick.selectionHandler = ^(AOPopupPickerViewController *picker, NSInteger selectedIndex)
    {
        if (selectedIndex != -1)
        {
            _currSelectedCard = _creditCards[selectedIndex];
            
            [self.tableView beginUpdates];
            
            NSIndexPath *card = [NSIndexPath indexPathForRow:kRowCard inSection:0];
            NSIndexPath *front = [NSIndexPath indexPathForRow:kRowCardFront inSection:0];
            NSIndexPath *back = [NSIndexPath indexPathForRow:kRowCardBack inSection:0];
            
            [self.tableView reloadRowsAtIndexPaths:@[card, front, back] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            [self.tableView endUpdates];
        }
        
        [picker hideAnimated:YES completion:^{
        }];
    };
}

- (void) p_updateSelectionStyleForCell:(AODocumentTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == kRowCard || indexPath.row == kRowCardFront || indexPath.row == kRowCardBack)
    {
        if ([_creditCards count] == 0)
        {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.showsArrow = NO;
        }
        else if ([_creditCards count] == 1)
        {
            if (indexPath.row == kRowCard)
            {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.showsArrow = NO;
            }
            else
            {
                cell.selectionStyle = UITableViewCellSelectionStyleDefault;
                cell.showsArrow = YES;
            }
        }
        else
        {
            if (_currSelectedCard != nil)
            {
                cell.selectionStyle = UITableViewCellSelectionStyleDefault;
                cell.showsArrow = YES;
            }
            else
            {
                if (indexPath.row == kRowCard)
                {
                    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
                    cell.showsArrow = YES;
                }
                else
                {
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.showsArrow = NO;
                }
            }
        }
    }
    else
    {
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.showsArrow = YES;
    }
}

- (void) p_configureDocumentCell:(AODocumentTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.contentView.backgroundColor = [UIColor whiteColor];

    [self p_updateSelectionStyleForCell:cell atIndexPath:indexPath];

    if (indexPath.row == kRowCardBack)
    {
        cell.iconImage = [UIImage imageNamed:@"cc_back_icon"];
    }
    else if (indexPath.row == kRowCardFront)
    {
        cell.iconImage = [UIImage imageNamed:@"cc_front_icon"];
    }
    else
    {
        cell.iconImage = nil;
    }
    
    if ([cell respondsToSelector:@selector(preservesSuperviewLayoutMargins)])
    {
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
    }

    cell.titleText = [self cellTitleAtIndexPath:indexPath];
    
    if (indexPath.row == kRowBill || indexPath.row == kRowId || indexPath.row == kRowCardFront || indexPath.row == kRowCardBack)
    {
        NSDictionary *fileInfo = nil;
        
        if (indexPath.row == kRowBill)
        {
            fileInfo = _docs[[NSString stringWithFormat:@"%@", @(AODocFileTypeUtilityBill)]];
        }
        else if (indexPath.row == kRowId)
        {
            fileInfo = [self p_appropriateIdFileInfo];
        }
        else
        {
            if (_currSelectedCard != nil)
            {
                NSDictionary *cardInfo = _ccDocs[[NSString stringWithFormat:@"%@", @(_currSelectedCard.id)]];
                if (indexPath.row == kRowCardFront)
                {
                    fileInfo = cardInfo[[NSString stringWithFormat:@"%@", @(AODocFileTypeCCFront)]];
                }
                else
                {
                    fileInfo = cardInfo[[NSString stringWithFormat:@"%@", @(AODocFileTypeCCBack)]];
                }
            }
        }
        
        if (fileInfo == nil)
        {
            cell.subtitleText = [[self class] stringFromFileStatus:AODocFileStatusRequested];
            cell.valueText = @"";
        }
        else
        {
            if ([fileInfo[@"isLock"] boolValue])
            {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.showsArrow = NO;
            }

            AODocFileStatus status = [fileInfo[kStatusMsgIdKey] integerValue];
            
            cell.subtitleText = [[self class] stringFromFileStatus:status];
            cell.subtitleColor = [[self class] colorForFileStatus:status];
            
            id name = fileInfo[@"fileNameForClient"];
            if (name == [NSNull null])
            {
                name = @"";
            }
            cell.valueText = name;
        }
    }
    else if (indexPath.row == kRowCard)
    {
        if (_currSelectedCard != nil)
        {
            cell.valueText = [NSString stringWithFormat:@"%@ (%@)",AOLocalizedString([_currSelectedCard.type objectForKey:@"name"]),[AOUtils formatCardNumberShort:_currSelectedCard.ccNumber]];
        }
        else
        {
            cell.valueText = @"";
        }
        cell.subtitleText = @"";
    }
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AODocumentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDocumentCellIdentifier forIndexPath:indexPath];
    [self p_configureDocumentCell:cell atIndexPath:indexPath];
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static AODocumentTableViewCell *cell = nil;
    
    if (cell == nil)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:kDocumentCellIdentifier];
    }
    
    [self p_configureDocumentCell:cell atIndexPath:indexPath];
    
    [cell layoutIfNeeded];
    
    CGSize size = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return MAX(size.height + 1.0, 50.0);
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return FLT_MIN;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == kRowCard)
    {
        if ([_creditCards count] > 1)
        {
            [self p_showCardPicker];
        }
        
        return;
    }
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell)
    {
        if (cell.selectionStyle == UITableViewCellSelectionStyleNone)
        {
            return;
        }
    }
    
    if ((indexPath.row == kRowCardFront || indexPath.row == kRowCardBack) && _currSelectedCard == nil)
    {
        return;
    }
    
    switch (indexPath.row)
    {
        case kRowBill:
        {
            _currSelectedFileType = AODocFileTypeUtilityBill;
            break;
        }
        case kRowCardFront:
        {
            _currSelectedFileType = AODocFileTypeCCFront;
            break;
        }
        case kRowCardBack:
        {
            _currSelectedFileType = AODocFileTypeCCBack;
            break;
        }
        default:
            break;
    }

    if (indexPath.row == kRowId)
    {
        __typeof__(self) __weak myself = self;
        
        AOProofOfIdPickerController *proofPicker = [self.storyboard instantiateViewControllerWithIdentifier:@"proofOfIdPickerVc"];
        proofPicker.items = @[AOLocalizedString(@"docsIdPassport"), AOLocalizedString(@"docsIdId"), AOLocalizedString(@"docsIdDriversLicense")];
#ifndef COPYOP
        AOPopupContainerViewController *containerVc = [[AOPopupContainerViewController alloc] initWithContentViewController:proofPicker];
        __typeof__(containerVc) __weak wContainer = containerVc;
        containerVc.didTapOutsideHandler = ^{
            [wContainer hideAnimated:YES completion:nil];
        };
        [containerVc showInParentViewController:MAINVC animated:YES];
#else
        COModalPopupViewController *popupVc = [[COModalPopupViewController alloc] initWithContentViewController:proofPicker];
        
        popupVc.closeButtonPressedHandler = popupVc.didTapOutsideHandler = ^{
            if (myself)
            {
                __typeof__(myself) __strong sself = myself;
                [sself dismissViewControllerAnimated:YES completion:nil];
            }
        };
        [self presentViewController:popupVc animated:YES completion:nil];
#endif
        proofPicker.selectionHandler = ^(NSInteger selectedIndex)
        {
            if (myself)
            {
                __typeof(myself) __strong sself = myself;
                
                switch (selectedIndex)
                {
                    case 0:
                        sself->_currSelectedFileType = AODocFileTypePassport;
                        break;
                    case 1:
                        sself->_currSelectedFileType = AODocFileTypeId;
                        break;
                    case 2:
                        sself->_currSelectedFileType = AODocFileTypeDriverLicense;
                        break;
                    default:
                        NSAssert(nil, @"this should not be possible!");
                }
                
#ifdef COPYOP
                [sself dismissViewControllerAnimated:YES completion:^{
                    [sself p_presentImagePicker];
                }];
#else
                [wContainer hideAnimated:YES completion:^{
                    [sself p_presentImagePicker];
                }];
#endif
            }
        };
    }
    else
    {
        [self p_presentImagePicker];
    }
}

- (void) p_presentImagePicker
{
#ifdef COPYOP
    COActionPickerViewController *picker = [[COActionPickerViewController alloc] initWithButtonTitles:@[AOLocalizedString(@"takePicture"),
                                                                                                        AOLocalizedString(@"uploadFromGallery")]
                                                                                    cancelButtonTitle:@"Cancel"];
    
    __typeof__(self) __weak myself = self;
    
    picker.selectionHandler = ^(NSInteger selectedButtonIndex)
    {
        if (myself)
        {
            __typeof__(myself) __strong sself = myself;
            [sself dismissViewControllerAnimated:YES completion:nil];
            sself.avatarPicker = nil;
            
            if (selectedButtonIndex != kCOActionPickerViewControllerCancelButtonIndex)
            {
                [sself handlePhotoPickerInput:selectedButtonIndex];
            }
        }
    };
    
    self.avatarPicker = picker;
    
    [picker presentInViewController:self animated:YES completionHander:nil];
#else
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:AOLocalizedString(@"button.cancel")
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:
                            AOLocalizedString(@"takePicture"),
                            AOLocalizedString(@"uploadFromGallery"), nil];
    [sheet showInView:self.view];
#endif
}

#pragma mark - UIActionSheetDelegate methods

#ifdef ANYOPTION

- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != actionSheet.cancelButtonIndex)
    {
        [self handlePhotoPickerInput:buttonIndex];
    }
}

#endif

#pragma mark - UIImagePickerControllerDelegate

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    void (^uploadFile)(NSString *fileName) = ^(NSString *fileName)
    {
        NSAssert((NSInteger)_currSelectedFileType != -1, @"file type not selected!");
        
        long ccId = -1;
        
        if (_currSelectedFileType == AODocFileTypeCCFront || _currSelectedFileType == AODocFileTypeCCBack)
        {
            // upload cc document
            NSAssert(_currSelectedCard != nil, @"card not selected!");
            ccId = _currSelectedCard.id;
        }
        
        [self.facade uploadDocumentWithFileType:_currSelectedFileType fileName:fileName ccId:ccId image:chosenImage completion:^(AOResponse *response) {
            if (response.error != nil)
            {
                [self handleResponceError:response.error];
            }
            else
            {
                [self.vUploadStatus removeConstraint:_lcStatusHeight];
                [self.tableView.tableFooterView setNeedsLayout];
                [self p_updateFooterHeight];
                
                [self p_collectData];
            }
        }];
    };
    
    void (^uploadFileWithRandomName)() = ^
    {
        NSString *fileName = [NSString stringWithFormat:@"IMG0%03ld.PNG", (long)(arc4random() % 900 + 100)];
        uploadFile(fileName);
    };
    
    // get the ref url
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    
    // define the block to call when we get the asset based on the url (below)
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
    {
        ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
        NSString *fileName = [imageRep filename];
        if ([fileName length] == 0)
        {
            uploadFileWithRandomName();
        }
        else
        {
            uploadFile(fileName);
        }
    };
    
    ALAssetsLibraryAccessFailureBlock failBlock = ^(NSError *error)
    {
        uploadFileWithRandomName();
    };
    
    // get the asset library and fetch the asset based on the ref url (pass in block above)
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:failBlock];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

@end
