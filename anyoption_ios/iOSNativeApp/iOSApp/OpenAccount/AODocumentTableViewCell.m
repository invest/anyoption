//
//  AODocumentTableViewCell.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/31/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AODocumentTableViewCell.h"

@interface AODocumentTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcIconImageWidth;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewArrow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcImageTitleDist;

@end

@implementation AODocumentTableViewCell

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    NSLayoutConstraint *valueWidthConstraint = [NSLayoutConstraint constraintWithItem:self.lblValue
                                                                            attribute:NSLayoutAttributeWidth
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.contentView
                                                                            attribute:NSLayoutAttributeWidth
                                                                           multiplier:0.35
                                                                             constant:0.0];
    [self.contentView addConstraint:valueWidthConstraint];
    
    self.lblSubtitle.font = [self.lblSubtitle.font fontWithSize:8.0];
    self.lblSubtitle.textColor = [UIColor lightGrayColor];
    
    self.lblValue.textColor = [UIColor lightGrayColor];
    self.lblValue.font = [self.lblValue.font fontWithSize:10.0];
    
    self.lblTitle.textColor = [theme collectionCellLabelColor];
    self.lblTitle.font = [theme docsCellTitleFont];
    
    [self p_updateTitleText];
    [self p_updateSubtitleText];
    [self p_updateValueText];
    [self p_updateIconImage];
    [self p_updateShowsArrow];
    [self p_updateSubtitleColor];
}

GENERATE_SETTER(setTitleText, NSString *, titleText, p_updateTitleText);
GENERATE_SETTER(setSubtitleText, NSString *, subtitleText, p_updateSubtitleText);
GENERATE_SETTER(setValueText, NSString *, valueText, p_updateValueText);
GENERATE_SETTER(setIconImage, UIImage *, iconImage, p_updateIconImage);
GENERATE_SETTER(setShowsArrow, BOOL, showsArrow, p_updateShowsArrow);
GENERATE_SETTER(setSubtitleColor, UIColor *, subtitleColor, p_updateSubtitleColor);

- (void) p_updateTitleText
{
    self.lblTitle.text = self.titleText;
}

- (void) p_updateSubtitleText
{
    self.lblSubtitle.text = self.subtitleText;
}

- (void) p_updateSubtitleColor
{
    self.lblSubtitle.textColor = self.subtitleColor;
}

- (void) p_updateValueText
{
    self.lblValue.text = self.valueText;
}

- (void) p_updateShowsArrow
{
    self.imgViewArrow.hidden = !self.showsArrow;
}

- (void) p_updateIconImage
{
    self.imgViewIcon.image = self.iconImage;
    
    if (self.iconImage == nil)
    {
        self.lcIconImageWidth.constant = 0.0;
        self.lcImageTitleDist.constant = 0.0;
    }
    else
    {
        self.lcIconImageWidth.constant = 20.0;
        self.lcImageTitleDist.constant = 8.0;
    }
}

@end
