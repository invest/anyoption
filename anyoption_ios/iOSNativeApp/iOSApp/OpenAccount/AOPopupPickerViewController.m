//
//  AOPopupPickerViewController.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/19/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AOPopupPickerViewController.h"

#import "AOPopupPickerTableViewCell.h"

@interface AOPopupPickerViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcTableHeight;

@end

@implementation AOPopupPickerViewController

static NSString * const kCellIdentifier = @"CellIdentifier";

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    [self setup];
    return self;
}

- (instancetype) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    [self setup];
    return self;
}

- (void) setup
{
    _cellTopBottomPadding = [AOPopupPickerTableViewCell defaultTopBottomPadding];
    _selectedIndex = -1;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    self.tableView.backgroundColor = [theme popupPickerTableBackgroundColor];
    self.tableView.separatorColor = [theme popupPickerTableSeparatorColor];
    self.tableView.layer.cornerRadius = 8.0;
    
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 0.0, FLT_MIN)];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 0.0, FLT_MIN)];
    
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.45];
    
    [self p_adjustSize];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self p_markSelectedRow];
}

- (void) setItems:(NSArray *)items
{
    _items = items;
    
    if (![self isViewLoaded])
    {
        return;
    }
    
    _selectedIndex = -1;
    
    [self p_adjustSize];
}

- (void) setSelectedIndex:(NSInteger)selectedIndex
{
    NSParameterAssert(selectedIndex == -1 || selectedIndex < [_items count]);
    _selectedIndex = selectedIndex;

    [self p_markSelectedRow];
}

- (void) p_markSelectedRow
{
    if (![self isViewLoaded])
    {
        return;
    }
    
    if (_selectedIndex == -1)
    {
        if ([self.tableView indexPathForSelectedRow] != nil)
        {
            [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:NO];
        }
    }
    else
    {
        NSIndexPath *path = [NSIndexPath indexPathForRow:_selectedIndex inSection:0];
        [self.tableView selectRowAtIndexPath:path animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
}

- (void) showInViewController:(UIViewController *)parentViewController animated:(BOOL)animated
{
    self.view.frame = parentViewController.view.bounds;
    
    [parentViewController.view addSubview:self.view];
    [parentViewController addChildViewController:self];
    
    if (animated)
    {
        self.view.alpha = 0.0;
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.view.alpha = 1.0;
                         } completion:^(BOOL finished) {
                             
                         }];
    }
}

- (void) hideAnimated:(BOOL)animated completion:(dispatch_block_t)completion
{
    void (^compl)() = ^
    {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        
        if (completion)
        {
            completion();
        }
    };
    
    if (!animated)
    {
        compl();
    }
    else
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.view.alpha = 0.0;
                         } completion:^(BOOL finished) {
                             compl();
                         }];
    }
}

- (void) p_adjustSize
{
    [self.tableView reloadData];
    [self.tableView layoutIfNeeded];
    
    self.lcTableHeight.constant = MIN(self.tableView.contentSize.height, CGRectGetHeight(self.view.bounds) * 2.0 / 3.0);
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.selectionHandler)
    {
        self.selectionHandler(self, -1);
    }
}

#pragma mark - UITableView methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AOPopupPickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    [self p_configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.selectionHandler)
    {
        self.selectionHandler(self, indexPath.row);
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static AOPopupPickerTableViewCell *cell = nil;
    
    if (cell == nil)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    }
    
    [self p_configureCell:cell atIndexPath:indexPath];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    CGSize size = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1.0;
}

- (void) p_configureCell:(AOPopupPickerTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.title = self.items[indexPath.row];
    
    cell.topBottomPadding = self.cellTopBottomPadding;
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(preservesSuperviewLayoutMargins)])
    {
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = NO;
    }
}

- (void) setCellTopBottomPadding:(CGFloat)cellTopBottomPadding
{
    _cellTopBottomPadding = cellTopBottomPadding;
    
    if ([self isViewLoaded])
    {
        [self.tableView reloadData];
        [self.tableView layoutIfNeeded];
    }
}

@end
