//
//  AORegQuestionnaireTableViewCell.m
//  iOSApp
//
//  Created by  on 8/14/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AORegQuestionnaireTableViewCell.h"

@interface AORegQuestionnaireTableViewCell () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (weak, nonatomic) IBOutlet UILabel *lblAnswer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcSepHeight;
@property (weak, nonatomic) IBOutlet UIView *vTopSeparator;
@property (weak, nonatomic) IBOutlet UIView *vBottomSeparator;
@property (weak, nonatomic) IBOutlet UILabel *lblErrorMessage;
@property (weak, nonatomic) IBOutlet UITextField *tfTextInput;

@end

@implementation AORegQuestionnaireTableViewCell
{
    NSLayoutConstraint *_lcTextInputHeight;
    BOOL _lcTextInputHeightAdded;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    _lcTextInputHeight = [NSLayoutConstraint constraintWithItem:self.tfTextInput
                                                      attribute:NSLayoutAttributeHeight
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:nil
                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                     multiplier:0.0
                                                       constant:0.0];

    self.lcSepHeight.constant = 1.0 / [UIScreen mainScreen].scale;
    
    UIColor *sepColor = [UIColor colorWithRed:208.0/255.0 green:224.0/255.0 blue:231.0/255.0 alpha:1.0];
    self.vTopSeparator.backgroundColor = self.vBottomSeparator.backgroundColor = sepColor;
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    self.lblQuestion.textColor = [theme textColor];
    self.lblQuestion.font = [self.lblQuestion.font fontWithSize:13.0];
    
    self.lblAnswer.textColor = [UIColor lightGrayColor];
    self.lblAnswer.font = [self.lblAnswer.font fontWithSize:10.0];
    
    self.lblErrorMessage.textColor = [UIColor redColor];
    self.lblErrorMessage.font = [self.lblErrorMessage.font fontWithSize:10.0];
    
    [self p_updateQuestionText];
    [self p_updateAnswerText];
    [self p_updateTopSeparator];
    [self p_updateBottomSeparator];
    [self p_updateErrorText];
    [self p_updateShowsTextInput];
    [self p_updateTextInputPlaceholder];
    
    self.lblErrorMessage.text = @"";
}

GENERATE_SETTER(setQuestionText, NSString *, questionText, p_updateQuestionText);
GENERATE_SETTER(setAnswerText, NSString *, answerText, p_updateAnswerText);
GENERATE_SETTER(setErrorText, NSString *, errorText, p_updateErrorText);
GENERATE_SETTER(setShowsTopSeparator, BOOL, showsTopSeparator, p_updateTopSeparator);
GENERATE_SETTER(setShowsBottomSeparator, BOOL, showsBottomSeparator, p_updateBottomSeparator);
GENERATE_SETTER(setShowsTextInput, BOOL, showsTextInput, p_updateShowsTextInput);
GENERATE_SETTER(setTextInputPlaceholder, NSString *, textInputPlaceholder, p_updateTextInputPlaceholder);

- (void) showKeyboard
{
    NSAssert(self.showsTextInput, @"expecting self.showsTextInput == YES");
    [self.tfTextInput becomeFirstResponder];
}

- (void) p_updateTextInputPlaceholder
{
    self.tfTextInput.placeholder = self.textInputPlaceholder;
}

- (void) p_updateShowsTextInput
{
    self.tfTextInput.hidden = !self.showsTextInput;
    self.lblAnswer.hidden = self.showsTextInput;
    
    if (self.showsTextInput)
    {
        [self.tfTextInput removeConstraint:_lcTextInputHeight];
        _lcTextInputHeightAdded = NO;
    }
    else
    {
        if (!_lcTextInputHeightAdded)
        {
            [self.tfTextInput addConstraint:_lcTextInputHeight];
            _lcTextInputHeightAdded = YES;
        }
    }
}

- (IBAction) tfEditingChanged:(id)sender
{
    if (self.textInputChangedChander)
    {
        self.textInputChangedChander(self, self.tfTextInput.text);
    }
}

- (void) p_updateQuestionText
{
    self.lblQuestion.text = self.questionText;
}

- (void) p_updateAnswerText
{
    self.lblAnswer.text = self.answerText;
    self.tfTextInput.text = self.answerText;
}

- (void) p_updateTopSeparator
{
    self.vTopSeparator.hidden = !self.showsTopSeparator;
}

- (void) p_updateBottomSeparator
{
    self.vBottomSeparator.hidden = !self.showsBottomSeparator;
}

- (void) p_updateErrorText
{
    self.lblErrorMessage.text = self.errorText;
}

#pragma mark - UITextField delegate methods

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (self.textInputDidEndEditing)
    {
        self.textInputDidEndEditing(self);
    }
    
    return YES;
}

@end
