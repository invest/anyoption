//
//  AORegQuestionnaire.m
//  iOSApp
//
//  Created by  on 8/14/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AORegQuestionnaire.h"

const NSInteger kProfessionQuesionId = 85;
const NSInteger kProfessionAnswerOtherId = 365;

@implementation AORegQuestionnaire

-(id)initWithDictionary:(NSDictionary*)regQuestionnaireResponseDictionary{
    self = [super init];
    
    
    if (self) {
        _questions = [[NSMutableArray alloc] initWithArray:[regQuestionnaireResponseDictionary objectForKey:@"questions"]];
        _userAnswers = [NSMutableArray arrayWithArray:[regQuestionnaireResponseDictionary objectForKey:@"userAnswers"]];
        
        
        //
        
        
    }
    
    return self;
}

-(NSArray*)answersForQuestionAt:(NSInteger)qIndex {
    return [_questions[qIndex] objectForKey:@"answers"];
}

-(NSInteger)questionIndexForId:(NSInteger)questionId{
    for (int q=0; q<_questions.count; q++) {
        if([[_questions[q] objectForKey:@"id"] intValue] == questionId){
            return q;
        }
    }
    return -1;
}

-(NSDictionary *) userAnswerForQuestionId:(NSInteger)questionId {
    for (int ua=0; ua<_userAnswers.count; ua++) {
        if ([[_userAnswers[ua] objectForKey:@"questionId"] intValue]==questionId) {
            
            
            return _userAnswers[ua];
            
            //return userAnswersServer[ua]!=[NSNull null]?userAnswersServer[ua]:@"";
        }
    }
    
    return nil;
}

-(NSArray*)userAnswersForQuestionId:(NSInteger)questionId{ //for mutli answer questions
    NSMutableArray* retArray=[[NSMutableArray alloc] init];
    
    for (int ua=0; ua<_userAnswers.count; ua++) {
        if ([[_userAnswers[ua] objectForKey:@"questionId"] intValue]==questionId) {
            [retArray addObject:_userAnswers[ua]];
        }
    }
    
    return retArray;
}



#pragma mark - additional helper methods
-(void)setFirstAnswerForEveryQuestion {
    for(int ua=0; ua < _userAnswers.count ; ua++){
        
        NSDictionary* userAnswer = _userAnswers[ua];
        
        int userAnswerQuestionId = [[userAnswer objectForKey:@"questionId"] intValue];
        
        NSString* textAnswer;
        NSString* idAnswer;
        int defaultAnswerId = 0;
        
        
        if ([userAnswer objectForKey:@"textAnswer"]==[NSNull null]) {
            NSInteger qIForId=[self questionIndexForId:userAnswerQuestionId];
            NSDictionary* theQuestion = [_questions objectAtIndex:qIForId] ;
            NSArray* theQuestionAnswers = [theQuestion objectForKey:@"answers"];
            NSString* defAnswerStringKey = [theQuestionAnswers[defaultAnswerId] objectForKey:@"name"];
            textAnswer =  AOLocalizedString(defAnswerStringKey);
        }
        else {
            textAnswer = AOLocalizedString([userAnswer objectForKey:@"textAnswer"]);
        }
        
        
        if([userAnswer objectForKey:@"answerId"]==[NSNull null]){
            idAnswer =  [[[_questions objectAtIndex:[self questionIndexForId:userAnswerQuestionId]] objectForKey:@"answers"][defaultAnswerId] objectForKey:@"id"];
        }
        else {
            idAnswer = [userAnswer objectForKey:@"answerId"];
        }
        
        
        
        
        
        [userAnswer setValue:textAnswer forKey:@"textAnswer"];
        [userAnswer setValue:idAnswer forKey:@"answerId"];
    }

}

-(void)setUserAnswerId:(NSInteger)userAnswerIndex forQuestionId:(NSInteger)questionIndex {
    
    
    
    id selectedQuestion = [_questions objectAtIndex:questionIndex];
    NSString* idAnswer =  [[selectedQuestion objectForKey:@"answers"][userAnswerIndex] objectForKey:@"id"];
    
    NSDictionary *userAnswer = [self userAnswerForQuestionId:[[selectedQuestion objectForKey:@"id"] intValue]];
    
    [userAnswer setValue:idAnswer forKey:@"answerId"];
}



-(void)setUserAnswerWithUserAnswerId:(NSInteger)userAnswerId andTextAnswer:(NSString*)textAnswer {
    for (int ua=0; ua<_userAnswers.count; ua++) {
        if ([[_userAnswers[ua] objectForKey:@"userAnswerId"] intValue]==userAnswerId) {
            [_userAnswers[ua] setObject:textAnswer forKey:@"textAnswer"];
        }
    }
}

-(void)setUserMultipleAnswerWithUserAnswerId:(NSInteger)userAnswerId andTextAnswer:(NSString*)textAnswer andAnswerId:(NSNumber*)answerId{
    for (int ua=0; ua < _userAnswers.count; ua++) {
        if ([[_userAnswers[ua] objectForKey:@"userAnswerId"] intValue]==userAnswerId) {
            [_userAnswers[ua] setObject:textAnswer forKey:@"textAnswer"];
            [_userAnswers[ua] setObject:answerId forKey:@"answerId"];
        }
    }
}

@end
