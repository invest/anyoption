//
//  AoRegQuestionnaireCheckboxTableViewCell.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/17/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AoRegQuestionnaireCheckboxTableViewCell : UITableViewCell

@property (nonatomic, assign) BOOL checked;
@property (nonatomic, copy) NSString *questionText;
@property (nonatomic, copy) NSString *errorMessage;

@property (nonatomic, copy) void (^checkedChanged)(AoRegQuestionnaireCheckboxTableViewCell *cell);
@property (nonatomic, copy) void (^linkPressed)(AoRegQuestionnaireCheckboxTableViewCell *cell);

@end
