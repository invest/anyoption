//
//  AOPopupPickerTableViewCell.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/19/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AOPopupPickerTableViewCell : UITableViewCell

@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) CGFloat topBottomPadding;

+ (CGFloat) defaultTopBottomPadding;

@end
