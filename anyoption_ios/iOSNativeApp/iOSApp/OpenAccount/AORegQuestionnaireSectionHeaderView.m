//
//  AORegQuestionnaireSectionHeaderView.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/17/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AORegQuestionnaireSectionHeaderView.h"

@interface AORegQuestionnaireSectionHeaderView ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcSeparatorHeight;
@property (weak, nonatomic) IBOutlet UIView *vTopSep;
@property (weak, nonatomic) IBOutlet UIView *vBottomSep;

@end

@implementation AORegQuestionnaireSectionHeaderView

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    self.lcSeparatorHeight.constant = 1.0 / [UIScreen mainScreen].scale;
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    UIColor *sepColor = [theme regSectionSeparatorColor];
    self.vTopSep.backgroundColor = sepColor;
    self.vBottomSep.backgroundColor = sepColor;
    
    self.lblTitle.textColor = [theme regSectionTextColor];
    self.lblTitle.font = [[AOThemeController sharedInstance].theme sectionTextFont];
    
    self.backgroundView = ({
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [theme regSectionBackgroundColor];
        view;
    });
}

- (NSString *) text
{
    return self.lblTitle.text;
}

- (void) setText:(NSString *)text
{
    self.lblTitle.text = text;
}

@end
