//
//  AOProofTableViewCell.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 9/1/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AOProofTableViewCell.h"

#import "RadioBtn.h"

@interface AOProofTableViewCell ()

@property (weak, nonatomic) IBOutlet RadioBtn *btnRadio;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation AOProofTableViewCell

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self p_updateChecked];
    [self p_updateTitle];
}

GENERATE_SETTER(setChecked, BOOL, checked, p_updateChecked);
GENERATE_SETTER(setTitle, NSString *, title, p_updateTitle);

- (void) p_updateChecked
{
    self.btnRadio.selected = self.checked;
}

- (void) p_updateTitle
{
    self.lblTitle.text = self.title;
}

@end
