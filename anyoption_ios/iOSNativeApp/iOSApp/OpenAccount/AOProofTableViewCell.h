//
//  AOProofTableViewCell.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 9/1/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AOProofTableViewCell : UITableViewCell

@property (nonatomic, assign) BOOL checked;
@property (nonatomic, copy) NSString *title;

@end
