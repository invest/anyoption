//
//  AOProofOfIdPickerController.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 9/1/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AOProofOfIdPickerController.h"

#import "AOProofTableViewCell.h"

#ifdef COPYOP
#import "AOCopyOpTheme.h"
#import "UIFont+Roboto.h"
#import "UIView+Shadows.h"
#endif

#import "COPopupHeaderView.h"

@interface AOProofOfIdPickerController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcTableViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *btnUpload;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcDistTableBottomView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end

@implementation AOProofOfIdPickerController
{
    NSInteger _selIndex;
    
    UIImageView *_shadowImage;
    UIImageView *_shadowUploadButton;
    
    CGFloat _prevTableHeight;
}

static NSString * const kCellIdentifier = @"CellIdentifier";

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    _selIndex = 0;
    
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    [self.tableView reloadData];
    [self.tableView layoutIfNeeded];
    
    self.tableView.scrollEnabled = NO;
    
    self.lcTableViewHeight.constant = self.tableView.contentSize.height;
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
#ifdef COPYOP
    UIColor *color = [(AOCopyOpTheme *)theme popupViewBackgroundColor];
    self.bottomView.backgroundColor = color;
    self.tableView.backgroundColor = color;
    
    self.lcDistTableBottomView.constant = 8.0;
    
    [self.btnUpload setTitleColor:[UIColor colorWithRed:234.0/255.0 green:69.0/255.0 blue:22.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    self.btnUpload.titleLabel.font = [UIFont robotoFontRegularWithSize:20.0];
    
    self.lblTitle.backgroundColor = [UIColor clearColor];
    
    self.bottomView.backgroundColor = [UIColor clearColor];
    
    self.view.backgroundColor = [UIColor clearColor];
#else
    self.bottomView.backgroundColor = [theme appBackgroundColor];
    self.tableView.backgroundColor = [theme appBackgroundColor];
    self.lblTitle.superview.backgroundColor = [theme navigationBarBackgroundColor];
    
    self.view.layer.cornerRadius = 8.0;
    self.view.layer.masksToBounds = YES;
#endif

    COPopupHeaderView *header = (COPopupHeaderView *)self.lblTitle.superview;
    header.showsSeparatorLine = NO;
    
    [UIView performWithoutAnimation:^{
        [self.btnUpload setTitle:AOLocalizedString(@"docsUploadButtonTitle") forState:UIControlStateNormal];
    }];
    
    CGRect fr = self.view.frame;
    fr.size.height = [self.view systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    self.view.frame = fr;
    
#ifdef COPYOP
    [self p_updateShadow];
#endif
}

- (void) setItems:(NSArray *)items
{
    _items = items;
    
    if (![self isViewLoaded])
    {
        return;
    }
    
    [self.tableView reloadData];
    [self.tableView layoutIfNeeded];
}

#ifdef COPYOP
- (void) p_updateShadow
{
    [_shadowImage removeFromSuperview];
    
    UIView *headView = self.lblTitle.superview;
    CGRect r = CGRectMake(CGRectGetMinX(headView.frame),
                          CGRectGetMinY(headView.frame),
                          CGRectGetWidth(headView.bounds),
                          CGRectGetMaxY(self.tableView.frame));
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:r
                                               byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                                     cornerRadii:CGSizeMake(5.0, 5.0)];
    CGFloat shadowSize = 3.0;
    UIImage *img = [UIView shadowImageWithBezierPath:path shadowSize:shadowSize];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [self.view insertSubview:imgView atIndex:0];
    
    CGRect fr = imgView.frame;
    fr.origin = CGPointMake(CGRectGetMinX(headView.frame) - shadowSize, CGRectGetMinY(headView.frame) - shadowSize);
    imgView.frame = fr;
    _shadowImage = imgView;
    
    if (_shadowUploadButton == nil)
    {
        [_shadowUploadButton removeFromSuperview];
        UIBezierPath *cancelPath = [UIBezierPath bezierPathWithRoundedRect:self.bottomView.bounds
                                                         byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight
                                                               cornerRadii:CGSizeMake(5.0, 5.0)];
        UIImage *cancelShadowImage = [UIView shadowImageWithBezierPath:cancelPath shadowSize:shadowSize];
        UIImageView *cancelImgView = [[UIImageView alloc] initWithImage:cancelShadowImage];
        [self.view insertSubview:cancelImgView belowSubview:self.bottomView];
        
        [self.bottomView pinShadowImageView:cancelImgView shadowSize:shadowSize];
        
        _shadowUploadButton = cancelImgView;
    }
}
#endif

- (IBAction) btnUploadPressed:(id)sender
{
    if (self.selectionHandler != nil)
    {
        self.selectionHandler(_selIndex);
    }
}

#pragma mark - UITableView methods

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
#ifdef COPYOP
    if (CGRectGetHeight(self.tableView.bounds) != _prevTableHeight)
    {
        _prevTableHeight = CGRectGetHeight(self.tableView.bounds);
        
        [self p_updateShadow];
    }
#endif
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (NSString *) p_titleAtIndexPath:(NSIndexPath *)indexPath
{
    return self.items[indexPath.row];
}

- (void) p_configureCell:(AOProofTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.checked = (_selIndex == indexPath.row);
    cell.title = [self p_titleAtIndexPath:indexPath];
    
    if ([cell respondsToSelector:@selector(preservesSuperviewLayoutMargins)])
    {
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
    }
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AOProofTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    [self p_configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return FLT_MIN;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return FLT_MIN;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (_selIndex != indexPath.row)
    {
        AOProofTableViewCell *oldCell = (AOProofTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_selIndex inSection:0]];
        oldCell.checked = NO;
        
        _selIndex = indexPath.row;
        AOProofTableViewCell *newCell = (AOProofTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        newCell.checked = YES;
    }
}

@end
