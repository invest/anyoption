//
//  AORegQuestionnaireSectionHeaderView.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/17/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AORegQuestionnaireSectionHeaderView : UITableViewHeaderFooterView

@property (nonatomic, copy) NSString *text;

@end
