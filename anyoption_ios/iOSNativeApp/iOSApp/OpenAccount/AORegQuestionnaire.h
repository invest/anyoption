//
//  AORegQuestionnaire.h
//  iOSApp
//
//  Created by  on 8/14/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const NSInteger kProfessionQuesionId;
extern const NSInteger kProfessionAnswerOtherId;


@interface AORegQuestionnaire : NSObject



@property (nonatomic, strong) NSMutableArray *questions;
@property (nonatomic, strong) NSMutableArray *userAnswers;


-(id)initWithDictionary:(NSDictionary*)regQuestionnaireResponseDictionary;

-(NSArray*)answersForQuestionAt:(NSInteger)qIndex ;
-(NSInteger)questionIndexForId:(NSInteger)questionId;
-(NSDictionary *) userAnswerForQuestionId:(NSInteger)questionId;
-(NSArray*)userAnswersForQuestionId:(NSInteger)questionId;
-(void)setFirstAnswerForEveryQuestion ;
-(void)setUserAnswerId:(NSInteger)userAnswerIndex forQuestionId:(NSInteger)questionIndex;
-(void)setUserAnswerWithUserAnswerId:(NSInteger)userAnswerId andTextAnswer:(NSString*)textAnswer;
-(void)setUserMultipleAnswerWithUserAnswerId:(NSInteger)userAnswerId andTextAnswer:(NSString*)textAnswer andAnswerId:(NSNumber*)answerId;

@end
