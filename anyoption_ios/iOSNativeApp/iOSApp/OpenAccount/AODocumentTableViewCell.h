//
//  AODocumentTableViewCell.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/31/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AODocumentTableViewCell : UITableViewCell

@property (nonatomic, copy) NSString *titleText;
@property (nonatomic, copy) NSString *subtitleText;
@property (nonatomic, strong) UIColor *subtitleColor;
@property (nonatomic, copy) NSString *valueText;
@property (nonatomic, strong) UIImage *iconImage;
@property (nonatomic, assign) BOOL showsArrow;

@end
