//
//  AOPopupContainerViewController.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 9/1/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^AOPopupContainerControllerCloseHandler)();

@interface AOPopupContainerViewController : UIViewController

- (id) initWithContentViewController:(UIViewController *)viewController;

@property (nonatomic, copy) AOPopupContainerControllerCloseHandler didTapOutsideHandler;

- (void) showInParentViewController:(UIViewController *)parentViewController animated:(BOOL)animated;
- (void) hideAnimated:(BOOL)animated completion:(dispatch_block_t)completion;

@end
