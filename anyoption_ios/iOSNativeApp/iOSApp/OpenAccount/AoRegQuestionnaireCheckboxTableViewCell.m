//
//  AoRegQuestionnaireCheckboxTableViewCell.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/17/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AoRegQuestionnaireCheckboxTableViewCell.h"

#import "CheckBox.h"

@interface AoRegQuestionnaireCheckboxTableViewCell () <CheckBoxDelegate>

@property (weak, nonatomic) IBOutlet CheckBox *btnCheckbox;
@property (weak, nonatomic) IBOutlet UILabel *lblAssessment;
@property (weak, nonatomic) IBOutlet UILabel *lblErrorMessage;

@end

@implementation AoRegQuestionnaireCheckboxTableViewCell
{
    BOOL _hasLinks;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    self.lblAssessment.textColor = [theme textColor];
    self.lblAssessment.font = [self.lblAssessment.font fontWithSize:13.0];
    
    [self.lblAssessment addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(linkTapped:)]];
    
    self.lblErrorMessage.textColor = [UIColor redColor];
    self.lblErrorMessage.font = [self.lblErrorMessage.font fontWithSize:10.0];
    
    [self p_updateQuestionText];
    [self p_updateChecked];
    
    self.btnCheckbox.delegate = self;
}

- (void) linkTapped:(UITapGestureRecognizer *)tapGs
{
    if (_hasLinks)
    {
        if (self.linkPressed)
        {
            self.linkPressed(self);
        }
    }
}

GENERATE_SETTER(setChecked, BOOL, checked, p_updateChecked);
GENERATE_SETTER(setErrorMessage, NSString *, errorMessage, p_updateErrorMessage);

- (void) setQuestionText:(NSString *)questionText
{
    _questionText = [questionText copy];
    
    [self p_updateQuestionText];
}

- (void) p_updateErrorMessage
{
    self.lblErrorMessage.text = self.errorMessage;
}

- (void) p_updateQuestionText
{
    if ([_questionText length] == 0)
    {
        self.lblAssessment.attributedText = [[NSAttributedString alloc] init];
        return;
    }
    
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;

    NSArray *linkRanges = nil;
    NSString *str = [AOUtils processString:_questionText tag:@"l" ranges:&linkRanges];

    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:str
                                                                             attributes:@{NSForegroundColorAttributeName : [theme textColor],
                                                                                          NSFontAttributeName : [theme sectionTextFont]}];

    for (NSValue *range in linkRanges)
    {
        [attr addAttribute:NSUnderlineStyleAttributeName value:@1 range:[range rangeValue]];
    }
    
    self.lblAssessment.attributedText = attr;
    
    _hasLinks = [linkRanges count] > 0;
    self.lblAssessment.userInteractionEnabled = (_hasLinks > 0);
}

- (void) p_updateChecked
{
    self.btnCheckbox.value = self.checked;
}

- (UIView *) hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    CGRect checkboxArea = CGRectInset(self.btnCheckbox.frame, -15.0, -15.0);
    if (CGRectContainsPoint(checkboxArea, point))
    {
        return self.btnCheckbox;
    }
    else
    {
        return [super hitTest:point withEvent:event];
    }
}

#pragma mark - CheckBoxDelegate methods

- (void) checkBoxValueChanged:(CheckBox *)checkbox
{
    _checked = checkbox.value;
    
    if (self.checkedChanged)
    {
        self.checkedChanged(self);
    }
}

@end
