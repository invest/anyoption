//
//  cityPickerView.m
//  iOSApp
//
//  Created by Lyubomir Marinov on 01/02/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import "cityPickerView.h"

@interface cityPickerView()

@property (nonatomic, strong) NSMutableArray *pickerDataSourceArr;
@property (nonatomic, strong) NSMutableArray *foundResults;
@property (nonatomic, strong) NSString* plistFileLocation;
@property (nonatomic, strong) NSString* titleKey;
@property (nonatomic, strong) NSString* valueKey;
@property (nonatomic, strong) NSString *idKey;

@end

@implementation cityPickerView

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.searchBar.backgroundColor = [UIColor colorWithHexString:@"#ddeaf4"];
    self.searchBar.placeholder = AOLocalizedString(@"register.city");
    
    // UISearchBar borders:
    
//    CALayer *bottomBorder = [CALayer layer];
//    bottomBorder.backgroundColor = [UIColor colorWithHexString:@"#548aad"].CGColor;
//    bottomBorder.frame = CGRectMake(0, CGRectGetHeight(self.searchBar.frame) -2, CGRectGetWidth(self.searchBar.frame), 2);
//    [self.searchBar.layer addSublayer:bottomBorder];
//    
//    CALayer *leftBorder = [CALayer layer];
//    leftBorder.backgroundColor = [UIColor colorWithHexString:@"#548aad"].CGColor;
//    leftBorder.frame = CGRectMake(0, CGRectGetHeight(self.searchBar.frame) -8, 2, 8);
//    [self.searchBar.layer addSublayer:leftBorder];
//    
//    CALayer *rightBorder = [CALayer layer];
//    rightBorder.backgroundColor = [UIColor colorWithHexString:@"#548aad"].CGColor;
//    rightBorder.frame = CGRectMake(CGRectGetWidth(self.searchBar.frame) -2, CGRectGetHeight(self.searchBar.frame) -8, 2, 8);
//    [self.searchBar.layer addSublayer:rightBorder];
    
    
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        self.searchTable.delegate = self;
        self.searchTable.dataSource = self;
        
        self.pickerDataSourceArr = [NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"cities" ofType:@"plist"]];
        self.foundResults = [NSMutableArray arrayWithArray:self.pickerDataSourceArr];
        [self.searchTable reloadData];
        
        // configure data source
        
        self.titleKey = @"name";
        self.valueKey = @"name";
        self.idKey = @"id";
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - UISearchBar methods
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.foundResults removeAllObjects];

    if([searchText length] == 0) {
        [self.foundResults addObjectsFromArray:self.pickerDataSourceArr];
    } else {
        for(NSDictionary *dict in self.pickerDataSourceArr) {
            if([[dict[@"name"] lowercaseString] containsString:[searchText lowercaseString]]) {
                [self.foundResults addObject:dict];
            }
        }
        
    }
    
    [self.searchTable reloadData];
}

#pragma mark - UITableView methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.foundResults.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cityListCell" forIndexPath:indexPath];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cityListCell"];
    }
    
    UILabel *label = [cell.contentView viewWithTag:5];
    if(label != nil) {
        label.text = self.foundResults[indexPath.row][self.titleKey];
        label.textColor = [UIColor colorWithHexString:@"#004c76"];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    [self.delegate didSelectCityWithName:self.foundResults[indexPath.row][self.titleKey] andID:self.foundResults[indexPath.row][self.idKey]];
}

@end
