//
//  ETCityPickerViewController.m
//  iOSApp
//
//  Created by Lyubomir Marinov on 26/01/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import "ETCityPickerViewController.h"

@interface ETCityPickerViewController ()

//Picker's datasource
@property (nonatomic, strong) NSMutableArray *pickerDataSourceArr;
@property (nonatomic, strong) NSMutableArray *foundResults;
@property (nonatomic, strong) NSString* plistFileLocation;
@property (nonatomic, strong) NSString* titleKey;
@property (nonatomic, strong) NSString* valueKey;
@property (nonatomic, strong) NSString *idKey;

@end

@implementation ETCityPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchBar.backgroundColor = [UIColor clearColor];
    self.searchBar.backgroundImage = [UIImage new];
    
    // UISearchBar borders:
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.backgroundColor = [[UIColor greenColor] CGColor];
    bottomBorder.frame = CGRectMake(0, CGRectGetHeight(self.searchBar.frame) -2, CGRectGetWidth(self.searchBar.frame), 2);
    [self.searchBar.layer addSublayer:bottomBorder];
    
    CALayer *leftBorder = [CALayer layer];
    leftBorder.backgroundColor = [[UIColor greenColor] CGColor];
    leftBorder.frame = CGRectMake(0, CGRectGetHeight(self.searchBar.frame) -8, 2, 8);
    [self.searchBar.layer addSublayer:leftBorder];
    
    CALayer *rightBorder = [CALayer layer];
    rightBorder.backgroundColor = [[UIColor greenColor] CGColor];
    rightBorder.frame = CGRectMake(CGRectGetWidth(self.searchBar.frame) -2, CGRectGetHeight(self.searchBar.frame) -8, 2, 8);
    [self.searchBar.layer addSublayer:rightBorder];
    
    self.titleKey = @"name";
    self.valueKey = @"name";
    self.idKey = @"id";
    
    self.pickerDataSourceArr = [NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"cities" ofType:@"plist"]];
    self.foundResults = self.pickerDataSourceArr;
    [self.searchResults reloadData];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.background.image = self.backgroundImage;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    self.bottomSpacing.constant = keyboardSize.height;
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    self.bottomSpacing.constant = 0;
}

#pragma mark - UISearchBar methods

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSString *searchString = searchBar.text;
    
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"name CONTAINS[cd] %@",
                                    searchString
                                    ];
    
    NSArray *matches = [self.pickerDataSourceArr filteredArrayUsingPredicate:resultPredicate];

    [self.foundResults removeAllObjects];
    self.foundResults = [matches mutableCopy];
    
    [self.searchResults reloadData];
}

#pragma mark - UITableView methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 32.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.foundResults.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cityListCell" forIndexPath:indexPath];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cityListCell"];
    }
    
    UILabel *cityLabel = [cell.contentView viewWithTag:5];
    if(cityLabel) {
        cityLabel.text = self.foundResults[indexPath.row][@"name"];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.view endEditing:YES];
    [self.delegate didPickCity:indexPath.row fromPicker:self];
}

-(IBAction)dismissViewControllerWithoutSelection:(id)sender {
    [self.view endEditing:YES];
    [self.delegate didNotPickCityFromPicker:self];
}

@end
