//
//  ETCityPickerViewController.h
//  iOSApp
//
//  Created by Lyubomir Marinov on 26/01/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ETCityPickerDelegate;

@interface ETCityPickerViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *background;
@property (nonatomic, weak) IBOutlet UIView *container;
@property (nonatomic, weak) IBOutlet UIView *searchContainer;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UITableView *searchResults;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bottomSpacing;

@property (nonatomic, strong) UIImage *backgroundImage;

@property (nonatomic, assign) id <ETCityPickerDelegate> delegate;

@end

@protocol ETCityPickerDelegate <NSObject>

-(void)didPickCity:(NSInteger)cityId fromPicker:(ETCityPickerViewController *)pickerViewController;
-(void)didNotPickCityFromPicker:(ETCityPickerViewController *)pickerViewController;

@end