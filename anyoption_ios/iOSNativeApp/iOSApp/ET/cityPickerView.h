//
//  cityPickerView.h
//  iOSApp
//
//  Created by Lyubomir Marinov on 01/02/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ETCityPickerViewDelegate;

@interface cityPickerView : UIView <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, assign) id<ETCityPickerViewDelegate> delegate;

@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UITableView *searchTable;

@end

@protocol ETCityPickerViewDelegate <NSObject>

-(void)didSelectCityWithName:(NSString *)cityName andID:(NSNumber *)cityId;

@end
