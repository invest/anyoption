//
//  AOTradeViewController.h
//  iOSApp
//
//  Created by mac_user on 1/6/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOBaseViewController.h"

@class AOTradeAssetsViewController,AOTradeOptionsViewController,AOCenteredTextImage,AppDelegate;

@interface AOTradeViewController : AOBaseViewController {
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIButton *assetsTabButton;
    __weak IBOutlet UIButton *optionsTabButton;
    
    __weak IBOutlet AOCenteredTextImage *openOptionsImage;
    __weak IBOutlet UIView *tabSeparator;
}

@property (strong,nonatomic) AOTradeAssetsViewController *tradeAssetsViewController;
@property (strong,nonatomic) AOTradeOptionsViewController *tradeOptionsViewController;
@property BOOL isOnAssets;

-(void)refreshLocalizedData;
- (void) updateInsurances;

@end
