//
//  AOTradeViewController.m
//  iOSApp
//
//  Created by mac_user on 1/6/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOTradeViewController.h"
#import "AOTradeAssetsViewController.h"
#import "AOTradeOptionsViewController.h"
#import "UILabel+SSText.h"
#import "AOCenteredTextImage.h"
#import "NSTimer+Blocks.h"
#import "AORequestService.h"
#import "AOUserMethodRequest.h"
#import "AOOpenOptionsButton.h"

@interface AOTradeViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcOpenOptionsButtonHeight;

@end

@implementation AOTradeViewController
{
    BOOL _viewWillAppearCalled;
    id _openOptionsObserver;
}

-(void)dealloc{
    // NSLog(@"AOTradeViewController dealloc");
    
    [[NSNotificationCenter defaultCenter] removeObserver:_openOptionsObserver];
}

- (IBAction) bntOpenOptionsPressed:(id)sender
{
    if (!_tradeOptionsViewController) {  //maybe redundant. check later
        _tradeOptionsViewController = [[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:@"TradeOptions"];
    }
    //[MAINVC   :@"Trade" pageName:@"TradeOptions" root:NO options:nil];
    
    [RESIDENT goTo:@"myOptions" withOptions:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _isOnAssets = YES;
    }
    return self;
}

- (void) p_updateOpenOptionsButtonTitle
{
    long openOptions = [DLG user].openedInvestmentsCount;
    
    NSString *title = nil;
    if (openOptions)
    {
#if !COPYOP && !ETRADER
     NSString *arrows = @">>";
#else
     NSString *arrows = @"»";
#endif
        
#ifdef ETRADER
        if (openOptions == 1)
        {
            title = @"השקעה פתוחה אחת";
        }
#endif
        if (title == nil)
        {
            title = [NSString stringWithFormat:@"%ld %@ %@", openOptions, AOLocalizedString(@"investments.open"), arrows];
        }
    }
    else
    {
        title = @"";
    }
    
    self.lcOpenOptionsButtonHeight.constant = openOptions > 0 ? 30.0 : 0.0;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // NSLog(@"AOTradeViewController viewDidLoad");

    __typeof__(self) __weak myself = self;
    _openOptionsObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kUserOpenOptionsUpdatedNotification
                                                                             object:nil
                                                                              queue:nil
                                                                         usingBlock:^(NSNotification *note) {
                                                                             if (myself)
                                                                             {
                                                                                 __typeof__(myself) __strong sself = myself;
                                                                                 [sself p_updateOpenOptionsButtonTitle];
                                                                             }
                                                                         }];
    [self p_updateOpenOptionsButtonTitle];
    //[self buildMainMenuButton];
    
    [assetsTabButton setTitleColor:MA_FIRSTTAB_TEXTCOLOR forState:UIControlStateNormal];
    [assetsTabButton setTitleColor:MA_FIRSTTAB_TEXTCOLOR forState:UIControlStateHighlighted];
    [optionsTabButton setTitleColor:MA_FIRSTTAB_TEXTCOLOR forState:UIControlStateNormal];
    [optionsTabButton setTitleColor:MA_FIRSTTAB_TEXTCOLOR forState:UIControlStateHighlighted];
    
    assetsTabButton.titleLabel.font = [UIFont fontWithName:APP_FONT_MED size:12];

    optionsTabButton.titleLabel.font = [UIFont fontWithName:APP_FONT_MED size:12];
    
    
    
    
    
   

    //tabSeparator.backgroundColor = M_NAVBAR_SEPARATOR;
    //[AOUtils replaceConstraintWithAttribute:NSLayoutAttributeWidth withConstant:0.5 onView:tabSeparator];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!_viewWillAppearCalled)
    {
        if ([self.navOptions[@"showOpenOptionsTab"] boolValue])
        {
            _isOnAssets = NO;
        }
        
        _viewWillAppearCalled = YES;
       
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //Cleaning currently viewd asset and is not a fevaourite one
    for (NSString *key in [RESIDENT.userFavoriteAssets allKeys]) {
        if ([ [RESIDENT.userFavoriteAssets objectForKey:key] isEqualToString:@"temp"]) {
            [RESIDENT.userFavoriteAssets removeObjectForKey:key];
        }
    }
    
    if (IS_IPAD) {
        [self buildMainMenuButton];
    }
     [self buildLogoTitleView];
    // NSLog(@"AOTradeViewController viewDidAppear");
    
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController setNavigationBarHidden:NO];
    
//    [self pressedAssetsTab:nil];
    if (_isOnAssets) {
        [self showAssetsTab];
    } else {
        [self showOptionsTab];
    }
    
    [self refreshLocalizedData];
    
    [MAINVC switchMainMenu:YES];
#ifdef COPYOP
    [self buildLogo];
#else
    [self buildHelpButtonWithTitle:@"assetPageHelpTitle" andContent:@"assetPageHelpContent"];
#endif
    
    
    
    
}

-(void)refreshLocalizedData {
    /*
    NSString *backString = AOLocalizedString(@"mobile.appContact.back");
    
    UIButton *backButton = (UIButton*)[self.backBarButtonItem.customView viewWithTag:NAVIGATION_LEFT_ITEM];
    [backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setTitle:backString forState:UIControlStateNormal];
    [backButton setTitle:backString forState:UIControlStateHighlighted];
    */
    
    [self setOpenOptionsText:AOLocalizedString(@"investments.open")];
    
    NSString *assetsString = AOLocalizedString(@"menu.strip.assets");
    
    [assetsTabButton setTitle:assetsString forState:UIControlStateNormal];
    [assetsTabButton setTitle:assetsString forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showAssetsTab {
    if (!_tradeAssetsViewController) {
        _tradeAssetsViewController = [[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:@"TradeAssets"];
    }
    _tradeAssetsViewController.view.frame = containerView.bounds;
    _tradeAssetsViewController.view.autoresizingMask = 0xff;
    [self cleanupContainerView];
    [containerView addSubview:_tradeAssetsViewController.view];
    //[self addChildViewController:_tradeAssetsViewController];
   // [_tradeAssetsViewController didMoveToParentViewController:self];
    
    [self resetButtonColor];
    assetsTabButton.backgroundColor = MA_FIRSTTAB_SELECTEDCOLOR;
}

-(IBAction)pressedAssetsTab:(id)sender{
    _isOnAssets = YES;
    [self showAssetsTab];
}

- (void) showOptionsTab {
    if (!_tradeOptionsViewController) {
        _tradeOptionsViewController = [[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:@"TradeOptions"];
    }
    _tradeOptionsViewController.view.frame = containerView.bounds;
    
    [self cleanupContainerView];
    [containerView addSubview:_tradeOptionsViewController.view];
    [self addChildViewController:_tradeOptionsViewController];
    [_tradeOptionsViewController didMoveToParentViewController:self];
    
    [self resetButtonColor];
    optionsTabButton.backgroundColor = MA_FIRSTTAB_SELECTEDCOLOR;
}

-(IBAction)pressedOptionsTab:(id)sender{
    _isOnAssets = NO;
    [self showOptionsTab];
}
-(void)cleanupContainerView{
    if(containerView.subviews.count>0){
        [[containerView.subviews objectAtIndex:0] removeFromSuperview];
    }
}
-(void)resetButtonColor{
    assetsTabButton.backgroundColor = [UIColor clearColor];
    optionsTabButton.backgroundColor = [UIColor clearColor];
}

-(void)setOpenOptionsText:(NSString*)text{
    [optionsTabButton setTitle:text forState:UIControlStateNormal];
    [optionsTabButton setTitle:text forState:UIControlStateHighlighted];
    if(RESIDENT.hasOpenOptions) {
//        openOptionsImage.hidden = NO;
        if(IOS_VERSION<7){
            [optionsTabButton setContentEdgeInsets:UIEdgeInsetsMake(1,10,0,0)];
        }
        else {
            [optionsTabButton setContentEdgeInsets:UIEdgeInsetsMake(0,10,0,0)];
        }
        
        openOptionsImage.refLabel = optionsTabButton.titleLabel;
        [openOptionsImage updatePosition];
    }
    else {
        openOptionsImage.hidden = YES;
        if(IOS_VERSION<7){
            [optionsTabButton setContentEdgeInsets:UIEdgeInsetsMake(1,0,0,0)];
        }
        else {
            [optionsTabButton setContentEdgeInsets:UIEdgeInsetsMake(0,0,0,0)];
        }
    }
}

- (void) updateInsurances {
    // NSLog(@"AOTradeViewController updateInsurances");
    if (!_isOnAssets) {
        [_tradeOptionsViewController updateInsurances];
    }
}

@end