//
//  AOFlippedButton.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/12/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AOFlippedButton : UIButton

@property (nonatomic, assign) BOOL forceFlip;

@end
