//
//  AOFlippedButton.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/12/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOFlippedButton.h"

#import "UIButton+Flipped.h"

@implementation AOFlippedButton

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    [self ao_flipForced:self.forceFlip];
}

@end
