//
//  AOOpenOptionsButton.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 9/5/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOOpenOptionsButton.h"

@implementation AOOpenOptionsButton

- (void) layoutSubviews
{
    [super layoutSubviews];

    if (IS_RIGHT_TO_LEFT)
    {
        if (self.imageView.image)
        {
            CGRect imageFrame = self.imageView.frame;
            imageFrame.origin.x = CGRectGetWidth(self.bounds) - 12.0 - CGRectGetWidth(imageFrame);
            imageFrame.origin.y = (CGRectGetHeight(self.bounds) - CGRectGetHeight(imageFrame)) / 2.0;
            self.imageView.frame = imageFrame;
            
            CGRect titleFrame = self.titleLabel.frame;
            titleFrame.origin.x = CGRectGetMinX(imageFrame) - 5.0 - CGRectGetWidth(titleFrame);
            titleFrame.origin.y = (CGRectGetHeight(self.bounds) - CGRectGetHeight(titleFrame)) / 2.0;
            self.titleLabel.frame = titleFrame;
        }
    }
}

@end
