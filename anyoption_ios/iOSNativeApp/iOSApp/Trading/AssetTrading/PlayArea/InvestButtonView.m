//
//  InvestButtonView.m
//  circleTest
//
//  Created by  on 11/19/13.
//  Copyright (c) 2013 Anyoption. All rights reserved.
//


#import "InvestButtonView.h"
#import <QuartzCore/QuartzCore.h>

@implementation InvestButtonView{
    UIImageView* circleView;
    UIImageView* bgView;
    UIImageView* arrowView;
    UIImageView* overView;
    

    UIImage* loadingCircle;
    UIImage* successCircle;
    UIImage* failCircle;
    UIImage* overlayImage;
    
    UILabel* title;
    
    BOOL isFading;
    
    
    NSTimer *fadingTimer;
    
    IBOutlet __weak InvestButtonView* otherButton;

}

@synthesize state=_state;



-(id)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if (self)
        [self setupButton];
    
    return self;
}


-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self)
        [self setupButton];
    
    return self;
}

#pragma mark -
#pragma mark Button Methods

- (void) setupButton {
    
    
    self.backgroundColor = [UIColor clearColor];
   
    
    bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"InvestBtnBg"]];
    bgView.frame = CGRectMake(0.0, 0.0, 102 , 102);
    [self addSubview:bgView];
    
  
    loadingCircle = [UIImage imageNamed:@"fillingCircle"];
    successCircle=[UIImage imageNamed:@"wholeCircle"];
    failCircle=[UIImage imageNamed:@"failCircle"];
    
   
    
    title = [[UILabel alloc] initWithFrame:CGRectMake(0.0, self.frame.size.height/2 - JOYSTICK_TITLE_FONT_SIZE/2, self.frame.size.width, JOYSTICK_TITLE_FONT_SIZE+2)];
    title.font = [UIFont fontWithName:JOYSTICK_TITLE_FONT_NAME size:JOYSTICK_TITLE_FONT_SIZE];
    title.textColor = JOYSTICK_TITLE_FONT_COLOR;
    title.textAlignment = NSTextAlignmentCenter;
    [self addSubview:title];
    
    circleView = [[UIImageView alloc] initWithFrame:CGRectMake(2.5, 2.5, 97  , 97)];
    [self addSubview:circleView];
    [self bringSubviewToFront:circleView];
    
    
    overlayImage = [UIImage imageNamed:@"Circle-active"];
    overView = [[UIImageView alloc] initWithImage:overlayImage];
    overView.frame = CGRectMake(5.5,6.0,overlayImage.size.width,overlayImage.size.height);
    overView.hidden = YES;
    [self addSubview:overView];
    
    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnPressed:)];
    [self addGestureRecognizer:tapper];
    
    [self setState:AOInvestButtonStateDefault];
}

- (void) btnPressed: (UITapGestureRecognizer *)sender {
  
    if (self.state==AOInvestButtonStateDefault){
        if ([self.delegate investButtonHolded:self])
        {
            [self setState:AOInvestButtonStateLoading];
        }
    }
}

- (AOInvestButtonState)state {
    return _state;
}

- (void) setState:(AOInvestButtonState)st{
    
    //reseting
    
    _state = st;
    title.text = @"";
    
    self.alpha = 1;
    self.userInteractionEnabled = YES;
    
    if(isFading) [self stopFadingTimer];
    
    switch (_state) {
        case AOInvestButtonStateLoading:
            [circleView setImage:loadingCircle];
            if(otherButton!=nil) [otherButton setState:AOInvestButtonStateInactive];
            [self startLoadingAnimation];
            break;
        case AOInvestButtonStateInactive:
            self.alpha = 0.6;
            self.userInteractionEnabled = NO;
            break;
        case AOInvestButtonStateSuccess:
            [self stopLoadingAnimation];
            [self startFadingTimer];
            [circleView setImage:successCircle];

            break;
        case AOInvestButtonStateFail:
            [self stopLoadingAnimation];
            [self startFadingTimer];
            [circleView setImage:failCircle];
            
            break;
        default:
            [circleView setImage:nil];
            break;
    }
}

#pragma mark -
#pragma mark  Timers methods


-(void)startFadingTimer {
   
    if(fadingTimer==nil)
        fadingTimer = [NSTimer scheduledTimerWithTimeInterval:0.010 target:self selector:@selector(fadingTimerTick) userInfo:nil repeats:YES];
    
    isFading=YES;
}

- (void) fadingTimerTick
{
    circleView.alpha -= 0.01;
    if(circleView.alpha < 0.3){
        [self stopFadingTimer];
        if(otherButton!=nil) [otherButton setState:AOInvestButtonStateDefault];
    }
}


- (void) stopFadingTimer {
    
    [fadingTimer invalidate];
    fadingTimer = nil;
    isFading=NO;
    [self setState:AOInvestButtonStateDefault];
    circleView.alpha = 1.0;
   
}

#pragma mark -
#pragma mark Drawing , Layout , Rotations

- (void) drawRect: (CGRect) rect
{

}

- (void) layoutSubviews {
    if(arrowView==nil){
        UIImage *arrowImage = [UIImage imageNamed:self.isCallBtn?@"InvestBtnArrowUp":@"InvestBtnArrowDown"];
        arrowView = [[UIImageView alloc] initWithImage:arrowImage];
    }
#warning strange vertical fix, check later
    float vertFix = self.isCallBtn ? 6:14;
    arrowView.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 + vertFix);
    [self addSubview:arrowView];
}

-(void)startLoadingAnimation {
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0];
    rotationAnimation.duration = 1.0;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 999;
    [circleView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    overView.hidden=NO;
    CABasicAnimation* blinkingAnimation;
    blinkingAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    blinkingAnimation.fromValue = [NSNumber numberWithFloat:1.0f];
    blinkingAnimation.toValue = [NSNumber numberWithFloat:0.0f];
    blinkingAnimation.additive = NO;
    blinkingAnimation.autoreverses = YES;
    blinkingAnimation.duration = 1.0;
    blinkingAnimation.fillMode = kCAFillModeBoth;
    blinkingAnimation.repeatCount = 999;
    [overView.layer addAnimation:blinkingAnimation forKey:@"blinkingAnimation"];
    
    
    CABasicAnimation* shrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    shrink.fromValue = [NSNumber numberWithDouble:1];
    shrink.toValue = [NSNumber numberWithDouble:0.7];
    shrink.duration = 0.5;
    shrink.fillMode = kCAFillModeForwards;
    shrink.removedOnCompletion = NO;
    [arrowView.layer addAnimation:shrink forKey:@"shrinkAnimation"];
    
    
}

-(void)stopLoadingAnimation {
    [circleView.layer removeAllAnimations];
    [overView.layer removeAllAnimations];
    overView.hidden=YES;
    
    CABasicAnimation* unShrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    unShrink.fromValue = [NSNumber numberWithDouble:0.7];
    unShrink.toValue = [NSNumber numberWithDouble:1];
    unShrink.duration = 0.5;
    unShrink.fillMode = kCAFillModeForwards;
    unShrink.removedOnCompletion = NO;
    [arrowView.layer addAnimation:unShrink forKey:@"unshrinkAnimation"];
    
}

-(void)setOptionType:(AOOpportunityType)oType{
    
    _optionType = oType;
    
    switch (oType) {
        case AOOpportunityTypeOptionPlus:
            [bgView setImage:[UIImage imageNamed:@"InvestBtnBgOptionPlus"]];
            [overView setImage:[UIImage imageNamed:@"Circle-active-OptionPlus"] ];
            break;
            
        default:
            break;
    }
    
}


@end
