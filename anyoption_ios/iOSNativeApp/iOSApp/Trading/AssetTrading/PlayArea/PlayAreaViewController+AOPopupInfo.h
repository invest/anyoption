//
//  PlayAreaViewController+AOPopupInfo.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 7/25/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "PlayAreaViewController.h"

@interface PlayAreaViewController (AOPopupInfo)

- (UIView *) vCall;
- (UIView *) vPut;
- (UIView *) vSlideHandle;
- (UIView *) vInvestRC;

@end
