//
//  PlayAreaViewController.h
//  iOSApp
//
//  Created by  on 2/12/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "InvestButtonView.h"
#import "AOPullableView.h"
#import "AOTrendView.h"
#import "InvestmentAmountsOverlayViewController.h"
#import "ReturnsOverlayViewController.h"
#import "Return.h"
#import "AOMarket.h"

@class AOInvestment;

@protocol PlayAreaDelegate <NSObject>

-(void)investmentReady:(NSDictionary*)invDict;
-(void)openOptionTapped:(AOInvestment*)inv;

@end

@interface PlayAreaViewController : UIViewController <UITableViewDataSource,InvestButtonDelegate>{
    NSMutableArray *dataSource;
    AppDelegate *appDelegate;
    __weak IBOutlet UIView *profitInfoContainer;
    __weak IBOutlet UIView *waitingForExpContainer;
    __weak IBOutlet UILabel *waitingForExpLabel;
    __weak IBOutlet UIActivityIndicatorView *waitingForExpActivityIndicator;
    __weak IBOutlet UIView *disabler;
    __weak IBOutlet UIButton *profitBtn;
    __weak IBOutlet UILabel *commissionLabel;
}



// I'll think about for a better solution later
@property (nonatomic, assign) int selectedProfitSelector;
@property (nonatomic, strong) NSArray *profitSelectors;

@property (nonatomic) float currentLevel;
@property (nonatomic, assign) AOOpportunityType optionType;
@property (nonatomic, strong) InvestmentAmountsOverlayViewController *amountsOverlay;
@property (nonatomic, strong) ReturnsOverlayViewController *returnsOverlay;
@property (nonatomic, strong, setter = setReturnValues:) NSArray* returnValues;
@property (nonatomic, weak) AOMarket* market;
@property (weak, nonatomic) id delegate;

@property AOInvestment *futureInvestment;
@property AOInvestment *pastInvestment;

- (void) setSettledOptions: (NSArray*) sOpts;
- (void) removeSettledOptions;
- (NSArray*) getOpenOptions;
- (void) setOpenOptions: (NSArray*) oOpts;
- (void) removeOpenOptions;
- (void) investmentResult: (BOOL) success;
- (void) addOpenOption: (AOInvestment*) investment;
- (void) setDefaultReturnWith:(NSString*)winInd andLoseIndex:(NSString*)loseInd ;
- (void) refreshLocalizedData ;
- (void) isWaiting:(BOOL)isWaiting withMessage:(NSString*)message;
- (void) openClosePullableView;
- (void) showHidePullableViewHandle:(BOOL)show;
- (void) showReceipt ;
- (void) setupCommissionLabel:(long)commission;

@end
