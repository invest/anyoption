//
//  InvestButtonView.h
//  circleTest
//
//  Created by  on 11/19/13.
//  Copyright (c) 2013 Anyoption. All rights reserved.
//
#import "AOOpportunity.h"

typedef enum {
    AOInvestButtonStateDefault,
    AOInvestButtonStateInactive,
    AOInvestButtonStateLoading,
    AOInvestButtonStateSuccess,
    AOInvestButtonStateFail
} AOInvestButtonState;

@protocol InvestButtonDelegate <NSObject>

-(BOOL) investButtonHolded:(id)sender;

@end

@interface InvestButtonView : UIView <UIGestureRecognizerDelegate>

@property(nonatomic, assign)  BOOL enabled;
@property(nonatomic)  BOOL isCallBtn;
@property (nonatomic, assign) AOOpportunityType optionType;
@property (nonatomic,weak) id delegate;
@property(nonatomic, assign) AOInvestButtonState state;





@end
