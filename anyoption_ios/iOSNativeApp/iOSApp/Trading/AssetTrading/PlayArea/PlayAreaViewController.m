
//
//  PlayAreaViewController.m
//  iOSApp
//
//  Created by  on 2/12/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "PlayAreaViewController.h"
#import "AOCurrencyLabel.h"
#import "AORoundCornerView.h"
#import "AOInvestment.h"
#import "Return.h"
#import "UILabel+SSText.h"
#import "NSString+Utils.h"
#import "InvestButtonView.h"
#import "UIView+Disabler.h"
#import "AOTradeOptionPlusQuoteViewController.h"
#import "GetQuoteButton.h"
#import "ReceiptViewController.h"
#import "AOMethodRequest.h"
#import "AORequestService.h"
#import "AOSlideView.h"
#import "NSString+Utils.h"
#import "UIImage+Utils.h"
#import "UIFont+Roboto.h"
#ifdef COPYOP
#import "COInvestmentAmountViewController.h"
#import "COReturnPercentViewController.h"
#import "COModalPopupViewController.h"
#import "RestAPIFacade.h"
#import "COCopyUserController.h"
#endif

#ifdef COPYOP
@interface PlayAreaViewController () <UIAlertViewDelegate, COInvestmentAmountViewControllerDelegate, COReturnPercentViewControllerDelegate>
#else
@interface PlayAreaViewController () <UIAlertViewDelegate>
#endif

#ifdef COPYOP
@property (nonatomic, strong) COModalPopupViewController *popupCont;
#endif

@end

@implementation PlayAreaViewController{
   
    __weak IBOutlet UIView *playAreaView;
    __weak IBOutlet InvestButtonView *callBtn;
    __weak IBOutlet InvestButtonView *putBtn;
    __weak IBOutlet UILabel *assetProfitWinTitleLabel;
    __weak IBOutlet AOCurrencyLabel *assetProfitWinInfoLabel;
    __weak IBOutlet UILabel *assetProfitLoseInfoLabel;
    __weak IBOutlet AORoundCornerView *investRCView;
    __weak IBOutlet AORoundCornerView *profitRCView;
    __weak IBOutlet UILabel *investTitleLabel;
    __weak IBOutlet AOCurrencyLabel *investAmountLabel;
    __weak IBOutlet UILabel *profitTitleLabel;
    __weak IBOutlet UILabel *profitPercentLabel;
    __weak IBOutlet AOSlideView *pullableView;
    __weak IBOutlet UITableView *pullableTable;
    __weak InvestButtonView *pressedInvestBtn;
    
    NSString* lastInvAmount;
    BOOL isWaitingForExpiry;
}

static const NSInteger kInvestNotLoggedInAlertTag = 11;

@synthesize returnValues=_returnValues;

-(void)dealloc {
    // NSLog(@"PlayAreaViewController dealloc");
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAOResidentStartedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUserLoggedInNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUserLoggedOutNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPad) {
        
        
    }
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
	callBtn.delegate = self;
    putBtn.delegate  = self;
    
    investRCView.fillColor = profitRCView.fillColor = AT_RCVIEW_FILLCOLOR;
    investRCView.strokeColor = profitRCView.strokeColor = [theme investProfitStrokeColor];
    investRCView.corners = UIRectCornerTopLeft|UIRectCornerBottomLeft;
    profitRCView.corners = UIRectCornerTopRight|UIRectCornerBottomRight;
    if (IS_IPAD) {
        investRCView.corners = UIRectCornerAllCorners;
        profitRCView.corners = UIRectCornerAllCorners;
    }
    
  
    investRCView.lineWidth = profitRCView.lineWidth = 1;
    investRCView.radius = profitRCView.radius = 6;
    
    waitingForExpLabel.textColor = M_TEXTCOLOR;
    
    assetProfitWinInfoLabel.textColor = [theme tradeInvestIfCorrectTextColor];
    assetProfitWinInfoLabel.font = [theme tradeInvestWinAmountFont];
    
    for (UILabel *investCaptionLabel in @[investTitleLabel, profitTitleLabel])
    {
        investCaptionLabel.font = [theme tradeInvestCaptionLabelsFont];
        investCaptionLabel.textColor = [theme tradeInvestLabelsTextColor];
    }
    
    for (UILabel *investValueLabel in @[investAmountLabel, profitPercentLabel])
    {
        investValueLabel.textColor = [theme tradeInvestLabelsTextColor];
    }
    
    for (UILabel *profitLabel in @[assetProfitWinTitleLabel, assetProfitLoseInfoLabel])
    {
        profitLabel.font = [theme tradeInvestIfCorrectFont];
        profitLabel.textColor = [theme tradeInvestLabelsTextColor];
    }
    
    investAmountLabel.font = [theme tradeInvestInvestLabelFont];
    profitPercentLabel.font = [theme tradeInvestProfitLabelFont];
    
    [assetProfitWinTitleLabel setLineHeight:12];
    [assetProfitLoseInfoLabel setLineHeight:12];
    //[profitPercentLabel ios6LabelFix];
    
    [self setupPlayArea];
    
    [[[self.view subviews] objectAtIndex:0] setBackgroundColor:AT_JOYSTICK_BACKGROUND];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(residentStarted:) name:kAOResidentStartedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedIn:) name:kUserLoggedInNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedOut:) name:kUserLoggedOutNotification object:nil];
    
    callBtn.optionType = self.optionType;
    putBtn.optionType = self.optionType;
    
    callBtn.state = putBtn.state = AOInvestButtonStateInactive;
    
    profitBtn.enabled = NO;
#if COPYOP
    pullableTable.backgroundColor = [UIColor whiteColor];
#else
    pullableTable.backgroundColor = [theme pullableTableBackgroundColor];
#endif
//    pullableTable.backgroundColor = [theme pullableTableBackgroundColor];
    
#ifdef COPYOP
    CAGradientLayer *gradientBackground = [CAGradientLayer layer];
    gradientBackground.frame = pullableView.bounds;
    gradientBackground.colors = [NSArray arrayWithObjects:(id)[UIColor colorWithHexString:@"#b2b6ba"].CGColor, (id)[UIColor colorWithHexString:@"#f2f2f2"].CGColor, nil];
    [playAreaView.layer insertSublayer:gradientBackground atIndex:0];
#endif
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self refreshLocalizedData];
}

- (void)setupPlayArea {
    dataSource = [[NSMutableArray alloc] init];
    
    NSMutableArray *openArray = [[NSMutableArray alloc] init];
    NSMutableArray *settledArray = [[NSMutableArray alloc] init];
    
    [dataSource addObject:openArray];
    [dataSource addObject:settledArray];
    
    AOUser *user = [DLG user];
    
    if (IS_LOGGED) {
        lastInvAmount = [NSString stringWithFormat:@"%@", [AOUtils formatCurrencyAmountWithoutSymbol:[AOUtils divideCurrencyAmount:user.defaultAmountValue]]];
    }
    
    if ([self p_shouldCallLogoutBalanceStep]) { // if the AOResident has started
        [self p_getLogoutBalanceStep];
    }
    
    [investAmountLabel setText:lastInvAmount];
    
    _futureInvestment = [[AOInvestment alloc] init];
    _futureInvestment.amount = [lastInvAmount doubleValue];
    
}

- (BOOL) p_shouldCallLogoutBalanceStep
{
    if (!IS_LOGGED && [DLG user].skinId != 0)
    {
        return YES;
    }
    
//    if (IS_LOGGED && [DLG user].skinId != 0 && [[DLG user].predefValues count] == 0)
//    {
//        return YES;
//    }
    
    return NO;
}

- (void) p_getLogoutBalanceStep
{
    AOMethodRequest* request = [[AOMethodRequest alloc] init];
    request.serviceMethod = @"getLogoutBalanceStep";
    
    AOUser *user = [DLG user];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary *resultDictionary) {
        double defaultAmount = [[resultDictionary valueForKey:@"defaultAmountValue"] doubleValue];
        lastInvAmount = [NSString stringWithFormat:@"%@", [AOUtils formatCurrencyAmountWithoutSymbol:[AOUtils divideCurrencyAmount:defaultAmount]]];
        
        user.predefValues = [resultDictionary valueForKey:@"predefValues"];
        
        _futureInvestment.amount = [[AOUtils formatCurrencyAmountWithoutSymbol:defaultAmount / 100.0] doubleValue];
        
        [defaults setDouble:defaultAmount forKey:@"defaultAmountValue"];
    } failed:nil];
}

- (void) residentStarted:(NSNotification *)notification
{
    if ([self p_shouldCallLogoutBalanceStep])
    {
        [self p_getLogoutBalanceStep];
    }
}

- (void) userLoggedIn:(NSNotification *)notification {
    [self setupPlayArea];
    [self refreshLocalizedData];
}

- (void) userLoggedOut:(NSNotification *)notification {
    [self setupPlayArea];
    [pullableTable reloadData];
//    [self refreshLocalizedData];
    NSLog(@"What happens next is anybody's choise, but I don't believe, that I hear your voice. \n\n %@", [dataSource objectAtIndex:0]);
}

-(void)viewWillDisappear:(BOOL)animated{
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refreshLocalizedData {
    investTitleLabel.text = AOLocalizedString(@"CMS.tradeBox.text.invest");
    profitTitleLabel.text = AOLocalizedString(@"binary0100.rightTable.profit");
    
    assetProfitWinTitleLabel.text = AOLocalizedString(@"CMS.tradeBox.text.if_correct_earn");
    
    [self setProfitLabels];
    
    [investAmountLabel setText:lastInvAmount];
}

-(void)setDefaultReturnWith:(NSString*)winInd andLoseIndex:(NSString*)loseInd {
    
    if(!_futureInvestment.oddsWin || !_futureInvestment.oddsLose) {
        
        if (winInd!=nil ){
            _futureInvestment.oddsWin = [winInd doubleValue]-1;
            
            if ([[profitPercentLabel.text trimWhitespaces] isEqualToString:@""])
                profitPercentLabel.text = [NSString stringWithFormat:@"%.0f%%",([winInd doubleValue]-1)*100];
        }
        
        if (loseInd!=nil )
            _futureInvestment.oddsLose = 1 - [loseInd doubleValue];
        
        NSLog(@"oddsWin: %@ ------ oddsLose: %@", winInd, loseInd);
        
        if (_futureInvestment.oddsWin && _futureInvestment.oddsLose && _futureInvestment.amount)
            [self setProfitLabels];
    }
    
//    if(!_futureInvestment.oddsWin || !_futureInvestment.oddsLose) {
//        
//        if (winInd!=nil ){
//            _futureInvestment.oddsWin = [winInd doubleValue];
//            
//            if ([[profitPercentLabel.text trimWhitespaces] isEqualToString:@""])
//                profitPercentLabel.text = [NSString stringWithFormat:@"%.0f%%",[winInd doubleValue]];
//        }
//        
//        if (loseInd!=nil )
//            _futureInvestment.oddsLose = [loseInd doubleValue];
//        
////        NSLog(@"oddsWin: %@ ------ oddsLose: %f", _futureInvestment, _futureInvestment.amount);
//        
//        if (_futureInvestment.oddsWin && _futureInvestment.oddsLose && _futureInvestment.amount)
//            [self setProfitLabels];
//    }
}


-(void)setReturnValues:(NSArray *)returnValues{
    
    NSMutableArray* retArr = [NSMutableArray new];
    for (int i=0; i<returnValues.count; i++) {
        Return *cRet = [Return new];
        cRet.winReturn = [NSString stringWithFormat:@"%@%%",[(NSDictionary*)returnValues[i] objectForKey:@"win" ]];
        cRet.loseReturn = [NSString stringWithFormat:@"%@%%",[(NSDictionary*)returnValues[i] objectForKey:@"lose" ]];
        
        cRet.winIndex = [[(NSDictionary*)returnValues[i] objectForKey:@"win" ] floatValue]/100 ;
        cRet.loseIndex =  1 - [[(NSDictionary*)returnValues[i] objectForKey:@"lose" ] floatValue]/100 ;
        [retArr addObject:cRet];
    }
    
    _returnValues = retArr;
    
    profitBtn.enabled = YES;
}

#pragma mark -
#pragma mark Investment Amount Button actions

- (IBAction)buttonTouchUpInside:(UIButton*)button{
    [self buttonCancel:button];
    if(button.tag==1){
#if !COPYOP
        AOUser *user = [DLG user];
        
        if (self.amountsOverlay==nil) {
            self.amountsOverlay = [[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:@"InvestmentAmountsOverlay"];
            self.amountsOverlay.delegate = self;
            [self.amountsOverlay setupWithAmounts:[AOUtils sortPredefValues:user.predefValues] andDefaultValue:user.defaultAmountValue];
        }
        
        [self presentViewController:self.amountsOverlay animated:NO completion:nil];
#else
        COInvestmentAmountViewController *popup = [[UIStoryboard storyboardWithName:@"CopyOP" bundle:nil] instantiateViewControllerWithIdentifier:@"investmentsAmountOverlay"];
        COModalPopupViewController *popupContainer = [[COModalPopupViewController alloc] initWithContentViewController:popup];
        popupContainer.modalPopupY = 40;
        popupContainer.closeButtonPressedHandler = ^
        {
            [self.popupCont dismissAnimated:YES completion:nil];
            self.popupCont = nil;
        };
        [popup setupWithSelectedAmount:_futureInvestment.amount];
        popup.delegate = self;
        self.popupCont = popupContainer;
        [popupContainer presentInViewController:self animated:YES completion:nil];
#endif
        
    }
    else if(button.tag==2 && self.optionType != AOOpportunityTypeOptionPlus) {
#if !COPYOP
        if (!self.returnsOverlay) {
            self.returnsOverlay = [[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:@"ReturnsOverlay"];
        }
        
        if (_returnValues) {
            [self.returnsOverlay setupWithReturns:self.returnValues andDefIndex:_futureInvestment.oddsWin];
//            [self.returnsOverlay setupWithReturns:self.returnValues andDefIndex:_futureInvestment.oddsWin andSelectedSelector:self.selectedProfitSelector];
            self.returnsOverlay.delegate = self;
            
            [self presentViewController:self.returnsOverlay animated:NO completion:nil];
        }
#else
        COReturnPercentViewController *popup = [[UIStoryboard storyboardWithName:@"CopyOP" bundle:nil] instantiateViewControllerWithIdentifier:@"returnPercentPopup"];
        COModalPopupViewController *popupContainer = [[COModalPopupViewController alloc] initWithContentViewController:popup];
        popupContainer.modalPopupY = 80;
        popupContainer.closeButtonPressedHandler = ^
        {
            [self.popupCont dismissAnimated:YES completion:nil];
            self.popupCont = nil;
        };
        self.popupCont = popupContainer;
        popup.delegate = self;
        [popup setupWithReturns:self.returnValues andDefaultPercent:_futureInvestment.oddsWin];
        [popupContainer presentInViewController:self animated:YES completion:nil];
        
#endif
    }
}

- (IBAction)buttonTouchDown:(UIButton*)button {
   
    if(button.tag==1){
        investRCView.fillColor = AT_RCVIEW_FILLHOVERCOLOR;
        [investRCView setNeedsDisplay];
    }
    else if(button.tag==2 && self.optionType != AOOpportunityTypeOptionPlus) {
        profitRCView.fillColor = AT_RCVIEW_FILLHOVERCOLOR;
        [profitRCView setNeedsDisplay];
    }
}

- (IBAction)buttonCancel:(UIButton*)button {
    if(button.tag==1){
        investRCView.fillColor = AT_RCVIEW_FILLCOLOR;
        [investRCView setNeedsDisplay];
    }
    else if(button.tag==2){
        profitRCView.fillColor = AT_RCVIEW_FILLCOLOR;
        [profitRCView setNeedsDisplay];
    }
}

#pragma mark -

-(void) investmentResult:(BOOL)success {
    
    if(pressedInvestBtn!=nil)
        [pressedInvestBtn setState:success?AOInvestButtonStateSuccess:AOInvestButtonStateFail];
}

-(void)isWaiting:(BOOL)isWaiting withMessage:(NSString*)message{
    isWaitingForExpiry = isWaiting;
    if(isWaiting) {
        profitInfoContainer.hidden = YES;
        waitingForExpContainer.hidden = NO;
        [waitingForExpActivityIndicator startAnimating];
        disabler.hidden = NO;
        callBtn.state = putBtn.state = AOInvestButtonStateInactive;
        self.view.alpha = 0.7;
        [pullableTable reloadData];
    }
    else {
        profitInfoContainer.hidden = NO;
        waitingForExpContainer.hidden = YES;
        [waitingForExpActivityIndicator stopAnimating];
        [self.view removeDisablerWithTag:8315796];
        disabler.hidden = YES;
        callBtn.state = putBtn.state = AOInvestButtonStateDefault;
        self.view.alpha = 1;
    }
    
    waitingForExpLabel.text = message;
}

#pragma mark -
#pragma mark InvestButtonDelegate methods

-(BOOL)investButtonHolded:(InvestButtonView*)btn {
    
    if (IS_LOGGED) {
        pressedInvestBtn = btn;
        if([DLG user].balance > 0) {
            /* AR - 1511 */
//            if([DLG user].hasBonus == 0){
//                
//                [[MAINVC popupVC] showPopup:AOClaimBonus withData:nil animated:YES completion:nil];
//                
//            }

            
            if([DLG userRegulation].thresholdBlock > 0) {
                [[MAINVC popupVC] showPopup:AOCysecRestricted withData:nil animated:YES completion:^{
//                    [self.delegate investmentReady:[NSDictionary dictionaryWithObjectsAndKeys:_futureInvestment,@"investment",[NSNumber numberWithInt:btn.isCallBtn],@"isCall", nil]];
                    btn.state = AOInvestButtonStateDefault;
                }];
                return NO;
            } else {
                [self.delegate investmentReady:[NSDictionary dictionaryWithObjectsAndKeys:_futureInvestment,@"investment",[NSNumber numberWithInt:btn.isCallBtn],@"isCall", nil]];
            }
            
        } else {
            NSLog(@"no money, no woman. %ld", [DLG user].balance);
            [MAINVC loadPage:@"Account" pageName:@"depositMenu" root:YES options:nil];
            RESIDENT.mainMenuSelectedIndex = MainMenuIndexMyAccount;
        }
        return YES;
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:AOLocalizedString(@"error.investment.notLogin1")
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:AOLocalizedString(@"button.cancel")
                                              otherButtonTitles:AOLocalizedString(@"menuStrip.openaccount"), AOLocalizedString(@"header.login"), nil];
        alert.tag = kInvestNotLoggedInAlertTag;
        [alert show];
        return NO;
    }
}

#pragma mark - UIAlertViewDelegate methods

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kInvestNotLoggedInAlertTag)
    {
        if (buttonIndex != alertView.cancelButtonIndex)
        {
            if (buttonIndex == alertView.firstOtherButtonIndex) // open account
            {
                [RESIDENT goTo:@"account" withOptions:nil];
            }
            else
            {
                [RESIDENT goTo:@"login" withOptions:nil];
            }
        }
    }
}

#pragma mark -
#pragma mark table delegate


- (void)setupCell:(UITableViewCell*)cell forIndexPath:(NSIndexPath*)indexPath {
    
    if(!cell) {
        if(!indexPath) return;
        cell = [pullableTable cellForRowAtIndexPath:indexPath];
    }
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    cell.contentView.backgroundColor = [theme openSettledOptionsCellBackgroundColor];
    
    UIButton  *cell1 = (UIButton*)[cell viewWithTag:1];
    cell1.userInteractionEnabled=NO;
    UIButton  *cell2 = (UIButton*)[cell viewWithTag:2];
    cell2.userInteractionEnabled=NO;
    UILabel   *cell3 = (UILabel*)[cell viewWithTag:3];
    cell3.userInteractionEnabled=NO;
    UILabel   *cell4 = (UILabel*)[cell viewWithTag:4];
    cell4.userInteractionEnabled=NO;
    
    [cell1 setTitleColor:[theme openSettledOptionsTimeTextColor] forState:UIControlStateNormal];
    cell1.titleLabel.font = AT_PULLABLETABLE_OPTION_LEVEL_BUTTON_FONT;
    cell1.titleLabel.adjustsFontSizeToFitWidth = YES;
    cell3.textColor = [theme openSettledOptionsTimeTextColor];
    cell4.textColor = [theme openSettledOptionsTimeTextColor];
    cell4.font = AT_PULLABLETABLE_OPTION_LEVEL_BUTTON_FONT;
    
    AOInvestment *investment = (AOInvestment*)[[dataSource objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]];
    if(!investment.settledInvestment)
        investment.level = [AOUtils formattedLevelWithLevel:_currentLevel andDecimalPoint:self.market.decimalPoint];
    
    [[cell viewWithTag:6] removeFromSuperview];
    if(indexPath.section==0) {
//        [cell1 setTitle:@"hi" forState:UIControlStateNormal];
        [cell1 setTitle:[investment getPurchaseTimeFormatted] forState:UIControlStateNormal];
        [cell1 setImage:[UIImage imageNamed:@"nav_blue-live-dot.png"] forState:UIControlStateNormal];
        cell4.text = [AOUtils formatCurrencyAmount:[AOUtils divideCurrencyAmount:[investment returnAtLevel:investment.currentLevel ]]];
        
#if ETRADER
        cell1.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
#endif
        
    }
    else {
        NSLog(@"[investment getPurchaseTimeFormatted]: %@", [investment getPurchaseTimeFormatted]);
        [cell1 setTitle:[investment getPurchaseTimeFormatted] forState:UIControlStateNormal];
        cell4.text = investment.amountReturnWF;
    }
    
    GetQuoteButton* getQuoteButton = (GetQuoteButton*)[cell viewWithTag:8];
    getQuoteButton.investment = investment;
  
    UIImage *levelImage = nil;
    UIImage *getQuoteButtonBgImage = [UIImage imageNamed:@"get_quote"];
    
    BOOL isWinning = [investment hasRibbon];
    
    if (isWinning)
    {
        if (investment.typeId == AOInvestmentTypeCall)
        {
            levelImage = [UIImage imageNamed:@"triangle_up_small_yellow"];
            
#ifdef COPYOP
        if ([investment.copyopType isEqualToString:@"COPY"]) {
                levelImage = [UIImage imageNamed:@"copied_up_winning"];
        }
#endif
        }
        else
        {
            levelImage = [UIImage imageNamed:@"triangle_down_small_yellow"];
            
#ifdef COPYOP
            if ([investment.copyopType isEqualToString:@"COPY"]) {
                levelImage = [UIImage imageNamed:@"copied_down_winning"];
            }
#endif
        }
        
        if (IS_RIGHT_TO_LEFT)
        {
            getQuoteButtonBgImage = [UIImage imageNamed:@"get_quote_win_btn_flipped"];
        }
        else
        {
            getQuoteButtonBgImage = [UIImage imageNamed:@"get_quote_win_btn"];
        }
    }
    else
    {
        if (investment.typeId == AOInvestmentTypeCall)
        {
            levelImage = [UIImage imageNamed:@"triangle_up_small_blue"];
           
#ifdef COPYOP
            if ([investment.copyopType isEqualToString:@"COPY"]) {
                levelImage = [UIImage imageNamed:@"copied_up_losing"];
            }
#endif
        }
        else
        {
            levelImage = [UIImage imageNamed:@"triangle_down_small_blue"];
            
#ifdef COPYOP
            if ([investment.copyopType isEqualToString:@"COPY"]) {
                levelImage = [UIImage imageNamed:@"copied_down_losing"];
            }
#endif
        }
    }

    [getQuoteButton setBackgroundImage:getQuoteButtonBgImage  forState:UIControlStateNormal];
    getQuoteButton.titleLabel.font = GET_QUOTES_BTN_FONT;
    
    [getQuoteButton setTitleColor:GET_QUOTES_BUTTON_COLOR forState:UIControlStateNormal];
    [getQuoteButton setTitle:AOLocalizedString(@"optionPlus.getQuote") forState:UIControlStateNormal];
   
    if(self.optionType == AOOpportunityTypeOptionPlus) {
        if (investment.settledInvestment) {
            getQuoteButton.hidden=YES;
        }
        else {
            getQuoteButton.hidden = isWaitingForExpiry;
        }
    }
    [getQuoteButton addTarget:self action:@selector(showQuotePopup:) forControlEvents:UIControlEventTouchUpInside];
   
    if([cell viewWithTag:7]) [[cell viewWithTag:7] removeFromSuperview];
    
    cell2.titleLabel.font = AT_PULLABLETABLE_OPTION_LEVEL_BUTTON_FONT;
    [cell2 setTitleColor:[theme openSettledOptionsTimeTextColor] forState:UIControlStateNormal];
    cell2.titleLabel.adjustsFontSizeToFitWidth = YES;
    [cell2 setTitle:[AOUtils formattedLevelWithLevel:investment.currentLevel andDecimalPoint:self.market.decimalPoint] forState:UIControlStateNormal];
    [cell2 setImage:levelImage forState:UIControlStateNormal];
    
#if ETRADER
    cell2.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
#else
    cell2.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5);
#endif
    
    UIImageView *levelImageView = (UIImageView*)[cell.contentView viewWithTag:7];
    levelImageView.transform = CGAffineTransformIdentity;
    if(investment.typeId==AOInvestmentTypePut) levelImageView.transform = CGAffineTransformConcat(CGAffineTransformMakeTranslation(0,-10),CGAffineTransformMakeScale(1, -1));
    [cell.contentView viewWithTag:7].frame = CGRectOffset([cell.contentView viewWithTag:7].frame,7,0);
    
    cell3.font = AT_PULLABLETABLE_OPTION_LEVEL_BUTTON_FONT;
    cell3.text = [AOUtils formatCurrencyAmount:[AOUtils divideCurrencyAmount:investment.amount]];
    
    [cell viewWithTag:5].hidden = !isWinning;
    if(self.optionType == AOOpportunityTypeOptionPlus && !investment.settledInvestment) [cell viewWithTag:5].hidden = !getQuoteButton.hidden;
    
    
    if(![cell.layer valueForKey:@"AODidLoadCell"]) {
        [cell.layer setValue:@"" forKey:@"AODidLoadCell"];
        
        UIView *selectedBackgroundView = [[UIView alloc] init];
        selectedBackgroundView.backgroundColor = [theme openSettledOptionsSelectedCellBackgroundColor];
        cell.selectedBackgroundView = selectedBackgroundView;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        
#if !COPYOP
        CGFloat cellWidth = cell.contentView.frame.size.width/4;
        
        for(int i=1;i<=3;i++){
            CALayer *border = [CALayer layer];
            border.frame = CGRectMake(cellWidth*i,0,0.5,cell.contentView.frame.size.height-0.5);
            border.backgroundColor = [UIColor colorWithRed:0.786 green:0.868 blue:0.923 alpha:1].CGColor;
            [cell.contentView.layer addSublayer:border];
        }
#endif
        
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0, cell.contentView.frame.size.height-0.5, cell.contentView.frame.size.width, 0.5);
        bottomBorder.backgroundColor = [theme openSettledOptionsSeparatorColor].CGColor;
        [cell.contentView.layer addSublayer:bottomBorder];
    }
    else {
        
    }
    
    if (IS_RIGHT_TO_LEFT)
    {
        UIImageView *imgView = (UIImageView *)[cell viewWithTag:5];
        imgView.image = [[UIImage imageNamed:@"misc_gold-badge.png"] flippedImage];
    }
    cell2.userInteractionEnabled = YES;
    
#ifdef COPYOP
    
    if ([investment.copyopType isEqualToString:@"COPY"]) {
        UITapGestureRecognizer *showoffTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleCopiedOpetionTapped:)];
        [cell2 addGestureRecognizer:showoffTapRecognizer];
    }
    else{
        for (UIGestureRecognizer *recognizer in cell2.gestureRecognizers) {
            [cell2 removeGestureRecognizer:recognizer];
        }
    }
    
#endif
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [dataSource count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    

    
    
    if(section == 1) {
        return [[dataSource objectAtIndex:section] count] + 1;
    } else {
        return [[dataSource objectAtIndex:section] count];
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 1) {
        if(indexPath.row == [dataSource[indexPath.section] count]) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"moreInfoCell"];
            
            UILabel *cellLabel = (UILabel*)[cell viewWithTag:2];
            cellLabel.userInteractionEnabled=NO;
            
            id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
            cellLabel.textColor = [theme settledOptionsTextColor];
            
            cellLabel.textAlignment = NSTextAlignmentCenter;
            cellLabel.numberOfLines = 2;
            cellLabel.backgroundColor = [UIColor clearColor];
#if ANYOPTION
            cellLabel.font = [UIFont robotoFontMediumWithSize:11.0];
            cellLabel.text = [NSString stringWithFormat:AOLocalizedString(@"settledMoreInfo"), @"anyoption"];
#elif COPYOP
            cellLabel.font = [UIFont robotoFontMediumWithSize:11.0];
            cellLabel.text = [NSString stringWithFormat:AOLocalizedString(@"settledMoreInfo"), @"copyop"];
#else
            
            cellLabel.font = [UIFont systemFontOfSize:11.0];
            cellLabel.text = [NSString stringWithFormat:AOLocalizedString(@"settledMoreInfo"), @"etrader"];
#endif
            
            if(IS_IPAD) {
                cell.contentView.backgroundColor = [theme assetsBackgroundColor];
                cell.backgroundColor = [theme assetsBackgroundColor];
            }
            
            return cell;
        } else {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"optionCell"];
            [self setupCell:cell forIndexPath:indexPath];
            return cell;
        }
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"optionCell"];
        [self setupCell:cell forIndexPath:indexPath];
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *container = [[[NSBundle mainBundle] loadNibNamed:@"OpenOptionsTableHeader" owner:nil options:nil] objectAtIndex:0];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    container.backgroundColor = [theme openSettledOptionsHeaderBackgroundColor];
    
    UILabel *titleLabel = (UILabel*)[container viewWithTag:1];
    titleLabel.textColor = [theme openSettledOptionsHeaderTextColor];
    titleLabel.text = [self getSectionTitleWithSection:section];
    UILabel *profitLabel = (UILabel*)[container viewWithTag:2];
    profitLabel.textColor = [theme openSettledOptionsProfitTextColor];
    profitLabel.text = section ? AOLocalizedString(@"button.return") : @"";
    container.layer.borderWidth = 0.5;
    container.layer.borderColor = [UIColor colorWithRed:0.545 green:0.714 blue:0.824 alpha:1].CGColor;
    
    return container;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if([[dataSource objectAtIndex:section] count]==0) return 0;
    else return 14;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if(indexPath.section == 1) {
        if(indexPath.row == [dataSource[indexPath.section] count]) {
            return 44.0f;
        }
    }
    
    return 26.0f;
}

-(NSString*)getSectionTitleWithSection:(NSInteger)section{
    
    if(section==0) return AOLocalizedString(@"investments.open");
    else return AOLocalizedString(@"investments.close");
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if([indexPath section]==0){ //tapped an open option
        AOInvestment *investment = (AOInvestment*)[[dataSource objectAtIndex:0] objectAtIndex:[indexPath row]];
        
        if (investment!=nil) {
            [self.delegate openOptionTapped:investment];
        }
    }
}

#pragma mark -
#pragma mark Set Options

-(void)setCurrentLevel:(float)currentLevel{
    _currentLevel = currentLevel;
    [pullableTable reloadData];
    NSMutableArray *openOptionsArray = [dataSource objectAtIndex:0];
    NSMutableArray *settledOptionsArray = [dataSource objectAtIndex:1];
    if(openOptionsArray.count==0&&settledOptionsArray.count==0) [pullableView showHandle:NO];
    else [pullableView showHandle:YES];
}

-(NSArray*)getOpenOptions{
    return [dataSource objectAtIndex:0];
}

- (void) setOpenOptions:(NSArray*)oOpts {
    NSMutableArray *openOptionsArray = [dataSource objectAtIndex:0];
    [openOptionsArray removeAllObjects];
    for(AOInvestment *investment in oOpts) {
        [openOptionsArray addObject:[investment copy]];
    }
    if (openOptionsArray.count)
        [pullableView showHandle:YES];
    
    [pullableTable reloadData];
}

-(void)removeOpenOptions{
    NSMutableArray *openOptionsArray = [dataSource objectAtIndex:0];
    [openOptionsArray removeAllObjects];
    [pullableTable reloadData];
}

-(void)addOpenOption:(AOInvestment*)investment {
    NSMutableArray *openOptionsArray = [dataSource objectAtIndex:0];
    [openOptionsArray insertObject:investment atIndex:0];
    [pullableTable reloadData];
    _pastInvestment = [investment copy];
    
    [pullableView showHandle:YES];
}

-(void)setSettledOptions:(NSArray*)sOpts {
    NSMutableArray *settledOptionsArray = [dataSource objectAtIndex:1];
    [settledOptionsArray removeAllObjects];
    for (NSDictionary *investmentDict in sOpts){
        AOInvestment *investment = [[AOInvestment alloc] init];
        investment.settledInvestment=YES;
        [investment setValuesForKeysWithDictionary:investmentDict];
        [settledOptionsArray addObject:investment];
    }
    
    if (settledOptionsArray.count)
        [pullableView showHandle:YES];
    [pullableTable reloadData];
}

-(void)removeSettledOptions{
    NSMutableArray *settledOptionsArray = [dataSource objectAtIndex:1];
    [settledOptionsArray removeAllObjects];
    [pullableTable reloadData];
}

#pragma mark -
#pragma mark InvestmentsOverlayDelegate

-(void)didChooseAmount:(NSNumber*)am {
    // NSLog(@"did choose amount");
    _futureInvestment.amount = [[AOUtils formatCurrencyAmountWithoutSymbol:am.doubleValue] doubleValue];
    [self setProfitLabels];
    lastInvAmount = [NSString stringWithFormat:@"%@", [AOUtils formatCurrencyAmountWithoutSymbol:am.doubleValue]];
    [investAmountLabel setText:lastInvAmount];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark -
#pragma mark ReturnsOverlayDelegate

-(void)didChooseReturnValue:(NSInteger)v {
    Return *futureReturn = (Return*)_returnValues[v];
    _futureInvestment.oddsWin = futureReturn.winIndex;
    _futureInvestment.oddsLose = futureReturn.loseIndex;
    [self setProfitLabels];
    profitPercentLabel.text = [(Return*)_returnValues[v] winReturn];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setProfitLabels{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if(!_futureInvestment.amount||!_futureInvestment.oddsWin) return;
        
        if(_futureInvestment.winReturn)
        assetProfitWinInfoLabel.text = [AOUtils formatCurrencyAmountWithoutSymbol:_futureInvestment.winReturn];

        if(_futureInvestment.loseReturn)
        assetProfitLoseInfoLabel.text = [NSString stringWithFormat:@"%@ %@", AOLocalizedString(@"CMS.tradeBox.text.if_incorrect_get"), [AOUtils formatCurrencyAmount:_futureInvestment.loseReturn]];
    });
}

-(void) showQuotePopup:(GetQuoteButton*)sender {
    AOTradeOptionPlusQuoteViewController *opvc = [[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:@"OptionPlusQuote"];
    opvc.investment = sender.investment;
    
    if (!IS_IPAD)
    {
        [self.navigationController presentViewController:opvc animated:YES completion:nil];
    }
    else
    {
        [MAINVC setLockOverlay:YES];
        [MAINVC loadPage:@"Trade" pageName:@"OptionPlusQuote" root:YES options:@{@"investment" : sender.investment, @"key":@"remove"}];
    }
}

#pragma mark - 
#pragma mark Show Receipt

-(void)showReceipt {
    ReceiptViewController *receiptViewController = [[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:@"receiptController"];
    receiptViewController.receiptInvestment = _pastInvestment;
    
    [self presentViewController:receiptViewController animated:YES completion:nil];
}

- (void)openClosePullableView {
    if (pullableView.opened) {
        [pullableView setSlideOpen:NO];
    } else {
        [pullableView setSlideOpen:YES];
    }
}

-(void)showHidePullableViewHandle:(BOOL)show {
    [pullableView showHandle:show];
}

- (void)setupCommissionLabel:(long)commission {
    commissionLabel.hidden = NO;
    NSString* commissionString = [NSString stringWithFormat:@"+%@ %@", AOLocalizedString(@"optionPlus.commission"), [AOUtils formatCurrencyAmount:[AOUtils divideCurrencyAmount:commission]]];
    commissionLabel.text = [commissionString stringByStrippingHTML];
}

#pragma mark - Popup Info

- (UIView *) vCall
{
    return callBtn;
}

- (UIView *) vPut
{
    return putBtn;
}

- (UIView *) vSlideHandle
{
    return pullableView.handleView;
}

- (UIView *) vInvestRC
{
    return investRCView;
}

#ifdef COPYOP

-(void)handleCopiedOpetionTapped:(UIGestureRecognizer *)tapper  {
    
    CGPoint tapLocation = [tapper locationInView:pullableTable];
    NSIndexPath *tappedIndexPath = [pullableTable indexPathForRowAtPoint:tapLocation];
    
    if (tappedIndexPath.section==0) {
        return;  //we only proceed for a settled & copied option
    }
    
    AOInvestment *selectedInvestment = dataSource[1][tappedIndexPath.row];
    
    
    [[RestAPIFacade sharedInstance] getOriginalInvestmentInfoFor:selectedInvestment.l_id andCompletion:^(COProfile *originalProfile, AOResponse * response) {
        
        if (response.error != nil && response.error.code != SERVICE_ERROR_USER_REMOVED)
        {
            [response.error show];
            
            return;
        }
        //error check
        
        [COCopyUserController  showOriginalInvestmentInfo:originalProfile parentController:self completion:nil ];
    }];
}

- (void)coDidChooseAmount:(double)amount {
    [self didChooseAmount:[NSNumber numberWithDouble:amount]];
}

- (void)coDidChooseReturnIndex:(NSInteger)index {
    [self didChooseReturnValue:index];
}

- (void)coReturnPercentViewControllerDidClose {
    [self.popupCont dismissViewControllerAnimated:YES completion:^{
        self.popupCont = nil;
    }];
}

#endif

@end