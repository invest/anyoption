//
//  AOTrendView.h
//  test
//
//  Created by mac_user on 11/11/13.
//  Copyright (c) 2013 mac_user. All rights reserved.
//

@interface AOTrendView : UIView{
    __weak IBOutlet UIButton *trendUp;
    __weak IBOutlet UIButton *trendDown;
    __weak IBOutlet UIView *scalableTrendView;
    __weak IBOutlet UIImageView *trendIcon;
}

@property (nonatomic,assign) int percent;

@end
