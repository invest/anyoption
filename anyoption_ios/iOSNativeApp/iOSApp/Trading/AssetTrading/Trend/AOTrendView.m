//
//  AOTrendView.m
//  test
//
//  Created by mac_user on 11/11/13.
//  Copyright (c) 2013 mac_user. All rights reserved.
//

#import "AOTrendView.h"

@implementation AOTrendView

-(void)dealloc{
    // NSLog(@"AOTrendView dealloc");
}

-(void)awakeFromNib{
//    CGFloat spacing = -10;
    CGFloat spacing = -8;
    trendUp.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
    trendDown.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
    [trendDown setImage:[UIImage imageNamed:@"trend_down_small_trngl"] forState:UIControlStateNormal];
    [trendUp setImage:[UIImage imageNamed:@"trend_up_small_trngl"] forState:UIControlStateNormal];
    trendDown.titleLabel.font = [UIFont fontWithName:APP_FONT_BOLD size:11.1];
    trendUp.titleLabel.font = [UIFont fontWithName:APP_FONT_BOLD size:11.1];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    self.backgroundColor = [theme trendsBackgroundColor];
    scalableTrendView.backgroundColor = [theme scalableTrendBackgroundColor];
}

-(void)setPercent:(int)percent{
    if(percent>100) {
        percent = 100;
    } else if (percent<0) {
        percent = 0;
    }
    
    _percent = percent;
    
    NSUInteger cVal = [self getWidthWithPercent:percent];
    if (percent>80) {
        cVal = [self getWidthWithPercent:80];
    } else if (percent<20) {
        cVal = [self getWidthWithPercent:20];
    }
    
    [trendUp setTitle:[self getPercentStringWithInt:_percent] forState:UIControlStateNormal];
    [trendDown setTitle:[self getPercentStringWithInt:100-_percent] forState:UIControlStateNormal];
    
    NSUInteger trendUpX = cVal/2 - trendUp.frame.size.width/2;
    NSUInteger trendDownX = cVal + (self.frame.size.width - cVal)/2 - trendDown.frame.size.width/2;
    NSUInteger trendIconX = cVal - trendIcon.frame.size.width/2;

    [UIView animateWithDuration:0.5 animations:^{
        scalableTrendView.frame = CGRectMake(scalableTrendView.frame.origin.x, scalableTrendView.frame.origin.y, cVal, scalableTrendView.frame.size.height);
        trendUp.frame = CGRectMake(trendUpX, trendUp.frame.origin.y, trendUp.frame.size.width, trendUp.frame.size.height);
        trendDown.frame = CGRectMake(trendDownX, trendDown.frame.origin.y, trendDown.frame.size.width, trendDown.frame.size.height);
        trendIcon.frame = CGRectMake(trendIconX, trendIcon.frame.origin.y, trendIcon.frame.size.width, trendIcon.frame.size.height);
    }];
}
-(int)getPercent{
    return self.percent;
}

-(NSString*)getPercentStringWithInt:(int)percent{
    return [NSString stringWithFormat:@"%d%%",percent];
}
-(float)getWidthWithPercent:(int)percent{
    return self.frame.size.width*percent/100;
}

@end
