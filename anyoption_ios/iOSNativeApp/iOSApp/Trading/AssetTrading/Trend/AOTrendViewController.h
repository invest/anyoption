//
//  AOTrendViewController.h
//  iOSApp
//
//  Created by mac_user on 3/6/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AOTrendView;

@interface AOTrendViewController : UIViewController {

}

@property (weak,nonatomic) IBOutlet AOTrendView *trendView;

@end
