
@class AOPullableView;

/**
 Protocol for objects that wish to be notified when the state of a
 PullableView changes
 */
@protocol AOPullableViewDelegate <NSObject>

/**
 Notifies of a changed state
 @param pView PullableView whose state was changed
 @param opened The new state of the view
 */
- (void)pullableView:(AOPullableView *)pView didChangeState:(BOOL)opened;
- (void)pullableView:(AOPullableView *)pView willChangeState:(BOOL)opened;
- (void)pullableViewAnimation:(AOPullableView *)pView isOpening:(BOOL)isOpening;

@end

/**
 Class that implements a view that can be pulled out by a handle, 
 similar to the Notification Center in iOS 5. This class supports
 pulling in the horizontal or vertical axis. This is determined by
 the values for openedCenter and closedCenter that you set: if
 both have the same x coordinate, the pulling should happen in the
 vertical axis, or the horizontal axis otherwise.
 @author Fabio Rodella fabio@crocodella.com.br
 */
@interface AOPullableView : UIView <UIGestureRecognizerDelegate> {
    CGPoint startPos;
    CGPoint handleStartPos;
    CGPoint minPos;
    CGPoint maxPos;
    
    BOOL verticalAxis;
    BOOL didInit;
}

@property(nonatomic,weak) IBOutlet NSLayoutConstraint *topConstraint;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *bottomConstraint;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *handleTopConstraint;
@property BOOL shiftsHandleUpwards;

/**
 The view that is used as the handle for the PullableView. You
 can style it, add subviews or set its frame at will.
 */
@property (nonatomic,weak) IBOutlet UIView *handleView;

/**
 The point that defines the center of the view when in its closed
 state. You must set this before using the PullableView.
 */
@property (readwrite,assign) CGPoint closedCenter;
@property (readwrite,assign) CGPoint handleClosedCenter;

/**
 The point that defines the center of the view when in its opened
 state. You must set this before using the PullableView.
 */
@property (readwrite,assign) CGPoint openedCenter;
@property (readwrite,assign) CGPoint handleOpenedCenter;

/**
 Gesture recognizer responsible for the dragging of the handle view.
 It is exposed as a property so you can change the number of touches
 or created dependencies to other recognizers in your views.
 */
@property (nonatomic,readonly) UIPanGestureRecognizer *dragRecognizer;

/**
 Gesture recognizer responsible for handling tapping of the handle view.
 It is exposed as a property so you can change the number of touches
 or created dependencies to other recognizers in your views.
 */
@property (nonatomic,readonly) UITapGestureRecognizer *tapRecognizer;

/**
 If set to YES, tapping the handle view will toggle the PullableView.
 Default value is YES.
 */
@property (readwrite,assign) BOOL toggleOnTap;

/**
 If set to YES, the opening or closing of the PullableView will
 be animated. Default value is YES.
 */
@property (readwrite,assign) BOOL animate;

/**
 Duration of the opening/closing animation, if enabled. Default
 value is 0.2.
 */
@property (readwrite,assign) float animationDuration;

/**
 Delegate that will be notified when the PullableView changes state.
 If the view is set to animate transitions, the delegate will be
 called only when the animation finishes.
 */
@property (readwrite,weak) IBOutlet id<AOPullableViewDelegate> delegate;

/**
 The current state of the `PullableView`.
 */
@property (readonly, assign) BOOL opened;

@property (nonatomic,assign) BOOL hidesHandle;

/**
 Toggles the state of the PullableView
 @param op New state of the view
 @param anim Flag indicating if the transition should be animated
 */
- (void)setOpened:(BOOL)op animated:(BOOL)anim;

-(void)onLayoutLoad;
-(void)onLayoutDisappear;

@end
