//
//  AOSlideView.h
//  iOSApp
//
//  Created by  on 5/26/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AOSlideView : UIView <UIGestureRecognizerDelegate>


@property(nonatomic, weak) IBOutlet UIView* pullableView;
@property(nonatomic, weak) IBOutlet UIView* handleView;

@property(nonatomic,strong) UIPanGestureRecognizer *panGestureRecognizer;

@property(nonatomic, getter = isHorizontalView) BOOL horizontalView;
@property(nonatomic, getter = isClosed) BOOL closed;
@property(nonatomic, getter = isOpened) BOOL opened;
@property (nonatomic) CGPoint handleViewClosedCenterPoint;
@property (nonatomic,) CGPoint handleViewOpenedCenterPoint;

- (void)showHandle:(BOOL)show;
- (void)setSlideOpen:(BOOL)open;

@end
