//
//  AOAssetInfoViewController.m
//  iOSApp
//
//  Created by Vasil Garov on 4/25/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOAssetInfoViewController.h"
#import "AOAssetIndexMethodRequest.h"
#import "AOPastExpiresMethodRequest.h"
#import "AORequestService.h"
#import <objc/runtime.h>
#import "NSString+Utils.h"
#import "AOOpportunity.h"

@interface AOAssetInfoViewController ()

@end

@implementation AOAssetInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)layoutView {
    float viewWidth = self.view.frame.size.width;
    float viewHeight = self.view.frame.size.height;
    
    if (IS_BELOW_IPHONE5) {
        CGRect fr = recentExpiriesScrollView.frame;
        fr.origin.y = CGRectGetHeight(self.view.frame) - CGRectGetHeight(fr);
        recentExpiriesScrollView.frame = fr;

        separator.frame = CGRectMake(0, CGRectGetMinY(recentExpiriesScrollView.frame), CGRectGetWidth(assetInfoScrollView.bounds), 1.0);
        
        assetInfoScrollView.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(assetInfoScrollView.bounds), CGRectGetMinY(separator.frame));
    } else if (IS_IPAD) {
        self.view.frame = CGRectMake(0, 0, viewWidth, viewHeight);
        assetInfoScrollView.frame = CGRectMake(0, 0, viewWidth, viewHeight-recentExpiriesScrollView.frame.size.height);
        separator.frame = CGRectMake(0, assetInfoScrollView.frame.size.height, assetInfoScrollView.frame.size.width, 1);
        recentExpiriesScrollView.frame = CGRectMake(0, viewHeight-recentExpiriesScrollView.frame.size.height, viewWidth, recentExpiriesScrollView.frame.size.height);
    }
}

-(void)reloadAssetInfo{
    [self layoutView];
    
    UIView *recentExpiriesScrollViewContainer = [[recentExpiriesScrollView subviews] objectAtIndex:0];
    if(recentExpiriesScrollViewContainer.subviews.count>0) {
        for(UILabel *label in recentExpiriesScrollViewContainer.subviews){
            [label removeFromSuperview];
        }
        for(UILabel *label in assetInfoScrollView.subviews){
            [label removeFromSuperview];
        }
        for(UIImageView *imageView in assetInfoScrollView.subviews){
            [imageView removeFromSuperview];
        }
    }
    recentExpiriesScrollViewContainer.layer.sublayers = nil;
    CGFloat recentExpiriesContentSpacing = 10;
    
    AOAssetIndexMethodRequest *aiRequest = [[AOAssetIndexMethodRequest alloc] init];
    
    aiRequest.marketId = _market.getId;
    
    [AORequestService startServiceRequestWithMethodRequest:aiRequest success:^(NSDictionary *resultDictionary) {
        NSDictionary *assetIndex = [resultDictionary objectForKey:@"assetIndex"];
        
        const char* className = class_getName([[resultDictionary objectForKey:@"assetIndex"] class]);
        // NSLog(@"yourObject is a: %s", className);
        
        NSString *assetDescription = assetIndex[@"description"];
        
        UIFont *font = [UIFont fontWithName:APP_FONT size:13.3];
        UIColor *textColor = [UIColor whiteColor];
        
        //replacing UILabel with UITextView in order to respond to link clicks
        UITextView *assetInfoLabel = [[UITextView alloc] init];
        assetInfoLabel.frame = CGRectMake(5,13,300,0);
        assetInfoLabel.font = font;
        assetInfoLabel.textColor = textColor;
        assetInfoLabel.backgroundColor = [UIColor clearColor];
        assetInfoLabel.editable = NO;
        assetInfoLabel.dataDetectorTypes = UIDataDetectorTypeLink;
        assetInfoLabel.linkTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor], NSUnderlineStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]};
//        assetInfoLabel.numberOfLines = 0;
        [assetInfoScrollView addSubview:assetInfoLabel];
        [assetInfoLabel setText:[[assetDescription stringByStrippingHTML] stringByUnescapingHTML]];
        [assetInfoLabel sizeToFit];
        
        if (IS_RIGHT_TO_LEFT)
        {
            CGRect fr = assetInfoLabel.frame;
            fr.origin.x = CGRectGetWidth(assetInfoLabel.superview.bounds) - 10.0 - CGRectGetWidth(fr);
            assetInfoLabel.frame = fr;
            assetInfoLabel.textAlignment = NSTextAlignmentRight;
        }
        
        CGFloat yOffset = assetInfoLabel.frame.origin.y+assetInfoLabel.frame.size.height+10;
        
        for(int i=0;i<5;i++){
            NSString *firstString = nil;
            NSString *secondString = nil;
            
            if(i==0){ //symbol
                firstString = [NSString stringWithFormat:@"%@: ", AOLocalizedString(@"CMS.assetsIndex.label.l1")];
                secondString = [resultDictionary objectForKey:@"assetIndex"]!=[NSNull null]&&[assetIndex objectForKey:@"feedName"]?[NSString stringWithString:[assetIndex objectForKey:@"feedName"]]:@"";
            }
            else if (i==1){ //expiry type
                firstString = [[NSString stringWithFormat:@"%@: ", AOLocalizedString(@"CMS.general_terms_content.text.text143")] stringByStrippingHTML];
                secondString = [[NSString stringWithString:AOLocalizedString(@"CMS.general_terms_content.text.text146")] stringByStrippingHTML];
            }
            else if (i==2){ //reuters field
                firstString = [[NSString stringWithFormat:@"%@: ", AOLocalizedString(@"CMS.general_terms_content.text.text144")] stringByStrippingHTML];
                secondString = [resultDictionary objectForKey:@"assetIndex"]!=[NSNull null]&&[assetIndex objectForKey:@"reutersField"]?[NSString stringWithString:[assetIndex objectForKey:@"reutersField"]]:@"";
            }
            else if (i==3){ //trading times
                firstString = [NSString stringWithFormat:@"%@ ", AOLocalizedString(@"assetIndex.tradingTime")];
                secondString = [resultDictionary objectForKey:@"assetIndex"]!=[NSNull null]?[NSString stringWithString:[self getTradingDaysWithAssetDict:assetIndex]]:@"";
            }
            else if (i==4){ // expiry formula
                firstString = [[NSString stringWithFormat:@"%@: ", AOLocalizedString(@"CMS.general_terms_content.text.text145")] stringByStrippingHTML];
                secondString = [resultDictionary objectForKey:@"assetIndex"]!=[NSNull null]&&[assetIndex objectForKey:@"expiryFormula"]?[NSString stringWithString:[assetIndex objectForKey:@"expiryFormula"]]:@"";
                secondString = [secondString stringByUnescapingHTML];
            }
            
            firstString = [[firstString stringByStrippingHTML] stringByUnescapingHTML];
            secondString = [[secondString stringByStrippingHTML] stringByUnescapingHTML];
            
            
            NSMutableAttributedString *firstAttrString = [[NSMutableAttributedString alloc] initWithString:firstString attributes:@{NSFontAttributeName:[UIFont fontWithName:APP_FONT_BOLD size:13.3]}];
            NSMutableAttributedString *secondAttrString = [[NSMutableAttributedString alloc] initWithString:secondString attributes:@{NSFontAttributeName:font}];
            
            [firstAttrString appendAttributedString:secondAttrString];
            
            float labelWidth;
            
            float leftPadding = 20;
            labelWidth = self.view.frame.size.width-leftPadding;
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,yOffset,labelWidth,0)];
            label.numberOfLines = 0;
            label.textColor = textColor;
            [assetInfoScrollView addSubview:label];
            label.attributedText = firstAttrString;
            [label sizeToFit];
            
            if(i==0 && self.optionType == AOOpportunityTypeOptionPlus) {
                UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"option_plus_icon_1"]];
                [assetInfoScrollView addSubview:imageView];
                imageView.frame = CGRectOffset(imageView.frame,label.frame.origin.x+label.frame.size.width+5,label.frame.origin.y-(imageView.frame.size.height-label.frame.size.height)/2);
            }
            
            yOffset += label.frame.size.height+2;
            
            if (IS_RIGHT_TO_LEFT)
            {
                CGRect fr = label.frame;
                fr.origin.x = CGRectGetWidth(label.superview.bounds) - 10.0 - CGRectGetWidth(fr);
                label.frame = fr;
                label.textAlignment = NSTextAlignmentRight;
            }
        }
        yOffset += 10;
        
        [assetInfoScrollView setContentSize:CGSizeMake(assetInfoScrollView.frame.size.width,yOffset)];
    }
                                                    failed:^(NSDictionary *errDictionary) {
                                                        // NSLog(@"getAssetIndexByMarket failed: %@",errDictionary);
                                                    }];
    
    AOPastExpiresMethodRequest *peRequest = [[AOPastExpiresMethodRequest alloc] init];
    peRequest.marketId = _market.getId;
    peRequest.startRow = 0;
    peRequest.pageSize = 3;
//    peRequest.pageSize = 0; // 0 - all
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSDateComponents *components = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:[AOUtils currentDatePrecise]];
//    NSString *yearString = [NSString stringWithFormat:@"%ld", (long)components.year];
//    NSString *monthString = [NSString stringWithFormat:@"%ld", (long)components.month];
//    NSString *dayString = [NSString stringWithFormat:@"%ld", (long)components.day];
//    peRequest.date = [AOUtils calendarOptionDictionaryWithYear:yearString month:monthString day:dayString from:YES];
    
    [AORequestService startServiceRequestWithMethodRequest:peRequest success:^(NSDictionary *resultDictionary) {
        // NSLog(@"getPastExpiries success:%@",resultDictionary);
        NSArray *pastExpiries = [resultDictionary objectForKey:@"opportunities"];
        // NSLog(@"getPastExpiriesSuccess:%@",pastExpiries);

        recentExpiriesScrollViewContentWidth = recentExpiriesContentSpacing;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [dateFormatter setDateFormat:@"HH:mm"];
        
        NSMutableArray *labels = [NSMutableArray array];
        
        for(int i=0;i<=pastExpiries.count;i++){
            NSMutableAttributedString *attrString = nil;
            
            if(i==0) {
                NSString *string = AOLocalizedString(@"menuStrip.past.expiries");
                attrString = [[NSMutableAttributedString alloc] initWithString:string];
                [attrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:APP_FONT_BOLD size:11] range:NSMakeRange(0,string.length)];
            }
            else {
//                NSDate *date = [AOUtils parseTimeFromJSON:[[pastExpiries objectAtIndex:i-1] objectForKey:@"timeEstClosing"]];
                
                NSDateFormatter *expFormatter = [[NSDateFormatter alloc] init];
//                [expFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
                [expFormatter setDateFormat:@"HH:mm dd/MM/yyyy"];
                NSDate *expDate = [expFormatter dateFromString:[[pastExpiries objectAtIndex:i-1] objectForKey:@"timeEstClosingTxt"]];

                NSString *timeString = [dateFormatter stringFromDate:expDate];
                NSString *levelString = [[pastExpiries objectAtIndex:i-1] objectForKey:@"closeLevelTxt"];
                
                // NSLog(@"timeEstClosingTXTTXT %@", [[pastExpiries objectAtIndex:i-1] objectForKey:@"timeEstClosingTxt"]);
                // NSLog(@"closeLevelTXTTXT %@", [[pastExpiries objectAtIndex:i-1] objectForKey:@"closeLevelTxt"]);
                
                NSString *string = [NSString stringWithFormat:@"%@ %@",timeString,levelString];
                attrString = [[NSMutableAttributedString alloc] initWithString:string];
                
                NSRange levelRange = [string rangeOfString:levelString];
                [attrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:APP_FONT size:11] range:NSMakeRange(0,string.length)];
                [attrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:APP_FONT_BOLD size:11] range:levelRange];
                [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.114 green:0.686 blue:0.925 alpha:1] range:levelRange];
            }
            
            UILabel *label = [[UILabel alloc] init];
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor whiteColor];
            label.numberOfLines = 1;
            label.frame = CGRectMake(recentExpiriesScrollViewContentWidth,0,attrString.size.width,recentExpiriesScrollView.frame.size.height);
            label.attributedText = attrString;
            [recentExpiriesScrollViewContainer addSubview:label];
            
            recentExpiriesScrollViewContentWidth += attrString.size.width+10;
            
            if (!IS_RIGHT_TO_LEFT)
            {
                if(i<pastExpiries.count){
                    [self p_addBorderAtX:recentExpiriesScrollViewContentWidth-recentExpiriesContentSpacing/2];
                }
            }
            
            [recentExpiriesScrollView setContentSize:CGSizeMake(recentExpiriesScrollViewContentWidth,recentExpiriesScrollView.frame.size.height)];
            
            [labels addObject:label];
        }
        
        if (IS_RIGHT_TO_LEFT)
        {
            CGFloat totalWidth = recentExpiriesContentSpacing;
            for (UILabel *lbl in labels)
            {
                totalWidth += CGRectGetWidth(lbl.bounds) + recentExpiriesContentSpacing;
            }
            
            CGFloat scrollViewWidth = CGRectGetWidth(recentExpiriesScrollView.bounds);
            CGFloat left;
            if (totalWidth < scrollViewWidth)
            {
                left = scrollViewWidth - totalWidth + recentExpiriesContentSpacing;
            }
            else
            {
                left = recentExpiriesContentSpacing;
            }
            
            if (totalWidth >= CGRectGetWidth(recentExpiriesScrollView.bounds) || YES)
            {
                for (NSInteger i = [labels count] - 1; i >= 0; i--)
                {
                    UILabel *lbl = labels[i];
                    CGRect fr = lbl.frame;
                    fr.origin.x = left;
                    lbl.frame = fr;
                    
                    left += CGRectGetWidth(lbl.bounds) + recentExpiriesContentSpacing;
                    
                    if (i > 0)
                    {
                        [self p_addBorderAtX:left - recentExpiriesContentSpacing / 2.0];
                    }
                }
                
                [recentExpiriesScrollView setContentSize:CGSizeMake(left,
                                                                    CGRectGetHeight(recentExpiriesScrollView.bounds))];
                [recentExpiriesScrollView scrollRectToVisible:CGRectMake(left - 1.0, 0.0, 1.0, 1.0) animated:NO];
            }
        }
    }
    failed:^(NSDictionary *errDictionary) {
        // NSLog(@"getPastExpiries failed: %@",errDictionary);
    }];
}

- (void) p_addBorderAtX:(CGFloat)x
{
    CALayer *border = [CALayer layer];
    border.frame = CGRectMake(x, 13.0, [UIScreen mainScreen].scale / 2.0, 9.0);
    border.backgroundColor = [UIColor whiteColor].CGColor;
    [recentExpiriesScrollView.layer addSublayer:border];
}

-(NSString*)getTradingDaysWithAssetDict:(NSDictionary*)assetDict{
//    NSString *tradingDaysJson = [assetDict objectForKey:@"tradingDays"];
//    
//    NSMutableArray *tradingDates = [[NSMutableArray alloc] init];
//    BOOL isParsingEndDate = NO;
    
//    for(int i=0;i<[(NSString*)tradingDaysJson length];i++){
//        BOOL isTradingTime = [[tradingDaysJson substringWithRange:NSMakeRange(i,1)] boolValue];
//        NSString *timeString = nil;
//        
//        if(!isParsingEndDate&&isTradingTime) {
//            timeString = [NSString stringWithFormat:@"%@ %@",[self getDayOfWeekWithInt:i+1 localizedString:NO],[assetDict objectForKey:@"timeFirstInvest"]];
//            isParsingEndDate = YES;
//        }
//        else if(isParsingEndDate) {
//            if(!isTradingTime) {
//                timeString = [NSString stringWithFormat:@"%@ %@",[self getDayOfWeekWithInt:i localizedString:NO],[assetDict objectForKey:@"timeEstClosing"]];
//            }
//            else if(isTradingTime&&i==6) {
//                timeString = [NSString stringWithFormat:@"%@ %@",[self getDayOfWeekWithInt:i+1 localizedString:NO],[assetDict objectForKey:@"timeEstClosing"]];
//            }
//        }
//        
//        if(timeString) {
//            [tradingDates addObject:[AOUtils parseTimeFromJSON:timeString withDateFormat:@"ccc HH:mm" addTimezoneOffset:YES]];
//        };
//    }
    
    
    NSString* timeString = [assetDict objectForKey:@"tradingDaysFormat"];
    
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSDateComponents *startComponents = [calendar components:(NSWeekdayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit) fromDate:[tradingDates objectAtIndex:0]];
//    NSDateComponents *endComponents = [calendar components:(NSWeekdayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit) fromDate:[tradingDates objectAtIndex:1]];
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
//    [dateFormatter setDateFormat:@"HH:mm"];
//    
//    NSString *firstString = [NSString stringWithFormat:@"%@ - %@",AOLocalizedString([self getDayOfWeekWithInt:startComponents.weekday localizedString:YES]),AOLocalizedString([self getDayOfWeekWithInt:endComponents.weekday localizedString:YES])];
//    NSString *secondString = [NSString stringWithFormat:@"%@ - %@",[dateFormatter stringFromDate:[tradingDates objectAtIndex:0]],[dateFormatter stringFromDate:[tradingDates objectAtIndex:1]] ];
//    return [NSString stringWithFormat:@"%@ %@",firstString,secondString];
    return timeString;
}

-(NSString*)getDayOfWeekWithInt:(NSInteger)dayOfWeek localizedString:(BOOL)localized{
    if(!localized){
        if(dayOfWeek==1) return @"Sun";
        else if(dayOfWeek==2) return @"Mon";
        else if(dayOfWeek==3) return @"Tue";
        else if(dayOfWeek==4) return @"Wed";
        else if(dayOfWeek==5) return @"Thu";
        else if(dayOfWeek==6) return @"Fri";
        else if(dayOfWeek==7) return @"Sat";
        else {
            // NSLog(@"getDayOfWeekWithInt error: %ld not a valid weekday",(long)dayOfWeek);
            return @"";
        }
    }
    else{
        if(dayOfWeek==1) return @"assetIndex.sunday";
        else if(dayOfWeek==2) return @"assetIndex.monday";
        else if(dayOfWeek==3) return @"assetIndex.tuesday";
        else if(dayOfWeek==4) return @"assetIndex.wednesday";
        else if(dayOfWeek==5) return @"assetIndex.thursday";
        else if(dayOfWeek==6) return @"assetIndex.friday";
        else if(dayOfWeek==7) return @"assetIndex.saturday";
        else {
            // NSLog(@"getDayOfWeekWithInt error: %ld not a valid weekday",(long)dayOfWeek);
            return @"";
        }
    }
}

@end
