//
//  AOAssetInfoViewController.h
//  iOSApp
//
//  Created by Vasil Garov on 4/25/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AOMarket.h"
#import "AOOpportunity.h"

@interface AOAssetInfoViewController : UIViewController {
    __weak IBOutlet UIScrollView *assetInfoScrollView;
    __weak IBOutlet UIScrollView *recentExpiriesScrollView;
    __weak IBOutlet UIView *separator;
    
    CGFloat recentExpiriesScrollViewContentWidth;
}

@property (strong,nonatomic) AOMarket *market;
@property (nonatomic,assign) AOOpportunityType optionType;

-(void)reloadAssetInfo;

@end