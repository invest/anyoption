
#import "AOPullableView.h"

/**
 @author Fabio Rodella fabio@crocodella.com.br
 */

@implementation AOPullableView

@synthesize handleView;
@synthesize closedCenter,handleClosedCenter;
@synthesize openedCenter,handleOpenedCenter;
@synthesize dragRecognizer;
@synthesize tapRecognizer;
@synthesize toggleOnTap;
@synthesize animate;
@synthesize animationDuration;
@synthesize delegate;
@synthesize opened;

-(void)dealloc{
    dragRecognizer.delegate = nil;
    // NSLog(@"AOPullableView dealloc");
}

-(void)awakeFromNib{
    [self initMethod];
}

-(void)initMethod{
    self.hidden = YES;
    animate = YES;
    animationDuration = 0.2;
    
    toggleOnTap = YES;
    
    // Creates the handle view. Subclasses should resize, reposition and style this view
    
    dragRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleDrag:)];
    dragRecognizer.minimumNumberOfTouches = 1;
    dragRecognizer.maximumNumberOfTouches = 1;
    dragRecognizer.delegate = self;
    
    [handleView addGestureRecognizer:dragRecognizer];
    
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.numberOfTouchesRequired = 1;
    
    [handleView addGestureRecognizer:tapRecognizer];
    
    opened = NO;
}

-(void)onLayoutLoad{
    // NSLog(@"onLayoutLoad");
    
    if(!didInit){
        
        [self setYOffset:-self.frame.size.height];
        
        self.hidden = NO;
        didInit = YES;
        opened = NO;
        
        closedCenter = CGPointMake(self.center.x,_topConstraint.constant);
        handleClosedCenter = CGPointMake(0,_handleTopConstraint.constant);
        openedCenter = CGPointMake(self.center.x,_topConstraint.constant+self.frame.size.height);
        handleOpenedCenter = CGPointMake(0,_handleTopConstraint.constant+self.frame.size.height);
        
        if(_shiftsHandleUpwards) handleOpenedCenter = CGPointMake(0,handleOpenedCenter.y-self.handleView.frame.size.height);
    }
    else {
        if(opened) {
            [self setYOffset:openedCenter.y];
        }
        else {
            [self setYOffset:closedCenter.y];
        }
    }
    
    self.hidden = NO;
    if(!_hidesHandle) self.handleView.hidden = NO;
    
    [self.superview setNeedsLayout];
}

-(void)onLayoutDisappear{
    if(_hidesHandle) self.handleView.hidden = YES;
    if(!opened) self.hidden = YES;
}

- (void)handleDrag:(UIPanGestureRecognizer *)sender {
    
    if ([sender state] == UIGestureRecognizerStateBegan) {
        //// NSLog(@"pullableView state began");
        //startPos = self.center;
        startPos = CGPointMake(0,_topConstraint.constant);
        handleStartPos = CGPointMake(0,_handleTopConstraint.constant);
        
        // Determines if the view can be pulled in the x or y axis
        verticalAxis = closedCenter.x == openedCenter.x;
        
        // Finds the minimum and maximum points in the axis
        if (verticalAxis) {
            minPos = closedCenter.y < openedCenter.y ? closedCenter : openedCenter;
            maxPos = closedCenter.y > openedCenter.y ? closedCenter : openedCenter;
        } else {
            minPos = closedCenter.x < openedCenter.x ? closedCenter : openedCenter;
            maxPos = closedCenter.x > openedCenter.x ? closedCenter : openedCenter;
        }
        
        tapRecognizer.enabled = NO;
    } else if ([sender state] == UIGestureRecognizerStateChanged) {
        //// NSLog(@"pullableView state changed");
        
        CGPoint translate = [sender translationInView:self.superview];
        
        CGPoint newPos;
        
        // Moves the view, keeping it constrained between openedCenter and closedCenter
        if (verticalAxis) {
            
            newPos = CGPointMake(startPos.x, startPos.y + translate.y);
            
            if (newPos.y < minPos.y) {
                newPos.y = minPos.y;
                translate = CGPointMake(0, newPos.y - startPos.y);
            }
            
            if (newPos.y > maxPos.y) {
                newPos.y = maxPos.y;
                translate = CGPointMake(0, newPos.y - startPos.y);
            }
        } else {
            
            newPos = CGPointMake(startPos.x + translate.x, startPos.y);
            
            if (newPos.x < minPos.x) {
                newPos.x = minPos.x;
                translate = CGPointMake(newPos.x - startPos.x, 0);
            }
            
            if (newPos.x > maxPos.x) {
                newPos.x = maxPos.x;
                translate = CGPointMake(newPos.x - startPos.x, 0);
            }
        }
        
        [sender setTranslation:translate inView:self.superview];
        
        //self.center = newPos;
        [self setYOffset:newPos.y];
        
        self.handleView.center = CGPointMake(6+handleStartPos.x+self.handleView.frame.size.width/2,220);
        //_handleTopConstraint.constant = handleStartPos.y+translate.y;
        
    } else if ([sender state] == UIGestureRecognizerStateEnded) {
        
        // Gets the velocity of the gesture in the axis, so it can be
        // determined to which endpoint the state should be set.
        
        CGPoint vectorVelocity = [sender velocityInView:self.superview];
        CGFloat axisVelocity = verticalAxis ? vectorVelocity.y : vectorVelocity.x;
        
        CGPoint target = axisVelocity < 0 ? minPos : maxPos;
        BOOL op = CGPointEqualToPoint(target, openedCenter);
        
        //// NSLog(@"pullableView state ended %d",op);
        [self setOpened:op animated:animate];
    }
}

- (void)handleTap:(UITapGestureRecognizer *)sender {
    //// NSLog(@"pullable handle tap");
    if ([sender state] == UIGestureRecognizerStateEnded) {
        [self setOpened:!opened animated:animate];
    }
}

- (void)setToggleOnTap:(BOOL)tap {
    toggleOnTap = tap;
    tapRecognizer.enabled = tap;
}

- (BOOL)toggleOnTap {
    return toggleOnTap;
}

- (void)setOpened:(BOOL)op animated:(BOOL)anim {
    if ([delegate respondsToSelector:@selector(pullableView:willChangeState:)]) {
        [delegate pullableView:self willChangeState:opened];
    }
    
    opened = op;
    
    if (anim) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:animationDuration];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    }
    
    if(opened) [self setYOffset:openedCenter.y];
    else [self setYOffset:closedCenter.y];
    self.handleView.center = opened ? CGPointMake(6+handleOpenedCenter.x+self.handleView.frame.size.width/2,handleOpenedCenter.y+self.handleView.frame.size.height/2) :  CGPointMake(6+handleClosedCenter.x+self.handleView.frame.size.width/2,handleClosedCenter.y+self.handleView.frame.size.height/2);
    //_handleTopConstraint.constant = opened ? handleOpenedCenter.y : handleClosedCenter.y;
    
    if ([delegate respondsToSelector:@selector(pullableViewAnimation:isOpening:)]) {
        [delegate pullableViewAnimation:self isOpening:opened];
    }
    
    if (anim) {
        
        // For the duration of the animation, no further interaction with the view is permitted
        dragRecognizer.enabled = NO;
        tapRecognizer.enabled = NO;
        
        [self.superview layoutIfNeeded];
        [self.handleView.superview layoutIfNeeded];
        [UIView commitAnimations];
        
    } else {
        
        if ([delegate respondsToSelector:@selector(pullableView:didChangeState:)]) {
            [delegate pullableView:self didChangeState:opened];
        }
    }
}
         
- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if (finished) {
        // Restores interaction after the animation is over
        dragRecognizer.enabled = YES;
        tapRecognizer.enabled = toggleOnTap;
        
        if ([delegate respondsToSelector:@selector(pullableView:didChangeState:)]) {
            [delegate pullableView:self didChangeState:opened];
        }
    }
}

-(void)setYOffset:(CGFloat)offset{
    _topConstraint.constant = offset;
    _bottomConstraint.constant = -offset;
}

-(void)setHidesHandle:(BOOL)hidesHandle{
    handleView.hidden = _hidesHandle = hidesHandle;
}

#pragma mark gesture recognizer

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

/*
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return NO;
}
*/

@end
