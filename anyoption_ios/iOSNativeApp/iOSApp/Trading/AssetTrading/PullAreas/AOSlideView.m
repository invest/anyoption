//
//  AOSlideView.m
//  iOSApp
//
//  Created by  on 5/26/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//
// Written only for closed by default and vertical(top:closed, bottom:opened) views for now!

#import "AOSlideView.h"

@implementation AOSlideView
{
    NSLayoutConstraint *lcHandleTop;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    
    if(self.handleView!=nil && self.pullableView != nil){
       
        if (CGPointEqualToPoint(self.handleViewClosedCenterPoint, CGPointZero) && CGPointEqualToPoint(self.handleViewOpenedCenterPoint, CGPointZero))
            [self setupOpenedClosedCenters];
            
        self.panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        self.panGestureRecognizer.delegate = self;
        self.panGestureRecognizer.minimumNumberOfTouches = 1;
        
        [self.handleView addGestureRecognizer:self.panGestureRecognizer];
        
        for (NSLayoutConstraint *c in self.handleView.superview.constraints)
        {
            if ((c.firstItem == self.handleView || c.secondItem == self.handleView) &&
                (c.firstAttribute == NSLayoutAttributeTop || c.secondAttribute == NSLayoutAttributeTop))
            {
                lcHandleTop = c;
                break;
            }
        }
    }
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self setSlideOpen:NO];
    self.clipsToBounds = YES;
}

- (void) setupOpenedClosedCenters {
    
    if(!self.isHorizontalView){
        
        self.handleViewClosedCenterPoint = self.handleView.center;
        self.handleViewOpenedCenterPoint = CGPointMake(self.handleView.center.x, self.handleViewClosedCenterPoint.y+self.frame.size.height);
        
        // NSLog(@"frame size height: %f", self.frame.size.height);

        
        //TODO: for reversed horizonal slide views
            
        
    }
    else {
        //TODO: horizontal inital setup
    }
    
    

}

- (void) p_moveHandleViewTo:(CGFloat)newY
{
    BOOL isIos8 = [AOUtils isIos8];
    if (isIos8)
    {
        lcHandleTop.constant = newY - CGRectGetHeight(self.handleView.bounds) / 2.0;
    }
    else
    {
        self.handleView.center = CGPointMake(self.handleView.center.x , newY );
    }
}

- (void) handlePan:(UIPanGestureRecognizer*)recognizer{
    
    CGPoint newPoint = [recognizer translationInView:self];
    [recognizer setTranslation:CGPointZero inView:recognizer.view];
    
    if (!self.isHorizontalView) {
        float newY = self.handleView.center.y+ newPoint.y;
        
        if (newY >= self.handleViewClosedCenterPoint.y && newY <= self.handleViewOpenedCenterPoint.y) {
            [self p_moveHandleViewTo:newY];
            self.pullableView.center = CGPointMake(self.pullableView.center.x, self.handleView.center.y - self.handleViewClosedCenterPoint.y - self.pullableView.frame.size.height/2 );
            self.pullableView.hidden=NO;
        }
    }
    else {
        //TODO: do it for horizontal views
    }
        
    if (recognizer.state == UIGestureRecognizerStateEnded)
    {
        if (!self.isHorizontalView) {
            if (self.pullableView.frame.origin.y + self.pullableView.frame.size.height > self.frame.size.height/2) {
                [self setSlideOpen:YES];
            }
            else {
               [self setSlideOpen:NO];
            }
        }
    }
}

- (void) setSlideOpen:(BOOL)open {
    // NSLog(@"pullable hidden: %@", [NSNumber numberWithBool:_pullableView.hidden]);
    
    if (_pullableView.hidden) {
        _pullableView.hidden = NO;
        [self showHandle:YES];
    }
    
    UIImageView* handleImageView = (UIImageView*)[self.handleView viewWithTag:6];
    
    handleImageView.highlighted = open;
    
    if (!open) {
        self.pullableView.center = CGPointMake(self.pullableView.center.x, -self.pullableView.frame.size.height/2 );
        [self p_moveHandleViewTo:self.handleViewClosedCenterPoint.y];
        self.pullableView.hidden=YES;
    }
    else {
        self.pullableView.center = CGPointMake(self.pullableView.center.x, self.frame.size.height - self.pullableView.frame.size.height/2 );
        [self p_moveHandleViewTo:self.handleViewOpenedCenterPoint.y];
        
        // NSLog(@"pullable view center y: %f", self.frame.size.height - self.pullableView.frame.size.height/2);
    }
   
    self.opened = open;
    self.userInteractionEnabled=open;
}

-(void)showHandle:(BOOL)show{
    self.handleView.hidden = !show;
}

@end
