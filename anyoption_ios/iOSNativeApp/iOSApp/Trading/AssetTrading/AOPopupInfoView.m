//
//  AOPopupInfoView.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 7/25/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOPopupInfoView.h"

#import "AOAssetDetailsViewController+AOPopupInfo.h"
#import "PlayAreaViewController+AOPopupInfo.h"

@implementation AOPopupInfoView
{
    AOAssetDetailsViewController * __weak _detailsVc;
}

static const CGFloat kPopupLabelMaxWidth = 110.0;

- (id) initWithDetailsViewController:(AOAssetDetailsViewController *)assetDetailsVc
{
    if ((self = [super initWithFrame:[MAINVC view].bounds]))
    {
        _detailsVc = assetDetailsVc;
        
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        
        [self p_positionSlideToAssetInfo];
        [self p_positionInvest];
        [self p_positionHelpInfo];
        [self p_positionSlideToTradingDetails];
        [self p_positionChangeAmount];
    }
    
    return self;
}

- (void) p_positionInvest
{
    if (!IS_IPAD)
    {
        [self p_positionTapToInvestCall];
        [self p_positionTapToInvestPut];
    }
    else
    {
        [self p_positionTapToInvestIpad];
    }
}

- (void) p_positionChangeAmount
{
    UIView *win = [MAINVC view];
    
    NSString *imageName = IS_IPAD ? @"Tap_to_change_amount_flipped" : @"Tap_to_change_amount";
    UIImage *image = [UIImage imageNamed:imageName];
    UIImageView *imgV = [[UIImageView alloc] initWithImage:image];
    UIView *vSlide = [_detailsVc.playArea vInvestRC];
    CGRect vSlideFrame = vSlide.frame;
    CGPoint pAnchor;
    if (!IS_IPAD)
    {
        pAnchor = CGPointMake(CGRectGetMaxX(vSlideFrame), CGRectGetMidY(vSlideFrame));
    }
    else
    {
        pAnchor = CGPointMake(CGRectGetMinX(vSlideFrame), CGRectGetMidY(vSlideFrame));
    }
    CGPoint winPoint = [win convertPoint:pAnchor fromView:vSlide.superview];
    
    CGRect r = imgV.frame;
    //    r.size = CGSizeMake(r.size.width * 0.5, r.size.height * 0.5);
    r.origin = CGPointMake(winPoint.x - 10.0, winPoint.y - r.size.height + 5.0);
    imgV.frame = r;
    [self addSubview:imgV];
    
    UILabel *lbl = [self p_infoLabelWithText:AOLocalizedString(@"overlay_amount")];
    lbl.center = CGPointMake(CGRectGetMidX(imgV.frame), CGRectGetMinY(imgV.frame) - lbl.bounds.size.height / 2.0);
    [self addSubview:lbl];
    [self p_fixLabelPosition:lbl];
}

- (void) p_positionSlideToTradingDetails
{
    if (IS_IPAD)
    {
        return;
    }
    
    UIView *win = [MAINVC view];
    
    UIImage *image = [UIImage imageNamed:@"Slide_to_open_trading_details"];
    UIImageView *imgV = [[UIImageView alloc] initWithImage:image];
    UIView *vSlide = [_detailsVc.playArea vSlideHandle];
    CGRect vSlideFrame = vSlide.frame;
    CGPoint pAnchor = CGPointMake(CGRectGetMidX(vSlideFrame), CGRectGetMidY(vSlideFrame));
    CGPoint winPoint = [win convertPoint:pAnchor fromView:vSlide.superview];
    
    CGRect r = imgV.frame;
    //    r.size = CGSizeMake(r.size.width * 0.5, r.size.height * 0.5);
    r.origin = CGPointMake(winPoint.x - r.size.width, winPoint.y - r.size.height - 5.0);
    imgV.frame = r;
    [self addSubview:imgV];
    
    UILabel *lbl = [self p_infoLabelWithText:AOLocalizedString(@"overlay_slide")];
    lbl.center = CGPointMake(CGRectGetMidX(imgV.frame), imgV.frame.origin.y - lbl.bounds.size.height / 2.0);
    [self addSubview:lbl];
    [self p_fixLabelPosition:lbl];
}

- (void) p_positionHelpInfo
{
    if (IS_IPAD)
    {
        // currently there's no help info button on iPad
        return;
    }
    
    UIView *win = [MAINVC view];
    
    UIImage *image = [UIImage imageNamed:@"Tap_to_page_info"];
    UIImageView *imgV = [[UIImageView alloc] initWithImage:image];
    UIView *vHelpInfo = [_detailsVc helpButton];
    CGRect vHelpInfoFrame = vHelpInfo.frame;
    CGPoint pAnchor = CGPointMake(CGRectGetMidX(vHelpInfoFrame), CGRectGetMaxY(vHelpInfoFrame));
    CGPoint winPoint = [win convertPoint:pAnchor fromView:vHelpInfo.superview];
    
    CGRect r = imgV.frame;
    //    r.size = CGSizeMake(r.size.width * 0.5, r.size.height * 0.5);
    r.origin = CGPointMake(winPoint.x - r.size.width, winPoint.y);
    imgV.frame = r;
    [self addSubview:imgV];
    
    UILabel *lbl = [self p_infoLabelWithText:AOLocalizedString(@"overlay_page_info")];
    lbl.center = CGPointMake(CGRectGetMinX(imgV.frame) - lbl.bounds.size.width / 2.0 + 30.0, CGRectGetMaxY(imgV.frame) + lbl.bounds.size.height / 2.0);
    [self addSubview:lbl];
    [self p_fixLabelPosition:lbl];
}

- (void) p_positionTapToInvestIpad
{
    UIView *win = [MAINVC view];
    
    UILabel *lbl = [self p_infoLabelWithText:AOLocalizedString(@"overlay_invest")];
    UIView *vCall = [_detailsVc.playArea vCall];
    UIView *vPut = [_detailsVc.playArea vPut];
    
    CGPoint pCallAnchor = CGPointMake(CGRectGetMinX(vCall.frame), CGRectGetMidY(vCall.frame));
    CGPoint pPutAnchor = CGPointMake(CGRectGetMinX(vPut.frame), CGRectGetMidY(vPut.frame));
    CGPoint winCall = [win convertPoint:pCallAnchor fromView:vCall.superview];
    CGPoint winPut = [win convertPoint:pPutAnchor fromView:vPut.superview];
    
    UIImage *upImage = [UIImage imageNamed:@"Tap_to_invest_iPad_up"];
    UIImageView *upImgView = [[UIImageView alloc] initWithImage:upImage];
    
    CGRect lblFrame = lbl.frame;
    lblFrame.origin = CGPointMake(winCall.x - CGRectGetWidth(upImgView.bounds) - CGRectGetWidth(lblFrame) / 2.0,
                                  winCall.y + ((winPut.y - winCall.y) - CGRectGetHeight(lblFrame)) / 2.0);
    lbl.frame = lblFrame;
    [self addSubview:lbl];
    
    CGRect upImgViewFrame = upImgView.frame;
    upImgViewFrame.origin = CGPointMake(CGRectGetMidX(lblFrame), CGRectGetMinY(lblFrame) - upImgViewFrame.size.height);
    upImgView.frame = upImgViewFrame;
    [self addSubview:upImgView];
    
    UIImage *downImage = [UIImage imageNamed:@"Tap_to_invest_iPad_down"];
    UIImageView *downImageView = [[UIImageView alloc] initWithImage:downImage];
    CGRect downImgViewFrame = downImageView.frame;
    downImgViewFrame.origin = CGPointMake(CGRectGetMidX(lblFrame), CGRectGetMaxY(lblFrame));
    downImageView.frame = downImgViewFrame;
    [self addSubview:downImageView];
}

- (void) p_positionTapToInvestCall
{
    UIView *win = [MAINVC view];
    
    UIImage *image = [UIImage imageNamed:@"Tap_to_Invest"];
    UIImageView *imgV = [[UIImageView alloc] initWithImage:image];
    UIView *vCall = [_detailsVc.playArea vCall];
    CGRect callBtnFrame = vCall.frame;
    CGPoint pAnchor = CGPointMake(CGRectGetMidX(callBtnFrame), CGRectGetMinY(callBtnFrame));
    CGPoint winPoint = [win convertPoint:pAnchor fromView:vCall.superview];
    
    CGRect r = imgV.frame;
    //    r.size = CGSizeMake(r.size.width * 0.5, r.size.height * 0.5);
    r.origin = CGPointMake(winPoint.x - r.size.width, winPoint.y - r.size.height + 20.0);
    imgV.frame = r;
    [self addSubview:imgV];
    
    UILabel *lbl = [self p_infoLabelWithText:AOLocalizedString(@"overlay_invest")];
    lbl.center = CGPointMake(CGRectGetMidX(imgV.frame), CGRectGetMinY(imgV.frame) - CGRectGetHeight(lbl.bounds) / 2.0);
    [self addSubview:lbl];
    [self p_fixLabelPosition:lbl];
}

- (void) p_positionTapToInvestPut
{
    UIView *win = [MAINVC view];
    
    UIImage *image = [UIImage imageNamed:@"Tap_to_Invest_2"];
    UIImageView *imgV = [[UIImageView alloc] initWithImage:image];
    UIView *vPut = [_detailsVc.playArea vPut];
    CGRect putBtnFrame = vPut.frame;
    CGPoint pAnchor = CGPointMake(CGRectGetMidX(putBtnFrame), CGRectGetMinY(putBtnFrame));
    CGPoint winPoint = [win convertPoint:pAnchor fromView:vPut.superview];
    
    CGRect r = imgV.frame;
    //    r.size = CGSizeMake(r.size.width * 0.5, r.size.height * 0.5);
    r.origin = CGPointMake(winPoint.x, winPoint.y - r.size.height + 20.0);
    imgV.frame = r;
    [self addSubview:imgV];
    
    UILabel *lbl = [self p_infoLabelWithText:AOLocalizedString(@"overlay_invest")];
    lbl.center = CGPointMake(CGRectGetMidX(imgV.frame), CGRectGetMinY(imgV.frame) - CGRectGetHeight(lbl.bounds) / 2.0);
    [self addSubview:lbl];
    [self p_fixLabelPosition:lbl];
}

- (void) p_positionSlideToAssetInfo
{
    UIView *win = [MAINVC view];
    
    UIImage *image = [UIImage imageNamed:@"Slide_to_asset_info"];
    UIImageView *imgV = [[UIImageView alloc] initWithImage:image];
    UIView *handleView = _detailsVc.assetInfoHandleView;
    CGPoint pAnchor = CGPointMake(CGRectGetMidX(handleView.frame), CGRectGetMaxY(handleView.frame));
    CGPoint winPoint = [win convertPoint:pAnchor fromView:handleView.superview];
    
    CGRect r = imgV.frame;
    r.origin = CGPointMake(winPoint.x, winPoint.y);
    //    r.size = CGSizeMake(r.size.width * 0.5, r.size.height * 0.5);
    imgV.frame = r;
    [self addSubview:imgV];
    
    UILabel *lbl = [self p_infoLabelWithText:AOLocalizedString(@"overlay_asset_info")];
    CGRect lblFrame = lbl.frame;
    lblFrame.origin = CGPointMake(CGRectGetMaxX(imgV.frame), CGRectGetMidY(imgV.frame));
    lbl.frame = lblFrame;
    [self addSubview:lbl];
    [self p_fixLabelPosition:lbl];
}

- (UILabel *) p_infoLabelWithText:(NSString *)text
{
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, kPopupLabelMaxWidth, 30.0)];
    lbl.lineBreakMode = NSLineBreakByWordWrapping;
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor whiteColor];
    lbl.numberOfLines = 0;
    lbl.font = [UIFont fontWithName:APP_FONT_BOLD size:16.0];
    lbl.text = text;
    [lbl sizeToFit];
    return lbl;
}

- (void) p_fixLabelPosition:(UILabel *)label
{
    CGRect r = label.frame;
    r.origin.x = MIN(MAX(5.0, r.origin.x), label.superview.bounds.size.width - label.bounds.size.width - 5.0);
    r.origin.y = MIN(MAX(5.0, r.origin.y), label.superview.bounds.size.height - label.bounds.size.height - 5.0);
    label.frame = r;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self removeFromSuperview];
}

- (void) show
{
    [[MAINVC view] addSubview:self];
}

@end
