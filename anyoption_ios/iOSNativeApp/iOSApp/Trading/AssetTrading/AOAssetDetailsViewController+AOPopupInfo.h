//
//  AOAssetDetailsViewController+AOPopupInfo.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 7/25/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOAssetDetailsViewController.h"

@class PlayAreaViewController;

@interface AOAssetDetailsViewController (AOPopupInfo)

- (PlayAreaViewController *) playArea;
- (UIView *) assetInfoHandleView;

@end
