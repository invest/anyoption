//
//  AOPopupInfoView.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 7/25/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AOAssetDetailsViewController;

@interface AOPopupInfoView : UIView

- (id) initWithDetailsViewController:(AOAssetDetailsViewController *)assetDetailsVc;
- (void) show;

@end
