//
//  AOInvestmentInfo.m
//  iOSApp
//
//  Created by mac_user on 12/16/13.
//  Copyright (c) 2013 Anyoption. All rights reserved.
//

#import "AOInvestmentInfoViewController.h"
#import "UILabel+SSText.h"
#import "AOInvestment.h"
#import "UIImage+Utils.h"
#import "AOUser.h"


@implementation AOInvestmentInfoViewController
{
    BOOL _isWaiting;
}

-(void)dealloc{
    // NSLog(@"AOInvestmentInfo dealloc");
}


-(void)viewDidLoad{
    [super viewDidLoad];
        
    openOptionsImageConstraintInitialValue = openOptionsImageConstraint.constant;
    levelImageConstraintInitialValue = levelImageConstraint.constant;
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    for(UIView *separator in investmentInfoSeparators){
        [AOUtils replaceConstraintWithAttribute:NSLayoutAttributeWidth withConstant:0.5 onView:separator];
        separator.backgroundColor = [theme investmentInfoBarSeparatorColor];
    }
    
    assetPointerImageInitialFrame = assetPointerImage.frame;
    
    UIColor *infoColor = [theme investmentInfoTextColor];
    UIFont *infoCaptionFont = [theme investmentInfoCaptionFont];
    UIFont *infoValueFont = [theme investmentInfoValueFont];
    
    for (UILabel *captionLabel in @[openOptionsTitleLabel, currentLevelTitleLabel, investedAmountTitleLabel, expectedReturnTitleLabel])
    {
        captionLabel.textColor = infoColor;
        captionLabel.font = infoCaptionFont;
    }
    
    for (UILabel *valueLabel in @[openOptionsLabel, currentLevelLabel, investedAmountLabel, expectedReturnLabel])
    {
        valueLabel.textColor = infoColor;
        valueLabel.font = infoValueFont;
    }
    
    self.view.backgroundColor = [theme investmentInfoBarBackgroundColor];
    
    currentLevelLabelConstraintDefaultConstant = currentLevelLabelConstraint.constant;
    
    [self refreshInterface];
    
    self.getQuotesBtn.hidden=YES;
    [self.getQuotesBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 2.0, 0.0, 21.0)];
    self.getQuotesBtn.titleLabel.font = [theme investmentInfoGetQuoteFont];
    self.getQuotesBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self.getQuotesBtn setTitleColor:[theme investmentInfoGetQuoteTextColor] forState:UIControlStateNormal];
    [self.getQuotesBtn setTitle:AOLocalizedString(@"optionPlus.getQuote") forState:UIControlStateNormal];
    
    [self.getQuotesBtn setBackgroundImage:[UIImage imageNamed:@"get_quote_btn_show"] forState:UIControlStateNormal];
    
    self.optionType = self.optionType; //override setter to arrange the oaded UI elements
    
    if (IS_RIGHT_TO_LEFT)
    {
        badgeImage.image = [[UIImage imageNamed:@"misc_gold-badge"] flippedImage];
    }
}

-(void)setOpenOptions:(NSInteger)openOptions{
    _openOptions = openOptions;
    [self setOpenOptionsText];
    [self p_updateQuoteInfo];
}
-(void)setCurrentLevel:(float)currentLevel{
    
    _currentLevel = currentLevel;
    if(!_isShowingInvestment){
        [self setCurrentLevelText];
    }
    else {
        currentInvestment.level = [NSString stringWithFormat:@"%f",_currentLevel];
        [self updateInvestmentLevelImage];
        
        [expectedReturnLabel setText:[AOUtils formatCurrencyAmountWithoutSymbol:[AOUtils divideCurrencyAmount:[currentInvestment returnAtLevel:_currentLevel]]]];
    }
    [self refreshInterface];
    
}
-(void)setInvestedAmount:(NSString *)investedAmount{
    _investedAmount = investedAmount;
    if(!_isShowingInvestment) [self setInvestedText];
}
-(void)setExpectedReturn:(NSString *)expectedReturn{
    _expectedReturn = expectedReturn;
    if(!_isShowingInvestment) [self setExpectedReturnText];
}

-(void)setOpenOptionsTitle:(NSString*)text{
    openOptionsTitleLabel.text = text;
}
-(void)setCurrentLevelTitle:(NSString*)text{
    currentLevelTitleLabel.text = text;
}
-(void)setInvestedTitle:(NSString*)text{
    investedAmountTitleLabel.text = text;
}
-(void)setExpectedReturnTitle:(NSString*)text{
    expectedReturnTitleLabel.text = text;
}

-(void)setOpenOptionsText{
    if (currentInvestment == nil)
    {
        [self resetOpenOptionsImage];
        openOptionsImageConstraint.constant = [openOptionsLabel offsetFromCenter]-4;
        if (_openOptions > 0)
        {
            if([DLG user].l_id) { /* CORE-2206 */
                openOptionsLabel.text =[NSString stringWithFormat:@"%ld", (long)_openOptions];
                openOptionsImage.hidden = NO;
            } else {
                openOptionsLabel.text =[NSString stringWithFormat:@"0"];
                openOptionsImage.hidden = YES;
            }
        } else {
            openOptionsLabel.text =[NSString stringWithFormat:@"0"];
            openOptionsImage.hidden = YES;
        }
    }
}
-(void)setCurrentLevelText{
    currentLevelLabel.text = [AOUtils formattedLevelWithLevel:_currentLevel andDecimalPoint:self.market.decimalPoint];
}
-(void)setInvestedText{
    //[investedAmountLabel setSSText:_investedAmount withOptions:nil];
    [investedAmountLabel setText:_investedAmount];
}
-(void)setExpectedReturnText{
    //[expectedReturnLabel setSSText:_expectedReturn withOptions:nil];
    
    if ([_investedAmount floatValue] < [_expectedReturn floatValue]) {
         badgeImage.hidden = NO;
    }
    else {
        badgeImage.hidden = YES;
    }
    [expectedReturnLabel setText:_expectedReturn];
}

-(void)setInvestment:(AOInvestment *)investment{
    
    currentInvestment = [investment copy];
    
    _isShowingInvestment = YES;
    currentLevelLabelConstraint.constant = currentLevelLabelConstraintDefaultConstant-4;
    
    [self setOpenOptionsTitle:AOLocalizedString(@"time")];
    [self setCurrentLevelTitle:AOLocalizedString(@"investments.level")];
    [self setInvestedTitle:AOLocalizedString(@"invested")];
    [self setExpectedReturnTitle:AOLocalizedString(@"expectedReturn")];
    
    [self setOpenOptionsTextWithTime:[currentInvestment getPurchaseTimeFormatted]];
    [self setCurrentLevelTextWithInvestmentLevel:currentInvestment.currentLevel];
    [investedAmountLabel setText:[AOUtils formatCurrencyAmountWithoutSymbol:[AOUtils divideCurrencyAmount:(double)investment.amount]]];
   
    if([investment hasRibbon]) {
        badgeImage.hidden = NO;
    }
    else {
        badgeImage.hidden = YES;
    }
    
    [self updateInvestmentLevelImage];
    [self setCurrentLevel:_currentLevel];
}
-(void)updateInvestmentLevelImage{
    levelImage.transform = CGAffineTransformIdentity;
    if(currentInvestment.typeId==AOInvestmentTypePut) levelImage.transform = CGAffineTransformConcat(CGAffineTransformMakeScale(1, -1),CGAffineTransformMakeTranslation(0,18));
   
    levelImage.image = [UIImage imageNamed:@"chart_investmentArrowLose.png"];
    
    if([currentInvestment hasRibbon]){
        levelImage.image = [UIImage imageNamed:@"chart_investmentArrowWin.png"];
    }
}
-(void)removeInvestment{
    _isShowingInvestment = NO;
    currentLevelLabelConstraint.constant = currentLevelLabelConstraintDefaultConstant;
    
    [self setOpenOptionsTitle:AOLocalizedString(@"investments.open")];
    [self setCurrentLevelTitle:AOLocalizedString(@"profitLine.currentLevel")];
    [self setInvestedTitle:AOLocalizedString(@"invested")];
    [self setExpectedReturnTitle:AOLocalizedString(@"expectedReturn")];
    
    currentInvestment = nil;
    
    [self refreshInterface];
}

-(void)refreshInterface{
    openOptionsImage.hidden = YES;
    levelImage.hidden = YES;
    badgeImage.hidden = YES;
    
    if (!_isShowingInvestment)
    {
        assetPointerImage.hidden = YES;
        assetPointerImage.frame = assetPointerImageInitialFrame;
    }
    
    [self setOpenOptionsText];
    
    if(!_isShowingInvestment) [self setCurrentLevelText];
    [self setInvestedText];
    [self setExpectedReturnText];
}

-(void)setOpenOptionsTextWithTime:(NSString*)text{
    [self resetOpenOptionsImage];
    openOptionsLabel.text = text;
    [openOptionsLabel setSSText:text withOptions:@{@"ssOffset":@0,@"separator":@" "}];
}

-(void)setCurrentLevelTextWithInvestmentLevel:(float)level{
    [self resetLevelImage];
    currentLevelLabel.text = [AOUtils formattedLevelWithLevel:level andDecimalPoint:self.market.decimalPoint];
    levelImageConstraint.constant = [currentLevelLabel offsetFromCenter]-5;
    levelImage.hidden = NO;
}

-(void)resetOpenOptionsImage{
    openOptionsImage.hidden = YES;
    openOptionsImageConstraint.constant = openOptionsImageConstraintInitialValue;
}
-(void)resetLevelImage{
    levelImage.hidden = YES;
    levelImageConstraint.constant = levelImageConstraintInitialValue;
}

-(void)setPointerOffset:(CGFloat)offset{
    assetPointerImage.frame = assetPointerImageInitialFrame;
    assetPointerImage.hidden = NO;
    assetPointerImage.center = CGPointMake(offset, assetPointerImage.center.y);
}

- (void) p_updateQuoteInfo
{
    BOOL quotesVisible = (_optionType == AOOpportunityTypeOptionPlus &&
                          !_isWaiting &&
                          self.openOptions > 0);
    
    [self p_setQuotesButtonHidden:!quotesVisible];
}

- (void) p_setQuotesButtonHidden:(BOOL)hidden
{
    self.getQuotesBtn.hidden = IS_IPAD?YES:hidden;
    expectedReturnLabel.hidden = IS_IPAD?NO:!hidden;
    expectedReturnTitleLabel.hidden = IS_IPAD?NO:!hidden;
}

- (void) isWaiting:(BOOL)isWaiting
{
    _isWaiting = isWaiting;
    
    [self p_updateQuoteInfo];
}

-(void)setOptionType:(AOOpportunityType)oType{
    _optionType = oType;
    
    [self p_updateQuoteInfo];
}

@end
