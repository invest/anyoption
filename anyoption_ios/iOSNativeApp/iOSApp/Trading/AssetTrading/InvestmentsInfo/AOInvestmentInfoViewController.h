//
//  AOInvestmentInfo.h
//  iOSApp
//
//  Created by mac_user on 12/16/13.
//  Copyright (c) 2013 Anyoption. All rights reserved.
//

#import "AOCurrencyLabel.h"
#import "AOOpportunity.h"
#import "AOMarket.h"
@class AOInvestment;

@interface AOInvestmentInfoViewController : UIViewController {
    __weak IBOutlet UIImageView *assetPointerImage;
    CGRect assetPointerImageInitialFrame;
    
    __weak IBOutlet UIImageView *openOptionsImage;
    __weak IBOutlet NSLayoutConstraint *openOptionsImageConstraint;
    CGFloat openOptionsImageConstraintInitialValue;
    
    __weak IBOutlet UIImageView *levelImage;
    __weak IBOutlet NSLayoutConstraint *levelImageConstraint;
    CGFloat levelImageConstraintInitialValue;
    
    __weak IBOutlet UILabel *openOptionsTitleLabel;
    __weak IBOutlet UILabel *openOptionsLabel;
    __weak IBOutlet UILabel *currentLevelTitleLabel;
    __weak IBOutlet UILabel *currentLevelLabel;
    __weak IBOutlet NSLayoutConstraint *currentLevelLabelConstraint;
    CGFloat currentLevelLabelConstraintDefaultConstant;
    __weak IBOutlet UILabel *investedAmountTitleLabel;
    __weak IBOutlet UILabel *investedAmountLabel;
    __weak IBOutlet UILabel *expectedReturnTitleLabel;
    __weak IBOutlet AOCurrencyLabel *expectedReturnLabel;
    
    __weak IBOutlet UIImageView *badgeImage;
    IBOutletCollection(UIView) NSMutableArray *investmentInfoSeparators;
    
    AOInvestment *currentInvestment;
    
    
    
    
}

@property (nonatomic) NSInteger openOptions;

@property (nonatomic) float currentLevel;
@property (nonatomic,copy) NSString *investedAmount;
@property (nonatomic,copy) NSString *expectedReturn;
@property (nonatomic,assign) BOOL isShowingInvestment;
@property (nonatomic, weak) IBOutlet UIButton *getQuotesBtn;
@property (nonatomic, assign) AOOpportunityType optionType;
@property (nonatomic, weak) AOMarket* market;


-(void)setInvestment:(AOInvestment*)investment;
-(void)removeInvestment;
-(void)setPointerOffset:(CGFloat)offset;

- (void) isWaiting:(BOOL)isWaiting;

@end
