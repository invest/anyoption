//
//  AOTradeAssetChartView.m (MINICHART)
//  iOSApp
//
//  Created by  on 4/16/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOTradeAssetChartView.h"
#import "LightstreamerClient.h"
#import "NSString+Utils.h"
#import "AOUtils.h"
#import "AOConstants.h"



@implementation AOTradeAssetChartView{
    UIImageView *assetBgView;
    LSExtendedTableInfo* lsTableInfo;
    UIView* titleView;
    
    NSString* marketName;
    NSString* marketProfit;
    NSString* marketExpires;
    

    NSTimeInterval timeLeftInterval;
    NSTimer* expiryTimer;
    
    
}

-(id)init{
    
    self = [super init];
    if(self){
        marketName=@"";
        marketProfit=@"";
        marketExpires=@"";
        
        
    }
    
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    
    if (titleView==nil) {
        id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
        titleView = [UIView new];
        [titleView setBackgroundColor:[theme chartTitleBackgroundColor]];
        [titleView setFrame:CGRectMake(0.0, 0.0, self.bounds.size.width, 20.0)];
        [self addSubview:titleView];
        
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectInset(titleView.bounds, 5.0, 0.0)];
        titleLabel.textColor = ATO_HEAD_TEXT_COLOR;
        titleLabel.font = [[AOThemeController sharedInstance].theme tradeAssetChartViewFeaturedFont];
        titleLabel.tag = 99;
        
        if (IS_RIGHT_TO_LEFT)
        {
            titleLabel.textAlignment = NSTextAlignmentRight;
        }
        
        [titleView addSubview:titleLabel];
    }
}

-(void)setChartData:(AOChartData *)data{
    [super setChartData:data];
  
    _market=[AOMarket marketWithChartData:data];
    
    timeLeftInterval = [[data timeLastInvestDate] timeIntervalSinceDate:[AOUtils currentDatePrecise]];
    expiryTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(calculateTimeLeft) userInfo:nil repeats:YES];
    
    if (assetBgView==nil) {
        assetBgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"market_unavailable"]];
        
#ifdef ETRADER
        marketName = [NSString stringWithFormat:@"%@   ", AOLocalizedString([data marketName])];
#else
        marketName = [NSString stringWithFormat:@"%@: %@   ",AOLocalizedString(@"featured"),AOLocalizedString([data marketName])];
#endif
        
        [self addSubview:assetBgView];
        [self sendSubviewToBack:assetBgView];
    }
    
    if(!lsTableInfo){
        NSArray *items = [[NSArray alloc] initWithObjects: [NSString stringWithFormat:@"aotps_1_%li", self.marketId ], nil];
        // NSLog(@"items: %@", items);
        
        lsTableInfo = [LSExtendedTableInfo extendedTableInfoWithItems:items mode:LSModeCommand fields:RESIDENT.lsConnectionHandler.fields dataAdapter:LS_DATA_ADAPTER snapshot:YES];
        lsTableInfo.requestedMaxFrequency= 1.0;
        [RESIDENT.lsConnectionHandler addTableWithTableInfo:lsTableInfo delegate:self];
    }
    
    [self refreshChartTitle:YES];
    
    
}

#pragma mark-
#pragma mark LS

-(void)table:(LSSubscribedTableKey *)tableKey itemPosition:(int)itemPosition itemName:(NSString *)itemName didUpdateWithInfo:(LSUpdateInfo *)updateInfo{

    dispatch_async(dispatch_get_main_queue(), ^{
        if ([updateInfo isChangedValueOfFieldName:@"ET_ODDS"]){
            marketProfit = [updateInfo currentValueOfFieldName:@"ET_ODDS"];
            [self refreshChartTitle:YES];
        }
        
        [self.chart lsUpdateWithItemPosition:itemPosition itemName:itemName didUpdateWithInfo:updateInfo];
        
        UIImage *backImage = [AOUtils backgroundForMarket:self.marketId];
        if (backImage)
        {
            assetBgView.image = backImage;
        }
        
        if ([updateInfo isChangedValueOfFieldName:@"ET_STATE"]) {
            [self updateStateWithInfo:updateInfo];
            
            int nState = [[updateInfo currentValueOfFieldName:@"ET_STATE"] intValue];
            
            if (nState == AOOpportunityStateSuspended || nState == AOOpportunityStateCreated || nState == AOOpportunityStatePaused)  {
                titleView.hidden = YES;
                
                float paddingTop = 45.0;
                float paddingLeft = 25.0;
                UILabel *unavailableAssetLabel = [[UILabel alloc] initWithFrame:CGRectMake(paddingLeft, paddingTop, self.bounds.size.width-2*paddingLeft, self.bounds.size.height-paddingTop)];
                unavailableAssetLabel.font = [UIFont fontWithName:APP_FONT size:17];
                unavailableAssetLabel.textColor = [UIColor whiteColor];
                unavailableAssetLabel.text = AOLocalizedString(@"CMS.offHours_tradeBox.text.first_row");
                unavailableAssetLabel.textAlignment = NSTextAlignmentCenter;
                unavailableAssetLabel.numberOfLines = 0;
                [unavailableAssetLabel sizeToFit];
                [self addSubview:unavailableAssetLabel];
            }
        }
    });
}

-(void)refreshChartTitle:(BOOL)showTimer {
    [(UILabel*)[self viewWithTag:99] setText:[NSString stringWithFormat:@"%@ %@ %@",marketName,
                                              ![marketProfit isEmpty]?[NSString stringWithFormat:@"%@: %@",AOLocalizedString(@"binary0100.rightTable.profit"), marketProfit]:@"" ,
                                              showTimer?![marketExpires isEmpty]?[NSString stringWithFormat:@"   %@ %@", AOLocalizedString(@"expire.level.expires"), marketExpires]:@"":@""
                                             ]];
}

-(void)calculateTimeLeft{
    timeLeftInterval--;
    
    if (timeLeftInterval<0) {
       [self refreshChartTitle:NO];
       [expiryTimer invalidate];
    }
    
    if (timeLeftInterval<0) {
        [expiryTimer invalidate];
        expiryTimer = nil;
    }
    else {
        int minutes = floor(timeLeftInterval/60);
        NSString *minutes2 = [NSString stringWithFormat:@"%02d", minutes];
        int seconds = round(timeLeftInterval - minutes * 60);
        NSString *seconds2 = [NSString stringWithFormat:@"%02d", seconds];

        marketExpires = [NSString stringWithFormat:@"%@:%@",minutes2,seconds2];
        [self refreshChartTitle:YES];
    }
}
-(void)removeLsTable{
   
    if (lsTableInfo!=nil)
        [RESIDENT.lsConnectionHandler removeTableWithTableInfo:lsTableInfo];
    
    
   
}
-(void)dealloc{
  
    if (lsTableInfo!=nil)
        [RESIDENT.lsConnectionHandler removeTableWithTableInfo:lsTableInfo];
   
}
@end