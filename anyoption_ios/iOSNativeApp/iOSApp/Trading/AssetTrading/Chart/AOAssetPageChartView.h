//
//  AOAssetPageChartView.h
//  JSON Test
//
//  Created by Anton Antonov on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#define MAX_ZOOM 8

#import "AOBaseChartView.h"

@protocol AOAssetPageChartViewDelegate <NSObject>
- (void)showInvestmentDetails:(AOInvestment*)inv screenPoint:(CGPoint)point;
- (void)beganDraggingChartView;
- (void)endedDraggingChartView;
@end

@interface AOAssetPageChartView : AOBaseChartView <UIGestureRecognizerDelegate,AOChartViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *timeLeftToInvestLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLeftToInvest;
@property (strong, nonatomic) IBOutlet UIProgressView *timeLeftToInvestProgress;
@property (strong, nonatomic) NSTimer *timeLeftToInvestTimer;

@property (weak,nonatomic) UIButton *button;

@property (weak, nonatomic) id delegate;


@property (nonatomic) BOOL inInvestmentMode;


-(void) zoomIn: (float)coef;
-(void) zoomOut: (float)coef;

-(void) addNewInvestment: (AOInvestment*)inv;

@end
