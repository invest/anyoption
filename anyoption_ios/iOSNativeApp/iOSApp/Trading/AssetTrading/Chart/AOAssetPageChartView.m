//
//  AOAssetPageChartView.m
//  JSON Test
//
//  Created by Anton Antonov on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AOAssetPageChartView.h"



@implementation AOAssetPageChartView{
   float pinchWidth;
    UIPinchGestureRecognizer *pinchRecognizer;
    UIImageView *currentLevelArrowImageView;
    UILabel* currentLevelArrowLabel;
    UIActivityIndicatorView* loadingView;
}

@synthesize timeLeftToInvestLabel;
@synthesize timeLeftToInvest;
@synthesize timeLeftToInvestProgress;
@synthesize timeLeftToInvestTimer;
@synthesize inInvestmentMode=_inInvestmentMode;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    // NSLog(@"AOAssetPageChartView.initWithCoder");
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        pinchWidth=-1;
        pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchChart:)];
        [self addGestureRecognizer:pinchRecognizer];
        [pinchRecognizer setDelegate:self];
        
        currentLevelArrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"current_level_background"]];
        currentLevelArrowLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, currentLevelArrowImageView.frame.size.width-5, currentLevelArrowImageView.frame.size.height)];
        currentLevelArrowLabel.font = CHART_CURRENT_LEVEL_ARROW_LABEL_FONT;
        currentLevelArrowLabel.textColor = CHART_CURRENT_LEVEL_ARROW_TEXT_COLOR;
        currentLevelArrowLabel.tag = 333;
        currentLevelArrowLabel.textAlignment = NSTextAlignmentCenter;
        currentLevelArrowLabel.adjustsFontSizeToFitWidth = YES;
        [currentLevelArrowImageView addSubview:currentLevelArrowLabel];
        currentLevelArrowImageView.hidden = YES;
        [self addSubview:currentLevelArrowImageView];
        
        loadingView = [UIActivityIndicatorView new];
        loadingView.frame = CGRectMake(self.frame.size.width/2-10,self.frame.size.height/2-10,20,20);
        [self addSubview:loadingView];
        
    }
    return self;
}

-(void)setChartData:(AOChartData *)data{
    [super setChartData:data];
    self.chart.delegate = self;
}

-(void)pinchChart:(UIPinchGestureRecognizer *)pRecognizer
{
    
        if ([pRecognizer state] == UIGestureRecognizerStateBegan || [pRecognizer state] == UIGestureRecognizerStateChanged) {
            
            if ([pRecognizer numberOfTouches] > 1) {
                
                UIView *theView = [pRecognizer view];
                
                //CGPoint locationOne = [pinchRecognizer locationOfTouch:0 inView:theView];
                //CGPoint locationTwo = [pinchRecognizer locationOfTouch:1 inView:theView];
               
                float newPinchWidth = [pRecognizer locationOfTouch:1 inView:theView].x - [pRecognizer locationOfTouch:0 inView:theView].x;
                
                if(pinchWidth==-1){
                    pinchWidth = newPinchWidth;
                    return;
                }
                    
                if(pinchWidth < newPinchWidth )
                    [self zoomIn:0.2];
                else
                    [self zoomOut:0.2];
                
                pinchWidth = newPinchWidth;
               
            }  // if numberOfTouches
        }  // StateBegan if
        
        if ([pinchRecognizer state] == UIGestureRecognizerStateEnded || [pinchRecognizer state] == UIGestureRecognizerStateCancelled) {
            // NSLog(@"StateEnded StateCancelled");
           // [scalableView setBackgroundColor:[UIColor whiteColor]];
            //arrows.hidden = TRUE;
            //blocked.hidden = TRUE;
        }
    
}

- (void) updateTimeLeftToInvest
{
    NSDate *now = [[NSDate alloc] initWithTimeInterval:0 sinceDate:[AOUtils currentDatePrecise]];
    int timeLeft = [self.chart.timeLastInvest timeIntervalSinceDate:now];

    if (timeLeft < 0) {
        [timeLeftToInvestTimer invalidate];
        timeLeftToInvestTimer = nil;
        return;
    }
    self.timeLeftToInvest.text = [NSString stringWithFormat:@"%1$02d:%2$02d", timeLeft / 60, timeLeft % 60];
    [self.timeLeftToInvestProgress setProgress:(600.0f - timeLeft) / 600.0f];
}

-(void) zoomIn:(float)coef {
    
    if(!self.inInvestmentMode) {
    
        if(self.chart.zoomCoef+coef<=MAX_ZOOM)
        {
            self.chart.zoomCoef += coef;
        }
        else {
            self.chart.zoomCoef=MAX_ZOOM;
        }
    }
}

- (void) zoomOut: (float) coef {
    if(!self.inInvestmentMode) {
        float newCoef = self.chart.zoomCoef - coef;
        if(newCoef < 1) {
            [self.chart scrollBack];
         newCoef = 1;
        }
        self.chart.zoomCoef = newCoef;
    }
}

- (IBAction) zoomChart:(id) sender {
    
    if(!self.inInvestmentMode) {
        if([(UIButton*)sender tag]==1 || [(UIButton*)sender tag]==71 )
            [self zoomOut:0.5];
        else
            [self zoomIn:0.5];
    }
}

- (void) setInInvestmentMode: (BOOL) im {
   
    if(!im) {
        [self addGestureRecognizer:pinchRecognizer];
        [self.chart unselectInvestmentButtons];
    }
    else
        [self removeGestureRecognizer:pinchRecognizer];
        
    _inInvestmentMode = im;
}

#pragma mark -
#pragma AOChartView Delegate methods
- (void) didTouchInvestment:(AOInvestment*)inv screenPoint:(CGPoint)point
{
    [self setInInvestmentMode:YES];
    [self.delegate showInvestmentDetails:inv screenPoint:point];
}

- (void) loadingChart:(BOOL)isloading{
    self.chart.userInteractionEnabled=!isloading;
    if (isloading) {
        [loadingView startAnimating];
        return;
    }
    
    [loadingView stopAnimating];
}


#pragma mark -
#pragma mark LS Update Info
- (void) updateStateWithInfo:(LSUpdateInfo *)updateInfo {
    [super updateStateWithInfo:updateInfo];
    
}

#pragma mark -
#pragma mark AOChartViewDelegate

#pragma mark Adding New Investment (NO CALL)
- (void) addNewInvestment: (AOInvestment*)inv{
     if (inv!=nil)
         [self.chart addNewInvestment:inv];
}

- (void) cursorChangedLocation:(CGPoint)cursorLocation {
    float cursorArrowX = currentLevelArrowImageView.frame.size.width/2;
    
    if (self.chart.zoomCoef>1 && cursorLocation.x > self.frame.size.width-self.chart.currentLevelLabelWidth/2-10) { //right
        cursorArrowX = self.frame.size.width-currentLevelArrowImageView.frame.size.width/2;
        [currentLevelArrowImageView setImage:[UIImage imageNamed:@"current_level_right_background"]];
        currentLevelArrowLabel.frame = CGRectMake(0.0, 0.0, currentLevelArrowLabel.frame.size.width, currentLevelArrowLabel.frame.size.height);
        currentLevelArrowImageView.hidden = NO;
    }
    else if (self.chart.zoomCoef>1 && cursorLocation.x < 0){ //left
       
        [currentLevelArrowImageView setImage:[UIImage imageNamed:@"current_level_background"]];
        currentLevelArrowLabel.frame = CGRectMake(5.0, 0.0, currentLevelArrowLabel.frame.size.width, currentLevelArrowLabel.frame.size.height);
        currentLevelArrowImageView.hidden = NO;
    }
    else { //inside
        currentLevelArrowImageView.hidden = YES;
    }
    self.chart.currentLevelLabel.hidden = !currentLevelArrowImageView.hidden;
    currentLevelArrowImageView.center = CGPointMake(cursorArrowX, cursorLocation.y+2/*design fix*/); //Only works with equal heights of AOAssetPageChartView and ChartView
}

- (void) currentLevelChanged:(NSString*)currentLevelText{
    [currentLevelArrowLabel setText:currentLevelText];
}

- (void) beganDraggingChartView{
    [self.delegate beganDraggingChartView];
}

- (void) endedDraggingChartView{
    [self.delegate endedDraggingChartView];
}

@end