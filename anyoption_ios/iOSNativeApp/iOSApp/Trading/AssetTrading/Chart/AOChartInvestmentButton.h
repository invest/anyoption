//
//  AOChartInvestmentButton.h
//  iOSApp
//
//  Created by  on 1/7/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOInvestment.h"

@interface AOChartInvestmentButton : UIButton


@property (nonatomic, strong) AOInvestment* investment;
@property CGPoint coords;
@property (assign, nonatomic) float currentLevel;



- (id)initWithInvestment:(AOInvestment*)location andCurrentLevel:(float)lvl;

@end
