//
//  AOTradeAssetChartView.h
//  iOSApp
//
//  Created by  on 4/16/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOBaseChartView.h"
#import "AOMarket.h"

@interface AOTradeAssetChartView : AOBaseChartView <LSTableDelegate>
@property(nonatomic, strong) AOMarket* market;
-(void)removeLsTable;
@end
