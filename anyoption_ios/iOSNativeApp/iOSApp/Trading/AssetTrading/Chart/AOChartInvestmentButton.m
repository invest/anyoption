//
//  AOChartInvestmentButton.m
//  iOSApp
//
//  Created by  on 1/7/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOChartInvestmentButton.h"
#import "AOInvestment.h"


@implementation AOChartInvestmentButton {
    UIImageView *arrView;
    CGRect defaultFrame;
    UIImage *selectedBackgroundImage;
    UIImageView* selectedBg;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (id)initWithInvestment:(AOInvestment*)inv andCurrentLevel:(float)lvl
{
    selectedBackgroundImage =[UIImage imageNamed:@"investBtnGlow.png"];
    selectedBg = [[UIImageView alloc] initWithImage:selectedBackgroundImage];
    
    self = [self initWithFrame:CGRectMake(0, 0, selectedBackgroundImage.size.width, selectedBackgroundImage.size.height)];
    if (self) {
       
        self.investment = inv;
        self.currentLevel = lvl;
    }
    return self;
}

-(id)init{
    self = [super init];
    
    if(self){
        
        

        
    }
    
    return self;
}


-(float)currentLevel:(float)lvl {
    
    return self.currentLevel;
}




-(void)setCurrentLevel:(float)lvl {
    
    self.investment.level = [NSString stringWithFormat:@"%f",lvl];
    
    if (arrView!=nil) {
        [arrView removeFromSuperview];
        arrView.image = nil;
        arrView = nil;
    }
    
    UIImage *arrow;
    
    if (self.investment.hasRibbon) {
#ifdef COPYOP
        if (self.investment.typeId == AOInvestmentTypeCall) {
            if ([self.investment.copyopType isEqualToString:@"COPY"]) {
                arrow = [UIImage imageNamed:@"copied_up_winning"];
            } else {
                arrow = [UIImage imageNamed:@"triangle_up_small_yellow"];
            }
        } else {
            if ([self.investment.copyopType isEqualToString:@"COPY"]) {
                arrow = [UIImage imageNamed:@"copied_down_winning"];
            } else {
                arrow = [UIImage imageNamed:@"triangle_down_small_yellow"];
            }
        }
#else 
        arrow = [UIImage imageNamed:[NSString stringWithFormat:@"chart_investmentArrow%@Win",(self.investment.typeId == AOInvestmentTypeCall)?@"":@"Down"]];
#endif
    } else {
#ifdef COPYOP
        if (self.investment.typeId == AOInvestmentTypeCall) {
            if ([self.investment.copyopType isEqualToString:@"COPY"]) {
                arrow = [UIImage imageNamed:@"copied_up_losing"];
            } else {
                arrow = [UIImage imageNamed:@"triangle_up_small_blue"];
            }
        } else {
            if ([self.investment.copyopType isEqualToString:@"COPY"]) {
                arrow = [UIImage imageNamed:@"copied_down_losing"];
            } else {
                arrow = [UIImage imageNamed:@"triangle_down_small_blue"];
            }
        }
#else
        arrow = [UIImage imageNamed:[NSString stringWithFormat:@"chart_investmentArrow%@Lose",(self.investment.typeId == AOInvestmentTypeCall)?@"":@"Down"]];
#endif
    }
    
    arrView = [[UIImageView alloc] initWithImage:arrow];
#ifdef COPYOP
    if (![self.investment.copyopType isEqualToString:@"COPY"]) {
        defaultFrame = CGRectMake(self.frame.size.width/2 - 2, self.frame.size.height/2 - 2 ,8.0, 8.0);
    } else {
        defaultFrame = CGRectMake(self.frame.size.width/2 - 6, self.frame.size.height/2 - 6 ,14.0, 14.0);
    }
#else
    defaultFrame = CGRectMake(self.frame.size.width/2 - 6, self.frame.size.height/2 - 6 ,12.0, 12.0);
#endif
    arrView.frame = defaultFrame;
    arrView.hidden = YES;
    [self addSubview:arrView];
    
    _currentLevel = lvl;
}

-(void)setSelected:(BOOL)selected{
    
    if (selected) {
        [self addSubview:selectedBg];
    }
    else {
        [selectedBg removeFromSuperview];
    }
}

-(void)layoutSubviews {
    
    if (self.investment.newlyAdded) {
        CGPoint center = arrView.center;
        CGRect newFrame = defaultFrame;
        newFrame.size = CGSizeMake(arrView.frame.size.width*3, arrView.frame.size.height*3);
        arrView.frame = newFrame;
        arrView.center = center;
        
        self.investment.newlyAdded=NO;
        [UIView animateKeyframesWithDuration:0.8 delay:0.0 options:UIViewKeyframeAnimationOptionBeginFromCurrentState animations:^{
            [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.8 animations:^{
                arrView.frame = defaultFrame;
            }];
            
        } completion:^(BOOL finished) {
            //
        }];
    }
    
    arrView.hidden = NO;
}

@end
