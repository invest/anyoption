//
//  AOAssetDetailsViewController.h
//  iOSApp
//
//  Created by Tony on 5/13/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOBaseViewController.h"
#import "AOInvestment.h"
#import "InvestmentAmountsOverlayViewController.h"
#import "ReturnsOverlayViewController.h"
#import "AOBaseViewController.h"
#import "InvestButtonView.h"
#import "AOLayoutWebView.h"
#import "AOAssetPageChartView.h"
#import "AOLightstreamerConnectionHandler.h"
#import "AOCurrencyLabel.h"
#import "AOSlideView.h"

@class AppDelegate,AOInvestmentInfoViewController,AOMarket;

@interface AOAssetDetailsViewController : AOBaseViewController <LSTableDelegate,AOAssetPageChartViewDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate> {
    
    
    __weak IBOutlet UIView *assetPullableView;
    __weak IBOutlet UIView *assetPulableViewIPad;
    
    __weak IBOutlet UIView *assetInfoHandleView;
    __weak IBOutlet NSLayoutConstraint *assetInfoSeparatorHeight;
   
    __weak IBOutlet AOLayoutWebView *assetTimeWebview;
    NSTimer *assetTimer;
    NSDate *timeLastInvest;
    
    AOInvestmentInfoViewController *aoInvestmentInfoController;
    __weak IBOutlet UIView *investmentInfoView;
    
    int marketLastState;
    
    __weak IBOutlet UIView *playAreaSpace;
    __weak IBOutlet UIView *trendViewSpace;
    
    UITapGestureRecognizer *investmentTapRecognizer;
    
    BOOL isGettingNextHourChartData;
}

@property (strong,nonatomic) AOMarket *market;
@property(nonatomic, weak) IBOutlet AOAssetPageChartView *chart;

@property (nonatomic,assign) AOOpportunityType optionType;
@property (strong, nonatomic) LSExtendedTableInfo *lsTableInfo;
@property (strong, nonatomic) LSExtendedTableInfo *lsTableInfoInsurances;

@property (weak, nonatomic) AOLightstreamerConnectionHandler *lsConnectionHandler;

-(void)startUp;

//ls methods
//- (void)applicationDidEnterBackground;
//- (void)applicationWillEnterForeground;
-(void)refreshLocalizedData;

- (void) showPopupIfNeeded;

@end