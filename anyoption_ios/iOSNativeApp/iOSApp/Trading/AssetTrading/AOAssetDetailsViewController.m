//
//  AOAssetDetailsViewController.m
//  iOSApp
//
//  Created by Tony on 5/13/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#define SWIPE_UP_THRESHOLD -1000.0f
#define SWIPE_DOWN_THRESHOLD 1000.0f
#define SWIPE_LEFT_THRESHOLD -1000.0f
#define SWIPE_RIGHT_THRESHOLD 1000.0f


#import "AOAssetDetailsViewController.h"
#import "AONavigationController.h"
#import "AOUtils.h"
#import "AOChartDataMethodRequest.h"
#import "AORequestService.h"
#import "AOChartData.h"
#import "UILabel+SSText.h"
#import "AORoundCornerView.h"
#import "UIView+Disabler.h"
#import "AOInvestmentInfoViewController.h"
#import "LSUpdateInfo.h"
#import "Return.h"
#import "AOMarket.h"
#import "AOInvestmentsMethodRequest.h"
#import "AOInsertInvestmentRequest.h"
#import "PlayAreaViewController.h"
#import "AOAssetIndexMethodRequest.h"
#import "AOPastExpiresMethodRequest.h"
#import "NSString+Utils.h"
#import "AOTrendViewController.h"
#import "AOAssetInfoViewController.h"
#import "SocialFeedViewController.h"
#import "ReceiptViewController.h"
#import "UIAlertView+Blocks.h"
#import "AOConstants.h"
#import "NSString+Utils.h"
#import "AOPopupInfoView.h"
#import "AOAssetDetailsViewController+AOPopupInfo.h"
#import "NSString+Utils.h"
#import "UIAlertView+Blocks.h"
#import "AOConstants.h"
#ifdef COPYOP
#import "COModalPopupViewController.h"
#import "COReceiptPopupViewController.h"
#import "COFundRequestPopupViewController.h"
#import "UIViewController+Helpers.h"
#endif

#ifdef COPYOP

@interface AOAssetDetailsViewController ()

@property (nonatomic, strong) COModalPopupViewController *popupCont;

@end

#endif

@implementation AOAssetDetailsViewController{
    BOOL initialUpdate;
    BOOL isShowingLandscapeView;
    BOOL showOptionPlusQuoteRow;
    
    PlayAreaViewController* playArea;
    AOTrendViewController *trendViewController;
    AOAssetInfoViewController *assetInfo;
    
    __block NSObject *dsLock;
    
    __weak IBOutlet UIImageView *assetBackgroundImage;
    
    
    SocialFeedViewController* socialFeedViewController;
    
    UIPanGestureRecognizer *bigSwipeGestureRecognizer;
    
    
    __weak IBOutlet UIImageView *leftScreenImageView;
    
    __weak IBOutlet UIImageView *rightScreenimageView;
    
    BOOL canBigSwipe;
    BOOL startingUp;
    __block BOOL processingInvestment;
    
    BOOL _pageReloaded;
}
-(void)dealloc{
    // NSLog(@"TradingViewController dealloc");
}

-(id)init{
    self = [super init];
    if(self){
        self.view.userInteractionEnabled=NO;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    canBigSwipe = !IS_IPAD;
    
  
    
    
#ifdef COPYOP
    [leftScreenImageView setImage:[UIImage imageNamed:@"cooverlayBG-568"]];
    [rightScreenimageView setImage:[UIImage imageNamed:@"cooverlayBG-568"]];
#endif
    
    bigSwipeGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleBigSwipe:)];
    bigSwipeGestureRecognizer.delegate = self;
    bigSwipeGestureRecognizer.minimumNumberOfTouches = 1;
    [self.view addGestureRecognizer:bigSwipeGestureRecognizer];
    self.optionType = AOOpportunityTypeRegular;
    if ([self.navOptions objectForKey:@"market"]!=nil && [[self.navOptions objectForKey:@"market"] isKindOfClass:[AOMarket class]]){
        
        self.market = [self.navOptions objectForKey:@"market"];
        
        if (self.market.isOptionPlusMarket) {
            self.optionType = AOOpportunityTypeOptionPlus;
            
        }
    }
    
    assetInfo = [[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:@"AssetInfo"];
    assetInfo.market = self.market;
    assetInfo.optionType = self.optionType;
    
    UIView *assetInfoContainer;
    
    if (IS_IPAD) {
        assetInfoContainer = assetPulableViewIPad;
    } else if (IS_BELOW_IPHONE5) {
        assetPullableView.frame = CGRectMake(assetPullableView.frame.origin.x, assetPullableView.frame.origin.y, assetPullableView.frame.size.width, assetPullableView.frame.size.height-88);
        assetInfoContainer = assetPullableView;
    } else {
        assetInfoContainer = assetPullableView;
    }
    
    assetInfo.view.frame = assetInfoContainer.frame;
    [assetInfoContainer addSubview:assetInfo.view];
    [self addChildViewController:assetInfo];
    [assetInfo didMoveToParentViewController:self];
    
    playArea = [[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPad)?@"PlayAreaIPad":@"PlayArea"];
 
    playArea.delegate = self;
    playArea.optionType = self.optionType;
    
    [self p_pinView:playArea.view toParentView:playAreaSpace];
    [self addChildViewController:playArea];
    [playArea didMoveToParentViewController:self];
    
    trendViewController = [[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:IS_IPAD?@"trendViewControllerIpad":@"trendViewController"];
    [self p_pinView:trendViewController.view toParentView:trendViewSpace];
    [self addChildViewController:trendViewController];
    [trendViewController didMoveToParentViewController:self];
    [self.view bringSubviewToFront:trendViewController.view];
    
    socialFeedViewController = [[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:IS_IPAD?@"socialfeedIPad":@"socialfeed"];
    [self p_pinView:socialFeedViewController.view toParentView:trendViewSpace];
    [self addChildViewController:socialFeedViewController];
    [socialFeedViewController didMoveToParentViewController:self];
    
//    trendViewSpace.backgroundColor = [UIColor purpleColor];
    
    aoInvestmentInfoController = [[AOInvestmentInfoViewController alloc] initWithNibName:@"AOInvestmentInfoViewController" bundle:nil];
    [self p_pinView:aoInvestmentInfoController.view toParentView:investmentInfoView];
    [self addChildViewController:aoInvestmentInfoController];
    [aoInvestmentInfoController didMoveToParentViewController:self];
    
    aoInvestmentInfoController.optionType = self.optionType;
    [aoInvestmentInfoController.getQuotesBtn addTarget:self action:@selector(showHideOptionsList:) forControlEvents:UIControlEventTouchUpInside];
   
    assetInfoSeparatorHeight.constant = 0.5;

    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseUrl = [NSURL fileURLWithPath:path];
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"AssetTime" ofType:@"html"];
    NSString* htmlCont = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [assetTimeWebview loadHTMLString:htmlCont baseURL:baseUrl];
    
    if (IS_RIGHT_TO_LEFT)
    {
        [assetTimeWebview evaluateJavascriptString:@"document.getElementById('assetQuestion').style.textAlign = 'right'"];
        [assetTimeWebview evaluateJavascriptString:@"document.getElementById('timerText').style.textAlign = 'right'"];
        [assetTimeWebview evaluateJavascriptString:@"document.body.style.direction='rtl'"];
    }
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    NSString *questionFontName = [theme assetWebViewQuestionFontName];
    NSString *timeFontName = [theme assetWebViewTimeFontName];
    [assetTimeWebview evaluateJavascriptString:[NSString stringWithFormat:@"document.getElementById('assetQuestion').style.fontFamily='%@'", questionFontName]];
    [assetTimeWebview evaluateJavascriptString:[NSString stringWithFormat:@"document.getElementById('timerText').style.fontFamily='%@'", timeFontName]];
    
    assetTimeWebview.hidden = YES;
    assetTimeWebview.layoutDelegeate = self;
    
    timeLastInvest = [NSDate dateWithTimeInterval:1800 sinceDate:[AOUtils currentDatePrecise]];
    
    investmentTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(investmentDidTapAnywhere:)];
    investmentTapRecognizer.delegate = self;

    marketLastState = -1;
    
    //=====
   
    [self.view bringSubviewToFront:investmentInfoView]; // very important - otherwise the play are handles the touches of the investment view
}

- (void) p_pinView:(UIView *)childView toParentView:(UIView *)parentView
{
    childView.translatesAutoresizingMaskIntoConstraints = NO;
    [parentView addSubview:childView];
    NSDictionary *vars = NSDictionaryOfVariableBindings(childView);
    [parentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[childView]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:vars]];
    [parentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[childView]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:vars]];
}

-(void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationItem.hidesBackButton = YES;
}

-(BOOL)shouldAutomaticallyForwardAppearanceMethods{
    return NO;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self startUp];
    
    NSString *titleKey = nil;
    NSString *contentsKey = nil;
    if (!self.market.isOptionPlusMarket)
    {
        titleKey = @"assetPageHelpTitle";
        contentsKey = @"assetTradeHelpContent";
    } else
    {
        titleKey = @"assetPagePlusHelpTitle";
        contentsKey = @"assetTradePlusHelpContent";
    }

    NSString *title = AOLocalizedString(titleKey);
    NSURL *arrowUp = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"triangle_small_up_white" ofType:@"png"]];
    NSURL *arrowDown = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"triangle_small_down_white" ofType:@"png"]];
    NSString *format = AOLocalizedString(contentsKey);
    NSString *contents = [NSString stringWithFormat:format, arrowUp.absoluteString, arrowDown.absoluteString];
    
    [self buildHelpButtonWithFinalTitle:title finalContent:contents];
    
    [self.view bringSubviewToFront:assetInfoHandleView];
    
    // for iPhone, show the popup in viewDidAppear, for iPad wait until all the overlays are closed
    if (!IS_IPAD)
    {
        [self showPopupIfNeeded];
    }
    
    
    self.view.userInteractionEnabled=YES;
}

- (void) showPopupIfNeeded
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (![userDefaults boolForKey:USER_DEFS_WALKTHROUGH_ASSET_SHOWN_KEY])
    {
        AOPopupInfoView *v = [[AOPopupInfoView alloc] initWithDetailsViewController:self];
        [v show];
        
        [userDefaults setBool:YES forKey:USER_DEFS_WALKTHROUGH_ASSET_SHOWN_KEY];
        [userDefaults synchronize];
    }
}

-(void)startUp {
    startingUp=YES;
    _market.closingTimeDate = nil;
    [assetTimer invalidate];
    assetTimer = nil;
    
    [self initAssetTimer];
    self.optionType=AOOpportunityTypeRegular;
    if(_market) {
        if (self.market.isOptionPlusMarket)
            self.optionType = AOOpportunityTypeOptionPlus;
        
        UIImage *image = nil;
        if(self.optionType == AOOpportunityTypeOptionPlus) image = [UIImage imageNamed:@"option_plus_icon_1"];
        [self buildTitleViewWithText:_market.displayName andImage:image];
    }
  
    
    //[assetPullableView onLayoutLoad];
    
    self.chart.delegate = self;
    self.chart.optionType = self.optionType;
    [self.chart.chart reset];
    
  
    
    
    [self getChartData];
    
    
    
}

-(void)refreshLocalizedData {
    marketLastState = -1;
    [aoInvestmentInfoController removeInvestment];
    [playArea refreshLocalizedData];
}

- (void)reloadAssets {
    
    
    assetBackgroundImage.frame = CGRectMake(0.0, 0.0, self.chart.frame.size.width, self.chart.frame.origin.y+self.chart.frame.size.height);
    UIImage *backImage = (!self.market.isUnavailable)?[AOUtils backgroundForMarket:_market.getId]:[UIImage imageNamed:@"market_unavailable"];
    if (backImage)
    {
        assetBackgroundImage.image = backImage;
    }
    else
    {
        assetBackgroundImage.image =[UIImage imageNamed:@"asset_pic1"];
    }
    
    if ([assetInfoHandleView viewWithTag:11]!=nil)
        [(UIImageView*)[assetInfoHandleView viewWithTag:11]  setImage:[AOUtils iconForMarket:_market.getId]];
    
    if (self.optionType==AOOpportunityTypeOptionPlus && [assetInfoHandleView viewWithTag:12]!=nil){
        [(UIImageView*)[assetInfoHandleView viewWithTag:12]  setImage:[UIImage imageNamed:@"option_plus_icon_1"]];
    }
    else {
        NSString *mIdStr = [NSString stringWithFormat:@"%ld", self.market.l_id];
        if ([RESIDENT.userFavoriteAssets objectForKey:mIdStr]) {
            [(UIImageView*)[assetInfoHandleView viewWithTag:12] setHidden:NO];
        }
    }
}

-(void)p_willLeave {
    self.view.userInteractionEnabled=NO;
    
    leftScreenImageView.hidden=YES;
    rightScreenimageView.hidden=YES;
    if (canBigSwipe)
    {
        self.view.center =  CGPointMake(self.view.frame.size.width/2, self.view.center.y);
    }
    //[assetPullableView onLayoutDisappear];
    
    [assetTimer invalidate];
    assetTimer = nil;
    
    [RESIDENT.lsConnectionHandler removeTableWithTableInfo:_lsTableInfo];
    _lsTableInfo = nil;
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self p_willLeave];
}

-(void)initAssetTimer{
        assetTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateAssetTimer) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:assetTimer forMode:NSRunLoopCommonModes];
    
}

-(void)updateAssetTimer{
    [_market setTimeLeft:nil];
    
    if(_market&&_market.closingTimeDate) {
        
        BOOL marketStateDidChange = NO;
        
        if(!_market.isClosingLevel&&marketLastState==3) {
            // NSLog(@"loading next hour chart data");
            [self getChartData]; //reload chart data 1 minute after expiry
        }
        
        if(_market.isClosingLevel&&marketLastState!=3) {
            marketLastState = 3;
            marketStateDidChange = YES;
            
            if(!isGettingNextHourChartData){
                NSString *waitingMessage = [NSString stringWithFormat:@"%@: %@",AOLocalizedString(@"assetIndex.closingLevel"),_market.closingLevel];
                
                [playArea isWaiting:YES withMessage:waitingMessage];
                [aoInvestmentInfoController isWaiting:YES];
                
                [assetTimeWebview setIdentifier:@"assetQuestion" withHTML:[NSString stringWithFormat:AOLocalizedString(@"willAssetGoUpOrDown"),_market.displayName]];
                [assetTimeWebview setIdentifier:@"timerText" withHTML:waitingMessage];
            }
            
            return;
        }
        else if(_market.isWaitingForExpiry&&marketLastState!=0) {
            marketLastState = 0;
            marketStateDidChange = YES;
            
            [assetTimeWebview setIdentifier:@"assetQuestion" withHTML:[NSString stringWithFormat:AOLocalizedString(@"willAssetGoUpOrDown"),_market.displayName]];
            
            NSString *firstString = [NSString stringWithFormat:AOLocalizedString(@"byTimeToday"),@"<span id='endTime'>-</span>"];
            NSString *secondString = AOLocalizedString(@"binary0100.mainarea.waiting.for.expiry");
            NSString *htmlString = [NSString stringWithFormat:@"%@&nbsp;|&nbsp;%@",firstString,secondString];
            [assetTimeWebview setIdentifier:@"timerText" withHTML:htmlString];
            
            [assetTimeWebview setIdentifier:@"endTime" withHTML:[_chart.chart.dateFormatter stringFromDate:_market.closingTimeDate]];
            
            [playArea isWaiting:YES withMessage:secondString];
            [aoInvestmentInfoController isWaiting:YES];
        }
        else if (_market.isCounting&&marketLastState!=2) {
            marketLastState = 2;
            [assetTimeWebview setIdentifier:@"assetQuestion" withHTML:[NSString stringWithFormat:AOLocalizedString(@"willAssetGoUpOrDown"),_market.displayName]];
            marketStateDidChange = YES;
            
            NSString *firstString = [NSString stringWithFormat:AOLocalizedString(@"byTimeToday"),@"<span id='endTime'>-</span>"];
            NSString *secondString = [NSString stringWithFormat:AOLocalizedString(@"minToInvest"),@"<span id='remainingTime'>-</span>"];
            NSString *htmlString = [NSString stringWithFormat:@"%@&nbsp;|&nbsp;%@",firstString,secondString];
            [assetTimeWebview setIdentifier:@"timerText" withHTML:htmlString];
            
            /*
             int minutes = floor(timeLeft/60); //round down
             int seconds = trunc(timeLeft-minutes*60);
             
             [assetTimeWebview setIdentifier:@"remainingTime" withHTML:[NSString stringWithFormat:@"%d",seconds] ];
             
             if(_market.isLast5Minutes) [assetTimeWebview evaluateJavascriptString:@"setRemainingTimeRed(1)"];
             else [assetTimeWebview evaluateJavascriptString:@"setRemainingTimeRed(0)"];
             */
            
            [assetTimeWebview setIdentifier:@"endTime" withHTML:[_chart.chart.dateFormatter stringFromDate:_market.closingTimeDate]];
            
            NSString *timeLeftString = [NSString stringWithFormat:@"setTimeInterval(%f)",_market.timeLeftInterval];
            
            [assetTimeWebview evaluateJavascriptString:timeLeftString];
        }
        else if(_market.isUnavailable&&marketLastState!=1) {
            marketLastState = 1;
            marketStateDidChange = YES;
            
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            [df setDateFormat:@"HH:mm|dd.MM.yy"];
            
            [assetTimeWebview setIdentifier:@"assetQuestion" withHTML:[NSString stringWithFormat:AOLocalizedString(@"suspend.upperline1"),_market.displayName]];
            
            NSString *firstString = AOLocalizedString(@"suspend.lowerline1");
            NSString *secondString = [df stringFromDate:_market.closingTimeDate];
            NSString *htmlString = [NSString stringWithFormat:@"%@: %@",firstString,secondString];
            [assetTimeWebview setIdentifier:@"timerText" withHTML:htmlString];
            
            [playArea isWaiting:YES withMessage:AOLocalizedString(@"suspend.upperline1")];
            [aoInvestmentInfoController isWaiting:YES];
            [self showHideOptionsList:nil];
            
            [playArea showHidePullableViewHandle:NO];
            
          
        }
        
        if(marketStateDidChange) {
            [assetTimeWebview evaluateJavascriptString:@"autoshrinkElementIdWithMaxLines('assetQuestion',2)"];
            [assetTimeWebview evaluateJavascriptString:@"autoshrinkElementIdWithMaxLines('timerText',1)"];
            
            if(!_market.isWaitingForExpiry&&!_market.isClosingLevel&&!_market.isUnavailable) {
                [playArea isWaiting:NO withMessage:nil];
                [aoInvestmentInfoController isWaiting:NO];
            }
        }
    }
    else if (marketLastState!=-1) {
        marketLastState = -1;
        [assetTimeWebview setIdentifier:@"assetQuestion" withHTML:@""];
        [assetTimeWebview setIdentifier:@"timerText" withHTML:@""];
    }
    
}

-(void)layoutWebviewDidFinishLoading:(AOLayoutWebView*)layoutWebview{
    layoutWebview.hidden = NO;
}

-(void)updateCurrentReturnWithLevel:(double)currLevelDouble{
    
        double totalProfit = 0;
        double totalAmount = 0;
        NSArray *investments = [playArea getOpenOptions];
        if (investments) {
            
            for (int i = 0; i < [investments count]; i++) {
                AOInvestment *inv = (AOInvestment*)[investments objectAtIndex:i];
                inv.level = _market.level;
                totalProfit += [inv returnAtLevel:currLevelDouble];
                totalAmount += [inv amount];
            }
        }
    
        aoInvestmentInfoController.investedAmount = [AOUtils formatCurrencyAmountWithoutSymbol:[AOUtils divideCurrencyAmount:totalAmount]];
        aoInvestmentInfoController.expectedReturn = [AOUtils formatCurrencyAmountWithoutSymbol:[AOUtils divideCurrencyAmount:totalProfit]];
    
}

#pragma mark -
#pragma mark ChartData & Options Requests
-(void)getChartData
{
    

    
    if (_lsTableInfo) {
        [RESIDENT.lsConnectionHandler removeTableWithTableInfo:_lsTableInfo];
        _lsTableInfo = nil;
        
       
    }
    
    
    AOChartDataMethodRequest *request = [[AOChartDataMethodRequest alloc] init];
    if (_market.getId){
        request.marketId = _market.getId;
    }
    else if(_market.boxId>=0){
        request.box = _market.boxId;
        request.opportunityTypeId = 1;
    }
    
    
    [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary* resultDictionary) {
        
        if (!IS_IPAD)
        {
            self.view.center =  CGPointMake(self.view.frame.size.width/2, self.view.center.y );
        }
        
        if (!self.market.l_id) { //if it is loaded by boxId
            self.market = [AOMarket marketWithId:[[resultDictionary objectForKey:@"marketId"] longLongValue] andName:[resultDictionary objectForKey:@"marketName"]];
        }
        
        assetInfo.market = self.market;
        playArea.market = self.market;
        aoInvestmentInfoController.market = self.market;
        
        self.market.decimalPoint = [[resultDictionary objectForKey:@"decimalPoint"] intValue];
        
        [self reloadAssets];
        [assetInfo reloadAssetInfo];
        
        if(!self.isViewLoaded||!self.view.window) return;
        
        AOChartData *chartData = [[AOChartData alloc] init];
        [chartData setValuesForKeysWithDictionary:[NSMutableDictionary dictionaryWithDictionary:resultDictionary]];
        [self.chart setChartData:chartData];
        aoInvestmentInfoController.openOptions = chartData.investments.count;
        
        //[self.landscapeChart setChartData:chartData];
        
        if(self.chart.chart.investments.count>0) {
            showOptionPlusQuoteRow = YES;
            [playArea setOpenOptions:[[self.chart.chart.investments reverseObjectEnumerator] allObjects]];
        }
        else [playArea removeOpenOptions];
        
        if (self.optionType == AOOpportunityTypeOptionPlus) {
            [playArea setupCommissionLabel:[[resultDictionary valueForKey:@"commission"] longLongValue]];
        }
        
        NSString *prefix = nil;
        if(self.optionType != AOOpportunityTypeOptionPlus ) prefix = [NSString stringWithFormat:@"aotps_%@",[resultDictionary objectForKey:@"scheduled"]];
        else prefix = [NSString stringWithFormat:@"op"];
        NSArray *items = [[NSArray alloc] initWithObjects: [NSString stringWithFormat:@"%@_%@", prefix, [resultDictionary objectForKey:@"marketId"] ],@"live_investmentsall", nil];
        
        NSMutableSet* assetDetailsFields = [NSMutableSet setWithArray:RESIDENT.lsConnectionHandler.fields];
        [assetDetailsFields addObjectsFromArray:RESIDENT.lsConnectionHandler.socialFeedFields];
        
       
        self.lsTableInfo = [LSExtendedTableInfo extendedTableInfoWithItems:items mode:LSModeCommand fields:[assetDetailsFields allObjects] dataAdapter:LS_DATA_ADAPTER snapshot:YES];
        self.lsTableInfo.requestedMaxFrequency= 1.0;
        
        [RESIDENT.lsConnectionHandler addTableWithTableInfo:self.lsTableInfo delegate:self];
        
        
        if(IS_LOGGED) [self getSettledOptions];
        [self refreshLocalizedData];
        startingUp=NO;
        canBigSwipe = !IS_IPAD;
    }
    failed:nil];
}

-(void)getSettledOptions{
    AOInvestmentsMethodRequest *request = [[AOInvestmentsMethodRequest alloc] init];
    request.startRow = 0;
//    request.pageSize = 10; // 0 - all
    request.pageSize = 0; // 0 - all
    request.isSettled = @YES;
    request.marketId = _market.getId;
    
    [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary *resultDictionary) {
        NSArray *investments = [resultDictionary objectForKey:@"investments"];
        if(investments.count>0) [playArea setSettledOptions:investments];
        else [playArea removeSettledOptions];
    } failed:^(NSDictionary *errDictionary) {
        // NSLog(@"getUserInvestments failed:%@", errDictionary);
    }];
}

#pragma mark -
#pragma mark Play Area delegate
-(void)investmentReady:(NSDictionary*)invDict {
#ifdef ETRADER
    if ([RESIDENT checkRegulationKnowledge] != AORegulationCheckResultSuccess) {
#else
    if ([RESIDENT checkRegulationKnowledge:AORegulationPEPActionTrade] != AORegulationCheckResultSuccess) {
        
#endif
    
        return;
        
    }
    
        if([DLG userRegulation].thresholdBlock > 0) {
            
            [[MAINVC popupVC] showPopup:AOCysecRestricted withData:nil animated:YES completion:nil];
            
        }

#if !COPYOP
    if ([DLG user].balance == 0.0)
        
    {
        [playArea investmentResult:YES];
        
        [UIAlertView showWithTitle:nil
                           message:[AOLocalizedString(@"CMS.tradeBox.text.insufficient_funds") stringByStrippingHTML]
                 cancelButtonTitle:AOLocalizedString(@"optionPlus.button.not.now")
                 otherButtonTitles:@[AOLocalizedString(@"profitLine.deposit")]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              if (buttonIndex == alertView.firstOtherButtonIndex)
                              {
                                  [MAINVC setLockOverlay:NO];
                                  [MAINVC loadPage:@"Account" pageName:@"depositMenu" root:YES options:nil];
                                  [RESIDENT setMainMenuSelectedIndex:MainMenuIndexDeposit];
                              }
                          }];
        return;
    }
#endif
    
    AOInvestment *futureInvestment = [invDict valueForKey:@"investment"];
    
    int choice = 2;
    if ([[invDict objectForKey:@"isCall"] integerValue])
        choice = 1;
    
    AOInsertInvestmentRequest *request = [[AOInsertInvestmentRequest alloc] init];
    
    request.userName = [DLG user].userName;
    request.password = [DLG user].encryptedPassword;
    request.serviceMethod = @"insertInvestment";
    
    request.opportunityId = self.chart.chart.oppId;
    request.choice  = choice;
    
    request.requestAmount = futureInvestment.amount;
    
    request.pageOddsWin = round((1+futureInvestment.oddsWin)*100.0)/100.0;
    request.pageOddsLose = round((1-futureInvestment.oddsLose)*100.0)/100.0;

    request.pageLevel = self.chart.chart.currentLevel; //[self.chart.chart.currentLevelLabel.text doubleValue];
    
    if (self.chart.chart.currentLevel < DBL_MIN) {
        SHOW_DEFAULT_ERROR_NOTIFICATION();
        return;
    }
    
    request.isFromGraph = !(self.interfaceOrientation == UIInterfaceOrientationPortrait);
    processingInvestment=YES;
    [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary* resultDictionary) {
        processingInvestment=NO;
        // NSLog(@"insertInvestment success: %@",resultDictionary);
        
        [RESIDENT setUserWithDict:resultDictionary];
        
        [playArea investmentResult:YES];
        
        NSMutableDictionary *investmentDict = [[NSMutableDictionary alloc] initWithDictionary:[resultDictionary objectForKey:@"investment"]];
        AOInvestment *investment = [[AOInvestment alloc] init];
        [investment setValuesForKeysWithDictionary:investmentDict];
        [self.chart addNewInvestment:investment];
        [playArea addOpenOption:investment];
        
        [self updateCurrentReturnWithLevel:playArea.currentLevel];
        
        aoInvestmentInfoController.openOptions++;

        //[TrackingManager trackEvent:TrackingEventTypeInvestment options:nil];
        
        AONotification* receiptNotification = AONOTIFICATIONINFO(AONotificationPurchaseReceipt, AOLocalizedString(@"purchaseCompleted"), [resultDictionary objectForKey:@"investment"]);
        
        [receiptNotification addActionEventHandler:^(id sender, UIEvent *event) {
#ifdef COPYOP
            COReceiptPopupViewController *receiptPopup = [[UIStoryboard storyboardWithName:@"CopyOP" bundle:nil] instantiateViewControllerWithIdentifier:@"receiptPopup"];
            COModalPopupViewController *popupContainer = [[COModalPopupViewController alloc] initWithContentViewController:receiptPopup];
            popupContainer.modalPopupY = 50;
            __typeof__(receiptPopup) __weak recVc = receiptPopup;
            popupContainer.closeButtonPressedHandler = receiptPopup.completionHandler = ^
            {
                if (recVc != nil)
                {
                    __typeof__(recVc) __strong sRecVc = recVc;
                    [sRecVc.co_modalPopupContainer dismissAnimated:YES completion:nil];
                }
            };
            receiptPopup.investment = playArea.pastInvestment;
            UIViewController *topVc = [MAINVC co_topMostViewController];
            [popupContainer presentInViewController:topVc animated:YES completion:nil];
#else
            ReceiptViewController *receiptViewController = [[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:@"receiptController"];
            receiptViewController.receiptInvestment = playArea.pastInvestment;
            
            [MAINVC presentViewController:receiptViewController animated:YES completion:nil];
#endif
        }];
        
        
        
        //Adding market to favourites
        NSString *mIdStr = [NSString stringWithFormat:@"%ld", self.market.l_id];
        if (![RESIDENT.userFavoriteAssets objectForKey:mIdStr]) {
            // NSLog(@"Add market to favorites %@", mIdStr);
            [RESIDENT.userFavoriteAssets setObject:@"" forKey:mIdStr];
            [RESIDENT.userDeletedAssets removeObjectForKey:mIdStr];
            [RESIDENT savePersonalAssetsList];
            [(UIImageView*)[assetInfoHandleView viewWithTag:12] setHidden:NO]; //showing blue star
        } else {
            // NSLog(@"Market %@ already in favorites", mIdStr);
            [RESIDENT.userFavoriteAssets setObject:@"" forKey:mIdStr];
        }
        
    } failed:^(NSDictionary* errorDictionary){
        processingInvestment=NO;
        
        if ([(NSError *)errorDictionary[@"error"] code] == SERVICE_ERROR_NIOU_LOW_BALANCE) {
#ifndef ETRADER
            [[MAINVC popupVC] showPopup:AONIOUNotice withData:errorDictionary animated:YES completion:nil];
            processingInvestment=NO;
            [playArea investmentResult:NO];
#endif
        } else {
#ifdef COPYOP
        if ([(NSError *)errorDictionary[@"error"] code] == SERVICE_ERROR_VALIDATION_LOW_BALANCE) {
            COFundRequestPopupViewController *fundRequestPopup = [[UIStoryboard storyboardWithName:@"CopyOP" bundle:nil] instantiateViewControllerWithIdentifier:@"fundRequestPopup"];
            COModalPopupViewController *popupContainer = [[COModalPopupViewController alloc] initWithContentViewController:fundRequestPopup];
            popupContainer.modalPopupY = 170;
            __typeof__(fundRequestPopup) __weak fundVc = fundRequestPopup;
            popupContainer.closeButtonPressedHandler = fundRequestPopup.completionHandler = ^
            {
                if (fundVc != nil)
                {
                    __typeof__(fundVc) __strong sFundVc = fundVc;
                    [sFundVc.co_modalPopupContainer dismissAnimated:YES completion:nil];
                }
            };
            UIViewController *topVc = [MAINVC co_topMostViewController];
            [popupContainer presentInViewController:topVc animated:YES completion:nil];
        } else {
#ifdef ETRADER
            if ([[errDictionary objectForKey:@"errorCode"] intValue]==SERVICE_ERROR_NOT_ANSWERED_YES_KNOWLEDGE) {
                [RESIDENT checkRegulationKnowledge:AORegulationPEPActionTrade];
            }
#endif      
            /* BAC - 1648 */
            if([DLG userRegulation].thresholdBlock <= 0) {
                SHOW_USER_NOTIFICATIONS(errorDictionary);
            }
        }
#endif
        processingInvestment=NO;
        [playArea investmentResult:NO];
        
#if !COPYOP
            /* BAC - 1648 */
    if([DLG userRegulation].thresholdBlock <= 0) {
        SHOW_USER_NOTIFICATIONS(errorDictionary);
        
     }
#endif
        
        
#ifdef ETRADER
        if ([(NSError *)errorDictionary[@"error"] code] == SERVICE_ERROR_NOT_ACCEPTED_TERMS && [[AOUtils parseTimeFromJSON:[DLG user].timeCreated] compare:[AOUtils parseTimeFromJSON:@"May 07, 2015 12:13:36 PM"]] != NSOrderedDescending) {
       
            [RESIDENT goTo:@"etTerms" withOptions:nil];
            return;
        }
#endif
            
            
        }

    } ];
}

-(void)openOptionTapped:(AOInvestment*)inv {
    
    [self.chart.chart selectInvestmentButtonWithInvestment:inv];
}

#pragma mark -
#pragma mark LSTableDelegate & Notification

- (void) table:(LSSubscribedTableKey *)tableKey itemPosition:(int)itemPosition itemName:(NSString *)itemName didUpdateWithInfo:(LSUpdateInfo *)updateInfo {
    // NSLog(@"assets details ->> table:itemPosition:itemName:didUpdateWithInfo: \n\n %@", [[updateInfo currentValueOfFieldName:@"ET_ODDS_GROUP"] trimWhitespaces]);
   
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (!_market.isUnavailable)
        {
            long currId = self.chart.chart.oppId;
            long newId = [@([[updateInfo currentValueOfFieldName:@"ET_OPP_ID"] integerValue]) intValue];
            BOOL isDifferentOpportunity = (newId != currId);
            
            if (isDifferentOpportunity)
            {
                if (_market.isWaitingForExpiry)
                {
                    NSString *newEstTimeClosed = [updateInfo currentValueOfFieldName:@"ET_EST_CLOSE"];
                    if (newEstTimeClosed != nil)
                    {
                        NSDate *newClosingTime = [[NSDate alloc] initWithTimeInterval:0 sinceDate:[AOUtils parseTimeFromLSUpdate:newEstTimeClosed]];
                        if ([newClosingTime compare:_market.closingTimeDate] == NSOrderedDescending)
                        {
                            BOOL hasInvestment = NO;
                            NSArray *openOptions = [self.playArea getOpenOptions];
                            for (AOInvestment *op in openOptions)
                            {
                                if (op.opportunityId == currId)
                                {
                                    hasInvestment = YES;
                                    break;
                                }
                            }
                            if (!hasInvestment)
                            {
                                if (self.isViewLoaded && self.view.window)
                                {
                                    if (!_pageReloaded)
                                    {
                                        _pageReloaded = YES;
                                        [self p_willLeave];
                                        //if (IS_LOGGED){
                                        
                                        //}
                                        [RESIDENT goTo:@"home" withOptions:nil];
                                        [MAINVC loadPage:@"Trade" pageName:IS_IPAD?ASSET_DETAILS_IPAD_PAGE_ID:ASSET_DETAILS_PAGE_ID root:NO options:@{@"market":[AOMarket marketWithId:self.market.l_id andName:[AOUtils marketNameForId:self.market.l_id] ],@"replaceVC":@YES}];
                                        return;
                                        
                                    }
                                }
                            }
                        }
                    }
                }
                
                return;
            }
        } else {
            
        }
        
        int nState = [[updateInfo currentValueOfFieldName:@"ET_STATE"] intValue];
        
        if(nState==AOOpportunityStateDone){
             [self p_willLeave];
            [MAINVC loadPage:@"Trade" pageName:IS_IPAD?ASSET_DETAILS_IPAD_PAGE_ID:ASSET_DETAILS_PAGE_ID root:NO options:@{@"market":[AOMarket marketWithId:self.market.l_id andName:[AOUtils marketNameForId:self.market.l_id] ],@"replaceVC":@YES}];
        }
        
        if ((nState == 0 || nState == AOOpportunityStateSuspended) && _market.state == AOOpportunityStateSuspended) {
            return;
        }
        
        // states of opportunity change from created (1) -> opened(2) -> last 10 min (3) -> closing 1 min (4) -> closing (5) -> closed(6) -> done (7)
        // when hour opp change the new one will open before the prev one is done so it will have < state
        // to keep the current opp in the row ignore updates with smaller state until the current one reach state "done"
        if (nState < _market.state && _market.state != AOOpportunityStateDone
            && _market.state != AOOpportunityStateSuspended       // exception - suspended state (9) - you can suspend from any state and go from suspend back to any state
            && _market.state != AOOpportunityStateClosing1Min) {  // exception - to avoid counter stuck on 00:00 we change the state client side. see AOMarket.updateTimeLeft
//            // NSLog(@"Ignore update for next opp.");
            return;
        }
        
        
        //        if ([updateInfo isChangedValueOfFieldName:@"ET_ODDS"]) {
        //            // NSLog(@"ET_ODDS %@",[updateInfo currentValueOfFieldName:@"ET_ODDS"]);
        //        }
        
        
        /* CORE - 2310 */
        if ([updateInfo currentValueOfFieldName:@"AO_OPP_STATE"] == 0 || nState != _market.state) {
            
            NSLog(@"!!!!AO_OPP_STATE: \n %@", [updateInfo currentValueOfFieldName:@"AO_OPP_STATE"]);

                _market.stateText = [updateInfo currentValueOfFieldName:@"ET_SUSPENDED_MESSAGE"];
                [assetBackgroundImage setImage:[UIImage imageNamed:@"market_unavailable"]];
                aoInvestmentInfoController.currentLevel = .000;
                _market.isUnavailable = YES;

        }
         /* CORE - 2310 */

        
        if ([updateInfo isChangedValueOfFieldName:@"ET_ODDS_WIN"] || nState != _market.state) {
             NSLog(@"ET_ODDS_WIN %@",[updateInfo currentValueOfFieldName:@"ET_ODDS_WIN"]);
            if (IS_IPAD || self.market.isOptionPlusMarket) {
                [playArea setDefaultReturnWith:[updateInfo currentValueOfFieldName:@"ET_ODDS_WIN"] andLoseIndex:nil];            }
        }
        
        if ([updateInfo isChangedValueOfFieldName:@"ET_ODDS_LOSE"] || nState != _market.state) {
             NSLog(@"ET_ODDS_LOSE %@",[updateInfo currentValueOfFieldName:@"ET_ODDS_LOSE"]);
            [playArea setDefaultReturnWith:nil andLoseIndex:[updateInfo currentValueOfFieldName:@"ET_ODDS_LOSE"]];
        }
        
        if ([updateInfo isChangedValueOfFieldName:@"ET_ODDS_GROUP"] || nState != _market.state) {
            NSLog(@"ET_ODDS_GROUP %@", [[updateInfo currentValueOfFieldName:@"ET_ODDS_GROUP"] trimWhitespaces] );
            [self getReturnValues:[NSArray arrayWithArray:[[[updateInfo currentValueOfFieldName:@"ET_ODDS_GROUP"] trimWhitespaces] componentsSeparatedByString:@" "]]];
        }
        
        if ([updateInfo isChangedValueOfFieldName:@"AO_CALLS_TREND"] || nState != _market.state) { //also has ET
            float trendPercent = [[updateInfo currentValueOfFieldName:@"AO_CALLS_TREND"] floatValue]*100;
            trendViewController.trendView.percent = (int)trendPercent;
        }
        
        if ([updateInfo isChangedValueOfFieldName:@"ET_EST_CLOSE"] || nState != _market.state) {
            NSString *closeTimeString = [updateInfo currentValueOfFieldName:@"ET_EST_CLOSE"];
            _market.closingTimeDate = [[NSDate alloc] initWithTimeInterval:0 sinceDate:[AOUtils parseTimeFromLSUpdate:closeTimeString]];
            [_market setTimeLeft:nil];
        }
        
        if ([updateInfo isChangedValueOfFieldName:@"ET_LAST_INV"] || nState != _market.state) {
            NSString *lastInvestTimeString = [updateInfo currentValueOfFieldName:@"ET_LAST_INV"];
            _market.timeLastInvestDate = [[NSDate alloc] initWithTimeInterval:0 sinceDate:[AOUtils parseTimeFromLSUpdate:lastInvestTimeString]];
        }
        
        if ([updateInfo isChangedValueOfFieldName:@"LI_ASSET"]) {//-------------------LS Social Feed
            // NSLog(@"LIVE ASSET CHANGED!");
#ifndef ETRADER
            if (self.market.l_id == [[updateInfo currentValueOfFieldName:@"LI_ASSET"] longLongValue])
                [socialFeedViewController addSocialFeedInfo:@{@"amount":[updateInfo currentValueOfFieldName:@"LI_AMOUNT"],@"country":[updateInfo currentValueOfFieldName:@"LI_COUNTRY"],@"level":[updateInfo currentValueOfFieldName:@"LI_LEVEL"],@"invType":[updateInfo currentValueOfFieldName:@"LI_INV_TYPE"],@"prodType":[updateInfo currentValueOfFieldName:@"LI_PROD_TYPE"]}];
#endif
        }
        
        NSString *levelField = [NSString stringWithFormat:@"LEVEL_%d",RESIDENT.skinGroupId];
        
        if (!processingInvestment) {
            float currLevelFloat = [[AOUtils parseLevelFromLSUpdate:[updateInfo currentValueOfFieldName:levelField]] floatValue];
            _market.level = [updateInfo currentValueOfFieldName:levelField];
            [self updateCurrentReturnWithLevel:currLevelFloat];
            
            aoInvestmentInfoController.currentLevel = currLevelFloat;
            
            playArea.currentLevel = currLevelFloat;
            if ([updateInfo isChangedValueOfFieldName:levelField] && currLevelFloat>0){
                
                
            }
            [self.chart lsUpdateWithItemPosition:itemPosition itemName:itemName didUpdateWithInfo:updateInfo];
        }

        if ([updateInfo isChangedValueOfFieldName:@"ET_STATE"] || nState != _market.state) {
            
            if (nState == AOOpportunityStateSuspended || nState == AOOpportunityStateCreated || nState == AOOpportunityStatePaused) {
            
                    _market.stateText = [updateInfo currentValueOfFieldName:@"ET_SUSPENDED_MESSAGE"];
                    [assetBackgroundImage setImage:[UIImage imageNamed:@"market_unavailable"]];
                    aoInvestmentInfoController.currentLevel = .000;
          
              
            }
            
                
            
            _market.state = nState;
            
            if (_market.isWaitingForExpiry) {
                //Check if there are updates for a new opportunity_id
                    //Check if there are NO open options  //_market.hasOpenedOptions
                
                        //getChartData  //[MAINVC loadPage:@"Trade" pageName:IS_IPAD?ASSET_DETAILS_IPAD_PAGE_ID:ASSET_DETAILS_PAGE_ID root:NO options:@{@"market":[self.market copy],@"replaceVC":@YES}];
                
            }
            
        }
       
               if(initialUpdate) {
            // [self.landscapeChart lsUpdateWithItemPosition:itemPosition itemName:itemName didUpdateWithInfo:updateInfo];
            initialUpdate = NO;
        }
    });
    
    
    
    
}

#pragma mark -
#pragma mark AO ASsetPageChartView Delegate methods

- (void)showInvestmentDetails:(AOInvestment*)inv screenPoint:(CGPoint)point
{
    [aoInvestmentInfoController setInvestment:inv];
    [self.view addGestureRecognizer:investmentTapRecognizer];
    [aoInvestmentInfoController setPointerOffset:inv.pointerX];
    
    UIWindow *win = [[UIApplication sharedApplication].delegate window];
    CGPoint convertedPoint = [win convertPoint:point toView:aoInvestmentInfoController.view];
    [aoInvestmentInfoController setPointerOffset:convertedPoint.x];
}

- (void)beganDraggingChartView{
    canBigSwipe=NO;
}

- (void)endedDraggingChartView{
    canBigSwipe=!IS_IPAD;
}

#pragma mark tap recognizer delegate

-(void)investmentDidTapAnywhere:(UIGestureRecognizer *)gestureRecognizer {
    
    [self.chart setInInvestmentMode:NO];
    [aoInvestmentInfoController removeInvestment];
    aoInvestmentInfoController.currentLevel = [[AOUtils parseLevelFromLSUpdate:_market.level] doubleValue];
    [self.view  removeGestureRecognizer:investmentTapRecognizer];
}

- (void)getReturnValues:(NSArray *)keys {
    if (self.isViewLoaded && self.view.window) {
        AOMethodRequest *request = [[AOMethodRequest alloc] init];
        request.serviceMethod = @"getAllReturnOdds";
        
        [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary* resultDictionary) {
            NSArray *returnValues = [resultDictionary valueForKey:@"opportunityOddsPair"];
             NSLog(@"didTapAnywhere %@", returnValues);

            [playArea setReturnValues:[self filterArray:returnValues forKeys:keys]];
        } failed:nil];
    }
}

- (NSMutableArray *)filterArray:(NSArray *)array forKeys:(NSArray *)keys {
    NSMutableArray *filteredArr = [[NSMutableArray alloc] init];
    
    int middle = floor(keys.count / 2);
    NSLog(@"middle one is %d", middle);
    
    for (int i=0; i<array.count; i++) {
        for (int j=0; j<keys.count; j++) {
            if ([[array[i] valueForKey:@"selectorID"] intValue] == [keys[j] intValue]) {
                [self exchangeKey:@"refundSelector" withKey:@"lose" inMutableDictionary:array[i]];
                [self exchangeKey:@"returnSelector" withKey:@"win" inMutableDictionary:array[i]];
                [array[i] removeObjectForKey:@"selectorID"];
                
                /* uncomment here after Tabakov's fix */
                if([keys[j] hasSuffix:@"d"]) {
                    float win = [array[i][@"win"] floatValue] / 100;
                    float lose = [array[i][@"lose"] floatValue] / 100;
                    
                    NSString *winString = [NSString stringWithFormat:@"%f", win + 1];
                    NSString *loseString = [NSString stringWithFormat:@"%f", lose];
                    [playArea setDefaultReturnWith:winString andLoseIndex:loseString];
                }
                
//                if(i == middle) {
//                    float win = [array[i][@"win"] floatValue] / 100;
//                    float lose = [array[i][@"lose"] floatValue] / 100;
//
//                    NSString *winString = [NSString stringWithFormat:@"%f", win + 1];
//                    NSString *loseString = [NSString stringWithFormat:@"%f", lose];
//                    [playArea setDefaultReturnWith:winString andLoseIndex:loseString];
//                }
                
                
                [filteredArr addObject:array[i]];
                
                if (filteredArr.count == keys.count) {
                    return filteredArr;
                }
            }
        }
    }
    
    return filteredArr;
}

- (void)exchangeKey:(NSString *)aKey withKey:(NSString *)aNewKey inMutableDictionary:(NSMutableDictionary *)aDict
{
    if (![aKey isEqualToString:aNewKey]) {
        id objectToPreserve = [aDict objectForKey:aKey];
        [aDict setObject:objectToPreserve forKey:aNewKey];
        [aDict removeObjectForKey:aKey];
    }
}

#pragma mark - 
#pragma mark Big Swipe Handler

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    
   
    
    CGPoint touchLocation = [touch locationInView:assetPullableView];
    return !CGRectContainsPoint(self.view.frame, touchLocation);
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if (gestureRecognizer==bigSwipeGestureRecognizer) {
        return canBigSwipe;
    }
    
    return YES;
}

- (void)handleBigSwipe:(UIPanGestureRecognizer*)recognizer
{
    CGPoint t = [recognizer translationInView:recognizer.view];
    [recognizer setTranslation:CGPointZero inView:recognizer.view];
    
    self.view.center = CGPointMake(self.view.center.x + t.x, self.view.center.y );
    
    
    if (recognizer.state == UIGestureRecognizerStateEnded)
    {
        __block BOOL  reloadPage = (self.view.center.x < 0 || self.view.center.x > self.view.frame.size.width);
        __block  BOOL isLeftSwipe = self.view.center.x < 0;
        __weak AOAssetDetailsViewController* weakSelf =  self;
        
        if(![RESIDENT.userFavoriteAssets objectForKey:self.market.stringId]){
            [RESIDENT.userFavoriteAssets setObject:@"temp" forKey:self.market.stringId];
        }
        
        NSMutableArray* bigSwipeArray = [[RESIDENT.userFavoriteAssets allKeys] mutableCopy];
        
        //Check if current market is already in Favourites
        NSInteger currentMarketIndex = [bigSwipeArray indexOfObjectPassingTest:^BOOL(NSString* favMarketIdString, NSUInteger idx, BOOL *stop) {
            if((long)[favMarketIdString longLongValue]==[self.market getId]){
                
                return YES;
                *stop = YES;
            }
            
            return NO;
        }];
        
        //if not we add it
        if (currentMarketIndex==NSNotFound) {
            [bigSwipeArray insertObject:[NSString stringWithFormat:@"%li",[self.market getId]] atIndex:0];
            currentMarketIndex=0;
        }
        
        if (bigSwipeArray.count<2) {
           
            reloadPage=NO;
        }
        
        if (reloadPage) {
            
            if (!isLeftSwipe) {
                
                currentMarketIndex --;
                if(currentMarketIndex<0){
                    currentMarketIndex=bigSwipeArray.count-1;
                }
                
            }
            else {
                
                currentMarketIndex ++;
                if (currentMarketIndex>bigSwipeArray.count-1) {
                    currentMarketIndex=0;
                }
            }
            
            long mId =(long)[bigSwipeArray[currentMarketIndex]  longLongValue] ;
            NSString* mName =  [[RESIDENT.marketsInfo objectForKey:bigSwipeArray[currentMarketIndex]] objectForKey:@"name"];
            
            self.market = nil;
            self.market = [AOMarket marketWithId:mId andName:mName];

            if(            [[[RESIDENT.marketsInfo objectForKey:bigSwipeArray[currentMarketIndex]] objectForKey:@"isOptionPlusMarket"] intValue]==1){
                self.market.isOptionPlusMarket = YES;
            }
            
            
            weakSelf.view.userInteractionEnabled=NO;
            [UIView animateWithDuration:PANEL_SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 
                                 if (reloadPage) {
                                     if (isLeftSwipe)//Left vel.x < SWIPE_LEFT_THRESHOLD
                                     {
                                         weakSelf.view.center =  CGPointMake(-self.view.frame.size.width/2, self.view.center.y );
                                     }
                                     else  //Right
                                     {
                                         weakSelf.view.center =  CGPointMake(3*self.view.frame.size.width/2, self.view.center.y );
                                     }
                                 }
                                 
                             }
                             completion:^(BOOL finished) {
                                 weakSelf.view.userInteractionEnabled=YES;
                                 
                                  [self p_willLeave];
                                 [MAINVC loadPage:@"Trade" pageName:IS_IPAD?ASSET_DETAILS_IPAD_PAGE_ID:ASSET_DETAILS_PAGE_ID root:NO options:@{@"market":[self.market copy],@"replaceVC":@YES}];
                                 
                             }];
        }
        else {
            weakSelf.view.userInteractionEnabled=NO;
            [UIView animateWithDuration:PANEL_SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 self.view.center =  CGPointMake(self.view.frame.size.width/2, self.view.center.y );
                             }
                             completion:^(BOOL finished) {
                                 weakSelf.view.userInteractionEnabled=YES;
                                 if(reloadPage) {
                                      [self p_willLeave];
                                     [MAINVC loadPage:@"Trade" pageName:IS_IPAD?ASSET_DETAILS_IPAD_PAGE_ID:ASSET_DETAILS_PAGE_ID root:NO options:@{@"market":[self.market copy],@"replaceVC":@YES}];
                                 }
                             }];
        }
    }

}

/*- (void)handleBigSwipe2:(UIPanGestureRecognizer*)recognizer  //kept just in case. Working with markets with open investments
{
    
    CGPoint t = [recognizer translationInView:recognizer.view];
    [recognizer setTranslation:CGPointZero inView:recognizer.view];
    
    self.view.center = CGPointMake(self.view.center.x + t.x, self.view.center.y );
    
    
    if (recognizer.state == UIGestureRecognizerStateEnded)
    {
        __block BOOL  reloadPage = (self.view.center.x < 0 || self.view.center.x > self.view.frame.size.width);
        __block  BOOL isLeftSwipe = self.view.center.x < 0;
         __weak AOAssetDetailsViewController* weakSelf =  self;
         if (reloadPage) {
            NSMutableArray* bigSwipeArray = [NSMutableArray new];
            long currentMarketIndex = 0;
            for (NSString *key in RESIDENT.marketsInfo) {
 
                 if (([[RESIDENT.marketsInfo objectForKey:key] objectForKey:@"hasOpenedOptions"]  || [[RESIDENT.marketsInfo objectForKey:key] objectForKey:@"currentlyViewed"])) {
                     [[RESIDENT.marketsInfo objectForKey:key] setObject:key forKey:@"id"];
                     [bigSwipeArray addObject:[RESIDENT.marketsInfo objectForKey:key]];
 
                     if ([key isEqualToString:[NSString stringWithFormat:@"%ld",[self.market getId]]]) {
                        currentMarketIndex = bigSwipeArray.count-1;
                     }
                 }
            }
             
             if (!isLeftSwipe) {
                 
                 currentMarketIndex --;
                 if(currentMarketIndex<0){
                     currentMarketIndex=bigSwipeArray.count-1;
                 }
                 
             }
             else {
                 
                 currentMarketIndex ++;
                 if (currentMarketIndex>bigSwipeArray.count-1) {
                     currentMarketIndex=0;
                 }
             }
             
             long mId =(long)[[bigSwipeArray[currentMarketIndex] objectForKey:@"id"] longLongValue] ;
             NSString* mName =  [bigSwipeArray[currentMarketIndex] objectForKey:@"name"];
             self.market = nil;
             self.market = [AOMarket marketWithId:mId andName:mName];
             
             if([[bigSwipeArray[currentMarketIndex] objectForKey:@"isOptionPlusMarket"] intValue]==1){
                 self.market.isOptionPlusMarket = YES;
             }
             
            
             weakSelf.view.userInteractionEnabled=NO;
             [UIView animateWithDuration:PANEL_SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                              animations:^{
                                  
                                  if (reloadPage) {
                                      if (isLeftSwipe)//Left vel.x < SWIPE_LEFT_THRESHOLD
                                      {
                                          weakSelf.view.center =  CGPointMake(-self.view.frame.size.width/2, self.view.center.y );
                                      }
                                      else  //Right
                                      {
                                          weakSelf.view.center =  CGPointMake(3*self.view.frame.size.width/2, self.view.center.y );
                                      }
                                  }
                                  
                              }
                              completion:^(BOOL finished) {
                                  weakSelf.view.userInteractionEnabled=YES;
                                  
                                      
                                      [MAINVC loadPage:@"Trade" pageName:IS_IPAD?ASSET_DETAILS_IPAD_PAGE_ID:ASSET_DETAILS_PAGE_ID root:NO options:@{@"market":[self.market copy],@"replaceVC":@YES}];
                                  
                              }];
         }
         else {
             weakSelf.view.userInteractionEnabled=NO;
             [UIView animateWithDuration:PANEL_SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                              animations:^{
                                    self.view.center =  CGPointMake(self.view.frame.size.width/2, self.view.center.y );
                              }
                              completion:^(BOOL finished) {
                                  weakSelf.view.userInteractionEnabled=YES;
                                  if(reloadPage) {
                                      
                                      [MAINVC loadPage:@"Trade" pageName:IS_IPAD?ASSET_DETAILS_IPAD_PAGE_ID:ASSET_DETAILS_PAGE_ID root:NO options:@{@"market":[self.market copy],@"replaceVC":@YES}];
                                  }
                              }];
         }
    }
}*/

-(void)showHideOptionsList:(UIButton*)sender{
    
    [playArea openClosePullableView];
    
    [sender setSelected:!sender.selected];
}

#pragma mark - Popup Info

- (PlayAreaViewController *) playArea
{
    return playArea;
}

- (UIView *) assetInfoHandleView
{
    return assetInfoHandleView;
}

@end