//
//  ReturnsOverlayViewController.m
//  iOSApp
//
//  Created by  on 2/6/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "ReturnsOverlayViewController.h"
#import "Return.h"
#import "ReturnCell.h"

@implementation ReturnsOverlayViewController{
    __weak IBOutlet UITableView *returnsTableView;
    __weak IBOutlet UILabel *chooseReturnTitle;
    __weak IBOutlet UILabel *ifRightLabel;
    __weak IBOutlet UILabel *ifWrongLabel;
    NSArray * dataSource;
    NSInteger selectedRow;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	returnsTableView.delegate = self;
    returnsTableView.dataSource = self;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    chooseReturnTitle.text = AOLocalizedString(@"chooseProfit");
    ifRightLabel.text = AOLocalizedString(@"CMS.tradeBox.text.if_correct_earn");
    ifWrongLabel.text = AOLocalizedString(@"CMS.tradeBox.text.if_incorrect_get");
}

-(void)viewDidLayoutSubviews{
    returnsTableView.center = CGPointMake(IS_IPAD?[AOUtils devicePortraitBounds].size.height/2:[AOUtils devicePortraitBounds].size.width/2, returnsTableView.center.y);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
}

#pragma mark -
#pragma mark UITableView Delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"ReturnCell";
    ReturnCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    Return *aRet = [dataSource objectAtIndex:indexPath.row];
    
    NSLog(@"index path row: %f", aRet.winIndex);
    NSLog(@"index path row: %f", self.selectedWinIndex);

    if (cell == nil)
        cell = [[ReturnCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.winReturnLabel.text  = aRet.winReturn;
    cell.loseReturnLabel.text = aRet.loseReturn;
    
    if (indexPath.row != dataSource.count-1) {
        [cell drawBottomBorder];
    }
    
    if (selectedRow==-1 && aRet.winIndex == self.selectedWinIndex) {
        selectedRow = indexPath.row;
        
    }
    return cell;
}

- (void) setupWithReturns: (NSArray*)rets andDefIndex:(float)ind {
    self.selectedWinIndex = ind;
    selectedRow = -1;
    dataSource = rets;
    if (IS_IPAD) {
        [self setBgImg:[UIImage imageNamed:@"overlayBG-iPad"]];
    }
    else {
        NSString* overlayImageName = [AOUtils isRetina4] ? @"overlayBG-568" : @"overlayBG";
#ifdef COPYOP
        overlayImageName = [@"co" stringByAppendingString:overlayImageName];
#endif
        [self setBgImg:[UIImage imageNamed:overlayImageName]];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *oldCell = [tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow: selectedRow inSection: 0]];
    oldCell.selected = NO;
    
    Return *sRet = [dataSource objectAtIndex:indexPath.row];
    self.selectedWinIndex = sRet.winIndex;
    selectedRow=-1;
    
    [self.delegate didChooseReturnValue:indexPath.row ];
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == selectedRow)
        cell.selected = YES;
    
}


@end
