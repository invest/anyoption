//
//  InvestmentButton.m
//  iOSApp
//
//  Created by  on 1/22/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "InvestmentButton.h"

@implementation InvestmentButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
         [self setupUI];
    }
    return self;
}

- (void) setupUI {
   // self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"predefinedAmountItemBar"]];
    [self setBackgroundImage:[UIImage imageNamed:@"predefinedAmountItemBar"] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageNamed:@"predefinedAmountItemBar_on"] forState:UIControlStateSelected];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];

    self.titleLabel.textAlignment = NSTextAlignmentCenter;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
