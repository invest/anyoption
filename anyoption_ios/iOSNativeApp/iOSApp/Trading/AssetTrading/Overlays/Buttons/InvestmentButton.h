//
//  InvestmentButton.h
//  iOSApp
//
//  Created by  on 1/22/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

@interface InvestmentButton : UIButton


@property (nonatomic, copy) NSNumber* amount;

@end
