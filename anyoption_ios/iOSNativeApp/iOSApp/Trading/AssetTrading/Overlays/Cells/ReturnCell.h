//
//  ReturnCell.h
//  iOSApp
//
//  Created by  on 2/6/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//



@interface ReturnCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *winReturnLabel;
@property (weak, nonatomic) IBOutlet UILabel *loseReturnLabel;

- (void)drawBottomBorder;

@end