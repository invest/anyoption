//
//  ReturnCell.m
//  iOSApp
//
//  Created by  on 2/6/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "ReturnCell.h"
#import "AOUtils.h"
#include <execinfo.h>

@implementation ReturnCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
        
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setupUI];
    }
    
    return self;
}

- (void) setupUI {
    self.backgroundView =  [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"returnCellBg"]  ];
    self.selectedBackgroundView =  [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"returnCellBg_on"] ];
    
    [self drawVerticalLine];
}

- (void)drawVerticalLine {
    CALayer *verticalLine = [CALayer layer];
    verticalLine.borderColor = [UIColor colorWithHexString:@"#87B5CF"].CGColor;
    verticalLine.borderWidth = 1;
    verticalLine.frame = CGRectMake(CGRectGetWidth(self.frame)/2, 0, 1, CGRectGetHeight(self.frame));
    
    [self.layer addSublayer:verticalLine];
}

-(void)drawBottomBorder {
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.borderColor = [UIColor colorWithHexString:@"#87B5CF"].CGColor;
    bottomBorder.borderWidth = 1;
    bottomBorder.frame = CGRectMake(0, CGRectGetHeight(self.frame)-1, CGRectGetWidth(self.frame), 1);
    
    [self.layer addSublayer:bottomBorder];
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

@end
