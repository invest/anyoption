//
//  ReturnsOverlayViewController.h
//  iOSApp
//
//  Created by  on 2/6/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//


#import "AOOverlayViewController.h"

@protocol ReturnsOverlayDelegate <NSObject>

-(void)didChooseReturnValue:(NSInteger)am;

@end

@interface ReturnsOverlayViewController : AOOverlayViewController <UITableViewDelegate,UITableViewDataSource>
@property (assign) float selectedWinIndex;

@property (nonatomic, weak) id delegate;

- (void) setupWithReturns: (NSArray*)rets andDefIndex:(float)ind ;
- (void) setupWithReturns: (NSArray*)rets andDefIndex:(float)ind andSelectedSelector:(int)sel;

@end
