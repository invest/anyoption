//
//  ReceiptViewController.h
//  iOSApp
//
//  Created by  on 2/7/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOOverlayViewController.h"

@interface ReceiptViewController : AOOverlayViewController
@property AOInvestment *receiptInvestment;
@end
