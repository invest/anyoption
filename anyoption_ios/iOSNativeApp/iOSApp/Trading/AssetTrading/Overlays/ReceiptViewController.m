//
//  ReceiptViewController.m
//  iOSApp
//
//  Created by  on 2/7/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "ReceiptViewController.h"
#import "AOInvestment.h"
#import "UILabel+SSText.h"

@interface ReceiptViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcExpireAboveRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcExpireBelowRight;

@end

@implementation ReceiptViewController {
    __weak IBOutlet UIView *investmentFirstSection;
    __weak IBOutlet UIView *investmentSecondSection;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *levelLabel;
    __weak IBOutlet UILabel *marketNameLabel;
    __weak IBOutlet UILabel *expiryTimeLabel;
    __weak IBOutlet UILabel *amountTitleLabel;
    __weak IBOutlet UILabel *amountLabel;
    __weak IBOutlet UILabel *profitTitleLabel;
    __weak IBOutlet UILabel *profitLabel;
    __weak IBOutlet UILabel *receiptIdLabel;
    __weak IBOutlet UIButton *submitButton;
    __weak IBOutlet UIImageView *optionImageView;
    __weak IBOutlet UIView *expAboveCell;
    __weak IBOutlet UILabel *expAboveTitleLabel;
    __weak IBOutlet UILabel *expAboveLabel;
    __weak IBOutlet UIView *expBelowCell;
    __weak IBOutlet UILabel *expBelowTitleLabel;
    __weak IBOutlet UILabel *expBelowLabel;

    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (IS_IPAD) {
        [self setBgImg:   [UIImage imageNamed:@"overlayBG-iPad"]      ];

    }
    else{
        NSString* overlayImageName = [AOUtils isRetina4] ? @"overlayBG-568" : @"overlayBG";
#ifdef COPYOP
        overlayImageName = [@"co" stringByAppendingString:overlayImageName];
#endif
        [self setBgImg:[UIImage imageNamed:overlayImageName]];
    }
    
    investmentFirstSection.layer.masksToBounds = investmentSecondSection.layer.masksToBounds = YES;
    investmentFirstSection.layer.cornerRadius = investmentSecondSection.layer.cornerRadius = 1;
    investmentFirstSection.alpha = investmentSecondSection.alpha = 0.79;
    investmentFirstSection.backgroundColor = RC_FIRSTSECTION_BACKGROUNDCOLOR;
    investmentSecondSection.backgroundColor = RC_SECONDSECTION_BACKGROUNDCOLOR;
    
    receiptIdLabel.textColor = RC_ID_TEXTCOLOR;
    
    int index = 0;
    for(UIView *view in investmentSecondSection.subviews){
        if(index==investmentSecondSection.subviews.count-1) break;
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0, view.frame.size.height-0.5, view.frame.size.width, 0.5);
        bottomBorder.backgroundColor = [UIColor colorWithHexString:@"#6ba5c0"].CGColor;
        [view.layer addSublayer:bottomBorder];
        index++;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self resetWinView:expAboveCell];
    [self resetWinView:expBelowCell];
    
    UIView *winView = nil;
    NSLayoutConstraint *lc = nil;
    if(_receiptInvestment.typeId==AOInvestmentTypePut) {
        optionImageView.highlighted = YES;
        winView = expBelowCell;
        lc = self.lcExpireBelowRight;
    }
    else if(_receiptInvestment.typeId==AOInvestmentTypeCall)
    {
        winView = expAboveCell;
        lc = self.lcExpireAboveRight;
    }
    
    if(winView) {
        for(UIView *view in winView.subviews){
            if([view isKindOfClass:[UILabel class]]) {
                UILabel *label = (UILabel*)view;
                label.textColor = RC_WINBADGE_TEXTCOLOR;
            }
        }
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"misc_gold-badge.png"]];
        imageView.frame = CGRectOffset(imageView.frame,winView.frame.size.width-imageView.frame.size.width,0);
        [winView addSubview:imageView];
        
        lc.constant = 12.0;
    }
}

-(void)resetWinView:(UIView*)winView{
    for(UIView *view in winView.subviews) {
        if([view isKindOfClass:[UIImageView class]]) [view removeFromSuperview];
        else if([view isKindOfClass:[UILabel class]]) {
            UILabel *label = (UILabel*)view;
            label.textColor = [UIColor whiteColor];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self refreshLocalizedData];
}

-(void)refreshLocalizedData{
    
    titleLabel.text = AOLocalizedString(@"youChose");
    NSString *okString = AOLocalizedString(@"alert.button");
    [submitButton setTitle:okString forState:UIControlStateNormal];
    amountTitleLabel.text = [NSString stringWithFormat:@"%@:",AOLocalizedString(@"bank.wire.amount")];
    profitTitleLabel.text = [NSString stringWithFormat:@"%@:",AOLocalizedString(@"binary0100.rightTable.profit")];
    profitLabel.text = [NSString stringWithFormat:@"%d%%",(int)[_receiptInvestment winReturnPercent]];
    levelLabel.text = [NSString stringWithFormat:@"%@: %@",AOLocalizedString(@"investments.level"),_receiptInvestment.level];
    marketNameLabel.text = _receiptInvestment.asset;
    amountLabel.text = _receiptInvestment.amountTxt;
    
    expAboveTitleLabel.text = [NSString stringWithFormat:AOLocalizedString(@"expAboveLevel"),_receiptInvestment.level];
    expBelowTitleLabel.text = [NSString stringWithFormat:AOLocalizedString(@"expBelowLevel"),_receiptInvestment.level];
    
    if(_receiptInvestment.typeId==AOInvestmentTypePut) {
        expAboveLabel.text = [NSString stringWithFormat:@"%@",[AOUtils formatCurrencyAmount:[AOUtils divideCurrencyAmount:[_receiptInvestment loseReturn]]] ];
        expBelowLabel.text = [NSString stringWithFormat:@"%@",[AOUtils formatCurrencyAmount:[AOUtils divideCurrencyAmount:[_receiptInvestment winReturn]]] ];
    }
    else {
        expAboveLabel.text = [NSString stringWithFormat:@"%@",[AOUtils formatCurrencyAmount:[AOUtils divideCurrencyAmount:[_receiptInvestment winReturn]]] ];
        expBelowLabel.text = [NSString stringWithFormat:@"%@",[AOUtils formatCurrencyAmount:[AOUtils divideCurrencyAmount:[_receiptInvestment loseReturn]]] ];
    }
    
    receiptIdLabel.text = [NSString stringWithFormat:@"%@: %ld", AOLocalizedString(@"CMS.documents.files.id"),_receiptInvestment.l_id];
    
    NSString *timeString1 = [[NSMutableString alloc] initWithString:[_receiptInvestment getExpiryTimeFormatted]];
    NSString *timeString2 = [timeString1 stringByReplacingOccurrencesOfString:@" " withString:@",, "];
    [expiryTimeLabel setSSText:[NSString stringWithFormat:@"%@: %@",AOLocalizedString(@"right.side.investments.time.closing.mobile"),timeString2] withOptions:@{@"ssOffset":@0,@"separator":@",,"}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)okAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (IS_IPAD)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [super touchesBegan:touches withEvent:event];
    }
}

@end
