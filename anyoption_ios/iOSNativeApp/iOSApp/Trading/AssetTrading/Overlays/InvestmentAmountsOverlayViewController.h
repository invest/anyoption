//
//  InvestmentAmountsOverlayViewController.h
//  iOSApp
//
//  Created by  on 1/21/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOOverlayViewController.h"

@protocol InvestmentsOverlayDelegate <NSObject>

-(void)didChooseAmount:(NSNumber*)am;

@end

@interface InvestmentAmountsOverlayViewController : AOOverlayViewController <UITextFieldDelegate>
@property (nonatomic, weak) id delegate;

- (void)setupWithAmounts:(NSArray*)amounts andDefaultValue:(double)am ;
@end
