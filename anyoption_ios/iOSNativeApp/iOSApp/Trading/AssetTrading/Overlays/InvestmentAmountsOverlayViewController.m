//
//  InvestmentAmountsOverlayViewController.m
//  iOSApp
//
//  Created by  on 1/21/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#define AMOUNT_BUTTON_HEIGHT 38

#import "InvestmentAmountsOverlayViewController.h"
#import "InvestmentButton.h"

@interface InvestmentAmountsOverlayViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcSubmitButtonBottom;

@end

@implementation InvestmentAmountsOverlayViewController{
    NSArray *invAmounts;
    
    __weak IBOutlet UIImageView *titleBarBg;
    __weak IBOutlet UILabel *titleBarLabel;
    __weak IBOutlet UIButton *submitBtn;
    
    UITapGestureRecognizer *tapper;
    double defaultAmount;
    UITextField *customTextField;
    NSString *cachedValue;
    BOOL _isEditing;
    
    
    float kbHeight;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if(self) {
        tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stopEditing:)];
        
    }
    
    return self;
}
-(void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification  object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)setupWithAmounts:(NSArray*)amounts andDefaultValue:(double)am{
    defaultAmount = am;
    invAmounts = [[NSArray alloc] initWithArray:amounts];
    if (IS_IPAD) {
        [self setBgImg:[UIImage imageNamed:@"overlayBG-iPad"]];
    }
    else {
        NSString* overlayImageName = [AOUtils isRetina4] ? @"overlayBG-568" : @"overlayBG";
#ifdef COPYOP
        overlayImageName = [@"co" stringByAppendingString:overlayImageName];
#endif
        [self setBgImg:[UIImage imageNamed:overlayImageName]];
    }
}


-(void)setupUI {
    [super setupUI];
    titleBarLabel.adjustsFontSizeToFitWidth = YES;
    titleBarLabel.minimumScaleFactor = 0.5;
    titleBarLabel.text = AOLocalizedString(@"chooseAmount");
    
    float bottomY = titleBarBg.frame.origin.y + titleBarBg.frame.size.height;
    bool hasSelection=NO;
    for (int i = 0; i < invAmounts.count; i++) {
        
        InvestmentButton *itemButton = [[InvestmentButton alloc] initWithFrame:CGRectMake(titleBarBg.frame.origin.x,bottomY , titleBarBg.frame.size.width, AMOUNT_BUTTON_HEIGHT)];
        
        itemButton.amount = invAmounts[i];
        if(defaultAmount==[itemButton.amount doubleValue])
            hasSelection=itemButton.selected=YES;
        
        double amount = [AOUtils divideCurrencyAmount:[[invAmounts objectAtIndex:i] doubleValue]];
        [itemButton setTitle:[AOUtils formatCurrencyAmount:amount] forState:UIControlStateNormal];
        [itemButton setTitle:[AOUtils formatCurrencyAmount:amount] forState:UIControlStateNormal];
        itemButton.tag = 30 + i;
        [itemButton addTarget:self action:@selector(amountChoosed:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:itemButton];
        
        bottomY = itemButton.frame.origin.y + itemButton.frame.size.height;
    }
    
    if(customTextField == nil ) {
        customTextField = [[UITextField alloc] initWithFrame:CGRectMake(titleBarBg.frame.origin.x, bottomY, titleBarBg.frame.size.width, AMOUNT_BUTTON_HEIGHT)];
        customTextField.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:hasSelection?@"predefinedAmountItemBar":@"predefinedAmountItemBar_on"]];
        customTextField.textColor = [UIColor whiteColor];
        customTextField.returnKeyType = UIReturnKeyDone;
        customTextField.textAlignment = NSTextAlignmentCenter;
       
        customTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        customTextField.keyboardType = UIKeyboardTypeDecimalPad;
        customTextField.delegate = self;
    }

    //customTextField.text = [NSString stringWithFormat:@"%.2f",defaultAmount];
    
    AOUser *user = [DLG user];
    
    customTextField.text = [NSString stringWithFormat:@"%@: %@", AOLocalizedString(@"custom"), [AOUtils formatCurrencyAmount:[AOUtils divideCurrencyAmount:user.defaultAmountValue]]];
    [self.view addSubview:customTextField];
    
    CGFloat distBetweenCustomEditAndSubmit = 20.0;
    self.lcSubmitButtonBottom.constant = CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(customTextField.frame) - distBetweenCustomEditAndSubmit - CGRectGetHeight(submitBtn.bounds);
    
    cachedValue = [NSString stringWithFormat:@"%f", [AOUtils divideCurrencyAmount:user.defaultAmountValue]];
    
    [self centerSubviews];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self unselectButtons];
    customTextField.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"predefinedAmountItemBar_on"]];
    
    customTextField.text = @"";
    _isEditing = YES;
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.view addGestureRecognizer:tapper];
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if([customTextField.text isEqualToString:@""]){
        customTextField.text = cachedValue;
        cachedValue = @"";
    }
    _isEditing = NO;
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    cachedValue = [customTextField.text stringByReplacingOccurrencesOfString:@"," withString:@"."];
    customTextField.text = [NSString stringWithFormat:@"%@: %@", AOLocalizedString(@"custom"), [AOUtils formatCurrencyAmount:[cachedValue floatValue]]];
}

-(void)stopEditing:(UITapGestureRecognizer*)gr {
    [self.view endEditing:YES];
    [self.view removeGestureRecognizer:tapper];
}

- (void) amountChoosed: (InvestmentButton*) btn {
    [self unselectButtons];
    btn.selected=YES;
    double amountAtIndex = [[invAmounts objectAtIndex:(btn.tag-30)] doubleValue];
    NSNumber *formattedAmount = [NSNumber numberWithDouble:[AOUtils divideCurrencyAmount:amountAtIndex]];
    [self.delegate didChooseAmount:formattedAmount];
}

-(void)unselectButtons {
    for (int i = 0; i < invAmounts.count; i++)
        [ (InvestmentButton*)[self.view viewWithTag:30+i] setSelected:NO];
    
     customTextField.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"predefinedAmountItemBar"]];
}

- (IBAction) submitOverlay: (id) sender {
        customTextField.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"predefinedAmountItemBar_on"]];
        [self stopEditing:nil];
       [self.delegate didChooseAmount:[NSNumber numberWithFloat:[cachedValue floatValue]]];
}

-(void)centerSubviews {
    for (int i=0; i < self.view.subviews.count; i++) {
        UIView* subView = self.view.subviews[i];
        titleBarBg.center = subView.center = CGPointMake(IS_IPAD?[AOUtils devicePortraitBounds].size.height/2:[AOUtils devicePortraitBounds].size.width/2, subView.center.y);
        
    }
}

- (void) keyboardWillShow:(NSNotification *)notif
{
    if (!IS_BELOW_IPHONE5) {
        return;
    }
    
    [UIView animateWithDuration:0.4 animations:^{  self.view.frame = CGRectOffset(self.view.frame, 0.0, -88);}];
}

- (void) keyboardWillBeHidden:(NSNotification *)notif
{
    if (!IS_BELOW_IPHONE5) {
        return;
    }
    
    [UIView animateWithDuration:0.3 animations:^{  self.view.frame = CGRectOffset(self.view.frame, 0.0, 88);}];
}

@end
