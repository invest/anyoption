//
//  Return.h
//  iOSApp
//
//  Created by  on 2/6/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

@interface Return : NSObject
@property (nonatomic, strong) NSString *winReturn;
@property (nonatomic, strong) NSString *loseReturn;
@property (nonatomic, assign) double winIndex;
@property (nonatomic, assign) double loseIndex;
@end
