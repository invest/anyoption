//
//  AOInvestmentsMethodRequest.h
//  AnyOption
//
//  Created by mac_user on 11/5/13.
//  Copyright (c) 2013 . All rights reserved.
//

#import "AOUserMethodRequest.h"

@interface AOInvestmentsMethodRequest : AOUserMethodRequest

@property (copy, nonatomic) NSString *accountId;
@property (copy, nonatomic) NSString *accountPassword;
@property (copy, nonatomic) NSDictionary *from;
@property (assign, nonatomic) long long fromEpoch;
@property (copy, nonatomic) NSDictionary *to;
@property (assign, nonatomic) long long toEpoch;
@property (assign, nonatomic) NSNumber *isSettled;
@property (assign, nonatomic) long marketId;
@property (assign, nonatomic) int startRow;
@property (assign, nonatomic) int pageSize;
@property (nonatomic, assign) int period;

@end
