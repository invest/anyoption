//
//  AOInvestmentsMethodRequest.m
//  AnyOption
//
//  Created by mac_user on 11/5/13.
//  Copyright (c) 2013 Anyoption. All rights reserved.
//

#import "AOAssetIndexMethodRequest.h"

@implementation AOAssetIndexMethodRequest

-(id)init{
    if(self = [super init]){
        self.serviceMethod = @"getAssetIndexByMarket";
    }
    return self;
}

-(NSMutableArray*)keys
{
    NSMutableArray *keys = [super keys];
    [keys addObject:@"marketId"];
    return keys;
}

@end
