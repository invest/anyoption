//
//  AOInvestmentsMethodRequest.h
//  AnyOption
//
//  Created by mac_user on 11/5/13.
//  Copyright (c) 2013 . All rights reserved.
//

#import "AOMethodRequest.h"

@interface AOPastExpiresMethodRequest : AOMethodRequest

@property (copy, nonatomic) NSDictionary *date;
@property (assign, nonatomic) long marketId;
@property (assign, nonatomic) long marketGroupId;
@property (assign, nonatomic) int startRow;
@property (assign, nonatomic) int pageSize;

@end
