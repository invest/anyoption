//
//  AOInsurancesMethodRequest.m
//  iOSApp
//
//  Created by Tony on 4/13/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOInsurancesMethodRequest.h"

@implementation AOInsurancesMethodRequest

-(id)init{
    if(self = [super init]){
        self.serviceMethod = @"buyInsurances";
    }
    return self;
}

-(NSMutableArray*)keys
{
    NSMutableArray *keys = [super keys];
    [keys addObject:@"insuranceType"];
    [keys addObject:@"investmentIds"];
    [keys addObject:@"insuranceAmounts"];
    return keys;
}

@end
