//
//  AOInvestmentsMethodRequest.m
//  AnyOption
//
//  Created by mac_user on 11/5/13.
//  Copyright (c) 2013 . All rights reserved.
//

#import "AOInvestmentsMethodRequest.h"

@implementation AOInvestmentsMethodRequest

-(id)init{
    if(self = [super init]){
        self.serviceMethod = @"getUserInvestments";
    }
    return self;
}

-(NSMutableArray*)keys
{
    NSMutableArray *keys = [super keys];
    [keys addObject:@"accountId"];
    [keys addObject:@"accountPassword"];
    [keys addObject:@"from"];
    [keys addObject:@"fromEpoch"];
    [keys addObject:@"to"];
    [keys addObject:@"toEpoch"];
    [keys addObject:@"isSettled"];
    [keys addObject:@"marketId"];
    [keys addObject:@"startRow"];
    [keys addObject:@"pageSize"];
    [keys addObject:@"period"];
    return keys;
}

@end
