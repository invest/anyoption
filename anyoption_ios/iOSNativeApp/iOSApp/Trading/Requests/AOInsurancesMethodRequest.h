//
//  AOInsurancesMethodRequest.h
//  iOSApp
//
//  Created by Tony on 4/13/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOUserMethodRequest.h"

@interface AOInsurancesMethodRequest : AOUserMethodRequest

@property int insuranceType;
@property (strong, nonatomic) NSArray *investmentIds;
@property (strong, nonatomic) NSArray *insuranceAmounts;

@end
