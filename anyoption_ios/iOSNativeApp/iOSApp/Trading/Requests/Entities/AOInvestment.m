//
//  AOInvestment.m
//  JSON Test
//
//  Created by Anton Antonov on 2/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AOInvestment.h"
#import "UILabel+SSText.h"
#import "NSString+Utils.h"

@implementation AOInvestment

@synthesize l_id;
@synthesize opportunityId;
@synthesize typeId;
@synthesize amount;
@synthesize currentLevel;
@synthesize oddsWin;
@synthesize oddsLose;
@synthesize timeCreated;
@synthesize marketId;
@synthesize marketName;
@synthesize timeEstClosing;
@synthesize timeEstClosingTxt;
@synthesize insuranceAmountRU;
@synthesize asset;
@synthesize level=_level;
@synthesize expiryLevel;
@synthesize timePurchased;
@synthesize timePurchaseTxt;
@synthesize amountTxt;
@synthesize amountReturnWF;
@synthesize currentLevelTxt;
@synthesize oneTouchUpDown;
@synthesize optionPlusFee;
@synthesize additionalInfoText;
@synthesize additionalInfoType;
@synthesize bonusDisplayName;
@synthesize opportunityTypeId;
@synthesize chartXPosition;
@synthesize hasRibbon=_hasRibbon;
@synthesize settledInvestment;
@synthesize timeSettled;
#ifdef COPYOP
@synthesize copyopType;
#endif

- (id)init
{
    self = [super init];
    if (self) {
        optionsDateFormatter = [[NSDateFormatter alloc] init];
        [optionsDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [optionsDateFormatter setDateFormat:@"HH:mm"];
        
        settledOptionsDateFormatter = [[NSDateFormatter alloc] init];
        [settledOptionsDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [settledOptionsDateFormatter setDateFormat:@"dd/MM | HH:mm"];
        
        settledInvestment = NO;
    }
    return self;
}

- (void)setValuesForKeysWithDictionary:(NSMutableDictionary*)keyedValues
{
    if([keyedValues objectForKey:@"id"])
        [AOUtils renameKey:@"id" to:@"l_id" inDictionary:keyedValues];
    
    [AOUtils validateDictionary:keyedValues withObject:self];
    
    [super setValuesForKeysWithDictionary:keyedValues];
    
    timeCreated = [AOUtils parseTimeFromJSON:(NSString*)timeCreated];
    timeEstClosing = [AOUtils parseTimeFromJSON:(NSString*)timeEstClosing];
    timeSettled = [AOUtils parseTimeFromJSON:(NSString*)timeSettled];
    
    if(oddsWin>1){
        oddsWin-=1;
        oddsLose = 1-oddsLose;
    }
}

- (float) returnAtLevel:(float)l
{
    //if(self.badgeType==AOBadgeTypeSame) return self.sameReturn;
    
    if (self.hasRibbon) {
        return [self winReturn];
    } else {
        return [self loseReturn];
    }
}


-(NSString*)getPurchaseTimeFormatted{
    if([self settledInvestment]){
        return [settledOptionsDateFormatter stringFromDate:[AOUtils parseTimeFromJSON3:timePurchaseTxt]];
    }
    else{
        return [self timeWithoutZero:[optionsDateFormatter stringFromDate:[AOUtils parseTimeFromJSON3:timePurchaseTxt]]];
    }
}

-(NSString*)getExpiryTimeFormatted{
    NSString *time = [optionsDateFormatter stringFromDate:[AOUtils parseTimeFromJSON3:timeEstClosingTxt]];
    NSString *time2 = [self timeWithoutZero:time];
    return time2;
}

-(NSString*)timeWithoutZero:(NSString*)time{
    NSRange zeroRange = NSMakeRange(0,1);
    if([[time substringWithRange:zeroRange] isEqualToString:@"0"]) time = [time stringByReplacingCharactersInRange:zeroRange withString:@""];
    return time;
}

-(double)winReturn{
//    // NSLog(@"winz %f",[self amountWithFee]);
    return [self amountWithFee] * (1 + oddsWin);
}
-(double)loseReturn{
    return [self amountWithFee] * (1 - oddsLose);
}
-(double)sameReturn{
    return [self amountWithFee];
}
-(double)winReturnPercent{
    return round(oddsWin*100);
}

-(double)amountWithFee{
    return amount - insuranceAmountRU - optionPlusFee;
}

- (id)copyWithZone:(NSZone *)zone
{
    AOInvestment *copy = [[AOInvestment alloc] init];
    
    if (copy) {
        copy.additionalInfoText = self.additionalInfoText;
        copy.additionalInfoType = self.additionalInfoType;
        copy.amount = self.amount;
        copy.amountReturnWF = self.amountReturnWF;
        copy.amountTxt = self.amountTxt;
        copy.asset = self.asset;
        copy.bonusDisplayName = self.bonusDisplayName;
        copy.chartXPosition = self.chartXPosition;
        copy.currentLevel = self.currentLevel;
        copy.currentLevelTxt = self.currentLevelTxt;
        copy.expiryLevel = self.expiryLevel;
        copy.l_id = self.l_id;
        copy.insuranceAmountRU = self.insuranceAmountRU;
        copy.level = self.level;
        copy.marketId = self.marketId;
        copy.marketName = self.marketName;
        copy.oddsLose = self.oddsLose;
        copy.oddsWin = self.oddsWin;
        copy.oneTouchUpDown = self.oneTouchUpDown;
        copy.opportunityId = self.opportunityId;
        copy.opportunityTypeId = self.opportunityTypeId;
        copy.optionPlusFee = self.optionPlusFee;
        copy.timeCreated = self.timeCreated;
        copy.timeEstClosing = self.timeEstClosing;
        copy.timeEstClosingTxt = self.timeEstClosingTxt;
        copy.timePurchaseTxt = self.timePurchaseTxt;
        copy.timePurchased = self.timePurchased;
        copy.typeId = self.typeId;
        copy.newlyAdded = self.newlyAdded;
        copy.hasRibbon = self.hasRibbon;
        copy.timeSettled = self.timeSettled;
#ifdef COPYOP
        copy.copyopType = self.copyopType;
#endif
    }
    
    return copy;
}

-(void)setLevel:(NSString *)lvl
{
    _level = lvl;
    if (settledInvestment) {
        return;
    }
    
    
    if (self.hasRibbon) {
        amountReturnWF = [AOUtils formatCurrencyAmount:[self winReturn] / 100];
    } else {
        amountReturnWF = [AOUtils formatCurrencyAmount:[self loseReturn] / 100];
    }
}

-(BOOL)hasRibbon{
    
    if([self settledInvestment]) {
        if (self.amount / 100 > [AOUtils parseAmount:self.amountReturnWF]) {
            _hasRibbon = NO;
        } else {
            _hasRibbon = YES;
        }
        if ((self.additionalInfoType == AOInvestmentAdditionalInfoTypeRollForward)
            || (self.additionalInfoType == AOInvestmentAdditionalInfoTypeRollForwardBought)) {
            if ([self.timeSettled compare:self.timeEstClosing] == NSOrderedAscending) {
                _hasRibbon = NO;
            }
        }
        
//        float levelFloat = self.currentLevel;
//        float expiryLevelFloat = [AOUtils parseLevelFromLSUpdate:expiryLevel].floatValue;
//        
//       if ((self.typeId == AOInvestmentTypeCall && levelFloat < expiryLevelFloat) || (typeId == AOInvestmentTypePut && levelFloat > expiryLevelFloat)) {
//            _hasRibbon=YES;
//        }
//        else {
//            _hasRibbon=NO;
//            
//        }
    }
    else {
    
        if (nil != _level && ![_level isEmpty]) {
            _hasRibbon = [AOInvestment isInvestmentInTheMoneyWithType:typeId buyLevel:currentLevel checkLevel:_level];
          
        } else {
            _hasRibbon=NO;
           
        }
    }
    
    
    return _hasRibbon;

}

+ (BOOL) isInvestmentInTheMoneyWithType:(long)typeId buyLevel:(float)buyLevel checkLevel:(NSString*)checkLevel {
    float levelFloat = [AOUtils parseLevelFromLSUpdate:checkLevel].floatValue;
    if ((typeId == AOInvestmentTypeCall && levelFloat > buyLevel) || (typeId == AOInvestmentTypePut && levelFloat < buyLevel)) {
        return YES;
    } else {
        return NO;
    }
}

@end
