//
//  AOChartData.m
//  AnyOption
//
//  Created by  on 8/9/13.
//  Copyright (c) 2013 Anyoption. All rights reserved.
//

#import "AOChartData.h"
#import "AOInvestment.h"

@implementation AOChartData


@synthesize timezoneOffset;
@synthesize marketId;
@synthesize opportunityId;
@synthesize timeEstClosing;
@synthesize scheduled;
@synthesize decimalPoint;
@synthesize ratesTimes;
@synthesize rates;
@synthesize marketName;
@synthesize timeFirstInvest;
@synthesize timeLastInvest;
@synthesize investments;
@synthesize opportunityTypeId;
@synthesize hasPreviousHour;
@synthesize commission;
@synthesize marketingStaticPart;


- (void)setValuesForKeysWithDictionary:(NSMutableDictionary *)keyedValues
{
    [AOUtils validateDictionary:keyedValues withObject:self];
    
    [super setValuesForKeysWithDictionary:keyedValues];
    
    _timeFirstInvestDate = [AOUtils parseTimeFromJSON:(NSString*)timeFirstInvest];
    _timeLastInvestDate = [AOUtils parseTimeFromJSON:(NSString*)timeLastInvest];
    _timeEstClosingDate = [AOUtils parseTimeFromJSON:(NSString*)timeEstClosing];
    
    if (investments != nil && [investments count] > 0) {
        // NSLog(@"Got %lu investments in the dictionary.", (unsigned long)[investments count]);
        NSMutableArray *arrayOfInvestments = [[NSMutableArray alloc] init];
        for (NSDictionary *d in investments) {
            AOInvestment *i = [[AOInvestment alloc] init];
            [i setValuesForKeysWithDictionary:d];
            [arrayOfInvestments addObject:i];
        }
        investments = arrayOfInvestments;
    } else {
        // NSLog(@"No investments in the dictionary.");
    }
}

-(NSString*)description
{
    return [NSString
            stringWithFormat:@"%@,\n"
            "timezoneOffset: %i,\n"
            "marketId: %li,\n"
            "opportunityId: %li,\n"
            "timeEstClosing: %@,\n"
            "scheduled: %i,\n"
            "decimalPoint: %i,\n"
            "ratesTimes: %@,\n"
            "rates: %@,\n"
            "marketName: %@,\n"
            "timeFirstInvest: %@,\n"
            "timeLastInvest: %@,\n"
            "investments: %@,\n"
            "opportunityTypeId: %li,\n"
            "hasPreviousHour: %i, \n"
            "commission: %li \n",
            [super description],
            timezoneOffset,
            marketId,
            opportunityId,
            timeEstClosing,
            scheduled,
            decimalPoint,
            ratesTimes,
            rates,
            marketName,
            timeFirstInvest,
            timeLastInvest,
            investments,
            opportunityTypeId,
            hasPreviousHour,
            commission];
}


@end
