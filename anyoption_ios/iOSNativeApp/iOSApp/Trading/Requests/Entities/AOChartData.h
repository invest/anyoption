//
//  AOChartData.h
//  AnyOption
//
//  Created by  on 8/9/13.
//  Copyright (c) 2013 Anyoption. All rights reserved.
//

#import "AOUserResponceData.h"

@interface AOChartData : AOUserResponceData

@property int timezoneOffset;
@property long marketId;
@property long opportunityId;
@property (copy, nonatomic) NSString *timeEstClosing;
@property int scheduled;
@property int decimalPoint;
@property (strong, nonatomic) NSArray *ratesTimes;
@property (strong, nonatomic) NSArray *rates;
@property (copy, nonatomic) NSString *marketName;
@property (copy, nonatomic) NSString *timeFirstInvest;
@property (copy, nonatomic) NSString *timeLastInvest;
@property (copy, nonatomic) NSString *marketingStaticPart;
@property (strong, nonatomic) NSArray *investments;
@property long opportunityTypeId;
@property BOOL hasPreviousHour;
@property long commission;
@property long timeLastInvestMillsec;
@property long timeEstClosingMillsec;
@property long timeFirstInvestMillsec;

@property (copy, nonatomic) NSDate *timeEstClosingDate;
@property (copy, nonatomic) NSDate *timeFirstInvestDate;
@property (copy, nonatomic) NSDate *timeLastInvestDate;


@end


