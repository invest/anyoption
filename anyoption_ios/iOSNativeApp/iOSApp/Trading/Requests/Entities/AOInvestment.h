//
//  AOInvestment.h
//  JSON Test
//
//  Created by Anton Antonov on 2/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

enum {
    AOInvestmentTypeCall = 1,
    AOInvestmentTypePut = 2,
    AOInvestmentTypeOne = 3
};

enum {
    AOInvestmentOneTouchPut = 0,
    AOInvestmentOneTouchCall = 1
};

enum {
    AOBadgeTypeWin = 1,
    AOBadgeTypeLose = 2,
    AOBadgeTypeSame = 0
};

enum {
    AOInvestmentAdditionalInfoTypeRollForward = 1,
    AOInvestmentAdditionalInfoTypeRollForwardBought = 2,
    AOInvestmentAdditionalInfoTypeTakeProfit = 3,
    AOInvestmentAdditionalInfoTypeBonus = 4,
    AOInvestmentAdditionalInfoTypeOptionPlus = 5,
    AOInvestmentAdditionalInfoTypeNotHourly = 6
};

@interface AOInvestment : NSObject <NSCopying> {
    NSDateFormatter *optionsDateFormatter;
    NSDateFormatter *settledOptionsDateFormatter;
}

@property (copy, nonatomic) NSString *additionalInfoText;
@property (assign, nonatomic) int additionalInfoType;
@property (assign, nonatomic) double amount;
@property (copy, nonatomic) NSString *amountReturnWF;
@property (copy, nonatomic) NSString *amountTxt;
@property (copy, nonatomic) NSString *asset;
@property (copy, nonatomic) NSString *bonusDisplayName;
@property (assign, nonatomic) float chartXPosition;
@property (assign, nonatomic) float chartYPosition;
@property (assign, nonatomic) float currentLevel;  //investment Level
@property (copy, nonatomic) NSString *currentLevelTxt;
@property (copy, nonatomic) NSString *expiryLevel;
@property (assign, nonatomic) long l_id;
@property (assign, nonatomic) long insuranceAmountRU;
@property (copy, nonatomic) NSString *level;
@property (assign, nonatomic) long marketId;
@property (copy, nonatomic) NSString *marketName;
@property (assign, nonatomic) double oddsLose;
@property (assign, nonatomic) double oddsWin;
@property (assign, nonatomic) long oneTouchUpDown;
@property (assign, nonatomic) long opportunityId;
@property (assign, nonatomic) long opportunityTypeId;
@property (assign, nonatomic) long optionPlusFee;
@property (copy, nonatomic) NSDate *timeCreated;
@property (copy, nonatomic) NSDate *timeEstClosing;
@property (copy, nonatomic) NSString *timeEstClosingTxt;
@property (copy, nonatomic) NSString *timePurchaseTxt;
@property (copy, nonatomic) NSDictionary *timePurchased;
@property (assign, nonatomic) long typeId;
@property (assign, nonatomic) float pointerX;
@property (nonatomic) BOOL newlyAdded;
@property (nonatomic) BOOL hasRibbon;
@property (assign, nonatomic) BOOL settledInvestment;
@property (copy, nonatomic) NSDate *timeSettled;
#ifdef COPYOP
@property (nonatomic, copy) NSString *copyopType;
#endif



- (float) returnAtLevel:(float)l;
-(NSString*) getPurchaseTimeFormatted;
-(NSString*) getExpiryTimeFormatted;

-(double)winReturn;
-(double)loseReturn;
-(double)sameReturn;

-(double)winReturnPercent;

+ (BOOL) isInvestmentInTheMoneyWithType:(long)typeId buyLevel:(float)buyLevel checkLevel:(NSString*)checkLevel;

@end
