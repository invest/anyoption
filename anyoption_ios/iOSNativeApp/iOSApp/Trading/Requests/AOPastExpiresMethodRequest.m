//
//  AOInvestmentsMethodRequest.m
//  AnyOption
//
//  Created by mac_user on 11/5/13.
//  Copyright (c) 2013 . All rights reserved.
//

#import "AOPastExpiresMethodRequest.h"

@implementation AOPastExpiresMethodRequest

-(id)init{
    if(self = [super init]){
        self.serviceMethod = @"getPastExpiries";
    }
    return self;
}

-(NSMutableArray*)keys
{
    NSMutableArray *keys = [super keys];
    [keys addObject:@"date"];
    [keys addObject:@"marketId"];
    [keys addObject:@"marketGroupId"];
    [keys addObject:@"startRow"];
    [keys addObject:@"pageSize"];
    return keys;
}

@end
