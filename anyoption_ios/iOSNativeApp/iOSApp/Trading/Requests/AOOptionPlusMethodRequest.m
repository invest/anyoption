//
//  AOOptionPlusMethodRequest.m
//  iOSApp
//
//  Created by Tony on 4/19/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOOptionPlusMethodRequest.h"

@implementation AOOptionPlusMethodRequest

-(id)init{
    if(self = [super init]){
        self.serviceMethod = @"getPrice";
    }
    return self;
}

-(NSMutableArray*)keys
{
    NSMutableArray *keys = [super keys];
    [keys addObject:@"investmentId"];
    [keys addObject:@"price"];
    return keys;
}

@end
