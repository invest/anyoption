//
//  AOOptionPlusMethodRequest.h
//  iOSApp
//
//  Created by Tony on 4/19/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOUserMethodRequest.h"

@interface AOOptionPlusMethodRequest : AOUserMethodRequest

@property (assign, nonatomic) long investmentId;
@property (assign, nonatomic) double price;

@end
