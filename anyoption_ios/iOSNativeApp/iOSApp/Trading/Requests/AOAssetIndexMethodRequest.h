//
//  AOInvestmentsMethodRequest.h
//  AnyOption
//
//  Created by mac_user on 11/5/13.
//  Copyright (c) 2013 Anyoption. All rights reserved.
//

#import "AOMethodRequest.h"

@interface AOAssetIndexMethodRequest : AOMethodRequest

@property (assign, nonatomic) long marketId;

@end
