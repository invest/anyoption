//
//  AOTradeOptionsSettled1MCell.h
//  iOSApp
//
//  Created by Lyubomir Marinov on 11/19/15.
//  Copyright © 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AOTradeOptionsSettled1MCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@end
