//
//  AORollForwardButton.m
//  iOSApp
//
//  Created by Tony on 4/11/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AORollForwardButton.h"

@implementation AORollForwardButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
#if !COPYOP
        [self setupButtonWithActiveBackground:@"ins_btn_bg" disabledBackground:@"ins_btn_bg" activeIcon:@"ins_btn_icn_rf" disabledIcon:@"ins_btn_icn_rfd" label:AOLocalizedString(@"golden.my.inv.no.level.RU")];
#else
        [self setupButtonWithActiveBackground:nil disabledBackground:nil activeIcon:@"roll_forward" disabledIcon:@"roll_forward_off" label:AOLocalizedString(@"golden.my.inv.no.level.RU")];
#endif
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
