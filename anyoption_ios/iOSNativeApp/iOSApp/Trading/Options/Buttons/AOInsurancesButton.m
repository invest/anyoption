//
//  AOInsurancesButton.m
//  iOSApp
//
//  Created by Tony on 4/11/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOInsurancesButton.h"
#import "AOInsurance.h"

@implementation AOInsurancesButton {
    UIImageView *activeIconView;
    UIImageView *disabledIconView;
    UILabel *activeLabel;
    UILabel *timerLabel;
    UILabel *disabledLabel;
    NSTimer *timer;
    NSDate *insurancePeriodEndTime;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setupButtonWithActiveBackground:(NSString*)activeBackground disabledBackground:(NSString*)disabledBackground activeIcon:(NSString*)activeIcon disabledIcon:(NSString*)disabledIcon label:(NSString*)label {

    [self setBackgroundColor:[UIColor clearColor]];
    [self setBackgroundImage:[[UIImage imageNamed:activeBackground] resizableImageWithCapInsets:UIEdgeInsetsMake(16, 5, 16, 5)] forState:UIControlStateNormal];
    [self setBackgroundImage:[[UIImage imageNamed:disabledBackground] resizableImageWithCapInsets:UIEdgeInsetsMake(16, 5, 16, 5)] forState:UIControlStateDisabled];
    
    activeIconView = [[UIImageView alloc] init];
    activeIconView.image = [UIImage imageNamed:activeIcon];
    activeIconView.frame = CGRectMake(50, 0, 46, 37);

    activeIconView.hidden = true;
    [self addSubview:activeIconView];
    
    disabledIconView = [[UIImageView alloc] init];
    disabledIconView.image = [UIImage imageNamed:disabledIcon];
    disabledIconView.frame = CGRectMake(50, 0, 46, 37);

//    disabledIconView.hidden = true;
    [self addSubview:disabledIconView];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    activeLabel = [[UILabel alloc] init];
    activeLabel.numberOfLines = 0;
    activeLabel.textColor = [theme insuranceButtonTextColorNormal];
    activeLabel.text = label;
    activeLabel.font = [theme insuranceButtonFont];
//    activeLabel.adjustsFontSizeToFitWidth = true;
    activeLabel.textAlignment = NSTextAlignmentCenter;
    activeLabel.frame = CGRectMake(5, 8, 50, 0.0);

    [activeLabel sizeToFit];
//    CGPoint p = activeLabel.center;
//    p.y = CGRectGetMidY(self.bounds);
//    activeLabel.center = p;
    activeLabel.hidden = true;
    [self addSubview:activeLabel];
    
    timerLabel = [[UILabel alloc] init];
    timerLabel.textColor = [theme insuranceButtonTimerTextColor];
    timerLabel.text = @"";
    timerLabel.font = [theme insuranceButtonTimerFont];
//    timerLabel.adjustsFontSizeToFitWidth = true;
    timerLabel.textAlignment = NSTextAlignmentCenter;
    timerLabel.frame = CGRectMake(5, 19, 50, 10);
    timerLabel.hidden = true;
    [self addSubview:timerLabel];
    
    disabledLabel = [[UILabel alloc] init];
    disabledLabel.numberOfLines = 0;
    disabledLabel.textColor = [theme insuranceButtonTextColorDisabled];
    disabledLabel.text = label;
    disabledLabel.font = [theme insuranceButtonFont];
//    disabledLabel.adjustsFontSizeToFitWidth = true;
    disabledLabel.textAlignment = NSTextAlignmentCenter;
    disabledLabel.frame = CGRectMake(5, 13, 50, 0.0);

    [disabledLabel sizeToFit];
    
//    CGPoint pp = disabledLabel.center;
//    pp.y = CGRectGetMidY(self.bounds);
//    disabledLabel.center = pp;

//    disabledLabel.hidden = true;
    [self addSubview:disabledLabel];
    
    
//    [self setBackgroundImage:[[UIImage imageNamed:@"Button1-green.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(22, 5, 22, 5)] forState:UIControlStateNormal];
//    
//    [self setTitleColor:color forState:UIControlStateHighlighted];
//    [self setBackgroundImage:[[UIImage imageNamed:@"Button1-green-hover.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(22, 5, 22, 5)] forState:UIControlStateHighlighted];
//    
//    self.titleLabel.font = [UIFont fontWithName:APP_FONT_LIGHT size:20];
//    self.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,0);

//    CALayer *bottomBorder = [CALayer layer];
//    bottomBorder.frame = CGRectMake(0, separator.frame.size.height-0.5, separator.frame.size.width, 0.5);
//    bottomBorder.backgroundColor = AC_TAB_SEPARATORCOLOR.CGColor;
//    [separator.layer addSublayer:bottomBorder];

#ifdef COPYOP
    self.alpha = 0.6;
    self.type = ButtonTypeBlue;
    activeIconView.frame = CGRectMake(74, 7, 27, 27);
    disabledIconView.frame = CGRectMake(74, 7, 27, 27);
    activeLabel.frame = CGRectMake(5, 6, 80, 0.0);
    [activeLabel sizeToFit];
    disabledLabel.frame = CGRectMake(5, 13, 80, 0.0);
    [disabledLabel sizeToFit];
    timerLabel.textAlignment = NSTextAlignmentLeft;
    timerLabel.frame = CGRectMake(5, 23, 50, 10);
#endif
    
}

- (void) setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    // NSLog(@"setEnabled %i", enabled);
    if (enabled) {
        disabledIconView.hidden = YES;
        disabledLabel.hidden = YES;
        activeIconView.hidden = NO;
        activeLabel.hidden = NO;
        timerLabel.hidden = NO;
        
        if ([RESIDENT.insurances count] > 0) {
            AOInsurance *ins = [[RESIDENT.insurances allValues] objectAtIndex:0];
            insurancePeriodEndTime = ins.closeTime;
            
            timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTimeLeft) userInfo:nil repeats:YES];
        }
#ifdef COPYOP
        self.alpha = 1;
#endif
    } else {
        disabledIconView.hidden = NO;
        disabledLabel.hidden = NO;
        activeIconView.hidden = YES;
        activeLabel.hidden = YES;
        timerLabel.hidden = YES;
        
        [timer invalidate];
        timer = nil;
#ifdef COPYOP
        self.alpha = 0.6;
#endif
    }
}

- (void) updateTimeLeft {
    NSTimeInterval timeLeft = [insurancePeriodEndTime timeIntervalSinceDate:[AOUtils currentDatePrecise]];
    if (timeLeft < 0) {
        timerLabel.text = @"00:00";
        [timer invalidate];
        timer = nil;
        self.enabled = NO;
    } else {
        long timeLeftLong = (long) timeLeft;
        timerLabel.text = [NSString stringWithFormat:@"%1$02ld:%2$02ld", timeLeftLong / 60, timeLeftLong % 60];
    }
}

@end
