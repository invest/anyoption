//
//  GetQuoteButton.h
//  iOSApp
//
//  Created by  on 4/24/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOInvestment.h"

@interface GetQuoteButton : UIButton
@property (nonatomic,strong) AOInvestment* investment;
@end
