//
//  AOInsurancesButton.h
//  iOSApp
//
//  Created by Tony on 4/11/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>
#ifdef COPYOP
#import "AOBlueButton.h"
#endif

#ifdef COPYOP
@interface AOInsurancesButton : AOBlueButton
#else
@interface AOInsurancesButton : UIButton
#endif

- (void) setupButtonWithActiveBackground:(NSString*)activeBackground disabledBackground:(NSString*)disabledBackground activeIcon:(NSString*)activeIcon disabledIcon:(NSString*)disabledIcon label:(NSString*)label;

@end
