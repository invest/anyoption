//
//  AOTradeSettledOptionsViewController.m
//  iOSApp
//
//  Created by Tony on 6/12/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOTradeSettledOptionsViewController.h"
#import "AOInvestmentsMethodRequest.h"
#import "AORequestService.h"
#import "AOTradeOptionsViewCell.h"
#import "AOTradeOptionsSettled1MCell.h"
#import "AOOpportunity.h"
#import "AOMarket.h"
#import "AOTradeOptionsHeaderViewCell.h"
#ifdef COPYOP
#import "COCopyUserController.h"
#import "COShowoffViewController.h"
#endif

#ifdef COPYOP
@interface AOTradeSettledOptionsViewController ()
#else
@interface AOTradeSettledOptionsViewController ()
#endif

@property (weak, nonatomic) IBOutlet UIView *tableSectionHeader;
@property (weak, nonatomic) IBOutlet AOLabel *assetCaption;
@property (weak, nonatomic) IBOutlet AOLabel *purchasedCaption;
@property (weak, nonatomic) IBOutlet AOLabel *currentCaption;


@end

@implementation AOTradeSettledOptionsViewController {
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        pageSize = 12;
    }
    return self;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    UIFont *headerFont = [theme openOptionsTableHeaderFont];
    UIColor *headerColor = [theme openOptionsTableHeaderTextColor];
    for (UILabel *captionLabel in @[self.assetCaption, self.purchasedCaption, self.currentCaption])
    {
        captionLabel.font = headerFont;
        captionLabel.textColor = headerColor;
        
        if (IS_RIGHT_TO_LEFT)
        {
            captionLabel.textAlignment = NSTextAlignmentRight;
        }
    }
    
    self.tableSectionHeader.backgroundColor = [theme settledOptionsHeaderBackgroundColor];
    
#ifdef ETRADER
    self.currentCaption.text = AOLocalizedString(@"assetIndex.closingLevel");
#endif
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // resolves the issue when the user presses quickly a table view cell while the dataSource is being empty
    [self.table reloadData];
    [self.table layoutIfNeeded];
}

- (void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //[self buildTitleViewWithText:AOLocalizedString(@"investments.close") andImage:nil];  //removed because it is integrated in my options
#ifdef COPYOP
    //[self buildLogo];  //removed because it is integrated in my options
#endif
}

- (void) reloadData {
    int pgSize = (int)dataSource.count;
    if (pgSize <= 13)
    {
        pgSize++; // quick fix if there's an update pending (withdraw, reverse withdraw, etc...)
    }
    [dataSource removeAllObjects];
    startRow = 0;
    [self loadDataPageSize:pgSize startRow:0];
}

- (void) loadDataPageSize:(int)pSize startRow:(int)sRow {
    // NSLog(@"satisfyMyLove");

    if (!loading) {
        [self startLoading];
        AOInvestmentsMethodRequest *request = [[AOInvestmentsMethodRequest alloc] init];
        request.isSettled = @YES;
        request.pageSize = pSize;
        request.startRow = sRow;
        
        if(![[NSUserDefaults standardUserDefaults] integerForKey:USER_DEFS_SETTLED_SELECTED_FILTER]) {
            request.period = 1;
        } else {
            switch ([[NSUserDefaults standardUserDefaults] integerForKey:USER_DEFS_SETTLED_SELECTED_FILTER]) {
                case 0:
                    request.period = 1;
                    break;
                case 1:
                    request.period = 2;
                    break;
                case 2:
                    request.period = 3;
                    break;
                    
                default:
                    break;
            }
        }
        
        [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary *resultDictionary) {
            // NSLog(@"AOInvestmentsMethodRequest \n\n %@", resultDictionary);
            
            NSArray *investments = [resultDictionary objectForKey:@"investments"];
            for (id investmentDict in investments) {
                AOInvestment *investment = [[AOInvestment alloc] init];
                investment.settledInvestment = YES;
                [investment setValuesForKeysWithDictionary:investmentDict];
//                investment.level = investment.expiryLevel;
//                investment.hasRibbon = [AOInvestment isInvestmentInTheMoneyWithType:investment.typeId buyLevel:investment.currentLevel checkLevel:investment.expiryLevel];
//                if (investment.amount / 100 > [AOUtils parseAmount:investment.amountReturnWF]) {
//                    investment.hasRibbon = NO;
//                } else {
//                    investment.hasRibbon = YES;
//                }
//                if ((investment.additionalInfoType == AOInvestmentAdditionalInfoTypeRollForward)
//                    || (investment.additionalInfoType == AOInvestmentAdditionalInfoTypeRollForwardBought)) {
//                    if ([investment.timeSettled compare:investment.timeEstClosing] == NSOrderedAscending) {
//                        investment.hasRibbon = NO;
//                    }
//                }
                
                [dataSource addObject:investment];
            }
            
            hasDataToLoad = investments.count == pSize;
            startRow += investments.count;
            [self stopLoading];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.table reloadData];
                
                self.table.contentOffset = CGPointMake(0, 1);
                self.table.contentOffset = CGPointMake(0, 0);
            });
        } failed:^(NSDictionary *errDictionary) {
            SHOW_DEFAULT_ERROR_NOTIFICATION();
            [self stopLoading];
        }];
    } else {
        // NSLog(@"Loading already in progress...");
    }
}

#pragma mark - Methods of UITableViewDataSource


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    if(indexPath.row < [dataSource count]) {
        AOTradeOptionsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentCell"];
        cell.contentView.backgroundColor = [theme settledOptionsCellBackgroundColor];
        
        AOInvestment *investment = [dataSource objectAtIndex:indexPath.row];
        
        cell.invId = investment.l_id;
        cell.iconMarketBorder.backgroundColor = [theme assetsLogoBorderColor];
        cell.iconMarket.backgroundColor = MA_ICON_BG_COLOR;
        cell.iconMarket.image = [AOUtils iconForMarket:investment.marketId];
        cell.iconMarketPlus.hidden = investment.opportunityTypeId != AOOpportunityTypeOptionPlus;
        
        cell.marketName.textColor = [theme settledOptionsTextColor];
        cell.marketName.text = investment.asset;
        cell.marketName.font = [theme settledOptionsMarketNameFont];
        
        cell.timeCreated.textColor = [theme settledOptionsTextColor];
        cell.timeCreated.text = investment.timePurchaseTxt;
        cell.timeCreated.font = [theme settledOptionsDefaultFont];
        
        cell.border1.backgroundColor = [theme settledOptionsBorderColor];
        
        cell.level.textColor = [theme settledOptionsTextColor];
        cell.level.text = investment.currentLevelTxt;
        cell.level.font = [theme settledOptionsDefaultFont];
        
        cell.amount.textColor = [theme settledOptionsTextColor];
        cell.amount.text = investment.amountTxt;
        cell.amount.font = [theme settledOptionsDefaultFont];
        
        cell.border2.backgroundColor = [theme settledOptionsBorderColor];
        
        cell.current.backgroundColor = [theme settledOptionsCurrentOutBackgroundColor];
        
        cell.currentLevel.textColor = ATO_CELL_TEXT_WHITE;
        cell.currentLevel.text = investment.expiryLevel;
        cell.currentLevel.font = [theme settledOptionsDefaultFont];
        cell.currentReturn.textColor = ATO_CELL_TEXT_WHITE;
        cell.currentReturn.text = investment.amountReturnWF;
        cell.currentReturn.font = [theme settledOptionsDefaultFont];
        if (investment.additionalInfoType == AOInvestmentAdditionalInfoTypeRollForward) {
            cell.currentLevel.text = AOLocalizedString(@"golden.my.inv.no.level.RU");
            cell.currentReturn.text = AOLocalizedString(@"investments.iscanceled");
        } else if (investment.additionalInfoType == AOInvestmentAdditionalInfoTypeTakeProfit) {
            cell.currentLevel.text = AOLocalizedString(@"golden.my.inv.no.level.GM");
        } else if (investment.additionalInfoType == AOInvestmentAdditionalInfoTypeOptionPlus) {
            if ([investment.timeSettled compare:investment.timeEstClosing] == NSOrderedAscending) {
#ifdef ETRADER
                cell.currentLevel.text = AOLocalizedString(@"home.page.tab.2");
#else
                cell.currentLevel.text = AOLocalizedString(@"CMS.optionPlus.metadata.keywords");
#endif
            }
        }
        
        cell.borderBottom.backgroundColor = [theme settledOptionsBorderColor];
        
        [self setInTheMoneyForCell:cell forInvestment:investment];
        
        return cell;
    } else {
        AOTradeOptionsSettled1MCell *cell = [tableView dequeueReusableCellWithIdentifier:@"settled1MCell"];
        cell.contentView.backgroundColor = [theme settledOptionsCellBackgroundColor];

        cell.messageLabel.numberOfLines = 2;

#if ANYOPTION
        cell.messageLabel.text = [NSString stringWithFormat:AOLocalizedString(@"settledMoreInfo"), @"anyoption"];
#elif COPYOP
        cell.messageLabel.text = [NSString stringWithFormat:AOLocalizedString(@"settledMoreInfo"), @"copyop"];
#else
        cell.messageLabel.text = [NSString stringWithFormat:AOLocalizedString(@"settledMoreInfo"), @"etrader"];
#endif
        
        return cell;
    }
}

- (void) setInTheMoneyForCell:(AOTradeOptionsViewCell*)cell forInvestment:(AOInvestment*)investment {
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    if (investment.hasRibbon) {
#ifdef COPYOP
        cell.showoffImageView.backgroundColor = [UIColor colorWithHexString:@"#ffe154"];
        cell.showoffImageView.image = [UIImage imageNamed:@"showoff_icon"];
        cell.showoffImageView.hidden = NO;
        
        UITapGestureRecognizer *showoffTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleShowoffTap:)];
        [cell.current addGestureRecognizer:showoffTapRecognizer];
#endif
        
        if (investment.typeId == AOInvestmentTypeCall) {
            cell.iconType.image = [UIImage imageNamed:@"triangle_up_small_yellow"];
#ifdef COPYOP
            if ([investment.copyopType isEqualToString:@"COPY"]) {
                cell.iconType.image = [UIImage imageNamed:@"copied_up_winning"];
            }
#endif
        } else {
            cell.iconType.image = [UIImage imageNamed:@"triangle_down_small_yellow"];
#ifdef COPYOP
            if ([investment.copyopType isEqualToString:@"COPY"]) {
                cell.iconType.image = [UIImage imageNamed:@"copied_down_winning"];
            }
#endif
        }
        cell.current.backgroundColor = [theme settledOptionsCurrentInBackgroundColor];
        cell.iconRibbon.hidden = NO;
    } else {
#ifdef COPYOP
        cell.showoffImageView.hidden = YES;
#endif
        
        if (investment.typeId == AOInvestmentTypeCall) {
            cell.iconType.image = [UIImage imageNamed:@"triangle_up_small_blue"];
#ifdef COPYOP
            if ([investment.copyopType isEqualToString:@"COPY"]) {
                cell.iconType.image = [UIImage imageNamed:@"copied_up_losing"];
            }
#endif
        } else {
            cell.iconType.image = [UIImage imageNamed:@"triangle_down_small_blue"];
#ifdef COPYOP
            if ([investment.copyopType isEqualToString:@"COPY"]) {
                cell.iconType.image = [UIImage imageNamed:@"copied_down_losing"];
            }
#endif
        }
        cell.current.backgroundColor = [theme settledOptionsCurrentOutBackgroundColor];
        cell.iconRibbon.hidden = YES;
    }
    
#ifdef COPYOP
    if ([investment.copyopType isEqualToString:@"COPY"]) {
        UITapGestureRecognizer *showoffTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleCopiedOpetionTapped:)];
        [cell.middleView addGestureRecognizer:showoffTapRecognizer];
    }
    else{
        for (UIGestureRecognizer *recognizer in cell.middleView.gestureRecognizers) {
            [cell.middleView removeGestureRecognizer:recognizer];
        }
    }
#endif
}

#pragma mark - Methods of UITableViewDelegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.table deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.row < [dataSource count]) {
        if (!loading)
        {
            AOInvestment *investment = [dataSource objectAtIndex:indexPath.row];
            AOMarket *market = [[AOMarket alloc] init];
            market.l_id = investment.marketId;
            market.displayName = investment.asset;
            market.isOptionPlusMarket = investment.opportunityTypeId == AOOpportunityTypeOptionPlus;
#ifdef COPYOP
            if(market.isOptionPlusMarket){
                return;
            }
#endif
            [MAINVC loadPage:@"Trade" pageName:IS_IPAD?ASSET_DETAILS_IPAD_PAGE_ID:ASSET_DETAILS_PAGE_ID root:NO options:@{@"market":market}];
        }
    }
}

#ifdef COPYOP
- (void)handleShowoffTap:(UIGestureRecognizer *)tapper {
    CGPoint tapLocation = [tapper locationInView:self.table];
    NSIndexPath *tappedIndexPath = [self.table indexPathForRowAtPoint:tapLocation];
    AOInvestment *selectedInvestment = [dataSource objectAtIndex:tappedIndexPath.row];
    COInvestmentInfo *info = [[COInvestmentInfo alloc] init];
    info.marketId = selectedInvestment.marketId;
    info.winReturn = selectedInvestment.winReturn;
    info.expiryTime = [selectedInvestment getExpiryTimeFormatted];
    info.amount = selectedInvestment.amount;
    info.copyopInvId = selectedInvestment.l_id;
    
    [COCopyUserController showShowoffPopupWithInvestmentInfo:info parentController:self completion:nil];
}

-(void)handleCopiedOpetionTapped:(UIGestureRecognizer *)tapper  {
    
    CGPoint tapLocation = [tapper locationInView:self.table];
    NSIndexPath *tappedIndexPath = [self.table indexPathForRowAtPoint:tapLocation];
    AOInvestment *selectedInvestment = [dataSource objectAtIndex:tappedIndexPath.row];
    
    
    [self.facade getOriginalInvestmentInfoFor:selectedInvestment.l_id andCompletion:^(COProfile *originalProfile, AOResponse * response) {
        
        if (response.error != nil && response.error.code != SERVICE_ERROR_USER_REMOVED)
        {
            [response.error show];
            
            return;
        }
        //error check
        
        [COCopyUserController  showOriginalInvestmentInfo:originalProfile parentController:self completion:nil ];
    }];
}

#endif

@end
