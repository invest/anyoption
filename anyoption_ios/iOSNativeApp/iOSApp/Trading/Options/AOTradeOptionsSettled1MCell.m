//
//  AOTradeOptionsSettled1MCell.m
//  iOSApp
//
//  Created by Lyubomir Marinov on 11/19/15.
//  Copyright © 2015 Anyoption. All rights reserved.
//

#import "AOTradeOptionsSettled1MCell.h"

@implementation AOTradeOptionsSettled1MCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
