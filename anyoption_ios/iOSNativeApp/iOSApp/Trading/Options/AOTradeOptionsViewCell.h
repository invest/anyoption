//
//  AOTradeOptionsViewCell.h
//  iOSApp
//
//  Created by Tony on 4/7/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AOTradeOptionsViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *iconMarketBorder;
@property (weak, nonatomic) IBOutlet UIImageView *iconMarket;
@property (weak, nonatomic) IBOutlet UIImageView *iconMarketPlus;
@property (weak, nonatomic) IBOutlet UILabel *marketName;
@property (weak, nonatomic) IBOutlet UILabel *timeCreated;
@property (weak, nonatomic) IBOutlet UIView *border1;
@property (weak, nonatomic) IBOutlet UIImageView *iconType;
@property (weak, nonatomic) IBOutlet UILabel *level;
@property (weak, nonatomic) IBOutlet UILabel *amount;
@property (weak, nonatomic) IBOutlet UIView *border2;
@property (weak, nonatomic) IBOutlet UIView *current;
@property (weak, nonatomic) IBOutlet UIView *middleView;
@property (weak, nonatomic) IBOutlet UILabel *currentLevel;
@property (weak, nonatomic) IBOutlet UILabel *currentReturn;
@property (weak, nonatomic) IBOutlet UIImageView *iconRibbon;
@property (weak, nonatomic) IBOutlet UIView *borderBottom;
@property (weak, nonatomic) IBOutlet UIImageView *showoffImageView;


@property (assign, nonatomic) long invId;

@end
