//
//  AOTradeOptionsViewCell.m
//  iOSApp
//
//  Created by Tony on 4/7/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOTradeOptionsViewCell.h"

#import "UIImage+Utils.h"

@implementation AOTradeOptionsViewCell{
    
    __weak IBOutlet UIImageView *infoIcon;
    __weak IBOutlet UIButton *dbbButton;
    __weak IBOutlet UIImageView *infoIconReturn;

    __weak IBOutlet UIImageView *infoIconAmount;
    __weak IBOutlet UIButton *dbbReturn;
    
    __weak IBOutlet UIButton *dbbAmount;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    

#ifndef ANYOPTION
    infoIcon.hidden = YES;
    infoIconReturn.hidden = YES;
    dbbButton.hidden = YES;
    dbbReturn.hidden = YES;
    dbbAmount.hidden = YES;
    infoIconReturn.hidden = YES;
    infoIconAmount.hidden = YES;
    
#endif
    
    if (IS_RIGHT_TO_LEFT)
    {
        self.iconRibbon.image = [[UIImage imageNamed:@"misc_gold-badge"] flippedImage];
    }
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    self.marketName.font = [theme openOptionsCellMarketNameFont];
    
    for (UILabel *label in @[_timeCreated, _level, _amount, _currentLevel, _currentReturn])
    {
        label.font = [theme openOptionsCellFont];
    }
}
#ifdef ANYOPTION
- (IBAction)showDBBalance:(id)sender {
    UIButton* showdbbButton = (UIButton*)sender;
    NSDictionary* data = @{@"invId":@(self.invId),@"type":@(showdbbButton.tag==99?DepositBonusBalanceTypeOpen:DepositBonusBalanceTypeSettledAmount )};
    SHOW_POPUP(AODepositBalanceSplit, data, YES, nil);

}
- (IBAction)showDBBReturnBalance:(id)sender {
    NSDictionary* data = @{@"invId":@(self.invId),@"type":@(DepositBonusBalanceTypeSettledReturn )};
    SHOW_POPUP(AODepositBalanceSplit, data, YES, nil);
}
#endif

@end
