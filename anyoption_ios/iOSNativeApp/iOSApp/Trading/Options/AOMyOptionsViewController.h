//
//  AOMyOptionsViewController.h
//  iOSApp
//
//  Created by  on 1/8/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AOBaseViewController.h"

@interface AOMyOptionsViewController : AOBaseViewController 
- (void) updateInsurances;
@end
