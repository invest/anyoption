//
//  AOTradeOptionPlusQuoteViewController.m
//  iOSApp
//
//  Created by Tony on 4/18/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOTradeOptionPlusQuoteViewController.h"
#import "AOOptionPlusMethodRequest.h"
#import "AORequestService.h"
#import "AOMarket.h"
#import "UIImage+Utils.h"

@interface AOTradeOptionPlusQuoteViewController () {
    CALayer *progressBarMask;
    NSString *currentLevelPattern;
    NSMutableArray *lsTableInfoArray;
    double price;
    NSTimer *timer;
    int timerTicks;
    BOOL expired;
    
    BOOL shouldClose;
}

@end

@implementation AOTradeOptionPlusQuoteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/*
 * The progress bar is implemented on 4 layers:
 * - lowest one is a view with the size of the progress bar with the "full" color
 * - on top of it text with color that should be visible on empty bar
 * - on top of it text with color that should be visible on full bar
 * - on top (upper most layer) a mask that will make the full bar and full text visible. We set the with of the mask to make the progress move.
 */
- (void) viewDidLoad {
    [super viewDidLoad];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    if (self.navOptions[@"investment"])
    {
        self.investment = self.navOptions[@"investment"];
    }
    
    if (IS_RIGHT_TO_LEFT)
    {
        _ribbonIcon.image = [[UIImage imageNamed:@"misc_gold-badge"] flippedImage];
    }
    
    for (UILabel *topCaptionLabel in @[_timeCaption, _levelCaption, _investmentAmountCaption])
    {
        topCaptionLabel.font = [theme quoteTopCaptionsFont];
        topCaptionLabel.textColor = [theme quoteTopCaptionsTextColor];
    }
    
    for (UILabel *topValueLabel in @[_time, _level, _investmentAmount])
    {
        topValueLabel.font = [theme quoteTopValuesFont];
        topValueLabel.textColor = [theme quoteTopValuesTextColor];
    }
    
    for (UILabel *timerCaptionLabel in @[_progressBarLine1Over, _messageLine1])
    {
        timerCaptionLabel.font = [theme quoteTimerCaptionFont];
        timerCaptionLabel.textColor = [theme quoteTimerCaptionTextColor];
    }
    
    for (UILabel *timerValueLabel in @[_progressBarLine2Over, _messageLine2])
    {
        timerValueLabel.font = [theme quoteTimerFont];
        timerValueLabel.textColor = [theme quoteTimerTextColor];
    }
    
    _marketName.textColor = OPP_TEXT_BLUE_COLOR;
    _currentLevel.textColor = OPP_TEXT_BLUE_COLOR;
    _expiry.textColor = OPP_TEXT_BLUE_COLOR;
    _border.backgroundColor = OPP_INVESTMENT_BORDER_COLOR;
    _progressBarContainer.backgroundColor = OPP_PROGRESS_EMPTY_COLOR;
    _progressBar.backgroundColor = OPP_PROGRESS_FULL_COLOR;
    _progressBarLine1Under.textColor = OPP_TEXT_BLUE_COLOR;
    _progressBarLine1Over.textColor = OPP_TEXT_PROGRESS_FULL_COLOR;
    _progressBarLine2Under.textColor = OPP_TEXT_BLUE_COLOR;
    _progressBarLine2Over.textColor = OPP_TEXT_PROGRESS_FULL_COLOR;
    _progressBarLine2Over.text = [NSString stringWithFormat:@"%1$02d:%2$02d", (70 - timerTicks) / 10, ((70 - timerTicks) % 10) * 10];
    _progressBarLine2Under.text = _progressBarLine2Over.text;
    _progressBarSuccess.hidden = YES;

    _messageLine1.textColor = OPP_TEXT_BLUE_COLOR;
    _messageLine2.textColor = OPP_TEXT_BLUE_COLOR;
    _messageLine2.text = [AOUtils formatCurrencyAmount:0.00];
    
    [_okButton setTitle:AOLocalizedString(@"alert.button") forState:UIControlStateNormal];
    [_okButton setTitleColor:OPP_BTN_OK_TEXT_COLOR forState:UIControlStateNormal];
    _okButton.hidden = YES;

    [_notNowButton setTitle:AOLocalizedString(@"optionPlus.button.not.now") forState:UIControlStateNormal];
    [_notNowButton setTitleColor:[theme quoteCancelButtonTextColor] forState:UIControlStateNormal];
    _notNowButton.exclusiveTouch = YES;
    
    [_sellButton setTitleColor:[theme quoteActionButtonTextColor] forState:UIControlStateNormal];
    _sellButton.titleLabel.font = [theme quoteActionButtonFont];
    [_sellButton setTitle:AOLocalizedString(@"optionPlus.buy") forState:UIControlStateNormal];
    [_sellButton setTitle:AOLocalizedString(@"optionPlus.buy") forState:UIControlStateDisabled];
    _sellButton.exclusiveTouch = YES;
    
    if (_investment.typeId == AOInvestmentTypeCall) {
        _investmentTypeIcon.image = [UIImage imageNamed:@"icn_opp_call"];
    } else {
        _investmentTypeIcon.image = [UIImage imageNamed:@"icn_opp_put"];
    }
    _marketName.text = _investment.asset;
    currentLevelPattern = [NSString stringWithFormat:@"%@: ", AOLocalizedString(@"profitLine.currentLevel")];
    _currentLevel.text = [NSString stringWithFormat:@"%@%@", currentLevelPattern, _investment.level];
    _currentLevel.font = [theme quoteCurrentLevelFont];
    _expiry.text = [NSString stringWithFormat:@"%@ %@", AOLocalizedString(@"expire.level.expires"), _investment.timeEstClosingTxt];
    _expiry.font = [theme quoteExpiryLevelFont];
    _time.text = [AOUtils formatTimeShort:_investment.timeCreated];
    _level.text = _investment.currentLevelTxt;
    _investmentAmount.text = _investment.amountTxt;
    _ribbonIcon.hidden = !_investment.hasRibbon;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackground)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:[UIApplication sharedApplication]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillEnterForeground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:[UIApplication sharedApplication]];
    
    [self addLsTables];
    [self getQuote];
}

- (void) viewWillAppear:(BOOL)animated {
    progressBarMask = [CALayer layer];
    progressBarMask.backgroundColor = [UIColor whiteColor].CGColor;
    progressBarMask.frame = CGRectMake(0, 0, 0, _progressBar.frame.size.height);
//    _progressBarLine1Over.layer.mask = progressBarMask;
//    _progressBarLine2Over.layer.mask = progressBarMask;
    _progressBar.layer.mask = progressBarMask;
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (shouldClose) [self closeScreen:nil];
    
    
}

-(void)dealloc{
    // NSLog(@"AOTradeOptionPlusQuoteViewController - dealloc - shouldClose: %i", shouldClose);
    [self removeLsTables];
}

- (void) addLsTables {
    [self removeLsTables];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    NSString *item = [NSString stringWithFormat:@"op_%ld", _investment.marketId];
    [items addObject:item];
    
    LSExtendedTableInfo *lsTableInfo = [LSExtendedTableInfo extendedTableInfoWithItems:items mode:LSModeCommand fields:RESIDENT.lsConnectionHandler.fields dataAdapter:LS_DATA_ADAPTER snapshot:YES];
    lsTableInfo.requestedMaxFrequency= 1.0;
    
    [RESIDENT.lsConnectionHandler addTableWithTableInfo:lsTableInfo delegate:self];
    [lsTableInfoArray addObject:lsTableInfo];
}

- (void) removeLsTables {
    if (lsTableInfoArray.count == 0) {
        return;
    }
    
    for (LSExtendedTableInfo *tableInfo in lsTableInfoArray) {
        [RESIDENT.lsConnectionHandler removeTableWithTableInfo:tableInfo];
    }
    
    [lsTableInfoArray removeAllObjects];
}

- (void) getQuote {
    _sellButton.enabled = NO;
    AOOptionPlusMethodRequest *request = [AOOptionPlusMethodRequest new];
    request.investmentId = _investment.l_id;
    [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary *resultDictionary) {
        
        if (resultDictionary==nil || (NSNull*)resultDictionary==[NSNull null] || [resultDictionary objectForKey:@"price"]==[NSNull null] ) {
            return ;
        }
        
        price = [[resultDictionary objectForKey:@"price"] doubleValue];
        _messageLine1.text = AOLocalizedString(@"CMS.optionplus.popup.text.ready.buy");
        _messageLine2.text = [resultDictionary objectForKey:@"priceTxt"];
        _notNowButton.hidden = NO;
        
        _progressBarLine1Under.hidden = NO;
        _progressBarLine1Over.hidden = NO;
        
        expired = NO;
    
        _sellButton.enabled = YES;
        _sellButton.hidden = NO;

        [_sellButton setTitle:AOLocalizedString(@"optionPlus.buy") forState:UIControlStateNormal];
        [_sellButton setTitle:AOLocalizedString(@"optionPlus.buy") forState:UIControlStateDisabled];

        timerTicks = 0;
        timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateTimeLeft) userInfo:nil repeats:YES];
    } failed:^(NSDictionary *errDictionary) {
        SHOW_DEFAULT_ERROR_NOTIFICATION();
        _sellButton.enabled = YES;
        [self closeScreen:nil];
        shouldClose=YES;
    }];
}

- (void) sell {
    _sellButton.enabled = NO;
    AOOptionPlusMethodRequest *request = [AOOptionPlusMethodRequest new];
    request.serviceMethod = @"optionPlusFlashSell";
    request.investmentId = _investment.l_id;
    request.price = price;
    
    [self stopTimer];
    
    [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary *resultDictionary) {
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        progressBarMask.frame = CGRectMake(0, 0, _progressBar.frame.size.width, _progressBar.frame.size.height);
        [CATransaction commit];
        _progressBar.backgroundColor = OPP_PROGRESS_SUCCESS_COLOR;
        _progressBarLine1Under.hidden = YES;
        _progressBarLine1Over.hidden = YES;
        _progressBarLine2Under.hidden = YES;
        _progressBarLine2Over.hidden = YES;
        _progressBarSuccess.hidden = NO;
        _messageLine1.text = AOLocalizedString(@"CMS.optionplus.popup.text.amount.add");
        _notNowButton.hidden = YES;
        _sellButton.hidden = YES;
        _okButton.hidden = NO;
    
        [TrackingManager trackEvent:TrackingEventTypeOptionSold options:nil];
    } failed:^(NSDictionary *errorDictionary) {
        SHOW_USER_NOTIFICATIONS(errorDictionary);
        _sellButton.enabled = YES;
        [self closeScreen:nil];
        shouldClose=YES;
    }];
}

- (void) updateTimeLeft {
    timerTicks++;
    _progressBarLine2Over.text = [NSString stringWithFormat:@"%1$02d:%2$02d", (70 - timerTicks) / 10, ((70 - timerTicks) % 10) * 10];
    _progressBarLine2Under.text = _progressBarLine2Over.text;

    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat:0.1] forKey:kCATransactionAnimationDuration];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
    float progress = (float) timerTicks / 70;
    progressBarMask.frame = CGRectMake(0, 0, _progressBar.frame.size.width * progress, _progressBar.frame.size.height);
    [CATransaction commit];
    
    if (timerTicks >= 70) {
        [self quoteExpired];
    }
}

- (void) quoteExpired {
    expired = YES;
    [self stopTimer];
    _progressBarLine1Under.hidden = YES;
    _progressBarLine1Over.hidden = YES;
    _progressBarLine2Under.text = AOLocalizedString(@"bonus.state6");
    _progressBarLine2Over.text = _progressBarLine2Under.text;
    _messageLine1.text = AOLocalizedString(@"CMS.optionplus.popup.text.text1");
    _messageLine2.text = AOLocalizedString(@"optionPlus.getPrice.note.expired2");
    
    [_sellButton setTitle:AOLocalizedString(@"optionPlus.getQuote") forState:UIControlStateNormal];
    [_sellButton setTitle:AOLocalizedString(@"optionPlus.getQuote") forState:UIControlStateDisabled];
    _sellButton.enabled = YES;
    _sellButton.hidden = NO;
}

- (void) stopTimer {
    if (nil != timer) {
        // NSLog(@"Stop option+ quote timer.");
        [timer invalidate];
        timer = nil;
    }
}

- (IBAction)submit:(id)sender {
    if (expired) {
        [self getQuote];
    } else {
        [self sell];
    }
}

- (IBAction) closeScreen:(id)sender {
    [self removeLsTables];
    [self stopTimer];
    if (!IS_IPAD)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [MAINVC setLockOverlay:NO];
        [MAINVC didTapOutside];
        
        AOMarket* newMarket = [AOMarket marketWithId:self.investment.marketId andName:self.investment.marketName];
        newMarket.isOptionPlusMarket = YES;
        newMarket.displayName = self.investment.asset;
#ifdef COPYOP
        if(newMarket.isOptionPlusMarket){
            return;
        }
#endif
        [MAINVC loadPage:@"Trade" pageName:IS_IPAD?ASSET_DETAILS_IPAD_PAGE_ID:ASSET_DETAILS_PAGE_ID root:NO options:@{@"market":newMarket}];
    }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    progressBarMask.frame = CGRectMake(0, 0, _progressBar.frame.size.width, _progressBar.frame.size.height);
}

-(void)applicationDidEnterBackground {
    // NSLog(@"AOTradeOptionPlusQuoteViewController - applicationDidEnterBackground");
}

- (void)applicationWillEnterForeground {
    // NSLog(@"AOTradeOptionPlusQuoteViewController - applicationDidBecomeActive");
    [self closeScreen:nil];
}

#pragma mark - Methods of LSTableDelegate

- (void) table:(LSSubscribedTableKey *)tableKey itemPosition:(int)itemPosition itemName:(NSString *)itemName didUpdateWithInfo:(LSUpdateInfo *)updateInfo {
    dispatch_async(dispatch_get_main_queue(), ^{
        _investment.level = [updateInfo currentValueOfFieldName:[RESIDENT.lsConnectionHandler levelField]];
        _currentLevel.text = [NSString stringWithFormat:@"%@%@", currentLevelPattern, _investment.level];
        _ribbonIcon.hidden = !_investment.hasRibbon;
    });
}

@end
