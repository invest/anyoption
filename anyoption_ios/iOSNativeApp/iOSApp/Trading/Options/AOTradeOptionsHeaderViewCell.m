//
//  AOTradeOptionsHeaderViewCell.m
//  iOSApp
//
//  Created by Tony on 6/13/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOTradeOptionsHeaderViewCell.h"

@implementation AOTradeOptionsHeaderViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
