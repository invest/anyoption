//
//  AOTradeOptionPlusQuoteViewController.h
//  iOSApp
//
//  Created by Tony on 4/18/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOLabel.h"
#import "AOInvestment.h"
#import "AOBaseViewController.h"

@interface AOTradeOptionPlusQuoteViewController : AOBaseViewController <LSTableDelegate>

@property (weak, nonatomic) IBOutlet UILabel *marketName;
@property (weak, nonatomic) IBOutlet UILabel *currentLevel;
@property (weak, nonatomic) IBOutlet UILabel *expiry;
@property (weak, nonatomic) IBOutlet UIView *border;
@property (weak, nonatomic) IBOutlet UIImageView *investmentTypeIcon;
@property (weak, nonatomic) IBOutlet AOLabel *timeCaption;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet AOLabel *levelCaption;
@property (weak, nonatomic) IBOutlet UILabel *level;
@property (weak, nonatomic) IBOutlet AOLabel *investmentAmountCaption;
@property (weak, nonatomic) IBOutlet UILabel *investmentAmount;
@property (weak, nonatomic) IBOutlet UIImageView *ribbonIcon;
@property (weak, nonatomic) IBOutlet UIView *progressBarContainer;
@property (weak, nonatomic) IBOutlet UIView *progressBar;
@property (weak, nonatomic) IBOutlet AOLabel *progressBarLine1Under;
@property (weak, nonatomic) IBOutlet AOLabel *progressBarLine1Over;
@property (weak, nonatomic) IBOutlet UILabel *progressBarLine2Under;
@property (weak, nonatomic) IBOutlet UILabel *progressBarLine2Over;
@property (weak, nonatomic) IBOutlet AOLabel *progressBarSuccess;
@property (weak, nonatomic) IBOutlet UILabel *messageLine1;
@property (weak, nonatomic) IBOutlet UILabel *messageLine2;
@property (weak, nonatomic) IBOutlet UIButton *notNowButton;
@property (weak, nonatomic) IBOutlet UIButton *sellButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

@property (nonatomic, copy) AOInvestment *investment;

- (void)applicationDidEnterBackground;
- (void)applicationWillEnterForeground;

@end
