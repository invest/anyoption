//
//  AOTradeOptionsViewController.m
//  iOSApp
//
//  Created by mac_user on 1/6/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOTradeOptionsViewController.h"
#import "AOInvestmentsMethodRequest.h"
#import "AORequestService.h"
#import "AOOpportunity.h"
#import "AOTradeOptionsViewCell.h"
#import "AOInsurance.h"
#import "AOMarket.h"
#import "AOTradeOptionPlusQuoteViewController.h"
#ifdef COPYOP
#import "COCopyUserController.h"
#endif

@interface AOTradeOptionsViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcHeadHeight;
@property (nonatomic, strong) IBOutletCollection(NSLayoutConstraint) NSArray *lcActionButtonsWidth;
@property (nonatomic, strong) IBOutletCollection(NSLayoutConstraint) NSArray *lcActionButtonsHeight;

@end

@implementation AOTradeOptionsViewController

-(void)dealloc{
    // NSLog(@"AOTradeOptionsViewController dealloc");
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    for (UILabel *leftCaption in @[investedCaption, expectedCaption])
    {
        leftCaption.textColor = [theme openOptionsLeftCaptionTextColor];
        leftCaption.font = [theme openOptionsLeftCaptionFont];
    }
    
    for (UILabel *leftValue in @[invested, expected])
    {
        leftValue.textColor = [theme openOptionsLeftCaptionTextColor];
        leftValue.font = [theme openOptionsLeftValueFont];
    }
    
    // apply theme
    head.backgroundColor = [theme openOptionsHeadBackgroundColor];
    headBorder.backgroundColor = [theme openOptionsSeparatorColor];
    tableSectionHeader.backgroundColor = [theme openOptionsTableHeaderBackgroundColor];
    
    tableFooter.backgroundColor = ATO_CELL_BG;
    table.backgroundColor = [theme settledOptionsCellBackgroundColor];
    
    // stateopenOptionsTableHeaderBackgroundColor
    dataSource = [[NSMutableArray alloc] init];
    _lsTableInfoArray = [[NSMutableArray alloc] init];
    
    invested.text = [AOUtils formatCurrencyAmount:0];
    expected.text = [AOUtils formatCurrencyAmount:0];
    
    
    UIFont *headerFont = [theme openOptionsTableHeaderFont];
    UIColor *headerColor = [theme openOptionsTableHeaderTextColor];
    for (UILabel *captionLabel in @[assetCaption, purchasedCaption, currentCaption])
    {
        captionLabel.font = headerFont;
        captionLabel.textColor = headerColor;
        
        if (IS_RIGHT_TO_LEFT)
        {
            captionLabel.textAlignment = NSTextAlignmentRight;
        }
    }
    
#ifdef COPYOP
    //[self buildLogo];  //removed because it is integrted in my options
    
    self.lcHeadHeight.constant = 128.0;
    
    for (NSLayoutConstraint *lc in self.lcActionButtonsWidth) {
        lc.constant = 108.0;
    }
    
    for (NSLayoutConstraint *lc in self.lcActionButtonsHeight) {
        lc.constant = 42;
    }
#endif
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //[self buildLogoTitleView]; 
    self.navigationItem.hidesBackButton = YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    // NSLog(@"AOTradeAssetsViewController viewDidAppear");
    
    iconUp.image = [UIImage imageNamed:@"dollar_up_disabled"];
    iconDown.image = [UIImage imageNamed:@"dollar_down_disabled"];
    lastTotalReturn = 0; // make sure the icons are updated on next LS update
    [self updateInsurances];
    
    if (IS_LOGGED) {
        [self fetchInvestments];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeLsTables];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchInvestments
{
    AOInvestmentsMethodRequest *request = [[AOInvestmentsMethodRequest alloc] init];
    request.isSettled = NO;
    [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary *resultDictionary) {
        
        if(dataSource) [dataSource removeAllObjects];
        for (id investmentDict in [resultDictionary objectForKey:@"investments"])
        {
            AOInvestment *investment = [[AOInvestment alloc] init];
            [investment setValuesForKeysWithDictionary:investmentDict];
            [dataSource addObject:investment];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [table reloadData];
        });
        
        [self addLsTables];
        
    } failed:^(NSDictionary *errDictionary) {
    }];
}

-(void)addLsTables
{
    
    [self removeLsTables];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    for (int i = 0; i < dataSource.count; i++)
    {
        AOInvestment *investment = [dataSource objectAtIndex:i];
        NSString *item = NULL;
        if (investment.opportunityTypeId == AOOpportunityTypeRegular) {
            item = [NSString stringWithFormat:@"aotps_1_%ld", investment.marketId];
        } else if (investment.opportunityTypeId == AOOpportunityTypeOptionPlus) {
            item = [NSString stringWithFormat:@"op_%ld", investment.marketId];
        }
        
        if (![items containsObject:item] && item!=nil)
        {
            [items addObject:item];
        }
    }
    
    LSExtendedTableInfo *lsTableInfo = [LSExtendedTableInfo extendedTableInfoWithItems:items mode:LSModeCommand fields:RESIDENT.lsConnectionHandler.fields dataAdapter:LS_DATA_ADAPTER snapshot:YES];
    lsTableInfo.requestedMaxFrequency= 1.0;
    
    [RESIDENT.lsConnectionHandler addTableWithTableInfo:lsTableInfo delegate:self];
    [_lsTableInfoArray addObject:lsTableInfo];
}

-(void)removeLsTables
{
    if (_lsTableInfoArray.count == 0)
    {
        return;
    }
    
    for (LSExtendedTableInfo *tableInfo in _lsTableInfoArray)
    {
        [RESIDENT.lsConnectionHandler removeTableWithTableInfo:tableInfo];
    }
    
    [_lsTableInfoArray removeAllObjects];
}

// Called by the AOResident when an update for insurance is received
- (void) updateInsurances {
    // NSLog(@"updateInsurances - Options %@",[RESIDENT.insurances description]);
    
    if ([RESIDENT.insurances count] > 0) {
        
        if (takeProfitButton.enabled || rollForwardButton.enabled) {
            return;
        }
        AOInsurance *insurance = [[RESIDENT.insurances allValues] objectAtIndex:0];
        if (insurance.insuranceType == AOInsuranceTypeTakeProfit) {
            // NSLog(@"enable TP button");
            takeProfitButton.enabled = YES;
        } else {
            // NSLog(@"enable RF button");
            rollForwardButton.enabled = YES;
        }
    } else {
        // NSLog(@"disable insurances buttons");
        if (takeProfitButton.enabled)
            takeProfitButton.enabled = NO;
        
        if (rollForwardButton.enabled)
            rollForwardButton.enabled = NO;
        
    }
}

- (IBAction)openInsurancesScreen:(id)sender {
#ifdef COPYOP
    [COCopyUserController showTradeInsurancesPopupOverParentController:self completion:nil];
#else
    [self.navigationController presentViewController:[[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:@"TradeInsurances"]
                                            animated:!IS_IPAD
                                          completion:nil];
#endif
}

- (void) setInTheMoneyForCell:(AOTradeOptionsViewCell*)cell forInvestment:(AOInvestment*)investment {
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    if (investment.hasRibbon) {
        
        if (investment.typeId == AOInvestmentTypeCall) {
            cell.iconType.image = [UIImage imageNamed:@"triangle_up_small_yellow"];
#ifdef COPYOP
            if ([investment.copyopType isEqualToString:@"COPY"]) {
                cell.iconType.image = [UIImage imageNamed:@"copied_up_winning"];
            }
#endif
        } else {
            cell.iconType.image = [UIImage imageNamed:@"triangle_down_small_yellow"];
#ifdef COPYOP
            if ([investment.copyopType isEqualToString:@"COPY"]) {
                cell.iconType.image = [UIImage imageNamed:@"copied_down_winning"];
            }
#endif
        }
        cell.current.backgroundColor = [theme settledOptionsCurrentInBackgroundColor];
        cell.iconRibbon.hidden = NO;
    } else {
        if (investment.typeId == AOInvestmentTypeCall) {
            cell.iconType.image = [UIImage imageNamed:@"triangle_up_small_blue"];
#ifdef COPYOP
            if ([investment.copyopType isEqualToString:@"COPY"]) {
                cell.iconType.image = [UIImage imageNamed:@"copied_up_losing"];
            }
#endif
        } else {
            cell.iconType.image = [UIImage imageNamed:@"triangle_down_small_blue"];
#ifdef COPYOP
            if ([investment.copyopType isEqualToString:@"COPY"]) {
                cell.iconType.image = [UIImage imageNamed:@"copied_down_losing"];
            }
#endif
        }
        cell.current.backgroundColor = [theme settledOptionsCurrentOutBackgroundColor];
        cell.iconRibbon.hidden = YES;
    }
    
    
#ifdef COPYOP
    if ([investment.copyopType isEqualToString:@"COPY"]) {
        UITapGestureRecognizer *showoffTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleCopiedOpetionTapped:)];
        [cell.middleView addGestureRecognizer:showoffTapRecognizer];
    }
    else{
        for (UIGestureRecognizer *recognizer in cell.middleView.gestureRecognizers) {
            [cell.middleView removeGestureRecognizer:recognizer];
        }
    }
#endif
}

#ifdef COPYOP
-(void)handleCopiedOpetionTapped:(UIGestureRecognizer *)tapper  {
    
    CGPoint tapLocation = [tapper locationInView:table];
    NSIndexPath *tappedIndexPath = [table indexPathForRowAtPoint:tapLocation];
    AOInvestment *selectedInvestment = [dataSource objectAtIndex:tappedIndexPath.row];
    
    
    [self.facade getOriginalInvestmentInfoFor:selectedInvestment.l_id andCompletion:^(COProfile *originalProfile, AOResponse * response) {
        
        if (response.error != nil && response.error.code != SERVICE_ERROR_USER_REMOVED)
        {
            [response.error show];
            
            return;
        }
        //error check
        
        [COCopyUserController  showOriginalInvestmentInfo:originalProfile parentController:self completion:nil ];
    }];
}
#endif

#pragma mark - Methods of UITableViewDataSource

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//	return 1;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // NSLog(@"AOTradeOptionsView numberrows %@",[dataSource description]);
    CLASS_NAME(dataSource);
    return [dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    AOTradeOptionsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentCell"];
    cell.contentView.backgroundColor = [theme settledOptionsCellBackgroundColor];
    
    AOInvestment *investment = [dataSource objectAtIndex:indexPath.row];
    
//    cell.contentView.layer.borderWidth = 2.0f;
//    cell.contentView.layer.borderColor = [UIColor redColor].CGColor;
    
    cell.invId = investment.l_id;
    
    cell.iconMarketBorder.backgroundColor = MA_ICON_BORDER_COLOR;
    cell.iconMarket.backgroundColor = MA_ICON_BG_COLOR;
    cell.iconMarket.image = [AOUtils iconForMarket:investment.marketId];
    cell.iconMarketPlus.hidden = investment.opportunityTypeId != AOOpportunityTypeOptionPlus;
    
    cell.marketName.textColor = [theme settledOptionsTextColor];
    cell.marketName.text = investment.asset;
    cell.marketName.font = [theme settledOptionsMarketNameFont];
    
    cell.timeCreated.textColor = [theme settledOptionsTextColor];
    cell.timeCreated.text = investment.timePurchaseTxt;
    cell.timeCreated.font = [theme settledOptionsDefaultFont];
    
    cell.border1.backgroundColor = [theme settledOptionsBorderColor];
    
    cell.level.textColor = [theme settledOptionsTextColor];
    cell.level.text = investment.currentLevelTxt;
    cell.level.font = [theme settledOptionsDefaultFont];
    
    cell.amount.textColor = [theme settledOptionsTextColor];
    cell.amount.text = investment.amountTxt;
    cell.amount.font = [theme settledOptionsDefaultFont];

    cell.border2.backgroundColor = [theme settledOptionsBorderColor];
    
    cell.current.backgroundColor = [theme settledOptionsCurrentOutBackgroundColor];
    
    cell.currentLevel.textColor = ATO_CELL_TEXT_WHITE;
    cell.currentLevel.text = investment.level;
    
    cell.currentReturn.textColor = ATO_CELL_TEXT_WHITE;
    cell.currentReturn.text = investment.amountReturnWF;
    
    cell.borderBottom.backgroundColor = [theme settledOptionsBorderColor];

    [self setInTheMoneyForCell:cell forInvestment:investment];
    
    return cell;
}

#pragma mark - Methods of UITableViewDelegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [table deselectRowAtIndexPath:indexPath animated:NO];
    
    AOInvestment *investment = [dataSource objectAtIndex:indexPath.row];
    AOMarket *market = [[AOMarket alloc] init];
    market.l_id = investment.marketId;
    market.displayName = investment.asset;
    market.isOptionPlusMarket = investment.opportunityTypeId == AOOpportunityTypeOptionPlus;
#ifdef COPYOP
    if(market.isOptionPlusMarket){
        return;
    }
#endif
    [MAINVC loadPage:@"Trade" pageName:IS_IPAD?ASSET_DETAILS_IPAD_PAGE_ID:ASSET_DETAILS_PAGE_ID root:NO options:@{@"market":market}];
}

#pragma mark - Methods of LSTableDelegate

- (void) table:(LSSubscribedTableKey *)tableKey itemPosition:(int)itemPosition itemName:(NSString *)itemName didUpdateWithInfo:(LSUpdateInfo *)updateInfo
{
     dispatch_async(dispatch_get_main_queue(), ^{
        long mId = [[updateInfo currentValueOfFieldName:@"ET_NAME"] longLongValue];
        long oppId = [[updateInfo currentValueOfFieldName:@"ET_OPP_ID"] longLongValue];
        long stateId = [[updateInfo currentValueOfFieldName:@"ET_STATE"] longLongValue];
        
        float totalInvested = 0;
        float totalReturn = 0;
        for (int i = 0; i < dataSource.count;) {
            AOInvestment *investment = [dataSource objectAtIndex:i];
            BOOL updateLongtermInv = investment.additionalInfoType == AOInvestmentAdditionalInfoTypeNotHourly;
            if ([investment.timeEstClosing compare:[[NSDate date] dateByAddingTimeInterval:3600]] != NSOrderedDescending) {
                updateLongtermInv = NO;
            }
            if (mId == investment.marketId && (oppId == investment.opportunityId || updateLongtermInv)) {
                if (stateId == AOOpportunityStateDone) {
                    [dataSource removeObjectAtIndex:i];
                    [table reloadData];
                    [[MAINVC statusBar] setOpenOptions:dataSource.count];
                    continue;
                } else {
                    int skinGroupId = [RESIDENT skinGroupId];
                    NSString *levelField = [NSString stringWithFormat:@"LEVEL_%d", skinGroupId];
                    NSString *newLevel = [updateInfo currentValueOfFieldName:levelField];
                    if ([newLevel compare:@"0"] == NSOrderedSame){
                        return; // ignore updates with level 0
                    }
                    
                    investment.level = newLevel;
                    NSIndexPath *ip = [NSIndexPath indexPathForRow:i inSection:0];
                    AOTradeOptionsViewCell *cell = (AOTradeOptionsViewCell*)[table cellForRowAtIndexPath:ip];
                    cell.currentLevel.text = investment.level;
                    cell.currentReturn.text = investment.amountReturnWF;
                    [self setInTheMoneyForCell:cell forInvestment:investment];
                }
            }
            totalInvested += investment.amount;
            if (investment.hasRibbon) {
                totalReturn += investment.winReturn;
            } else {
                totalReturn += investment.loseReturn;
            }
            i++;
        }
        
        if (lastTotalReturn == 0 || lastTotalReturn != totalReturn) {
            invested.text = [AOUtils formatCurrencyAmount:totalInvested / 100];
            expected.text = [AOUtils formatCurrencyAmount:totalReturn / 100];
            if (totalInvested > 0) {
                iconUp.image = [UIImage imageNamed:@"dollar_up"];
            } else {
                iconUp.image = [UIImage imageNamed:@"dollar_up_disabled"];
            }
            if (totalReturn > totalInvested) {
                iconDown.image = [UIImage imageNamed:@"dollar_down"];
                iconHeadRibbon.image = [UIImage imageNamed:@"misc_gold-badge"];
            } else {
                iconDown.image = [UIImage imageNamed:@"dollar_down_disabled"];
                iconHeadRibbon.image = nil;
            }
        }
        lastTotalReturn = totalReturn;
     });
}

@end