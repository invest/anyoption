//
//  AOTradeOptionsViewController.h
//  iOSApp
//
//  Created by mac_user on 1/6/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOBaseViewController.h"
#import "AOLabel.h"
#import "AOTakeProfitButton.h"
#import "AORollForwardButton.h"

@interface AOTradeOptionsViewController : AOBaseViewController <UITableViewDataSource, UITableViewDelegate, LSTableDelegate>
{
    NSMutableArray *dataSource;
    
    __weak IBOutlet UIView *head;
    __weak IBOutlet UIImageView *iconUp;
    __weak IBOutlet UIImageView *iconDown;
    __weak IBOutlet UIImageView *iconHeadRibbon;
    __weak IBOutlet AOLabel *investedCaption;
    __weak IBOutlet UILabel *invested;
    __weak IBOutlet AOLabel *expectedCaption;
    __weak IBOutlet UILabel *expected;
    __weak IBOutlet UIView *headBorder;
    __weak IBOutlet AOTakeProfitButton *takeProfitButton;
    __weak IBOutlet AORollForwardButton *rollForwardButton;
    __weak IBOutlet UIView *tableSectionHeader;
    __weak IBOutlet AOLabel *assetCaption;
    __weak IBOutlet AOLabel *purchasedCaption;
    __weak IBOutlet AOLabel *currentCaption;
    __weak IBOutlet UITableView *table;
    __weak IBOutlet UIView *tableFooter;
    
    float lastTotalReturn;
}

@property (strong,nonatomic) NSMutableArray *lsTableInfoArray;

- (void) updateInsurances;

@end
