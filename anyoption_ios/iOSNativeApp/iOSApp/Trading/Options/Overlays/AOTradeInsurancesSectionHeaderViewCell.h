//
//  AOTradeInsurancesSectionHeaderView.h
//  iOSApp
//
//  Created by Tony on 4/9/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOLabel.h"

@interface AOTradeInsurancesSectionHeaderViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *marketName;
@property (weak, nonatomic) IBOutlet UILabel *currentLevel;
@property (weak, nonatomic) IBOutlet AOLabel *amountCaption;
@property (weak, nonatomic) IBOutlet AOLabel *insuranceCaption;
@property (weak, nonatomic) IBOutlet UIView *borderBottom;

@end
