//
//  AOTradeInsurancesViewCell.m
//  iOSApp
//
//  Created by Tony on 4/9/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOTradeInsurancesViewCell.h"

@implementation AOTradeInsurancesViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
