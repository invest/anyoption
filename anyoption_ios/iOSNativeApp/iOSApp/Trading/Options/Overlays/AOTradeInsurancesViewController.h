//
//  AOTradeInsurancesViewController.h
//  iOSApp
//
//  Created by Tony on 4/5/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOOverlayViewController.h"
#import "AOLabel.h"
#import "AOBlueButton.h"
#import "AOGreenButton.h"

@interface AOTradeInsurancesViewController : AOOverlayViewController <UITableViewDataSource, UITableViewDelegate>
{
    __weak IBOutlet UIView *header;
    __weak IBOutlet UIImageView *insuranceTypeIcon;
    __weak IBOutlet UILabel *insuranceType;
    __weak IBOutlet UILabel *insuranceDescription;
    __weak IBOutlet UITableView *table;
    __weak IBOutlet UIView *footer;
    __weak IBOutlet AOLabel *totalCaption;
    __weak IBOutlet UILabel *total;
    __weak IBOutlet AOLabel *allCaption;
    __weak IBOutlet UIButton *allButton;
    __weak IBOutlet AOLabel *timeLeftCaption;
    __weak IBOutlet UILabel *timeLeft;
    __weak IBOutlet AOBlueButton *cancelButton;
    __weak IBOutlet AOGreenButton *buyButton;
}

- (void) updateInsurances;

@end
