//
//  AOTradeInsurancesViewController.m
//  iOSApp
//
//  Created by Tony on 4/5/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOTradeInsurancesViewController.h"
#import "AOInvestmentsMethodRequest.h"
#import "AORequestService.h"
#import "AOInsurance.h"
#import "AOTradeInsurancesViewCell.h"
#import "AOTradeInsurancesSectionHeaderViewCell.h"
#import "AOInsurancesMethodRequest.h"

@interface AOTradeInsurancesViewController () {
    NSMutableArray *dataSource;
    NSTimer *timer;
    NSDate *insurancePeriodEndTime;
}

@end

@interface AOTradeInsurancesViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcBottom;

@end

@implementation AOTradeInsurancesViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    
    if (IS_IPAD) {
        [self setBgImg:[UIImage imageNamed:@"overlayBG-iPad"]];
    }
    else {
        NSString* overlayImageName = [AOUtils isRetina4] ? @"overlayBG-568" : @"overlayBG";
#ifdef COPYOP
        overlayImageName = [@"co" stringByAppendingString:overlayImageName];
#endif
        [self setBgImg:[UIImage imageNamed:overlayImageName]];
    }
    
    [self p_setupConstraints];
    
    dataSource = [[NSMutableArray alloc] init];
    AOInsurance *ins = nil;
    for (ins in [RESIDENT.insurances allValues]) {
        NSMutableArray *crrMarketSection = nil;
        for (int i = 0; i < [dataSource count]; i++) {
            NSMutableArray *sectionArray = [dataSource objectAtIndex:i];
            AOInsurance *sectionIns = (AOInsurance*)[sectionArray objectAtIndex:0];
            if ([sectionArray count] > 0 && (ins.marketId == sectionIns.marketId)) {
                crrMarketSection = sectionArray;
                break;
            }
        }
        if (nil == crrMarketSection) {
            crrMarketSection = [[NSMutableArray alloc] init];
            [dataSource addObject:crrMarketSection];
        }
        [crrMarketSection addObject:ins];
    }
    if (dataSource.count && [[dataSource objectAtIndex:0] count]) {
        ins = [[dataSource objectAtIndex:0] objectAtIndex:0];
        header.backgroundColor = ATO_CELL_BG_IN;
        insuranceType.textColor = ATO_INS_TEXT_COLOR;
        insuranceDescription.textColor = ATO_INS_TEXT_COLOR;
        if (ins.insuranceType == AOInsuranceTypeTakeProfit) {
            insuranceTypeIcon.image = [UIImage imageNamed:@"ins_tp"];
            insuranceType.text = AOLocalizedString(@"golden.my.inv.no.level.GM");
            insuranceDescription.text = AOLocalizedString(@"insurances.take.profit.description");
            NSDate *expTime = [self calculateExpiryTime:ins.closeTime];
            insuranceDescription.text = [NSString stringWithFormat:insuranceDescription.text, [AOUtils formatTimeShort:expTime]];
        } else {
            insuranceTypeIcon.image = [UIImage imageNamed:@"ins_rf"];
            insuranceType.text = AOLocalizedString(@"golden.my.inv.no.level.RU");
            insuranceDescription.text = AOLocalizedString(@"insurances.roll.forward.description");
            NSDate *expTime = [self calculateExpiryTime:ins.closeTime];
            insuranceDescription.text = [NSString stringWithFormat:insuranceDescription.text, [AOUtils formatTimeShort:expTime], [AOUtils formatTimeShort:[expTime dateByAddingTimeInterval:3600]]];
        }
        insurancePeriodEndTime = ins.closeTime;
    }
    table.backgroundColor = ATO_CELL_BG_IN;
    totalCaption.textColor = ATO_INS_TEXT_COLOR;
    total.textColor = ATO_INS_TEXT_COLOR;
    allCaption.textColor = ATO_INS_TEXT_COLOR;
    timeLeftCaption.textColor = ATO_INS_TIME_LEFT_CAPTION_COLOR;
    timeLeftCaption.text = [NSString stringWithFormat:@"%@:", timeLeftCaption.text];
    timeLeft.textColor = ATO_INS_TIME_LEFT_TIME_COLOR;
    
    [self updateTotalLine];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTimeLeft) userInfo:nil repeats:YES];
    
    if (IS_RIGHT_TO_LEFT)
    {
        allCaption.textAlignment = NSTextAlignmentLeft;
    }
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    timeLeft.text = @"";
}

- (void) p_setupConstraints
{
    CGFloat distBetweenButtonsAndCenterX = 2.0;

    UIButton *leftButton = IS_RIGHT_TO_LEFT ? buyButton : cancelButton;
    UIButton *rightButton = IS_RIGHT_TO_LEFT ? cancelButton : buyButton;
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:leftButton
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:-distBetweenButtonsAndCenterX]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:rightButton
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:distBetweenButtonsAndCenterX]];
    
    if (IS_IPAD)
    {
        CGFloat screenHeight = [AOUtils devicePortraitBounds].size.width;
        CGFloat iPhone5Size = 568.0;
        CGFloat dist = floorf((screenHeight - iPhone5Size) / 2.0);
        self.lcTop.constant = dist;
        self.lcBottom.constant = dist;
    }
}

- (NSDate*) calculateExpiryTime:(NSDate*)closeTime {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSMinuteCalendarUnit) fromDate:closeTime];
    long min = [components minute];
    NSDateComponents *dc = [[NSDateComponents alloc] init];
    if (min < 30) {
        dc.minute = 30 - min;
    } else {
        dc.minute = 60 - min;
    }
    return [calendar dateByAddingComponents:dc toDate:closeTime options:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Called by the AOResident when an update for insurance is received
- (void) updateInsurances {
    // NSLog(@"updateInsurances");
    [table reloadData];
}

- (void) updateTimeLeft {
    NSTimeInterval timeLeftInterval = [insurancePeriodEndTime timeIntervalSinceDate:[AOUtils currentDatePrecise]];
    if (timeLeftInterval < 0) {
//        timeLeft.text = @"00:00";
        [self stopTimer];
    } else {
        long timeLeftLong = (long) timeLeftInterval;
        timeLeft.text = [NSString stringWithFormat:@"%1$02ld:%2$02ld", timeLeftLong / 60, timeLeftLong % 60];
    }
}

- (void) stopTimer {
    // NSLog(@"AOTradeInsurancesViewController stopTimer");
    [timer invalidate];
    timer = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onAll:(id)sender {
    AOInsurance *insurance = nil;
    for (int i = 0; i < dataSource.count; i++) {
        for (int j = 0; j < [[dataSource objectAtIndex:i] count]; j++) {
            insurance = [[dataSource objectAtIndex:i] objectAtIndex:j];
            insurance.selected = !allButton.selected;
        }
    }
    allButton.selected = !allButton.selected;
    [table reloadData];
    [self updateTotalLine];
}

- (IBAction) onBuy:(AOGreenButton *)sender {
    // NSLog(@"onBuy");
    AOInsurancesMethodRequest *request = [[AOInsurancesMethodRequest alloc] init];
    
    NSMutableArray *invIds = [[NSMutableArray alloc] init];
    NSMutableArray *insAmounts = [[NSMutableArray alloc] init];
    
    AOInsurance *insurance = nil;
    for (int i = 0; i < dataSource.count; i++) {
        for (int j = 0; j < [[dataSource objectAtIndex:i] count]; j++) {
            insurance = [[dataSource objectAtIndex:i] objectAtIndex:j];
            request.insuranceType = insurance.insuranceType;
            if (insurance.selected) {
                [invIds addObject:[NSNumber numberWithLong:insurance.l_id]];
                [insAmounts addObject:[NSNumber numberWithDouble:[insurance getInsuranceAsDouble]]];
            }
        }
    }
    request.investmentIds = invIds;
    request.insuranceAmounts = insAmounts;
    
    buyButton.showsLoadingAnimation = YES;
    buyButton.enabled = NO;
    
    [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary *resultDictionary) {
        NSArray *results = [resultDictionary objectForKey:@"results"];
        NSArray *investmentIds = [resultDictionary objectForKey:@"investmentIds"];
        long insType = 0;
        BOOL hasSuccess = NO;
        NSString *result = nil;
        for (int k = 0; k < results.count; k++) {
            result = [results objectAtIndex:k];
            long investmentId = [[investmentIds objectAtIndex:k] longLongValue];
            if ([result hasPrefix:@"OK"]) {
                hasSuccess = YES;

                [RESIDENT.lsConnectionHandler sendMessage:[NSString stringWithFormat:@"invId_%ld", investmentId]];

                AOInsurance *insurance = nil;
                for (int i = 0; i < dataSource.count; i++) {
                    for (int j = 0; j < [[dataSource objectAtIndex:i] count]; j++) {
                        insurance = [[dataSource objectAtIndex:i] objectAtIndex:j];
                        if (insurance.l_id == investmentId) {
                            NSMutableArray *sectionArr = [dataSource objectAtIndex:i];
                            [sectionArr removeObject:insurance];
                            if (sectionArr.count == 0) {
                                [dataSource removeObject:sectionArr];
                            }
                            break;
                        }
                    }
                }
                insType = insurance.insuranceType;
            } else {
                AONOTIFICATION(AONotificationError, result);
            }
        }
        if (hasSuccess) {
            if (insType == AOInsuranceTypeTakeProfit) {
                result = AOLocalizedString(@"insurances.take.profit.success");
                result = [NSString stringWithFormat:result, [resultDictionary objectForKey:@"credit"]];
            } else {
                result = AOLocalizedString(@"golden.recipt.txt1_RU");
            }
            AONOTIFICATION(AONotificationDefault, result);
            if (dataSource.count > 0) {
                [table reloadData];
            } else {
                [self stopTimer];
            }
            
            [TrackingManager trackEvent:TrackingEventTypeTradeInsurancesAction options:nil];
        } else {
            AONOTIFICATION(AONotificationError, result);
        }
        [self updateTotalLine];
    } failed:^(NSDictionary *errDictionary) {
        SHOW_USER_NOTIFICATIONS(errDictionary);
        [self updateTotalLine];
    }];
}

- (IBAction) onCancel:(AOBlueButton *)sender {
    // NSLog(@"onCancel");
    [self stopTimer];
}

- (void) updateTotalLine {
    double totalInsAmount = 0;
    BOOL allSelected = true;
    
    AOInsurance *insurance = nil;
    for (int i = 0; i < dataSource.count; i++) {
        for (int j = 0; j < [[dataSource objectAtIndex:i] count]; j++) {
            insurance = [[dataSource objectAtIndex:i] objectAtIndex:j];
            if (insurance.selected) {
                totalInsAmount += [insurance getInsuranceAsDouble];
            } else {
                allSelected = false;
            }
        }
    }
    
    total.text = [AOUtils formatCurrencyAmount:totalInsAmount];
    allButton.selected = allSelected;
    
    buyButton.showsLoadingAnimation = totalInsAmount > 0;
    buyButton.enabled = totalInsAmount > 0;
}

#pragma mark - Methods of UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[dataSource objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AOTradeInsurancesViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InsuranceCell"];
    cell.contentView.backgroundColor = ATO_CELL_BG_IN;
    
    AOInsurance *insurance = [[dataSource objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    cell.time.textColor = ATO_CELL_TEXT_WHITE;
    cell.time.text = insurance.level;

    cell.border1.backgroundColor = ATO_INS_CELL_BORDER_COLOR;

    if (insurance.investmentType == AOInvestmentTypeCall) {
        cell.typeIcon.image = [UIImage imageNamed:@"triangle_up_small_blue"];
    } else {
        cell.typeIcon.image = [UIImage imageNamed:@"triangle_down_small_blue"];
    }
    
    cell.level.textColor = ATO_CELL_TEXT_WHITE;
    cell.level.text = insurance.level;

    cell.border2.backgroundColor = ATO_INS_CELL_BORDER_COLOR;

    cell.amount.textColor = ATO_CELL_TEXT_WHITE;
    cell.amount.text = insurance.amount;

    cell.border3.backgroundColor = ATO_INS_CELL_BORDER_COLOR;

    cell.insurance.textColor = ATO_CELL_TEXT_WHITE;
    cell.insurance.text = insurance.insurance;
    
    if (insurance.selected) {
        cell.selectedIcon.image = [UIImage imageNamed:@"ins_cb_checked"];
    } else {
        cell.selectedIcon.image = [UIImage imageNamed:@"ins_cb_unchecked"];
    }
    
    cell.borderBottom.backgroundColor = ATO_INS_CELL_BORDER_COLOR;
    
    if (IS_RIGHT_TO_LEFT)
    {
        cell.time.textAlignment = cell.level.textAlignment = cell.amount.textAlignment = cell.insurance.textAlignment = NSTextAlignmentRight;
    }
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    AOTradeInsurancesSectionHeaderViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"InsuranceSectionHeader"];
    
    AOInsurance *ins = (AOInsurance*)[[dataSource objectAtIndex:section] objectAtIndex:0];
    
    headerCell.backgroundColor = ATO_CELL_BG_OUT;

    headerCell.marketName.textColor = ATO_CELL_TEXT_WHITE;
    headerCell.marketName.text = ins.marketName;
    
    headerCell.currentLevel.textColor = ATO_CELL_TEXT_WHITE;
    headerCell.currentLevel.text = ins.currentLevel;
    
    headerCell.amountCaption.textColor = ATO_CELL_TEXT_WHITE;
    headerCell.insuranceCaption.textColor = ATO_CELL_TEXT_WHITE;
    
    headerCell.borderBottom.backgroundColor = ATO_INS_CELL_BORDER_COLOR;
    
    return headerCell;
}

#pragma mark - Methods of UITableViewDelegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [table deselectRowAtIndexPath:indexPath animated:NO];
    
    AOInsurance *insurance = [[dataSource objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    insurance.selected = !insurance.selected;
    [table reloadData];
    [self updateTotalLine];
}

@end