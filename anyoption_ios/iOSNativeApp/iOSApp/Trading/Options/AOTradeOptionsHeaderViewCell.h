//
//  AOTradeOptionsHeaderViewCell.h
//  iOSApp
//
//  Created by Tony on 6/13/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOLabel.h"

@interface AOTradeOptionsHeaderViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AOLabel *asset;
@property (weak, nonatomic) IBOutlet AOLabel *purchased;
@property (weak, nonatomic) IBOutlet AOLabel *closing;

@end
