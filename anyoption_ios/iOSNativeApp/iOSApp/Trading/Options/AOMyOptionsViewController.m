//
//  AOMyOptionsViewController.m
//  iOSApp
//
//  Created by  on 1/8/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AOMyOptionsViewController.h"
#import "AOTradeSettledOptionsViewController.h"
#import "AOTradeOptionsViewController.h"

#import "AOThemeController.h"

//typedef NS_ENUM(NSInteger, AOSelectedOptionsType)
//{
//    AOSelectedOptionsOpened,
//    AOSelectedOptionsSettled
//
//
//};



@implementation AOMyOptionsViewController{
    AOTradeSettledOptionsViewController* _settledViewController;
    AOTradeOptionsViewController* _openViewController;
    __weak IBOutlet UIView *_containerView;
    
    id _currentlyLoadedVC;
    
    __weak IBOutlet UIButton *_openSegButton;
    __weak IBOutlet UIButton *_settledSegButton;
    __weak IBOutlet UIButton *_settledFilterButton;
    
    UIButton* _selSegButton;
}


-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self p_customizeSegButton:_openSegButton];
    [self p_customizeSegButton:_settledSegButton];
    
    if (![self.navOptions[@"settled"] boolValue])
    {
        [_openSegButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [_settledSegButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    
    self.view.clipsToBounds = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHidePopupMenu:) name:UIMenuControllerDidHideMenuNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didShowPopupMenu:) name:UIMenuControllerDidShowMenuNotification object:nil];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    // create title string
    NSMutableString *settledTitle = [NSMutableString stringWithString:AOLocalizedString(@"settledUpper")];
    
#if ETRADER
    _settledSegButton.contentEdgeInsets = UIEdgeInsetsMake(0, 36, 0, 8);
#else
    _settledSegButton.contentEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 36);
#endif
    
    _settledSegButton.titleLabel.numberOfLines = 1;
    _settledSegButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    _settledSegButton.titleLabel.lineBreakMode = NSLineBreakByClipping;
    
    // check if user changed filter, otherwise (and set) use default value
    if(![userDefaults integerForKey:USER_DEFS_SETTLED_SELECTED_FILTER]) {
        [settledTitle appendString:[NSString stringWithFormat:@" (%@)", AOLocalizedString(@"settledFilter24H")]];
        
        [userDefaults setInteger:0 forKey:USER_DEFS_SETTLED_SELECTED_FILTER];
        [userDefaults synchronize];
    } else {
        switch ([userDefaults integerForKey:USER_DEFS_SETTLED_SELECTED_FILTER]) {
            case 0:
                [settledTitle appendString:[NSString stringWithFormat:@" (%@)", AOLocalizedString(@"settledFilter24H")]];
                break;
            case 1:
                [settledTitle appendString:[NSString stringWithFormat:@" (%@)", AOLocalizedString(@"settledFilter1W")]];
                break;
            case 2:
                [settledTitle appendString:[NSString stringWithFormat:@" (%@)", AOLocalizedString(@"settledFilter1M")]];
                break;
                
            default:
                break;
        }
    }
        
    [_settledSegButton setTitle:[settledTitle uppercaseString] forState:UIControlStateNormal];
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:UIMenuControllerDidShowMenuNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:UIMenuControllerDidHideMenuNotification];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self buildTitleViewWithText:AOLocalizedString(@"myOptions") andImage:nil];
    
    [_settledSegButton setTitle:[AOLocalizedString(@"settledUpper") uppercaseString] forState:UIControlStateNormal];
    [_openSegButton setTitle:[AOLocalizedString(@"openUpper") uppercaseString] forState:UIControlStateNormal];
    
    NSLog(@"awedeebawewewe %@", NSStringFromCGRect(self.view.frame));
    
}


- (IBAction)setButtonPressed:(id)sender {
    
    
    
    UIButton* pressedButton =  (UIButton*)sender;
    
    
    
    if(_selSegButton != pressedButton ){
    
        _selSegButton.selected = NO;
        pressedButton.selected = YES;
        
        _selSegButton = pressedButton;
        
        
        
        if(_selSegButton == _openSegButton){
            if (_openViewController==nil) {
                _openViewController = (AOTradeOptionsViewController*)[RESIDENT getScreen:@"openOptions"];
                _openViewController.navOptions = self.navOptions;
                
                NSLog(@"abedeeba1 %@", [NSNumber numberWithBool:_openViewController.view.clipsToBounds]);
                
            }
            
            
            [self p_setCurrentViewController:_openViewController];
        }
        else {
            if (_settledViewController==nil) {
                _settledViewController = (AOTradeSettledOptionsViewController*)[RESIDENT getScreen:@"settledOptions"];
                _settledViewController.navOptions = self.navOptions;
                
                NSLog(@"abedeeba2 %@", [NSNumber numberWithBool:_settledViewController.view.clipsToBounds]);

            }
            
            
            [self p_setCurrentViewController:_settledViewController];

        }
    }
    
}

/*****************************
 * SETTLED Popup menu
 ****************************/
-(IBAction)changeSettledFilter:(UIButton*)sender {
    [self becomeFirstResponder];
    
    UIMenuItem* mItem24H = [[UIMenuItem alloc] initWithTitle:AOLocalizedString(@"settledFilterMenu24H") action:@selector(filter24H)];
    UIMenuItem* mItem1W = [[UIMenuItem alloc] initWithTitle:AOLocalizedString(@"settledFilterMenu1W") action:@selector(filter1W)];
    UIMenuItem* mItem1M = [[UIMenuItem alloc] initWithTitle:AOLocalizedString(@"settledFilterMenu1M") action:@selector(filter1M)];

    NSArray *filterItems = @[mItem24H, mItem1W, mItem1M];
    
    UIMenuController* sharedController = [UIMenuController sharedMenuController];
    
    CGRect drawRect = [sender convertRect:sender.bounds toView:self.view];
    [sharedController setTargetRect:drawRect inView:self.view];
    
    [sharedController setMenuItems:filterItems];
    
    [sharedController setMenuVisible:YES animated:YES];

}

-(void)filter24H {
    // NSLog(@"24 HOURS");
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:0 forKey:USER_DEFS_SETTLED_SELECTED_FILTER];
    [userDefaults synchronize];
    
    [self reloadTableAfterFilter];
}

-(void)filter1W {
    // NSLog(@"1 WEEK");
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:1 forKey:USER_DEFS_SETTLED_SELECTED_FILTER];
    [userDefaults synchronize];
    
    [self reloadTableAfterFilter];
}

-(void)filter1M {
    // NSLog(@"1 MONTH");
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:2 forKey:USER_DEFS_SETTLED_SELECTED_FILTER];
    [userDefaults synchronize];
    
    [self reloadTableAfterFilter];
}

-(void)reloadTableAfterFilter {
    [_settledViewController performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
}

-(void)didShowPopupMenu:(NSNotification *)notification {
    [_settledFilterButton setImage:[UIImage imageNamed:@"settled_up"] forState:UIControlStateNormal];
}

-(void)didHidePopupMenu:(NSNotification *)notification {
    [_settledFilterButton setImage:[UIImage imageNamed:@"settled_down"] forState:UIControlStateNormal];
}


-(void)p_setCurrentViewController:(id)currentViewController {
    
    if(currentViewController==nil || ![currentViewController isKindOfClass:[UIViewController class]]) return;
    
    
    //cleanup container view
//    if(_containerView.subviews.count>0)
//        [[_containerView.subviews objectAtIndex:0] removeFromSuperview];
    
    
    UIViewController *vc = [self.childViewControllers lastObject];
    [vc willMoveToParentViewController:nil];
    [vc.view removeFromSuperview];
    [vc removeFromParentViewController];
    
    
    
    [self addChildViewController:currentViewController];
    CGRect newFrame = [currentViewController view].frame;
    newFrame.size.height = self.view.frame.size.height - _openSegButton.frame.size.height;
    newFrame.size.width = 320;
    [currentViewController view].frame = newFrame;
    
    NSLog(@"bebebebe %@", NSStringFromCGRect([currentViewController view].frame));
    
    [_containerView addSubview:[currentViewController view]];
    [currentViewController didMoveToParentViewController:self];
    
    
    
    _currentlyLoadedVC = currentViewController;
}



- (UIImage *) p_segBtnNormalImage
{
    CGSize size = CGSizeMake(3.0, 3.0);
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [[UIColor whiteColor] setFill];
    UIRectFill(CGRectMake(0.0, 0.0, size.width, size.height));
    UIImage *readyImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [readyImage resizableImageWithCapInsets:UIEdgeInsetsMake(1.0, 1.0, 1.0, 1.0) resizingMode:UIImageResizingModeStretch];
}

- (UIImage *) p_segBtnSelImage
{
    CGFloat bottomStripHeight = 5.0;
    CGSize size = CGSizeMake(3.0, 2.0 + bottomStripHeight);
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    
    [[UIColor whiteColor] setFill];
    UIRectFill(CGRectMake(0.0, 0.0, size.width, size.height));
    
    UIColor *bottomStripColor = [UIColor colorWithHexString:@"#364b5e"];
    [bottomStripColor setFill];
    UIRectFill(CGRectMake(0.0, size.height - bottomStripHeight, size.width, bottomStripHeight));
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return [image resizableImageWithCapInsets:UIEdgeInsetsMake(1.0, 1.0, bottomStripHeight, 1.0) resizingMode:UIImageResizingModeStretch];
}

- (void) p_customizeSegButton:(UIButton *)button
{
    
    
    
    [button setTitleColor:[UIColor colorWithRed:53.0/255.0 green:53.0/255.0 blue:53.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1.0] forState:UIControlStateHighlighted];
    button.titleLabel.font = [[AOThemeController sharedInstance].theme myOptionsTabFont];//[UIFont robotoFontRegularWithSize:14.0];
    [button setBackgroundImage:[self p_segBtnNormalImage] forState:UIControlStateNormal];
    [button setBackgroundImage:[self p_segBtnSelImage] forState:UIControlStateSelected];
    [button setBackgroundImage:[self p_segBtnNormalImage] forState:UIControlStateHighlighted];
    [button setBackgroundImage:[self p_segBtnSelImage] forState:UIControlStateHighlighted | UIControlStateSelected];
}


- (void) updateInsurances{

        if ([_openViewController respondsToSelector:NSSelectorFromString(@"updateInsurances")]) {
            [_openViewController performSelector:NSSelectorFromString(@"updateInsurances")];
        }

}

-(BOOL)canBecomeFirstResponder{
    return YES;
}

@end
