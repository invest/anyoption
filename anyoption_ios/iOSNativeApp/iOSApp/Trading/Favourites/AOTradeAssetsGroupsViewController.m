//
//  AOTradeAssetsGroupsViewController.m
//  iOSApp
//
//  Created by Tony on 4/12/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOTradeAssetsGroupsViewController.h"
#import "AOMethodRequest.h"
#import "AORequestService.h"
#import "AOMarket.h"
#import "AOAssetDetailsViewController.h"
#import "AOTradeAssetsAllViewCell.h"
#import "AOTradeAssetsHeaderViewCell.h"
#import "AOTradeAssetsViewCell.h"
#import "AOInvestmentsMethodRequest.h"
#import "AOFAQViewController.h"
#import "AOTradeAssetsGroupsTableHeaderView.h"
#ifdef COPYOP
#import "COBorderedButton.h"
#endif

@interface AOTradeAssetsGroupsViewController () <AOTradeAssetsGroupsTableHeaderViewDelegate> {
    NSMutableArray *dataSource;
    NSDateFormatter *dateFormatter;
    NSTimer *assetTimer;
    
    BOOL isOnOptionPlus;
    
    NSMutableArray *lsTableInfoArray;
    
    int toDeleteRow;
    int toDeleteSection;
    
    BOOL investmentsLoaded;
    
    NSMutableSet *_binaryExpandedSections;
    NSMutableSet *_optionPlusExpandedSections;
    
    id _shoppingBagObserver;
}

@end

@implementation AOTradeAssetsGroupsViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:_shoppingBagObserver];
}

-(id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _allAssets = true;
        toDeleteRow = -1;
        
    }
    return self;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    lsTableInfoArray = [[NSMutableArray alloc] init];
    
    _binaryExpandedSections = [NSMutableSet set];
    _optionPlusExpandedSections = [NSMutableSet set];
    
    dataSource = [[NSMutableArray alloc] init];
    
    buttonContainer.backgroundColor = M_BACKGROUND;
    
    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(0, 0, buttonContainer.frame.size.width, 0.5);
    topBorder.backgroundColor = M_NAVBAR_SEPARATOR.CGColor;
    [buttonContainer.layer addSublayer:topBorder];
    
    CALayer *separator = [CALayer layer];
    separator.frame = CGRectMake(buttonContainer.frame.size.width/2, 0, 0.5, buttonContainer.frame.size.height);
    separator.backgroundColor = M_NAVBAR_SEPARATOR.CGColor;
    [buttonContainer.layer addSublayer:separator];
    
    [self setupBinaryButton];
    [self setupOptionPlus];
    
    isOnOptionPlus = false;
    if (self.navOptions) {
        isOnOptionPlus = [[self.navOptions objectForKey:@"isOnOptionPlus"] boolValue];
    }
    
    [table registerNib:[UINib nibWithNibName:@"AOTradeAssetsHeaderView" bundle:[NSBundle mainBundle]] forHeaderFooterViewReuseIdentifier:@"Header"];
    
    self.navigationItem.hidesBackButton = YES;
    
    __typeof__(self) __weak myself = self;
    _shoppingBagObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kShoppingBagUpdatedNotification
                                                                object:nil
                                                                queue:nil
                                                                usingBlock:^(NSNotification *note) {
                                                                    if (myself)
                                                                    {
                                                                        __typeof__(myself) __strong sself = myself;
                                                                        [sself fetchInvestments];
                                                                    }
                                                                }];
    
#ifdef COPYOP
    lcButtonContainerHeight.constant = 0;
    buttonContainer.clipsToBounds = YES;
#endif
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_allAssets) {
        [self buildTitleViewWithText:AOLocalizedString(@"investment.header.market.slosestToExp") andImage:nil];
    }
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // NSLog(@"AOTradeAssetsGroupsViewController viewDidAppear");
    
    self.view.userInteractionEnabled=YES;
    
    if (!isOnOptionPlus) {
        [self pressedBinaryTab:nil];
    } else {
        [self pressedOptionPlusTab:nil];
    }
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    table.backgroundColor = [theme appBackgroundColor];
    
    [self updateAssetTimer];
    
    if (IS_LOGGED && !investmentsLoaded) {
        [self fetchInvestments];
    }
    
    [RESIDENT.lsConnectionHandler stopLightstreamer];
    [RESIDENT.lsConnectionHandler startLightstreamer];
    [RESIDENT addLightstreamerSharedTables];
#ifndef COPYOP
    [self buildHelpButtonWithTitle:@"assetPageHelpTitle" andContent:@"assetPageHelpContent"];
#endif
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // NSLog(@"AOTradeAssetsGroupsViewController.viewWillDisappear");
    [self removeLsTables];
    [assetTimer invalidate];
    assetTimer = nil;
    investmentsLoaded = NO;
}

- (void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) fetchInvestments {
    
    AOInvestmentsMethodRequest *request = [[AOInvestmentsMethodRequest alloc] init];
    request.isSettled = NO;
    [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary *resultDictionary) {
        investmentsLoaded = YES;
        NSMutableSet *mSet = [NSMutableSet new];
        for (id investmentDict in [resultDictionary objectForKey:@"investments"]) {
            [mSet addObject:[investmentDict objectForKey:@"marketId"]];
        }
        
        [self setMarketsOpenedInvestmentsForGroups:RESIDENT.binaryGroups withInvestments:mSet];
        [self setMarketsOpenedInvestmentsForGroups:RESIDENT.optionPlusGroup withInvestments:mSet];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [table reloadData];
            
        });
    } failed:^(NSDictionary *errDictionary) {
        // NSLog(@"fetch investments failed.");
    }];
}

- (void) setMarketsOpenedInvestmentsForGroups:(NSMutableArray*)groups withInvestments:(NSSet*)marketsWithInvestmetns {
    
    for (int i = 0; i < dataSource.count; i++) {
        
        for (AOMarket *m in [[dataSource objectAtIndex:i] objectForKey:@"markets"]) {
            if ([marketsWithInvestmetns member:[NSNumber numberWithLong:m.l_id]]) {
                [[RESIDENT.marketsInfo objectForKey:[NSString stringWithFormat:@"%ld",m.l_id]] setObject:[NSNumber numberWithBool:YES] forKey:@"hasOpenedOptions"];
                
                
                m.hasOpenedOptions = YES;
            } else {
                m.hasOpenedOptions = NO;
            }
        }
    }
}

- (void) addLsTables {
    [self removeLsTables];
    // NSLog(@"AOTradeAssetsGroupsViewController.addLsTables");
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < dataSource.count; i++) {
        
        for (AOMarket *m in [[dataSource objectAtIndex:i] objectForKey:@"markets"]) {
            NSString *idString = [NSString stringWithFormat:@"%li",[m getId]];
            if (m.isOptionPlusMarket) {
                [items addObject:[NSString stringWithFormat:@"op_%@", idString]];
            } else {
                [items addObject:[NSString stringWithFormat:@"aotps_1_%li", [m getId]]];
            }
        }
    }
    
    LSExtendedTableInfo *lsTableInfo = [LSExtendedTableInfo extendedTableInfoWithItems:items mode:LSModeCommand fields:RESIDENT.lsConnectionHandler.fields dataAdapter:LS_DATA_ADAPTER snapshot:YES];
    lsTableInfo.requestedMaxFrequency= 1.0;
    [RESIDENT.lsConnectionHandler addTableWithTableInfo:lsTableInfo delegate:self];
    [lsTableInfoArray addObject:lsTableInfo];
}

- (void) removeLsTables  {
    // NSLog(@"AOTradeAssetsGroupsViewController.removeLsTables");
    
    if (lsTableInfoArray.count == 0) {
        return;
    }
    
    for (LSExtendedTableInfo *tableInfo in lsTableInfoArray) {
        [RESIDENT.lsConnectionHandler removeTableWithTableInfo:tableInfo];
    }
    [lsTableInfoArray removeAllObjects];
    
   
}

- (IBAction) pressedBinaryTab:(id)sender {
    // NSLog(@"Binary");
    [self resetButtonColor];
    binaryButton.backgroundColor = MA_SECONDTAB_SELECTEDCOLOR;
    isOnOptionPlus = NO;
    binaryButton.selected = YES;
    optionPlusButton.selected = NO;
    [self updateDataSource];
#ifndef COPYOP
    [self buildHelpButtonWithTitle:@"assetPageHelpTitle" andContent:@"assetPageHelpContent"];
#endif
}

- (IBAction) pressedOptionPlusTab:(id)sender {
    // NSLog(@"option+");
    [self resetButtonColor];
    optionPlusButton.backgroundColor = MA_SECONDTAB_SELECTEDCOLOR;
    isOnOptionPlus = YES;
    binaryButton.selected = NO;
    optionPlusButton.selected = YES;
    [self updateDataSource];
#ifndef COPYOP
    [self buildHelpButtonWithTitle:@"assetPagePlusHelpTitle" andContent:@"assetPagePlusHelpContent"];
#endif
}

- (void) setupBinaryButton {
    //    binaryButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    UIColor *textColor = [theme assetButtonTextColor];
    [binaryButton setTitleColor:textColor forState:UIControlStateNormal];
    [binaryButton setTitle:AOLocalizedString(@"home.page.tab.1") forState:UIControlStateNormal];
    //    [binaryButton setImage:[UIImage imageNamed:@"icn_tab_bin_off"] forState:UIControlStateNormal];
    [binaryButton setTitleColor:textColor forState:UIControlStateSelected];
    [binaryButton setTitle:AOLocalizedString(@"home.page.tab.1") forState:UIControlStateSelected];
    binaryButton.titleLabel.font = [theme assetButtonsFont];
    //    [binaryButton setImage:[UIImage imageNamed:@"icn_tab_bin_on"] forState:UIControlStateSelected];
}

- (void) setupOptionPlus {
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    UIColor *textColor = [theme assetButtonTextColor];
    //    optionPlusButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [optionPlusButton setTitleColor:textColor forState:UIControlStateNormal];
    [optionPlusButton setTitle:AOLocalizedString(@"home.page.tab.2") forState:UIControlStateNormal];
    //    [optionPlusButton setImage:[UIImage imageNamed:@"option_plus_icon_1_off"] forState:UIControlStateNormal];
    [optionPlusButton setTitleColor:textColor forState:UIControlStateSelected];
    [optionPlusButton setTitle:AOLocalizedString(@"home.page.tab.2") forState:UIControlStateSelected];
    optionPlusButton.titleLabel.font = [theme assetButtonsFont];
    //    [optionPlusButton set]
    //    [optionPlusButton setImage:[UIImage imageNamed:@"option_plus_icon_1"] forState:UIControlStateSelected];
}

- (void) resetButtonColor {
    binaryButton.backgroundColor = [UIColor clearColor];
    optionPlusButton.backgroundColor = [UIColor clearColor];
}

- (void) updateDataSource {
    [dataSource removeAllObjects];
    
    
    if(!isOnOptionPlus) {
        [dataSource addObjectsFromArray:RESIDENT.binaryGroups];
    }
    else {
        [dataSource addObjectsFromArray:RESIDENT.optionPlusGroup];
    }
    
    if (!_allAssets) {
        //leaving only top and favourite markets
        NSMutableArray* currenctDataSource = [NSMutableArray new];
        
        for (int g=0; g < dataSource.count; g++) {
            id groupDict = dataSource[g];
            NSString* groupID = [groupDict objectForKey:@"id"] ;
            
            NSDictionary *newGroupDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:groupID,@"key",[NSMutableArray new],@"markets", nil];
            [currenctDataSource addObject:newGroupDict];
            
            for (int m=0;m<[[groupDict objectForKey:@"markets"] count];m++) {
                AOMarket* market  = [groupDict objectForKey:@"markets"][m];
                NSString* mIdStr = [NSString stringWithFormat:@"%li", market.l_id];
                
                if ( market.isTopMarket || ([RESIDENT.userFavoriteAssets objectForKey:mIdStr] && ![RESIDENT.userDeletedAssets objectForKey:mIdStr])){
                    
                    [[newGroupDict objectForKey:@"markets"] addObject:market];
                    
                }
            }
        }
        
        [dataSource removeAllObjects];
        dataSource = currenctDataSource ;
    }
    
    [self p_sortDataSource];
    
    [table reloadData];
    [table layoutIfNeeded];
    
    [self addLsTables];
}

- (void) p_sortDataSource
{
    for (NSDictionary *group in dataSource)
    {
        NSMutableArray *markets = group[@"markets"];
        // first sort by name
        [markets sortUsingComparator:^NSComparisonResult(AOMarket *m1, AOMarket *m2) {
            return [m1.displayName compare:m2.displayName options:NSCaseInsensitiveSearch];
        }];
        // then sort stable by visibility
        [markets sortWithOptions:NSSortStable usingComparator:^NSComparisonResult(AOMarket *m1, AOMarket *m2) {
            BOOL m1Closed = [self p_stateHidden:m1.state];
            BOOL m2Closed = [self p_stateHidden:m2.state];
            if (m1Closed && !m2Closed)
            {
                return NSOrderedAscending;
            }
            else if (!m1Closed && m2Closed)
            {
                return NSOrderedDescending;
            }
            else
            {
                return NSOrderedSame;
            }
        }];
    }
}

- (void) updateAssetTimer {
    //    // NSLog(@"updateAssetTimer tick");
    if (!assetTimer) {
        assetTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateAssetTimer) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:assetTimer forMode:NSRunLoopCommonModes];
    }
    
    for (UITableViewCell *cell in [table visibleCells]) {
        NSIndexPath *indexPath = [table indexPathForCell:cell];
        if ([self isSeeAllCell:indexPath]) {
            continue;
        }
        
        AOMarket *market = (AOMarket*)[[[dataSource objectAtIndex:indexPath.section] objectForKey:@"markets"] objectAtIndex:indexPath.row];
        [market updateTimeLeft];
    }
    //    [table reloadData]; // if you are in delete mode reloadData will close it (which happens every 1 sec)
    
    for (NSIndexPath *indexPath in [table indexPathsForVisibleRows]) {
        if (toDeleteRow != -1 && (indexPath.row == toDeleteRow && indexPath.section == toDeleteSection)) {
            continue;
        }
        if ([self isSeeAllCell:indexPath]) {
            continue;
        }
        [self setupCell:nil forIndexPath:indexPath];
    }
}

- (BOOL) isSeeAllCell:(NSIndexPath*)indexPath {
    
    if (!_allAssets && indexPath.section == dataSource.count - 1) {
        
        if (indexPath.row == [[[dataSource objectAtIndex:indexPath.section] objectForKey:@"markets"] count]) {
            return true;
        }
    }
    return false;
}

#pragma mark - table delegate

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
	return [dataSource count];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger cnt = [[[dataSource objectAtIndex:section] objectForKey:@"markets"] count];
    if (!_allAssets && section == dataSource.count - 1) {
        return cnt + 1;
    }
    return cnt;
}

- (void) setupCell:(AOTradeAssetsViewCell*)cell forIndexPath:(NSIndexPath*)indexPath {
    if (!cell) {
        if (!indexPath) {
            return;
        }
        cell = (AOTradeAssetsViewCell*)[table cellForRowAtIndexPath:indexPath];
    }
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    cell.contentView.backgroundColor = [theme assetsBackgroundColor];
    cell.clipsToBounds = YES;
    
    AOMarket *market = [[[dataSource objectAtIndex:indexPath.section] objectForKey:@"markets"] objectAtIndex:indexPath.row];
    
    cell.logoBorder.backgroundColor = [theme assetsLogoBorderColor];
    cell.logo.backgroundColor = MA_ICON_BG_COLOR;
    cell.logo.image = [AOUtils iconForMarket:market.l_id];
    cell.openOptions.hidden = !market.hasOpenedOptions;
    cell.plus.hidden = !market.isOptionPlusMarket;
    
    cell.marketName.textColor = [theme assetsMarketNameColor];
    cell.marketName.text = market.displayName;
    
    cell.stateInfo1.textColor = [theme assetsMarketNameColor];
    cell.stateInfo1.text = market.stateText;
    
    if (market.isLast5Minutes) {
        cell.stateInfo2.textColor = [theme assetsLast5MinutesTextColor];
    } else {
        cell.stateInfo2.textColor = [theme assetsTimeLeftTextColor];
    }
    cell.stateInfo2.text = market.timeLeftText;
    
    cell.level.hidden = !((market.state >= AOOpportunityStateOpened) && (market.state <= AOOpportunityStateClosed));
    cell.level.textColor = [theme assetsMarketNameColor];
    cell.level.text = market.level;
    
    if (![cell.layer valueForKey:@"AODidLoadCell"]) {
        [cell.layer setValue:@"" forKey:@"AODidLoadCell"];
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0, cell.contentView.frame.size.height-0.5, cell.contentView.frame.size.width, 0.5);
        bottomBorder.backgroundColor = MA_TABLE_SEPARATORCOLOR.CGColor;
        [cell.contentView.layer addSublayer:bottomBorder];
        
        UIView *selectedBackgroundView = [[UIView alloc] init];
        selectedBackgroundView.backgroundColor = [UIColor whiteColor];
        cell.selectedBackgroundView = selectedBackgroundView;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self isSeeAllCell:indexPath]) {
#ifdef COPYOP
        return 70;
#endif
        
        return tableView.rowHeight;
    }
    
    AOMarket *market = [[[dataSource objectAtIndex:indexPath.section] objectForKey:@"markets"] objectAtIndex:indexPath.row];
    
    CGFloat ret;
    
    NSNumber *sectionKey = @(indexPath.section);
    NSSet *set = isOnOptionPlus ? _optionPlusExpandedSections : _binaryExpandedSections;
    BOOL expanded = [set containsObject:sectionKey];
    if (!expanded && [self p_stateHidden:market.state])
    {
        ret = 0.0;
    }
    else
    {
        ret = tableView.rowHeight;
    }
    
    return ret;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isSeeAllCell:indexPath]) {
        id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
        AOTradeAssetsAllViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AssetsSeeAll"];
        cell.contentView.backgroundColor = [theme allAssetsBackgroundColor];
        cell.seeAll.textColor = MA_FIRSTTAB_TEXTCOLOR;
        
#ifdef COPYOP
        cell.seeAll.hidden = YES;
        CGFloat margin = 10;
        CGFloat height = 45;
        COBorderedButton *button = [[COBorderedButton alloc] initWithFrame:CGRectMake(margin, margin, self.view.bounds.size.width-2*margin, height)];
        button.hasArrow = YES;
        NSString *seeAllAssetsText = [AOLocalizedString(@"seeAllActiveAssets") stringByReplacingOccurrencesOfString:@">>" withString:@""];
        [button setTitle:seeAllAssetsText forState:UIControlStateNormal];
        [button setUserInteractionEnabled:NO];
        [cell addSubview:button];
#endif
        
        return cell;
    }
    AOTradeAssetsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AssetsCell"];
    [self setupCell:cell forIndexPath:indexPath];
    return cell;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    AOTradeAssetsGroupsTableHeaderView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Header"];
    header.delegate = self;
    header.showsExpandButton = ![self p_allMarketsOpenInSection:section];
    header.title = [self getSectionTitleWithSection:section];
    header.section = section;
    NSSet *set = isOnOptionPlus ? _optionPlusExpandedSections : _binaryExpandedSections;
    [header setExpanded:[set containsObject:@(section)] animated:NO];
    return header;
}

- (BOOL) p_allMarketsOpenInSection:(NSInteger)section
{
    NSArray *markets = dataSource[section][@"markets"];
    for (AOMarket *market in markets)
    {
        if (market.state != 0 && [self p_stateHidden:market.state]) {
            return NO;
        }
    }
    return YES;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 21.0;
}

- (NSString*) getSectionTitleWithSection:(NSInteger)section {
    
    if (!isOnOptionPlus)
    {
        if (RESIDENT.binaryGroups.count) {
            NSNumber *sectionId = [[RESIDENT.binaryGroups objectAtIndex:section] objectForKey:@"id"];
            
            if ([sectionId intValue] == AOMarketGroupIndices) {
                return AOLocalizedString(@"rightnav.indexes");
            } else if ([sectionId intValue] == AOMarketGroupStocks) {
                return AOLocalizedString(@"rightnav.stocks");
            } else if ([sectionId intValue] == AOMarketGroupCurrencies) {
                return AOLocalizedString(@"rightnav.currencies");
            } else if ([sectionId intValue] == AOMarketGroupCommodities) {
                return AOLocalizedString(@"rightnav.commodities");
#ifdef ETRADER
            } else if ([sectionId intValue] == AOMarketGroupAbroadStocks) {
                    return AOLocalizedString(@"abroadStocks");
#endif
                //        } else if ([sectionId intValue] == AOMarketGroupAbroadStocks) {
                //            return AOLocalizedString(@"abroadStocks");
                //        } else if ([sectionId intValue] == AOMarketGroupEtraderIndices) {
                //            return AOLocalizedString(@"etraderIndices");
            }
        }
    }
#warning return "Option" as in Android
    return AOLocalizedString(@"Option");
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!_allAssets && ![self isSeeAllCell:indexPath]) {
        return YES;
    }
    return NO;
}

// Override to support editing the table view.
- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [assetTimer invalidate];
        assetTimer = nil;
        [self removeLsTables];
        
        NSMutableArray *marketsArray = [[dataSource objectAtIndex:indexPath.section] objectForKey:@"markets"];
        AOMarket *marketToRemove = (AOMarket*)[marketsArray objectAtIndex:indexPath.row];
        // NSLog(@"deleting market %@",marketToRemove);
        NSString *mIdStr = [NSString stringWithFormat:@"%ld", marketToRemove.l_id];
        [RESIDENT.userFavoriteAssets removeObjectForKey:mIdStr];
        [RESIDENT.userDeletedAssets setObject:@"" forKey:mIdStr];
        [RESIDENT savePersonalAssetsList];
        if (indexPath.row <marketsArray.count)
            [marketsArray removeObjectAtIndex:indexPath.row];
        
        [table reloadData];
    }
}

- (void) tableView:(UITableView*)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    // NSLog(@"willBeginEditingRow");
    toDeleteRow = (int)indexPath.row;
    toDeleteSection = (int)indexPath.section;
}
- (void) tableView:(UITableView*)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    // NSLog(@"didEndEditingRow");
    toDeleteRow = -1;
}

/*
 - (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
 // NSLog(@"willDisplayCell");
 }
 
 - (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath {
 // NSLog(@"didEndDisplayingCell");
 }
 */

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [table deselectRowAtIndexPath:indexPath animated:NO];
    
    if ([self isSeeAllCell:indexPath]) {
        // Pressed the "See All Assets"
        // NSLog(@"Press See All Assets");
        
        [MAINVC loadPage:@"Trade" pageName:@"TradeAssetsGroups" root:NO options:@{@"isOnOptionPlus":[NSNumber numberWithBool:isOnOptionPlus]}];
        return;
    }
    
    AOMarket *market = (AOMarket*)[[[dataSource objectAtIndex:indexPath.section] objectForKey:@"markets"] objectAtIndex:indexPath.row];
    // NSLog(@"selected market id %ld name %@", market.l_id, market.displayName);
    
    AOMarket* newMarket = [AOMarket marketWithId:market.l_id andName:market.displayName];
    if (optionPlusButton.selected){
        newMarket.isOptionPlusMarket=YES;
    }

#ifdef COPYOP
    if(newMarket.isOptionPlusMarket){
        return;
    }
#endif
    if(!IS_IPAD){
        self.view.userInteractionEnabled=NO;
    }
    [MAINVC loadPage:@"Trade" pageName:IS_IPAD?ASSET_DETAILS_IPAD_PAGE_ID:ASSET_DETAILS_PAGE_ID root:NO options:@{@"market":[newMarket copy]}];
}

#pragma mark - Methods of LSTableDelegate

- (BOOL) p_stateHidden:(int)state
{
    return state == AOOpportunityStateCreated || state == AOOpportunityStatePaused || state == AOOpportunityStateSuspended;
}

/* AO Jumping times */
- (void) table:(LSSubscribedTableKey *)tableKey itemPosition:(int)itemPosition itemName:(NSString *)itemName didUpdateWithInfo:(LSUpdateInfo *)updateInfo {
    
    
    long marketId = [[updateInfo currentValueOfFieldName:@"ET_NAME"] longLongValue];
    AOMarket *m = nil;
    for (int i = 0; i < dataSource.count; i++) {
        NSArray *markets = [[dataSource objectAtIndex:i] objectForKey:@"markets"];
        for (int j = 0; j < markets.count; j++) {
            AOMarket *tmp = [markets objectAtIndex:j];
            if (tmp.l_id == marketId) {
                m = tmp;
                break;
            }
        }
        if (m != nil) {
            break;
        }
    }
    
    
    int skinGroupId = [RESIDENT skinGroupId];
    int nState = [[updateInfo currentValueOfFieldName:@"ET_STATE"] intValue];
    
    long nOppId = [[updateInfo currentValueOfFieldName:@"ET_OPP_ID"] intValue];
    
    
    
    /* NSLog(@"itemPosition: %i itemName: %@ oppId: %ld state: %d level: %@ clr: %@ close: %@ odds: %@ short: %@",
     itemPosition,
     m.displayName,
     nOppId,
     nState,
     [updateInfo currentValueOfFieldName:[NSString stringWithFormat:@"LEVEL_%d",skinGroupId]],
     [updateInfo currentValueOfFieldName:[NSString stringWithFormat:@"CLR_%d",skinGroupId]],
     [updateInfo currentValueOfFieldName:@"ET_EST_CLOSE"],
     [updateInfo currentValueOfFieldName:@"ET_ODDS"],
     [AOUtils reformatTimeShort:[updateInfo currentValueOfFieldName:@"ET_EST_CLOSE"]]);*/
    
    BOOL networkDisconect = NO;
    if ([m.closingTimeDate timeIntervalSinceDate:[AOUtils currentDatePrecise]] < -120) {
        networkDisconect = YES;
    }
    
    // states of opportunity change from created (1) -> opened(2) -> last 10 min (3) -> closing 1 min (4) -> closing (5) -> closed(6) -> done (7)
    // when hour opp change the new one will open before the prev one is done so it will have < state
    // to keep the current opp in the row ignore updates with smaller state until the current one reach state "done"
    if (nState < m.state && m.state != AOOpportunityStateDone
        && m.state != AOOpportunityStateSuspended       // exception - suspended state (9) - you can suspend from any state and go from suspend back to any state
        && m.state != AOOpportunityStateClosing1Min     // exception - to avoid counter stuck on 00:00 we change the state client side. see AOMarket.updateTimeLeft
        && !networkDisconect) {                         // if more than 2 min passed since time est close assume there were network failure
//        // NSLog(@"Ignore update for next opp.");
        return;
    }
    
    if([updateInfo currentValueOfFieldName:@"ET_NH"] != nil && [updateInfo currentValueOfFieldName:@"ET_NH"] != (id)[NSNull null]){
        NSString *etNh = [updateInfo currentValueOfFieldName:@"ET_NH"];
        NSArray *nhParts = [etNh componentsSeparatedByString:@"|"];
        
        NSString *nhId = nhParts[0];
        
        if(nhId == nil) {
            return;
        }
                
        if([nhId integerValue] != nOppId) {
            if ([updateInfo isChangedValueOfFieldName:@"ET_EST_CLOSE"] || nState != m.state) { // update on state change because of opp change
                m.closingTimeDate = [AOUtils parseTimeFromLSUpdate:[updateInfo currentValueOfFieldName:@"ET_EST_CLOSE"]];
            }
            
            if ([updateInfo isChangedValueOfFieldName:@"ET_LAST_INV"] || nState != m.state) { // update on state change because of opp change
                m.timeLastInvestDate = [AOUtils parseTimeFromLSUpdate:[updateInfo currentValueOfFieldName:@"ET_LAST_INV"]];
            }
        }
    } else {
        
        if ([updateInfo isChangedValueOfFieldName:@"ET_EST_CLOSE"] || nState != m.state) { // update on state change because of opp change
            m.closingTimeDate = [AOUtils parseTimeFromLSUpdate:[updateInfo currentValueOfFieldName:@"ET_EST_CLOSE"]];
        }
        
        if ([updateInfo isChangedValueOfFieldName:@"ET_LAST_INV"] || nState != m.state) { // update on state change because of opp change
            m.timeLastInvestDate = [AOUtils parseTimeFromLSUpdate:[updateInfo currentValueOfFieldName:@"ET_LAST_INV"]];
        }
    }
    
    NSString *levelField = [NSString stringWithFormat:@"LEVEL_%d", skinGroupId];
    if ([updateInfo isChangedValueOfFieldName:levelField]) {
        NSString *nLevel = [updateInfo currentValueOfFieldName:levelField];
        if ([nLevel compare:@"0"] != NSOrderedSame) { // after time_est_closing JBoss is sending 0 in the updates. don't show them
            m.level = nLevel;
        }
    }
    
    int prevState = m.state;
    
    if ([updateInfo isChangedValueOfFieldName:@"ET_STATE"] || nState != m.state) { // update on state change because of opp change
        if (nState == AOOpportunityStateSuspended) {
            m.stateText = [updateInfo currentValueOfFieldName:@"ET_SUSPENDED_MESSAGE"];
        }
        m.state = nState;
        
        if ((([self p_stateHidden:prevState] && ![self p_stateHidden:nState]) ||
             ([self p_stateHidden:nState] && ![self p_stateHidden:prevState])))
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self p_sortDataSource];
                
                // currently we reload the whole table and not just the row in order to affect the visibility of the expand button in table headers
                [table reloadData];
                //                NSIndexPath *path = [self indexPathWithItemPosition:itemPosition];
                //                [table reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
            });
        }
    }
    
    /*
     if ([updateInfo isChangedValueOfFieldName:@"ET_EST_CLOSE"]){
     m.levelColor = [[updateInfo currentValueOfFieldName:[NSString stringWithFormat:@"CLR_%d",skinGroupId]] intValue];
     }
     
     if ([updateInfo isChangedValueOfFieldName:@"ET_EST_CLOSE"]){
     if (m.state >= AOOpportunityStateOpened && m.state <= AOOpportunityStateDone) {
     m.closingTime = [AOUtils reformatTimeShort:[updateInfo currentValueOfFieldName:@"ET_EST_CLOSE"]];
     } else {
     m.closingTime = [AOUtils reformatTimeLong:[updateInfo currentValueOfFieldName:@"ET_EST_CLOSE"]];
     }
     }
     
     if ([updateInfo isChangedValueOfFieldName:@"ET_ODDS"]) {
     m.returnPercent = [updateInfo currentValueOfFieldName:@"ET_ODDS"];
     }
     */
    
    /*
     @synchronized (_rowsToBeReloaded) {
     if(![_rowsToBeReloaded containsObject:indexPath]) [_rowsToBeReloaded addObject:[self indexPathWithItemPosition:itemPosition]];
     }
     
     [self performSelectorOnMainThread:@selector(reloadTableRows) withObject:nil waitUntilDone:NO];
     */
}

-(NSIndexPath*)indexPathWithItemPosition:(int)itemPosition{
    int totalCount = 0;
    int section = 0;
    int sectionCount = 0;
    for(int i=0;i<dataSource.count;i++){
        
        sectionCount = (int)[[[dataSource objectAtIndex:i] objectForKey:@"markets"] count];
        totalCount += sectionCount;
        if(itemPosition<=totalCount) {
            section = i;
            //// NSLog(@"section count %d count: %d",section,sectionCount);
            break;
        }
    }
    int rowIndex = totalCount-itemPosition-sectionCount+1;
    return [NSIndexPath indexPathForRow:-rowIndex inSection:section];
}

/*
 -(void)reloadTableRows{
 NSMutableArray *rowsToBeReloaded= nil;
 @synchronized (_rowsToBeReloaded) {
 rowsToBeReloaded= [[NSMutableArray alloc] initWithCapacity:[_rowsToBeReloaded count]];
 
 NSArray *visibleIndexPaths = [table indexPathsForVisibleRows];
 for (NSIndexPath *indexPath in _rowsToBeReloaded) {
 if(![visibleIndexPaths containsObject:indexPath]) [rowsToBeReloaded addObject:indexPath];
 }
 
 [_rowsToBeReloaded removeAllObjects];
 }
 // NSLog(@"updating cells");
 [table beginUpdates];
 [table reloadRowsAtIndexPaths:rowsToBeReloaded withRowAnimation:UITableViewRowAnimationNone];
 [table endUpdates];
 }
 */

#pragma mark AOTradeAssetsGroupsTableHeaderViewDelegate methods

- (void) aoTradeAssetsGroupsTableHeaderViewDidPressMoreButton:(AOTradeAssetsGroupsTableHeaderView *)headerView
{
    NSMutableSet *set = isOnOptionPlus ? _optionPlusExpandedSections : _binaryExpandedSections;
    NSNumber *sectionKey = @(headerView.section);
    if ([set containsObject:sectionKey])
    {
        [set removeObject:sectionKey];
    }
    else
    {
        [set addObject:sectionKey];
    }
    
    [table reloadSections:[[NSIndexSet alloc] initWithIndex:headerView.section] withRowAnimation:UITableViewRowAnimationAutomatic];
}

@end
