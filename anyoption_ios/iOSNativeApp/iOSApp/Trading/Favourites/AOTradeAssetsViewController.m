//
//  AOTradeAssetsViewController.m
//  iOSApp
//
//  Created by mac_user on 1/6/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOTradeAssetsViewController.h"
#import "AOTradeAssetsGroupsViewController.h"
#import "AOChartView.h"
#import "AOTradeAssetChartView.h"
#import "AOChartDataMethodRequest.h"
#import "AORequestService.h"

static const long miniChartStartTag=1001;

@implementation AOTradeAssetsViewController {
    AOTradeAssetsGroupsViewController *assetsTable;
    AOTradeAssetChartView* currentChartView;
    int miniChartsPage;
    
    UIButton* previousFeaturedAssetBtn;
    UIButton* nextFeaturedAssetBtn;
    
    UITapGestureRecognizer* featuredTapGestureRecognizer;
    
    
    __weak IBOutlet NSLayoutConstraint *lc_scrollViewHeight;
    
    __weak IBOutlet NSLayoutConstraint *lc_assetsTableSpaceHeight;
}

-(void)dealloc{
    // NSLog(@"AOTradeAssetsViewController dealloc");
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    if (previousFeaturedAssetBtn==nil && nextFeaturedAssetBtn==nil) {
        previousFeaturedAssetBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, _scrollView.bounds.origin.y+_scrollView.bounds.size.height/2 - 22.0, 44.0, 44.0)];
        [previousFeaturedAssetBtn setImage:[UIImage imageNamed:@"arrow_left_white"] forState:UIControlStateNormal];
        previousFeaturedAssetBtn.hidden = YES;
        [previousFeaturedAssetBtn addTarget:self action:@selector(showFeaturedAsset:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:previousFeaturedAssetBtn];
        nextFeaturedAssetBtn = [[UIButton alloc] initWithFrame:CGRectMake(_scrollView.bounds.size.width-44.0, previousFeaturedAssetBtn.frame.origin.y, 44.0, 44.0)];
        [nextFeaturedAssetBtn setImage:[UIImage imageNamed:@"arrow_right_white"] forState:UIControlStateNormal];
        [nextFeaturedAssetBtn addTarget:self action:@selector(showFeaturedAsset:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:nextFeaturedAssetBtn];
    }
    
    assetsTable = [[UIStoryboard storyboardWithName:@"Trade" bundle:nil] instantiateViewControllerWithIdentifier:@"TradeAssetsGroups"];
    assetsTable.allAssets = NO;
    [assetsTableSpace addSubview:assetsTable.view];
    [self addChildViewController:assetsTable];
    [assetsTable didMoveToParentViewController:self];
    assetsTable.view.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *vars = @{@"assetsTableView" : assetsTable.view};
    [assetsTableSpace addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[assetsTableView]|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:vars]];
    [assetsTableSpace addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[assetsTableView]|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:vars]];
    
    miniChartsPage=0;
    
    featuredTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToFeaturedAsset)];
    [_scrollView addGestureRecognizer:featuredTapGestureRecognizer];
    //2
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // NSLog(@"AOTradeAssetsViewController viewDidAppear");
    if (IS_IPAD) {
        //lc_assetsTableSpaceHeight.constant += 50;
        nextFeaturedAssetBtn.hidden=YES;
        previousFeaturedAssetBtn.hidden=YES;
        pageControl.hidden=YES;
       lc_scrollViewHeight.constant=0;
    }
    else {
          [self populateScrollview];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [currentChartView removeLsTable];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark scrollview delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // NSLog(@"scroll view did end decelerating");
    [self changeFeaturedAssetPage];
}

-(void) populateScrollview {
    if ([[_scrollView subviews] count] == 0) {
        
         CGRect sb = _scrollView.bounds;
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        [_scrollView setContentSize:CGSizeMake((4 * sb.size.width), sb.size.height)];
        pageControl.numberOfPages = 4;
        pageControl.currentPage = 0;
        
        for (int i = 0; i < 4; i++)
            [_scrollView addSubview:[self createMiniChartViewWithBoxId:i]];
            
        currentChartView = (AOTradeAssetChartView*)[_scrollView viewWithTag:miniChartStartTag];
        
        [self getMiniChartData];
    }
}

-(void)changeFeaturedAssetPage{
    [currentChartView removeLsTable];
    miniChartsPage = _scrollView.contentOffset.x/_scrollView.frame.size.width;
    
    
    if(miniChartsPage != pageControl.currentPage){
        AOTradeAssetChartView* newMiniChartView =  [self createMiniChartViewWithBoxId:currentChartView.tag-miniChartStartTag];
        
        [currentChartView removeFromSuperview];
        currentChartView=nil;
        [_scrollView addSubview:newMiniChartView];
        currentChartView = newMiniChartView;
        
        pageControl.currentPage = miniChartsPage;
        currentChartView = (AOTradeAssetChartView*)[_scrollView viewWithTag:miniChartStartTag+miniChartsPage];
        [self getMiniChartData];
    }
    
    if(_scrollView.contentOffset.x==_scrollView.contentSize.width-_scrollView.bounds.size.width)
        nextFeaturedAssetBtn.hidden=YES;
    else
        nextFeaturedAssetBtn.hidden=NO;
    
    
    if(_scrollView.contentOffset.x==0)
        previousFeaturedAssetBtn.hidden=YES;
    else
        previousFeaturedAssetBtn.hidden=NO;
}


-(void)showFeaturedAsset:(UIButton*)sender {
    if(sender == previousFeaturedAssetBtn){
        if(_scrollView.contentOffset.x)
            [_scrollView setContentOffset:CGPointMake(_scrollView.contentOffset.x-_scrollView.bounds.size.width, _scrollView.contentOffset.y)];
            [self changeFeaturedAssetPage];
    }
    else {
        if(_scrollView.contentOffset.x<_scrollView.contentSize.width-_scrollView.bounds.size.width)
            [_scrollView setContentOffset:CGPointMake(_scrollView.contentOffset.x+_scrollView.bounds.size.width, _scrollView.contentOffset.y)];
            [self changeFeaturedAssetPage];
    }
}

-(void)goToFeaturedAsset {
//    NSString *mIdStr = [NSString stringWithFormat:@"%ld", currentChartView.market.l_id];
    if (currentChartView.market != nil)
    {
        [MAINVC loadPage:@"Trade" pageName:IS_IPAD?ASSET_DETAILS_IPAD_PAGE_ID:ASSET_DETAILS_PAGE_ID root:NO options:@{@"market":currentChartView.market}];
    }
}

-(void) getMiniChartData {
    AOChartDataMethodRequest *request = [[AOChartDataMethodRequest alloc] init];
    
    request.box = miniChartsPage;
    request.opportunityTypeId = 1;
    [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary* resultDictionary){
        
        AOChartData* chartData = [AOChartData new];
        [chartData setValuesForKeysWithDictionary:resultDictionary];
        [currentChartView setChartData:chartData];
         currentChartView.frame = _scrollView.bounds;
    }failed:nil];
    
}

-(AOTradeAssetChartView*) createMiniChartViewWithBoxId:(long)box {
    CGRect sb = _scrollView.bounds;
    AOTradeAssetChartView *miniChartView = [[AOTradeAssetChartView alloc] init];
 
    miniChartView.chartViewMode = ChartViewModeMini;
    miniChartView.tag = miniChartStartTag+box;
    miniChartView.frame = CGRectMake(box*sb.size.width, 0, sb.size.width, sb.size.height);
    
    return miniChartView;
}

@end
