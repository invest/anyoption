//
//  AOTradeAssetsGroupsTableHeaderView.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 7/22/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AOTradeAssetsGroupsTableHeaderViewDelegate;

@interface AOTradeAssetsGroupsTableHeaderView : UITableViewHeaderFooterView

@property (nonatomic, weak) id<AOTradeAssetsGroupsTableHeaderViewDelegate> delegate;

@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) BOOL expanded;
- (void) setExpanded:(BOOL)expanded animated:(BOOL)animated;
@property (nonatomic, assign) BOOL showsExpandButton;
@property (nonatomic, copy) NSString *title;

@end

@protocol AOTradeAssetsGroupsTableHeaderViewDelegate <NSObject>

@optional
- (void) aoTradeAssetsGroupsTableHeaderViewDidPressMoreButton:(AOTradeAssetsGroupsTableHeaderView *)headerView;

@end