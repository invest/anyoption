//
//  AOTradeAssetsViewCell.m
//  iOSApp
//
//  Created by Tony on 4/12/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOTradeAssetsViewCell.h"

#import "AOThemeController.h"

@implementation AOTradeAssetsViewCell

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    self.marketName.font = [theme marketNameFont];
    self.stateInfo2.font = [theme marketTimeFont];
}

@end
