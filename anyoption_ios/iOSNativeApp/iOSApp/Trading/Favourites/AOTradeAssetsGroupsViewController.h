//
//  AOTradeAssetsGroupsViewController.h
//  iOSApp
//
//  Created by Tony on 4/12/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AOBaseViewController.h"
#import "AOCenteredTextImage.h"

enum {
    AOMarketGroupIndices = 2,
    AOMarketGroupStocks = 3,
    AOMarketGroupCurrencies = 4,
    AOMarketGroupCommodities = 5,
    AOMarketGroupAbroadStocks = 6,
    AOMarketGroupEtraderIndices = 7
};

@interface AOTradeAssetsGroupsViewController : AOBaseViewController <UITableViewDataSource,UITableViewDelegate,LSTableDelegate> {
    __weak IBOutlet UITableView *table;
    __weak IBOutlet UIView *buttonContainer;
    __weak IBOutlet UIButton *binaryButton;
    __weak IBOutlet UIButton *optionPlusButton;
    __weak IBOutlet NSLayoutConstraint *lcButtonContainerHeight;
}

@property BOOL allAssets;

@end
