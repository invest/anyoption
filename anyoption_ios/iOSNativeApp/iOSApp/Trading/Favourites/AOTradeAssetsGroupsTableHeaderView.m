//
//  AOTradeAssetsGroupsTableHeaderView.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 7/22/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOTradeAssetsGroupsTableHeaderView.h"

@interface AOTradeAssetsGroupsTableHeaderView ()
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end

@implementation AOTradeAssetsGroupsTableHeaderView
{
    UIImageView *_imgViewCircle;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    self.backView.backgroundColor = [theme assetsHeaderBackgroundColor];
    self.lblTitle.textColor = MA_SECTION_TEXTCOLOR;
    self.lblTitle.font = MA_SECTION_TEXTFONT;
    
    self.btnMore.hidden = !self.showsExpandButton;
    
    UIImage *image = [self.btnMore imageForState:UIControlStateNormal];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:image];
    [self.btnMore addSubview:imgView];
    imgView.autoresizingMask = 0xff;
    _imgViewCircle = imgView;
    
    [self.btnMore setImage:nil forState:UIControlStateNormal];
    [self.btnMore setTitle:nil forState:UIControlStateNormal];
}

- (IBAction) btnMorePressed:(id)sender
{
    [self onMoreButtonPressed];
    
    [self setExpanded:!self.expanded animated:YES];
}

- (void) setTitle:(NSString *)title
{
    if (![_title isEqualToString:title])
    {
        _title = [title copy];
        
        self.lblTitle.text = _title;
    }
}

- (void) setExpanded:(BOOL)expanded
{
    [self setExpanded:expanded animated:NO];
}

- (void) setExpanded:(BOOL)expanded animated:(BOOL)animated
{
    _expanded = expanded;
    
    void (^update)() = ^
    {
        if (expanded)
        {
            _imgViewCircle.transform = CGAffineTransformMakeRotation(M_PI / 4.0);
        }
        else
        {
            _imgViewCircle.transform = CGAffineTransformIdentity;
        }
    };
    
    if (animated)
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             update();
                         } completion:nil];
    }
    else
    {
        update();
    }
}

- (void) onMoreButtonPressed
{
    if ([self.delegate respondsToSelector:@selector(aoTradeAssetsGroupsTableHeaderViewDidPressMoreButton:)])
    {
        [self.delegate aoTradeAssetsGroupsTableHeaderViewDidPressMoreButton:self];
    }
}

- (void)setShowsExpandButton:(BOOL)showsExpandButton
{
    if (_showsExpandButton != showsExpandButton)
    {
        _showsExpandButton = showsExpandButton;
        
        self.btnMore.hidden = !_showsExpandButton;
    }
}

- (BOOL) p_shouldReceiveTouchForMoreButtonAtPoint:(CGPoint)point
{
    if (self.showsExpandButton)
    {
        CGRect rect = CGRectInset(self.btnMore.frame, -10.0, -10.0);
        if (CGRectContainsPoint(rect, point))
        {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL) pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if ([self p_shouldReceiveTouchForMoreButtonAtPoint:point])
    {
        return YES;
    }
    return [super pointInside:point withEvent:event];
}

- (UIView *) hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if ([self p_shouldReceiveTouchForMoreButtonAtPoint:point])
    {
        return self.btnMore;
    }
    return [super hitTest:point withEvent:event];
}

@end
