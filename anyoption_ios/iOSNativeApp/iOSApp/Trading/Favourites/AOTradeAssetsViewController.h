//
//  AOTradeAssetsViewController.h
//  iOSApp
//
//  Created by mac_user on 1/6/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOBaseViewController.h"

@class AOCenteredTextImage, AppDelegate;

@interface AOTradeAssetsViewController : AOBaseViewController <UIScrollViewDelegate> {
    
    NSDateFormatter *dateFormatter;
    
    __weak IBOutlet UIScrollView *_scrollView;
    __weak IBOutlet UIPageControl *pageControl;
    __weak IBOutlet UIView *assetsTableSpace;
}

@end