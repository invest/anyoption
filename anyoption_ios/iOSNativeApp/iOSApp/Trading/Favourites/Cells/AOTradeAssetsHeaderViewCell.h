//
//  AOTradeAssetsHeaderViewCell.h
//  iOSApp
//
//  Created by Tony on 4/12/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AOTradeAssetsHeaderViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
