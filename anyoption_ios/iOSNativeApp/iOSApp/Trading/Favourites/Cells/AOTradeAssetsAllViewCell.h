//
//  AOTradeAssetsAllViewCell.h
//  iOSApp
//
//  Created by Tony on 4/12/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AOLabel.h"

@interface AOTradeAssetsAllViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AOLabel *seeAll;

@end
