//
//  AOTradeAssetsViewCell.h
//  iOSApp
//
//  Created by Tony on 4/12/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AOCenteredTextImage.h"

@interface AOTradeAssetsViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *logoBorder;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet AOCenteredTextImage *openOptions;
@property (weak, nonatomic) IBOutlet UIImageView *plus;
@property (weak, nonatomic) IBOutlet UILabel *marketName;
@property (weak, nonatomic) IBOutlet UILabel *stateInfo1;
@property (weak, nonatomic) IBOutlet UILabel *stateInfo2;
@property (weak, nonatomic) IBOutlet UILabel *level;

@end
