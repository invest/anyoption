//
//  AOChartView.m
//  JSON Test
//
//  Created by Anton Antonov on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//  Notes: Get 'real' x -> (self.center.x - self.frame.size.width/2)

#import "AOChartView.h"
#import "AOMarketRate.h"
#import "AOInvestment.h"
#import "AOChartDataMethodRequest.h"
#import "AORequestService.h"
#import "LightstreamerClient.h"
#import "AOBaseChartView.h"
#include <math.h>

@interface AOChartView()

- (int) calcPeriodInSecFrom:(NSDate*)from to:(NSDate*)to;
- (double) getLevel:(AOMarketRate*)rate;
- (void) cleanUnneededRates;
- (void) initDisplay;
- (void) initInvestments;
- (void) initChartAreas;
- (int) getLengthInSec;
- (UIColor*) getColor:(int)colorId;
- (void) drawChartSegmentWithContext:(CGContextRef)context x1:(float)x1 y1:(float)y1 x2:(float)x2 y2:(float)y2 height:(int)height ySpread:(float)ySpread;
- (void) updateState;
- (void) updateYLabels;
- (void) updateCurrentLevelLabel;
- (void) updateCurrentReturn;
- (void) ensureYOffset;
- (void) takeRateSample;

@property (nonatomic, readwrite, assign) CGFloat currentLevelX;
@property (nonatomic, readwrite, assign) CGFloat currentLevelY;

@end

@implementation AOChartView {
    
    float touchXDiff;
    
    IBOutlet UIView *timeLine;
    
    IBOutlet UIView *cursorLine;
    
    NSMutableDictionary* investmentButtonsDict;
    NSMutableDictionary* investmentLinesDict;
    
    BOOL isReady;
    
    UIImageView *glowerImageView;
    
    BOOL isInInvestmentMode;
    
    CGPoint lastTouchPoint;
    
    float chartXOffset;
    
    int showingInvestmentIndex;
    BOOL isShowingInvestment;
    
    CGRect initialFrame;
    
    float chartActiveAreaHeight ;
    
    UIImageView* stripsImageView;
    
    
    
}

@synthesize zoomCoef=_zoomCoef,dateFormatter,secPerPixel,portrait,marketId,oppId,oppTypeId,oppState,timezoneOffset,timeEstClosing,scheduled,decimalPoint,rates=_rates,ratesLoaded,hasPreviousHour,previousHourLoaded,displayStartTime,firstSec,ratesStartTime,yMinVal,yMaxVal,currentLevel,currentLevelTime,displayOffset,call,put,timeFirstInvest,timeLastInvest,investments,investmentsLoaded,crrMin,crrSec,totalProfit,totalAmount,chartAreas,chartOffsetLeft,chartOffsetTop,chartOffsetRight,chartOffsetBottom,levelMin,levelMax,callBtn,putBtn,winOdds,loseOdds,isPreviousHour;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    // NSLog(@"AOChartView.initWithCoder");
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initialize];
    }
    
    return self;
}

- (void) initialize {
    
    self.currentLevelLabelWidth = 70.0f;
    
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    initialFrame = self.frame;
    
    callSmall = [UIImage imageNamed:@"call_small.png"];
    putSmall = [UIImage imageNamed:@"put_small.png"];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    oppState = AOOpportunityStateOpened;
    chartAreas = [[NSMutableArray alloc] init];
    portrait = YES;
    self.zoomCoef = 1;
    
    investmentButtonsDict = [[NSMutableDictionary alloc] init];
    investmentLinesDict = [[NSMutableDictionary alloc] init];
    
    cursorLine.contentMode = UIViewContentModeRedraw;
    
    self.currentLevelLabel = [[UILabel alloc] initWithFrame:CGRectMake(-9990.0, 0.0, self.currentLevelLabelWidth, 40.0)];
    self.currentLevelLabel.textColor = [UIColor whiteColor];
    self.currentLevelLabel.adjustsFontSizeToFitWidth = YES;
    
    [self addSubview:self.currentLevelLabel];
    self.currentLevelLabel.hidden=YES;
    
    UIImage *glowImage = [UIImage imageNamed:@"currLevelLineGlow"];
    glowerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"currLevelLineGlow"]];
    glowerImageView.frame = CGRectMake(0.0, -20, glowImage.size.width, glowImage.size.height);
    glowerImageView.hidden = YES;
    [self addSubview:glowerImageView];
    
    chartXOffset = 0.0;
    showingInvestmentIndex=-1;
    
    if(cursorLine==nil) {
        cursorLine = [UIView new];
        cursorLine.backgroundColor = CHART_CURRENT_LEVEL_LINE_COLOR;
        [self addSubview:cursorLine];
    }
    
    stripsImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"strips"]];
    stripsImageView.frame = CGRectMake(0, 0, 0, 0);
    [self addSubview:stripsImageView];
    
}

- (void)animateGlow {
    glowerImageView.hidden = NO;
    CABasicAnimation* blinkingAnimation;
    blinkingAnimation = [CABasicAnimation animationWithKeyPath:@"position.x"];
    blinkingAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    blinkingAnimation.toValue = [NSNumber numberWithFloat:initialFrame.size.width];
    blinkingAnimation.additive = NO;
    blinkingAnimation.autoreverses = NO;
    blinkingAnimation.duration = 1.5;
    blinkingAnimation.fillMode = kCAFillModeBoth;
    blinkingAnimation.repeatCount = 99999;
    [glowerImageView.layer addAnimation:blinkingAnimation forKey:@"glowAnimation"];
}

-(void)setChartConstrains {
    
    switch (self.chartViewMode) {
        case ChartViewModeMini:
            chartOffsetLeft = 0;
            chartOffsetTop = 10;
            chartOffsetRight = 0;
            chartOffsetBottom = 10;
            chartActiveAreaHeight = self.bounds.size.height - chartOffsetTop - chartOffsetBottom;
            break;
            
        case ChartViewModeLandscape:
            break;
            
        default:
            chartOffsetLeft = 0;
            chartOffsetTop = 60;
            chartOffsetRight = 0;
            chartOffsetBottom = 50;
            chartActiveAreaHeight = self.bounds.size.height - chartOffsetTop - chartOffsetBottom;
            break;
    }
    timeLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"timeline.png"]];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if([[event allTouches]count] >1 ) {
        return;
    }
    
    if (self.chartViewMode==ChartViewModePortrait && self.delegate != nil){
        [self.delegate beganDraggingChartView];
    }
    
    CGPoint touchPoint = lastTouchPoint =  [[touches anyObject] locationInView:self];
    // NSLog(@"touchesBegan - x: %f, y: %f", touchPoint.x, touchPoint.y);
    touchXDiff = touchPoint.x - self.bounds.origin.x ;
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if([[event allTouches]count] >1 ) {
        return;
    }
    
    CGPoint touchPoint = [[touches anyObject] locationInView:self.superview];
    
    if (!isInInvestmentMode /*&& self.currentLevelX>self.superview.bounds.size.width - chartOffsetRight*/) {
        
        chartXOffset =touchPoint.x - touchXDiff;
        
        if(chartXOffset + self.frame.size.width < self.superview.bounds.size.width){
            
            chartXOffset =  self.superview.bounds.size.width - self.frame.size.width;
        }
        else if (chartXOffset >= 0){
            if (!hasPreviousHour || (hasPreviousHour && previousHourLoaded)) {
                chartXOffset = 0;
            }
        }
        
        self.center = CGPointMake(chartXOffset + self.frame.size.width/2, self.center.y);
        [self p_positionLabels];
    }
    else {
        
        if (abs(touchPoint.x - lastTouchPoint.x )>10) {
            
            if(touchPoint.x - lastTouchPoint.x > 0) {
                [self showNextInvestment];
            }
            else {
                
                [self showPrevInvestment];
            }
            
        }
        else {
            return;
        }
    }
    
    lastTouchPoint = touchPoint;
}


- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    lastTouchPoint = CGPointZero;
    isShowingInvestment=NO;
    
    if (chartXOffset >= 0){
        self.center = CGPointMake(chartXOffset + self.frame.size.width/2, self.center.y);
        if (hasPreviousHour &&  !previousHourLoaded) {
            [self scrollBack];
        }
    }
    
    if (self.chartViewMode==ChartViewModePortrait && self.delegate != nil)
        [self.delegate endedDraggingChartView];
}

- (float) zoomCoef {
    return _zoomCoef;
}

- (void) setZoomCoef:(float)zc {
    _zoomCoef  = zc;
    if(zc>=1 && isReady){
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, initialFrame.size.width*self.zoomCoef, self.frame.size.height);
        [self cleanUnneededRates];
        [self initInvestments];
        [self needsDisplayMethod];
    }
}

- (void) showNextInvestment {
    
    if (!isShowingInvestment && showingInvestmentIndex < investments.count-1) {
        showingInvestmentIndex++;
        isShowingInvestment=YES;
        [self showInvestment];
        isShowingInvestment=NO;
    }
}

-(void) showPrevInvestment {
    
    if (!isShowingInvestment && showingInvestmentIndex > 0) {
        isShowingInvestment=YES;
        
        showingInvestmentIndex--;
        [self showInvestment];
        isShowingInvestment=NO;
        
    }
}

-(void) showInvestment {
    
    [self unselectInvestmentButtons];
    isInInvestmentMode=YES;
    
    for (AOChartInvestmentButton* sb in investmentButtonsDict) {
        
        AOChartInvestmentButton* aInvBtn = (AOChartInvestmentButton*)[investmentButtonsDict objectForKey:sb];
        AOInvestment* selInv = investments[showingInvestmentIndex];
        
        
        if(selInv == aInvBtn.investment){
            aInvBtn.selected = YES;
            
            CGPoint screenPoint = [[[UIApplication sharedApplication].delegate window] convertPoint:aInvBtn.center
                                                                                           fromView:aInvBtn.superview];
            if (self.delegate != nil) {
                [self.delegate didTouchInvestment:aInvBtn.investment screenPoint:screenPoint];
            }
            
            if (screenPoint.x > initialFrame.size.width) {
                
                self.center = CGPointMake(self.center.x-(screenPoint.x - initialFrame.size.width)-30,self.center.y);
            }
            
            if (screenPoint.x < 30 && self.center.x + (30-screenPoint.x) < self.frame.size.width/2 ) {
                
                
                self.center = CGPointMake(self.center.x + (30-screenPoint.x),self.center.y);
            }
            
            
            break;
        }
    }
}

- (void) setCurrentLevelY:(CGFloat)currentLevelY
{
    if (_currentLevelY != currentLevelY)
    {
        _currentLevelY = currentLevelY;
        
        [self setNeedsLayout];
    }
}

- (void)drawRect:(CGRect)rect
{
    //    // NSLog(@"AOChartView drawrect. Rates Count: %lu %f",(unsigned long)[rates count],self.zoomCoef);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //float width = self.frame.size.width;
    
    int height = self.bounds.size.height - chartOffsetTop - chartOffsetBottom;
    
    float ySpread = yMaxVal - yMinVal;
    
    if (ySpread==0) {
        ySpread=0.1;  //avoiding division by zero
    }
    
    if ([self.rates count] == 0) return;
    CGContextSetStrokeColorWithColor(context, [self getColor:kColorIdChart].CGColor);
    CGContextSetLineWidth(context, 1.0);
    CGContextSetLineDash(context, 0, nil, 0);
    
    AOMarketRate *prev = nil;
    AOMarketRate *crr = nil;
    int testRemove = 0;
    
    for (int i = 0; i < [self.rates count]; i++) {
        crr = [self.rates objectAtIndex:i];
        //if (displayOffset <= crr.chartXPosition && crr.chartXPosition - displayOffset <= width - chartOffsetLeft - chartOffsetRight) {
        self.currentLevelX = crr.chartXPosition;//chartOffsetLeft + (crr.chartXPosition - displayOffset)*self.zoomCoef;
        
        
        self.currentLevelY = chartOffsetTop + height * (1 - ([self getLevel:crr] - yMinVal) / ySpread);
        
        if (isnan(self.currentLevelY)) {
            self.currentLevelY = chartOffsetTop;
        }
        if (nil != prev) {
            
            for (AOInvestment *inv in investments){
                if (inv.chartXPosition < self.currentLevelX && inv.chartXPosition > prev.chartXPosition){
                    testRemove++;
                    CGContextAddLineToPoint(context, inv.chartXPosition, [self investmentChartYPosition:inv]);
                }
            }
            
            if (!isnan(self.currentLevelX) && !isnan(self.currentLevelY)) {
                testRemove++;
                CGContextAddLineToPoint(context, roundf(self.currentLevelX), roundf(self.currentLevelY));
            }
            
        } else {
            if (!isnan(self.currentLevelX) && !isnan(self.currentLevelY)) {
                testRemove++;
                CGContextMoveToPoint(context, roundf(self.currentLevelX), roundf(self.currentLevelY));
            }
        }
        prev = crr;
        //}
    }
    
    
    
    
    CGContextStrokePath(context);
    
    CGContextSetFillColorWithColor(context, [self getColor:kColorIdCrrLvlCircle].CGColor);
    CGContextAddEllipseInRect(context, CGRectMake(self.currentLevelX, self.currentLevelY, 4, 4));
    CGContextDrawPath(context, kCGPathFill);
    
    if(self.chartViewMode != ChartViewModeMini || oppState==1 || oppState==8 || oppState==9) {
        NSDate* last10MinStart = [timeLastInvest dateByAddingTimeInterval:-(10*60)];
        
        float lastInvestmentsLength = self.zoomCoef * ([self calcPeriodInSecFrom:last10MinStart to:timeLastInvest]/secPerPixel);
        float closedForInvestmentLength = self.zoomCoef * ([self calcPeriodInSecFrom:timeLastInvest to:timeEstClosing]/secPerPixel);
        float normalStateLength = self.frame.size.width -closedForInvestmentLength -lastInvestmentsLength;
        
        id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
        
        CGContextSetFillColorWithColor(context, [theme chartColorlineDefaultColor].CGColor);
        CGContextAddRect(context, CGRectMake(0.0, self.frame.size.height - 4.5 , normalStateLength, 4.5));
        CGContextDrawPath(context, kCGPathFill);
        
        CGContextSetFillColorWithColor(context, [theme chartColorlineLastInvestmentColor].CGColor);
        CGContextAddRect(context, CGRectMake(normalStateLength, self.frame.size.height - 4.5 , lastInvestmentsLength, 4.5));
        CGContextDrawPath(context, kCGPathFill);
        
        CGContextSetFillColorWithColor(context, [theme chartColorlineClosingColor].CGColor);
        CGContextAddRect(context, CGRectMake(self.frame.size.width - closedForInvestmentLength, self.frame.size.height - 4.5 , closedForInvestmentLength, 4.5));
        CGContextDrawPath(context, kCGPathFill);
        
        CGContextSetFillColorWithColor(context, [theme chartColorlineShadowColor].CGColor);
        CGContextAddRect(context, CGRectMake(0, self.frame.size.height - 4.5 , self.currentLevelX, 4.5));
        CGContextDrawPath(context, kCGPathFill);
        
        
        stripsImageView.frame =   CGRectMake(self.frame.size.width - closedForInvestmentLength, 0 , closedForInvestmentLength, self.frame.size.height);
        
        
    }
    else {
        
    }
    
    [self drawTimeLabels];
}

- (void) drawChartSegmentWithContext:(CGContextRef)context x1:(float)x1 y1:(float)y1 x2:(float)x2 y2:(float)y2 height:(int)height ySpread:(float)ySpread
{
    CGContextAddLineToPoint(context, roundf(x2), roundf(y2));
}

- (void)setChartData:(AOChartData*)data
{
    // NSLog(@"AOChartView.setChartData -> %@", data.rates);
    
    [self reset];
    
    timezoneOffset = data.timezoneOffset;
    marketId = data.marketId;
    oppId = data.opportunityId;
    oppTypeId = data.opportunityTypeId;
    
    
    
    
    self.rates = [[NSMutableArray alloc] init];
    
    if (data.rates != nil) {
        for (int i = 0; i < data.rates.count; i++) {
            double rt = [[data.ratesTimes objectAtIndex:i] doubleValue];
            double rv = [[data.rates objectAtIndex:i] doubleValue];
            [self.rates addObject:[[AOMarketRate alloc] initWithRateTime:[[NSDate alloc]initWithTimeIntervalSince1970:rt/1000] graphRate:rv]];
        }
    }
    
    if (self.rates.count > 0) {
        currentLevel = [self getLevel:[self.rates objectAtIndex:[self.rates count] - 1]];
    }
    
    timeFirstInvest = data.timeFirstInvestDate; //[data.timeFirstInvestDate dateByAddingTimeInterval:-3600*5];
    
    timeLastInvest = data.timeLastInvestDate;
    timeEstClosing = data.timeEstClosingDate;
    
    AOMarketRate* lastRate = [self.rates lastObject];
    
    if([timeEstClosing timeIntervalSinceDate:timeFirstInvest]>7200 ){
        
        
        if([timeEstClosing timeIntervalSinceDate:lastRate.rateTime]<= 7200){
        
            timeFirstInvest = [timeEstClosing dateByAddingTimeInterval:-7200];
        }
        else {
            
             timeFirstInvest = [lastRate.rateTime dateByAddingTimeInterval:-7200];
            
        }
        
    }
    
    [self calcSecPerPixel];
    scheduled = data.scheduled;
    decimalPoint = data.decimalPoint;
    
    
    ratesLoaded = YES;
    hasPreviousHour = data.hasPreviousHour;
    // NSLog(@"s2etChart data  Rates Count: %lu",(unsigned long)[self.rates count]);
    [self cleanUnneededRates];
    // NSLog(@"s3etChart data  Rates Count: %lu",(unsigned long)[self.rates count]);
    [self updateCurrentLevelLabel];
    
    investments = [[NSMutableArray alloc] init];
    if (nil != data.investments) {

        //[investments addObjectsFromArray:[data.investments copy]];  //cannot add all since the server sends investments of all markets
        
        
        __typeof(self) __weak wSelf = self;
        
        [data.investments filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(AOInvestment* inv, NSDictionary *bindings) {
            
            if(wSelf!=nil){
                __typeof(wSelf) __strong sSelf = wSelf;
                
                
                if (sSelf.marketId == inv.marketId){
                    [sSelf.investments addObject:inv];
                }
                
                
            }
            
            return NO;
            
        }]];

    } else {
        // NSLog(@"Got no investments.");
    }
    investmentsLoaded = YES;
    
    [self initDisplay];
    if (self.chartViewMode!=ChartViewModeMini) [self initInvestments];
    [self initChartAreas];
    [self needsDisplayMethod];
    
    isReady = YES;
}

- (void) reset
{
    // NSLog(@"chart reset!");
    
    if ([chartAreas count] > 0) {
        [chartAreas removeAllObjects];
    }
    
    if(investments.count){
        
        AOInvestment *inv = nil;
        for (int i = 0; i < [investments count]; i++) {
            
            inv = [investments objectAtIndex:i];
            
            if([investmentButtonsDict objectForKey:[NSNumber numberWithFloat:inv.l_id] ]!=nil){
                [[investmentButtonsDict objectForKey:[NSNumber numberWithFloat:inv.l_id] ] removeFromSuperview];
                
                
                
            }
        }
    }
    
    investmentButtonsDict = [[NSMutableDictionary alloc] init];
    
    
    
    self.rates = [[NSMutableArray alloc] init];
    ratesLoaded = NO;
    
    call = NO;
    put = NO;
    yMinVal = -1;
    yMaxVal = -1;
    investmentsLoaded = NO;
    hasPreviousHour = NO;
    previousHourLoaded = NO;
    oppState = AOOpportunityStateOpened;
    currentLevel = 0;
    currentLevelTime = nil;
    [self setChartConstrains];
    if(CGRectIsEmpty(initialFrame)) initialFrame = self.frame;
    [self needsDisplayMethod];
    [self animateGlow];
    
}

- (void) calcSecPerPixel
{
    secPerPixel = [self getLengthInSec] / (self.frame.size.width - chartOffsetLeft - chartOffsetRight);
}

- (int) calcPeriodInSecFrom:(NSDate*)from to:(NSDate*)to
{
    return [to timeIntervalSince1970] - [from timeIntervalSince1970];
}

- (int) getLengthInSec
{
    
    int returnLenghtSec = oppTypeId == AOOpportunityTypeRegular ?
                            (timeEstClosing!=nil && timeFirstInvest!=nil)? [timeEstClosing timeIntervalSinceDate:timeFirstInvest] : AOOpportunityLengthInSecRegular
                            : AOOpportunityLengthInSecOptionPlus;
    
    if (previousHourLoaded) {
        returnLenghtSec = returnLenghtSec + 3600;
    }
    return  returnLenghtSec;
}

- (double) getLevel:(AOMarketRate*)rate
{
    return scheduled == AOOpportunityScheduledHourly ? rate.graphRate : rate.dayRate;
}

- (void) drawTimeLabels
{
    
    NSDate *d = [displayStartTime copy];
    
    float labelWidth=25;
    
    int allLabelsCount = 2*(floorf(self.zoomCoef)-1)+5;
    int spacesCount = allLabelsCount-1;
    float spaceWidth = (initialFrame.size.width*self.zoomCoef - allLabelsCount*labelWidth)/spacesCount;
    if(spaceWidth >= 30){
        
        UIFont *font = [UIFont fontWithName:CHART_TIMELINE_LABEL_FONT_NAME size:CHART_TIMELINE_LABEL_FONT_SIZE];
        
        for (int i=0; i < allLabelsCount; i++) {
            float xPosCorrection = 0;
            
            NSString *text = [dateFormatter stringFromDate:d];
            
            CGRect labelRect = CGRectMake(i * (labelWidth+spaceWidth) + xPosCorrection, timeLine.frame.origin.y-12, labelWidth, 10.0);
            
            NSMutableParagraphStyle *par = [[NSMutableParagraphStyle alloc] init];
            par.alignment = NSTextAlignmentCenter;
            if (i==allLabelsCount-1)
            {
                par.alignment = NSTextAlignmentRight;
            }
            if (i==0)
            {
                par.alignment = NSTextAlignmentLeft;
            }
            
            [text drawInRect:labelRect withAttributes:@{NSFontAttributeName : font,
                                                        NSForegroundColorAttributeName : CHART_TIMELINE_LABEL_FONT_COLOR,
                                                        NSParagraphStyleAttributeName : par}];
            
            d = [d dateByAddingTimeInterval:[self getLengthInSec] / spacesCount];
        }
    }
    
}

- (void) cleanUnneededRates
{
    if (!ratesLoaded || oppId == 0) {
        return;
    }
    
    
    NSDate *crrt = [[NSDate alloc] initWithTimeInterval:0 sinceDate:[AOUtils currentDatePrecise]];
    // Utils.fixTime(crrt);
    ratesStartTime = [timeEstClosing copy];
    do {
        ratesStartTime = [ratesStartTime dateByAddingTimeInterval:-[self getLengthInSec]]; // 1 opp period
    } while ([crrt compare:ratesStartTime] == NSOrderedAscending);
    if ([self.rates count] > 0 && [[[self.rates objectAtIndex:0] rateTime] compare:ratesStartTime] == NSOrderedAscending) {
        //        // NSLog(@"there is previous hour in the history");
        ratesStartTime = [ratesStartTime dateByAddingTimeInterval:-[self getLengthInSec]]; // 1 opp period
    }
    
    //    // NSLog(@"Cleaning old hour rates - length: %lu yMinVal: %f yMaxVal: %f startTime: %@", (unsigned long)[rates count], yMinVal, yMinVal, [dateFormatter stringFromDate:ratesStartTime]);
    while ([self.rates count] > 0 && [[[self.rates objectAtIndex:0] rateTime] compare:ratesStartTime] == NSOrderedAscending) {
        [self.rates removeObjectAtIndex:0];
    }
    
    AOMarketRate *r = nil;
    for (int i = 0; i < [self.rates count]; i++) {
        r = [self.rates objectAtIndex:i];
        if (i == 0) {
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *components = [calendar components:(NSSecondCalendarUnit) fromDate:[r rateTime]];
            firstSec = [components second];
            yMinVal = yMaxVal = [self getLevel:r];
        }
        
        [r setChartXPosition: chartOffsetLeft + (([self calcPeriodInSecFrom:ratesStartTime to:[r rateTime]] - firstSec) / secPerPixel - displayOffset)*self.zoomCoef];
        
        if ([self getLevel:r] < yMinVal) {
            yMinVal = [self getLevel:r];
        }
        if ([self getLevel:r] > yMaxVal) {
            yMaxVal = [self getLevel:r];
        }
    }
    
    if ([self.rates count] > 0) {
        float offset = (yMaxVal - yMinVal) * 0.02f;
        if (offset == 0) {
            offset = yMaxVal * 0.001f;
        }
        yMinVal = yMinVal - offset;
        yMaxVal = yMaxVal + offset;
    } else {
        yMinVal = -1;
        yMaxVal = -1;
    }
    [self updateYLabels];
    
    //    // NSLog(@"After cleaning - length: %lu yMinVal: %f yMaxVal: %f startTime: %@", (unsigned long)[rates count], yMinVal, yMinVal, [dateFormatter stringFromDate:ratesStartTime]);
}

- (void) initDisplay
{
    if (!ratesLoaded || oppId == 0) {
        return;
    }
    displayStartTime = [timeFirstInvest copy];
    
    NSDate *ct = [[NSDate alloc] initWithTimeInterval:0 sinceDate:[AOUtils currentDatePrecise]];
    
    int timePassed = [self calcPeriodInSecFrom:displayStartTime to:ct];
    int oppLength = [self calcPeriodInSecFrom:displayStartTime to:timeEstClosing];
    // NSLog(@"timePassed: %i oppLength: %i oppState: %i", timePassed, oppLength, oppState);
    
    //    if (timePassed > 3600 || oppLength < 3600 || oppState >= AOOpportunityStateLast10Min) {
    //
    //        // NSLog(@"Scroll...");
    //        displayStartTime = [timeEstClosing copy];
    //        do {
    //            displayStartTime = [displayStartTime dateByAddingTimeInterval:-[self getLengthInSec]];
    //        } while ([displayStartTime compare:ct] == NSOrderedDescending);
    //    } else {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSMinuteCalendarUnit) fromDate:displayStartTime];
    if ([components minute] %10 != 0) {
        displayStartTime = [displayStartTime dateByAddingTimeInterval:[components minute] % 10];
    }
    //}
    
    displayOffset = ([self calcPeriodInSecFrom:ratesStartTime to:displayStartTime] - firstSec) / secPerPixel;
    
    // NSLog(@"firstSec:%ld ratesStartTime: %@ displayStartTime: %@ displayOffset: %f", (long)firstSec, [dateFormatter stringFromDate:ratesStartTime], [dateFormatter stringFromDate:displayStartTime], displayOffset);
}

- (void) initInvestments
{
    if (!investmentsLoaded) {
        return;
    }
    AOInvestment *inv = nil;
    for (int i = 0; i < [investments count]; i++) {
        inv = [investments objectAtIndex:i];
        
        if(inv.currentLevel < yMinVal) yMinVal = inv.currentLevel;
        if(inv.currentLevel > yMaxVal) yMaxVal = inv.currentLevel;
        
        [inv setChartXPosition:self.zoomCoef * ([self calcPeriodInSecFrom:ratesStartTime to:[inv timeCreated]] - firstSec) / secPerPixel];
        
        [inv setPointerX:(self.center.x - self.frame.size.width/2)+inv.chartXPosition ];
    }
}

- (void) initChartAreas
{
    if ([chartAreas count] > 0) {
        [chartAreas removeAllObjects];
    }
    
    if ([investments count] > 0 && self.chartViewMode!=ChartViewModeMini) {
        NSMutableArray *invLvls = [[NSMutableArray alloc] initWithCapacity:[investments count]];
        for (AOInvestment *inv in investments) {
            [invLvls addObject:[NSNumber numberWithDouble:inv.currentLevel]];
        }
        [invLvls sortUsingSelector:@selector(compare:)];
        
        if (yMinVal == [[invLvls objectAtIndex:0] doubleValue]) {
            yMinVal = yMinVal * 0.999f;
        }
        if (yMaxVal == [[invLvls objectAtIndex:[invLvls count] - 1] doubleValue]) {
            yMaxVal = yMaxVal * 1.001f;
        }
        
        double btm = yMinVal;
        double top = 0;
        for (int i = 0; i < [invLvls count]; i++) {
            top = [[invLvls objectAtIndex:i] floatValue];
            int type = [AOChartArea isProfitableChartAreaWithBottomLevel:btm topLevel:top investments:investments] ? AOChartAreaProfitable : AOChartAreaNotProfitable;
            AOChartArea *ca = [[AOChartArea alloc] initWithBottomLevel:btm topLevel:top type:type last:NO];
            [chartAreas addObject:ca];
            btm = top;
        }
        int type = [AOChartArea isProfitableChartAreaWithBottomLevel:btm topLevel:yMaxVal investments:investments] ? AOChartAreaProfitable : AOChartAreaNotProfitable;
        AOChartArea *ca = [[AOChartArea alloc] initWithBottomLevel:btm topLevel:yMaxVal type:type last:YES];
        [chartAreas addObject:ca];
    }
    // NSLog(@"initChartAreas chartAreas: %@", chartAreas);
}

- (UIColor*) getColor:(int)colorId
{
    UIColor *clr = nil;
    switch (colorId) {
        case kColorIdCurrentLevelRed:
            clr = kColorCurrentLevelVoid;
            break;
        case kColorIdCurrentLevelVoid:
            clr = kColorCurrentLevelVoid;
            break;
        case kColorIdCurrentLevelGreen:
            clr = kColorCurrentLevelVoid;
            break;
        case kColorIdBackground:
            clr = kColorBackground;
            break;
        case kColorIdChart:
            clr = kColorChart;
            break;
        case kColorIdCrrLvlLines:
            clr = kColorCrrLvlLines;
            break;
        case kColorIdCrrLvlSquare:
            clr = kColorCrrLvlSquare;
            break;
        case kColorIdCrrLvlCircle:
            clr = kColorCrrLvlCircle;
            break;
        default:
            clr = [UIColor clearColor];
            break;
    }
    if (oppState != AOOpportunityStateOpened && oppState != AOOpportunityStateLast10Min) {
        return [clr colorWithAlphaComponent:1];
    }
    
    
    return clr;
}

- (CGFloat) getAlpha
{
    switch (oppState) {
        case AOOpportunityStateOpened:
        case AOOpportunityStateLast10Min:
            return kColorAlphaOpened;
        case AOOpportunityStateClosing1Min:
        case AOOpportunityStateClosing:
            return kColorAlphaWFE;
        default:
            return kColorAlphaClosed;
    }
}

- (void) updateState
{
    CGFloat alpha = [self getAlpha];
    levelMin.textColor = [levelMin.textColor colorWithAlphaComponent:alpha];
    levelMax.textColor = levelMin.textColor;
}

- (void) updateYLabels
{
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    [nf setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [nf setMinimumFractionDigits:decimalPoint];
    [nf setMaximumFractionDigits:decimalPoint];
    [nf setUsesGroupingSeparator:YES];
    [nf setGroupingSize:3];
    [nf setGroupingSeparator:@","];
    nf.minimumIntegerDigits = 1;
    [levelMin setText:[nf stringFromNumber:[NSNumber numberWithFloat:yMinVal]]];
    [levelMax setText:[nf stringFromNumber:[NSNumber numberWithFloat:yMaxVal]]];
    
    if (yMinVal==-1 || yMaxVal==-1) {
        levelMin.hidden=YES;
        levelMax.hidden=YES;
        
    }
    else {
        levelMin.hidden=NO;
        levelMax.hidden=NO;
        
    }
    
    
}

- (void) updateCurrentLevelLabel
{
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    [nf setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [nf setMinimumFractionDigits:decimalPoint];
    [nf setMaximumFractionDigits:decimalPoint];
    [nf setUsesGroupingSeparator:YES];
    [nf setGroupingSize:3];
    [nf setGroupingSeparator:@","];
    [self.currentLevelLabel setText:[nf stringFromNumber:[NSNumber numberWithFloat:currentLevel]]];
    if(currentLevel>0){
        self.currentLevelLabel.hidden=NO;
        if (self.delegate != nil) {
            [self.delegate currentLevelChanged:[nf stringFromNumber:[NSNumber numberWithFloat:currentLevel]]];
        }
    }
    else {
        self.currentLevelLabel.hidden=YES;
    }
    
}


/* AO jumping times */
- (void) lsUpdateWithItemPosition:(int)itemPosition itemName:(NSString *)itemName didUpdateWithInfo:(LSUpdateInfo *)updateInfo
{
    
    if (!ratesLoaded||isPreviousHour) {
        return;
    }
        
    NSString *crrLvl = [updateInfo currentValueOfFieldName:[NSString stringWithFormat:@"LEVEL_%d",RESIDENT.skinGroupId]];
    NSString *crrState = [updateInfo currentValueOfFieldName:@"ET_STATE"];
    NSString *crrTime = [updateInfo currentValueOfFieldName:@"AO_TS"];
    //// NSLog(@"level: %@ state: %@ time: %@", crrLvl, crrState, crrTime);
    
    if ([updateInfo isChangedValueOfFieldName:@"ET_STATE"]) {
        oppState = [crrState intValue];
        [self updateState];
    }
    
    if ([updateInfo isChangedValueOfFieldName:@"ET_ODDS_WIN"]) {
        winOdds = [[updateInfo currentValueOfFieldName:@"ET_ODDS_WIN"] floatValue];
    }
    if ([updateInfo isChangedValueOfFieldName:@"ET_ODDS_LOSE"]) {
        loseOdds = [[updateInfo currentValueOfFieldName:@"ET_ODDS_LOSE"] floatValue];
    }
    
    double tmpLevel = [[AOUtils parseLevelFromLSUpdate:crrLvl] doubleValue];
    
    if (tmpLevel == 0) {
        [self.currentLevelLabel setText:@""];//AOLocalizedString(@"please.wait")
        if (self.delegate != nil) {
            [self.delegate currentLevelChanged:@""];//AOLocalizedString(@"please.wait")
        }
    }
    if (tmpLevel == 0 || oppState < AOOpportunityStateOpened || oppState > AOOpportunityStateClosed) {
        [self needsDisplayMethod];
        return;
    }
    
    currentLevel = tmpLevel;
    currentLevelTime = [[NSDate alloc] initWithTimeIntervalSince1970:[crrTime doubleValue] / 1000];
    
    if (yMinVal == -1 || currentLevel < yMinVal) {
        yMinVal = currentLevel;
        [self updateYLabels];
    }
    if (yMaxVal == -1 || currentLevel > yMaxVal) {
        yMaxVal = currentLevel;
        [self updateYLabels];
    }
    [self.currentLevelLabel setText:crrLvl];
    if (self.delegate != nil) {
        [self.delegate currentLevelChanged:crrLvl];
    }
    
    /*NSString *clrKey = [NSString stringWithFormat:@"CLR_%d",RESIDENT.skinGroupId];
     if ([updateInfo isChangedValueOfFieldName:clrKey]) {
     //int crrClrId = [[updateInfo currentValueOfFieldName:clrKey] intValue];
     
     //[self.currentLevelLabel setTextColor:[self getColor:crrClrId]];
     
     
     }*/
    
    [self updateCurrentReturn];
    [self ensureYOffset];
    [self takeRateSample];
    [self cleanUnneededRates];
    [self needsDisplayMethod];
}

- (void) updateCurrentReturn
{
    totalProfit = 0;
    totalAmount = 0;
    if (nil != investments) {
        AOInvestment *inv = nil;
        for (int i = 0; i < [investments count]; i++) {
            inv = [investments objectAtIndex:i];
            if ([inv marketId] == marketId) {
                totalProfit += [inv returnAtLevel:currentLevel];
                totalAmount += [inv amount];
            }
        }
    }
}

- (void) ensureYOffset
{
    // don't let the current level line go too close to the top/bottom of the chart
    float tooClose = (yMaxVal - yMinVal) * 0.07f;
    if (tooClose == 0) {
        tooClose = yMinVal * 0.0001f;
    }
    if (currentLevel < yMinVal + tooClose) {
        float newMinVal = MIN(yMinVal, currentLevel);
        yMinVal = newMinVal - (yMaxVal - newMinVal) *0.07f;
        if (yMinVal == currentLevel) {
            yMinVal = yMinVal - tooClose;
        }
        if ([chartAreas count] > 0) {
            [[chartAreas objectAtIndex:0] setBottomLevel:yMinVal];
        }
    }
    if (currentLevel > yMaxVal - tooClose) {
        float newMaxVal = MAX(yMaxVal, currentLevel);
        yMaxVal = newMaxVal + (newMaxVal - yMinVal) * 0.07f;
        if (yMaxVal == currentLevel) {
            yMaxVal = yMaxVal + tooClose;
        }
        if ([chartAreas count] > 0) {
            [[chartAreas objectAtIndex:[chartAreas count] - 1] setTopLevel:yMaxVal];
        }
    }
}

- (void) takeRateSample
{
    if (self.rates.count == 0) {
        return;
    }
    
    NSInteger secFromStart = [self calcPeriodInSecFrom:ratesStartTime to:currentLevelTime] - firstSec;
    AOMarketRate* newRate = [[AOMarketRate alloc] initWithRateTime:currentLevelTime graphRate:currentLevel dayRate:currentLevel chartXPosition:secFromStart / secPerPixel];
    
    long latestNonSlingRateIndex = self.rates.count - 1;
    AOMarketRate* lastRate = [self.rates objectAtIndex:self.rates.count-1];
    
    if (lastRate.sling && latestNonSlingRateIndex>0) {
        latestNonSlingRateIndex--;
    }
    
    if ([currentLevelTime timeIntervalSinceDate:[[self.rates objectAtIndex:latestNonSlingRateIndex] rateTime]] > secPerPixel) {
        newRate.sling=NO;
    }
    else {
        newRate.sling=YES;
        [self.rates removeLastObject];
    }
    
    [self.rates addObject:newRate];
}

- (void) addInvestment:(AOInvestment*)i
{
    [investments addObject:i];
    [self initInvestments];
    [self initChartAreas];
    [self updateCurrentReturn];
    [self needsDisplayMethod];
}

-(BOOL)canScrollBack {
    return (self.chartViewMode!=ChartViewModeMini && hasPreviousHour &&
            ([displayStartTime compare:timeFirstInvest] == NSOrderedDescending ||
             [displayStartTime compare:timeFirstInvest] == NSOrderedSame)
            );
    //return (hasPreviousHour && displayStartTime.getTime() >= timeFirstInvest.getTime());
}

-(BOOL)canScrollForward {
    return (hasPreviousHour &&
            ([displayStartTime compare:timeFirstInvest] == NSOrderedAscending)
            );
    //return (hasPreviousHour && displayStartTime.getTime() < timeFirstInvest.getTime());
    //        Date ct = new Date();
    //        fixTime(ct);
    //        if (ct.getTime() > timeEstClosing.getTime()) {
    //            ct.setTime(timeEstClosing.getTime());
    //        }
    //        // if the display start time is more than an hour old and the closing is not late
    //        // we must be showing the previous hour
    //        ct.setTime(ct.getTime() - getLengthInSec() * 1000); // 1 opp period
    //        if (displayStartTime.getTime() < ct.getTime()) {
    //            return true;
    //        }
    //        return false;
}

- (void) scrollBack {
    
    if(![self canScrollBack]) return;
    NSDate* displayStartTimeBackup = displayStartTime;
    displayStartTime = [displayStartTime dateByAddingTimeInterval:-[self getLengthInSec]];
    //displayOffset = ([self calcPeriodInSecFrom:ratesStartTime to:displayStartTime] - firstSec) / secPerPixel;
    
    
    if(!previousHourLoaded) {
        
        
        AOChartDataMethodRequest *request = [[AOChartDataMethodRequest alloc] init];
        request.serviceMethod = @"getChartDataPreviousHour";
        request.marketId = marketId;
        request.opportunityId = self.oppId;
        request.encrypt = [NSNumber numberWithBool:YES];
        if (self.delegate != nil) {
            [self.delegate loadingChart:YES];
        }
        [AORequestService startServiceRequestWithMethodRequest:request success:^(NSDictionary *resultDictionary) {
            
            if ([resultDictionary objectForKey:@"rates"]==[NSNull null] || [resultDictionary objectForKey:@"ratesTimes"]==[NSNull null]) {
                if (self.delegate != nil) {
                    [self.delegate loadingChart:NO];
                }
                displayStartTime =  displayStartTimeBackup;
                [self needsDisplayMethod];
                return;
            }
            
            previousHourLoaded = YES;
            
            NSArray *ratesResult = [resultDictionary objectForKey:@"rates"];
            
            NSArray *ratesTimesResult = [resultDictionary objectForKey:@"ratesTimes"];
            
            for(NSString *rate in ratesResult){
                NSUInteger index = [ratesResult indexOfObject:rate];
                double rt = [[ratesTimesResult objectAtIndex:index] doubleValue];
                double rv = [rate doubleValue];
                [self.rates insertObject:[[AOMarketRate alloc] initWithRateTime:[[NSDate alloc] initWithTimeIntervalSince1970:rt/1000] graphRate:rv] atIndex:0];
            }
            
            //sorting the rates in ascendenting order
            self.rates = [[self.rates sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"rateTime" ascending:YES]]] mutableCopy];
            [self calcSecPerPixel];
            [self cleanUnneededRates];
            [self initInvestments];
            [self initChartAreas];
            //displayOffset = ([self calcPeriodInSecFrom:ratesStartTime to:displayStartTime] - firstSec) / secPerPixel;
            // NSLog(@"displayOffset: %f",displayOffset);
            //[self takeRateSample];
            [self needsDisplayMethod];
            if (self.delegate != nil) {
                [self.delegate loadingChart:NO];
            }
            // NSLog(@"getChartDataPreviousHour success %@",resultDictionary);
        } failed:^(NSDictionary *errDictionary) {
            previousHourLoaded = NO;
            // NSLog(@"getChartDataPreviousHour failed %@",errDictionary);
            if (self.delegate != nil) {
                [self.delegate loadingChart:NO];
            }
        }];
    }
}

-(IBAction)scrollForward:(id)sender {
    if(![self canScrollForward]) return;
    displayStartTime = [displayStartTime dateByAddingTimeInterval:[self getLengthInSec]]; // 1 opp period
    displayOffset = ([self calcPeriodInSecFrom:ratesStartTime to:displayStartTime] - firstSec) / secPerPixel;
    [self needsDisplayMethod];
}

-(void)needsDisplayMethod{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsDisplay];
        [self setNeedsLayout];
    });
}

-(void)layoutSubviews {
    // it's important to add new views in the hierarchy before the [super layoutSubviews] is called so that it can set proper auto layout constraints
    if(investments.count && self.chartViewMode!=ChartViewModeMini){
        
        AOInvestment *inv = nil;
        for (int i = 0; i < [investments count]; i++) {
            
            inv = [investments objectAtIndex:i];
            
            if([investmentButtonsDict objectForKey:[NSNumber numberWithFloat:inv.l_id] ]==nil){
                
                AOChartInvestmentButton* b = [[AOChartInvestmentButton alloc] initWithInvestment:inv andCurrentLevel:currentLevel];
                UIImage*  selectedBackgroundImage =[UIImage imageNamed:@"investBtnGlow.png"];
                [b setBackgroundImage:selectedBackgroundImage forState:(UIControlStateHighlighted | UIControlStateSelected)];
                [investmentButtonsDict setObject:b forKey:[NSNumber numberWithFloat:inv.l_id]  ];
                [b addTarget:self action:@selector(investmentButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:b];
                
                UIView *invLine = [[UIView alloc] initWithFrame:CGRectMake(0.0, [self investmentChartYPosition:inv]-2, self.frame.size.width * self.zoomCoef, 2)];
                invLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"dash"]];
                [investmentLinesDict setObject:invLine forKey:[NSNumber numberWithFloat:inv.l_id]  ];
                
                [self addSubview:invLine];
            }
            
        }
    }
    
    [super layoutSubviews];
    
    if (oppState==1 || oppState==8 || oppState==9) {
        stripsImageView.hidden=YES;
        timeLine.hidden=YES;
        self.currentLevelLabel.hidden=YES;
        cursorLine.hidden=YES;
        glowerImageView.hidden=YES;
        return;
    }
    
    
    timeLine.frame  = CGRectMake(timeLine.frame.origin.x, self.frame.size.height-4.5/*colorLineHeight*/-timeLine.frame.size.height, self.frame.size.width , timeLine.frame.size.height);
    glowerImageView.center = CGPointMake(glowerImageView.center.x, self.currentLevelY+2);
    cursorLine.frame = CGRectMake(0.0, self.currentLevelY+1.5, self.frame.size.width*self.zoomCoef, 1);
    levelMin.frame = CGRectMake(10-(self.center.x - self.frame.size.width/2), levelMin.frame.origin.y, levelMin.frame.size.width, levelMin.frame.size.height);
    levelMax.frame = CGRectMake(10-(self.center.x - self.frame.size.width/2), levelMax.frame.origin.y, levelMax.frame.size.width, levelMax.frame.size.height);
    
    
    
    
    if(investments.count && self.chartViewMode!=ChartViewModeMini){
        
        AOInvestment *inv = nil;
        for (int i = 0; i < [investments count]; i++) {
            
            inv = [investments objectAtIndex:i];
            
            [[investmentButtonsDict objectForKey:[NSNumber numberWithFloat:inv.l_id] ] setCenter:CGPointMake(inv.chartXPosition,[self investmentChartYPosition:inv] )];
            [(AOChartInvestmentButton*)[investmentButtonsDict objectForKey:[NSNumber numberWithFloat:inv.l_id] ] setCurrentLevel:currentLevel];
            [[investmentLinesDict objectForKey:[NSNumber numberWithFloat:inv.l_id] ] setFrame:CGRectMake(0.0, [self investmentChartYPosition:inv]-2, self.frame.size.width * self.zoomCoef, 2)];
        }
    }
    
    
    if(self.frame.origin.x  > 0)
        self.frame = CGRectMake(0,self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    
    if(self.frame.origin.x  + self.frame.size.width < initialFrame.size.width){
        self.frame = CGRectMake(initialFrame.size.width - self.frame.size.width,self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    }
    
    if (self.delegate != nil) {
        [self.delegate cursorChangedLocation:CGPointMake((self.center.x - self.frame.size.width/2) + self.currentLevelX, self.currentLevelY)];
    }
    
    [self p_positionLabels];
    self.backgroundColor= [UIColor clearColor];
}

-(void)p_positionLabels {
    
    levelMin.frame = CGRectMake(10-(self.center.x - self.frame.size.width/2), levelMin.frame.origin.y, levelMin.frame.size.width, levelMin.frame.size.height);
    levelMax.frame = CGRectMake(10-(self.center.x - self.frame.size.width/2), levelMax.frame.origin.y, levelMax.frame.size.width, levelMax.frame.size.height);
    
    if (self.currentLevelLabel.text!=nil && [self.currentLevelLabel.text floatValue]>0) {
        
        self.currentLevelLabel.hidden=NO;
        self.currentLevelLabel.center = CGPointMake(self.currentLevelX, self.currentLevelY-10);
        if(self.currentLevelLabel.center.y - self.currentLevelLabel.frame.size.height/2 < ChartViewModeMini ? 25 : 0 ) {
            self.currentLevelLabel.center = CGPointMake(self.currentLevelX, self.currentLevelLabel.frame.size.height/2 + ChartViewModeMini ? 25 : 0);
        }
        
        if(self.center.x - self.frame.size.width/2+self.currentLevelLabel.frame.origin.x < 10){
            self.currentLevelLabel.frame  = CGRectMake( levelMin.frame.origin.x, self.currentLevelY-10, self.currentLevelLabel.frame.size.width, self.currentLevelLabel.frame.size.height);
        }
        
        if (self.delegate != nil) {
            [self.delegate cursorChangedLocation:CGPointMake((self.center.x - self.frame.size.width/2) + self.currentLevelX, self.currentLevelY)];
        }
    }
    else{
        self.currentLevelLabel.hidden=YES;
    }
}

-(void)unselectInvestmentButtons{
    isInInvestmentMode=NO;
    for(NSNumber* b in investmentButtonsDict)
        [(AOChartInvestmentButton*)[investmentButtonsDict objectForKey:b] setSelected:NO];
}

- (void) selectInvestmentButtonWithInvestment:(AOInvestment*)investmentToSelect {  //investment in playarea is tapped => the button in chart should be selected
    
    for( NSNumber *aKey in [investmentButtonsDict allKeys] )
    {
        AOChartInvestmentButton* aInvestmentButton = [investmentButtonsDict objectForKey:aKey];
        if (aInvestmentButton.investment.l_id == investmentToSelect.l_id) {
            [self investmentButtonPressed:aInvestmentButton];
        }
    }
}

- (void) investmentButtonPressed: (AOChartInvestmentButton*)b {
    [self unselectInvestmentButtons];
    [b setSelected:YES];
    isInInvestmentMode = YES;
    for (int i=0 ; i < investments.count; i++) {
        if ((AOInvestment*)investments[i] == b.investment) {
            showingInvestmentIndex = i;
            break;
        }
    }
    
    CGPoint screenPoint = [[[UIApplication sharedApplication].delegate window] convertPoint:b.center fromView:b.superview];
    if (self.delegate != nil) {
        [self.delegate didTouchInvestment:b.investment screenPoint:screenPoint];
    }
}

- (void) addNewInvestment: (AOInvestment*)inv {
    if (inv!=nil) {
        inv.newlyAdded=YES;
        [investments addObject:inv];
        [inv setChartXPosition:self.zoomCoef * ([self calcPeriodInSecFrom:ratesStartTime to:[inv timeCreated]] - firstSec) / secPerPixel];
        [inv setPointerX:(self.center.x - self.frame.size.width/2)+inv.chartXPosition];
        [self needsDisplayMethod];
        inv.newlyAdded=NO;
    }
}

- (float) investmentChartYPosition:(AOInvestment*)inv{
    float retY =  chartOffsetTop + chartActiveAreaHeight * (1 - (inv.currentLevel - yMinVal) / (yMaxVal-yMinVal)); //chartOffsetTop+ (chartActiveAreaHeight*(inv.currentLevel - yMinVal))/(yMaxVal-yMinVal);
    return retY;
}

@end