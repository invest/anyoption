//
//  AOBaseChartView.h
//  JSON Test
//
//  Created by Anton Antonov on 2/23/12.
//  Copyright (c) 2012 Anyoption. All rights reserved.
//

#import "AOChartView.h"
#import "AOOpportunity.h"


@interface AOBaseChartView : UIView 

@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *returnPercentLabel;
@property (strong, nonatomic) IBOutlet UILabel *returnPercent;
@property (strong, nonatomic) IBOutlet UILabel *expTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *expTime;
@property (strong, nonatomic) IBOutlet AOChartView *chart;
@property (strong, nonatomic) IBOutlet UILabel *totalInvestmentsLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalInvestments;
@property float totalInvestmentsAmount;
@property long marketId;
@property (strong, nonatomic) IBOutlet UILabel *currentReturnLabel;
@property (strong, nonatomic) IBOutlet UILabel *currentReturn;
@property float currentReturnAmount;
@property (strong, nonatomic) IBOutlet UILabel *waitingForExpiry;
@property (strong, nonatomic) IBOutlet UILabel *closed;
@property (assign,nonatomic) ChartViewMode chartViewMode;

@property (nonatomic, weak) IBOutlet UIButton* zoomInButton;
@property (nonatomic, weak) IBOutlet UIButton* zoomOutButton;


@property (assign,nonatomic) AOOpportunityType optionType;

- (void) setChartData:(AOChartData*)data;
- (void) lsUpdateWithItemPosition:(int)itemPosition itemName:(NSString *)itemName didUpdateWithInfo:(LSUpdateInfo *)updateInfo;
- (void) updateTotalFields;
- (void) updateStateWithInfo:(LSUpdateInfo *)updateInfo;
- (void) showCurrentReturn;
- (void) hideCurrentReturn;
- (void) setInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
- (void) addInvestment:(AOInvestment*)i;
- (void) needsDisplayMethod;

@end