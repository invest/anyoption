//
//  AOCenteredTextImage.m
//  iOSApp
//
//  Created by mac_user on 1/15/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "AOCenteredTextImage.h"

@implementation AOCenteredTextImage

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib{
    refConstraintInitialValue = _refConstraint.constant;
}

-(void)updatePosition{
    [self resetImage];
    _refConstraint.constant = refConstraintInitialValue-_refLabel.attributedText.size.width/2;
//    self.hidden = NO;
}
-(void)resetImage{
//    self.hidden = YES;
    _refConstraint.constant = refConstraintInitialValue;
}

@end
