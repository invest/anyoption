//
//  AOBaseChartView.m
//  JSON Test
//
//  Created by Anton Antonov on 2/23/12.
//  Copyright (c) 2012 Anyoption. All rights reserved.
//

#import "AOBaseChartView.h"
#import "LightstreamerClient.h"
#import "AOConstants.h"

@interface AOBaseChartView()


@end

@implementation AOBaseChartView

@synthesize name;
@synthesize returnPercentLabel;
@synthesize returnPercent;
@synthesize expTimeLabel;
@synthesize expTime;
@synthesize chart;
@synthesize totalInvestmentsLabel;
@synthesize totalInvestments;
@synthesize totalInvestmentsAmount;

@synthesize currentReturnLabel;
@synthesize currentReturn;
@synthesize currentReturnAmount;
@synthesize waitingForExpiry;
@synthesize closed;

-(id)init{
  self =  [super init];
    if (self)
        self.chartViewMode = ChartViewModePortrait;
        self.chart.levelMax.hidden=YES;
        self.chart.levelMin.hidden=YES;
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        self.chart = [AOChartView new];
        [self addSubview:self.chart];
    }
    
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    if (IS_BELOW_IPHONE5) {
          self.zoomInButton.frame  = CGRectOffset(self.zoomInButton.frame, 0.f, -20);
          self.zoomOutButton.frame = CGRectOffset(self.zoomOutButton.frame, 0.f, -40);
    }
  
}

- (void)setChartData:(AOChartData*)data
{
    self.closed.hidden = YES;
    [self.chart reset];
    self.marketId = data.marketId;
    [self.name setText:[data marketName]];
    self.chart.frame = CGRectMake(self.chart.frame.origin.x, self.chart.frame.origin.y, self.frame.size.width, self.frame.size.height);
    [self.chart setChartData:data];
        
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [df setDateFormat:@"HH:mm"];
    [self.expTime setText:[df stringFromDate:self.chart.timeEstClosing]];
    
    [chart.investments count] > 0 ? [self showCurrentReturn] : [self hideCurrentReturn];
    
}

- (void) lsUpdateWithItemPosition:(int)itemPosition itemName:(NSString *)itemName didUpdateWithInfo:(LSUpdateInfo *)updateInfo
{
    if (self.chart.oppId != 0 && [[updateInfo currentValueOfFieldName:@"ET_OPP_ID"] intValue] != self.chart.oppId) {
        // NSLog(@"chart.oppId: %ld update oppId: %@", self.chart.oppId, [updateInfo currentValueOfFieldName:@"ET_OPP_ID"]);
        return;
    }
    if ([updateInfo isChangedValueOfFieldName:@"ET_ODDS"]) {
        [self.returnPercent setText:[updateInfo currentValueOfFieldName:@"ET_ODDS"]];
    }
    
    [self.chart lsUpdateWithItemPosition:itemPosition itemName:itemName didUpdateWithInfo:updateInfo];
    [self updateTotalFields];
    
    if ([updateInfo isChangedValueOfFieldName:@"ET_STATE"]) {
        [self updateStateWithInfo:updateInfo];
    }
    
}

- (void) updateTotalFields
{
    float tmp = [chart totalProfit];
    if (currentReturnAmount != tmp) {
        currentReturnAmount = tmp;
        currentReturn.text = [AOUtils formatCurrencyAmount:currentReturnAmount];
    }
    tmp = [chart totalAmount];
    if (totalInvestmentsAmount != tmp) {
        totalInvestmentsAmount = tmp;
        totalInvestments.text = [AOUtils formatCurrencyAmount:totalInvestmentsAmount];
    }
}

-(void)hideEverything{
    self.chart.levelMax.hidden=YES;
    self.chart.levelMin.hidden=YES;
    self.zoomOutButton.hidden=YES;
    self.zoomInButton.hidden=YES;
    self.closed.hidden=YES;
    
    if (IS_IPAD) {
        if([self viewWithTag:70]!=nil)
            [[self viewWithTag:70] setHidden:YES];
        
        if([self viewWithTag:71]!=nil)
            [[self viewWithTag:71] setHidden:YES];
    }
  
}

- (void) updateStateWithInfo:(LSUpdateInfo *)updateInfo
{
    // NSLog(@"AOBaseChartView.updateStateWithInfo - state: %i", self.chart.oppState);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        
        if (self.chart.oppState == AOOpportunityStateOpened || self.chart.oppState == AOOpportunityStateLast10Min) {
            
        } else {
           
        }
        
        if (self.chart.oppState == AOOpportunityStateClosing1Min || self.chart.oppState == AOOpportunityStateClosing) {

            
        } else {
            self.waitingForExpiry.hidden = YES;
        }
        
        if (self.chart.oppState == AOOpportunityStateClosed) {
            self.chart.currentLevelLabel.hidden = YES;
        } else {
            self.chart.currentLevelLabel.hidden = NO;
            
        }
        
        if (self.chart.oppState < AOOpportunityStateOpened || self.chart.oppState > AOOpportunityStateClosing) {
            
            
            if (self.chart.oppState == AOOpportunityStateClosed) {
               
                self.closed.text = [NSString stringWithFormat:@"%@\n%@",AOLocalizedString(@"assetIndex.closingLevel"), self.chart.currentLevelLabel.text];
                
            } else if (self.chart.oppState == AOOpportunityStateSuspended) {
                
                
                self.closed.text = [NSString stringWithFormat:@"%@\n%@",AOLocalizedString(@"assetIndex.closingLevel"), self.chart.currentLevelLabel.text];
            } else {
                self.closed.text = AOLocalizedString(@"profitLineOffhours");
                
            }
        }
        
        if(self.chart.oppState==1 || self.chart.oppState==8 || self.chart.oppState==9){
            [self hideEverything];
        }
    
        
    });
    [self needsDisplayMethod];
}

- (void)showCurrentReturn
{
    totalInvestmentsLabel.hidden = NO;
    totalInvestments.hidden = NO;
    currentReturnLabel.hidden = NO;
    currentReturn.hidden = NO;
}

- (void)hideCurrentReturn
{
    totalInvestmentsLabel.hidden = YES;
    totalInvestments.hidden = YES;
    currentReturnLabel.hidden = YES;
    currentReturn.hidden = YES;
}

- (void) setInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (interfaceOrientation == UIInterfaceOrientationPortrait) {
        chart.portrait = YES;
    } else {
        chart.portrait = NO;
    }
}

- (void) addInvestment:(AOInvestment*)i
{
    [chart addInvestment:i];
    [self updateTotalFields];
    if ([chart.investments count] == 1) {
        [self showCurrentReturn];
    }
}

-(void)setOptionType:(AOOpportunityType)optType{
    _optionType = optType;
    self.chart.optionType = optType;
}

-(void)needsDisplayMethod{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsDisplay];
    });
}

-(void)setChartViewMode:(ChartViewMode)cvMode{
    
    if(self.chart!=nil) self.chart.chartViewMode = cvMode;
    _chartViewMode = cvMode;
}

@end