
//
//  AORoundCornerView.m
//  iOSApp
//
//  Created by mac_user on 12/2/13.
//  Copyright (c) 2013 Anyoption. All rights reserved.
//

#import "AORoundCornerView.h"

@implementation AORoundCornerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self setup];
    return self;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void) setup
{
    self.backgroundColor = [UIColor clearColor];
    self.opaque = NO;
}

- (void) drawRect:(CGRect)rect
{
    [self.strokeColor setStroke];
    [self.fillColor setFill];

    if (self.shadowColor != nil)
    {
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        
        CGContextSetShadowWithColor(ctx, CGSizeZero, 3.0, self.shadowColor.CGColor);
    }
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(self.bounds, _lineWidth, _lineWidth) cornerRadius:_radius];
    path.lineWidth = _lineWidth;
    [path fill];
    [path stroke];

//    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(self.bounds,_lineWidth,_lineWidth) byRoundingCorners:_corners cornerRadii:CGSizeMake(_radius, _radius)];
//    maskPath.lineWidth = _lineWidth;
//    
//    [maskPath fill];
//    [maskPath stroke];
}

GENERATE_SETTER(setFillColor, UIColor *, fillColor, setNeedsDisplay);
GENERATE_SETTER(setStrokeColor, UIColor *, strokeColor, setNeedsDisplay);
GENERATE_SETTER(setRadius, CGFloat, radius, setNeedsDisplay);
GENERATE_SETTER(setLineWidth, CGFloat, lineWidth, setNeedsDisplay);
GENERATE_SETTER(setCorners, UIRectCorner, corners, setNeedsDisplay);
GENERATE_SETTER(setShadowColor, UIColor *, shadowColor, setNeedsDisplay);

@end
