//
//  AORoundCornerView.h
//  iOSApp
//
//  Created by mac_user on 12/2/13.
//  Copyright (c) 2013 Anyoption. All rights reserved.
//

@interface AORoundCornerView : UIView

@property (nonatomic,strong) UIColor *fillColor;
@property (nonatomic,strong) UIColor *strokeColor;
@property (nonatomic, strong) UIColor *shadowColor;
@property (nonatomic, assign) CGFloat radius;
@property (nonatomic, assign) CGFloat lineWidth;
@property (nonatomic, assign) UIRectCorner corners;

@end
