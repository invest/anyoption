//
//  AOChartView.h
//  JSON Test
//
//  Created by Anton Antonov on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AOChartData.h"
#import "AOChartArea.h"
#import "AOOpportunity.h"
#import "AOInvestment.h"
#import "AOChartInvestmentButton.h"

typedef enum {
    ChartViewModePortrait,
    ChartViewModeLandscape,
    ChartViewModeMini
}ChartViewMode;


@protocol AOChartViewDelegate <NSObject>

@optional
- (void) didTouchInvestment:(AOInvestment*)inv screenPoint:(CGPoint)point;
- (void) cursorChangedLocation:(CGPoint)cursorLocation;
- (void) currentLevelChanged:(NSString*)currentLevelText;
- (void) beganDraggingChartView;
- (void) endedDraggingChartView;
- (void) loadingChart:(BOOL)isloading;
@end

//#define kChartLeftBorderTag 101
//#define kChartBottomBorderTag 102
//#define kChartLastTimeBorderTag 103

#define kColorIdCurrentLevelRed 0
#define kColorIdCurrentLevelVoid 1
#define kColorIdCurrentLevelGreen 2
#define kColorIdBackground 3
#define kColorIdChart 4
#define kColorIdCrrLvlLines 5
#define kColorIdCrrLvlSquare 6
#define kColorIdCrrLvlCircle 7

#define kColorCurrentLevelRed ([UIColor colorWithRed:0.875 green:0 blue:0 alpha:1.0]) /*#df0000*/
#define kColorCurrentLevelVoid ([UIColor colorWithRed:1 green:1 blue:1 alpha:1.0])  /*#ffffff*/
#define kColorCurrentLevelGreen ([UIColor colorWithRed:0.082 green:0.627 blue:0.122 alpha:1.0]) /*#15a01f*/
#define kColorBackground ([UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.0]) /*#ffffff*/
//#define kColorChart ([UIColor colorWithRed:0.008 green:0.518 blue:0.839 alpha:1.0]) /*#0284d6*/
#define kColorChart ([UIColor colorWithWhite:1.0 alpha:0.6])

#define kColorCrrLvlLines ([UIColor colorWithWhite:1.0 alpha:1])

#define kColorCrrLvlSquare ([UIColor colorWithRed:0.035 green:0.216 blue:0.333 alpha:1.0]) /*#093755*/
#define kColorCrrLvlCircle ([UIColor colorWithWhite:1 alpha:1.0])


#define kColorInvestmentLine ([UIColor colorWithRed:0.965 green:0.804 blue:0.063 alpha:1]) /*#f6cd10*/

#define kColorAlphaOpened 1.0f
#define kColorAlphaWFE 0.698f
#define kColorAlphaClosed 0.4f

#define investmentXOffset  20    //Used when swiping to focus on an investment . Not to be next to the borders.

@interface AOChartView : UIView {
    AppDelegate *appDelegate;
    UIImage *callSmall;
    UIImage *putSmall;
}

@property (strong,nonatomic) NSDateFormatter *dateFormatter;

@property float secPerPixel;
@property BOOL portrait;

@property long marketId;
@property long oppId;
@property long oppTypeId;
@property int oppState;

@property int timezoneOffset;
@property (strong, nonatomic) NSDate *timeEstClosing;
@property int scheduled;
@property int decimalPoint;
@property (strong, nonatomic) __block NSMutableArray *rates;
@property BOOL ratesLoaded;
@property BOOL hasPreviousHour;
@property BOOL previousHourLoaded;

@property (strong, nonatomic) NSDate *displayStartTime;
@property NSInteger firstSec;
@property (strong, nonatomic) NSDate *ratesStartTime;
@property float yMinVal;
@property float yMaxVal;
@property double currentLevel;
@property (strong, nonatomic) NSDate *currentLevelTime;
@property float displayOffset;
@property BOOL call;
@property BOOL put;
@property (strong, nonatomic) NSDate *timeFirstInvest;
@property (strong, nonatomic) NSDate *timeLastInvest;
@property (strong, nonatomic) NSMutableArray *investments;
@property BOOL investmentsLoaded;

@property int crrMin;
@property int crrSec;
@property double totalProfit;
@property double totalAmount;

@property (strong, nonatomic) NSMutableArray *chartAreas;

@property float chartOffsetLeft;
@property float chartOffsetTop;
@property float chartOffsetRight;
@property float chartOffsetBottom;

@property (strong, nonatomic) UILabel* currentLevelLabel;
@property (strong, nonatomic) IBOutlet UILabel *levelMin;
@property (strong, nonatomic) IBOutlet UILabel *levelMax;
@property (strong, nonatomic) IBOutlet UIButton *callBtn;

@property (strong, nonatomic) IBOutlet UIButton *putBtn;
@property (weak, nonatomic) IBOutlet UIButton *hourNext;
@property (weak, nonatomic) IBOutlet UIButton *hourPrev;

@property (weak, nonatomic) id delegate;

@property (nonatomic, readonly) CGFloat currentLevelX;
@property (nonatomic, readonly) CGFloat currentLevelY;
@property float winOdds;
@property float loseOdds;
@property float currentLevelLabelWidth;
@property ChartViewMode chartViewMode;

@property BOOL isPreviousHour;
@property float zoomCoef;
@property (nonatomic, assign) AOOpportunityType optionType;

- (CGFloat) getAlpha;
- (void) setChartData:(AOChartData*)data;
- (void) lsUpdateWithItemPosition:(int)itemPosition itemName:(NSString *)itemName didUpdateWithInfo:(LSUpdateInfo *)updateInfo;
- (void) addInvestment:(AOInvestment*)i;
- (void) unselectInvestmentButtons;
- (void) needsDisplayMethod;
- (void) reset;
- (void) selectInvestmentButtonWithInvestment:(AOInvestment*)investmentToSelect ;
- (void) scrollBack;
- (void) addNewInvestment: (AOInvestment*)inv ;
@end
