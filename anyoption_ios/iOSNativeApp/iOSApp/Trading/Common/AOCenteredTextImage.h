//
//  AOCenteredTextImage.h
//  iOSApp
//
//  Created by mac_user on 1/15/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

@interface AOCenteredTextImage : UIImageView {
    CGFloat refConstraintInitialValue;
}

@property (weak,nonatomic) IBOutlet UILabel *refLabel;
@property (weak,nonatomic) IBOutlet NSLayoutConstraint *refConstraint;

-(void)updatePosition;

@end
