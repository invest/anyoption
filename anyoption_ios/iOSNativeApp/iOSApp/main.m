             //
//  main.m
//  iOSApp
//
//  Created by  on 11/12/13.
//  Copyright (c) 2013 Anyoption. All rights reserved.
//

int main(int argc, char * argv[])
{
    @autoreleasepool {
        
#ifdef ETRADER
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSMutableArray *arr = [[userDefaults objectForKey:@"AppleLanguages"] mutableCopy];
        [arr removeObject:@"he"];
        [arr insertObject:@"he" atIndex:0];
        [userDefaults setObject:arr forKey:@"AppleLanguages"];
        [userDefaults synchronize];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"AppLayoutDirectionTrigger" object:nil];
#endif
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}