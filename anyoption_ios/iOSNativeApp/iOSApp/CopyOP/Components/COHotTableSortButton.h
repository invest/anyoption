//
//  COHotTableSortBUtton.h
//  iOSApp
//
//  Created by Lyubomir Marinov on 09/02/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface COHotTableSortButton : UIButton

@property (nonatomic, assign) NSInteger sortTag;

@end
