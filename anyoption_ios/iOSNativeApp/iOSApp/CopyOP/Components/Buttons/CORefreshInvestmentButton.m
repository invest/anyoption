//
//  CORefreshInvestmentButton.m
//  iOSApp
//
//  Created by Kaloyan Petrov on 2/8/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import "CORefreshInvestmentButton.h"

@implementation CORefreshInvestmentButton

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [super touchesBegan:touches withEvent:event];
    
    if(self.state == UIControlStateHighlighted) {
        [[self layer] setShadowOpacity:0.0];
        [[self layer] setShadowRadius:0.0];
        [[self layer] setShadowColor:nil];
    }
}

@end
