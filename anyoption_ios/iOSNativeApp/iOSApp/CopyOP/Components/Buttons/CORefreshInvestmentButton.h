//
//  CORefreshInvestmentButton.h
//  iOSApp
//
//  Created by Kaloyan Petrov on 2/8/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "COBorderedButton.h"

@interface CORefreshInvestmentButton : COBorderedButton

@end
