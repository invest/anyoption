//
//  COWithdrawalPopupViewController.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/28/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "COWithdrawalPopupViewController.h"

#import "CORoundedButton.h"
#import "UIFont+Roboto.h"
#import "UIView+Shadows.h"
#import "COPopupHeaderView.h"

@interface COWithdrawalPopupViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet CORoundedButton *btnCancel;
@property (weak, nonatomic) IBOutlet CORoundedButton *btnDone;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewIcon;

@end

@implementation COWithdrawalPopupViewController
{
    UIImageView *_shadowImage;
    UIImageView *_shadowCancelButton;
    UIImageView *_shadowDoneButton;
}

- (UIRectCorner) p_cancelRectCorners
{
    return UIRectCornerBottomLeft;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    self.lblMessage.font = [UIFont robotoFontLightWithSize:14.0];
    
    self.btnCancel.hasBorder = 0;
    self.btnCancel.solidColor = [UIColor whiteColor];
    self.btnCancel.roundedCorners = [self p_cancelRectCorners];
    self.btnCancel.cornerRadius = 5.0;
    self.btnCancel.titleLabel.font = [UIFont robotoFontLightWithSize:17.0];
    [self.btnCancel setTitleColor:[UIColor colorWithHexString:@"#364b5e"] forState:UIControlStateNormal];
    
    self.btnDone.hasBorder = 0;
    [self.btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnDone.titleLabel.font = [UIFont robotoFontMediumWithSize:17.0];
    self.btnDone.solidColor = [UIColor colorWithHexString:@"#ea553a"];
    
    self.lblMessage.preferredMaxLayoutWidth = CGRectGetWidth(self.view.bounds) - 2 * 20.0;
    
    self.lblMessage.attributedText = self.message;
    
    COPopupHeaderView *header = (COPopupHeaderView *)self.imgViewIcon.superview;
    header.showsSeparatorLine = NO;
    
    self.lblMessage.superview.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    
    [UIView performWithoutAnimation:^{
        [self.btnCancel setTitle:AOLocalizedString(@"CMS.optionplus.popup.text.btnNotNow") forState:UIControlStateNormal];
        [self.btnDone setTitle:AOLocalizedString(@"sure") forState:UIControlStateNormal];
        
        [self p_updateSize];
        [self.view layoutIfNeeded];
        [self p_updateShadow];
    }];
}

GENERATE_SETTER(setMessage, NSAttributedString *, message, p_updateMessage);

- (void) p_updateMessage
{
    self.lblMessage.attributedText = self.message;
    
    [self p_updateSize];
}

- (void) p_updateSize
{
    if (![self isViewLoaded])
    {
        return;
    }
    
    CGRect fr = self.view.frame;
    fr.size.height = [self.view systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    self.view.frame = fr;
}

- (IBAction) btnCancelPressed:(id)sender
{
    [self p_onDidDismiss];
}

- (IBAction) btnDonePressed:(id)sender
{
    [self p_onDidSubmit];
}

- (void) p_onDidDismiss
{
    if (self.completionHandler)
    {
        self.completionHandler(YES);
    }
}

- (void) p_onDidSubmit
{
    if (self.completionHandler)
    {
        self.completionHandler(NO);
    }
}


- (void) p_updateShadow
{
    [_shadowImage removeFromSuperview];
    
    UIView *headView = self.imgViewIcon.superview;
    CGRect r = CGRectMake(CGRectGetMinX(headView.frame),
                          CGRectGetMinY(headView.frame),
                          CGRectGetWidth(headView.bounds),
                          CGRectGetMaxY(self.lblMessage.superview.frame) - CGRectGetMinY(headView.frame));
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:r
                                               byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                                     cornerRadii:CGSizeMake(5.0, 5.0)];
    CGFloat shadowSize = 3.0;
    UIImage *img = [UIView shadowImageWithBezierPath:path shadowSize:shadowSize];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [self.view insertSubview:imgView atIndex:0];
    CGRect fr = imgView.frame;
    fr.origin = CGPointMake(CGRectGetMinX(headView.frame) - shadowSize, CGRectGetMinY(headView.frame) - shadowSize);
    imgView.frame = fr;
    _shadowImage = imgView;
    
    if (_shadowCancelButton == nil)
    {
        [_shadowCancelButton removeFromSuperview];
        UIBezierPath *cancelPath = [UIBezierPath bezierPathWithRoundedRect:self.btnCancel.bounds
                                                         byRoundingCorners:[self p_cancelRectCorners]
                                                               cornerRadii:CGSizeMake(5.0, 5.0)];
        UIImage *cancelShadowImage = [UIView shadowImageWithBezierPath:cancelPath shadowSize:shadowSize];
        UIImageView *cancelImgView = [[UIImageView alloc] initWithImage:cancelShadowImage];
        [self.view insertSubview:cancelImgView belowSubview:self.btnCancel];
        
        [self.btnCancel pinShadowImageView:cancelImgView shadowSize:shadowSize];
        
        _shadowCancelButton = cancelImgView;
    }
    
    if (_shadowDoneButton == nil)
    {
        [_shadowDoneButton removeFromSuperview];
        UIBezierPath *donePath = [UIBezierPath bezierPathWithRect:self.btnDone.bounds];
        UIImage *doneShadowImage = [UIView shadowImageWithBezierPath:donePath shadowSize:shadowSize];
        UIImageView *doneImgView = [[UIImageView alloc] initWithImage:doneShadowImage];
        [self.view insertSubview:doneImgView belowSubview:self.btnDone];
        
        [self.btnDone pinShadowImageView:doneImgView shadowSize:shadowSize];
        
        _shadowImage = doneImgView;
    }
}

@end
