//
//  COWithdrawalPopupViewController.h
//  iOSApp
//
//  Created by Antoan Tateosyan on 8/28/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^COWithdrawalPopupViewControllerCompletionHandler)(BOOL cancelled);

@interface COWithdrawalPopupViewController : UIViewController

@property (nonatomic, strong) NSAttributedString *message;

@property (nonatomic, copy) COWithdrawalPopupViewControllerCompletionHandler completionHandler;

@end
