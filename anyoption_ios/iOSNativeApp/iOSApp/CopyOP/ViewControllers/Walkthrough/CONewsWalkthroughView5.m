//
//  CONewsWalkthroughView5.m
//  iOSApp
//
//  Created by Kaloyan Petrov on 2/22/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import "CONewsWalkthroughView5.h"

#import "CONewsFeedViewController.h"
#import "UIFont+Roboto.h"

@implementation CONewsWalkthroughView5
{
    CONewsFeedViewController *_newsFeedVc;
    UILabel *_lblNews;
    UILabel *_lblTraders;
    UILabel *_lblFb;
    UILabel *_lblOutstanding;
}

- (id) initWithNewsFeedViewController:(CONewsFeedViewController *)newsFeedVc
{
    if ((self = [super initWithFrame:[MAINVC view].bounds]))
    {
        _newsFeedVc = newsFeedVc;
        
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (CGRect) p_visibleTableFrame
{
    return CGRectMake(0.0, 0.0, 1.0 / 3.0 * 1280.0, 40.0);
}

- (void) show
{
    [_newsFeedVc.navigationController.view addSubview:self];
    
    UIButton *gotItButton = [self gotItButton];
    [gotItButton addTarget:self action:@selector(btnGotItPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:gotItButton];
    
    [self maskBackgroundExceptRegion:[self p_visibleTableFrame]];
    
    [self p_positionNewsLabel];
    [self p_positionTradersLabel];

    
    CGRect btnFrame = gotItButton.frame;
    btnFrame.origin = CGPointMake(180.0, CGRectGetMaxY(_lblOutstanding.frame) + 30.0);
    gotItButton.frame = btnFrame;
    
    [self ensureGotItButtonVisible];
}

- (void) btnGotItPressed:(id)sender
{
    [self hideAndMarkWalkthroughAsShown:USER_DEFS_WALKTHROUGH_NEWS_SHOWN_KEY];

}

/*
 "tradeOnYourOwn" = "Trade on your own!";
 "wantToBeATradingLeader" = "Want to be a trading leader and trade on your own?";
 "checkTradePage" = "Check out our TRADE page";
 */
- (void) p_positionNewsLabel
{
    UIImage *image = [UIImage imageNamed:@"arrow_screen5_walkthroguh_9_2"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    CGRect arrFr = imageView.frame;
    CGRect vis = [self p_visibleTableFrame];
    arrFr.origin = CGPointMake(18.5, CGRectGetMaxY(vis) + 5.0);
    imageView.frame = arrFr;
    [self addSubview:imageView];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectZero];
    lbl.numberOfLines = 0;
    lbl.textAlignment = NSTextAlignmentCenter;
    
    
    NSMutableAttributedString *totalText = [[NSMutableAttributedString alloc] init];
    NSString *news = AOLocalizedString(@"wantToBeATradingLeader");
    NSAttributedString *newsStr = [[NSAttributedString alloc] initWithString:news attributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                               NSFontAttributeName : [UIFont robotoFontRegularWithSize:14.0],
                                                                                               NSUnderlineStyleAttributeName : @1}];
//    NSString *info = [NSString stringWithFormat:@"\n%@", AOLocalizedString(@"checkTradePage")];
//    NSAttributedString *infoStr = [[NSAttributedString alloc] initWithString:info attributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
//                                                                                               NSFontAttributeName : [UIFont robotoFontRegularWithSize:14.0]}];
    [totalText appendAttributedString:newsStr];
  //  [totalText appendAttributedString:infoStr];
    lbl.attributedText = totalText;
    
    
    CGRect boundingRect = [totalText boundingRectWithSize:CGSizeMake(280.0, 10000.0) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    CGRect lblFr = lbl.frame;
    
    lblFr.size = CGSizeMake(boundingRect.size.width - 15.0, boundingRect.size.height + 1.0);
    lblFr.origin = CGPointMake(CGRectGetMinX(arrFr) + 40.0, CGRectGetMaxY(arrFr) - 50.0);
    lbl.frame = lblFr;
    
    [self addSubview:lbl];
    
    _lblNews = lbl;
}

- (void) p_positionTradersLabel
{
    UILabel *lbl = [self labelWithText:AOLocalizedString(@"checkTradePage") maxWidth:260.0 alignment:NSTextAlignmentCenter];
  
    CGRect fr = lbl.frame;
    fr.origin = CGPointMake(40.0, CGRectGetMaxY(_lblNews.frame) + 30.0);
    lbl.frame = fr;
    
    [self addSubview:lbl];
    
    _lblTraders = lbl;
    _lblOutstanding = lbl;
}



@end
