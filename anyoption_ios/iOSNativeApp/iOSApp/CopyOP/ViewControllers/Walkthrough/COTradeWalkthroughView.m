//
//  COTradeWalkthroughView.m
//  iOSApp
//
//  Created by Kaloyan Petrov on 2/22/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import "COTradeWalkthroughView.h"
#import "CONewsFeedViewController.h"
#import "AOMainViewController.h"
#import "MainMenuViewController.h"

@implementation COTradeWalkthroughView
{
    MainMenuViewController *_mainMenuVc;
}

- (id) initWithNewsFeedViewController:(MainMenuViewController *)mainMenuVc
{
    if ((self = [super initWithFrame:[MAINVC view].bounds]))
    {
        _mainMenuVc = mainMenuVc;
        
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (CGRect) p_visibleTableFrame
{
    return CGRectMake(0.0, 293.0, CGRectGetWidth(_mainMenuVc.view.window.bounds) - 40, 43.0);
//    return CGRectMake(0.0, 293.0, 2.0 / 3.0 * 620.0, 43.0);
}

- (void) show
{
    [_mainMenuVc.view addSubview:self];
    
    UIButton *gotItButton = [self gotItButton];
    [gotItButton addTarget:self action:@selector(btnGotItPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:gotItButton];
    CGRect btnFrame = gotItButton.frame;
    btnFrame.origin = CGPointMake(220.0, CGRectGetMaxY([self p_visibleTableFrame]) + 160.0);
    gotItButton.frame = btnFrame;
    
    [self maskBackgroundExceptRegion:[self p_visibleTableFrame]];
    
//    [self p_positionFindingGoodTradersLabel];
    [self p_positionLiveNewsLabel];
    [self p_positionHotTradersLabel];
    
    [self ensureGotItButtonVisible];
}

- (void) btnGotItPressed:(id)sender
{
    [self hideAndMarkWalkthroughAsShown:USER_DEFS_WALKTHROUGH_TRADE_SHOWN_KEY];
}

//- (void) p_positionFindingGoodTradersLabel
//{
//    UILabel *lbl = [self labelWithText:AOLocalizedString(@"findingGoodTraders") maxWidth:200.0 alignment:NSTextAlignmentLeft];
//    CGRect fr = lbl.frame;
//    fr.origin = CGPointMake(5.0, 5.0);
//    lbl.frame = fr;
//    [self addSubview:lbl];
//}

- (void) p_positionLiveNewsLabel
{
    UIImage *image = [UIImage imageNamed:@"arrow_walkthroguh_9_1_new@2x"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    CGRect arrFr = imageView.frame;
    CGRect vis = [self p_visibleTableFrame];
    arrFr.origin = CGPointMake(CGRectGetMinX(vis) + 100.0, CGRectGetMaxY(vis) + 5.0);
    imageView.frame = arrFr;
    [self addSubview:imageView];
    
    UILabel *lbl = [self labelWithText:AOLocalizedString(@"tradeOnYourOwn") maxWidth:180.0 alignment:NSTextAlignmentLeft];
    CGRect fr = lbl.frame;
    fr.origin = CGPointMake(CGRectGetMaxX(arrFr) - 30.0, CGRectGetMaxY(arrFr) + 5.0);
    lbl.frame = fr;
    [self addSubview:lbl];
}

- (void) p_positionHotTradersLabel
{
    UIImage *image = [UIImage imageNamed:@"arrow_walkthroguh_9_1_new@2x"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    CGRect arrFr = imageView.frame;
    CGRect vis = [self p_visibleTableFrame];
    arrFr.origin = CGPointMake(CGRectGetMaxX(vis) + 100.0, CGRectGetMidY(vis));
    imageView.frame = arrFr;
    [self addSubview:imageView];
    
    UILabel *lbl = [self labelWithText:AOLocalizedString(@"orHotTradersTable") maxWidth:100.0 alignment:NSTextAlignmentLeft];
    CGRect fr = lbl.frame;
    fr.origin = CGPointMake(CGRectGetMaxX(arrFr) - CGRectGetWidth(fr) / 2.0, CGRectGetMaxY(vis) + 5.0);
    lbl.frame = fr;
    [self addSubview:lbl];
}

@end


