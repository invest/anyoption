//
//  CONewsWalkthroughView5.h
//  iOSApp
//
//  Created by Kaloyan Petrov on 2/22/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//


#import "COWalkthroughView.h"

@class CONewsFeedViewController;

@interface CONewsWalkthroughView5 : COWalkthroughView

- (id) initWithNewsFeedViewController:(CONewsFeedViewController *)newsFeedVc;
- (void) show;

@end