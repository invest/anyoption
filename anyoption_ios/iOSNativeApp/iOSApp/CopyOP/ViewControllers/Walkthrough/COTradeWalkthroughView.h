//
//  COTradeWalkthroughView.h
//  iOSApp
//
//  Created by Kaloyan Petrov on 2/22/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "COWalkthroughView.h"
#import "MainMenuViewController.h"
#import "AOBaseViewController.h"

@class MainMenuViewController;

@interface COTradeWalkthroughView: COWalkthroughView

- (id) initWithNewsFeedViewController:(MainMenuViewController *)mainMenuVc;
- (void) show;

@end
