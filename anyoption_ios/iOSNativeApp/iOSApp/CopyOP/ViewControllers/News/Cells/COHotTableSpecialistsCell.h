//
//  COHotTableViewCell.h
//  iOSApp
//
//  Created by  on 10/13/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "COHotsFeedItem.h"
#import "COFullProfile.h"

typedef void (^COCopyButtonEventHandler)();

@interface COHotTableSpecialistsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *columnTwo;
@property (weak, nonatomic) IBOutlet UILabel *columnThree;
@property (weak, nonatomic) IBOutlet UIButton *columnFourCopyButton;

@property (weak, nonatomic) IBOutlet UILabel *idLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (nonatomic, strong) COHotsFeedItem *feedItem;
@property (nonatomic, strong) COFullProfile *_fullProfile;


@property (nonatomic, assign) BOOL fbFriendCell;

@property (nonatomic, copy) COCopyButtonEventHandler eventHandlerCopy;

-(IBAction)copyButtonTapped:(UIButton *)sender;

-(void)configureCellWithHotsFeedItem:(COHotsFeedItem*)item ;
-(void)configureCellWithFBFriendItem:(NSDictionary*)fbFriend;
@end
