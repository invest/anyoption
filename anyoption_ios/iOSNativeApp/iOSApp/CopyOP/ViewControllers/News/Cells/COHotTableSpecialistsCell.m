//
//  COHotTableViewCell.m
//  iOSApp
//
//  Created by  on 10/13/14.
//  Copyright (c) 2014 Anyoption. All rights reserved.
//

#import "COHotTableSpecialistsCell.h"
#import "COCopyUserController.h"
#import "AOUtils.h"
#import "AOThemeController.h"
#import "AOCopyOpTheme.h"
#import "RestAPIFacade.h"
#import "FormUtils.h"

@implementation COHotTableSpecialistsCell{
    AOCopyOpTheme* theme;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    theme = (AOCopyOpTheme*)[AOThemeController sharedInstance].theme;
    
    self.nameLabel.font = [theme hotTableNameFont];
    self.columnTwo.font = [theme hotTableRowFont];
    self.columnThree.font = [theme hotTableRowFont];
    self.idLabel.font = [theme hotTableIdFont];
    self.idLabel.textColor = [theme hotTableIdColor];
    
    self.columnFourCopyButton.layer.borderColor = [UIColor colorWithHexString:@"#c4c4c4"].CGColor;
    self.columnFourCopyButton.layer.borderWidth = 1.0f;
    self.columnFourCopyButton.layer.cornerRadius = 8.0f;
    
    [self p_addSeparatorsTo:self.columnTwo];
    [self p_addRightSeparatorTo:self.columnThree];
    
}

-(void)p_addSeparatorsTo:(UILabel*)label {
    label.clipsToBounds = YES;
    
    CALayer *rightBorder = [CALayer layer];
    rightBorder.backgroundColor = [UIColor grayColor].CGColor;
    rightBorder.frame = CGRectMake(CGRectGetWidth(label.frame)-1, 10, 0.5, CGRectGetHeight(label.frame)-20);
    [label.layer addSublayer:rightBorder];
    
    
    CALayer *leftBorder = [CALayer layer];
    leftBorder.backgroundColor = [UIColor grayColor].CGColor;
    leftBorder.frame = CGRectMake(0, 10, 0.5, CGRectGetHeight(label.frame)-20);
    [label.layer addSublayer:leftBorder];
    
}

-(void)p_addRightSeparatorTo:(UILabel*)label {
    label.clipsToBounds = YES;
    
    CALayer *rightBorder = [CALayer layer];
    rightBorder.backgroundColor = [UIColor grayColor].CGColor;
    rightBorder.frame = CGRectMake(CGRectGetWidth(label.frame)-1, 10, 0.5, CGRectGetHeight(label.frame)-20);
    [label.layer addSublayer:rightBorder];
}

-(NSString*)p_formatCurrencyLabel:(NSString*)amount {
    
    
    
    NSString* formattedAmountProfit = @"";
    float wholeAmount = [amount floatValue];
    
    
    if (wholeAmount / 1000000 >= 1) {
        formattedAmountProfit = [NSString stringWithFormat:@"%.2fM ",wholeAmount / 1000000];
    }
    else if (wholeAmount/1000>=1){
          formattedAmountProfit = [NSString stringWithFormat:@"%.2fK ",wholeAmount / 1000];
    }
    else {
        formattedAmountProfit = [NSString stringWithFormat:@"%.0f ",wholeAmount ];
    }
    
    if ([[DLG user] isCurrencyLeftSymbol]) {
        formattedAmountProfit = [NSString stringWithFormat:@" +%@%@ ", [[DLG user] getCurrencySymbol], formattedAmountProfit  ];
    } else {
        formattedAmountProfit = [NSString stringWithFormat:@" +%@%@ ",formattedAmountProfit , [[DLG user] getCurrencySymbol]];
    }
    
    
    return formattedAmountProfit;
}

-(void)configureCellWithFBFriendItem:(NSDictionary*)fbFriend {
//    self.fbFriendCell=YES;
//    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@",fbFriend[@"firstName"],fbFriend[@"lastName"]];
//    self.columnTwo.text = [NSString stringWithFormat:@"%.2f%%",[fbFriend[@"hitRate"] floatValue] ];
//    self.columnThree.text = fbFriend[@"copiers"];
//    self.columnFour.text = fbFriend[@"watchers"];
//    self.backgroundColor = [UIColor clearColor];
//    
//     RestAPIFacade* facade = [RestAPIFacade sharedInstance];
//    
//    __weak COHotTableViewCell* wself = self;
//   
//    [facade getAvatarPictureWithURLString:fbFriend[@"avatar"] sideLength:AvatarPictureSize48 cachedVersion:YES completion:^(UIImage* avatarImage, AOResponse* response){
//        COHotTableViewCell* strongCell = wself;
//        if(response.error==nil){
//            strongCell.profileImageView.image = avatarImage;
//        }
//        
//    } ];
}

-(void)configureCellWithHotsFeedItem:(COHotsFeedItem*)item {
    
    self.feedItem = item;
  
    RestAPIFacade* facade = [RestAPIFacade sharedInstance];
    if([item.properties objectForKey:PROPERTY_KEY_NICKNAME] && [item.properties objectForKey:PROPERTY_KEY_NICKNAME]!=[NSNull null]) self.nameLabel.text = [item.properties objectForKey:PROPERTY_KEY_NICKNAME];
    __weak COHotTableSpecialistsCell* weakCell = self;
    
    if ([item.properties objectForKey:PROPERTY_KEY_AVATAR] && [item.properties objectForKey:PROPERTY_KEY_AVATAR]!=[NSNull null]) {
        [facade getAvatarPictureWithURLString:[item.properties objectForKey:PROPERTY_KEY_AVATAR] sideLength:AvatarPictureSize48 cachedVersion:YES completion:^(UIImage* avatarImage, AOResponse* response){
            COHotTableSpecialistsCell* strongCell = weakCell;
            if(response.error==nil){
                strongCell.profileImageView.image = avatarImage;
            }
        } ];
    }
   
    
    NSString* formattedAmountProfit = [self p_formatCurrencyLabel:[item.properties objectForKey:[NSString stringWithFormat:@"%@_%li",PROPERTY_KEY_PROFIT,[DLG user].currencyId]  ]];
    
   
    switch (item.msgType) {
        case BEST_TRADERS:
        case BEST_TRADERS_ONE_WEEK:
        case BEST_TRADERS_ONE_MOTNH:
        case BEST_TRADERS_TRHEE_MOTNH:
            self.columnTwo.text = formattedAmountProfit;
            self.columnThree.text = [NSString stringWithFormat:@" %@%% ",[item.properties objectForKey:PROPERTY_KEY_HIT_RATE]];
//            self.columnFour.text = [NSString stringWithFormat:@" %@ ",[item.properties objectForKey:PROPERTY_KEY_COPIERS]];
            break;
            
        case BEST_TRADES:
        case BEST_TRADES_ONE_WEEK:
        case BEST_TRADES_ONE_MOTNH:
        case BEST_TRADES_TRHEE_MOTNH:
        {
            self.columnTwo.text = formattedAmountProfit;
            self.columnThree.text = [NSString stringWithFormat:@" %@ ",[[RESIDENT.marketsInfo objectForKey:[item.properties objectForKey:PROPERTY_KEY_MARKET_ID]] objectForKey:@"name"]] ;
            
            //            self.columnFour.numberOfLines = 0;
            //            self.columnFour.text = [[self class] p_hotDateStringFromDate:[AOUtils parseTimeFromJSON2:[item.properties objectForKey:PROPERTY_KEY_ASSET_CREATE_DATE_TIME]]];
        }
            break;
            
            
            
        case BEST_SPECIALISTS:
        case BEST_SPECIALISTS_ONE_WEEK:
        case BEST_SPECIALISTS_ONE_MOTNH:
        case BEST_SPECIALISTS_TRHEE_MOTNH:
        {
            self.columnTwo.text = formattedAmountProfit;
            self.columnThree.text = [NSString stringWithFormat:@" %@ ",[[RESIDENT.marketsInfo objectForKey:[item.properties objectForKey:PROPERTY_KEY_MARKET_ID]] objectForKey:@"name"]] ;
            
            //            self.columnFour.numberOfLines = 0;
            //            self.columnFour.text = [[self class] p_hotDateStringFromDate:[AOUtils parseTimeFromJSON2:[item.properties objectForKey:PROPERTY_KEY_ASSET_CREATE_DATE_TIME]]];
        }
            break;
            
        case BEST_COPIERS:
        case BEST_COPIERS_ONE_WEEK:
        case BEST_COPIERS_ONE_MOTNH:
        case BEST_COPIERS_TRHEE_MOTNH:
            self.columnTwo.text = formattedAmountProfit;
            self.columnThree.text = [NSString stringWithFormat:@" %@%% ",[item.properties objectForKey:PROPERTY_KEY_HIT_RATE]];
//            self.columnFour.text = [NSString stringWithFormat:@" %@ ",[item.properties objectForKey:PROPERTY_KEY_COPIERS]];
            break;
        default:
            break;
    }
    
}

-(IBAction)copyButtonTapped:(UIButton *)sender {

    if (self.eventHandlerCopy)
    {
        self.eventHandlerCopy();
    }
    NSLog(@"copyButtonTapped: %@", self.feedItem.properties);
}

+ (NSString *) p_hotDateStringFromDate:(NSDate *)date
{
    static NSDateFormatter *df = nil;
    if (df == nil)
    {
        df = [[NSDateFormatter alloc] init];
        df.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        df.timeZone = [NSTimeZone timeZoneWithName:@"GMT+0"];
    }
    
    df.dateFormat = [AOUtils isDateToday:date] ? @" HH:mm " : @"HH:mm,\ndd/MM";
    
    return [df stringFromDate:date];
}

@end