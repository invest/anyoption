//
//  NIOUpopupViewController.h
//  iOSApp
//
//  Created by Lyubomir Marinov on 12/2/15.
//  Copyright © 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AOAttributedLabel.h"
typedef void (^COModalPopupViewControllerCloseHandler)(void);

@interface NIOUpopupViewController : UIViewController <AOAttributedLabelDelegate>

@property (nonatomic, copy) COModalPopupViewControllerCloseHandler closeButtonPressedHandler;

@property (nonatomic, strong) NSDictionary *niouData;

@end
