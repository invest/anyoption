//
//  LSLogDelegate.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>


/**
 * The LSLogDelegate protocol can be used to redirect the local logging
 * system to a different destination, such as a file or an application-wide
 * logging system.
 */
@protocol LSLogDelegate <NSObject>


#pragma mark -
#pragma mark Logging

/**
 * Called when a log line has to be appended. The line contains preformatted content,
 * such as the current thread pointer, the logging source name and its pointer, and
 * the actual log message. The line does not contain any line-ending.
 *
 * @param logLine The log line to be appended.
 */
- (void) appendLogLine:(NSString *)logLine;


@end
