//
//  AccountLockedBySecurityPopupViewController.m
//  iOSApp
//
//  Created by Kaloyan Petrov on 2/17/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import "AccountLockedBySecurityPopupViewController.h"
#import "AORegulationStatePopupViewController.h"
#import "AOThemeController.h"
#import "NSAttributedString+RT.h"
#import "CheckBox.h"
#import "RestAPIFacade.h"
#import "AOBottomCorneredView.h"
#import "RegulationRestrictedKnowledgeCheckbox.h"
#import "COPopupHeaderView.h"
#import "RestAPIFacade.h"
#import "AOUser.h"
#ifdef COPYOP
#import "UIView+Shadows.h"
#endif


#ifdef ANYOPTION
static CGFloat popupWidth = 300.0;
#elif COPYOP
static CGFloat popupWidth = 280.0;
#endif
@interface AccountLockedBySecurityPopupViewController()<CheckBoxDelegate>
@property (weak, nonatomic) IBOutlet UIButton *activateButton;

@property (weak, nonatomic) IBOutlet COPopupHeaderView *headerView;


@property (weak, nonatomic) IBOutlet UIButton *aoCloseButton;


@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *titleLabel;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *mainLabel;
//Titles
@property (weak, nonatomic) IBOutlet UILabel *emailTitle;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberTitle;
@property (weak, nonatomic) IBOutlet UILabel *faxNumberTitle;
@property (weak, nonatomic) IBOutlet UILabel *workingHoursTitle;

//Values
@property (weak, nonatomic) IBOutlet UIButton *email;
@property (weak, nonatomic) IBOutlet UIButton *phoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *faxNumber;
@property (weak, nonatomic) IBOutlet UITextView *workingHours;


@property (weak, nonatomic) IBOutlet RegulationRestrictedKnowledgeCheckbox *restrictedCheckBox;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_labelTop;

@property (weak, nonatomic) IBOutlet UIView *vContainer;
@property (weak, nonatomic) IBOutlet AOBottomCorneredView *vBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcCheckboxHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcOkButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcHeaderViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iPhoneVersionConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iPhoneVesrionImageH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iPhoneVersionImageW;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewBottomHeight;

@property (nonatomic, strong) UIFont *regularFont;
@property (nonatomic, strong) UIFont *boldFont;
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic) BOOL activateButoonIsClicked;

@end

@implementation AccountLockedBySecurityPopupViewController{
    AOUser *currUser;
    NSString *supportPhone;
    NSString *supportFax;
     NSNumber * countryId;
    //    BOOL restrictedKnowledge;
#ifdef COPYOP
    UIImageView *_shadowImage;
    UIImageView *_shadowUploadButton;
#endif
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.activateButoonIsClicked = NO;
    
    
    
    
#ifndef COPYOP
    self.viewBottomHeight.constant = 0;
#else
    [self.aoCloseButton removeFromSuperview];
#endif
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    if(CGRectGetHeight([UIScreen mainScreen].bounds) <= 480) {
        
        self.regularFont = [UIFont fontWithName:[theme defaultFontName] size:12.0];
        self.boldFont = [UIFont fontWithName:[theme defaultFontNameBold] size:12.0];
        self.titleFont = [UIFont fontWithName:[theme defaultFontNameBold] size:18.0];
        
        
    }else{
        
        self.regularFont = [UIFont fontWithName:[theme defaultFontName] size:14.0];
        self.boldFont = [UIFont fontWithName:[theme defaultFontNameBold] size:14.0];
        self.titleFont = [UIFont fontWithName:[theme defaultFontNameBold] size:20.0];
        
    }
    
#ifdef COPYOP
    [self.activateButton setTintColor:[theme updateInfoHeadingTextColor]];
#endif
    
    
    [self.activateButton addTarget:self action:@selector(activateMyAccountButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    NSMutableAttributedString* rtString = [[[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"cysegButton")withTheme:theme
                                                                          andAttributes:@{
                                                                                          NSFontAttributeName: self.titleFont
                                                                                          }  ] mutableCopy];
    //  NSMutableString *activateButtonTitle = [[NSMutableString alloc]initWithFormat:@"%@",rtString];
    //    [self.activateButton setTitle: activateButtonTitle forState: UIControlStateNormal];
    [self.activateButton setAttributedTitle:rtString forState:UIControlStateNormal];
    
#ifdef COPYOP
    self.vBottom.hidden = NO;
    //Changing the button text depending on the langauge
    
    
    
    self.headerView.hidden = NO;
    
    self.view.backgroundColor = [UIColor clearColor];
    self.headerView.showsSeparatorLine = NO;
    self.vBottom.backgroundColor = [UIColor clearColor];
    
    self.vContainer.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    
    [self.activateButton setTitleColor:[UIColor colorWithRed:234.0/255.0 green:85.0/255.0 blue:58.0/255.0 alpha:1.0] forState:UIControlStateNormal];
#endif
    
    self.view.backgroundColor = [UIColor clearColor];
    
    [self updateUI];
    
    
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if(CGRectGetHeight([UIScreen mainScreen].bounds) <= 480) {
        self.iPhoneVersionConstraint.constant = 0.0f;
        self.iPhoneVersionImageW.constant = 75.0f;
        self.iPhoneVesrionImageH.constant = 75.0f;
        
    } else {
        self.iPhoneVersionConstraint.constant = 0.0f;
        self.iPhoneVersionImageW.constant = 90.0f;
        self.iPhoneVesrionImageH.constant = 90.0f;
    }
}

#ifdef COPYOP

-(void)okButtonPressed:(id)sender {
    if (self.closeButtonPressedHandler != nil)
    {
        self.closeButtonPressedHandler();
    }
}

#ifdef COPYOP
- (void) p_updateShadow
{
    [self.view layoutIfNeeded];
    
    [_shadowImage removeFromSuperview];
    
    UIView *headView = self.headerView;
    CGRect r = CGRectMake(CGRectGetMinX(headView.frame),
                          CGRectGetMinY(headView.frame),
                          CGRectGetWidth(headView.bounds),
                          CGRectGetMaxY(self.vContainer.frame));
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:r
                                               byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                                     cornerRadii:CGSizeMake(5.0, 5.0)];
    CGFloat shadowSize = 3.0;
    UIImage *img = [UIView shadowImageWithBezierPath:path shadowSize:shadowSize];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [self.view insertSubview:imgView atIndex:0];
    
    CGRect fr = imgView.frame;
    fr.origin = CGPointMake(CGRectGetMinX(headView.frame) - shadowSize, CGRectGetMinY(headView.frame) - shadowSize);
    imgView.frame = fr;
    _shadowImage = imgView;
    
    if (_shadowUploadButton == nil)
    {
        [_shadowUploadButton removeFromSuperview];
        UIBezierPath *cancelPath = [UIBezierPath bezierPathWithRoundedRect:self.vBottom.bounds
                                                         byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight
                                                               cornerRadii:CGSizeMake(5.0, 5.0)];
        UIImage *cancelShadowImage = [UIView shadowImageWithBezierPath:cancelPath shadowSize:shadowSize];
        UIImageView *cancelImgView = [[UIImageView alloc] initWithImage:cancelShadowImage];
        [self.view insertSubview:cancelImgView belowSubview:self.vBottom];
        
        [self.vBottom pinShadowImageView:cancelImgView shadowSize:shadowSize];
        
        _shadowUploadButton = cancelImgView;
    }
}
#endif

#endif



- (void) updateUI
{
    if (![self isViewLoaded])
    {
        return;
    }
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    //    self.restrictedCheckBox.hidden = YES;
    //
    //    self.lcCheckboxHeight.constant = 0.0;
    
    
#ifdef ANYOPTION
    self.viewBottomHeight.constant = 0.0;
    self.lcOkButtonHeight.constant = 0.0;
    [self.headImageView setImage:[UIImage imageNamed:@"AOblocked_account_icon"]];
    self.vContainer.backgroundColor = [UIColor clearColor];
#elif COPYOP
    [self.headImageView setImage:[UIImage imageNamed:@"COblocked_account_icon"]];
#endif
    //Populating strings for BAC-1189
    //First line
    //    UIFont *normalFont = [UIFont fontWithName:[theme defaultFontName] size:16.0];
    //    UIFont *boldFont = [UIFont fontWithName:[theme defaultFontNameBold] size:16.0];
    //    UIFont *boldFontLarge = [UIFont fontWithName:[theme defaultFontNameBold] size:20.0];
    self.emailTitle.text = AOLocalizedString(@"accountLockedBySecurityEmail");
    self.phoneNumberTitle.text = AOLocalizedString(@"accountLockedBySecurityPhoneNumber");
    self.faxNumberTitle.text = AOLocalizedString(@"accountLockedBySecurityFaxNumber");
    self.workingHoursTitle.text = AOLocalizedString(@"accountLockedBySecurityWorkingHours");
    [self.email setTitle: AOLocalizedString(@"suspended.support.email") forState:UIControlStateNormal];
    self.email.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    self.titleLabel.text = AOLocalizedString(@"accountLockedBySecurityTitle");
     self.mainLabel.text = AOLocalizedString(@"accountLockedBySecurityDescription");
    
//    NSDictionary *userDict = [[NSDictionary alloc]init];
//    [[RestAPIFacade sharedInstance] getUser:@{} completion:^(AOUser *user,AOUserRegulation *reg, AOResponse *responseDict, NSInteger minInvestmentAm){
//    
//    
//    }];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    long currSkinId = [defaults integerForKey:@"currentSkinIdForLockedPopUp"];
    
    
//    NSString *skinIdx = [NSString stringWithFormat:@"%ld", [DLG user].skinId];
    NSString *skinIdx = [NSString stringWithFormat:@"%ld", currSkinId];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"skins" ofType:@"plist"];
    NSDictionary *skinsDict = [[NSDictionary alloc]initWithContentsOfFile:filePath];
    NSDictionary *skinDict = skinsDict[skinIdx];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm"];
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *startTime = [df dateFromString:[skinDict objectForKey:@"support_start_time"]];
    NSDate *endTime = [df dateFromString:[skinDict objectForKey:@"support_end_time"]];
//    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
    NSString *localStartTimeStr = [df stringFromDate:startTime];
    NSString *localEndTimeStr = [df stringFromDate:endTime];
    
    NSString *wt = [AOLocalizedString(@"contact.working.hours") stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    NSString *wholeText = [NSString stringWithFormat:wt, localStartTimeStr, localEndTimeStr,@"",@""];
    self.workingHours.text = wholeText;
    self.workingHours.contentScaleFactor = 0.5;
   
    NSLog(@"USER SKIN-ID %ld",[DLG user].skinId);
 
    [[RestAPIFacade sharedInstance] fetchCountryIdWithCompletion:^(NSNumber* inCountryId, BOOL isRestricted, AOResponse *response) {
   
            countryId = inCountryId;
            NSString *cId = [countryId stringValue];
        

        NSArray* countriesArray  = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"plist"]] ;
        for (int i = 0; i < [countriesArray count]; i++) {
            
            if ([[[countriesArray objectAtIndex:i] objectForKey:@"id"] isEqualToString:cId]) {
                supportPhone = [[countriesArray objectAtIndex:i] objectForKey:@"support_phone"];
                 supportFax = [[countriesArray objectAtIndex:i] objectForKey:@"support_fax"];
                [self.phoneNumber setTitle:supportPhone forState:UIControlStateNormal];
                self.phoneNumber.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                [self.faxNumber setTitle:supportFax forState:UIControlStateNormal];
                self.faxNumber.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                break;
            }
        }
        
    }];

    
    
    
    NSMutableAttributedString* rtString = [[[NSAttributedString alloc] initWithRTString:[NSString stringWithFormat:@"%@", @""]
                                                                              withTheme:theme
                                                                          andAttributes:@{
                                                                                          NSFontAttributeName: self.titleFont
                                                                                          }  ] mutableCopy];
    NSAttributedString *cr = [[NSAttributedString alloc] initWithString: @"\n"];
    
    
    

//    [rtString appendAttributedString:lineOneAtributedString];
    [rtString appendAttributedString:cr];
//    [rtString appendAttributedString:lineTwo];
    [rtString appendAttributedString:cr];
//    [rtString appendAttributedString:lineThree];
    
//    self.mainLabel.attributedText = rtString;
    
    
    //AutoLayout fitting of the popUp
    CGSize size = [self.view systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    NSLog(@"%@",NSStringFromCGSize(size));
    
    CGRect fr = self.view.frame;
    fr.size.height = size.height;
    self.view.frame = fr;
#ifdef COPYOP
    [self p_updateShadow];
#endif
}




#pragma mark -
#pragma mark  AOAttributedLabel delegate

//-(void)tappedAttributes:(NSDictionary*)attributes{
//
//    if ([attributes objectForKey:@"action"]!= nil && [attributes objectForKey:@"action"]!=[NSNull null] && ![[attributes objectForKey:@"action"] isEmpty]) {
//
//        NSArray* actionAttribute = [[attributes objectForKey:@"action"] componentsSeparatedByString:@"_"];
//        if (!actionAttribute.count || [actionAttribute[1] isEmpty]) {
//            return;
//        }
//
//        [[MAINVC popupVC] hide];
//        [RESIDENT goTo:actionAttribute[1] withOptions:nil];
//    }
//}

#pragma mark -
#pragma mark Checkbox Delegate

//-(void)checkBoxValueChanged:(CheckBox *)checkbox {
//    self.restrictedKnowledge = checkbox.value;
//}
//
#pragma mark - IBActions

- (IBAction)emailButtonPressed:(id)sender {
    NSURL *mailUrl = [NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@",AOLocalizedString(@"suspended.support.email")]];
    [[UIApplication sharedApplication]openURL:mailUrl];
    
}

- (IBAction)phoneNumberButtonPressed:(id)sender {
    
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        NSString *phoneNumber = [@"tel://" stringByAppendingString:supportPhone];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    } else {
        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [Notpermitted show];
    }
}

- (IBAction)faxButtonPressed:(id)sender {
    
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        NSString *phoneNumber = [@"tel://" stringByAppendingString:supportPhone];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    } else {
        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [Notpermitted show];
    }
}

/* BAC - 1189 */
-(void) activateMyAccountButtonPressed {
    
    if (self.activateButoonIsClicked == NO) {
        
        self.activateButoonIsClicked = YES;
        NSLog(@"TRESHOLD BLOCKED USER!!!!!!: %@ :",[DLG user]);
        
        [[RestAPIFacade sharedInstance] updateCysec:[DLG user] withCompletion:^(AOResponse *response) {
            if ([response.parsedResponce[@"errorCode"]intValue] == 0) {
                
                [DLG userRegulation].thresholdBlock = -1;
                [DLG user].isSafeMode = -1;
                NSLog(@"RELOADED USER!!!!!!: %@ :",[DLG user]);
                
                if(self.activateButtonPressedHandler) {
                    self.activateButtonPressedHandler();
                }
            } else {
                self.activateButoonIsClicked = NO;
                
                //                if(self.activateButtonPressedHandler) {
                //                    self.activateButtonPressedHandler();
                //                }
            }
        }];
        
    }
    
}

- (IBAction)aoCloseButtonPressed:(id)sender {
    if(self.hideButtonPressedHandler) {
        self.hideButtonPressedHandler();
    }
}

@end
