//
//  PEPPopupViewController.m
//  iOSApp
//
//  Created by Lyubomir Marinov on 11/26/15.
//  Copyright © 2015 Anyoption. All rights reserved.
//

#import "PEPPopupViewController.h"
#import "AORegulationStatePopupViewController.h"
#import "AOThemeController.h"
#import "NSAttributedString+RT.h"
#import "CheckBox.h"
#import "RestAPIFacade.h"
#import "AOBottomCorneredView.h"
#import "RegulationRestrictedKnowledgeCheckbox.h"
#import "COPopupHeaderView.h"
#ifdef COPYOP
#import "UIView+Shadows.h"
#endif


#ifdef ANYOPTION
static CGFloat popupWidth = 300.0;
#elif COPYOP
static CGFloat popupWidth = 280.0;
#endif


@interface PEPPopupViewController () <CheckBoxDelegate>


@property (weak, nonatomic) IBOutlet UIButton *okButton;

@property (weak, nonatomic) IBOutlet COPopupHeaderView *headerView;




@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *mainLabel;

@property (weak, nonatomic) IBOutlet RegulationRestrictedKnowledgeCheckbox *restrictedCheckBox;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_labelTop;

@property (weak, nonatomic) IBOutlet UIView *vContainer;
@property (weak, nonatomic) IBOutlet AOBottomCorneredView *vBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcCheckboxHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcOkButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcHeaderViewHeight;

@end

@implementation PEPPopupViewController{
    BOOL restrictedKnowledge;
#ifdef COPYOP
    UIImageView *_shadowImage;
    UIImageView *_shadowUploadButton;
#endif
}


-(void)viewDidLoad{
    [super viewDidLoad];
    self.restrictedCheckBox.value = NO;
    self.mainLabel.adelegate = self;
    
#ifdef COPYOP
    self.vBottom.hidden = NO;
    
    
    [self.okButton addTarget:self action:@selector(okButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"currentLang"] isEqualToString:@"es"])
    {
        [self.okButton setTitle:@"OK" forState:UIControlStateNormal];
    }
    else
    {
        [self.okButton setTitle:AOLocalizedString(@"alert.button") forState:UIControlStateNormal];
    }
    
    self.headerView.hidden = NO;
    
    self.view.backgroundColor = [UIColor clearColor];
    self.headerView.showsSeparatorLine = NO;
    self.vBottom.backgroundColor = [UIColor clearColor];
    
    self.vContainer.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    
    [self.okButton setTitleColor:[UIColor colorWithRed:234.0/255.0 green:85.0/255.0 blue:58.0/255.0 alpha:1.0] forState:UIControlStateNormal];
#endif
    
    self.view.backgroundColor = [UIColor clearColor];
    
    [self updateUI];
}


#ifdef COPYOP

-(void)okButtonPressed:(id)sender {
    if (self.closeButtonPressedHandler != nil)
    {
        self.closeButtonPressedHandler();
    }
}

#ifdef COPYOP
- (void) p_updateShadow
{
    [self.view layoutIfNeeded];
    
    [_shadowImage removeFromSuperview];
    
    UIView *headView = self.headerView;
    CGRect r = CGRectMake(CGRectGetMinX(headView.frame),
                          CGRectGetMinY(headView.frame),
                          CGRectGetWidth(headView.bounds),
                          CGRectGetMaxY(self.vContainer.frame));
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:r
                                               byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                                     cornerRadii:CGSizeMake(5.0, 5.0)];
    CGFloat shadowSize = 3.0;
    UIImage *img = [UIView shadowImageWithBezierPath:path shadowSize:shadowSize];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [self.view insertSubview:imgView atIndex:0];
    
    CGRect fr = imgView.frame;
    fr.origin = CGPointMake(CGRectGetMinX(headView.frame) - shadowSize, CGRectGetMinY(headView.frame) - shadowSize);
    imgView.frame = fr;
    _shadowImage = imgView;
    
    if (_shadowUploadButton == nil)
    {
        [_shadowUploadButton removeFromSuperview];
        UIBezierPath *cancelPath = [UIBezierPath bezierPathWithRoundedRect:self.vBottom.bounds
                                                         byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight
                                                               cornerRadii:CGSizeMake(5.0, 5.0)];
        UIImage *cancelShadowImage = [UIView shadowImageWithBezierPath:cancelPath shadowSize:shadowSize];
        UIImageView *cancelImgView = [[UIImageView alloc] initWithImage:cancelShadowImage];
        [self.view insertSubview:cancelImgView belowSubview:self.vBottom];
        
        [self.vBottom pinShadowImageView:cancelImgView shadowSize:shadowSize];
        
        _shadowUploadButton = cancelImgView;
    }
}
#endif

#endif

- (void) updateUI
{
    if (![self isViewLoaded])
    {
        return;
    }
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    self.restrictedCheckBox.hidden = YES;
    self.lcCheckboxHeight.constant = 0.0;
            
            
#ifdef ANYOPTION
    self.lcOkButtonHeight.constant = 0.0;
    [self.headImageView setImage:[UIImage imageNamed:@"restricted_icon"]];
    self.vContainer.backgroundColor = [UIColor clearColor];
#elif COPYOP
    [self.headImageView setImage:[UIImage imageNamed:@"co_blocked_icon"]];
#endif
//    NSMutableAttributedString* rtString = [[[NSAttributedString alloc] initWithRTString:[NSString stringWithFormat:AOLocalizedString(@"popupUserBlockedTitle"),AOLocalizedString(@"app_name")] withTheme:theme andAttributes:@{NSFontAttributeName:[theme quoteExpiryLevelFont]}  ] mutableCopy];
    NSMutableAttributedString* rtString = [[[NSAttributedString alloc] initWithRTString:[NSString stringWithFormat:AOLocalizedString(@"PEPWarnng"), [DLG user].email] withTheme:theme andAttributes:@{NSFontAttributeName:[theme quoteExpiryLevelFont]}  ] mutableCopy];

    NSAttributedString* rtFooterString = [[NSAttributedString alloc] initWithRTString:[NSString stringWithFormat:@"%@",AOLocalizedString(@"popupUserBlockedFooter")] withTheme:theme andAttributes:@{NSFontAttributeName:[theme quoteExpiryLevelFont]}  ] ;
            [rtString appendAttributedString:rtFooterString];
    NSRange termsRange = [rtString.string rangeOfString:[DLG user].email];
    [rtString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:16] range:termsRange];
    
            self.mainLabel.attributedText = rtString;

    CGSize size = [self.view systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    CGRect fr = self.view.frame;
    fr.size.height = size.height;
    self.view.frame = fr;
#ifdef COPYOP
    [self p_updateShadow];
#endif
}

#pragma mark -
#pragma mark  AOAttributedLabel delegate

-(void)tappedAttributes:(NSDictionary*)attributes{
    if ([attributes objectForKey:@"action"]!= nil && [attributes objectForKey:@"action"]!=[NSNull null] && ![[attributes objectForKey:@"action"] isEmpty]) {
        
        NSArray* actionAttribute = [[attributes objectForKey:@"action"] componentsSeparatedByString:@"_"];
        if (!actionAttribute.count || [actionAttribute[1] isEmpty]) {
            return;
        }
        
        [[MAINVC popupVC] hide];
        [RESIDENT goTo:actionAttribute[1] withOptions:nil];
    }
}

#pragma mark -
#pragma mark Checkbox Delegate

-(void)checkBoxValueChanged:(CheckBox *)checkbox {
    restrictedKnowledge = checkbox.value;
}
@end
