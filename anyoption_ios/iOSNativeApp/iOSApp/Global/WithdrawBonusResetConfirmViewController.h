//
//  WithdrawBonusResetConfirmViewController.h
//  iOSApp
//
//  Created by  on 9/9/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AOAttributedLabel.h"
#ifdef COPYOP
typedef void (^COModalPopupViewControllerCloseHandler)(void);
typedef void (^COYesHandler)(void);
typedef void (^CONoHandler)(void);
#endif

@interface WithdrawBonusResetConfirmViewController : UIViewController  <AOAttributedLabelDelegate>


#ifdef COPYOP
@property (nonatomic, copy) COModalPopupViewControllerCloseHandler closeButtonPressedHandler;
@property (nonatomic, copy) COYesHandler yesPressedHandler;
@property (nonatomic, copy) CONoHandler noPressedHandler;
#endif

@end