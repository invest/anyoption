//
//  RestrictedPopupViewController.h
//  iOSApp
//
//  Created by Kaloyan Petrov on 1/19/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AOAttributedLabel.h"


typedef void (^COModalPopupViewControllerCloseHandler)(void);
typedef void (^COModalPopupViewControllerHideHandler)(void);
typedef void (^COModalPopupViewControllerActivateHandler)(void);




@interface CysecPopupViewController : UIViewController <AOAttributedLabelDelegate>


@property (nonatomic, copy) COModalPopupViewControllerCloseHandler closeButtonPressedHandler;
@property (nonatomic, copy) COModalPopupViewControllerHideHandler hideButtonPressedHandler;
@property (nonatomic, copy) COModalPopupViewControllerActivateHandler activateButtonPressedHandler;

@property (nonatomic, assign) BOOL restrictedKnowledge;



@end
