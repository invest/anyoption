//
//  AORegulationStatePopupViewController.m
//  iOSApp
//
//  Created by  on 8/31/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AORegulationStatePopupViewController.h"
#import "RegulationPopupViewController.h"
#import "CustomIOSAlertView.h"


@interface AORegulationStatePopupViewController()


@property (nonatomic, assign) AORegulationStatePopupType popupType;
@property (nonatomic, strong) RegulationPopupViewController* popupVC;

@end


@implementation AORegulationStatePopupViewController
//
//-(id)initWithType:(AORegulationStatePopupType)type {
//    self = [super init];
//    
//    
//    if (self) {
//        _popupType = type;
//        switch (_popupType) {
//            case AORegulationStatePopupRestricted:{
//#ifdef ANYOPTION
//                
//                
//                self.popupVC = [RESIDENT getScreen:@"regulationPopup"];
//              
//                
//                
//#elif COPYOP
//#endif
//                
//            }
//                break;
//                
//            default:  //blocked
//                self.popupVC = [RESIDENT getScreen:@"regulationPopup"];
//                break;
//        }
//
//    }
//    
//    return self;
//}
//
//
//
//- (void) showPopupАnimated:(BOOL)animated
//{
//    
//    
//    //self.popupVC.regulationType = _popupType;
//
//    
//    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
//    alertView.backgroundColor = [UIColor whiteColor];
//    
//    [alertView setContainerView:self.popupVC.view];
//    
//    
//    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:AOLocalizedString(@"alert.button"), nil]];
//    //[alertView setDelegate:self];
//    
//    
//    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
//        [alertView close];
//    }];
//    
//    [alertView setUseMotionEffects:true];
//    
//   
//
//    [alertView show];
//    
//    
//   
//    
//    [[MAINVC view] addSubview:alertView];
//    
//    
//    
//    
//    if (animated)
//    {
//        alertView.alpha = 0.0;
//        
//        [UIView animateWithDuration:0.3
//                              delay:0.0
//                            options:UIViewAnimationOptionCurveEaseInOut
//                         animations:^{
//                             alertView.alpha = 1.0;
//                         } completion:^(BOOL finished) {
//                             
//                         }];
//    }
//    
//    
//    
//}
//
//
//


@end
