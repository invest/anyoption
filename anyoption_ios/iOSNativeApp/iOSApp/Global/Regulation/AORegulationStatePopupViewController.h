//
//  AORegulationStatePopupViewController.h
//  iOSApp
//
//  Created by  on 8/31/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {
    _AORegulationStatePopupRestricted,
    _AORegulationStatePopupBlocked
}AORegulationStatePopupType;


@interface AORegulationStatePopupViewController : UIViewController
-(id)initWithType:(AORegulationStatePopupType)type;
- (void) showPopupАnimated:(BOOL)animated;
@end
