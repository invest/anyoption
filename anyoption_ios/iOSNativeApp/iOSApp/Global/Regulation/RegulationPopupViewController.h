//
//  RegulationPopupViewController.h
//  iOSApp
//
//  Created by  on 9/1/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AOAttributedLabel.h"

typedef void (^COModalPopupViewControllerCloseHandler)(void);

typedef enum {
    AORegulationUserRestricted,
    AORegulationUserBlocked
}AORegulationType;

@interface RegulationPopupViewController : UIViewController <AOAttributedLabelDelegate>

@property (nonatomic, assign) AORegulationType regulationType;

@property (nonatomic, copy) COModalPopupViewControllerCloseHandler closeButtonPressedHandler;

@property (nonatomic, assign) BOOL restrictedKnowledge;

@end
