//
//  RegulationRestrictedKnowledgeCheckbox.m
//  iOSApp
//
//  Created by  on 9/16/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "RegulationRestrictedKnowledgeCheckbox.h"
#import "RestAPIFacade.h"

@implementation RegulationRestrictedKnowledgeCheckbox

//Sorry
-(void)willMoveToWindow:(UIWindow *)newWindow{
    [super willMoveToWindow:newWindow];
    if (newWindow==nil) {
        [[RestAPIFacade sharedInstance] regulationRestrictedKnowledge:self.value withCompletion:^(AOResponse *response) {
            if(self.value == YES) {
//                [[MAINVC popupVC] showPopup:AOPEPRestricted withData:nil animated:YES completion:nil];
                if([DLG userRegulation].pepState == 1 || [DLG userRegulation].pepState == 2) {
                    [[MAINVC popupVC] showPopup:AOPEPRestricted withData:nil animated:YES completion:nil];
                }

            } else {
                NSLog(@"how 'bout NO");
            }
        }];  // use completion!
    }
   
}
@end
