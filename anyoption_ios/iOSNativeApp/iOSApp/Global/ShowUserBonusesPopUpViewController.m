//
//  ShowUserBonusesPopUpViewController.m
//  iOSApp
//
//  Created by Kaloyan Petrov on 2/15/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import "ShowUserBonusesPopUpViewController.h"
#import "AORegulationStatePopupViewController.h"
#import "AOThemeController.h"
#import "NSAttributedString+RT.h"
#import "CheckBox.h"
#import "RestAPIFacade.h"
#import "AOBottomCorneredView.h"
#import "RegulationRestrictedKnowledgeCheckbox.h"
#import "COPopupHeaderView.h"
#import "AOBonusUsers.h"
#ifdef COPYOP
#import "UIView+Shadows.h"
#endif

#ifdef ANYOPTION
static CGFloat popupWidth = 300.0;
#elif COPYOP
static CGFloat popupWidth = 280.0;
#endif


@interface ShowUserBonusesPopUpViewController ()<CheckBoxDelegate>
@property (weak, nonatomic) IBOutlet UIButton *claimButton;

@property (weak, nonatomic) IBOutlet COPopupHeaderView *headerView;
@property (weak, nonatomic) IBOutlet UIView *separatorLineTop;


@property (weak, nonatomic) IBOutlet UIButton *coCloseButton;
@property (weak, nonatomic) IBOutlet UIButton *aoCloseButton;

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *mainLabel;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *secondLabel;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *thirdLabel;

@property (weak, nonatomic) IBOutlet AOAttributedLabel *bonusLabel;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *titleLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_labelTop;

@property (weak, nonatomic) IBOutlet UIView *vContainer;
@property (weak, nonatomic) IBOutlet AOBottomCorneredView *vBottom;
@property (weak, nonatomic) IBOutlet UIView *vbonusLabelBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcCheckboxHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcOkButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcHeaderViewHeight;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iPhoneVersionConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iPhoneVesrionImageH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iPhoneVersionImageW;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewBottomHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeaderHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewLabelHeaderHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *singleBonusViewHeight;


@property (nonatomic, strong) UIFont *regularFont;
@property (nonatomic, strong) UIFont *boldFont;
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic) BOOL activateButoonIsClicked;


@end

@implementation ShowUserBonusesPopUpViewController{

NSArray *turnoverFactorArray;
//AOBonusUsers *bonusUsersObject;
UIImageView *_shadowImage;
UIImageView *_shadowUploadButton;

}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.activateButoonIsClicked = NO;
    
    self.vbonusLabelBottom.backgroundColor = [UIColor colorWithHexString:@"#f1e9dc"];
    
//    id<AOAppTheme> themeAO = [AOThemeController sharedInstance].theme;
//    self.headerView.backgroundColor = [themeAO appBackgroundColor];

    
    
#ifndef COPYOP
    self.viewBottomHeight.constant = 0;

#else
    [self.aoCloseButton removeFromSuperview];
#endif
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    if(CGRectGetHeight([UIScreen mainScreen].bounds) <= 480) {
        
        self.regularFont = [UIFont fontWithName:[theme defaultFontNameMedium] size:16.0];
        self.boldFont = [UIFont fontWithName:[theme defaultFontNameBold] size:12.0];
        self.titleFont = [UIFont fontWithName:[theme defaultFontNameBold] size:18.0];
        
        
    }else{
        
        self.regularFont = [UIFont fontWithName:[theme defaultFontNameMedium] size:16.0];
        self.boldFont = [UIFont fontWithName:[theme defaultFontNameBold] size:14.0];
        self.titleFont = [UIFont fontWithName:[theme defaultFontNameBold] size:20.0];
        
    }
    
#ifdef COPYOP
    [self.titleLabel setTextColor:[theme updateInfoHeadingTextColor]];
  
    [self.claimButton setTintColor:[theme updateInfoHeadingTextColor]];
#endif
    
    
    [self.claimButton addTarget:self action:@selector(activateMyAccountButtonPressed) forControlEvents:UIControlEventTouchUpInside];
  
    
#ifdef COPYOP
    self.vBottom.hidden = NO;
    //Changing the button text depending on the langauge
    
    
    
    self.headerView.hidden = NO;
    
    self.view.backgroundColor = [UIColor clearColor];
    self.headerView.showsSeparatorLine = NO;
    self.vBottom.backgroundColor = [UIColor clearColor];
    
    self.vContainer.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    
    [self.claimButton setTitleColor:[UIColor colorWithRed:234.0/255.0 green:85.0/255.0 blue:58.0/255.0 alpha:1.0] forState:UIControlStateNormal];
#endif
 
    self.view.backgroundColor = [UIColor clearColor];
    
    [self updateUI];
    
    
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if(CGRectGetHeight([UIScreen mainScreen].bounds) <= 480) {
        self.iPhoneVersionConstraint.constant = 0.0f;
        self.iPhoneVersionImageW.constant = 75.0f;
        self.iPhoneVesrionImageH.constant = 75.0f;
        
    } else {
        self.iPhoneVersionConstraint.constant = 0.0f;
        self.iPhoneVersionImageW.constant = 90.0f;
        self.iPhoneVesrionImageH.constant = 90.0f;
    }
}

#ifdef COPYOP

-(void)okButtonPressed:(id)sender {
    if (self.closeButtonPressedHandler != nil)
    {
        self.closeButtonPressedHandler();
    }
}

#ifdef COPYOP
- (void) p_updateShadow
{
    [self.view layoutIfNeeded];
    
    [_shadowImage removeFromSuperview];
    
    UIView *headView = self.headerView;
    CGRect r = CGRectMake(CGRectGetMinX(headView.frame),
                          CGRectGetMinY(headView.frame),
                          CGRectGetWidth(headView.bounds),
                          CGRectGetMaxY(self.vContainer.frame));
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:r
                                               byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                                     cornerRadii:CGSizeMake(5.0, 5.0)];
    CGFloat shadowSize = 3.0;
    UIImage *img = [UIView shadowImageWithBezierPath:path shadowSize:shadowSize];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [self.view insertSubview:imgView atIndex:0];
    
    CGRect fr = imgView.frame;
    fr.origin = CGPointMake(CGRectGetMinX(headView.frame) - shadowSize, CGRectGetMinY(headView.frame) - shadowSize);
    imgView.frame = fr;
    _shadowImage = imgView;
    
    if (_shadowUploadButton == nil)
    {
        [_shadowUploadButton removeFromSuperview];
        UIBezierPath *cancelPath = [UIBezierPath bezierPathWithRoundedRect:self.vBottom.bounds
                                                         byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight
                                                               cornerRadii:CGSizeMake(5.0, 5.0)];
        UIImage *cancelShadowImage = [UIView shadowImageWithBezierPath:cancelPath shadowSize:shadowSize];
        UIImageView *cancelImgView = [[UIImageView alloc] initWithImage:cancelShadowImage];
        [self.view insertSubview:cancelImgView belowSubview:self.vBottom];
        
        [self.vBottom pinShadowImageView:cancelImgView shadowSize:shadowSize];
        
        _shadowUploadButton = cancelImgView;
    }
}
#endif

#endif

- (void) updateUI
{
    if (![self isViewLoaded])
    {
        return;
    }
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    //    self.restrictedCheckBox.hidden = YES;
    //
    //    self.lcCheckboxHeight.constant = 0.0;
    
    NSMutableAttributedString* rtButtonString;
    NSMutableAttributedString* rtString;
    NSAttributedString *cr;
    NSAttributedString* lineOneString;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:@"unseenBonusFromBonusUsers"];
    NSData *myEncodedTurnoverObject = [defaults objectForKey:@"unseenTurnoverBonusFromBonusUsers"];
    
    turnoverFactorArray = [NSKeyedUnarchiver unarchiveObjectWithData:myEncodedTurnoverObject];
    NSDictionary *bonusUsersObject = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    AOBonusUsers *bonus = [[AOBonusUsers alloc] init];
    [bonus setValuesForKeysWithDictionary:bonusUsersObject];

    
    if([defaults boolForKey:@"bonusIsSingle"] == YES){
        
        self.bonusLabel.text = bonus.bonusDescriptionTxt;
        rtButtonString = [[[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"claimBonus")withTheme:theme
                                                              andAttributes:@{
                                                                              NSFontAttributeName: self.titleFont
                                                                              }  ] mutableCopy];
        
        [self.claimButton setAttributedTitle:rtButtonString forState:UIControlStateNormal];
        
        
        rtString = [[NSMutableAttributedString alloc] init];
        cr = [[NSAttributedString alloc] initWithString: @"\n"];
        
        
        lineOneString = [[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"newBonusInbox")
                                                           withTheme:theme
                                                       andAttributes:@{
                                                                       NSFontAttributeName: self.regularFont
                                                                       }  ] ;
        
        self.titleLabel.attributedText =  [[[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"newBonus")
                                                                              withTheme:theme
                                                                          andAttributes:@{
                                                                                          NSFontAttributeName: self.titleFont
                                                                                          }  ] mutableCopy];
        
        
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
        
        
        
        
        
        
        [rtString appendAttributedString:cr];
        [rtString appendAttributedString:lineOneString];
        //    [rtString appendAttributedString:lineOneAtributedString];
        
        
        
        self.mainLabel.attributedText = rtString;
        
    }else{
         self.singleBonusViewHeight.constant = 0;
        rtButtonString = [[[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"seeBonuses")withTheme:theme
                                                         andAttributes:@{
                                                                         NSFontAttributeName: self.titleFont
                                                                         }  ] mutableCopy];
        
        [self.claimButton setAttributedTitle:rtButtonString forState:UIControlStateNormal];
        
        
        rtString = [[NSMutableAttributedString alloc] init];
        cr = [[NSAttributedString alloc] initWithString: @"\n"];
        
        
        lineOneString = [[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"newBonusesInbox")
                                                           withTheme:theme
                                                       andAttributes:@{
                                                                       NSFontAttributeName: self.regularFont
                                                                       }  ] ;
        
        self.titleLabel.attributedText =  [[[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"newBonuses")
                                                                              withTheme:theme
                                                                          andAttributes:@{
                                                                                          NSFontAttributeName: self.titleFont
                                                                                          }  ] mutableCopy];
        
        
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
        
        
        
        
        
        
        [rtString appendAttributedString:cr];
        [rtString appendAttributedString:lineOneString];
        //    [rtString appendAttributedString:lineOneAtributedString];
        
        
        
        self.mainLabel.attributedText = rtString;
    
    
    }
    
    
    //AutoLayout fitting of the popUp
    CGSize size = [self.view systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    NSLog(@"%@",NSStringFromCGSize(size));
    
    CGRect fr = self.view.frame;
    fr.size.height = size.height;
    self.view.frame = fr;
#ifdef COPYOP
    [self p_updateShadow];
#endif
}






/* BAC - 1189 */
-(void) activateMyAccountButtonPressed {
 
    if(self.activateButtonPressedHandler) {
        self.activateButtonPressedHandler();
    }
    
}

- (IBAction)aoCloseButtonPressed:(id)sender {
    if(self.hideButtonPressedHandler) {
        self.hideButtonPressedHandler();
    }
}

@end
