 //
//  WithdrawBonusResetConfirmViewController.m
//  iOSApp
//
//  Created by  on 9/9/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "WithdrawBonusResetConfirmViewController.h"
#import "AOThemeController.h"
#import "NSAttributedString+RT.h"
#import "CheckBox.h"
#import "RestAPIFacade.h"
#import "AOBottomCorneredView.h"
#import "AOBottomCorneredLeftView.h"
#import "AOBottomCorneredRightView.h"

#import "AORegulationStatePopupViewController.h"
#import "AOThemeController.h"
#import "RegulationRestrictedKnowledgeCheckbox.h"


#import "COPopupHeaderView.h"
#ifdef COPYOP
#import "UIView+Shadows.h"
#endif

#ifdef ANYOPTION
static CGFloat popupWidth = 300.0;
#elif COPYOP
static CGFloat popupWidth = 280.0;
#endif

@interface WithdrawBonusResetConfirmViewController()
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (weak, nonatomic) IBOutlet COPopupHeaderView *headerView;

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *mainLabel;

@property (weak, nonatomic) IBOutlet UIView *vContainer;
@property (weak, nonatomic) IBOutlet AOBottomCorneredView *vBottom;
@property (weak, nonatomic) IBOutlet AOBottomCorneredLeftView *vLeftBottom;
@property (weak, nonatomic) IBOutlet AOBottomCorneredRightView *vRightBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcLeftButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcRightButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcHeaderViewHeight;
@end

@implementation WithdrawBonusResetConfirmViewController{
    BOOL restrictedKnowledge;
#ifdef COPYOP
    UIImageView *_shadowImage;
    UIImageView *_shadowUploadButtonLeft;
    UIImageView *_shadowUploadButtonRight;
#endif
}


-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.mainLabel.adelegate = self;
    
#ifdef COPYOP
    self.vBottom.hidden = NO;
    
//    self.vLeftBottom.layer.masksToBounds = YES;
//    self.vRightBottom.layer.masksToBounds = YES;
    
    
    [self.okButton addTarget:self action:@selector(okButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton addTarget:self action:@selector(cancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    /* CORE - 2281 */
    
    
    self.headerView.hidden = NO;
    
    self.view.backgroundColor = [UIColor clearColor];
    self.headerView.showsSeparatorLine = NO;
    self.vLeftBottom.backgroundColor = [UIColor clearColor];
    self.vRightBottom.backgroundColor = [UIColor clearColor];
    
    self.vContainer.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    

#endif
    
    self.view.backgroundColor = [UIColor clearColor];
    
    [self updateUI];
    
}
#ifdef COPYOP
#ifdef COPYOP
-(IBAction)yesButtonTapped:(id)sender {
    if(self.yesPressedHandler != nil) {
        self.yesPressedHandler();
    }
}

-(IBAction)noButtonTapped:(id)sender {
    if(self.noPressedHandler != nil) {
        self.noPressedHandler();
    }
}
#endif

#endif

#ifdef COPYOP

-(void)okButtonPressed {
    
    if (self.closeButtonPressedHandler != nil)
    {
        self.closeButtonPressedHandler();
    }
}
-(void)cancelButtonPressed{
    
//    if (self.hideButtonPressedHandler != nil)
//    {
//        self.hideButtonPressedHandler();
//    }
    
}

#ifdef COPYOP
- (void) p_updateShadow
{
    [self.view layoutIfNeeded];
    
    [_shadowImage removeFromSuperview];
    
    UIView *headView = self.headerView;
    CGRect r = CGRectMake(CGRectGetMinX(headView.frame),
                          CGRectGetMinY(headView.frame),
                          CGRectGetWidth(headView.bounds),
                          CGRectGetMaxY(self.vContainer.frame));
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:r
                                               byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                                     cornerRadii:CGSizeMake(5.0, 5.0)];
    CGFloat shadowSize = 3.0;
    UIImage *img = [UIView shadowImageWithBezierPath:path shadowSize:shadowSize];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [self.view insertSubview:imgView atIndex:0];
    
    CGRect fr = imgView.frame;
    fr.origin = CGPointMake(CGRectGetMinX(headView.frame) - shadowSize, CGRectGetMinY(headView.frame) - shadowSize);
    imgView.frame = fr;
    _shadowImage = imgView;
    
    if (_shadowUploadButtonLeft == nil)
    {
        [_shadowUploadButtonLeft removeFromSuperview];
        UIBezierPath *cancelPath = [UIBezierPath bezierPathWithRoundedRect:self.vLeftBottom.bounds
                                                         byRoundingCorners:UIRectCornerBottomLeft
                                                               cornerRadii:CGSizeMake(5.0, 5.0)];
        UIImage *cancelShadowImage = [UIView shadowImageWithBezierPath:cancelPath shadowSize:shadowSize];
        UIImageView *cancelImgView = [[UIImageView alloc] initWithImage:cancelShadowImage];
        [self.view insertSubview:cancelImgView belowSubview:self.vLeftBottom];
        
        [self.vLeftBottom pinShadowImageView:cancelImgView shadowSize:shadowSize];
        
        _shadowUploadButtonLeft = cancelImgView;
    }
    
    if (_shadowUploadButtonRight == nil)
    {
        [_shadowUploadButtonRight removeFromSuperview];
        UIBezierPath *cancelPath = [UIBezierPath bezierPathWithRoundedRect:self.vRightBottom.bounds
                                                         byRoundingCorners:UIRectCornerBottomRight
                                                               cornerRadii:CGSizeMake(5.0, 5.0)];
        UIImage *cancelShadowImage = [UIView shadowImageWithBezierPath:cancelPath shadowSize:shadowSize];
        UIImageView *cancelImgView = [[UIImageView alloc] initWithImage:cancelShadowImage];
        [self.view insertSubview:cancelImgView belowSubview:self.vRightBottom];
        
        [self.vRightBottom pinShadowImageView:cancelImgView shadowSize:shadowSize];
        
        _shadowUploadButtonRight = cancelImgView;
    }
}
#endif

#endif

- (void) updateUI
{
    if (![self isViewLoaded])
    {
        return;
    }
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
#ifdef ANYOPTION
    [self.okButton setHidden:YES];
    [self.cancelButton setHidden:YES];
    self.lcLeftButtonHeight.constant = 0.0;
    self.lcRightButtonHeight.constant = 0.0;
    self.lcHeaderViewHeight.constant = 0.0;
    [self.headImageView setImage:[UIImage imageNamed:@"withdrawal_reset_bonus"]];
    self.vContainer.backgroundColor = [UIColor clearColor];
#elif COPYOP
    [self.headImageView setImage:[UIImage imageNamed:@"withdrawal_reset_bonus"]];
#endif
    
    NSMutableAttributedString* rtString = [[[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"withdrawBonusReset") withTheme:theme andAttributes:@{NSFontAttributeName:[theme withdrawLightFont]}  ] mutableCopy];
    
    self.mainLabel.attributedText = rtString;
           
    
    CGSize size = [self.view systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    CGRect fr = self.view.frame;
    fr.size.height = size.height;
    self.view.frame = fr;
    
    
    
#ifdef COPYOP
    [self p_updateShadow];
#endif
}



#pragma mark -
#pragma mark  AOAttributedLabel delegate

-(void)tappedAttributes:(NSDictionary*)attributes{
    
    if ([attributes objectForKey:@"action"]!= nil && [attributes objectForKey:@"action"]!=[NSNull null] && ![[attributes objectForKey:@"action"] isEmpty]) {
        
        NSArray* actionAttribute = [[attributes objectForKey:@"action"] componentsSeparatedByString:@"_"];
        if (!actionAttribute.count || [actionAttribute[1] isEmpty]) {
            return;
        }
        
        [[MAINVC popupVC] hide];
        [RESIDENT goTo:actionAttribute[1] withOptions:nil];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

    
}



#pragma mark -
#pragma mark Checkbox Delegate

-(void)checkBoxValueChanged:(CheckBox *)checkbox {
    restrictedKnowledge = checkbox.value;
}
@end
