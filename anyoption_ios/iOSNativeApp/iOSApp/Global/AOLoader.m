//
//  AOLoader.m
//  LoaderTest
//
//  Created by Antoan Tateosyan on 8/25/15.
//  Copyright (c) 2015 SmileApps. All rights reserved.
//

#import "AOLoader.h"

@implementation AOLoader
{
    CAReplicatorLayer *_repLayer;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    [self setup];
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self setup];
    return self;
}

- (void) setup
{
    self.backgroundColor = [UIColor clearColor];
    
    [_repLayer removeFromSuperlayer];

    if (CGRectIsEmpty(self.bounds))
    {
        return;
    }
    
    CAReplicatorLayer *rep = [CAReplicatorLayer layer];
    rep.frame = self.bounds;
    rep.backgroundColor = [UIColor clearColor].CGColor;
    [self.layer addSublayer:rep];
    
    CALayer *layer = [CALayer layer];
    layer.bounds = CGRectMake(0.0, 0.0, 1.0, CGRectGetHeight(rep.bounds) / 8.0);
    layer.backgroundColor = [UIColor whiteColor].CGColor;
    layer.position = CGPointMake(CGRectGetWidth(rep.bounds) / 2.0, CGRectGetHeight(layer.bounds) / 2.0);
    [rep addSublayer:layer];
    
    NSTimeInterval duration = 1.0;
    rep.instanceCount = 22;
    rep.instanceTransform = CATransform3DMakeRotation(2.0 * M_PI / rep.instanceCount, 0.0, 0.0, 1.0);
    rep.instanceDelay = duration / rep.instanceCount;
    layer.opacity = 0.2;
    
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:NSStringFromSelector(@selector(opacity))];
    anim.fromValue = @1.0;
    anim.toValue = @0.2;
    anim.duration = duration;
    anim.repeatCount = FLT_MAX;
    [layer addAnimation:anim forKey:nil];
}

- (void) setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    
    [self setup];
}

@end
