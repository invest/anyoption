//
//  LowBalancePopupViewController.m
//  iOSApp
//
//  Created by Kaloyan Petrov on 2/16/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import "LowBalancePopupViewController.h"
#import "AORegulationStatePopupViewController.h"
#import "AOThemeController.h"
#import "NSAttributedString+RT.h"
#import "CheckBox.h"
#import "RestAPIFacade.h"
#import "AOBottomCorneredView.h"
#import "RegulationRestrictedKnowledgeCheckbox.h"
#import "COPopupHeaderView.h"
#ifdef COPYOP
#import "UIView+Shadows.h"
#endif

#ifdef ANYOPTION
static CGFloat popupWidth = 300.0;
#elif COPYOP
static CGFloat popupWidth = 280.0;
#endif

@interface LowBalancePopupViewController ()<CheckBoxDelegate>

@property (weak, nonatomic) IBOutlet UIButton *claimButton;

@property (weak, nonatomic) IBOutlet COPopupHeaderView *headerView;


@property (weak, nonatomic) IBOutlet UIButton *aoCloseButton;


@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *mainLabel;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *secondLabel;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *thirdLabel;


@property (weak, nonatomic) IBOutlet AOAttributedLabel *titleLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_labelTop;

@property (weak, nonatomic) IBOutlet UIView *vContainer;
@property (weak, nonatomic) IBOutlet AOBottomCorneredView *vBottom;
@property (weak, nonatomic) IBOutlet AOBottomCorneredView *vbonusLabelBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcCheckboxHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcOkButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcHeaderViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iPhoneVersionConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iPhoneVesrionImageH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iPhoneVersionImageW;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewBottomHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeaderHeight;

//Image size constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headImageW;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headImageH;

@property (nonatomic, strong) UIFont *regularFont;
@property (nonatomic, strong) UIFont *boldFont;
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic) BOOL activateButoonIsClicked;



@end

@implementation LowBalancePopupViewController{
    
#ifdef COPYOP
    UIImageView *_shadowImage;
    UIImageView *_shadowUploadButton;
#endif
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.activateButoonIsClicked = NO;
    
    
    
    
#ifndef COPYOP
    self.viewBottomHeight.constant = 0;
    self.viewHeaderHeight.constant = 0;
    
#else
    
    [self.aoCloseButton removeFromSuperview];
#endif
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    if(CGRectGetHeight([UIScreen mainScreen].bounds) <= 480) {
        
        self.regularFont = [UIFont fontWithName:[theme defaultFontName] size:16.0];
        self.boldFont = [UIFont fontWithName:[theme defaultFontNameBold] size:20.0];
        self.titleFont = [UIFont fontWithName:[theme defaultFontNameBold] size:20.0];
        
        
    }else{
        
        self.regularFont = [UIFont fontWithName:[theme defaultFontName] size:18.0];
        self.boldFont = [UIFont fontWithName:[theme defaultFontNameBold] size:20.0];
        self.titleFont = [UIFont fontWithName:[theme defaultFontNameBold] size:22.0];
        
    }
    
#ifdef COPYOP
    [self.claimButton setTintColor:[theme updateInfoHeadingTextColor]];
#endif
  
    [self.claimButton setTitle:AOLocalizedString(@"profitLine.deposit") forState:UIControlStateNormal];
    [self.claimButton addTarget:self action:@selector(activateMyAccountButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    NSMutableAttributedString* rtString = [[[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"profitLine.deposit") withTheme:theme
                                                                          andAttributes:@{
                                                                                          NSFontAttributeName: self.titleFont
                                                                                          }  ] mutableCopy];
    //  NSMutableString *activateButtonTitle = [[NSMutableString alloc]initWithFormat:@"%@",rtString];
    //    [self.activateButton setTitle: activateButtonTitle forState: UIControlStateNormal];
    [self.claimButton setAttributedTitle:rtString forState:UIControlStateNormal];
    
#ifdef COPYOP
    self.vBottom.hidden = NO;
    //Changing the button text depending on the langauge
    
    
    
    self.headerView.hidden = NO;
    
    self.view.backgroundColor = [UIColor clearColor];
    self.headerView.showsSeparatorLine = NO;
    self.vBottom.backgroundColor = [UIColor clearColor];
    
    self.vContainer.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    
    [self.claimButton setTitleColor:[UIColor colorWithRed:234.0/255.0 green:85.0/255.0 blue:58.0/255.0 alpha:1.0] forState:UIControlStateNormal];
#endif
    
    self.view.backgroundColor = [UIColor clearColor];
    
    [self updateUI];
    
    
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if(CGRectGetHeight([UIScreen mainScreen].bounds) <= 480) {
        self.iPhoneVersionConstraint.constant = 0.0f;
        self.iPhoneVersionImageW.constant = 75.0f;
        self.iPhoneVesrionImageH.constant = 75.0f;
        
    } else {
        self.iPhoneVersionConstraint.constant = 0.0f;
        self.iPhoneVersionImageW.constant = 90.0f;
        self.iPhoneVesrionImageH.constant = 90.0f;
    }
}

#ifdef COPYOP

-(void)okButtonPressed:(id)sender {
    if (self.closeButtonPressedHandler != nil)
    {
        self.closeButtonPressedHandler();
    }
}

#ifdef COPYOP
- (void) p_updateShadow
{
    [self.view layoutIfNeeded];
    
    [_shadowImage removeFromSuperview];
    
    UIView *headView = self.headerView;
    CGRect r = CGRectMake(CGRectGetMinX(headView.frame),
                          CGRectGetMinY(headView.frame),
                          CGRectGetWidth(headView.bounds),
                          CGRectGetMaxY(self.vContainer.frame));
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:r
                                               byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                                     cornerRadii:CGSizeMake(5.0, 5.0)];
    CGFloat shadowSize = 3.0;
    UIImage *img = [UIView shadowImageWithBezierPath:path shadowSize:shadowSize];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [self.view insertSubview:imgView atIndex:0];
    
    CGRect fr = imgView.frame;
    fr.origin = CGPointMake(CGRectGetMinX(headView.frame) - shadowSize, CGRectGetMinY(headView.frame) - shadowSize);
    imgView.frame = fr;
    _shadowImage = imgView;
    
    if (_shadowUploadButton == nil)
    {
        [_shadowUploadButton removeFromSuperview];
        UIBezierPath *cancelPath = [UIBezierPath bezierPathWithRoundedRect:self.vBottom.bounds
                                                         byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight
                                                               cornerRadii:CGSizeMake(5.0, 5.0)];
        UIImage *cancelShadowImage = [UIView shadowImageWithBezierPath:cancelPath shadowSize:shadowSize];
        UIImageView *cancelImgView = [[UIImageView alloc] initWithImage:cancelShadowImage];
        [self.view insertSubview:cancelImgView belowSubview:self.vBottom];
        
        [self.vBottom pinShadowImageView:cancelImgView shadowSize:shadowSize];
        
        _shadowUploadButton = cancelImgView;
    }
}
#endif

#endif

- (void) updateUI
{
    if (![self isViewLoaded])
    {
        return;
    }
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    //    self.restrictedCheckBox.hidden = YES;
    //
    //    self.lcCheckboxHeight.constant = 0.0;
    
    
#ifdef ANYOPTION
    self.headImageW.constant = 130.0;
    self.headImageH.constant = 130.0;
    self.lcOkButtonHeight.constant = 0.0;
    [self.headImageView setImage:[UIImage imageNamed:@"withdrawal_reset_bonus"]];
    self.vContainer.backgroundColor = [UIColor clearColor];
#elif COPYOP
    [self.headImageView setImage:[UIImage imageNamed:@"co_withdrawal_reset_bonus"]];
#endif
    //Populating strings for BAC-1189
    //First line
    //    UIFont *normalFont = [UIFont fontWithName:[theme defaultFontName] size:16.0];
    //    UIFont *boldFont = [UIFont fontWithName:[theme defaultFontNameBold] size:16.0];
    //    UIFont *boldFontLarge = [UIFont fontWithName:[theme defaultFontNameBold] size:20.0];
    
    
     NSAttributedString *cr = [[NSAttributedString alloc] initWithString: @"\n"];
    NSMutableAttributedString* rtString = [[NSMutableAttributedString alloc]init];
   
    
    CGFloat separator = 100.0f;
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    CGFloat minInvestAmount = (CGFloat)delegate.minInvestmentAmount;
    

    /* CORE - 2432 */
    NSNumber *minInvestAmountNumber =[NSNumber numberWithDouble:(minInvestAmount / separator)];
    
    
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    
    NSString *formattedMinInvestment = [formatter stringFromNumber:minInvestAmountNumber];
    /* CORE - 2432 */
    
    NSAttributedString *minInvestAmountString = nil;
    
    if([DLG user].currency.isLeftSymbol) {
        minInvestAmountString = [[NSAttributedString alloc] initWithRTString:[NSString stringWithFormat:@"%@%@", [DLG user].currency.symbol, formattedMinInvestment]
                                                                    withTheme:theme
                                                                andAttributes:@{
                                                                                NSFontAttributeName: self.boldFont
                                                                                }];

    } else {
        minInvestAmountString = [[NSAttributedString alloc] initWithRTString:[NSString stringWithFormat:@"%@%@",  formattedMinInvestment,[DLG user].currency.symbol]
                                                                   withTheme:theme
                                                               andAttributes:@{
                                                                               NSFontAttributeName: self.boldFont
                                                                               }];
    }
    
    
    
    NSMutableAttributedString* lineOneAtributedString = [[NSMutableAttributedString alloc] initWithRTString:AOLocalizedString(@"minInvestAmount") withTheme:theme
                                                                                               andAttributes: @{
                                                                                                                NSFontAttributeName: self.regularFont
                                                                                                                }];
    [lineOneAtributedString appendAttributedString:minInvestAmountString];
    

#ifdef ANYOPTION
   NSMutableAttributedString *insufficientFunds = [[[NSAttributedString alloc] initWithRTString:             AOLocalizedString(@"insufficientFunds")
                                                                        withTheme:theme
                                                                    andAttributes:@{
                                                                                    NSFontAttributeName: self.boldFont
                                                                                    }  ] mutableCopy];
   
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:4];
    [style setAlignment:NSTextAlignmentCenter];
    [rtString appendAttributedString:insufficientFunds];
  
    [rtString appendAttributedString:cr];
    [rtString appendAttributedString:lineOneAtributedString];
    [rtString addAttribute:NSParagraphStyleAttributeName
                     value:style
                     range:NSMakeRange(0, rtString.length)];

    
#endif
    
#ifdef COPYOP
    [rtString appendAttributedString:cr];
    
    [rtString appendAttributedString:lineOneAtributedString];

#endif
    
    
    
    self.mainLabel.attributedText = rtString;
    self.titleLabel.attributedText = [[[NSAttributedString alloc] initWithRTString:             AOLocalizedString(@"insufficientFunds")
                                                                         withTheme:theme
                                                                     andAttributes:@{
                                                                                     NSFontAttributeName: self.titleFont
                                                                                     }  ] mutableCopy];
     [self.titleLabel setTextColor:[theme updateInfoHeadingTextColor]];
    
    //AutoLayout fitting of the popUp
    CGSize size = [self.view systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    NSLog(@"%@",NSStringFromCGSize(size));
    
    CGRect fr = self.view.frame;
    fr.size.height = size.height;
    self.view.frame = fr;
#ifdef COPYOP
    [self p_updateShadow];
#endif
}




#pragma mark -
#pragma mark  AOAttributedLabel delegate

//-(void)tappedAttributes:(NSDictionary*)attributes{
//
//    if ([attributes objectForKey:@"action"]!= nil && [attributes objectForKey:@"action"]!=[NSNull null] && ![[attributes objectForKey:@"action"] isEmpty]) {
//
//        NSArray* actionAttribute = [[attributes objectForKey:@"action"] componentsSeparatedByString:@"_"];
//        if (!actionAttribute.count || [actionAttribute[1] isEmpty]) {
//            return;
//        }
//
//        [[MAINVC popupVC] hide];
//        [RESIDENT goTo:actionAttribute[1] withOptions:nil];
//    }
//}

#pragma mark -
#pragma mark Checkbox Delegate

//-(void)checkBoxValueChanged:(CheckBox *)checkbox {
//    self.restrictedKnowledge = checkbox.value;
//}
//


/* BAC - 1189 */
-(void) activateMyAccountButtonPressed {
    
    [RESIDENT goTo:@"depositMenu" withOptions:nil];
    NSLog(@"\nGO TO DEPOSIT MENU!\n");
    if(self.activateButtonPressedHandler) {
        self.activateButtonPressedHandler();
    }
    

}

- (IBAction)aoCloseButtonPressed:(id)sender {
    if(self.hideButtonPressedHandler) {
        self.hideButtonPressedHandler();
    }
}

@end
