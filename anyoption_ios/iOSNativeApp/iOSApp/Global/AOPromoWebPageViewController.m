//
//  AOPromoWebPageViewController.m
//  iOSApp
//
//  Created by Vladimir Gradev on 11/27/15.
//  Copyright © 2015 Anyoption. All rights reserved.
//

#import "AOPromoWebPageViewController.h"
#import "UIView+Disabler.h"

@implementation AOPromoWebPageViewController{
        NSString *urlToLoad;
        NSNumber *promoCombId;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
#ifdef ANYOPTION
    urlToLoad = [NSString stringWithFormat:@"%@?pageR=%@&s=%li&p=ao",RESIDENT.DEEPLINKS_CDN_URL,self.navOptions[@"pageR"][@"pageR"],[DLG user].skinId];
#elif COPYOP
    urlToLoad = [NSString stringWithFormat:@"%@?pageR=%@&s=%li&p=co",RESIDENT.DEEPLINKS_CDN_URL,self.navOptions[@"pageR"][@"pageR"],[DLG user].skinId];
    
#endif    
    
    promoCombId = self.navOptions[@"pageR"][@"combid"];
    [self.navigationController setNavigationBarHidden:YES];
    [self showUrl];
}

-(void)startLoading {
    [activityIndicator startAnimating];
    [self.view addDisablerWithColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.1] andTag:999];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *currentLang = [defaults objectForKey:@"currentLang"];
    
    if ([currentLang isEqualToString:@"de"])
    {
        [self buildBackButtonWithText:nil];
    }
}

-(void)stopLoading {
    activityIndicator.hidden = YES;
    [self.view removeDisablerWithTag:999];
}

- (void)showUrl {
 
    webView.hidden = NO;
    webView.delegate = self;
    NSURL *url = [NSURL URLWithString: urlToLoad];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: url];
    [webView loadRequest: request];
    
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
    [self startLoading];
    [self.view bringSubviewToFront:activityIndicator];
}

-(void)webViewDidFinishLoad:(UIWebView *)wView {
    [self stopLoading];
    [self.view sendSubviewToBack:activityIndicator];
    
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    // NSLog(@"web view error.");
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if(![request.URL.absoluteString containsString:@"deeplinks"]) {
        
#ifdef ANYOPTION
        
        [RESIDENT goTo:@"login" withOptions:@{@"account":@YES,@"marketing":@YES,@"combId":promoCombId}]; // qwertyuiop
        
#elif COPYOP
        
        [RESIDENT goTo:@"newAccountFull" withOptions:@{@"account":@YES,@"marketing":@YES,@"combId":promoCombId}];
        
#endif
        /* AR - 1502 */
        NSUserDefaults *marketingPageUserDefaults = [NSUserDefaults standardUserDefaults];
        [marketingPageUserDefaults setObject:@"YES" forKey:@"comeFromPromoWebPage"];
         /* AR - 1502 */
        
        return NO;
        
    }
    
    return YES;
}

@end
