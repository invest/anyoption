//
//  AOShowUserBonusesPopUpViewController.m
//  iOSApp
//
//  Created by Kaloyan Petrov on 2/17/16.
//  Copyright © 2016 Anyoption. All rights reserved.
//

#import "AOShowUserBonusesPopUpViewController.h"
#import "AORegulationStatePopupViewController.h"
#import "AOThemeController.h"
#import "NSAttributedString+RT.h"
#import "CheckBox.h"
#import "RestAPIFacade.h"
#import "AOBottomCorneredView.h"
#import "RegulationRestrictedKnowledgeCheckbox.h"
#import "COPopupHeaderView.h"
#import "AOBonusUsers.h"
#ifdef COPYOP
#import "UIView+Shadows.h"
#endif

#ifdef ANYOPTION
static CGFloat popupWidth = 300.0;
#elif COPYOP
static CGFloat popupWidth = 280.0;
#endif

@interface AOShowUserBonusesPopUpViewController ()<CheckBoxDelegate>
@property (weak, nonatomic) IBOutlet UIButton *claimButton;
@property (strong, nonatomic) IBOutlet UIView *mainVIewContainer;

@property (weak, nonatomic) IBOutlet COPopupHeaderView *headerView;
@property (weak, nonatomic) IBOutlet UIView *separatorLineTop;


@property (weak, nonatomic) IBOutlet UIButton *coCloseButton;
@property (weak, nonatomic) IBOutlet UIButton *aoCloseButton;

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *mainLabel;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *secondLabel;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *thirdLabel;

@property (weak, nonatomic) IBOutlet AOAttributedLabel *bonusLabel;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *titleLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_labelTop;

@property (weak, nonatomic) IBOutlet UIView *vContainer;
@property (weak, nonatomic) IBOutlet AOBottomCorneredView *vBottom;
@property (weak, nonatomic) IBOutlet UIView *vbonusLabelBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bonusSingleCellHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcOkButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcHeaderViewHeight;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iPhoneVersionConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iPhoneVesrionImageH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iPhoneVersionImageW;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewBottomHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeaderHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewBonusBottomSpecialHeight;

@property (nonatomic, strong) UIFont *regularFont;
@property (nonatomic, strong) UIFont *boldFont;
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic) BOOL activateButoonIsClicked;


@end

@implementation AOShowUserBonusesPopUpViewController{


    
#ifdef COPYOP
    UIImageView *_shadowImage;
    UIImageView *_shadowUploadButton;
#endif
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.activateButoonIsClicked = NO;
    
    
    //    id<AOAppTheme> themeAO = [AOThemeController sharedInstance].theme;
    //    self.headerView.backgroundColor = [themeAO appBackgroundColor];
    
    
    
#ifndef COPYOP
    self.viewBottomHeight.constant = 0;
    
#else
    [self.aoCloseButton removeFromSuperview];
#endif
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    
    if(CGRectGetHeight([UIScreen mainScreen].bounds) <= 480) {
        
        self.regularFont = [UIFont fontWithName:[theme defaultFontNameMedium] size:16.0];
        self.boldFont = [UIFont fontWithName:[theme defaultFontNameBold] size:12.0];
        self.titleFont = [UIFont fontWithName:[theme defaultFontNameBold] size:18.0];
        
        
    }else{
        
        self.regularFont = [UIFont fontWithName:[theme defaultFontNameMedium] size:16.0];
        self.boldFont = [UIFont fontWithName:[theme defaultFontNameBold] size:14.0];
        self.titleFont = [UIFont fontWithName:[theme defaultFontNameBold] size:20.0];
        
    }
    
#ifdef COPYOP
    [self.titleLabel setTextColor:[theme updateInfoHeadingTextColor]];
    
    [self.claimButton setTintColor:[theme updateInfoHeadingTextColor]];
#endif
    
    

    
#ifdef COPYOP
    self.vBottom.hidden = NO;
    //Changing the button text depending on the langauge
    
    
    
    self.headerView.hidden = NO;
    
    self.view.backgroundColor = [UIColor clearColor];
    self.headerView.showsSeparatorLine = NO;
    self.vBottom.backgroundColor = [UIColor clearColor];
    
    self.vContainer.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    
    [self.claimButton setTitleColor:[UIColor colorWithRed:234.0/255.0 green:85.0/255.0 blue:58.0/255.0 alpha:1.0] forState:UIControlStateNormal];
#endif
    
    self.view.backgroundColor = [UIColor clearColor];
    
    [self updateUI];
    
    
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if(CGRectGetHeight([UIScreen mainScreen].bounds) <= 480) {
        self.iPhoneVersionConstraint.constant = 0.0f;
        self.iPhoneVersionImageW.constant = 75.0f;
        self.iPhoneVesrionImageH.constant = 75.0f;
        
    } else {
        self.iPhoneVersionConstraint.constant = 0.0f;
        self.iPhoneVersionImageW.constant = 90.0f;
        self.iPhoneVesrionImageH.constant = 90.0f;
    }
}

#ifdef COPYOP

-(void)okButtonPressed:(id)sender {
    if (self.closeButtonPressedHandler != nil)
    {
        self.closeButtonPressedHandler();
    }
}

#ifdef COPYOP
- (void) p_updateShadow
{
    [self.view layoutIfNeeded];
    
    [_shadowImage removeFromSuperview];
    
    UIView *headView = self.headerView;
    CGRect r = CGRectMake(CGRectGetMinX(headView.frame),
                          CGRectGetMinY(headView.frame),
                          CGRectGetWidth(headView.bounds),
                          CGRectGetMaxY(self.vContainer.frame));
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:r
                                               byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                                     cornerRadii:CGSizeMake(5.0, 5.0)];
    CGFloat shadowSize = 3.0;
    UIImage *img = [UIView shadowImageWithBezierPath:path shadowSize:shadowSize];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [self.view insertSubview:imgView atIndex:0];
    
    CGRect fr = imgView.frame;
    fr.origin = CGPointMake(CGRectGetMinX(headView.frame) - shadowSize, CGRectGetMinY(headView.frame) - shadowSize);
    imgView.frame = fr;
    _shadowImage = imgView;
    
    if (_shadowUploadButton == nil)
    {
        [_shadowUploadButton removeFromSuperview];
        UIBezierPath *cancelPath = [UIBezierPath bezierPathWithRoundedRect:self.vBottom.bounds
                                                         byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight
                                                               cornerRadii:CGSizeMake(5.0, 5.0)];
        UIImage *cancelShadowImage = [UIView shadowImageWithBezierPath:cancelPath shadowSize:shadowSize];
        UIImageView *cancelImgView = [[UIImageView alloc] initWithImage:cancelShadowImage];
        [self.view insertSubview:cancelImgView belowSubview:self.vBottom];
        
        [self.vBottom pinShadowImageView:cancelImgView shadowSize:shadowSize];
        
        _shadowUploadButton = cancelImgView;
    }
}
#endif

#endif

- (void) updateUI
{
    if (![self isViewLoaded])
    {
        return;
    }
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    

    [self.claimButton addTarget:self action:@selector(activateMyAccountButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    NSMutableAttributedString* buttonRtString;
    
    NSMutableAttributedString* rtString;
    
    NSAttributedString *popUpTitle;
    
    NSAttributedString *cr = [[NSAttributedString alloc] initWithString: @"\n"];
    
    NSAttributedString* lineOneString;


    [self.claimButton setAttributedTitle:buttonRtString forState:UIControlStateNormal];
//    self.viewLabelHeaderHeight.constant = 0.0;
    self.viewHeaderHeight.constant = 72.0;
    self.lcOkButtonHeight.constant = 0.0;
    
    
    //    [self.headImageView setImage:[UIImage imageNamed:@"AOblocked_account_icon"]];
    self.vContainer.backgroundColor = [UIColor clearColor];


    
//    [bonusDefaults setBool:bonusIsSingle forKey:@"bonusIsSingle"];
//    [bonusDefaults setObject:bonusFromBonusUsers forKey:@"unseenBonusFromBonusUsers"];
//      UIColor *fillColor = [[AOThemeController sharedInstance].theme appBackgroundColor];
    self.vbonusLabelBottom.backgroundColor = [UIColor colorWithHexString:@"#eff5e3"];

    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:@"unseenBonusFromBonusUsers"];
    NSDictionary *bonusUsersObject = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    [defaults objectForKey:@"bonusIsSingle"];
    AOBonusUsers *bonus = [[AOBonusUsers alloc] init];
    [bonus setValuesForKeysWithDictionary:bonusUsersObject];
    
//    "newBonuses" = "New Bonuses";
//    "newBonusesInbox" = "You have new bonuses waiting for you in your bonus inbox";
//    "seeBonuses" = "See Bonuses";
//    
//    "newBonus" = "New Bonus";
//    "newBonusInbox" = "You have a new bonus waiting for you in your bonus inbox";
//    "claimBonus" = "Claim Bonus";
    
    //If the bonus object is only one we populate the Popup with different objects
    if ([defaults boolForKey:@"bonusIsSingle"] == NO) {
        self.bonusSingleCellHeight.constant = 0;
        [self.vBottom removeFromSuperview];
        [self.claimButton removeFromSuperview];
        rtString = [[NSMutableAttributedString alloc] init];
        popUpTitle = [[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"newBonuses")
                                                                            withTheme:theme
                                                                        andAttributes:@{
                                                                                        NSFontAttributeName: self.titleFont
                                                                                        }  ] ;
        lineOneString = [[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"newBonusesInbox")
                                                           withTheme:theme
                                                       andAttributes:@{
                                                                       NSFontAttributeName: self.regularFont
                                                                       }  ] ;

    }else{
        self.bonusLabel.text = bonus.bonusDescriptionTxt;
        [self.vBottom removeFromSuperview];
        [self.claimButton removeFromSuperview];
        self.viewBonusBottomSpecialHeight.constant = 0;
    
        rtString = [[NSMutableAttributedString alloc] init];
        popUpTitle = [[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"newBonus")
                                                                            withTheme:theme
                                                                        andAttributes:@{
                                                                                        NSFontAttributeName: self.titleFont
                                                                                        }  ] ;
        lineOneString = [[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"newBonusInbox")
                                                           withTheme:theme
                                                       andAttributes:@{
                                                                       NSFontAttributeName: self.regularFont
                                                                       }  ] ;

    }

    
    
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    


    [rtString appendAttributedString:popUpTitle];
    [rtString appendAttributedString:cr];
    [rtString appendAttributedString:cr];
    [rtString appendAttributedString:lineOneString];
    //    [rtString appendAttributedString:lineOneAtributedString];
    
    
    
    self.mainLabel.attributedText = rtString;
    self.titleLabel.attributedText =  [[[NSAttributedString alloc] initWithRTString:AOLocalizedString(@"newBonus")
                                                                          withTheme:theme
                                                                      andAttributes:@{
                                                                                      NSFontAttributeName: self.titleFont
                                                                                      }  ] mutableCopy];
    
    //AutoLayout fitting of the popUp
    CGSize size = [self.view systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    NSLog(@"%@",NSStringFromCGSize(size));
    
    CGRect fr = self.view.frame;
    fr.size.height = size.height;
    self.view.frame = fr;
#ifdef COPYOP
    [self p_updateShadow];
#endif
}



/* BAC - 1189 */
-(void) activateMyAccountButtonPressed {
    
    if (self.activateButoonIsClicked == NO) {
        
        self.activateButoonIsClicked = YES;
        NSLog(@"TRESHOLD BLOCKED USER!!!!!!: %@ :",[DLG user]);
        
        [[RestAPIFacade sharedInstance] updateCysec:[DLG user] withCompletion:^(AOResponse *response) {
            if ([response.parsedResponce[@"errorCode"]intValue] == 0) {
                
                [DLG userRegulation].thresholdBlock = -1;
                [DLG user].isSafeMode = -1;
                NSLog(@"RELOADED USER!!!!!!: %@ :",[DLG user]);
                
                if(self.activateButtonPressedHandler) {
                    self.activateButtonPressedHandler();
                }
            } else {
                self.activateButoonIsClicked = NO;
                
                //                if(self.activateButtonPressedHandler) {
                //                    self.activateButtonPressedHandler();
                //                }
            }
        }];
        
    }
    
}

- (IBAction)aoCloseButtonPressed:(id)sender {
    
    if(self.hideButtonPressedHandler) {
        self.hideButtonPressedHandler();
    }
}

@end
