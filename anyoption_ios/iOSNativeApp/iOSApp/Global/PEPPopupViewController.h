//
//  PEPPopupViewController.h
//  iOSApp
//
//  Created by Lyubomir Marinov on 11/26/15.
//  Copyright © 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AOAttributedLabel.h"
#ifdef COPYOP
typedef void (^COModalPopupViewControllerCloseHandler)(void);
#endif

@interface PEPPopupViewController : UIViewController <AOAttributedLabelDelegate>

#ifdef COPYOP
@property (nonatomic, copy) COModalPopupViewControllerCloseHandler closeButtonPressedHandler;
#endif

@end
