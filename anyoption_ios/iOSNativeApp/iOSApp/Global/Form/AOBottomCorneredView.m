//
//  AOBottomCorneredView.m
//  iOSApp
//
//  Created by Antoan Tateosyan on 9/7/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AOBottomCorneredView.h"

#import "AOThemeController.h"
#ifdef COPYOP
#import "AOCopyOpTheme.h"
#endif

@implementation AOBottomCorneredView

- (id) initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        self.contentMode = UIViewContentModeRedraw;
    }
    
    return self;
}

- (void) drawRect:(CGRect)rect
{
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                               byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight
                                                     cornerRadii:CGSizeMake(5.0, 5.0)];
    [path addClip];
    
    UIColor *fillColor;
#ifdef COPYOP
    fillColor = [(AOCopyOpTheme *)[AOThemeController sharedInstance].theme popupViewBackgroundColor];
#else
    fillColor = [[AOThemeController sharedInstance].theme appBackgroundColor];
#endif
    
    [fillColor setFill];
    [path fill];
}

@end
