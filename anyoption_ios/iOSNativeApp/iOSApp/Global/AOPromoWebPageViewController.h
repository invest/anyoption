//
//  AOPromoWebPageViewController.h
//  iOSApp
//
//  Created by Vladimir Gradev on 11/27/15.
//  Copyright © 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AOBaseViewController.h"

@interface AOPromoWebPageViewController : AOBaseViewController<UIWebViewDelegate> {
    __weak IBOutlet UIWebView *webView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
}
@end
