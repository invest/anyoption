//
//  AOMainPopupViewController.m
//  iOSApp
//
//  Created by  on 9/3/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AOMainPopupViewController.h"
#import "CustomIOSAlertView.h"
#import "RegulationPopupViewController.h"
#import "WithdrawBonusResetConfirmViewController.h"
#import "PEPPopupViewController.h"
#import "NIOUPopupViewController.h"
#import "CysecPopupViewController.h"
#import "AOShowUserBonusesPopUpViewController.h"
/* CORE - 2281 */
#import "AOBonusUsers.h"
#import "WithdrawBonusResetConfirmViewController.h"
#import "ShowUserBonusesPopUpViewController.h"
#import "RestAPIFacade.h"
#import "LowBalancePopupViewController.h"
#import "NSAttributedString+RT.h"
#import "AccountLockedBySecurityPopupViewController.h"

#ifdef COPYOP
#import  "COModalPopupViewController.h"
#import  "UIViewController+Helpers.h"
#endif


static int popupContentTag = 303;
static int co_popupContentTag = 304;



@implementation AOMainPopupViewController{
    UIViewController* popupContentController;
    
    
    CustomIOSAlertView *ao_popupContainer;
    WEPopoverController *ao_arrow_popupContainer;
    
#ifdef COPYOP
    COModalPopupViewController *co_popupContainer;
#endif
}

- (void) showPopup:(AOPopupType)type withData:(NSDictionary*)data animated:(BOOL)animated completion:(void (^)()) completion{
    if(self.shown && type!=AODepositBalanceSplitTotal) return;
    
    self.popupType = type;
    self.shown = YES;
//    NSMutableArray *popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"alert.button"), nil];
    NSMutableArray *popupButtons = nil;
    BOOL arrowPopup = NO;
#ifdef ANYOPTION
    BOOL aoStylePopup = YES;
#else
    BOOL aoStylePopup = NO;
#endif
    
    
    BOOL shouldShowPepAfterRestricted = NO;
    
    switch (type) {
            
        case AORegulationStatePopupRestricted:{
            
            popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"alert.button"), nil];

            popupContentController = (RegulationPopupViewController*)[RESIDENT getScreen:@"regulationPopup"];
            [(RegulationPopupViewController*)popupContentController setRegulationType:AORegulationUserRestricted];
            
            
#ifdef COPYOP
            __typeof__(self) __weak wSelf = self;
            [(RegulationPopupViewController*)popupContentController setCloseButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
                    [sSelf hide];
                }
                
            }];
#endif
        }
            break;
            
            
           
        case AORegulationStatePopupBlocked:{
            
            popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"alert.button"), nil];
            
            popupContentController = [RESIDENT getScreen:@"regulationPopup"];
            [(RegulationPopupViewController*)popupContentController setRegulationType:AORegulationUserBlocked];
#ifdef COPYOP
            __typeof__(self) __weak wSelf = self;
            [(RegulationPopupViewController*)popupContentController setCloseButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    /* MAIN ACTION */
                    NSLog(@"debadebadeba");
                    __typeof__(self) __strong sSelf = wSelf;
                    [sSelf hide];
                }
                
            }];
            
            if(self.noButtonPressedHandler) {
                NSLog(@"has noButtonPressHandler");
            } else {
                NSLog(@"mainata ti");
            }
            
            if(self.yesButtonPressedHandler) {
                NSLog(@"has yesButtonPressHandler");
            } else {
                NSLog(@"mainata ti, I said");
            }
            
#endif
            
        }
            break;
            
        case AOPEPRestricted:{
            
            popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"alert.button"), nil];
            
            popupContentController = [RESIDENT getScreen:@"pepPopup"];
            
#ifdef COPYOP
            __typeof__(self) __weak wSelf = self;
            [(PEPPopupViewController *)popupContentController setCloseButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
                    [sSelf hide];
                }
                
            }];
#endif
            
        }
            break;
            
        case AOCysecRestricted:{
            
            popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"cysegButton"), nil];
            
            popupContentController = [RESIDENT getScreen:@"cysecPopup"];
            
#ifdef COPYOP
            __typeof__(self) __weak wSelf = self;
            [(CysecPopupViewController *)popupContentController setCloseButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
                    [sSelf hide];
                }
                
            }];
            
            [(CysecPopupViewController *)popupContentController setActivateButtonPressedHandler:^{
                
                if(completion) {
                    completion();
                }
                
                __typeof__(self) __strong sSelf = wSelf;
                [sSelf hide];
            }];
#endif
            
            
#ifdef ANYOPTION
            __typeof__(self) __weak wSelf = self;
            [(CysecPopupViewController *)popupContentController setHideButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
                    [sSelf hide];
                }
            }];
#endif
            
        }
            break;
            
        case AOAccountLockedBySecurity:{
            
            
            popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"alert.button"), nil];
            
            popupContentController = [RESIDENT getScreen:@"lockedBySecurityPopup"];
       
            
#ifdef ANYOPTION
            __typeof__(self) __weak wSelf = self;
            [(AccountLockedBySecurityPopupViewController *)popupContentController setHideButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
                    [sSelf hide];
                }
            }];
#endif
            
        }
            break;

            
            
            
        case AOLowBalance:{
         
            
            popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"profitLine.deposit"), nil];
            
            popupContentController = [RESIDENT getScreen:@"lowBalancePopup"];
            
            NSUserDefaults *depositButtonDefaults = [NSUserDefaults standardUserDefaults];
            [depositButtonDefaults setBool:YES forKey:@"lowBallanceDepositButton"];
            
#ifdef COPYOP
            __typeof__(self) __weak wSelf = self;
            [(LowBalancePopupViewController *)popupContentController setCloseButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
//                     [RESIDENT goTo:@"depositMenu" withOptions:nil];
                    [sSelf hide];
                }
                
            }];
            
            [(LowBalancePopupViewController *)popupContentController setActivateButtonPressedHandler:^{
                
                if(completion) {
                    completion();
                }
                
                __typeof__(self) __strong sSelf = wSelf;
                [sSelf hide];
            }];
#endif
            
            
#ifdef ANYOPTION
            __typeof__(self) __weak wSelf = self;
            [(LowBalancePopupViewController *)popupContentController setHideButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
                    [sSelf hide];
                }
            }];
#endif
            
        }
            break;

            
        case COClaimBonus:{
            
                       NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSData *encodedObject = [defaults objectForKey:@"unseenBonusFromBonusUsers"];
            NSData *myEncodedTurnoverObject = [defaults objectForKey:@"unseenTurnoverBonusFromBonusUsers"];
            
            NSArray *turnoverFactorArray = [NSKeyedUnarchiver unarchiveObjectWithData:myEncodedTurnoverObject];
            NSDictionary *bonusUsersObject = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
            AOBonusUsers *bonus = [[AOBonusUsers alloc] init];
            [bonus setValuesForKeysWithDictionary:bonusUsersObject];

            
            popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"gotIt"), nil];
            popupContentController = [RESIDENT getScreen:@"showBonusPopup"];
            
            
            
#ifdef COPYOP
            __typeof__(self) __weak wSelf = self;
            [(ShowUserBonusesPopUpViewController *)popupContentController setCloseButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
                    [sSelf hide];
                    if ([defaults boolForKey:@"bonusIsSingle"] == YES) {
                        [MAINVC loadPage:@"Account"
                                pageName:@"BonusDetails"
                                    root:NO
                                 options:@{
                                           @"bonus":bonus,
                                           @"turnoverFactor":turnoverFactorArray
                                           }];
                    } else {
                        [MAINVC loadPage:@"Account" pageName:@"bonuses" root:YES options:nil];
                        
                    }
                }
                
            }];
            
            [(ShowUserBonusesPopUpViewController *)popupContentController setActivateButtonPressedHandler:^{
                
                if(completion) {
                    completion();
                }
                
                __typeof__(self) __strong sSelf = wSelf;
                [sSelf hide];
                if ([defaults boolForKey:@"bonusIsSingle"] == YES) {
                    [MAINVC loadPage:@"Account"
                            pageName:@"BonusDetails"
                                root:NO
                             options:@{
                                       @"bonus":bonus,
                                       @"turnoverFactor":turnoverFactorArray
                                       }];
                } else {
                    
                    [MAINVC loadPage:@"Account" pageName:@"bonuses" root:YES options:nil];
                    
                }
            }];
#endif
            
            
#ifdef ANYOPTION
            __typeof__(self) __weak wSelf = self;
            [(ShowUserBonusesPopUpViewController *)popupContentController setHideButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
                    [sSelf hide];
                }
            }];
#endif

            
        }
             break;
            
        case AOClaimBonus:{
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            if([defaults boolForKey:@"bonusIsSingle"] == NO) {
             popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"seeBonuses"), nil];
            }else{
            popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"claimBonus"), nil];
            }
           
            
            popupContentController = [RESIDENT getScreen:@"aoShowBonusPopup"];
            
#ifdef COPYOP
            __typeof__(self) __weak wSelf = self;
            [(AOShowUserBonusesPopUpViewController *)popupContentController setCloseButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
                    [sSelf hide];
                }
                
            }];
            
            [(AOShowUserBonusesPopUpViewController *)popupContentController setActivateButtonPressedHandler:^{
                
                if(completion) {
                    completion();
                }
                
                __typeof__(self) __strong sSelf = wSelf;
                [sSelf hide];
            }];
#endif
            
            
#ifdef ANYOPTION
            __typeof__(self) __weak wSelf = self;
            [(AOShowUserBonusesPopUpViewController *)popupContentController setHideButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
                    [sSelf hide];
                }
            }];
#endif
            
            
        }

            break;
            
        case AONIOUNotice:{
            
            popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"profitLine.deposit"), nil];
            
            popupContentController = [RESIDENT getScreen:@"niouPopup"];
            ((NIOUpopupViewController *)popupContentController).niouData = data;
            
//#ifdef COPYOP
            __typeof__(self) __weak wSelf = self;
            [(NIOUpopupViewController *)popupContentController setCloseButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
                    [sSelf hide];
                    [RESIDENT goTo:@"depositMenu" withOptions:nil];
                }
                
            }];
             
//#endif
            
        }
            break;
            /* CORE - 2281 */
        case AOWithdrawBonusResetConfirm:{
            
            popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"alert.button"), nil];
            
            popupContentController = [RESIDENT getScreen:@"withdrawBonusPopup"];
            //[(WithdrawBonusResetConfirmViewController*)popupContentController setRegulationType:AORegulationUserBlocked];
            popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"CMS.reg_opt_quest.text.answer.yes"),AOLocalizedString(@"CMS.reg_opt_quest.text.answer.no"), nil];
            
#ifdef COPYOP
            __typeof__(self) __weak wSelf = self;
            [(WithdrawBonusResetConfirmViewController*)popupContentController setCloseButtonPressedHandler:^{
                if (wSelf != nil)
                {
                    /* MAIN ACTION */
                    __typeof__(self) __strong sSelf = wSelf;
                    [sSelf hide];
                }
                
            }];
            
            [(WithdrawBonusResetConfirmViewController*)popupContentController setYesPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
                    if(sSelf.yesButtonPressedHandler != nil) {
                        sSelf.yesButtonPressedHandler(@"YES");
                        [sSelf hide];
                    }
                }
            }];
            
            [(WithdrawBonusResetConfirmViewController*)popupContentController setNoPressedHandler:^{
                if (wSelf != nil)
                {
                    __typeof__(self) __strong sSelf = wSelf;
                    if(sSelf.noButtonPressedHandler != nil) {
                        sSelf.noButtonPressedHandler(@"NO");
                        [sSelf hide];
                    }
                }
            }];

            
#endif
            
        }
            break;
            
        case AODepositBalanceSplit:{
            
            popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"alert.button"), nil];
            
            long invId = [[data objectForKey:@"invId"] intValue];
            int  type  = [[data objectForKey:@"type"] intValue];
            popupContentController = [[DepositBonusBalanceTableViewController alloc] initWithType:type andInvestmentId:invId];
            
        }
            break;
            
        case AODepositBalanceSplitTotal:{
            
            popupButtons = [NSMutableArray arrayWithObjects:AOLocalizedString(@"alert.button"), nil];
            
            int  type  = [[data objectForKey:@"dbbType"] intValue];
            popupContentController = [[DepositBonusBalanceTableViewController alloc] initWithType:type andInvestmentId:0];
            arrowPopup = YES;
            aoStylePopup = YES;
        }
            break;
            
        default:
            
            break;
    }

    
    
    
    if(aoStylePopup) {
        if (!arrowPopup) {
                ao_popupContainer = [[CustomIOSAlertView alloc] init];
                ao_popupContainer.backgroundColor = [UIColor whiteColor];
            
                [ao_popupContainer setContainerView:popupContentController.view];

                [ao_popupContainer setButtonTitles:popupButtons];            
            
                __typeof__(self) __weak wSelf = self;
                [ao_popupContainer setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
                    
                    if (wSelf!=nil) {
                        
                        __typeof__(wSelf) __strong sSelf = wSelf;
                        
                        
                        if (sSelf.popupButtonPressedHandler != nil) {
                            sSelf.popupButtonPressedHandler(buttonIndex,sSelf.popupType);
                        }
                        [sSelf hide];
                        
                        if(type == AOClaimBonus){
                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            NSData *encodedObject = [defaults objectForKey:@"unseenBonusFromBonusUsers"];
                            NSData *myEncodedTurnoverObject = [defaults objectForKey:@"unseenTurnoverBonusFromBonusUsers"];
                            
                            NSArray *turnoverFactorArray = [NSKeyedUnarchiver unarchiveObjectWithData:myEncodedTurnoverObject];
                            NSDictionary *bonusUsersObject = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
                            AOBonusUsers *bonus = [[AOBonusUsers alloc] init];
                            [bonus setValuesForKeysWithDictionary:bonusUsersObject];
                            if ([defaults boolForKey:@"bonusIsSingle"] == YES) {
                                [MAINVC loadPage:@"Account"
                                        pageName:@"BonusDetails"
                                            root:NO
                                         options:@{
                                                   @"bonus":bonus,
                                                   @"turnoverFactor":turnoverFactorArray
                                                   }];
                            } else {
                                [MAINVC loadPage:@"Account" pageName:@"bonuses" root:YES options:nil];
                                
                            }

                        }
                        if(type == AONIOUNotice) {
                            [RESIDENT goTo:@"depositMenu" withOptions:nil];
                        }
                        if(type == AOLowBalance) {
                            
                            [RESIDENT goTo:@"depositMenu" withOptions:nil];
                        }
                        
                        if(type == AOCysecRestricted) {
                            /* BAC-1189 BUTTON ACTION */
                            
                            NSLog(@"ACTIVATE MY ACCOUNT BUTTON!");
                            BOOL isClicked = NO;
                            if(isClicked == NO){
                                
                            [[RestAPIFacade sharedInstance] updateCysec:[DLG user] withCompletion:^(AOResponse *response) {
                                NSLog(@"main popup:\n %@", response.parsedResponce);
                                NSLog(@"AOPOPUP RESPONSE: %@",response);
                                [DLG userRegulation].thresholdBlock = -1;
//                                if(response != nil){
//                                 [sSelf hide];
//                                }
                                
                                if(completion) {
                                    completion();
                                }
                            }];
                               
                                isClicked = YES;
                            }
                        }
                        
                        
                        
                        
                        
//                        if(type == AORegulationStatePopupRestricted) {
//#ifndef ETRADER
//                            if(((RegulationPopupViewController*)popupContentController).restrictedKnowledge == YES) {
//                                if([DLG userRegulation].pepState == 1 || [DLG userRegulation].pepState == 2) {
//                                    [[MAINVC popupVC] showPopup:AOPEPRestricted withData:nil animated:YES completion:nil];
//                                    
//                                    NSLog(@"AORegulationStatePopupRestricted");
//                                }
//                            }
//                            
//#endif
//
//                        }
                        
                    }
                }];
                [ao_popupContainer setUseMotionEffects:true];
                [ao_popupContainer show];
//                [[[MAINVC centerViewController] view] addSubview:ao_popupContainer];
//            
//                [[[MAINVC centerViewController] view] bringSubviewToFront:ao_popupContainer];
            
            if(IS_IPAD) {
                [[[[UIApplication sharedApplication] windows] firstObject] addSubview:ao_popupContainer];
                
                [[[[UIApplication sharedApplication] windows] firstObject] bringSubviewToFront:ao_popupContainer];
//                [ao_arrow_popupContainer presentPopoverFromRect:[MAINVC splitBalanceStatusButton].frame
//                                                         inView:[[[UIApplication sharedApplication] windows] firstObject]
//                                       permittedArrowDirections:(UIPopoverArrowDirectionUp)
//                                                       animated:YES];
            } else {
                [[[MAINVC centerViewController] view] addSubview:ao_popupContainer];
                
                [[[MAINVC centerViewController] view] bringSubviewToFront:ao_popupContainer];
            }
            
            NSLog(@"ao_popupContainer %@", NSStringFromCGRect(ao_popupContainer.frame));
                
                if (animated)
                {
                    ao_popupContainer.alpha = 0.0;
                    
                    [UIView animateWithDuration:0.3
                                          delay:0.0
                                        options:UIViewAnimationOptionCurveEaseInOut
                                     animations:^{
                                         ao_popupContainer.alpha = 1.0;
                                     } completion:^(BOOL finished) {
                                         
                                     }];
                }
        }
        else {
            
            
            
                ao_arrow_popupContainer = nil;
                ao_arrow_popupContainer = [[WEPopoverController alloc] initWithContentViewController:popupContentController];
                ao_arrow_popupContainer.delegate = self;    
                ao_arrow_popupContainer.popoverContentSize = CGSizeMake(CGRectGetWidth(popupContentController.view.frame),CGRectGetHeight(popupContentController.view.frame));
            
                [ao_arrow_popupContainer setContainerViewProperties:[self improvedContainerViewProperties]];
            
                [ao_arrow_popupContainer presentPopoverFromRect:[MAINVC splitBalanceStatusButton].frame
                                                     inView:[MAINVC view]
                                   permittedArrowDirections:(UIPopoverArrowDirectionUp)
                                                   animated:YES];
        }
        
    }
    else
    {
        

#ifdef COPYOP
      
            co_popupContainer = [[COModalPopupViewController alloc] initWithContentViewController:popupContentController];
            
            //co_popupContainer.modalPopupY = 75;
            
            [co_popupContainer presentInViewController:MAINVC animated:YES completion:^{}];
            
            co_popupContainer.didTapOutsideHandler = co_popupContainer.closeButtonPressedHandler =
            ((RegulationPopupViewController*)popupContentController).closeButtonPressedHandler;
        
    
  

#else
        ao_popupContainer = [[CustomIOSAlertView alloc] init];
        ao_popupContainer.backgroundColor = [UIColor whiteColor];
        [ao_popupContainer setContainerView:popupContentController.view];
        
        [ao_popupContainer setButtonTitles:popupButtons];
        
        __typeof__(self) __weak wSelf = self;
        [ao_popupContainer setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
            if (wSelf!=nil) {
                __typeof__(wSelf) __strong sSelf = wSelf;
                
                if (sSelf.popupButtonPressedHandler != nil) {
                    sSelf.popupButtonPressedHandler(buttonIndex,sSelf.popupType);
                }
                
                [sSelf hide];
                
                //                    sSelf.shown = NO;
                //
                
            }
            // [alertView close];
        }];
        [ao_popupContainer setUseMotionEffects:true];
        [ao_popupContainer show];
        
        if (animated)
        {
            ao_popupContainer.alpha = 0.0;
            
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 ao_popupContainer.alpha = 1.0;
                             } completion:^(BOOL finished) {
                                 
                             }];
        }

#endif
    }
}



-(void)hide {
     self.shown = NO;
    //[[[MAINVC view] viewWithTag:popupContentTag] removeFromSuperview];

    if (ao_popupContainer!=nil) {
        [ao_popupContainer close];
        ao_popupContainer = nil;
    }
    else if(ao_arrow_popupContainer!=nil) {
        [ao_arrow_popupContainer dismissPopoverAnimated:YES];
        ao_arrow_popupContainer = nil;
    }
    
#ifdef COPYOP
    if (co_popupContainer!=nil) {
        [co_popupContainer dismissViewControllerAnimated:YES completion:nil];
        co_popupContainer = nil;
    }
#endif

    
    if(self.afterDismissedHandler!=nil){
        self.afterDismissedHandler();
        self.afterDismissedHandler = nil;
    }
}

- (WEPopoverContainerViewProperties *)improvedContainerViewProperties {
    
    WEPopoverContainerViewProperties *props = [[WEPopoverContainerViewProperties alloc] init];
    NSString *bgImageName = nil;
    CGFloat bgMargin = 0.0;
    CGFloat bgCapSize = 0.0;
    CGFloat contentMargin = 4.0;
    
    bgImageName = @"popoverBg-white.png";
    
    // These constants are determined by the popoverBg.png image file and are image dependent
    bgMargin = 13; // margin width of 13 pixels on all sides popoverBg.png (62 pixels wide - 36 pixel background) / 2 == 26 / 2 == 13
    bgCapSize = 31; // ImageSize/2  == 62 / 2 == 31 pixels
    
    props.leftBgMargin = bgMargin;
    props.rightBgMargin = bgMargin;
    props.topBgMargin = bgMargin;
    props.bottomBgMargin = bgMargin;
    props.leftBgCapSize = bgCapSize;
    props.topBgCapSize = bgCapSize;
    props.bgImageName = bgImageName;
    //  props.leftContentMargin = contentMargin;
    // props.rightContentMargin = contentMargin - 1; // Need to shift one pixel for border to look correct
    //props.topContentMargin = contentMargin;
    // props.bottomContentMargin = contentMargin;
    
    props.arrowMargin = 4.0;
#ifndef COPYOP
    props.upArrowImageName = @"ddbpopuparrowup.png";
    props.downArrowImageName = @"popoverArrowDown.png";
    props.leftArrowImageName = @"popoverArrowLeft.png";
    props.rightArrowImageName = @"popoverArrowRight.png";
#else
    props.upArrowImageName = @"co_popoverArrowUp-white.png";
    props.downArrowImageName = @"popoverArrowDown.png";
    props.leftArrowImageName = @"popoverArrowLeft.png";
    props.rightArrowImageName = @"popoverArrowRight.png";
#endif
    return props;
}


-(void) cysecSendUserIdToActivateAccount{

    
    [[RestAPIFacade sharedInstance] updateCysec:[DLG user] withCompletion:^(AOResponse *response) {
        NSLog(@"awbabebabbaba: \n %@", response.parsedResponce);
    }];
}

#pragma mark -
#pragma mark WePopoverControllerDelegate
- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController{
    self.shown = NO;
}
@end
