//
//  AOAttributedLabel.m
//  iOSApp
//
//  Created by  on 9/3/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AOAttributedLabel.h"
#import "NSAttributedString+RT.h"

@implementation AOAttributedLabel

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.userInteractionEnabled = YES;
    }
    return self;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //find the character index
    UITouch *t = [touches anyObject];
    CGPoint tp = [t locationInView:self];
    CFIndex index = [self.attributedText indexAtPoint:tp viewBounds:self.bounds];
    if (index >= [self.attributedText string].length || index <0) {
        return;
    }
    
    NSRange attrRange = NSMakeRange(0, 0);
    NSDictionary* indexAttributes = [self.attributedText attributesAtIndex:index effectiveRange:&attrRange];
    
    if (indexAttributes!=nil && self.adelegate!=nil) {
        [self.adelegate tappedAttributes:indexAttributes];
    }
}


-(void)setAdelegate:(id<AOAttributedLabelDelegate>)del{
    
    
    _adelegate = del;
    
}




@end
