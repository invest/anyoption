//
//  AOAttributedLabel.h
//  iOSApp
//
//  Created by  on 9/3/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "AOLabel.h"
@protocol AOAttributedLabelDelegate

@optional
-(void)tappedAttributes:(NSDictionary*)attributes;
@end

@interface AOAttributedLabel : UILabel
@property (nonatomic, weak) id <AOAttributedLabelDelegate> adelegate;
@end
