//
//  AOMainPopupViewController.h
//  iOSApp
//
//  Created by  on 9/3/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WEPopoverController.h"
typedef enum {
    AORegulationStatePopupRestricted,
    AORegulationStatePopupBlocked,
    AOWithdrawBonusResetConfirm,
    AODepositBalanceSplitTotal,
    AODepositBalanceSplit,
    AOPEPRestricted,
    AOCysecRestricted,
    AONIOUNotice,
    AOClaimBonus,
    COClaimBonus,
    AOLowBalance,
    AOAccountLockedBySecurity
    
}AOPopupType;

typedef void (^OnPopupButtonPressed)(int buttonIndex,AOPopupType type);
typedef void (^OnPopupDismissed)();

typedef void (^YesButtonPressed)(NSString *demo);
typedef void (^NoButtonPressed)(NSString *demo);


@interface AOMainPopupViewController : UIViewController <WEPopoverControllerDelegate>


@property(nonatomic,assign) BOOL shown;
@property (nonatomic, copy) OnPopupButtonPressed popupButtonPressedHandler;
@property (nonatomic, assign) AOPopupType popupType;
@property (nonatomic, copy) OnPopupDismissed afterDismissedHandler;

@property (nonatomic, copy) YesButtonPressed yesButtonPressedHandler;
@property (nonatomic, copy) NoButtonPressed noButtonPressedHandler;

- (void) showPopup:(AOPopupType)type withData:(NSDictionary*)data animated:(BOOL)animated completion:(void (^)()) completion;
- (void) hide;

@end
