//
//  NIOUpopupViewController.m
//  iOSApp
//
//  Created by Lyubomir Marinov on 12/2/15.
//  Copyright © 2015 Anyoption. All rights reserved.
//

#import "NIOUpopupViewController.h"
#import "AOThemeController.h"
#import "NSAttributedString+RT.h"
#import "CheckBox.h"
#import "RestAPIFacade.h"
#import "AOBottomCorneredView.h"
#import "RegulationRestrictedKnowledgeCheckbox.h"
#import "COPopupHeaderView.h"
#ifdef COPYOP
#import "UIView+Shadows.h"
#endif


#ifdef ANYOPTION
static CGFloat popupWidth = 300.0;
#elif ETRADER
static CGFloat popupWidth = 300.0;
#elif COPYOP
static CGFloat popupWidth = 280.0;
#endif

@interface NIOUpopupViewController ()


@property (weak, nonatomic) IBOutlet UIButton *okButton;

@property (weak, nonatomic) IBOutlet COPopupHeaderView *headerView;


@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet AOAttributedLabel *mainLabel;
@property (nonatomic, weak) IBOutlet UILabel *mainLabelRegular;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_labelTop;

@property (weak, nonatomic) IBOutlet UIView *vContainer;
@property (weak, nonatomic) IBOutlet AOBottomCorneredView *vBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcCheckboxHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcOkButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcHeaderViewHeight;

@end

@implementation NIOUpopupViewController {
    BOOL restrictedKnowledge;
#ifdef COPYOP
    UIImageView *_shadowImage;
    UIImageView *_shadowUploadButton;
#endif
}

-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self.okButton addTarget:self action:@selector(okButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.okButton setTitle:AOLocalizedString(@"profitLine.deposit") forState:UIControlStateNormal];

    self.mainLabel.text = @" ";
    
#ifdef COPYOP
    self.vBottom.hidden = NO;
    
    
//    [self.okButton setTitle:AOLocalizedString(@"profitLine.deposit") forState:UIControlStateNormal];

    self.headerView.hidden = NO;
    
    self.view.backgroundColor = [UIColor clearColor];
    self.headerView.showsSeparatorLine = NO;
    self.vBottom.backgroundColor = [UIColor clearColor];
    
    self.mainLabel.numberOfLines = 0;
    
    self.vContainer.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    
    [self.okButton setTitleColor:[UIColor colorWithRed:234.0/255.0 green:85.0/255.0 blue:58.0/255.0 alpha:1.0] forState:UIControlStateNormal];
#endif
    
    self.view.backgroundColor = [UIColor clearColor];
    
    [self updateUI];
}


-(void)okButtonPressed:(id)sender {
    if (self.closeButtonPressedHandler != nil)
    {
        self.closeButtonPressedHandler();
    }
}

#ifdef COPYOP
- (void) p_updateShadow
{
    [self.view layoutIfNeeded];
    
    [_shadowImage removeFromSuperview];
    
    UIView *headView = self.headerView;
    CGRect r = CGRectMake(CGRectGetMinX(headView.frame),
                          CGRectGetMinY(headView.frame),
                          CGRectGetWidth(headView.bounds),
                          CGRectGetMaxY(self.vContainer.frame));
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:r
                                               byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                                     cornerRadii:CGSizeMake(5.0, 5.0)];
    CGFloat shadowSize = 3.0;
    UIImage *img = [UIView shadowImageWithBezierPath:path shadowSize:shadowSize];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [self.view insertSubview:imgView atIndex:0];
    
    CGRect fr = imgView.frame;
    fr.origin = CGPointMake(CGRectGetMinX(headView.frame) - shadowSize, CGRectGetMinY(headView.frame) - shadowSize);
    imgView.frame = fr;
    _shadowImage = imgView;
    
    if (_shadowUploadButton == nil)
    {
        [_shadowUploadButton removeFromSuperview];
        UIBezierPath *cancelPath = [UIBezierPath bezierPathWithRoundedRect:self.vBottom.bounds
                                                         byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight
                                                               cornerRadii:CGSizeMake(5.0, 5.0)];
        UIImage *cancelShadowImage = [UIView shadowImageWithBezierPath:cancelPath shadowSize:shadowSize];
        UIImageView *cancelImgView = [[UIImageView alloc] initWithImage:cancelShadowImage];
        [self.view insertSubview:cancelImgView belowSubview:self.vBottom];
        
        [self.vBottom pinShadowImageView:cancelImgView shadowSize:shadowSize];
        
        _shadowUploadButton = cancelImgView;
    }
}
#endif


-(NSString *)extractNumberFromText:(NSString *)text
{
    NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return [[text componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""];
}

- (void) updateUI
{
    if (![self isViewLoaded])
    {
        return;
    }
    
    [self.okButton setTitle:AOLocalizedString(@"profitLine.deposit") forState:UIControlStateNormal];
    
    id<AOAppTheme> theme = [AOThemeController sharedInstance].theme;
    self.lcCheckboxHeight.constant = 0.0;
    
#ifdef ANYOPTION    
    self.lcOkButtonHeight.constant = 0.0;
    self.vContainer.backgroundColor = [UIColor clearColor];
#endif
    
#ifdef ETRADER
    self.lcOkButtonHeight.constant = 0.0;
    self.vContainer.backgroundColor = [UIColor clearColor];
#endif
    
    if([self.niouData[@"apiErrorCode"] isEqualToString:@"V999"]) {
        NSLog(@"fffffffa -1");
        
        if(self.niouData[@"userCashBalance"] != [NSNull null]) {
            NSString *cashBalanceString = [self extractNumberFromText:(NSString *)[self.niouData[@"userCashBalance"] lastObject]];
            
            NSString *balance = [NSString stringWithFormat:@"\n%.02f%@", (CGFloat)([cashBalanceString intValue] / 100), [DLG user].currency.symbol];
            NSMutableAttributedString* rtString = [[[NSAttributedString alloc] initWithRTString:[NSString stringWithFormat:AOLocalizedString(@"NIOUpopup"), balance] withTheme:theme andAttributes:@{NSFontAttributeName:[theme quoteExpiryLevelFont]}  ] mutableCopy];
            
            self.mainLabel.attributedText = rtString;
        } else {
            NSLog(@"fffffffa 0");
            
            NSMutableAttributedString* rtString = [[[NSAttributedString alloc] initWithRTString:[NSString stringWithFormat:AOLocalizedString(@"NIOUpopup"), @" "] withTheme:theme andAttributes:@{NSFontAttributeName:[theme quoteExpiryLevelFont]}  ] mutableCopy];
            
            self.mainLabel.attributedText = rtString;
        }
        
    } else if([self.niouData[@"apiErrorCode"] isEqualToString:@"V999"]) {
        NSLog(@"fffffffa 1");

        if([self.niouData[@"message"] containsString:@"NO_CASH_FOR_NIOU="]) {
            NSArray *message = [self.niouData[@"message"] componentsSeparatedByString:@"NO_CASH_FOR_NIOU="];
            NSString *cashBalanceString = [self extractNumberFromText:(NSString *)[message lastObject]];
            
            
            NSString *balance = [NSString stringWithFormat:@"\n%.02f%@", (CGFloat)([cashBalanceString intValue] / 100), [DLG user].currency.symbol];
            NSMutableAttributedString* rtString = [[[NSAttributedString alloc] initWithRTString:[NSString stringWithFormat:AOLocalizedString(@"NIOUpopup"), balance] withTheme:theme andAttributes:@{NSFontAttributeName:[theme quoteExpiryLevelFont]}  ] mutableCopy];
            
            self.mainLabel.attributedText = rtString;
        } else {
            NSLog(@"fffffffa 2");

            NSMutableAttributedString* rtString = [[[NSAttributedString alloc] initWithRTString:[NSString stringWithFormat:AOLocalizedString(@"NIOUpopup"), @" "] withTheme:theme andAttributes:@{NSFontAttributeName:[theme quoteExpiryLevelFont]}  ] mutableCopy];
            
            self.mainLabel.attributedText = rtString;
        }
        
    } else {
        [[RestAPIFacade sharedInstance] getUserDepositBonusBalance:DepositBonusBalanceTypeTotalBig investmentId:0 andCompletion:^(AOResponse* r, DepositBonusBalance* dbb){
            
            if (dbb.show == NO) {
                NSLog(@"fffffffa 3");

                NSMutableAttributedString* rtString = [[[NSAttributedString alloc] initWithRTString:[NSString stringWithFormat:AOLocalizedString(@"NIOUpopup"), @" "] withTheme:theme andAttributes:@{NSFontAttributeName:[theme quoteExpiryLevelFont]}  ] mutableCopy];
                
                self.mainLabel.attributedText = rtString;
            } else {
                NSString *balance = @"";
                if([DLG user].currency.isLeftSymbol == 1) {
                    balance = [NSString stringWithFormat:@"\n%@%.02f", [DLG user].currency.symbol, (CGFloat)(dbb.depositCashBalance / 100.0f)];
                } else {
                    balance = [NSString stringWithFormat:@"\n%.02f%@", (CGFloat)(dbb.depositCashBalance / 100.0f), [DLG user].currency.symbol];

                }
                
                NSMutableAttributedString* rtString = [[[NSAttributedString alloc] initWithRTString:[NSString stringWithFormat:AOLocalizedString(@"NIOUpopup"), balance] withTheme:theme andAttributes:@{NSFontAttributeName:[theme quoteExpiryLevelFont]}  ] mutableCopy];
                
                self.mainLabel.attributedText = rtString;
                NSLog(@"fffffffa 4 %@", rtString);
            }
            
            
            CGSize size = [self.view systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
            CGRect fr = self.view.frame;
            fr.size.height = size.height;
            self.view.frame = fr;
            
#ifdef COPYOP
            [self p_updateShadow];
#endif
        }];
    }
}

@end
