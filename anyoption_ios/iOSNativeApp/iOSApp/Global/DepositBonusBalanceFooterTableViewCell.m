//
//  DepositBonusBalanceFooterTableViewCell.m
//  iOSApp
//
//  Created by  on 9/7/15.
//  Copyright (c) 2015 Anyoption. All rights reserved.
//

#import "DepositBonusBalanceFooterTableViewCell.h"

@implementation DepositBonusBalanceFooterTableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.textLabel.textAlignment = NSTextAlignmentLeft;
        self.textLabel.numberOfLines = 0;
        self.textLabel.adjustsFontSizeToFitWidth = YES;
        self.textLabel.minimumScaleFactor = 0.5;
        
        
        
    }
    return self;
}
@end
