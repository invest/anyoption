--- anyoption_ios/iOSApp/Global/Lightstreamer/include/LSTableDelegate.h	(revision 34474)
+++ anyoption_ios/iOSApp/Global/Lightstreamer/include/LSTableDelegate.h	(nonexistent)
@@ -1,103 +0,0 @@
-//
-//  LSTableDelegate.h
-//  Lightstreamer client for iOS
-//
-
-#import <Foundation/Foundation.h>
-
-
-@class LSSubscribedTableKey;
-@class LSUpdateInfo;
-
-/**
- * The LSTableDelegate protocol receives notification of data updates and subscription termination for a table.
- * Upon update of each item, the current and previous state of all fields is provided.
- * <BR/>If the subscription configuration enables the "COMMAND logic", then the special "command" and "key" fields are determined and the updates
- * are meant as ADD, UPDATE and DELETE commands on the rows of an underlying table, identified by a key value. In this case, the old field values related
- * with an update are referred to the same key.
- * <BR/>Both names and positional information are available to identify specific items and fields, unless an LSTableInfo was used
- * to describe the table: in that case, only positional information is available.
- * <BR/>The delegate is called directly from the session thread. The method implementations should be fast and nonblocking. Any slow operations should
- * be enqueued and performed asynchronously.
- */
-@protocol LSTableDelegate <NSObject>
-
-
-#pragma mark -
-#pragma mark Data updates
-
-/**
- * Notification of an update of the values for an item in the table. Field value information is supplied through a suitable value object:
- * as such, the value object can be stored and inquired in a different thread, if needed.
- *
- * @param tableKey Key of the related table, as returned by the subscription method.
- * @param itemPosition 1-based index of the item in the involved table.
- * @param itemName Name of the changed item, or null if an LSTableInfo was used to describe the table.
- * @param updateInfo Value object which contains field value information.
- */
-- (void) table:(LSSubscribedTableKey *)tableKey itemPosition:(int)itemPosition itemName:(NSString *)itemName didUpdateWithInfo:(LSUpdateInfo *)updateInfo;
-
-
-@optional
-
-
-#pragma mark -
-#pragma mark Snapshot status notification
-
-/**
- * Notification that no more snapshot events are coming for an item. This notification is always received once, when the item is subscribed
- * in DISTINCT or COMMAND mode and the snapshot is requested.
- * <BR/>Note that previous to version 1.2.3 for iOS and 1.0.2 for OS X the library had a typo in the method signature, <I>Snaphost</I> instead of <I>Snapshot</I>:
- * <BR/><CODE>- (void) table:(LSSubscribedTableKey *)tableKey didEndSnaphostForItemPosition:(int)itemPosition itemName:(NSString *)itemName</CODE>
- * <BR/>The signature is now correct, but a workaround is in place to call the old (incorrect) signature if the new (correct) one is not found
- * on the delegate.
- *
- * @param tableKey Key of the related table, as returned by the subscription method.
- * @param itemPosition 1-based index of the item in the involved table.
- * @param itemName Name of the changed item, or null if an LSTableInfo was used to describe the table.
- */
-- (void) table:(LSSubscribedTableKey *)tableKey didEndSnapshotForItemPosition:(int)itemPosition itemName:(NSString *)itemName;
-
-
-#pragma mark -
-#pragma mark Update lose notification
-
-/**
- * Notification of one or more updates that were suppressed in the Server because of internal memory limitations. The notification can be sent
- * if the subscription mode is RAW or COMMAND. It can also be sent if the subscription mode is MERGE or DISTINCT and unfiltered dispatching has
- * been requested. In all these cases, an update loss may be unacceptable for the client (in filtered COMMAND mode, this applies to ADD and
- * DELETE events only).
- *
- * @param tableKey Key of the related table, as returned by the subscription method.
- * @param itemPosition 1-based index of the item in the involved table.
- * @param itemName Name of the changed item, or null if an LSTableInfo was used to describe the table.
- * @param numberOfUpdates Number of consecutive lost updates for the item.
- */
-- (void) table:(LSSubscribedTableKey *)tableKey itemPosition:(int)itemPosition itemName:(NSString *)itemName didLoseRawUpdates:(int)numberOfUpdates;
-
-
-#pragma mark -
-#pragma mark Unsubscription notification
-
-/**
- * Notification of the unsubscription of an item in the table. It is sent if an LSTableInfo was used to describe the table, 
- * and just before tableDidUnsubscribeAllItems:. Any data notification received after this call can be ignored.
- * <BR/>If an LSTableInfo was used to describe the table, then the notification will not be sent.
- *
- * @param tableKey Key of the related table, as returned by the subscription method.
- * @param itemPosition 1-based index of the item in the involved table.
- * @param itemName Name of the changed item, or null if an LSTableInfo was used to describe the table.
- */
-- (void) table:(LSSubscribedTableKey *)tableKey didUnsubscribeItemPosition:(int)itemPosition itemName:(NSString *)itemName;
-
-/**
- * Notification of the unsubscription of all the items in the table. The unsubscription may be subsequent to an unsubscribeTable call
- * or to the closure of the connection. There is no guarantee that this call will not be followed by some
- * further update notifications. Such extra calls should be ignored.
- *
- * @param tableKey Key of the related table, as returned by the subscription method.
- */
-- (void) tableDidUnsubscribeAllItems:(LSSubscribedTableKey *)tableKey;
-
-
-@end
