/**
 * 
 */
package com.anyoption.android.app;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.results.OptionsMethodResult;
import com.anyoption.json.util.OptionItem;

/**
 * @author IdanZ
 *
 */
public class ChangePersonalDetailsActivity extends ChangePersonalDetailsCommonActivity{
	
	protected static final String DIALOG_DATE_PATTERN = "EEEEE, dd/MMM/yyyy";
	protected static final DateFormat DIALOG_DATE_FORMAT = new SimpleDateFormat(DIALOG_DATE_PATTERN);

	protected Dialog dateDialog;
	protected int birthYear;
    protected int birthMonth;
    protected int birthDay;
    protected Date currentServerTime;
    
    // Used to validate the birth date while it's entered in the date dialog.
    private DatePicker datePicker;
    private int lastSavedYear;
	private int lastSavedMonth;
	private int lastSavedDay;
    
	@Override
	public void init(User user) {
		MethodRequest request = new MethodRequest();
	    OptionsMethodResult result = new OptionsMethodResult(); 
		Utils.requestService(this, responseH, "stateCallBack", request, result, "getStatesOptions");
		
		AnyoptionApplication ap = ((AnyoptionApplication) getApplication());
		// Get all the countries to be shown to the user and the selected one.
		Country[] countries;
		if (Utils.isRegulated(this)) {
			countries = Utils.getRegulatedCountries(this);
		} else {
			countries = Utils.getNonRegulatedCountries(this);
		}
		
		Spinner countriesSpinner = (Spinner) findViewById(R.id.spinnerCountry);	
		ArrayAdapter<Country> spinnerCountriesArrayAdapter = new ArrayAdapter<Country>(this, android.R.layout.simple_spinner_item, countries);
        spinnerCountriesArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        countriesSpinner.setAdapter(spinnerCountriesArrayAdapter);    	                        
	        
        //Add state spinner when selecting USA 
        countriesSpinner.setOnItemSelectedListener(
    	    new AdapterView.OnItemSelectedListener() {
    	    	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {          	    	
    	    		ArrayAdapter<Country> adapter = (ArrayAdapter<Country>) parent.getAdapter();        	    		
    	    		Country c = adapter.getItem(pos);  
    	            //Mobile phone prefix 
    	            TextView mobilePhonePrefix   = (TextView)findViewById(R.id.personal_details_change_mobileprefix);
    	            TextView addPhonePrefix   = (TextView)findViewById(R.id.personal_details_change_phoneprefix);
    	            String phoenCode = c.getPhoneCode();
    	            User u = ((AnyoptionApplication)getApplication()).getUser();
    	            if (!Utils.isEtraderProject(context) && null != u && u.getUserName().equalsIgnoreCase(u.getEmail())) {
	    	            if (phoenCode.trim().length() > 3) { //if its more then 3 digits we want to show only the first digit
	    	            	phoenCode = phoenCode.substring(0, 1);
	    	            }
    	            }
    	            mobilePhonePrefix.setText(getResources().getString(R.string.personal_details_change_mobile, "+" + phoenCode));
    	            addPhonePrefix.setText(getResources().getString(R.string.personal_details_change_phone, "+" + phoenCode));
    	            if (c.getId() == Constants.COUNTRY_UNITED_STATES_ID){
    	            	Spinner  spinnerStates = (Spinner) findViewById(R.id.spinnerState);
    	            	spinnerStates.setVisibility(View.VISIBLE);
    	            } else {
    	            	Spinner  spinnerStates = (Spinner) findViewById(R.id.spinnerState);
    	            	spinnerStates.setVisibility(View.GONE);
    	            }
				}
				public void onNothingSelected(AdapterView<?> arg0) {												
				}
    	    }
    	);                               
		
        ((EditText)findViewById(R.id.personal_details_change_city)).setText(user.getCityName());
        
		// Set country spinner
		int countryPosition = spinnerCountriesArrayAdapter.getPosition(ap.getCountries().get(user.getCountryId()));
		countriesSpinner.setSelection(countryPosition);
		 
		 if (null == user.getTimeBirthDate()) {
			 findViewById(R.id.rowEditDateOfBirth).setVisibility(View.VISIBLE);
			 findViewById(R.id.rowDateOfBirth).setVisibility(View.GONE);
			 Button dateOfBirth = (Button)findViewById(R.id.buttonDateOfBirth);
			 dateOfBirth.setText(getString(R.string.register_birthdate));                                
			 // add a click listener to the date of birth
			 dateOfBirth.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	                showDialog(Constants.DIALOG_DATE_ID);
	            }
			 });        
			 //Set default date (01/01/1980)
			 birthDay = 1;
	       	 birthMonth = Calendar.JANUARY;
	       	 birthYear = 1980;
	       	 
	       	 lastSavedDay = birthDay;
	       	 lastSavedMonth = birthMonth;
	       	 lastSavedYear = birthYear;
		 }
		 
		 if (null == user.getGenderTxt()) {
			 findViewById(R.id.rowEditGender).setVisibility(View.VISIBLE);
			 findViewById(R.id.rowGender).setVisibility(View.GONE);
		 }
		 
		 if (user.getUserName().equalsIgnoreCase(user.getEmail())) { //new user
			 ((TextView)findViewById(R.id.personal_details_email)).setText(user.getEmail());
			 findViewById(R.id.rowEmail).setVisibility(View.VISIBLE);
			 findViewById(R.id.rowEditEmail).setVisibility(View.GONE);
			 findViewById(R.id.rowEditUsername).setVisibility(View.GONE);
		 }
		 
		 currentServerTime = Utils.getServerTimeOnUpdate();
	}
	
	public void stateCallBack(Object result) {
      Spinner spinner = (Spinner) findViewById(R.id.spinnerState);
      fillSpinner(spinner, result);
	}
	
	public void fillSpinner(Spinner spinner, Object result) {
    	OptionsMethodResult omr = (OptionsMethodResult) result;
        ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, omr.getOptions());  
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner.setAdapter(spinnerArrayAdapter);
        
        //set state spinner
        OptionItem stateItem = null;
        for (int i = 0 ; i < omr.getOptions().length ; i++) {
        	if (omr.getOptions()[i].getId() == user.getState()) {
        		stateItem = omr.getOptions()[i];
        		break;
        	}
        }
        if (null != stateItem) {
        	int statePosition = spinnerArrayAdapter.getPosition(stateItem);
        	spinner.setSelection(statePosition);
        }
    }

	@Override
	public String validateAndSet(String error, EditText phone) {
		EditText city = (EditText)findViewById(R.id.personal_details_change_city);
		
		//City
		String citytxt = city.getText().toString();
		if (!citytxt.equalsIgnoreCase("") && citytxt.length() != 0) {
			error = Utils.validateLettersOnlyAndEmpty(context, citytxt, getString(R.string.register_city));
	    	if (error!= null){
	    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
	    		return error;
	    	}
	    	user.setCityName(citytxt);
		}
    	
    	Spinner spinnerCountry = (Spinner) findViewById(R.id.spinnerCountry);
    	
    	user.setCountryId(((Country)spinnerCountry.getAdapter().getItem(spinnerCountry.getSelectedItemPosition())).getId());
    	if (0 != user.getCountryId() && user.getCountryId() == Constants.COUNTRY_UNITED_STATES_ID){
        	Spinner spinnerState = (Spinner) findViewById(R.id.spinnerState);
        	user.setState(((OptionItem)spinnerState.getAdapter().getItem(spinnerState.getSelectedItemPosition())).getId());
    	}
    	
    	RadioButton RadioGenderMale = (RadioButton)findViewById(R.id.radioMale); 
    	user.setGender(RadioGenderMale.isChecked()? "M" : "F");

		Button dateOfBirth = (Button)findViewById(R.id.buttonDateOfBirth);
    	if (!dateOfBirth.getText().toString().equalsIgnoreCase(getString(R.string.register_birthdate))){
        	user.setBirthDayUpdate(String.valueOf(birthDay));
        	user.setBirthMonthUpdate(String.valueOf(birthMonth + 1));
        	user.setBirthYearUpdate(String.valueOf(birthYear));
        	
        	Calendar c = Calendar.getInstance();
        	c.clear();
        	c.set(birthYear, birthMonth, birthDay);
        	
        	user.setTimeBirthDate(c.getTime());
    	}
    	
    	EditText street = (EditText)findViewById(R.id.personal_details_change_streetaddress);
    	//Street
    	String streetTxt = street.getText().toString();
		if (!streetTxt.equalsIgnoreCase("") && streetTxt.length() != 0) {
	    	error = Utils.validateStreetAndEmpty(context, streetTxt, getString(R.string.register_street));
	    	if (error!= null){
	    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
	    		return error;
	    	}	
	    	user.setStreet(streetTxt);
		}
		return error;
	}
	
	// the callback received when the user "sets" the date in the dialog
    private DatePickerDialog.OnDateSetListener mDateSetListener =
		    new DatePickerDialog.OnDateSetListener() {
		
		        @Override
				public void onDateSet(DatePicker view, int year, 
		                              int monthOfYear, int dayOfMonth) {
		        	birthYear = datePicker.getYear();
		        	birthMonth = datePicker.getMonth();
		        	birthDay = datePicker.getDayOfMonth();
		        	
		        	Button dateOfBirth = (Button)findViewById(R.id.buttonDateOfBirth);
		        	dateOfBirth.setText(new StringBuilder()
		        		// Month is 0 based so add 1
		            		.append(birthMonth + 1).append("-")
		            		.append(birthDay).append("-")
		            		.append(birthYear).append(" "));
		        }
		    };
		    
    @Override
    protected Dialog onCreateDialog(int id) {
    	Dialog dialog;
    	
		switch (id) {
		case Constants.DIALOG_DATE_ID:
			dialog = new DatePickerDialog(this, mDateSetListener, birthYear,
					birthMonth, birthDay);
			// Create custom DatePicker.
			datePicker = new DatePicker(this);
			
			// Disable the keyboard while the dialog is displayed.
			datePicker.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
			
			datePicker.init(birthYear, birthMonth, birthDay, new OnDateChangedListener() {
				
				@Override
				public void onDateChanged(DatePicker view, int year, int monthOfYear,
						int dayOfMonth) {
					if (isBirthDateValid(year, monthOfYear, dayOfMonth)) {
						// If the date is valid, we save the values.
						lastSavedYear = year;
						lastSavedMonth = monthOfYear;
						lastSavedDay = dayOfMonth;
						
						// Set the date as title of the dialog.
						((DatePickerDialog) dateDialog).setTitle(getFormattedDate(year, monthOfYear, dayOfMonth));
						
					} else {
						// If the date is not valid, last valid date is
						// displayed.
						view.updateDate(lastSavedYear, lastSavedMonth,
								lastSavedDay);
					}
				}
			});
			
			// Add custom date picker to the dialog.
			((DatePickerDialog) dialog).setView(datePicker);
			
			// Set the date as title of the dialog.
			((DatePickerDialog) dialog).setTitle(getFormattedDate(birthYear, birthMonth, birthDay));
			
			dateDialog = dialog;
			break;
		default:
			dialog = super.onCreateDialog(id);
	    }
        return dialog;
    }
    
    @Override
    public void onResume() {
    	 super.onResume();
    	if (null != dateDialog && dateDialog.isShowing()) {
    		dismissDialog(Constants.DIALOG_DATE_ID);
    	}
    }
    
    /**
     * Handle on prepare dialog event
     * Called every time the dialog is opened  	
     */
    protected void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
        case Constants.DIALOG_DATE_ID:
            ((DatePickerDialog) dialog).updateDate(birthYear, birthMonth, birthDay);            
            break;
   		default:
   			break;
        }	    	
    }
    
	/**
	 * Returns true if the user is older than 18.
	 */
	private boolean isBirthDateValid(int year, int month, int dayOfMonth) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(year, month, dayOfMonth);
		calendar.add(Calendar.YEAR, 18);
		
		Calendar calendarNow = Calendar.getInstance();
		calendarNow.setTime(currentServerTime);

		return !calendar.after(calendarNow);
	}
	
	private static String getFormattedDate(int year, int month, int day) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, day);
		return DIALOG_DATE_FORMAT.format(cal.getTime());
	}
    
}
