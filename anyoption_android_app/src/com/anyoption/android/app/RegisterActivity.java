package com.anyoption.android.app;

import java.util.HashMap;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Register;
import com.anyoption.common.beans.base.Country;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.requests.RegisterAttemptMethodRequest;
import com.anyoption.json.results.CountryIdMethodResult;

public class RegisterActivity extends RegisterCommonActivity {
	@Override
	public void init() {
		MethodRequest request = new MethodRequest();
//        OptionsMethodResult result = new OptionsMethodResult();        
//        Utils.requestService(this, responseH, "currencyCallBack", request, result, "getSkinCurrenciesOptions");        
//        Utils.requestService(this, responseH, "stateCallBack", request, result, "getStatesOptions");
        CountryIdMethodResult countryIdResult = new CountryIdMethodResult();
        Utils.requestService(this, responseH, "getCountryIdCallBack", request, countryIdResult, "getCountryId");
        
		TextView tvEmailToolTip = (TextView) findViewById(R.id.registerEmailToolTip);
		tvEmailToolTip.setText(Html.fromHtml(getString(R.string.register_email_tooltip)));
		
		editTextEmail.setOnFocusChangeListener(new OnFocusChangeListener() {			
        	public void onFocusChange(View v, boolean hasFocus) {
        		if(!hasFocus) {
        			if (((EditText) v).getText().toString().length() > 0) {
	    				String error = null;
	    				error = Utils.validateEmailAndEmpty(context, editTextEmail.getText().toString(), Constants.EMPTY_STRING);  
	    				if (null == error) {
	    					insertRegisterAttempt();
	    					showViewById(R.id.imageViewValidEmail, View.VISIBLE);
	    				} else {
	    					showAlert(error);
	    					showViewById(R.id.imageViewValidEmail, View.GONE);
    				}
        			}
        		}
        			
			}
		});
		
		editTextMobile.setOnFocusChangeListener(new OnFocusChangeListener() {			
        	public void onFocusChange(View v, boolean hasFocus) {
        		if(!hasFocus) {
        			if (((EditText) v).getText().toString().length() > 0) {
	    				String error = null;
	    				error = Utils.validateDigitsOnlyAndEmptyAndLength(context, editTextMobile.getText().toString(), getString(R.string.register_mobilephone));
	    				if (null == error) {
	    					insertRegisterAttempt();
	    					showViewById(R.id.imageViewValidMobilePhone, View.VISIBLE);
	    				} else {
	    					showAlert(error);
	    					showViewById(R.id.imageViewValidMobilePhone, View.GONE);
    				}
        			}
        		}
        			
			}
		});
		
		EditText firstName = (EditText) findViewById(R.id.editTextFirstname);
		firstName.setOnFocusChangeListener(new OnFocusChangeListener() {			
        	public void onFocusChange(View v, boolean hasFocus) {
        		if (!hasFocus) {
        			if (((EditText) v).getText().toString().length() > 0) {
	    				String error = null;
	    				error = Utils.validateNameAndEmpty(context, ((EditText) v).getText().toString(), Constants.EMPTY_STRING);  
	    				if (null == error) {
	    					showViewById(R.id.imageViewValidFirstName, View.VISIBLE);
	    				} else {
	    					showAlert(error);
	    					showViewById(R.id.imageViewValidFirstName, View.GONE);
	    				}
        			}
        		}
        			
			}
		});
		
		EditText lastName = (EditText) findViewById(R.id.editTextLastname);
		lastName.setOnFocusChangeListener(new OnFocusChangeListener() {			
        	public void onFocusChange(View v, boolean hasFocus) {
        		if (!hasFocus) {
        			if (((EditText) v).getText().toString().length() > 0) {
	    				String error = null;
	    				error = Utils.validateNameAndEmpty(context, ((EditText) v).getText().toString(), Constants.EMPTY_STRING);  
	    				if (null == error) {
	    					showViewById(R.id.imageViewValidLastName, View.VISIBLE);
	    				} else {
	    					showAlert(error);
	    					showViewById(R.id.imageViewValidLastName, View.GONE);
	    				}
        			}
        		}
        			
			}
		});
		
		EditText password = (EditText) findViewById(R.id.editTextPassword);
		password.setOnFocusChangeListener(new OnFocusChangeListener() {			
        	public void onFocusChange(View v, boolean hasFocus) {
        		if (!hasFocus) {
        			if (((EditText) v).getText().toString().length() > 0) {
	    				String error = null;
	    				error = Utils.validatePassword(context, ((EditText) v).getText().toString());  
	    				if (null == error) {
	    					showViewById(R.id.imageViewValidPassword, View.VISIBLE);
	    				} else {
	    					showAlert(error);
	    					showViewById(R.id.imageViewValidPassword, View.GONE);
	    				}
        			}
        		}
        			
			}
		});
		
		EditText password2 = (EditText) findViewById(R.id.editTextPassword2);
		password2.setOnFocusChangeListener(new OnFocusChangeListener() {			
        	public void onFocusChange(View v, boolean hasFocus) {
        		if (!hasFocus) {
        			if (((EditText) v).getText().toString().length() > 0) {
	    				String error = null;
	    				error = Utils.validateRetypePassword(context, ((EditText) v).getText().toString(), ((EditText) findViewById(R.id.editTextPassword)).getText().toString());  
	    				if (null == error) {
	    					showViewById(R.id.imageViewValidPassword2, View.VISIBLE);
	    				} else {
	    					showAlert(error);
	    					showViewById(R.id.imageViewValidPassword2, View.GONE);
	    				}
        			}
        		}
        			
			}
		});
	}
	
	public void currencyCallBack(Object result) {
		Spinner spinner = (Spinner) findViewById(R.id.spinnerCurrency);
		fillSpinner(spinner, result);
	}

	public void stateCallBack(Object result) {
		Spinner spinner = (Spinner) findViewById(R.id.spinnerState);
		fillSpinner(spinner, result);
	}
	
	@Override
	public HashMap<String, String> validateCityDetails() {
//		String city = ((EditText) findViewById(R.id.editTextCity)).getText().toString();
//    	String error = Utils.validateLettersOnlyAndEmpty(context, city, getString(R.string.register_city));
    	HashMap<String, String> result = new HashMap<String, String>();
    	result.put(CITY_DETAILS_ERROR, null);
//    	result.put(CITY_DETAILS_CITY, city);
//    	result.put(CITY_DETAILS_STREETNO, null);
    	return result;
	}
	
	@Override
	public void setUserUniqueDetalis(Register user) {
//		Spinner spinnerCountry = (Spinner) findViewById(R.id.spinnerCountry);
//    	user.setCountryId(((Country)spinnerCountry.getAdapter().getItem(spinnerCountry.getSelectedItemPosition())).getId());
//    	if (0 != user.getCountryId() && user.getCountryId() == Constants.COUNTRY_UNITED_STATES_ID){
//        	Spinner spinnerState = (Spinner) findViewById(R.id.spinnerState);
//        	user.setStateId(((OptionItem)spinnerState.getAdapter().getItem(spinnerState.getSelectedItemPosition())).getId());	
//    	}
    	    	
//    	Spinner spinnerCurrency = (Spinner) findViewById(R.id.spinnerCurrency);
//    	user.setCurrencyId(((OptionItem)spinnerCurrency.getAdapter().getItem(spinnerCurrency.getSelectedItemPosition())).getId());
    	
		user.setContactBySms(true);
		user.setUserName(user.getEmail());
		user.setCountryId(countryId);
    	
	}
	
	 public void getCountryIdCallBack(Object result) {
	    	Log.d(getPackageName(), "getCountryIdCallBack");
	    	CountryIdMethodResult omr = (CountryIdMethodResult) result;
	    	countryId = omr.getCountryId();

	    	// If the country is USA, the user must not be able to register.
	    	if(countryId != Constants.COUNTRY_UNITED_STATES_ID) {
	    		findViewById(R.id.linearLayout1).setVisibility(View.VISIBLE); 
	    	} else {
	    		AlertDialog errorDialog = Utils.showAlert(context, Constants.EMPTY_STRING, context.getString(R.string.error_us_citizens_not_accept), context.getString(R.string.alert_title));
	    		errorDialog.setOnDismissListener(new OnDismissListener() {
					
					@Override
					public void onDismiss(DialogInterface dialog) {
						finish();
					}
				});
	    		return;
	    	}
	    	
	    	final AlertDialog.Builder ad = new AlertDialog.Builder(RegisterActivity.this);
	        final ArrayAdapter<Country> spinnerCountriesArrayAdapter = new ArrayAdapter<Country>(RegisterActivity.this,
	                android.R.layout.simple_spinner_item, ((AnyoptionApplication)getApplication()).getCountriesArray());  

	        int countryPosition = spinnerCountriesArrayAdapter.getPosition(((AnyoptionApplication)getApplication()).getCountries().get(countryId));
			ad.setSingleChoiceItems(spinnerCountriesArrayAdapter, countryPosition, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface di, int pos) {
					
					ArrayAdapter<Country> adapter = spinnerCountriesArrayAdapter;      	
      	    		Country c = adapter.getItem(pos);  
      	            //a2 + phone code 
      	            Button countryButton = (Button)findViewById(R.id.buttonCountry);
      	            String phoenCode = c.getPhoneCode();
      	            if (phoenCode.trim().length() > 3) { //if its more then 3 digits we want to show only the first digit
      	            	phoenCode = phoenCode.substring(0, 1);
      	            }
      	            countryButton.setText(c.getA2() + " +" + phoenCode);
      	            countryId = c.getId();
      	            di.dismiss();
				}
			});
	    	Button countryButton = (Button)findViewById(R.id.buttonCountry);
	    	countryButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					Log.d(getPackageName(), "country click");
					
					ad.show();
				}
			});    	
    		Country c = spinnerCountriesArrayAdapter.getItem(countryPosition);  
            //a2 + phone code       	            
            countryButton.setText(c.getA2() + " +" + c.getPhoneCode());
	    	
			
	          //set default country spinner
	   		
//	   		countriesSpinner.setSelection(countryPosition);	 
	                          
	          //Add state spinner when selecting USA 
//	          Spinner spinnerCountries = (Spinner) findViewById(R.id.spinnerCountry);
//	          spinnerCountries.setOnItemSelectedListener(
//	          	    new AdapterView.OnItemSelectedListener() {
//	          	    	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {          	    	
//	          	    		ArrayAdapter<Country> adapter = (ArrayAdapter<Country>) parent.getAdapter();        	    		
//	          	    		Country c = adapter.getItem(pos);  
//	          	            //Mobile phone prefix 
//	          	            Button countryButton   = (Button)findViewById(R.id.buttonCountry);        	            
//	          	          countryButton.setText("+" + c.getPhoneCode());
//	          	            if (c.getId() == Constants.COUNTRY_UNITED_STATES_ID){
//	          	            	Spinner  spinnerStates = (Spinner) findViewById(R.id.spinnerState);
//	          	            	spinnerStates.setVisibility(View.VISIBLE);
//	          	            } else {
//	          	            	Spinner  spinnerStates = (Spinner) findViewById(R.id.spinnerState);
//	          	            	spinnerStates.setVisibility(View.GONE);
//	          	            }
//	          	          countryId = c.getId();
//	  					}
//	  					public void onNothingSelected(AdapterView<?> arg0) {												
//	  					}
//	          	    }
//	          );
	    }
	 
	public void setInsertRegisterRequestAttempt(RegisterAttemptMethodRequest request) {
//		Spinner spinnerCountry = (Spinner) findViewById(R.id.spinnerCountry);    	
//	    long countryId = ((Country)spinnerCountry.getAdapter().getItem(spinnerCountry.getSelectedItemPosition())).getId();
		request.setCountryId(countryId);
		request.setContactBySMS(true);
	}

	@Override
	public String mobilePrefixValidate() {
		return null;
	}

	@Override
	public String validateNumId() {
		return null;
	}


	public void showViewById(int id, int visibilityState) {
		findViewById(id).setVisibility(visibilityState);
	}


	public void showAlert(String error) {
		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));
	}

	@Override
	/**
	 * only for et
	 */
	public void validateDateOfBirth() {
	}

	@Override
	/**
	 * only for et
	 */
	public String validateStreet() {
		return null;
	}

	@Override
	/**
	 * only for et
	 */
	public String validateUserName() {
		return null;
	}	
}
