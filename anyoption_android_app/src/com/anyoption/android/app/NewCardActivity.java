package com.anyoption.android.app;


import com.anyoption.android.app.util.Constants;
import com.anyoption.common.beans.base.CreditCard;

/**
 * @author KobiM
 *
 */
public class NewCardActivity extends NewCardCommonActivity {
            
    /** Called when the activity is first created. */


	@Override
	public String setHolderId(CreditCard card) {
		card.setHolderId(Constants.NO_ID_NUM);
		return null;		
	}
}