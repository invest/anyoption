/**
 * 
 */
package com.anyoption.android.app;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.anyoption.android.app.BankWireWithdrawCommonActivity;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Wire;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.results.OptionsMethodResult;
import com.anyoption.json.util.OptionItem;

/**
 * @author IdanZ
 *
 */
public class BankWireWithdrawActivity extends BankWireWithdrawCommonActivity {
	
	@Override
	public void init() {
		AnyoptionApplication app = (AnyoptionApplication) getApplication();
		if(app.getSkinId() == Skin.SKIN_GERMAN || app.getSkinId() == Skin.SKIN_REG_DE){
			EditText branchAddress= (EditText) findViewById(R.id.editTextBranchAddress);
			EditText branchNumber = (EditText) findViewById(R.id.editTextBranch);		
			branchAddress.setVisibility(View.GONE);
			branchNumber.setVisibility(View.GONE);
		}
		if (app.getSkinId() == Skin.SKIN_CHINESE || app.getSkinId() == Skin.SKIN_CHINESE_VIP) {
	        Utils.requestService(this, responseH, "bankWireWithdrawCallBack", new MethodRequest(), new OptionsMethodResult(), "getBankWireWithdrawOptions");
	        EditText branchAddress= (EditText) findViewById(R.id.editTextBranchAddress);
	        EditText swift = (EditText) findViewById(R.id.editTextSwift);
	        EditText iban = (EditText) findViewById(R.id.editTextIban);
	        EditText bankName = (EditText) findViewById(R.id.editTextBankName);
	        EditText branchName = (EditText) findViewById(R.id.editTextBranchName);
	        TextView textViewBeneficiary = (TextView) findViewById(R.id.textViewBeneficiary);
	        branchAddress.setVisibility(View.GONE);
	        swift.setVisibility(View.GONE);
	        iban.setVisibility(View.GONE);
	        bankName.setVisibility(View.GONE);
	        branchName.setVisibility(View.VISIBLE);
	        textViewBeneficiary.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	public String setParams(Wire wire, String error) {
		EditText beneficiary = (EditText) findViewById(R.id.editTextBeneficiary);
		EditText swift = (EditText) findViewById(R.id.editTextSwift);
		EditText iban = (EditText) findViewById(R.id.editTextIban);
		EditText bankName = (EditText) findViewById(R.id.editTextBankName);
		EditText branchAddress= (EditText) findViewById(R.id.editTextBranchAddress);
		EditText branchName = (EditText) findViewById(R.id.editTextBranchName);
		
		AnyoptionApplication app = (AnyoptionApplication) getApplication();
		//beneficiary
		error = Utils.isFieldEmpty(this, beneficiary.getText().toString(), getString(R.string.bank_wire_beneficiary_name));
		if (error != null) {
			beneficiary.requestFocus();
			Utils.showAlert(this, "", error,  this.getString(R.string.alert_title));  
    		return error;
		}
		if (app.getSkinId() != Skin.SKIN_CHINESE && app.getSkinId() != Skin.SKIN_CHINESE_VIP) {
			//swift
			error = Utils.isFieldEmpty(this, swift.getText().toString(), getString(R.string.bank_wire_swift));
			if (error != null) {
				swift.requestFocus();
				Utils.showAlert(this, "", error,  this.getString(R.string.alert_title));
	    		return error;
			}
			//iban
			error = Utils.isFieldEmpty(this, iban.getText().toString(), getString(R.string.bank_wire_iban));
			if (error != null) {
				iban.requestFocus();
				Utils.showAlert(this, "", error,  this.getString(R.string.alert_title));
	    		return error;
			}
		}
		//bank name and branch name
		String bankNameText = bankName.getText().toString();
		boolean needGetFromEditTextBankName = true;
		if (app.getSkinId() == Skin.SKIN_CHINESE || app.getSkinId() == Skin.SKIN_CHINESE_VIP) {
			//branchName
			error = Utils.validateChineseLettersOnlyAndEmpty(this, branchName.getText().toString(), getString(R.string.bank_wire_branch_name));
			if (error != null) {
				branchName.requestFocus();
				Utils.showAlert(this, "", error,  this.getString(R.string.alert_title));  
	    		return error;
			}
			//bank name
			Spinner spinner = (Spinner) findViewById(R.id.spinnerBankName);
			long bankId = ((OptionItem)spinner.getSelectedItem()).getId();
			if (bankId != Constants.BANK_WIRE_OTHER_BANK) {
				bankNameText = ((OptionItem)spinner.getSelectedItem()).getValue();
				needGetFromEditTextBankName = false;
			}
			wire.setBankId(String.valueOf(bankId));
			wire.setBranchName(branchName.getText().toString());
		}
		//bank name
		if (needGetFromEditTextBankName) {
			error = Utils.isFieldEmpty(this, bankNameText, getString(R.string.bank_wire_bank_name));
			if (error != null) {
				bankName.requestFocus();
				Utils.showAlert(this, "", error,  this.getString(R.string.alert_title));
	    		return error;
			}
		}
		//bank address
		if(app.getSkinId() != Skin.SKIN_REG_DE && app.getSkinId() != Skin.SKIN_GERMAN && 
				app.getSkinId() != Skin.SKIN_CHINESE && app.getSkinId() != Skin.SKIN_CHINESE_VIP){
			error = Utils.isFieldEmpty(this, branchAddress.getText().toString(), getString(R.string.bank_wire_branch_address));
			if (error != null) {
				branchAddress.requestFocus();
				Utils.showAlert(this, "", error,  this.getString(R.string.alert_title));
	    		return error;
			}
		}
		wire.setBeneficiaryName(beneficiary.getText().toString());
		wire.setSwift(swift.getText().toString());
		wire.setIban(iban.getText().toString());
		wire.setBankNameTxt(bankNameText);
		if(branchAddress.getText().toString().length() == 0){
			wire.setBranchAddress(null);
		} else {
			wire.setBranchAddress(branchAddress.getText().toString());
		}
		return error;
		
	}
	
	/**
	 * bank wire withdraw CallBack.
	 * @param result
	 */
	public void bankWireWithdrawCallBack(Object result) {
		Log.d("BankWireWithdrawActivity", "bankWireWithdrawCallBack");
		Spinner spinner = (Spinner) findViewById(R.id.spinnerBankName);
		fillSpinner(spinner, result);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				long bankNameId = ((OptionItem)parent.getAdapter().getItem(parent.getSelectedItemPosition())).getId();
				EditText bankName = (EditText) findViewById(R.id.editTextBankName);
				Log.d("BankWireWithdrawActivity", "bankNameId = " + bankNameId);
				if (bankNameId == Constants.BANK_WIRE_OTHER_BANK) {
			        bankName.setVisibility(View.VISIBLE);
				} else {
					bankName.setVisibility(View.GONE);
				}		
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});
        spinner.setVisibility(View.VISIBLE);
	}
	
	/**
	 * fill spinner
	 * @param spinner
	 * @param result
	 */
	public void fillSpinner(Spinner spinner, Object result) {
		OptionsMethodResult omr = (OptionsMethodResult) result;
		ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, omr.getOptions());
		spinnerArrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spinner.setAdapter(spinnerArrayAdapter);
	}
	
}
