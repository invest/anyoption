package com.copyop.migration;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;

import com.copyop.common.managers.ManagerBase;
import com.copyop.common.util.JndiMocker;

 
public class UsersPasswordReset {
	public static void main(String args[]) {
		
		Properties props = new Properties();
		//String propFileName = "passReset.properties";
		String propFileName = "avatarReset.properties";
 
		InputStream inputStream = UsersPasswordReset.class.getClassLoader().getResourceAsStream(propFileName);
		if (inputStream == null) {
			System.out.println("Properties file"+ propFileName+ " Not found in class path !");
		}		
		try {
			props.load(inputStream);
			String oracleUrl = props.getProperty("oracle.url");
			String oracleUsername = props.getProperty("oracle.username");
			String oraclePassword = props.getProperty("oracle.password");
			String oracleServiceName = props.getProperty("oracle.service.name");
			String cassandraUrlsCsv = props.getProperty("cassandra.urls.csv");
			String cassandraKeyspace = props.getProperty("cassandra.keyspace");
			boolean valid =
							(oracleUrl != null) && 
							(oracleUsername != null) && 
							(oraclePassword != null) && 
							(oracleServiceName != null);
			
			if(!valid) {
				System.out.println("Unsuficient info in properties file :");
				System.out.println(props);
			}
			
			ManagerBase.init(new String[]{cassandraUrlsCsv}, cassandraKeyspace);
			ManagerBase.getSession();
		
			JndiMocker.setUpJndiDataSource(oracleUrl, oracleUsername, oraclePassword, oracleServiceName);
			
			//UsersMigrationManager.generatePassword();
			UsersMigrationManager.generateAvatar();

		} catch (IOException e) {
			System.out.println("Error while loading propertis file");
			e.printStackTrace();
		/*} catch (SQLException e) {
			System.out.println("SQL problem");
			e.printStackTrace();*/
		} catch(Throwable e){
			e.printStackTrace();
			System.out.println("general error");
		}	finally {
			ManagerBase.closeSession();
			ManagerBase.closeCluster();
		}
	}
}
