package com.copyop.migration;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;

import com.copyop.common.managers.ManagerBase;
import com.copyop.common.util.JndiMocker;

 
public class FixStatistics {
	public static void main(String args[]) {
		
		Properties props = new Properties();
		String propFileName = "copyopMigration.properties";
 
		InputStream inputStream = FixStatistics.class.getClassLoader().getResourceAsStream(propFileName);
		if (inputStream == null) {
			System.out.println("Properties file"+ propFileName+ " Not found in class path !");
		}		
		try {
			props.load(inputStream);
			String cassandraUrlsCsv = props.getProperty("cassandra.urls.csv");
			String cassandraKeyspace = props.getProperty("cassandra.keyspace");
			String oracleUrl = props.getProperty("oracle.url");
			String oracleUsername = props.getProperty("oracle.username");
			String oraclePassword = props.getProperty("oracle.password");
			String oracleServiceName = props.getProperty("oracle.service.name");
			String userId  = props.getProperty("user.id");
			boolean valid = (cassandraUrlsCsv != null) && 
							(cassandraKeyspace != null) && 
							(oracleUrl != null) && 
							(oracleUsername != null) && 
							(oraclePassword != null) && 
							(oracleServiceName != null);
			
			if(!valid) {
				System.out.println("Unsuficient info in properties file :");
				System.out.println(props);
			}
			
			ManagerBase.init(new String[]{cassandraUrlsCsv}, cassandraKeyspace);
			ManagerBase.getSession();
		
			JndiMocker.setUpJndiDataSource(oracleUrl, oracleUsername, oraclePassword, oracleServiceName);
			if(userId != null) {
				UserStatisticsManager.updateUserStatistics(Long.parseLong(userId));
			} else {
				System.out.println("No user id specified");
			}
			
		} catch (IOException e) {
			System.out.println("Error while loading propertis file");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("SQL problem");
			e.printStackTrace();
		} catch(Throwable e){
			e.printStackTrace();
			System.out.println("general error");
		}	finally {
			ManagerBase.closeSession();
			ManagerBase.closeCluster();
		}
	}
}
