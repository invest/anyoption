package com.copyop.common.managers;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.copyop.common.dto.ProfileLink;
import com.copyop.common.enums.ProfileLinkChangeReasonEnum;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;

public class ProfileLinksManagerTest {

/*
 * Can't test cache
	@Test
	public void testProfilesOnline() {
		Set<Long> list = ProfilesOnlineManager.getUserIdsOnline();
		System.out.println(list);
		Assert.assertNotNull(list.iterator().next());
	}
*/
	@Test
	public void testProfileLinks() {
		long userId = 1;
		long destUserId = 2;

		ProfileLinksManager.insertWatchProfileLink(userId, destUserId, false, false);

//		List<ProfileLink> a = ProfileLinksManager.getProfileLinksByType(userId, ProfileLinkTypeEnum.COPY);
//		Assert.assertEquals(0, a.size());
//		ProfileLinksManager.insertProfileLinkHistory(a.get(0), ProfileLinkChangeReasonEnum.STOP_COPY);
//		List<ProfileLink> b = ProfileLinksManager.getProfileLinksByType(userId, ProfileLinkTypeEnum.COPIED_BY);
//		Assert.assertEquals(0, b.size());
//		List<ProfileLink> c = ProfileLinksManager.getProfileLinksByType(destUserId, ProfileLinkTypeEnum.COPY);
//		Assert.assertEquals(0, c.size());
//		List<ProfileLink> d = ProfileLinksManager.getProfileLinksByType(destUserId, ProfileLinkTypeEnum.COPIED_BY);
//		Assert.assertEquals(1, d.size());

		List<ProfileLink> e = ProfileLinksManager.getProfileLinksByType(userId, ProfileLinkTypeEnum.WATCH);
		Assert.assertEquals(1, e.size());
		List<ProfileLink> f = ProfileLinksManager.getProfileLinksByType(userId, ProfileLinkTypeEnum.WATCHED_BY);
		Assert.assertEquals(0, f.size());
		List<ProfileLink> g = ProfileLinksManager.getProfileLinksByType(destUserId, ProfileLinkTypeEnum.WATCH);
		Assert.assertEquals(0, g.size());
		List<ProfileLink> h = ProfileLinksManager.getProfileLinksByType(destUserId, ProfileLinkTypeEnum.WATCHED_BY);
		Assert.assertEquals(1, h.size());


		List<Long> userIdCopiers = ProfileLinksManager.getCopiersIds(userId);
		Assert.assertEquals(0, userIdCopiers.size());
		List<Long> destUserIdCopiers = ProfileLinksManager.getCopiersIds(destUserId);
		Assert.assertEquals(0, destUserIdCopiers.size());
		ArrayList<ProfileLink> userIdLinks = ProfileLinksManager.getProfileLinks(userId);
		Assert.assertEquals(0, userIdLinks.size());
		ArrayList<ProfileLink> destUserIdLinks = ProfileLinksManager.getProfileLinks(destUserId);
		Assert.assertEquals(1, destUserIdLinks.size());

		ProfileLinksManager.deleteCopyProfileLinkBatch(userId, destUserId, false, false);
		ProfileLinksManager.deleteWatchProfileLink(userId, destUserId, false, ProfileLinkChangeReasonEnum.STOP_WATCH, false, false);
	}
}
