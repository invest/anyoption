package com.copyop.common.managers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.copyop.common.dto.Profile;
import com.datastax.driver.core.utils.UUIDs;

public class ProfileManagerTest {
	
	@Test
	public void testifExists() {
		Assert.assertFalse(ProfileManager.profileExists(1));
		Assert.assertTrue(ProfileManager.profileExists(96429));
	}
	
	@Test
	public void testInsertProfile() {
		Profile p1 = new Profile (1, "testNickname", "testAvatar", "testFbId", false, false);
		ProfileManager.insertProfile(p1);
		Profile p2 = ProfileManager.getProfile(1);
		Assert.assertNotNull(p2);
	}

	@Test
	public void testInsertRiskAppetite() {
		long userId = 1;
		Profile p = ProfileManager.getProfile(userId);
		List<Float> ra = p.getRiskAppetite();
		if(ra == null ) {
			ra = new ArrayList<Float>();
		}
		int before = ra.size();
		ProfileManager.insertRiskAppetite(userId, (float)0.1);
		int after = ProfileManager.getProfile(userId).getRiskAppetite().size();
		Assert.assertEquals(before, after-1);
	}
	@Test
	public void testProfileNickname() {
		String b = ProfileManager.getProfileNickname(62509);
		Assert.assertEquals("marksonumesi", b);
	}
	
	@Test
	public void testIsTestProfile() {
		boolean b = ProfileManager.isTestProfile(62509);
		Assert.assertTrue(!b);
	}
	@Test
	public void testCoinsReset() {
		long l = ProfileManager.getCoinsRemainingConvertTime(62509);
		Assert.assertTrue(l>0);
	}

	
	@Test
	public void testLastTimeMostCopied() {
		
		ProfileManager.updateLastTimeMostCopied(1);
		Date date = ProfileManager.getLastTimeMostCopied(1);
		Assert.assertNotNull(date);
	}

	@Test
	public void testProfileUpdate() {
		UUID timeUuid = UUIDs.timeBased();
		ProfileManager.updateProfile(timeUuid, 62509);
		Profile p = ProfileManager.getProfile(62509);
		Assert.assertEquals(timeUuid, p.getTimeResetCoins());
	}
	@Test
	public void testProfileLockUnlock() {
		boolean b1 = ProfileManager.lockProfile(62509);
		Assert.assertTrue(b1);
		boolean b2 = ProfileManager.lockProfile(62509);
		Assert.assertTrue(!b2);
		ProfileManager.unlockProfile(62509);
		boolean b3 = ProfileManager.isLockedProfile(62509);
		Assert.assertTrue(!b3);
	}

}
