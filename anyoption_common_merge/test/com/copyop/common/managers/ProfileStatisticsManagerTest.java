package com.copyop.common.managers;

import junit.framework.Assert;

import org.junit.Test;

import com.copyop.common.dto.ProfileInvResultCopy;
import com.copyop.common.dto.ProfileInvResultSelf;
import com.copyop.common.enums.base.UserStateEnum;

public class ProfileStatisticsManagerTest {


	@Test
	public void testUpfateProfileStatistics() {
		long userId = Long.MAX_VALUE;
		// load initial state
		ProfileInvResultCopy pirc_before = ProfileStatisticsManager.getProfileInvResultCopy(userId, 40);
		ProfileInvResultSelf pirs_before = ProfileStatisticsManager.getProfileInvResultSelf(userId, 120, UserStateEnum.STATE_REGULAR);
		// insert new value
		ProfileStatisticsManager.insertProfileInvResultCopy(true, userId);
		ProfileStatisticsManager.insertProfileInvResultSelf(false, 1, userId);
		// get new state
		ProfileInvResultCopy pirc = ProfileStatisticsManager.getProfileInvResultCopy(userId, 40);
		ProfileInvResultSelf pirs = ProfileStatisticsManager.getProfileInvResultSelf(userId, 120, UserStateEnum.STATE_REGULAR);
		// compare
		Assert.assertTrue(pirc.getResults().size()>=pirc_before.getResults().size());
		Assert.assertTrue(pirs.getResults().size()>=pirs_before.getResults().size());
	}

}
