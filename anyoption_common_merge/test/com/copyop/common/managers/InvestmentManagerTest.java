package com.copyop.common.managers;

import junit.framework.Assert;

import org.junit.Test;

import com.copyop.common.dto.CopiedInvestment;

public class InvestmentManagerTest {

	@Test
	public void testCopiedInvestments() {
		Long userId = Long.MAX_VALUE;
		Long destUserId = 0l;
		Long marketId = 1l;
		InvestmentManager.updateCopiedInvestmentProfit(1, userId, marketId, destUserId);
		CopiedInvestment cp = InvestmentManager.getCopiedInvestment(userId, marketId, destUserId);
		int k = cp.getCopiedCount();
		cp.setCopiedCount(k+1);
		InvestmentManager.updateCopiedInvestment(cp, k);
		CopiedInvestment cp1 = InvestmentManager.getCopiedInvestment(userId, marketId, destUserId);
		Assert.assertEquals(k+1, cp1.getCopiedCount());
	}
}
