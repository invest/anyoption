package com.copyop.common.managers;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

public class CopyHistoryManagerTest {
	@Test
	public void testCopyHistory() {
		CopyHistoryManager.insertCopyHistory(1, 2);
		CopyHistoryManager.insertCopyHistory(1, 3);
		List<Long> l = CopyHistoryManager.getCopyHistoryOf(1);
		System.out.println(l);
		Assert.assertTrue(l.size()>=2);
	}
}
