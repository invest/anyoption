package com.copyop.common.util;

import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

public class JndiMocker {

		private static boolean jndiDataSourceSet = false;

		/*
		 * Creates initial context with datasource
		 * 
		 */
	    public static void setUpJndiDataSource(String server, String userName, String password, String serviceName) {
	    	if(!jndiDataSourceSet) {
		        // setup the jndi context and the datasource
		        try {
		            // Create initial context
		            System.setProperty(Context.INITIAL_CONTEXT_FACTORY,  "org.apache.naming.java.javaURLContextFactory");
		            System.setProperty(Context.URL_PKG_PREFIXES, "org.apache.naming");            
		            InitialContext ic = new InitialContext();
	
		            ic.createSubcontext("java:");
		            ic.createSubcontext("java:comp");
		            ic.createSubcontext("java:DefaultDS");
		            ic.createSubcontext("java:comp/env");
		            ic.createSubcontext("java:comp/env/jdbc");
		            ic.createSubcontext("java:comp/env/jdbc/anyoptiondb");
		            
		            // Construct DataSource
		            OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();
		            ds.setURL("jdbc:oracle:thin:@"+server+":1521/"+serviceName);
		            ds.setUser(userName);
		            ds.setPassword(password);

		            	// IL TEST
//		            ds.setURL("jdbc:oracle:thin:@dbtest.etrader.co.il:1521/etrader1");
//		            ds.setUser("etrader");
//		            ds.setPassword("superman#1");
		            
		            try{
		            	ic.unbind("java:comp/env/jdbc/anyoptiondb");
		            	ic.unbind("java:DefaultDS");
		            } catch (NamingException ex) {
		            	
		            }
		            ic.bind("java:comp/env/jdbc/anyoptiondb", ds);
		            ic.bind("java:DefaultDS", ds);
		            System.out.println("Loaded Oracle DS in JNDI");
		        } catch (NamingException ex) {
		            ex.printStackTrace();
		        } catch (SQLException e) {
					e.printStackTrace();
				}
		        jndiDataSourceSet = true;
	    	} else {
	    		System.out.println("jndiDataSourceSet already set up");
	    	}
	    }
}
