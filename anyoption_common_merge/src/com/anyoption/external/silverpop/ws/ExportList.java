//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.01.14 at 09:35:00 AM EET 
//


package com.anyoption.external.silverpop.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LIST_ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="LIST_DATE_FORMAT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EXPORT_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EXPORT_FORMAT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ADD_TO_STORED_FILES" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DATE_START" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DATE_END" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EXPORT_COLUMNS">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="COLUMN" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "listid",
    "listdateformat",
    "exporttype",
    "exportformat",
    "addtostoredfiles",
    "datestart",
    "dateend",
    "exportcolumns"
})
public class ExportList {

    @XmlElement(name = "LIST_ID")
    protected int listid;
    @XmlElement(name = "LIST_DATE_FORMAT", required = true)
    protected String listdateformat;
    @XmlElement(name = "EXPORT_TYPE", required = true)
    protected String exporttype;
    @XmlElement(name = "EXPORT_FORMAT", required = true)
    protected String exportformat;
    @XmlElement(name = "ADD_TO_STORED_FILES", required = true)
    protected String addtostoredfiles;
    @XmlElement(name = "DATE_START", required = true)
    protected String datestart;
    @XmlElement(name = "DATE_END", required = true)
    protected String dateend;
    @XmlElement(name = "EXPORT_COLUMNS", required = true)
    protected EXPORTCOLUMNS exportcolumns;

    /**
     * Gets the value of the listid property.
     * 
     */
    public int getLISTID() {
        return listid;
    }

    /**
     * Sets the value of the listid property.
     * 
     */
    public void setLISTID(int value) {
        this.listid = value;
    }

    /**
     * Gets the value of the listdateformat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLISTDATEFORMAT() {
        return listdateformat;
    }

    /**
     * Sets the value of the listdateformat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLISTDATEFORMAT(String value) {
        this.listdateformat = value;
    }

    /**
     * Gets the value of the exporttype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXPORTTYPE() {
        return exporttype;
    }

    /**
     * Sets the value of the exporttype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXPORTTYPE(String value) {
        this.exporttype = value;
    }

    /**
     * Gets the value of the exportformat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXPORTFORMAT() {
        return exportformat;
    }

    /**
     * Sets the value of the exportformat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXPORTFORMAT(String value) {
        this.exportformat = value;
    }

    /**
     * Gets the value of the addtostoredfiles property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADDTOSTOREDFILES() {
        return addtostoredfiles;
    }

    /**
     * Sets the value of the addtostoredfiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDTOSTOREDFILES(String value) {
        this.addtostoredfiles = value;
    }

    /**
     * Gets the value of the datestart property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDATESTART() {
        return datestart;
    }

    /**
     * Sets the value of the datestart property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDATESTART(String value) {
        this.datestart = value;
    }

    /**
     * Gets the value of the dateend property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDATEEND() {
        return dateend;
    }

    /**
     * Sets the value of the dateend property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDATEEND(String value) {
        this.dateend = value;
    }

    /**
     * Gets the value of the exportcolumns property.
     * 
     * @return
     *     possible object is
     *     {@link EXPORTCOLUMNS }
     *     
     */
    public EXPORTCOLUMNS getEXPORTCOLUMNS() {
        return exportcolumns;
    }

    /**
     * Sets the value of the exportcolumns property.
     * 
     * @param value
     *     allowed object is
     *     {@link EXPORTCOLUMNS }
     *     
     */
    public void setEXPORTCOLUMNS(EXPORTCOLUMNS value) {
        this.exportcolumns = value;
    }

}
