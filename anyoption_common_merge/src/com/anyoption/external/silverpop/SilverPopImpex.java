package com.anyoption.external.silverpop;

import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.external.silverpop.ws.RESULT;

public class SilverPopImpex {
	private static final Logger log = Logger.getLogger(SilverPopClient.class);
	
	final static String JOB_STATUS_CANCELED	= "CANCELED";
	final static String JOB_STATUS_ERROR	= "ERROR";
	final static String JOB_STATUS_COMPLETE	= "COMPLETE";
	
	String userName;
	String password;
	String url;
	String email;
	
	static long jobWaitTimeoutDefault	= 1800000;	// 30 minutes
	static int sleepTimeMilis1Default	= 60000;	// 1 minute
	
	public SilverPopImpex(String url, String userName, String password, String email) {
	    this.url = url;
	    this.userName = userName;
	    this.password = password;
	    this.email = email;
	}
	
	public String importExport(String fileName, String mapFile, int listId, long jobWaitTimeoutMillis, int sleepTimeMillis, Date from, boolean contacts) throws SilverPopException {
		SilverPopClient spc = null;
		String jsessionId = null;
		String filePath = null;
		try {
			spc = new SilverPopClient(url);
			jsessionId = spc.login(userName, password);
			spc.sendListToSilverPop(jsessionId, fileName, mapFile, email);
			
			RESULT res = spc.retrieveListFromSilverPop(jsessionId, listId, from, contacts);
			int jobId = res.getJOBID();
			filePath = res.getFILEPATH();
			int pathDelimiter = filePath.lastIndexOf("/");  
			if(pathDelimiter >= 0) {
				filePath = filePath.substring(pathDelimiter+1);
			}
			
			long startTime = System.currentTimeMillis();
			long currentTime = 0;
			while((currentTime - startTime) < jobWaitTimeoutMillis) {
				String status = spc.getJobStatus(jsessionId, jobId);
				if(status.equalsIgnoreCase(JOB_STATUS_CANCELED) ||   
						status.equalsIgnoreCase(JOB_STATUS_ERROR) || 
						status.equalsIgnoreCase(JOB_STATUS_COMPLETE)) {
					log.debug("Job status:"+status+" time spent:"+(currentTime-startTime)+"ms");
					return filePath;
				}
				try {
					Thread.sleep(sleepTimeMillis);
				} catch (InterruptedException e) {
				}
				currentTime = System.currentTimeMillis();
			}
			
			String msg = "Job "+jobId+ " timed out. Waited :"+jobWaitTimeoutMillis; 
			log.debug(msg);
			throw new SilverPopException(msg);
		} finally {
			if(spc != null && jsessionId != null) {
				try {
					spc.logout(jsessionId);
					log.debug("Logged out successfully.");
				} catch (SilverPopException e) {
					log.debug("Can't logout :"+jsessionId);
				}
			}
		}

	}

}
