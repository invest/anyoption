package com.anyoption.external.silverpop;

public class SilverPopException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -893601794168433850L;
	public SilverPopException(String msg) {
		super(msg);
	}
}
