package com.anyoption.external.silverpop;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.external.silverpop.ws.Envelope;
import com.anyoption.external.silverpop.ws.RESULT;

public class SilverPopClient {
	private static final Logger log = Logger.getLogger(SilverPopClient.class);
	private static final String SUCCESS = "true";
	String url;
	
    private Marshaller marshaller = null;
    private Unmarshaller unmarshaller = null;
    
	private SilverPopClient() throws SilverPopException{
		try{
            JAXBContext context = JAXBContext.newInstance("com.anyoption.external.silverpop.ws");
            marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            // pretty print
            //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            //marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            unmarshaller = context.createUnmarshaller();
		}catch(JAXBException ex) {
			String msg = "Cannot create jaxb context ";
			log.debug(msg+ex.getMessage());
			throw new SilverPopException(msg);
		}
	}
	
	public SilverPopClient(String url) throws SilverPopException {
		this();
		this.url = url;
	}
	
	public Envelope sendRequest(Envelope en, String jsessionId) throws SilverPopException {
		String fullUrl = url;
		if(jsessionId != null) {
			fullUrl = url + ";jsessionid=" +jsessionId;
		}
		try {
			String responseString = executePOSTRequest(fullUrl, marshalToString(en));
			Envelope response = unmarshal(responseString);
			return response;
		} catch (IOException e) {
			String msg ="IOException while sending request ";
			log.debug(msg+e.getMessage());
			throw new SilverPopException(msg);
		}
	}
	
	public String getJobStatus(String jsessionId, int jobId) throws SilverPopException {
		Envelope getJobStatus = SilverPopRequestFactory.createGetJobStatusRequest(jobId);
		Envelope res = sendRequest(getJobStatus, jsessionId);
		if(isSuccessful(res)) {
			return res.getBody().getRESULT().getJOBSTATUS();
		} else {
			throw new SilverPopException("Can't check job status");
		}
	}
	
	public int sendListToSilverPop(String jsessionId, String filename, String mapfile, String email) throws SilverPopException {
		Envelope sendToSP = SilverPopRequestFactory.createImportListRequest(filename, mapfile, email);
		Envelope res = sendRequest(sendToSP, jsessionId);
		if(SilverPopClient.isSuccessful(res)){
			int jobId = res.getBody().getRESULT().getJOBID();
			log.debug("JobID = " + jobId);
			return jobId;
		} else {
			String msg = "ImportList unsuccessful"; 
			log.debug(msg);
			try{
				log.debug(res.getBody().getFault().getFaultString());
			} catch(Exception e) {
			}
			throw new SilverPopException(msg);
		}
	}
	
	public void getLists(String jsessionId) throws SilverPopException {
		Envelope getList = SilverPopRequestFactory.createGetListRequest();
		Envelope res = sendRequest(getList, jsessionId);
		log.debug(res);
	}
	
	public RESULT retrieveListFromSilverPop(String jsessionId, int listId, Date from, boolean contacts) throws SilverPopException{
		Envelope getList = SilverPopRequestFactory.createExportListRequest(listId, from, contacts);
		Envelope res = sendRequest(getList, jsessionId);
		if(SilverPopClient.isSuccessful(res)){
			int jobId = res.getBody().getRESULT().getJOBID();
			log.debug("JobID = " + jobId);
			return res.getBody().getRESULT();
		} else {
			String msg = "ExportList unsuccessful"; 
			log.debug(msg);
			try{
				log.debug(res.getBody().getFault().getFaultString());
			} catch(Exception e) {
			}
			throw new SilverPopException(msg);
		}

	}
	
	public String login(String userName, String password) throws SilverPopException{
		Envelope login = SilverPopRequestFactory.createLoginRequest(userName, password);
		Envelope loginResult = sendRequest(login, null);
		if(isSuccessful(loginResult)) {
			return loginResult.getBody().getRESULT().getSESSIONID();
		} else {
			throw new SilverPopException("Can't login");
		}
	}
	
	public void logout(String jsessionId) throws SilverPopException {
		Envelope logout = SilverPopRequestFactory.createLogoutRequest();
		sendRequest(logout, jsessionId);
	}
	
	public static boolean isSuccessful(Envelope e){
		if(e != null){
			if(e.getBody() != null) {
				if(e.getBody().getRESULT() != null) {
					if(e.getBody().getRESULT().getSUCCESS().equalsIgnoreCase(SUCCESS)) {
						return true;
					}
				}
			}
		}
		return false;
	}
    
    private Envelope unmarshal(String source) {
    	try {
    		StringReader reader = new StringReader(source);
    		return (Envelope) unmarshaller.unmarshal(reader);
    	} catch (JAXBException jaxbe) {
    		log.debug("Cannot unmarshal:"+source);
    	}
		return null;
    }
	    
    private String marshalToString(Envelope obj) {
        try {
            StringWriter sw = new StringWriter();
            marshaller.marshal(obj, sw);
            String s = sw.toString();
            return s;
        } catch (JAXBException jaxbe) {
        	log.debug("Cannot marshal"+obj.toString());
        }
		return null;
    }
	    
    private String executePOSTRequest(String url, String xmlRequest) throws IOException {
        if (log.isEnabledFor(Level.DEBUG)) {
            log.log(Level.DEBUG, url);
            log.log(Level.DEBUG, xmlRequest);
        }

        String response = null;
        
        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            byte[] data = xmlRequest.getBytes("UTF-8");
            URL u = new URL(url);
            HttpURLConnection httpCon = (HttpURLConnection)u.openConnection();
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("POST");
            httpCon.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
            httpCon.setRequestProperty("Content-Length", String.valueOf(data.length));

            OutputStream os = httpCon.getOutputStream();
            os.write(data, 0, data.length);
            os.flush();
            try {
                os.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close output stream.", e);
            }
            if (log.isEnabledFor(Level.DEBUG)) {
                log.log(Level.DEBUG, "Request sent.");
            }
            int respCode = httpCon.getResponseCode();
            if (log.isEnabledFor(Level.DEBUG)) {
                log.log(Level.DEBUG, "ResponseCode: " + respCode);
            }
            is = httpCon.getInputStream();
            isr = new InputStreamReader(is, "UTF-8");
            //isr = new InputStreamReader(is, "ISO-8859-8");

            br = new BufferedReader(isr);
            StringBuffer xmlResp = new StringBuffer();
            String line = null;
            while ((line = br.readLine()) != null) {
                xmlResp.append(line);
            }
            response = xmlResp.toString();
            if (log.isEnabledFor(Level.DEBUG)) {
                log.log(Level.DEBUG, "Response received.");
                log.log(Level.DEBUG, response);
            }
        } catch (IOException ioe) {
			log.log(Level.ERROR, "Give up");
			throw ioe;
        } finally {
            if (null != br) {
                try {
                    br.close();
                } catch (Exception e) {
                    log.log(Level.ERROR, "Can't close the buffered reader.", e);
                }
            }
            if (null != isr) {
                try {
                    isr.close();
                } catch (Exception e) {
                    log.log(Level.ERROR, "Can't close the input stream reader.", e);
                }
            }
            if (null != is) {
                try {
                    is.close();
                } catch (Exception e) {
                    log.log(Level.ERROR, "Can't close the input stream.", e);
                }
            }
        }

        return response;
    }
}
