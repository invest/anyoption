package com.anyoption.ets.beans;

public class MarketingTrackingParamBase implements java.io.Serializable {
	

	private static final long serialVersionUID = 1657430793485404851L;

	private long midId;
	private String mId;
	private String combinationId;
	private String dyanmicParameter;
	private String httpReferer;
	private String requestUrl;
	private String ip;

    /**
     * @return the dyanmicParameter
     */
    public String getDyanmicParameter() {
        return dyanmicParameter;
    }
    /**
     * @param dyanmicParameter the dyanmicParameter to set
     */
    public void setDyanmicParameter(String dyanmicParameter) {
        this.dyanmicParameter = dyanmicParameter;
    }
    /**
     * @return the httpReferer
     */
    public String getHttpReferer() {
        return httpReferer;
    }
    /**
     * @param httpReferer the httpReferer to set
     */
    public void setHttpReferer(String httpReferer) {
        this.httpReferer = httpReferer;
    }
   
    /**
     * @return the combinationId
     */
    public String getCombinationId() {
        return combinationId;
    }
    /**
     * @param combinationId the combinationId to set
     */
    public void setCombinationId(String combinationId) {
        this.combinationId = combinationId;
    }
    /**
     * @return the mId
     */
    public String getmId() {
        return mId;
    }
    /**
     * @param mId the mId to set
     */
    public void setmId(String mId) {
        this.mId = mId;
    }
    /**
     * @return the midId
     */
    public long getMidId() {
        return midId;
    }
    /**
     * @param midId the midId to set
     */
    public void setMidId(long midId) {
        this.midId = midId;
    }
    /**
     * @return the requestUrl
     */
    public String getRequestUrl() {
        return requestUrl;
    }
    /**
     * @param requestUrl the requestUrl to set
     */
    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }
    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }
    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }
}


