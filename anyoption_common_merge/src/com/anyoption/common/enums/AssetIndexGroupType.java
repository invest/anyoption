package com.anyoption.common.enums;
import java.util.HashMap;

public enum AssetIndexGroupType {
	TYPE_GROUP(0l), COUNTRY_GROUP(1l), MARKET_GROUP(2l);

	private final long id;
	private static final HashMap<Long, AssetIndexGroupType> idGroupMap = new HashMap<Long, AssetIndexGroupType>(AssetIndexGroupType.values().length);
	static {
		for (AssetIndexGroupType family : AssetIndexGroupType.values()) {
			idGroupMap.put(family.getId(), family);
		}
	}
	
	AssetIndexGroupType(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public static AssetIndexGroupType getById(long id) {
		AssetIndexGroupType family = idGroupMap.get(id);
		if (family == null) {
			throw new IllegalArgumentException("No asset Index Group with ID: " + id);
		} else {
			return family;
		}
	}
}