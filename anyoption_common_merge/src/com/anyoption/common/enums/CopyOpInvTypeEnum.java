package com.anyoption.common.enums;

public enum CopyOpInvTypeEnum {
	SELF(0, "Original"), COPY(1, "Copy"), FOLLOW(2, "Follow");
	
	private int code;
	private String displayName;
	 
	private CopyOpInvTypeEnum(int c, String displayName) {
		code = c;
		this.displayName = displayName;
	}

	public int getCode() {
		return code;
	}
	
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public static CopyOpInvTypeEnum of(int code) {

	    switch (code) {
	        case 0: 
        		return SELF;
	        case 1:
	        	return COPY;
	        case 2:
	        	return FOLLOW;
	        default: 
	        	return null;
	    }
	}
}
