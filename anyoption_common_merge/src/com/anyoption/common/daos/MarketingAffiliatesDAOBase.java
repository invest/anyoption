package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketingAffiliate;

/**
 * @author eranl
 *
 */
public class MarketingAffiliatesDAOBase extends DAOBase {
	
	private static final Logger logger = Logger.getLogger(MarketingAffiliatesDAOBase.class);
	
	  /**
	   * Get affiliate by key
	   * @param con db connection
	   * @param affiliateKey
	   * @return  affiliate instance
	   * @throws SQLException
	   */
	   public static MarketingAffiliate getAffiliateByKey(Connection con, long affiliateKey) throws SQLException {

	   	  PreparedStatement ps = null;
		  ResultSet rs = null;
		  MarketingAffiliate ma = null;

		  try {
			    String sql =" SELECT " +
			    			"	ma.* " +
			    			" FROM " +
			    			"	marketing_affilates ma " +
			    			" WHERE " +
			    			"	ma.key = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, affiliateKey);
				rs = ps.executeQuery();
				if (rs.next()) {
					ma = getVO(rs);									
				}
			}
			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return ma;
	   }
	   
	/**
	 * Get VO
	 * 
	 * @param rs
	 *            Result set instance
	 * @return MarketingAffilate object
	 * @throws SQLException
	 */
	protected static MarketingAffiliate getVO(ResultSet rs) throws SQLException {
		MarketingAffiliate vo = new MarketingAffiliate();
		vo.setId(rs.getLong("id"));
		vo.setName(rs.getString("name"));
		vo.setKey(rs.getString("key"));
		vo.setWriterId(rs.getLong("writer_id"));
		return vo;
	}
	
}