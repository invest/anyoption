package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import com.anyoption.common.beans.OpportunityOddsType;

/**
 * @author kirilim
 */
public class OpportunityOddsTypesDAOBase extends DAOBase {

	public static HashMap<Long, OpportunityOddsType> getTypes(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, OpportunityOddsType> hm = new HashMap<Long, OpportunityOddsType>();
		try {
			String sql = "select * from OPPORTUNITY_ODDS_TYPES ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				hm.put(rs.getLong("id"), getOppOddsVO(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;
	}

	protected static OpportunityOddsType getOppOddsVO(ResultSet rs) throws SQLException {
		OpportunityOddsType vo = new OpportunityOddsType();

		vo.setId(rs.getLong("ID"));
		vo.setName(rs.getString("NAME"));
		vo.setOverOddsWin(rs.getFloat("OVER_ODDS_WIN"));
		vo.setOverOddsLose(rs.getFloat("OVER_ODDS_LOSE"));
		vo.setUnderOddsWin(rs.getFloat("UNDER_ODDS_WIN"));
		vo.setOddsWinDeductStep(rs.getFloat("ODDS_WIN_DEDUCT_STEP"));
		vo.setUnderOddsLose(rs.getFloat("UNDER_ODDS_LOSE"));
		vo.setIsDefault(rs.getInt("IS_DEFAULT"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));

		return vo;
	}
}