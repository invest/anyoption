/**
 * 
 */
package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Tournament;
import com.anyoption.common.beans.TournamentBannerFile;
import com.anyoption.common.beans.TournamentLanguageTabs;
import com.anyoption.common.beans.TournamentNumberOfUsers;
import com.anyoption.common.beans.TournamentSkinsLanguages;
import com.anyoption.common.beans.TournamentType;
import com.anyoption.common.beans.TournamentsBanners;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author pavelt
 *
 */
public class TournamentDAOBase extends DAOBase {

	private static final Logger logger = Logger.getLogger(TournamentDAOBase.class);
	
	/**
	 * get all tournaments
	 * @param conn
	 * @param isActive show only active tournaments or all
	 * @return tournaments list
	 * @throws SQLException
	 */
	public static ArrayList<Tournament> getAllTournaments(Connection conn, boolean isActive) throws SQLException{
		      PreparedStatement ps = null;
			  ResultSet rs = null;
			  ArrayList<Tournament> list = new ArrayList<Tournament>();
			  int active = 0;
			  if(isActive){
				  active = Tournament.ACTIVE;
			  }
			  try{
				  String sql = "SELECT "
							  		+ " t.*, "
							  		+ " tt.name as type_name, "
							  		+ " tt.process_class, "
							  		+ " tnu.users_to_show, "
							  		+ " c.code " +
				  				" FROM "
					  				+ " tournaments t, "
					  				+ " tournament_type tt, "
					  				+ " tournament_num_of_users tnu, "
					  				+ " currencies c "+
						  		" WHERE "
							  		+ " t.type_id = tt.id "
							  		+ " AND t.number_of_users_id = tnu.id "
							  		+ " AND t.currency_id = c.id "
				  					+ " AND DECODE(?, 0, 0, t.is_active) = DECODE(?, 0, 0, ?)"
					  	 			+ " ORDER BY t.id DESC ";
				  
				  ps = conn.prepareStatement(sql);
				  ps.setInt(1, active);
				  ps.setInt(2, active);
				  ps.setInt(3, active);
				  rs = ps.executeQuery();
				  while (rs.next()){
					  list.add(getTournamentVO(rs));
				  }
			  } finally {
					closeResultSet(rs);
					closeStatement(ps);
			  }
			  return list;
	}
	
	public static ArrayList<Tournament> getAllTournamentSkinLang(Connection conn, ArrayList<Tournament> tournamentsList) throws SQLException {
		if (tournamentsList != null && !tournamentsList.isEmpty()) {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql = " SELECT tsl.*, tt.label, tt.def_lng_id "
						+ " FROM tournament_skin_lang tsl, tournament_be_tabs_lng tt "
						+ " WHERE tsl.tab_id = tt.id "
						+ " AND tsl.tournament_id = ? ";
				ps = conn.prepareStatement(sql);
				for (Tournament i : tournamentsList) {
					ArrayList<TournamentSkinsLanguages> list = new ArrayList<TournamentSkinsLanguages>();
					ps.setLong(1, i.getId());
					rs = ps.executeQuery();
					while (rs.next()) {
						list.add(getTournamentSkinLangVO(rs));
					}
					i.setTournamentSkinsLangList(list);

				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		}
		return tournamentsList;

	}
	
	
	
	protected static TournamentSkinsLanguages getTournamentSkinLangVO(ResultSet rs) throws SQLException{
		TournamentSkinsLanguages vo = new TournamentSkinsLanguages();
		
		vo.setId(rs.getLong("id"));
		vo.setTournamentId(rs.getLong("tournament_id"));
		vo.setTabId(rs.getLong("tab_id"));
		vo.setPrizeText(rs.getString("prize_text"));
		vo.setPrizeImage(rs.getString("prize_image"));
		vo.setTabLabel(rs.getString("label"));
		vo.setTabDefLangId(rs.getLong("def_lng_id"));
		return vo;
	}
	
	
	protected static Tournament getTournamentVO(ResultSet rs) throws SQLException {
		
		Tournament vo = new Tournament();
		TournamentType type = new TournamentType();
		TournamentNumberOfUsers numberOfUsersToShow = new TournamentNumberOfUsers();
		Currency currency = new Currency();
		
		
		vo.setId(rs.getLong("id"));
		
		type.setId(rs.getLong("TYPE_ID"));
		type.setName(rs.getString("TYPE_NAME"));
		type.setProccessClass(rs.getString("process_class"));
		vo.setType(type);
		
		vo.setName(rs.getString("NAME"));
		vo.setTimeStart(convertToDate(rs.getTimestamp("TIME_START")));
		vo.setTimeEnd(convertToDate(rs.getTimestamp("TIME_END")));
		vo.setActive(rs.getInt("IS_ACTIVE"));
		vo.setMinInvAmount(rs.getLong("MIN_INV_AMOUNT")/100);
		vo.setWriterId(rs.getLong("WRITER_ID"));
		vo.setTimeUpdated(convertToDate(rs.getTimestamp("TIME_UPDATED")));
		
		numberOfUsersToShow.setId(rs.getLong("NUMBER_OF_USERS_ID"));
		numberOfUsersToShow.setUsersToShow(rs.getLong("USERS_TO_SHOW"));
		vo.setNumberOfUsersToShow(numberOfUsersToShow);
		
		vo.setSkinsToParticipate(rs.getString("SKIN"));
		
		currency.setId(rs.getLong("CURRENCY_ID"));
		currency.setCode(rs.getString("CODE"));
		vo.setCurrency(currency);
		
		vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
		vo.setSkinsFromDB(vo.getSkinsToParticipate());
		return vo;
	}
	
	public static ArrayList<TournamentLanguageTabs> getLangTabs(Connection con)throws SQLException {
		ArrayList<TournamentLanguageTabs> list = new ArrayList<TournamentLanguageTabs>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT * FROM tournament_be_tabs_lng ORDER BY id";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				TournamentLanguageTabs item = new TournamentLanguageTabs();
				item.setTabId(rs.getLong("id"));
				item.setTabLabel(rs.getString("label"));
				item.setLangId(rs.getLong("def_lng_id"));
				list.add(item);
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
	
	public static ArrayList<String> getTournamentsBannersBySkinId(Connection con, long skinId)throws SQLException {
		ArrayList<String> list = new ArrayList<String>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT bb.banner_image " +
							"FROM skins s, tournament_be_tabs_lng t, tournaments_banners bb " +
						   "WHERE s.default_language_id = t.def_lng_id "+
							 "AND t.id = bb.tab_id " +
							 "AND s.id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);
			rs = ps.executeQuery();

			while (rs.next()) {
				list.add(rs.getString("banner_image"));
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
	
	public static ArrayList<TournamentsBanners> getTournamentsBanners(Connection con) throws SQLException {
		ArrayList<TournamentsBanners> list = new ArrayList<TournamentsBanners>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT t.*, tl.label "
						+ " FROM tournaments_banners t, tournament_be_tabs_lng tl "
						+ " WHERE t.tab_id = tl.id ORDER BY tab_id, banner_index";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			long lastTabId = 0;
			HashMap<Long, TournamentsBanners> hm = new HashMap<Long, TournamentsBanners>();
			while (rs.next()) {

				if (lastTabId != rs.getLong("tab_id")) {
					lastTabId = rs.getLong("tab_id");
					TournamentsBanners item = new TournamentsBanners();// Lang tab
					item.setTabId(rs.getLong("tab_id"));
					item.setWriterId(rs.getLong("writer_id"));
					item.setDateCreated(rs.getDate("date_created"));
					item.setTabLabel(rs.getString("label"));
					hm.put(rs.getLong("tab_id"), item);
				    list.add(hm.get(rs.getLong("tab_id")));
				}

				TournamentBannerFile banner = new TournamentBannerFile(); // files
				banner.setBannerIndex(rs.getLong("banner_index"));
				banner.setBannerImage(rs.getString("banner_image"));
				banner.setId(rs.getLong("id"));

				hm.get(rs.getLong("tab_id")).getFiles().add(banner);
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
}
