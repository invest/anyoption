package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.JmxServerConfig;

public class JmxServerConfigDAOBase extends DAOBase {
    public static ArrayList<JmxServerConfig> getJmxServersConfig(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<JmxServerConfig> list = new ArrayList<JmxServerConfig>();
        try {
            String sql =
                "SELECT " +
                    " * " +
                "FROM " +
                    "JMX_SERVER_CONFIG " +
                "WHERE " +
                    " is_active = 1 " ;

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
            	JmxServerConfig jmxServerConfig = new JmxServerConfig();
            	
            	jmxServerConfig.setId(rs.getLong("ID"));
            	jmxServerConfig.setAddress(rs.getString("ADDRESS"));
            	jmxServerConfig.setPort(rs.getLong("PORT"));
            	jmxServerConfig.setUserName(rs.getString("USER_NAME"));
            	jmxServerConfig.setPassword(rs.getString("PASS"));
            	
            	list.add(jmxServerConfig);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return list;
    }
}