package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.anyoption.common.beans.UsersAwardBonusGroup;
import com.anyoption.common.util.CommonUtil;

public class UsersAwardBonusGroupDAO extends DAOBase{
	  public static void insert(Connection con, UsersAwardBonusGroup vo) throws SQLException {
		  PreparedStatement ps = null;

		  try {

				String sql = "INSERT INTO " +
								"users_award_bonus_groups " +
							    "( " +
							      "time_created, " +
							      "description, " +
							      "redeem_until_date, " +
							      "id, " +
							      "bonus_id " +
							    ") " +
						     "VALUES " +
							    "( " +
							      "?, " +
							      "?, " +
							      "?, " +
							      "seq_users_award_bonus_groups.nextval, " +
							      "? " +
							    ")";

				ps = con.prepareStatement(sql);
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(new Date()));
				ps.setString(2, vo.getDescription());
				ps.setTimestamp(3, CommonUtil.convertToTimeStamp(vo.getRedeemUntilDate()));
				ps.setLong(4, vo.getBonusId());

				ps.executeUpdate();
				
				vo.setId(getSeqCurValue(con, "seq_users_award_bonus_groups"));
		  } finally {
			  closeStatement(ps);
		  }
	  }
	  
	  public static UsersAwardBonusGroup getVO(ResultSet rs, UsersAwardBonusGroup vo) throws SQLException {
		  return getVO(rs, vo, "id");
	  }
	  
	  public static UsersAwardBonusGroup getVO(ResultSet rs, UsersAwardBonusGroup vo, String id) throws SQLException {
		  vo.setId(rs.getLong(id));
		  vo.setBonusId(rs.getLong("bonus_id"));
		  vo.setDescription(rs.getString("description"));
		  vo.setRedeemUntilDate(rs.getDate("redeem_until_date"));
		  vo.setTimeCreated(rs.getDate("time_created"));
		  return vo;
	  }

}
