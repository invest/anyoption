package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardTask;
import com.anyoption.common.beans.RewardTaskCurrency;
import com.anyoption.common.rewards.Task;
import com.anyoption.common.rewards.TaskUser;

/**
 * @author liors
 *
 */
public class RewardTasksDAO extends DAOBase {
	private final static Logger logger = Logger.getLogger(RewardTasksDAO.class);

	public static TaskUser getUserTasks(Connection connection, TaskUser taskUser, long rewardTaskType) throws SQLException {
    	PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int ind = 1;
		try {   
    		String sql =	"SELECT" +
		    				"	ru.id as ru_id" +
		    				"	,ru.user_id" +
		    				"	,ru.rwd_tier_id" +
		    				"	,ru.exp_points as ru_exp_points" +
		    				"	,ru.time_last_activity" +
		    				"	,ru.time_last_reduced_points" +
				    		"	,rt.id as rt_id" +
				    		"	,rt.rwd_task_type_id" +
				    		"	,rt.rwd_task_level_id" +
				    		"	,rt.rwd_tier_id" +
				    		"	,rt.num_actions" +
				    		"	,rt.num_recurring" +
				    		"	,rt.exp_points as rt_exp_points" +
				    		"	,rut.id as rut_id" +
				    		"	,rut.num_actions as user_num_actions" +
				    		"	,rut.num_recurring as user_num_recurring" +
				    		"	,rut.is_done" +
				    		"	,rtt.rwd_task_group_type_id" +
				    		"	,rtc.amount_to_points " +
				    		"	,(select id from rwd_user_platforms rup where rup.user_id = ? and rup.product_type_id = ?) as rwd_user_platforms_id " +
				    		"FROM" +
				    		"	rwd_users ru" +
				    		"	,rwd_task_types rtt" +
				    		"	,rwd_tasks rt" +
				    		"		left join rwd_task_currencies rtc on rt.id = rtc.rwd_task_id" +
				    		"		left join rwd_task_platforms rtp on rt.id = rtp.rwd_task_id" +
				    		"		left join rwd_user_tasks rut on rt.id = rut.rwd_task_id AND rut.user_id = ? " +
				    		"WHERE" +
				    		"	rt.rwd_task_type_id = rtt.id" +
				    		"	AND	(ru.rwd_tier_id >= rt.rwd_tier_id OR rt.rwd_tier_id is null)" +
				    		"	AND (rtc.id is null OR (rtc.currency_id = ? AND ((rtc.amount_to_points is not null) OR (rtc.from_amount <= ? AND rtc.to_amount >= ?))))" +
				    		"	AND ru.user_id = ?" +
				    		"	AND (rut.is_done = 0 OR rut.is_done is null) ";
	    		if (rewardTaskType > 0) {
	    			sql +=  "	AND rt.rwd_task_type_id = ? ";
	    		} else {
		    		sql +=  "	AND rtt.rwd_task_group_type_id = ? ";
	    		}
	    			sql +=	"ORDER BY " +
				    		"	rt.rwd_task_type_id" +
				    		"	,rt.rwd_task_level_id" +
				    		"	,rt.rwd_tier_id" +
				    		"	,rtc.from_amount desc";
				            
		    pstmt = connection.prepareStatement(sql);
		    pstmt.setLong(ind++, taskUser.getUserId());
		    pstmt.setLong(ind++, taskUser.getProductTypeId());
		    pstmt.setLong(ind++, taskUser.getUserId());
		    pstmt.setLong(ind++, taskUser.getCurrencyId());
		    pstmt.setLong(ind++, taskUser.getAmount());
		    pstmt.setLong(ind++, taskUser.getAmount());
		    pstmt.setLong(ind++, taskUser.getUserId());
		    if (rewardTaskType > 0) {
		    	pstmt.setLong(ind++, rewardTaskType);
		    } else {
		    	pstmt.setInt(ind++, taskUser.getTaskGroupTypeId());
		    }
		    resultSet = pstmt.executeQuery();

		    int taskTypeId  = -1;
		    while (resultSet.next()) {
		    	if (taskTypeId != resultSet.getInt("rwd_task_type_id")) {
		    		Task task = new Task();
		    		task.setRewardUser(RewardUsersDAO.getRewardUserCustomValue(resultSet));
		    		task.setRewardTask(getRewardTaskCustomValue(resultSet));
		    		task.setRewardUserTask(RewardUserTasksDAO.getRewardUserTaskCustomValue(resultSet));
		    		task.getRewardUserPlatform().setId(resultSet.getLong("rwd_user_platforms_id"));
		    		task.setRewardUserPlatform(task.getRewardUserPlatform());
		    		taskTypeId = resultSet.getInt("rwd_task_type_id");
			    	taskUser.getTasks().add(task);
			    	logger.debug(task.getRewardTask().getId());
		    	}  	
		    }
    	} finally {
		   closeStatement(pstmt);
		   closeResultSet(resultSet);
		}
		return taskUser;
	}
	
	public static RewardTask getRewardTaskCustomValue(ResultSet resultSet) throws SQLException {
		RewardTask valueObject = new RewardTask();
		valueObject.setId(resultSet.getInt("rt_id"));
		valueObject.setTypeId(resultSet.getInt("rwd_task_type_id"));
		valueObject.setLevelId(resultSet.getInt("rwd_task_level_id"));
		valueObject.setRewardTierId(resultSet.getInt("rwd_tier_id"));
		valueObject.setNumOfActions(resultSet.getInt("num_actions"));
		valueObject.setNumOfRecurring(resultSet.getInt("num_recurring"));
		valueObject.setExperiencePoints(resultSet.getDouble("rt_exp_points"));
		valueObject.setRewardTaskCurrency(new RewardTaskCurrency(resultSet.getDouble("amount_to_points")));
		
		return valueObject;
	}
}
