package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.beans.RewardUserTask;
import com.anyoption.common.rewards.RewardUtil;

/**
 * @author liors
 *
 */
public class RewardUserTasksDAO extends DAOBase {
	
	/**
	 * insert into rwd_user_tasks.
	 * @param connection
	 * @param rewardUserTask
	 * @return
	 * @throws SQLException
	 */
	public static boolean insertValueObject(Connection connection, RewardUserTask rewardUserTask) throws SQLException {
		PreparedStatement preparedStatement = null;
		int index = 1;
		try {
			String sql =
						"INSERT INTO rwd_user_tasks" +
						"	(" +
						"		id" +
						"		,user_id" +
						"		,rwd_task_id" +
						"		,num_actions" +
						"		,num_recurring" +
						"		,is_done" +
						"	)" +
						"	VALUES" +
						"	(" +
						"		seq_rwd_user_tasks.nextval" +
						"		,?" +
						"		,?" +
						"		,?" +
						"		,?" +
						"		,?" +
						"	)";

			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setLong(index++, rewardUserTask.getUserId());
			preparedStatement.setInt(index++, rewardUserTask.getTaskId());
			preparedStatement.setInt(index++, rewardUserTask.getNumOfActions());
			preparedStatement.setInt(index++, rewardUserTask.getNumOfRecurring());
			preparedStatement.setBoolean(index++, rewardUserTask.isDone());
			preparedStatement.executeUpdate();
			rewardUserTask.setId(RewardUtil.safeLongToInt(getSeqCurValue(connection, "seq_rwd_user_tasks")));			
		} finally {
			closeStatement(preparedStatement);
		}	
		return true;
	}
	
	//TODO unit test.
	/**
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	public static RewardUserTask getRewardUserTaskValueObject(ResultSet resultSet) throws SQLException {
		RewardUserTask valueObject = new RewardUserTask();
		valueObject.setId(resultSet.getInt("id"));
		valueObject.setUserId(resultSet.getLong("user_id"));
		valueObject.setTaskId(resultSet.getInt("rwd_task_id"));
		valueObject.setNumOfActions(resultSet.getInt("num_actions"));
		valueObject.setNumOfRecurring(resultSet.getInt("num_recurring"));
		valueObject.setDone(resultSet.getBoolean("is_done"));
		
		return valueObject;
	}
	
	/**
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	public static RewardUserTask getRewardUserTaskCustomValue(ResultSet resultSet) throws SQLException {
		RewardUserTask valueObject = new RewardUserTask();
		valueObject.setId(resultSet.getInt("rut_id"));
		valueObject.setUserId(resultSet.getLong("user_id"));
		valueObject.setTaskId(resultSet.getInt("rt_id"));
		valueObject.setNumOfActions(resultSet.getInt("user_num_actions"));
		valueObject.setNumOfRecurring(resultSet.getInt("user_num_recurring"));
		valueObject.setDone(resultSet.getBoolean("is_done"));
		
		return valueObject;
	}
	
	public static void update(Connection connection, RewardUserTask rewardUserTask) throws SQLException {
		int index = 1;
		PreparedStatement pstmt = null;
		
		try {
			String sql = 
					"UPDATE " +
					"	rwd_user_tasks " +
					"SET" +
					"	num_actions = ?" +
					"	,num_recurring = ?" +
					"	,is_done = ? " +
					"WHERE" +
					"	id = ?";
			pstmt = connection.prepareStatement(sql.toString());
			pstmt.setInt(index++, rewardUserTask.getNumOfActions());
			pstmt.setInt(index++, rewardUserTask.getNumOfRecurring());
			pstmt.setBoolean(index++, rewardUserTask.isDone());
			pstmt.setInt(index++, rewardUserTask.getId());
			
			pstmt.executeUpdate();
		} finally {
			closeStatement(pstmt);
		}
	}
}
