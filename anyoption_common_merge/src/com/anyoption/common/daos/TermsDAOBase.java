package com.anyoption.common.daos;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.anyoption.common.beans.base.TermsPartFile;
import com.anyoption.common.beans.base.TermsParts;
import com.anyoption.common.service.requests.TermsFileMethodRequest;

import oracle.jdbc.OracleTypes;

public class TermsDAOBase extends DAOBase {

    public static Map<Long, Map<Long, ArrayList<TermsParts>>> getTermsFilter(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        //<PlatformId, <SkinId, ArrayList<TermsPart>>>
        Map<Long, Map<Long, ArrayList<TermsParts>>> hm = new HashMap<Long, Map<Long, ArrayList<TermsParts>>>();
        try {
            String sql =
                "SELECT " +
                  " t.platform_id,t.skin_id, t.part_id, p.parts_name, p.order_num " +
                " FROM " + 
              	  " terms t, " +
              	  " terms_parts p " +
                " WHERE " +
                 " t.part_id = p.id " +
                " ORDER BY p.order_num ";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                if(hm.get(rs.getLong("platform_id")) == null){
                	Map<Long, ArrayList<TermsParts>> shm = new HashMap<Long, ArrayList<TermsParts>>();
                	hm.put(rs.getLong("platform_id"), shm);
                }
                
                if(hm.get(rs.getLong("platform_id")).get(rs.getLong("skin_id")) == null){
                	 ArrayList<TermsParts> arrList = new ArrayList<TermsParts>();
                	hm.get(rs.getLong("platform_id")).put(rs.getLong("skin_id"), arrList);
                }
                
                hm.get(rs.getLong("platform_id")).get(rs.getLong("skin_id")).add( new TermsParts(rs.getLong("part_id"), rs.getString("parts_name")));
                
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return hm;
    }
    
    public static TermsPartFile getTermsFile(Connection conn, long platformId, long skinId, long partId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        TermsPartFile file = new TermsPartFile();
        try {
            String sql =
                "SELECT f.id, f.file_name, f.title, p.type, t.is_active " + 
                " FROM " + 
                	" terms t, " +
                	" terms_files f, " +
                	" terms_parts p " +
                " WHERE t.file_id = f.id " +
                	" and t.part_id = p.id " +
                	" and t.platform_id = ? " +
                	" and t.skin_id = ? " +
                	" and t.part_id = ? ";                        
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, platformId);
            pstmt.setLong(2, skinId);
            pstmt.setLong(3, partId);
            rs = pstmt.executeQuery();           
            if (rs.next()) {
            	file.setId(rs.getLong("id"));
            	file.setFileName(rs.getString("file_name"));
            	file.setTitle(rs.getString("title"));
            	file.setTypeId(rs.getLong("type"));
            	file.setActive(rs.getBoolean("is_active"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return file;
    }
    
    public static ArrayList<TermsPartFile> getTermsList(Connection conn, long platformId, long skinId, long typeId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<TermsPartFile> list = new ArrayList<TermsPartFile>();
        String type = "(" + typeId + ")";
        if(typeId == TermsFileMethodRequest.AGREEMENT_TERMS){
        	 type = "(" + TermsFileMethodRequest.GENERAL_TERMS + ", " 
        			 		+ TermsFileMethodRequest.AGREEMENT_TERMS + ")";
        }
        try {
            String sql =
                "SELECT " +
                  " t.id, p.order_num, f.file_name, f.title, p.type " +
                " FROM " + 
              	  " terms t, " +
              	  " terms_parts p, " +
              	  " terms_files f " +
                " WHERE " +
                 " t.part_id = p.id " +
                 " and t.file_id = f.id " +
                 " and t.is_active = 1 " +
                 " and p.type in " + type +
                 " and t.platform_id = ? " +
                 " and t.skin_id = ? ";
              
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, platformId);
            pstmt.setLong(2, skinId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	list.add( new TermsPartFile(rs.getLong("id"), 
            								rs.getLong("order_num"), 
            								rs.getString("file_name"), 
            								rs.getString("title"), 
            								"",
            								rs.getLong("type")));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }
    
    public static HashMap<String,String> getTermsParams(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        HashMap<String,String> hm = new HashMap<String,String>();
        try {
            String sql =
                "SELECT " +
                  " * " +
                " FROM " + 
              	  " TERMS_PARAMS ";
              
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	hm.put(rs.getString("KEY"), rs.getString("VALUE"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return hm;
    }
	
	public static void updateFile(TermsFileMethodRequest request, long fileId, Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_terms.update_file(i_file_id => ? "+
															    ",i_file_title => ? "+
															    ",i_file_part_id => ? "+
															    ",i_is_active => ? " + 
															    ",i_platform_id => ? " + 
															    ",i_skin_id => ? )}");
			
			cstmt.setLong(index++, fileId);
			cstmt.setString(index++, request.getTitle());
			cstmt.setLong(index++, request.getPartIdFilter());
			cstmt.setBoolean(index++, request.isActive());
			cstmt.setLong(index++, request.getPlatformIdFilter());
			cstmt.setLong(index++, request.getSkinIdFilter());
			
			
			cstmt.execute();

			
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static Long get1TInvestLimit(Connection conn, long currencyId, long skinId) throws SQLException {
		Long res = null;
		CallableStatement cstmt = null;
		BigDecimal v ;
		int index = 1;
		try {
			String sql = "{call pkg_terms.get_onetouch_invlimit(?, ?, ?)}";
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.NUMERIC);
			cstmt.setLong(index++, currencyId);
			cstmt.setLong(index++, skinId);
			cstmt.executeQuery();
			
			v = (BigDecimal) cstmt.getObject(1);
			res = v.longValue();
		} finally {
			closeStatement(cstmt);
		}
		return res;
	}
	
	public static Long getMinFD(Connection conn, long currencyId, long skinId) throws SQLException {
		Long res = null;
		CallableStatement cstmt = null;
		BigDecimal v ;
		int index = 1;
		try {
			String sql = "{call pkg_terms.get_min_first_deposit(?, ?, ?)}";
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.NUMERIC);
			cstmt.setLong(index++, currencyId);
			cstmt.setLong(index++, skinId);
			cstmt.executeQuery();
			
			v = (BigDecimal) cstmt.getObject(1);
			res = v.longValue();
		} finally {
			closeStatement(cstmt);
		}
		return res;
	}		
}