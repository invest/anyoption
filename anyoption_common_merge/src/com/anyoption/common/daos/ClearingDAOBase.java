package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.ClearingRoute;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingManagerConstants;
import com.anyoption.common.clearing.ClearingProviderConfig;
import com.anyoption.common.util.ConstantsBase;

public class ClearingDAOBase extends DAOBase implements ClearingManagerConstants {
	
	private static final Logger log = Logger.getLogger(ClearingDAOBase.class);
    public static String ADD_CLEARING_ROUTES_COMMENT = "Added by Rerouting Mechanism ";

	
    /**
     * Apply routing rules to skin, bin and currency and give route if available.
     *
     * @param conn
     * @param skinId
     * @param bin
     * @param currencyId
     * @param deposit
     * @param countryId
     * @param ccTypeId
     * @return Provider id to process the transaction through or 0 if no route configured.
     * @throws SQLException
     */
    public static ClearingRoute findRoute(Connection conn, long skinId, String bin, long currencyId, 
    		boolean deposit, long countryId, long ccTypeId, int platformId) throws SQLException {
    	log.info("Trying to find rout with: " +
				"skinId= " + skinId + ", bin= " + bin + ", currencyId= " + currencyId + "" +
						", countryId=" + countryId + ", ccTypeId=" + ccTypeId + ", platformId=" + platformId);
    	
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int index = 1;
        try {
        	String sql =
                    " select " +
                    	" b.id as bin_id, " +
                    	" cr.id, " +
                    	" cr.time_last_success, " +
                    	" cr.time_last_reroute, " +
                    	" cr.start_bin, " +
                    	" cr.deposit_clearing_provider_id, " +
    				    " cr.withdraw_clearing_provider_id, " +
    				    " cr.cft_clearing_provider_id " +
    				" from " +
    				   	" CLEARING_ROUTES cr LEFT JOIN bins b ON cr.start_bin like b.from_bin " +
                    " where " +
    	                " cr.is_active = 1 " +
    		            " and (cr.start_bin is null or (? between cr.start_bin and cr.end_bin)) " +
    	                " and (cr.cc_type is null or cr.cc_type = ?) " +
    	                " and (cr.country_id is null or cr.country_id = ?) " +
    	                " and (cr.currency_id is null or cr.currency_id = ?) " +
    	                " and (cr.skin_id is null or cr.skin_id = ?) " +
    	                " and (cr.business_skin_id is null or cr.business_skin_id in (select s.business_case_id " +
    				                                                                " from skins s " +
    				                                                                " where s.id = ?)) " +
                        " and cr.platform_id = ? " +
    				" order by " +
    				   	" cr.start_bin, " +
    	                " cr.currency_id, " +
    				   	" cr.cc_type, " +
    	                " cr.country_id, " +
    	                " cr.skin_id, " +
    	                " cr.business_skin_id ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setString(index++, bin);
            pstmt.setLong(index++, ccTypeId);
            pstmt.setLong(index++, countryId);
            pstmt.setLong(index++, currencyId);
            pstmt.setLong(index++, skinId);
            pstmt.setLong(index++, skinId);
            pstmt.setLong(index++, platformId);

            rs = pstmt.executeQuery();
            if (rs.next()) {
            	ClearingRoute cr = new ClearingRoute();
            	cr.setId(rs.getLong("id"));
            	cr.setStartBIN(rs.getString("start_bin"));
            	cr.setTimelastSuccess(rs.getDate("time_last_success"));
            	cr.setTimeLastReroute(rs.getDate("time_last_reroute"));
            	cr.setBinId(rs.getLong("bin_id"));

            	// check if black list
            	if (rs.getLong("deposit_clearing_provider_id") == 0 && rs.getLong("withdraw_clearing_provider_id") == 0 && rs.getLong("cft_clearing_provider_id") == 0){
            		cr.setDepositClearingProviderId(ConstantsBase.BLACK_LIST_BIN);
            		return cr ;
            	}

            	cr.setDepositClearingProviderId(rs.getLong("deposit_clearing_provider_id"));
            	cr.setWithdrawClearingProviderId(rs.getLong("withdraw_clearing_provider_id"));
            	cr.setCFTClearingProviderId(rs.getLong("cft_clearing_provider_id"));
            	log.info("\nRouting for bin=" + bin + " skinId=" + skinId + " currencyId=" + currencyId + " countryId=" +  countryId + " ccTypeId=" + ccTypeId  + "\n" +
           			 "To clearing routing Id=" + cr.getId() + " Deposit Clearing Provider Id=" + cr.getDepositClearingProviderId());
            	return cr;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return null;
    }

 	/**
 	 * BusinessCase as a key for outside hash map
	 * PaymentGroup as a key for inside hash map
 	 * @param conn
 	 * @return cpByPaymentGroupAndBusinessCase as HashMap<Long, HashMap<Long, Long>>
 	 * @throws SQLException
 	 */
 	public static HashMap<Long, HashMap<Long, Long>> loadProvidersIdByPaymentGroupAndBusinessCase(Connection conn) throws SQLException {
        HashMap<Long, HashMap<Long, Long>> cpByPaymentGroupAndBusinessCase = new HashMap<Long, HashMap<Long, Long>>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                " SELECT " +
                    " cp.id, " +
                    " cp.business_case_id, " +
                    " cp.payment_group_id " +
                " FROM " +
                    " clearing_providers cp" +
                " WHERE " +
                    " cp.is_active = 1 ";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	long businessCaseId = rs.getLong("business_case_id");
            	if (businessCaseId != CLEARING_REFERENCE_ANYOPTION_AND_OUROBOROS) {
            		insertCpPaymentGroupAndBusinessCase(cpByPaymentGroupAndBusinessCase, businessCaseId, rs.getLong("payment_group_id"), rs.getLong("id"));
            	} else {
            		insertCpPaymentGroupAndBusinessCase(cpByPaymentGroupAndBusinessCase, Skin.SKIN_BUSINESS_ANYOPTION, rs.getLong("payment_group_id"), rs.getLong("id"));
            		insertCpPaymentGroupAndBusinessCase(cpByPaymentGroupAndBusinessCase, Skin.SKIN_BUSINESS_OUROBOROS, rs.getLong("payment_group_id"), rs.getLong("id"));
            	}
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return cpByPaymentGroupAndBusinessCase;
    }


 
	public static ArrayList<Long> getRelevantlimits(Connection conn, ClearingInfo ci, ClearingRoute cr) throws SQLException{
		ArrayList<Long> list = new ArrayList<Long>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		String sql = " SELECT distinct( crl.clearing_provider_group_id) as clearing_group " +
					 " FROM clearing_routing_limitations crl " +
					 " WHERE crl.is_active = 1 " +
					 " AND (crl.CURRENCY_ID is null or crl.CURRENCY_ID  = ? ) " +
					 " AND (crl.CREDIT_CARD_TYPE_ID is null or crl.CREDIT_CARD_TYPE_ID  = ? ) " +
					 " AND (crl.BIN_ID is null or crl.BIN_ID  = ? ) " +
					 " AND (crl.COUNTRY_ID is null or crl.COUNTRY_ID  = ? ) ";
		try {

			ps = conn.prepareStatement(sql);

			ps .setLong(index++, ci.getCurrencyId());
			ps .setLong(index++, ci.getCcTypeId());
			ps .setLong(index++, cr.getBinId());
			ps .setLong(index++, ci.getCcCountryId());

			rs = ps.executeQuery();
			while(rs.next()){
				Long l =  new Long(rs.getLong("clearing_group"));
				list.add(l);
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;

	}

	public static void updateTimeLastReroute(Connection conn, long id) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " UPDATE clearing_routes SET time_last_reroute = sysdate WHERE id = ? ";
		try{

			ps = conn.prepareStatement(sql);
			ps .setLong(1, id);
			rs = ps.executeQuery();

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}


	public static long insertNewClearingRouteAndLog(Connection con, ClearingRoute cr, CreditCard cc, long newClearingProviderId, long errorCodeId, long currnecyId, boolean isInatec, int platformId) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		String sqlInsertRoute = "INSERT into clearing_routes cr (id, start_bin, end_bin, currency_id, cc_type, country_id, skin_id, " +
								" business_skin_id, deposit_clearing_provider_id, withdraw_clearing_provider_id, cft_clearing_provider_id, " +
								" comments, is_active, time_last_success, time_last_reroute, platform_id) " +
								" values (SEQ_CLEARING_ROUTES.nextval, ?, ?, ?, null, null, null, " +
								" 2, ?, ?, ?, ?, 1, null, null, ? ) ";

		String sqlChangeLog = " INSERT into clearing_routes_changes_log crl (id, changed_bin_id, error_code_id, credit_card_type, credit_card_country, " +
				 			  " former_provider_id, current_provider_id, time_created) " +
				  			  " values (seq_clearing_routes_changes.nextval, ?,?,?,?,?,?, sysdate )";

		try {
			con.setAutoCommit(false);
			ps = con.prepareStatement(sqlInsertRoute);
			ps.setString(index++, cc.getBinStr());
			ps.setString(index++, cc.getBinStr());
			if(currnecyId > 0 ){
				ps.setLong(index++, currnecyId);
			} else {
				ps.setNull(index++, Types.LONGVARCHAR);
			}
			if(!isInatec){
				ps.setLong(index++, newClearingProviderId);
				ps.setLong(index++, newClearingProviderId);
				ps.setLong(index++, newClearingProviderId);
			}else {
				ps.setLong(index++, newClearingProviderId);
				if(newClearingProviderId == INATEC_PROVIDER_ID_TRY){
					ps.setLong(index++, newClearingProviderId);
					ps.setNull(index++, Types.LONGNVARCHAR);
				}else{
					ps.setNull(index++, Types.LONGNVARCHAR);
					ps.setLong(index++, INATEC_PROVIDER_ID_CFT);
				}
			}
			ps.setString(index++, ADD_CLEARING_ROUTES_COMMENT);
			ps.setInt(index++, platformId);
			ps.executeQuery();
			index = 1;

			ps = con.prepareStatement(sqlChangeLog);
			ps.setLong(index++, DAOBase.getSeqCurValue(con, "SEQ_CLEARING_ROUTES"));
			ps.setLong(index++, errorCodeId);
			ps.setLong(index++, cc.getTypeIdByBin());
			ps.setLong(index++, cc.getCountryId());
			ps.setLong(index++, cr.getDepositClearingProviderId());
			ps.setLong(index++, newClearingProviderId);

			rs = ps.executeQuery();

			con.commit();

		} catch(SQLException s) {
			try{
				con.rollback();
			}catch(SQLException se){

			}
			throw s;

		} finally {
			con.setAutoCommit(true);
			closeResultSet(rs);
			closeStatement(ps);
		}
		return DAOBase.getSeqCurValue(con, "SEQ_CLEARING_ROUTES");
	}

	public static ClearingRoute getClearingRouteById(Connection conn ,long id) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		ClearingRoute cr = null;
		String sql =" select * from clearing_routes cr where cr.id = ? ";
		try{
			ps = conn.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()){
				cr = new ClearingRoute();
				cr.setId(rs.getLong("id"));
				cr.setStartBIN(rs.getString("start_bin"));
				cr.setDepositClearingProviderId(rs.getLong("deposit_clearing_provider_id"));
            	cr.setWithdrawClearingProviderId(rs.getLong("withdraw_clearing_provider_id"));
            	cr.setCFTClearingProviderId(rs.getLong("cft_clearing_provider_id"));
			}
		}finally{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return cr ;
	}

	public static ArrayList<ClearingRoute> getClearingRouteByBin(Connection con, String bin){
		ArrayList<ClearingRoute> list = new ArrayList<ClearingRoute>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " SELECT cr.*  " +
					 " FROM clearing_routes cr " +
					 " WHERE cr.start_bin = ? and cr.cc_type is null  " +
							                " and cr.country_id is null  " +
							                " and cr.currency_id is null  " +
							                " and cr.skin_id is null " +
					 " ORDER BY    cr.start_bin, " +
								 " cr.currency_id, " +
								 " cr.cc_type, " +
								 " cr.country_id, " +
								 " cr.skin_id ";
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, bin);
			rs = ps.executeQuery();
			while(rs.next()){
				ClearingRoute cr = new ClearingRoute();
            	cr.setId(rs.getLong("id"));
            	cr.setStartBIN(rs.getString("start_bin"));
            	cr.setTimelastSuccess(rs.getDate("time_last_success"));
            	cr.setTimeLastReroute(rs.getDate("time_last_reroute"));
            	cr.setBusinessSkinId(rs.getLong("business_skin_id"));
            	cr.setDepositClearingProviderId(rs.getLong("deposit_clearing_provider_id"));
            	cr.setWithdrawClearingProviderId(rs.getLong("withdraw_clearing_provider_id"));
            	cr.setCFTClearingProviderId(rs.getLong("cft_clearing_provider_id"));
            	cr.setActive(rs.getLong("is_active") == 0 ? false : true);
            	list.add(cr);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}

 	public static void updateClearingRouteIsActive(Connection conn, boolean isActive, long crId, CreditCard cc, long errorCodeId) throws Exception{
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		String sql = " UPDATE clearing_routes " +
					 " SET is_active = ? " +
					 " WHERE  " +
					   " id = ? ";

		String sqlChangeLog = " INSERT into clearing_routes_changes_log crl (id, changed_bin_id, error_code_id, credit_card_type, credit_card_country, former_provider_id, current_provider_id, time_created) " +
				  " values (seq_clearing_routes_changes.nextval, ?,?,?,?,0,0, sysdate )";
		try {
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setLong(1, isActive ? 1 : 0);
			ps .setLong(2, crId);
			ps.executeQuery();

			ps = conn.prepareStatement(sqlChangeLog);
			ps.setLong(index++, crId);
			ps.setLong(index++, errorCodeId);
			ps.setLong(index++, cc.getTypeIdByBin());
			ps.setLong(index++, cc.getCountryId());

			rs = ps.executeQuery();

			conn.commit();
		} catch (Exception e) {
			log.debug("failed to update is Active where id :" + crId);
			try{
				conn.rollback();
			}catch(SQLException se){

			}
			throw e;

		} finally {
			conn.setAutoCommit(true);
			closeStatement(ps);
			closeResultSet(rs);
		}
	}
 	
	public static ArrayList<ClearingRoute> existclearingRouteWithCurrnecy(Connection conn, String bin) {
		ArrayList<ClearingRoute> list = new ArrayList<ClearingRoute>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " SELECT cr.* " +
					 " from clearing_routes cr " +
					 " WHERE cr.start_bin = ? and ( cr.currency_id = ? or " +
					 							"	cr.currency_id = ? or " +
					 							"	cr.currency_id = ? or " +
					 							"	cr.currency_id = ? ) " +
					 							" 	and cr.cc_type is null  " +
					 							" 	and cr.country_id is null  " +
					 							" 	and cr.skin_id is null  ";
		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1, bin);
			ps.setLong(2, ConstantsBase.CURRENCY_EUR_ID);
			ps.setLong(3, ConstantsBase.CURRENCY_GBP_ID);
			ps.setLong(4, ConstantsBase.CURRENCY_TRY_ID);
			ps.setLong(5, ConstantsBase.CURRENCY_USD_ID);
			rs = ps.executeQuery();
			
			while(rs.next()){
				ClearingRoute cr = new ClearingRoute();
				cr.setId(rs.getLong("id"));
            	cr.setStartBIN(rs.getString("start_bin"));
            	cr.setTimelastSuccess(rs.getDate("time_last_success"));
            	cr.setTimeLastReroute(rs.getDate("time_last_reroute"));
            	cr.setBusinessSkinId(rs.getLong("business_skin_id"));
            	cr.setDepositClearingProviderId(rs.getLong("deposit_clearing_provider_id"));
            	cr.setWithdrawClearingProviderId(rs.getLong("withdraw_clearing_provider_id"));
            	cr.setCFTClearingProviderId(rs.getLong("cft_clearing_provider_id"));
            	cr.setActive(rs.getLong("is_active") == 0 ? false : true);
            	cr.setCurrnecyId(rs.getLong("currency_id"));
            	list.add(cr);
			}			
			
		} catch (SQLException e) {
			log.debug(e);
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
     * Load all providers configs.
     *
     * @param conn
     * @param live <code>true</code> to load live providers configs, <code>false</code> to load test
     * @return <code>ArrayList<ClearingProviderConfig></code>
     * @throws SQLException
     */
    public static ArrayList<ClearingProviderConfig> loadClearingProvidersConfigs(Connection conn) throws SQLException {
        ArrayList<ClearingProviderConfig> l = new ArrayList<ClearingProviderConfig>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "clearing_providers";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            ClearingProviderConfig c = null;
            while (rs.next()) {
                c = new ClearingProviderConfig();
                c.setId(rs.getLong("id"));
                c.setProviderClass(rs.getString("provider_class"));
                c.setUrl(rs.getString("url"));
                c.setTerminalId(rs.getString("terminal_id"));
                c.setUserName(rs.getString("user_name"));
                c.setPassword(rs.getString("password"));
                c.setProps(rs.getString("props"));
                c.setName(rs.getString("name"));
                c.setNonCftAvailable(rs.getLong("non_cft_available"));
                c.setActive(rs.getLong("is_active") == 0 ? false : true);
                c.setPrivateKey(rs.getString("private_key"));
                c.setIs3dActive(rs.getLong("is_3d_available") == 0 ? false : true);
                l.add(c);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

    /**
     * Apply routing rules to skin, bin and currency and give route if available.
     *
     * @param conn
     * @param skinId
     * @param bin
     * @param currencyId
     * @param deposit
     * @param countryId
     * @param ccTypeId
     * @param platformId 
     * @return Provider id to process the transaction through or 0 if no route configured.
     * @throws SQLException
     */
	public static ArrayList<ClearingRoute> findRouteList(Connection conn, long skinId, String bin, long currencyId, long countryId, long ccTypeId, int platformId) throws SQLException {
		log.info("Trying to find rout with: " +
				"skinId= " + skinId + ", bin= " + bin + ", currencyId= " + currencyId + 
						", countryId=" + countryId + ", ccTypeId=" + ccTypeId + ", platformId=" + platformId);
		
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int index = 1;
        try {
            String sql =
                " select " +
                	" b.id as bin_id, " +
                	" cr.id, " +
                	" cr.time_last_success, " +
                	" cr.time_last_reroute, " +
                	" cr.start_bin, " +
                	" cr.deposit_clearing_provider_id, " +
				    " cr.withdraw_clearing_provider_id, " +
				    " cr.cft_clearing_provider_id " +
				" from " +
				   	" CLEARING_ROUTES cr LEFT JOIN bins b ON cr.start_bin like b.from_bin " +
                " where " +
	                " cr.is_active = 1 " +
		            " and (cr.start_bin is null or (? between cr.start_bin and cr.end_bin)) " +
	                " and (cr.cc_type is null or cr.cc_type = ?) " +
	                " and (cr.country_id is null or cr.country_id = ?) " +
	                " and (cr.currency_id is null or cr.currency_id = ?) " +
	                " and (cr.skin_id is null or cr.skin_id = ?) " +
	                " and (cr.business_skin_id is null or cr.business_skin_id in (select s.business_case_id " +
				                                                                " from skins s " +
				                                                                " where s.id = ?)) " +
                    " and cr.platform_id = ? " +
				" order by " +
				   	" cr.start_bin, " +
	                " cr.currency_id, " +
				   	" cr.cc_type, " +
	                " cr.country_id, " +
	                " cr.skin_id, " +
	                " cr.business_skin_id ";


            pstmt = conn.prepareStatement(sql);
            pstmt.setString(index++, bin);
            pstmt.setLong(index++, ccTypeId);
            pstmt.setLong(index++, countryId);
            pstmt.setLong(index++, currencyId);
            pstmt.setLong(index++, skinId);
            pstmt.setLong(index++, skinId);
            pstmt.setInt(index++, platformId);

            rs = pstmt.executeQuery();
            ArrayList<ClearingRoute> al = new ArrayList<ClearingRoute>();
            while (rs.next()) {
            	ClearingRoute cr = new ClearingRoute();
            	cr.setId(rs.getLong("id"));
            	cr.setStartBIN(rs.getString("start_bin"));
            	cr.setTimelastSuccess(rs.getDate("time_last_success"));
            	cr.setBinId(rs.getLong("bin_id"));
            	cr.setTimeLastReroute(rs.getDate("time_last_reroute"));

            	// check if black list
            	if (rs.getLong("deposit_clearing_provider_id") == 0 && rs.getLong("withdraw_clearing_provider_id") == 0 && rs.getLong("cft_clearing_provider_id") == 0){
            		cr.setDepositClearingProviderId(ConstantsBase.BLACK_LIST_BIN);
            	} else {
	            	cr.setDepositClearingProviderId(rs.getLong("deposit_clearing_provider_id"));
	            	cr.setWithdrawClearingProviderId(rs.getLong("withdraw_clearing_provider_id"));
	            	cr.setCFTClearingProviderId(rs.getLong("cft_clearing_provider_id"));
            	}
            	al.add(cr);
            	log.info("found rout with: " +
        				"skinId= " + skinId + ", bin= " + bin + ", currencyId= " + currencyId + 
        						", countryId=" + countryId + ", ccTypeId=" + ccTypeId + ", platformId=" + platformId + ", clearing_rout_id= " + cr.getId());
            }
            if(al.size()>0) {
            	return al;
            } else {
            	return null;
            }

        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
    }

    /**
     * Load provider conf by probider id
     * @param conn
     * @param id provider to load
     * @return
     * @throws SQLException
     */
    public static ClearingProviderConfig loadClearingProviderById(Connection conn, long id) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ClearingProviderConfig c = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "clearing_providers " +
                "WHERE " +
                	"id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                c = new ClearingProviderConfig();
                c.setId(rs.getLong("id"));
                c.setProviderClass(rs.getString("provider_class"));
                c.setUrl(rs.getString("url"));
                c.setTerminalId(rs.getString("terminal_id"));
                c.setUserName(rs.getString("user_name"));
                c.setPassword(rs.getString("password"));
                c.setProps(rs.getString("props"));
                c.setName(rs.getString("name"));
                c.setActive(rs.getLong("is_active") == 0 ? false : true);
                c.setPrivateKey(rs.getString("private_key"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return c;
    }

	/**
	 * get providers name
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getProvidersSI(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		try {
			String sql =
				"SELECT " +
				    "id, " +
					"name " +
				"FROM " +
					"clearing_providers " +
				"ORDER BY " +
					"Name";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("name")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
	
	/**
	 * get providers name
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static Map<Integer, String> getProvidersNameMaping(Connection conn) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<Integer, String> providers = new LinkedHashMap<Integer, String>();
		try {
			String sql =
				"SELECT " +
			    "	id " +
				"	,name " +
				"FROM " +
				"	clearing_providers " +
				"ORDER BY " +
				"	name";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				providers.put(rs.getInt("id"), rs.getString("name"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return providers;
	}
	
	/**
	 * get providers name
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static Map<Integer, String> getCCProvidersNameMaping(Connection conn) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<Integer, String> providers = new LinkedHashMap<Integer, String>();
		try {
			String sql =
				"SELECT " +
			    "	id " +
				"	,name " +
				"FROM " +
				"	clearing_providers " +
				"WHERE " +
				"	cc_available = 1 " +
				"	AND is_active = 1 " +
				"ORDER BY " +
				"	name";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				providers.put(rs.getInt("id"), rs.getString("name"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return providers;
	}

	public static ArrayList<Long> getRoutingLimitations(Connection conn, long currencyId, long ccTypeId, long binId, long countryId) throws SQLException{
		ArrayList<Long> list = new ArrayList<Long>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		String sql = " SELECT distinct( crl.clearing_provider_group_id) as clearing_group " +
					 " FROM clearing_routing_limitations crl " +
					 " WHERE crl.is_active = 1 " +
					 " AND (crl.CURRENCY_ID is null or crl.CURRENCY_ID  = ? ) " +
					 " AND (crl.CREDIT_CARD_TYPE_ID is null or crl.CREDIT_CARD_TYPE_ID  = ? ) " +
					 " AND (crl.BIN_ID is null or crl.BIN_ID  = ? ) " +
					 " AND (crl.COUNTRY_ID is null or crl.COUNTRY_ID  = ? ) ";
		try {

			ps = conn.prepareStatement(sql);

			ps .setLong(index++, currencyId);
			ps .setLong(index++, ccTypeId);
			ps .setLong(index++, binId);
			ps .setLong(index++, countryId);

			rs = ps.executeQuery();
			while(rs.next()){
				Long l =  new Long(rs.getLong("clearing_group"));
				list.add(l);
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;

	}

	public static void updateTimeLastSuccess(Connection conn, long id) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " UPDATE clearing_routes SET time_last_success = sysdate WHERE id = ? ";
		try{

			ps = conn.prepareStatement(sql);
			ps .setLong(1, id);
			rs = ps.executeQuery();

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}

	public static void updateTimeLastRerouted(Connection conn, long id) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " UPDATE clearing_routes SET time_last_reroute = sysdate WHERE id = ? ";
		try{

			ps = conn.prepareStatement(sql);
			ps .setLong(1, id);
			rs = ps.executeQuery();

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}

	public static void updateClearingProviderAndLog(Connection con, ClearingRoute cr, CreditCard cc, long newClearingProviderId, long errorCodeId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		String sqlChangeProvider = " UPDATE clearing_routes " +
								   " SET deposit_clearing_provider_id = ? , " +
								   	   " withdraw_clearing_provider_id = ? , " +
								   	   " cft_clearing_provider_id = ? , " +
								   	   " time_last_success = sysdate " +
								   "  WHERE  " +
								   " id = ? ";
		
		String sqlChangeLog = " INSERT into clearing_routes_changes_log crl (id, changed_bin_id, error_code_id, credit_card_type, credit_card_country, former_provider_id, current_provider_id, time_created) " +
							  " values (seq_clearing_routes_changes.nextval, ?,?,?,?,?,?, sysdate )";
		try {
			con.setAutoCommit(false);
			ps = con.prepareStatement(sqlChangeProvider);
			ps.setLong(index++, newClearingProviderId);
			ps.setLong(index++, newClearingProviderId);
			ps.setLong(index++, newClearingProviderId);
//			if(!isInatec){
//				ps.setLong(index++, newClearingProviderId);
//				ps.setLong(index++, newClearingProviderId);
//				ps.setLong(index++, newClearingProviderId);
//			}else {
//				ps.setLong(index++, newClearingProviderId);
//				if(newClearingProviderId == ClearingManager.INATEC_PROVIDER_ID_TRY){
//					ps.setLong(index++, newClearingProviderId);
//					ps.setNull(index++, Types.LONGNVARCHAR);
//				}else{
//					ps.setNull(index++, Types.LONGNVARCHAR);
//					ps.setLong(index++, ClearingManager.INATEC_PROVIDER_ID_CFT);
//				}
//			}
			ps.setLong(index++, cr.getId());
			
			ps.executeQuery();
			index = 1;
			
			ps = con.prepareStatement(sqlChangeLog);
			ps.setLong(index++, cr.getId());
			ps.setLong(index++, errorCodeId);
			ps.setLong(index++, cc.getTypeIdByBin());
			ps.setLong(index++, cc.getCountryId());
			ps.setLong(index++, cr.getDepositClearingProviderId());
			ps.setLong(index++, newClearingProviderId);
			
			rs = ps.executeQuery();
			
			con.commit();
			
		} catch(SQLException s) {
			try{
				con.rollback();
			}catch(SQLException se){

			}
			throw s;

		} finally {
			con.setAutoCommit(true);
			closeResultSet(rs);
			closeStatement(ps);
		}

	}

//	public static long insertNewClearingRouteAndLog(Connection con, ClearingRoute cr, CreditCard cc, long newClearingProviderId, long errorCodeId, long currnecyId, boolean isInatec) throws SQLException{
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		int index = 1;
//		String sqlInsertRoute = "INSERT into clearing_routes cr (id, start_bin, end_bin, currency_id, cc_type, country_id, skin_id, " +
//								" business_skin_id, deposit_clearing_provider_id, withdraw_clearing_provider_id, cft_clearing_provider_id, " +
//								" comments, is_active, time_last_success, time_last_reroute) " +
//								" values (SEQ_CLEARING_ROUTES.nextval, ?, ?, ?, null, null, null, " +
//								" 2, ?, ?, ?, ?, 1, null, null ) ";
//
//		String sqlChangeLog = " INSERT into clearing_routes_changes_log crl (id, changed_bin_id, error_code_id, credit_card_type, credit_card_country, " +
//				 			  " former_provider_id, current_provider_id, time_created) " +
//				  			  " values (seq_clearing_routes_changes.nextval, ?,?,?,?,?,?, sysdate )";
//
//		try {
//			con.setAutoCommit(false);
//			ps = con.prepareStatement(sqlInsertRoute);
//			ps.setString(index++, cc.getBinStr());
//			ps.setString(index++, cc.getBinStr());
//			if(currnecyId > 0 ){
//				ps.setLong(index++, currnecyId);
//			} else {
//				ps.setNull(index++, Types.LONGVARCHAR);
//			}
//			if(!isInatec){
//				ps.setLong(index++, newClearingProviderId);
//				ps.setLong(index++, newClearingProviderId);
//				ps.setLong(index++, newClearingProviderId);
//			}else {
//				ps.setLong(index++, newClearingProviderId);
//				if(newClearingProviderId == ClearingManager.INATEC_PROVIDER_ID_TRY){
//					ps.setLong(index++, newClearingProviderId);
//					ps.setNull(index++, Types.LONGNVARCHAR);
//				}else{
//					ps.setNull(index++, Types.LONGNVARCHAR);
//					ps.setLong(index++, ClearingManager.INATEC_PROVIDER_ID_CFT);
//				}
//			}
//			ps.setString(index++, ConstantsBase.ADD_CLEARING_ROUTES_COMMENT);
//
//			ps.executeQuery();
//			index = 1;
//
//			ps = con.prepareStatement(sqlChangeLog);
//			ps.setLong(index++, DAOBase.getSeqCurValue(con, "SEQ_CLEARING_ROUTES"));
//			ps.setLong(index++, errorCodeId);
//			ps.setLong(index++, cc.getTypeIdByBin());
//			ps.setLong(index++, cc.getCountryId());
//			ps.setLong(index++, cr.getDepositClearingProviderId());
//			ps.setLong(index++, newClearingProviderId);
//
//			rs = ps.executeQuery();
//
//			con.commit();
//
//		} catch(SQLException s) {
//			try{
//				con.rollback();
//			}catch(SQLException se){
//
//			}
//			throw s;
//
//		} finally {
//			con.setAutoCommit(true);
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return DAOBase.getSeqCurValue(con, "SEQ_CLEARING_ROUTES");
//	}
//
//	public static ClearingRoute getClearingRouteById(Connection conn ,long id) throws SQLException{
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ClearingRoute cr = null;
//		String sql =" select * from clearing_routes cr where cr.id = ? ";
//		try{
//			ps = conn.prepareStatement(sql);
//			ps.setLong(1, id);
//			rs = ps.executeQuery();
//			if (rs.next()){
//				cr = new ClearingRoute();
//				cr.setId(rs.getLong("id"));
//				cr.setStartBIN(rs.getString("start_bin"));
//            	cr.setBusinessSkinId(rs.getLong("business_skin_id"));
//            	cr.setSkinId(rs.getLong("skin_id"));
//            	cr.setCurrnecyId(rs.getLong("currency_id"));
//            	cr.setCountryId(rs.getLong("country_id"));
//            	cr.setCcType(rs.getLong("cc_type"));
//				cr.setDepositClearingProviderId(rs.getLong("deposit_clearing_provider_id"));
//            	cr.setWithdrawClearingProviderId(rs.getLong("withdraw_clearing_provider_id"));
//            	cr.setCFTClearingProviderId(rs.getLong("cft_clearing_provider_id"));
//			}
//		}finally{
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return cr ;
//	}

 	public static long getProviderGroupIdByProviderId(Connection conn, long clearingProviderId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;

		String sql = " SELECT " +
								"group_id " +
					 " FROM " +
					 			"clearing_providers_groups " +
					 " WHERE " +
					 			"clearing_provider_id = ? ";

		try {

			ps = conn.prepareStatement(sql);
			ps .setLong(1, clearingProviderId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("group_id" );
			} else {
				return 0l;
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

	}
 	
 	/**
 	 * insert clearing providers id by payment group and business case
 	 * @param cpByPaymentGroupAndRef
 	 * @param refId
 	 * @param paymentGroupId
 	 * @param providerId
 	 */
 	private static void insertCpPaymentGroupAndBusinessCase(HashMap<Long, HashMap<Long, Long>> cpByPaymentGroupAndRef, long refId, long paymentGroupId, long providerId) {
    	if (!cpByPaymentGroupAndRef.containsKey(refId)) {
    		HashMap<Long, Long> hm = new HashMap<Long, Long>();
    		hm.put(paymentGroupId, providerId);
    		cpByPaymentGroupAndRef.put(refId, hm);
    	} else {
    		HashMap<Long, Long> hm = cpByPaymentGroupAndRef.get(refId);
    		hm.put(paymentGroupId, providerId);
    	}
 	}
 	
  	/**
 	 * @param conn
 	 * @return
 	 * @throws SQLException
 	 */
 	public static ArrayList<ClearingRoute> getClearingRoutes(Connection conn) throws SQLException {
 		ArrayList<ClearingRoute> clearingRoute = new ArrayList<ClearingRoute>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                "	cr.id as cr_id " +
                "	,cr.start_bin" +
                "	,cr.end_bin" +
                "	,cr.currency_id" +
                "	,cr.cc_type" +
                "	,cr.country_id" +
                "	,cr.skin_id" +
                "	,cr.business_skin_id" +
                "	,cr.deposit_clearing_provider_id" +
                "	,cr.withdraw_clearing_provider_id" +
                "	,cr.cft_clearing_provider_id" +
                "	,cr.comments" +
                "	,cr.is_active" +
                "	,cr.time_last_success" +
                "	,cr.time_last_reroute" +
                //"	,cr.platform_id" +                
                "	,p.id as platformId" +
                "	,p.name" +
                "	,p.display_name" +
                "	,p.comments " +
				"FROM " +
			   	"	clearing_routes cr " +
			   	"	,platforms p " +
			   	"WHERE" +
			   	"	cr.platform_id = p.id " +
				"ORDER BY " +
			   	" 	cr.start_bin " +
                " 	,cr.currency_id " +
			   	" 	,cr.cc_type " +
                " 	,cr.country_id " +
                " 	,cr.skin_id " +
                " 	,cr.business_skin_id ";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	ClearingRoute cr = new ClearingRoute();
            	cr.setId(rs.getLong("cr_id"));            	
            	cr.setStartBIN(rs.getString("start_bin"));
            	cr.setEndBIN(rs.getString("end_bin"));
            	cr.setCurrnecyId(rs.getLong("currency_id"));
            	cr.setCcType(rs.getLong("cc_type"));
            	cr.setCountryId(rs.getLong("country_id"));
            	cr.setSkinId(rs.getLong("skin_id"));
            	cr.setBusinessSkinId(rs.getLong("business_skin_id"));
            	cr.setDepositClearingProviderId(rs.getLong("deposit_clearing_provider_id"));
            	cr.setWithdrawClearingProviderId(rs.getLong("withdraw_clearing_provider_id"));
            	cr.setCFTClearingProviderId(rs.getLong("cft_clearing_provider_id"));
            	cr.setComments(rs.getString("comments"));
            	cr.setActive(rs.getBoolean("is_active"));
            	cr.setTimelastSuccess(rs.getDate("time_last_success"));
            	cr.setTimeLastReroute(rs.getDate("time_last_reroute"));
            	cr.setPlatform(new Platform(rs.getInt("platformId"), rs.getString("name"), rs.getString("comments")));
            	clearingRoute.add(cr);        	
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        
        return clearingRoute;
    }
 		
}
