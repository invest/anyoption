/**
 * 
 */
package com.anyoption.common.daos;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.AppsflyerEvent;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.EpgTransactionStatus;
import com.anyoption.common.beans.PaymentMethod;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.WithdrawFee;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.beans.base.MarketingInfo;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.bl_vos.ClearingLimitaion;
import com.anyoption.common.bl_vos.ClearingRoute;
import com.anyoption.common.bl_vos.TransactionBin;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingManagerBase;
import com.anyoption.common.clearing.ClearingManagerConstants;
import com.anyoption.common.enums.Writers;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.payments.CaptureWithdrawal;
import com.anyoption.common.payments.FinalizeDeposit;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawEntryResult;
import com.anyoption.common.payments.WithdrawUtil;
import com.anyoption.common.payments.epg.EpgDepositRequestDetails;
import com.anyoption.common.payments.epg.EpgRequestDetails;
import com.anyoption.common.payments.epg.EpgWithdrawalRequestDetails;
import com.anyoption.common.service.gmwithdrawal.GmWithdrawalPermissionToDisplayRequest;
import com.anyoption.common.service.gmwithdrawal.GmWithdrawalPermissionToDisplayResult;
import com.anyoption.common.service.gmwithdrawal.GmWithdrawalRequest;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.DbmsOutput;

import oracle.jdbc.OracleTypes;

/**
 * @author pavelhe
 */
public abstract class TransactionsDAOBase extends DAOBase {

	private static final Logger log = Logger.getLogger(TransactionsDAOBase.class);
	
	/**
	 * This specific first deposit check is used for additional fields of funnel
	 * deposits. We have to make sure that the user should be able to change
	 * his/hers currency only once.
	 * 
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static boolean isFirstPossibleDeposit(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " SELECT COUNT(*) num "
							+ " FROM transactions t, "
							  + " transaction_types tt "
							+ " WHERE t.user_id = ? "
							+ " AND "
							  + " t.type_id = tt.id "
							+ " AND (tt.class_type = "
							  			+ TransactionsManagerBase.TRANS_CLASS_DEPOSIT
							+ " OR  "
							  + " t.type_id IN ("
							  		+ TransactionsManagerBase.TRANS_TYPE_ADMIN_DEPOSIT
							  		+ " , "
							  		+ TransactionsManagerBase.TRANS_TYPE_DEPOSIT_BY_COMPANY
							  		+ " , "
							  		+ TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT
							  		+ ")) ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return !(rs.getLong("num") > 0);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return false;
	}
	
	/**
	 * This specific first real deposit check is used for additional fields of funnel
	 * deposits. We have to make sure that the user should be able to change
	 * his/hers DOB and address only once.
	 * 
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static boolean isFirstPossibleDepositExcludeBonus(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " SELECT COUNT(*) num "
							+ " FROM transactions t, "
							  + " transaction_types tt "
							+ " WHERE t.user_id = ? "
							+ " AND "
							  + " t.type_id = tt.id "
							+ " AND (tt.class_type = "
							  			+ TransactionsManagerBase.TRANS_CLASS_DEPOSIT
							+ " OR  "
							  + " t.type_id IN ("
							  		+ TransactionsManagerBase.TRANS_TYPE_ADMIN_DEPOSIT
							  		+ " , "
							  		+ TransactionsManagerBase.TRANS_TYPE_DEPOSIT_BY_COMPANY
							  		+ ")) ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return !(rs.getLong("num") > 0);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return false;
	}	

	public static long getTransactionCountByType(Connection con, long userId, long transType) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select count(id) as count from transactions where user_id = ? and type_id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, transType);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				return rs.getLong("count");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return 0;
	}
	
	/**
	 * Is first succeed deposit
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static boolean isFirstDeposit(Connection con,long userId, long expectedValue) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
			    String sql = " select count(*) num" +
			    			 " from transactions t, transaction_types tt " +
			    			 " where t.type_id = tt.id and t.user_id=? and "+
			    			 " tt.class_type = ? AND "+
			    			 " t.status_id in ("+ TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES +") ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);

				rs = ps.executeQuery();

				if (rs.next()) {
					long num = rs.getLong("num");
					if (num > expectedValue) {
						return false;
					} else {
						return true;
					}

				}
				return false;
			}

			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
	}
	
	public static boolean isPastDeposit(Connection con,long userId) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
			    String sql = " select t.id " +
			    			 " from transactions t, transaction_types tt " +
			    			 " where t.type_id = tt.id and " +
			    			 " t.user_id = ? AND " +
			    			 " tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " AND "+
			    			 " t.status_id in (" + TransactionsManagerBase.TRANS_STATUS_SUCCEED + ", " 
			    			 					 + TransactionsManagerBase.TRANS_STATUS_PENDING + ", " 
			    			 					 + TransactionsManagerBase.TRANS_STATUS_CANCELED_ETRADER + 
			    			 			      ") ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();

			if (rs.next()) {
				return true;
			}
			return false;
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	/**
	 * Count number of real deposit
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static int countRealDeposit(Connection con, long userId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  int numberOfTransactions = 0;
		  try {
			    String sql =" SELECT " +
			    			"	count(*) num" +
			    			 " FROM " +
			    			 "	transactions t, " +
			    			 "	transaction_types tt " +
			    			 " WHERE " +
			    			 "	t.type_id = tt.id " +
			    			 "	AND t.user_id = ? " +
			    			 "	AND tt.class_type = ? " +
			    			 "	AND t.status_id in ("+ TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES +") ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setInt(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);			
			rs = ps.executeQuery(); 
			if (rs.next()) {
				numberOfTransactions = rs.getInt("num");
			}
			return numberOfTransactions;
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}		
	}

	public static AppsflyerEvent checkAppsflyerFirstDeposit(Connection con,
			long transactionId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		AppsflyerEvent appsflyerEvent = null;
		try {
			String sql =" SELECT " +
							" duis.appsflyer_id, " +
							" t.ip, " +
							" (t.amount * t.rate)/100 as amount, " +
							" t.time_created, " +
							" ac.app_id, " +
							" ac.dev_key, " +
							" ac.os_type_id " +
						" FROM  " +
							" transactions t, " +
							" users u, " +
							" device_unique_ids_skin duis, " +
							" appsflyer_config ac " +
						" WHERE " +
							" t.id = ? AND " +
							" t.writer_id not in (" + Writer.WRITER_ID_MOBILE + ", " + Writer.WRITER_ID_COPYOP_MOBILE + ") AND " + //--not deposit from mobile or copyop 
							" t.user_id = u.id AND " +
							" u.writer_id in (" + Writer.WRITER_ID_MOBILE + ", " + Writer.WRITER_ID_COPYOP_MOBILE + ") AND " + //-- user register in mobile or copyop
							" u.device_unique_id = duis.device_unique_id AND " +
							" duis.appsflyer_id IS NOT null AND " + //-- got appsflyer id 
							" ac.os_type_id = duis.os_type_id AND " +
							" ac.platform_id = u.platform_id ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, transactionId);	
			rs = ps.executeQuery(); 
			if (rs.next()) {
				appsflyerEvent = new AppsflyerEvent();
				appsflyerEvent.setAppsflyerId(rs.getString("appsflyer_id"));
				appsflyerEvent.setIp(rs.getString("ip"));
				appsflyerEvent.setAmount(rs.getDouble("amount"));
				appsflyerEvent.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				appsflyerEvent.setAppId(rs.getString("app_id"));
				appsflyerEvent.setDevKey(rs.getString("dev_key"));
				appsflyerEvent.setOsTypeId(rs.getInt("os_type_id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return appsflyerEvent;
	}
	
	/**
	 * Get marketing ftds
	 * @param con
	 * @param mi
	 * @param affiliateKey
	 * @param dateRequest
	 * @return MarketingInfo
	 * @throws SQLException
	 * @throws ParseException
	 */
	public static MarketingInfo getMarketingFtds(Connection con, MarketingInfo mi, long affiliateKey, String dateRequest) throws SQLException, ParseException {
		CallableStatement stmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = "{? = call API.GET_MARKETING_FTDS (?,?)}";
			DbmsOutput dbmsOutput = new DbmsOutput( con );
	        dbmsOutput.enable();
			stmt = con.prepareCall(sql);
	        stmt.registerOutParameter(index++, OracleTypes.CURSOR);
	        stmt.setString(index++, dateRequest);
	        stmt.setLong(index++, affiliateKey);
	        stmt.execute();
	        dbmsOutput.show();
	        dbmsOutput.close();
	        rs = (ResultSet)stmt.getObject(1);
			if (rs.next()) {
				mi.setCountFtds(rs.getLong("num_ftds"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(stmt);
		}
		return mi;
	}
	
	
	/**
	 * @param connection
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static boolean isHasOpenWithdraw(Connection connection, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean result = false;
		try {
			String sql =
					"SELECT " +
					"	u.id as user_id" +
					"	,u.user_name" + 
					"	,t.id as transaction_id" + 
					"	,t.status_id " + 
					"FROM " + 
					"	users u" + 
					"	,transactions t" + 
					"	,transaction_types tt" +
					"	,transaction_class_types tct " +
					"WHERE " + 
					"	u.id = t.user_id" + 
					"	AND t.type_id = tt.id " +
					"	AND tt.class_type = tct.id " +
					"	AND tct.id = " + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_WITHDRAWALS +
					" 	AND t.status_id  in (" + TransactionsManagerBase.TRANS_STATUS_REQUESTED + 
												"," + TransactionsManagerBase.TRANS_STATUS_APPROVED + 
												"," + TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL + 
											")" +
					"	AND t.user_id = ? ";
			
			ps = connection.prepareStatement(sql);
			ps.setLong(1, userId);
			result = ps.executeQuery().next();
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return result;		
	}
	
	public static void updateVisibleByUserId(Connection con, long userId, long cardId) throws SQLException{
		PreparedStatement ps = null;
		try {
			String sql="UPDATE " +
					"credit_cards " +
					"SET " +
					"is_visible=0 " +
					"WHERE " +
					"id=? " +
					"AND " +
					"user_id=?";

			ps = con.prepareStatement(sql);
			ps.setLong(1, cardId);
			ps.setLong(2, userId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

		/**
		 * Check if user have withdrawals transactions This is for fee logic, in case the user did some withdrawal then we take Fixed fee
		 * amount.
		 * 
		 * @param con Db connection
		 * @param userId user id - to check his transactions
		 * @param status take transactions with this statuses
		 * @return
		 * @throws SQLException
		 */
		public static long getMonthlyWithdrawalsCount(Connection con, long userId, Date timeCreated, String utcOffset) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql = "SELECT count(t.id) as tr_count "
								+ "FROM transactions t,transaction_types p, users u "
								+ "WHERE t.user_id=u.id AND t.user_id = ? AND "
								+ "(t.status_id = ? OR ( t.status_id = ? AND is_accounting_approved = 1 )) AND "
								+ "t.type_id = p.id AND "
								+ "p.class_type = ? "
								+ "AND EXTRACT (MONTH FROM GET_TIME_BY_OFFSET(t.time_created, t.utc_offset_created)) = EXTRACT(MONTH FROM GET_TIME_BY_OFFSET(?, ?)) "
								+ "AND EXTRACT (YEAR FROM GET_TIME_BY_OFFSET(t.time_created, t.utc_offset_created)) = EXTRACT(YEAR FROM GET_TIME_BY_OFFSET(?, ?)) ";
				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setLong(2, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
				ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL);
				ps.setLong(4, TransactionsManagerBase.TRANS_CLASS_WITHDRAW);
				ps.setTimestamp(5, CommonUtil.convertToTimeStamp(timeCreated));
				ps.setString(6, utcOffset);
				ps.setTimestamp(7, CommonUtil.convertToTimeStamp(timeCreated));
				ps.setString(8, utcOffset);
				rs = ps.executeQuery();
				if (rs.next()) {
					return rs.getLong("tr_count");
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return 0l;
		}
	 
	/**
	 * Check if user have withdrawals transactions This is for fee logic, in case the user did some withdrawal then we take Fixed fee
	 * amount.
	 * 
	 * @param con Db connection
	 * @param userId user id - to check his transactions
	 * @param status take transactions with this statuses
	 * @return
	 * @throws SQLException
	 */
	public static boolean hasMonthlyWithdrawals(Connection con, long userId, Date timeCreated, String utcOffset) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean flag = false;
		try {
			String sql = "SELECT t.id "
							+ "FROM transactions t,transaction_types p, users u "
							+ "WHERE t.user_id=u.id AND t.user_id = ? AND "
							+ "(t.status_id = ? OR ( t.status_id = ? AND is_accounting_approved = 1 )) AND "
							+ "t.type_id = p.id AND "
							+ "p.class_type = ? "
							+ "AND EXTRACT (MONTH FROM GET_TIME_BY_OFFSET(t.time_created, t.utc_offset_created)) = EXTRACT(MONTH FROM GET_TIME_BY_OFFSET(?, ?)) "
							+ "AND EXTRACT (YEAR FROM GET_TIME_BY_OFFSET(t.time_created, t.utc_offset_created)) = EXTRACT(YEAR FROM GET_TIME_BY_OFFSET(?, ?)) ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
			ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_APPROVED);
			ps.setLong(4, TransactionsManagerBase.TRANS_CLASS_WITHDRAW);
			ps.setTimestamp(5, CommonUtil.convertToTimeStamp(timeCreated));
			ps.setString(6, utcOffset);
			ps.setTimestamp(7, CommonUtil.convertToTimeStamp(timeCreated));
			ps.setString(8, utcOffset);
			rs = ps.executeQuery();
			if (rs.next()) {   // user have monthly withdrawals
				flag = true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return flag;
	}

	public static boolean shouldChargeForLowWithdraw(Connection con, long id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select UTILS.GET_APPLY_WITHDRAWAL_FEE(?) as should_charge from dual";
			ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getBoolean("should_charge");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}

	public static long insertTranBin(Connection con, TransactionBin tb) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String sql = " insert into TRANSACTIONS_BINS( id, from_bin, time_last_success, time_last_failed_reroute, updated_by_rerouting, business_case_id, platform_id) " +
				" values (SEQ_TRANSACTIONS_BINS.nextval , ?, null, null, 1, ?, ?)";
		try {
			ps = con.prepareStatement(sql);
			ps.setLong(1, tb.getBin());
			ps.setLong(2, tb.getBusinessCaseId());
			ps.setLong(3, tb.getPlatformId());
			rs = ps.executeQuery();
			tb.setId(getSeqCurValue(con, "SEQ_TRANSACTIONS_BINS"));
			tb = getTransactionBinById(con, tb.getId());
		} finally {
			closeResultSet(rs);
			closeStatement(ps);			
		}
		return tb.getId();
	}
	
	public static TransactionBin getTransactionBinById(Connection conn, long id) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		TransactionBin tb = new TransactionBin();
		String sql ="select * from transactions_bins where id = ?";
		try {
			ps = conn.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				tb.setId(rs.getLong("id"));
				tb.setBin(rs.getLong("from_bin"));
				tb.setTimeLastSuccess(rs.getDate("time_last_success"));
				tb.setTimeLastFailedReroute(rs.getDate("time_last_failed_reroute"));
				tb.setBusinessCaseId(rs.getLong("business_case_id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return tb;		
	}
	
	public static TransactionBin getTransactionBinByBin(Connection con, long bin, long businessCaseId) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		TransactionBin tb = new TransactionBin();
		String sql = "select * from transactions_bins where from_bin = ? and business_case_id = ? ";
		try{
			ps = con.prepareStatement(sql);
			ps.setLong(1, bin);
			ps.setLong(2, businessCaseId);
			rs = ps.executeQuery();
			if(rs.next()){
				tb.setBin(bin);
				tb.setId(rs.getLong("id"));
				tb.setTimeLastSuccess(rs.getDate("time_last_success"));
				tb.setTimeLastFailedReroute(rs.getDate("time_last_failed_reroute"));
				tb.setBusinessCaseId(rs.getLong("business_case_id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return tb;
	}
	
	public static ArrayList<Transaction> getRelatedSplittedTran(Connection con, Transaction vo) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList< Transaction> list = new ArrayList<Transaction>();
		String sql = " SELECT t.*" +
					 " FROM transactions t " +
					 " WHERE t.splitted_reference_id = ? and " +
					 " t.type_id = ? ";
		try{
			ps = con.prepareStatement(sql);
			ps.setLong(1, vo.getId());
			ps.setLong(2, TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT);
			rs = ps.executeQuery();
			while(rs.next()){
				Transaction t = new Transaction();
				t.setId(rs.getLong("id"));
				t.setCreditWithdrawal(rs.getLong("is_credit_withdrawal") == 1 ? true : false	);
				t.setDepositReferenceId(rs.getLong("deposit_reference_id"));
				t.setTypeId(rs.getLong("type_id"));
				list.add(t);
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
	/**
	 * Update deposit pixel run time
	 * @param con
	 * @param id
	 * @throws SQLException
	 */
	public static void updatePixelRunTime(Connection con, long id) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
				"UPDATE " +
					"transactions " +
				"SET " +
					"pixel_run_time = sysdate " +
				"WHERE " +
					"id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	public static boolean maintenanceFeeCanceled(Connection con, long maintenanceFeeTrnID) throws SQLException {
		PreparedStatement ps = null;
    	ResultSet rs = null;
		try {
				String sql =	" SELECT " +
											" * " +
								" FROM " +
											" transactions " +
								" WHERE " +
											" reference_id = ? " +
								" AND " +
											" type_id = ? " ;
				ps = con.prepareStatement(sql);
				ps.setLong(1, maintenanceFeeTrnID);
				ps.setLong(2, TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE_CANCEL);
				rs = ps.executeQuery();
				if (rs.next()) {
					return true;
				}
		 } finally {
				closeStatement(ps);
				closeResultSet(rs);
		 }
		 return false;
	}
	
	/**
	 * @param conn
	 * @param transactionId
	 * @return list with all rerouts transactions id's
	 * @throws SQLException
	 */
	public static ArrayList<Long> getTransactionsReroutConnectedIDById(Connection conn, long transactionId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Long> list = new ArrayList<Long>();
		try {
			String sql = 
						" SELECT " +
							" t.id " +
						" FROM " +
							" transactions t " +
						" WHERE " +
							" t.rerouting_transaction_id = ? " +
						" ORDER BY " +
							" t.id asc ";   
				           
			ps = conn.prepareStatement(sql);
			ps.setLong(1, transactionId);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(rs.getLong("id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;	
	}
	
//    public static void insert(Connection con, Transaction vo) throws SQLException {
//        OraclePreparedStatement ps = null;
//        ResultSet rs = null;
//        try {
//            String rate = "trunc(convert_amount_to_usd(1, (select currency_id from users where id = ?), sysdate), " + ConstantsBase.RATE_PRECISISON + ")";
//            String sql =
//                "INSERT INTO transactions " +
//                    "(id, user_id, credit_card_id, type_id, amount, status_id, description, writer_id, ip, time_settled, comments, " +
//                    "processed_writer_id, time_created, reference_id, cheque_id, wire_id, charge_back_id, receipt_num, fee_cancel, " +
//                    "utc_offset_created, utc_offset_settled, is_credit_withdrawal, splitted_reference_id, deposit_reference_id, " +
//                    "payment_type_id, bonus_user_id, is_accounting_approved, rate, auth_number, clearing_provider_id, acquirer_response_id" +
//                    ", is_rerouted, rerouting_transaction_id, selector_id) " +
//                "VALUES " +
//                    "(seq_transactions.nextval, ? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,sysdate, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + rate + ", ?, ?, ?, ?, ?, ?)";
//
//            ps = (OraclePreparedStatement) con.prepareStatement(sql);
//            ps.setFormOfUse(6, OraclePreparedStatement.FORM_NCHAR);
//            ps.setFormOfUse(10, OraclePreparedStatement.FORM_NCHAR);
//
//            ps.setLong(1, vo.getUserId());
//            if (null == vo.getCreditCardId() || 0 == vo.getCreditCardId().longValue()) {
//            	ps.setNull(2, Types.NUMERIC);
//            } else {
//            	ps.setBigDecimal(2,vo.getCreditCardId());
//            }
//            ps.setLong(3, vo.getTypeId());
//            ps.setLong(4, vo.getAmount());
//            ps.setLong(5, vo.getStatusId());
//            ps.setString(6, vo.getDescription());
//            ps.setLong(7, vo.getWriterId());
//            ps.setString(8, vo.getIp());
//            ps.setTimestamp(9, convertToTimestamp(vo.getTimeSettled()));
//            ps.setString(10, vo.getComments());
//            ps.setLong(11, vo.getProcessedWriterId());
//            if (null == vo.getReferenceId() || 0 == vo.getReferenceId().longValue()) {
//                ps.setNull(12, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(12, vo.getReferenceId());
//            }
//            if (null == vo.getChequeId() || 0 == vo.getChequeId().longValue()) {
//                ps.setNull(13, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(13, vo.getChequeId());
//            }
//            if (null == vo.getWireId() || 0 == vo.getWireId().longValue()) {
//                ps.setNull(14, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(14, vo.getWireId());
//            }
//            if (null == vo.getChargeBackId() || 0 == vo.getChargeBackId().longValue()) {
//                ps.setNull(15, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(15, vo.getChargeBackId());
//            }
//            if (null == vo.getReceiptNum() || 0 == vo.getReceiptNum().longValue()) {
//                ps.setNull(16, Types.NUMERIC);
//            } else {
//                ps.setBigDecimal(16, vo.getReceiptNum());
//            }
//            if (vo.isFeeCancel()) {
//                ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_YES);
//            } else {
//                ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_NO);
//            }
//            ps.setString(18, vo.getUtcOffsetCreated());
//            ps.setString(19, vo.getUtcOffsetSettled());
//            if (vo.isCreditWithdrawal()) {
//                ps.setInt(20, 1);
//            } else {
//                ps.setInt(20, 0);
//            }
//            ps.setLong(21, vo.getSplittedReferenceId());
//            ps.setLong(22, vo.getDepositReferenceId());
//            ps.setLong(23, vo.getInatecPaymentTypeId());
//            if (vo.getBonusUserId() > 0) {
//                ps.setLong(24, vo.getBonusUserId());
//            } else {
//                ps.setNull(24, Types.NUMERIC);
//            }
//            if (vo.isAccountingApproved()) {
//                ps.setInt(25, 1);
//            } else {
//                ps.setInt(25, 0);
//            }
//            ps.setLong(26, vo.getUserId());
//            ps.setString(27, vo.getAuthNumber());
//            if (0 == vo.getClearingProviderId()) {
//            	ps.setNull(28, Types.NUMERIC);
//            } else {
//            	ps.setLong(28, vo.getClearingProviderId());
//            }
//            ps.setString(29, vo.getAcquirerResponseId());
//            ps.setLong(30, vo.isRerouted()?1:0);
//            ps.setLong(31, vo.getReroutingTranId());
//            ps.setLong(32, vo.getSelectorId());
//            ps.executeUpdate();
//
//            vo.setId(getSeqCurValue(con, "seq_transactions"));
//
//            if (log.isDebugEnabled()) {
//                log.debug("Transaction started: " + vo.toString());
//            }
//        } finally {
//            closeStatement(ps);
//            closeResultSet(rs);
//        }
//    }

	  public static void insert(Connection con,Transaction vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			  	//String rate = " trunc(convert_amount_to_usd(1,(select currency_id from users where id=?),sysdate),"+ ConstantsBase.RATE_PRECISISON +")";
			  	long currencyId = UsersDAOBase.getCurrIdByUserId(con, vo.getUserId());
			  	double rate = CurrencyRatesManagerBase.getInvestmentsRate(currencyId);
				String sql=	" INSERT INTO transactions t("
															+ " id,"
															+ " user_id,"
															+ " credit_card_id,"
															+ " type_id," 
															+ " amount,"
															+ " status_id,"
															+ " description,"
															+ " writer_id,"
															+ " ip,"
															+ " time_settled,"
															+ " comments,"
															+ " processed_writer_id,"
															+ " time_created,"
															+ " reference_id,"
															+ " cheque_id,"
															+ " wire_id,"
															+ " charge_back_id,"
															+ " receipt_num,"
															+ " fee_cancel,"
															+ " utc_offset_created,"
															+ " utc_offset_settled,"
															+ " is_credit_withdrawal," 
															+ " splitted_reference_id,"
															+ " deposit_reference_id,"
															+ " payment_type_id,"
															+ " bonus_user_id,"
															+ " is_accounting_approved,"
															+ " rate,"
															+ " auth_number,"
															+ " clearing_provider_id,"
															+ " paypal_email,"
															+ " envoy_account_num,"
															+ " moneybookers_email,"
															+ " webmoney_purse,"
															+ " xor_id_authorize, "
															+ " xor_id_capture,"
															+ " acquirer_response_id,"
															+ " is_rerouted,"
															+ " rerouting_transaction_id,"
															+ " selector_id,"
															+ " login_id,"
															+ " wd_fee_exempt_writer_id"
														+ " ) " +
														" VALUES ( "
															+ "SEQ_TRANSACTIONS.NEXTVAL," //id
															+ " ?," //1 user_id
															+ " ?," //2 credit_card_id  				
															+ " ?," //3 type_id
															+ " ?," //4 amount
															+ " ?," //5 status_id
															+ " ?," //6 description
															+ " ?," //7 writer_id
															+ " ?," //8 ip
															+ " ?," //9 time_settled
															+ " ?," //10 comments
															+ " ?," //11 processed_writer_id
															+ " SYSDATE," // time_created
															+ " ?," //12 reference_id
															+ " ?," //13 cheque_id
															+ " ?," //14 wire_id
															+ " ?," //15 charge_back_id
															+ " ?," //16 receipt_num
															+ " ?," //17 fee_cancel
															+ " ?," //18 utc_offset_created
															+ " ?," //19 utc_offset_settled
															+ " ?," //20 is_credit_withdrawal
															+ " ?," //21 splitted_reference_id
															+ " ?," //22 deposit_reference_id
															+ " ?," //23 payment_type_id
															+ " ?," //24 bonus_user_id
															+ " ?," //25 is_accounting_approved
															+ " ?," //26 rate
															+ " ?," //27 auth_number
															+ " ?," //28 clearing_provider_id
															+ " ?," //29 paypal_email
															+ " ?," //30 envoy_account_num
															+ " ?," //31 moneybookers_email
															+ " ?," //32 webmoney_purse
															+ " ?," //33 xor_id_authorize
															+ " ?," //34 xor_id_capture
															+ " ?," //35 acquirer_response_id
															+ " ?," //36 is_rerouted
															+ " ?," //37 rerouting_transaction_id
															+ " ?," //38 selector_id
															+ " ?," //39 login_id
															+ " ? " //40 wd_fee_exempt_writer_id
															+ " ) ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, vo.getUserId());
				ps.setBigDecimal(2,vo.getCreditCardId());
				ps.setLong(3, vo.getTypeId());
				ps.setLong(4, vo.getAmount());
				ps.setLong(5, vo.getStatusId());
				ps.setString(6, vo.getDescription());
				ps.setLong(7, vo.getWriterId());
				ps.setString(8, vo.getIp());
				ps.setTimestamp(9, CommonUtil.convertToTimeStamp(vo.getTimeSettled()));
				ps.setString(10, vo.getComments());
				ps.setLong(11, vo.getProcessedWriterId());
				ps.setBigDecimal(12,vo.getReferenceId());
				ps.setBigDecimal(13,vo.getChequeId());
				ps.setBigDecimal(14,vo.getWireId());
				ps.setBigDecimal(15,vo.getChargeBackId());
				ps.setBigDecimal(16,vo.getReceiptNum());
				if (vo.isFeeCancel()) {
					ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_YES);
				}
				else {
					ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_NO);
				}
				ps.setString(18, vo.getUtcOffsetCreated());
				ps.setString(19, vo.getUtcOffsetSettled());
				if ( vo.isCreditWithdrawal() ) {
					ps.setInt(20, 1);
				} else {
					ps.setInt(20, 0);
				}
				ps.setLong(21, vo.getSplittedReferenceId());
				ps.setLong(22, vo.getDepositReferenceId());
				ps.setLong(23, vo.getPaymentTypeId());
                if (vo.getBonusUserId() > 0) {
                    ps.setLong(24, vo.getBonusUserId());
                } else {
                    ps.setNull(24, Types.NUMERIC);
                }
				if (vo.isAccountingApproved()) {
					ps.setInt(25, 1);
				}
				else {
					ps.setInt(25, 0);
				}
				ps.setDouble(26, rate);
				ps.setString(27, vo.getAuthNumber());
				ps.setLong(28, vo.getClearingProviderId());
				ps.setString(29, vo.getPayPalEmail());
				ps.setString(30, vo.getEnvoyAccountNum());
				ps.setString(31, vo.getMoneybookersEmail());
				ps.setString(32, vo.getWebMoneyPurse());

				if (vo.isPayPalDeposit()){
					ps.setString(33, vo.getXorIdAuthorize());
					ps.setString(34, vo.getXorIdCapture());
				}else{
					ps.setString(33, null);
					ps.setString(34, null);
				}
				ps.setString(35, vo.getAcquirerResponseId());
				ps.setLong(36, vo.isRerouted() ? 1 : 0);
				ps.setLong(37, vo.getReroutingTranId());
				ps.setLong(38, vo.getSelectorId());
				if (vo.getLoginId() > 0) {
                    ps.setLong(39, vo.getLoginId());
                } else {
                    ps.setNull(39, Types.NUMERIC);
                }				
				if (vo.getWithdrawalFeeExemptWriterId() > 0) {
                    ps.setLong(40, vo.getWithdrawalFeeExemptWriterId());
                } else {
                    ps.setNull(40, Types.NUMERIC);
                }
				ps.executeUpdate();

				vo.setId(getSeqCurValue(con,"seq_transactions"));

				if (log.isDebugEnabled()) {
		            String ls = System.getProperty("line.separator");
		            log.log(Level.DEBUG, ls + "Transaction started:" + vo.toString());
				}
		  }	finally {
				closeStatement(ps);
				closeResultSet(rs);
			}

	  } 
	  
    public static long update(Connection con, Transaction vo) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql =
                "UPDATE " +
	                "transactions t " +
	                "SET " +
	                "user_id = ?, " +
	                "credit_card_id = ?, " +
	                "type_id = ?, " +
	                "amount = ?, " +
	                "status_id = ?, " +
	                "description = ?, " +
	                "writer_id = ?, " +
	                "ip = ?, " +
	                "time_settled = ?, " +
	                "comments = ?, " +
	                "processed_writer_id = ?, " +
	                "reference_id = ?, " +
	                "cheque_id = ?, " +
	                "wire_id = ?, " +
	                "charge_back_id = ?, " +
	                "receipt_num = ?, " +
	                "auth_number = ?, " +
	                "xor_id_authorize = ?, " +
	                "utc_offset_settled = ?, " +
	                "clearing_provider_id = ?, " +
	                "fee_cancel = ?, " +
	                "xor_id_capture = ? , " +
	                "acquirer_response_id =?, " +
	                "is_rerouted = ? , " +
	                "rerouting_transaction_id = ?, " +
	                "is_3d = ?,  " +
	                "fibo_3d = ? " +
                "WHERE " +
                	"id = ?";

            ps = con.prepareStatement(sql);

            ps.setLong(1, vo.getUserId());
            if (null == vo.getCreditCardId() || 0 == vo.getCreditCardId().longValue()) {
            	ps.setNull(2, Types.NUMERIC);
            } else {
            	ps.setBigDecimal(2,vo.getCreditCardId());
            }

            ps.setLong(3, vo.getTypeId());
            ps.setLong(4, vo.getAmount());
            ps.setLong(5, vo.getStatusId());
            ps.setString(6, vo.getDescription());
            ps.setLong(7, vo.getWriterId());
            ps.setString(8, vo.getIp());
            ps.setTimestamp(9, convertToTimestamp(vo.getTimeSettled()));
            ps.setString(10, vo.getComments());
            ps.setLong(11, vo.getProcessedWriterId());
            if (null == vo.getReferenceId() || 0 == vo.getReferenceId().longValue()) {
                ps.setNull(12, Types.NUMERIC);
            } else {
                ps.setBigDecimal(12, vo.getReferenceId());
            }
            if (null == vo.getChequeId() || 0 == vo.getChequeId().longValue()) {
                ps.setNull(13, Types.NUMERIC);
            } else {
                ps.setBigDecimal(13, vo.getChequeId());
            }
            if (null == vo.getWireId() || 0 == vo.getWireId().longValue()) {
                ps.setNull(14, Types.NUMERIC);
            } else {
                ps.setBigDecimal(14, vo.getWireId());
            }
            if (null == vo.getChargeBackId() || 0 == vo.getChargeBackId().longValue()) {
                ps.setNull(15, Types.NUMERIC);
            } else {
                ps.setBigDecimal(15, vo.getChargeBackId());
            }
            if (null == vo.getReceiptNum() || 0 == vo.getReceiptNum().longValue()) {
                ps.setNull(16, Types.NUMERIC);
            } else {
                ps.setBigDecimal(16, vo.getReceiptNum());
            }
            ps.setString(17, vo.getAuthNumber());
            ps.setString(18, vo.getXorIdAuthorize());
            ps.setString(19, vo.getUtcOffsetSettled());
            if (0 == vo.getClearingProviderId()) {
            	ps.setNull(20, Types.NUMERIC);
            } else {
            	ps.setLong(20, vo.getClearingProviderId());
            }
            if (vo.isFeeCancel()) {
                ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_YES);
            } else {
                ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_NO);
            }
            ps.setString(22, vo.getXorIdCapture());
            ps.setString(23, vo.getAcquirerResponseId());
            ps.setLong(24, vo.isRerouted()?1:0);
            ps.setLong(25, vo.getReroutingTranId());
            ps.setLong(26, vo.isThreeD() ? 1 : 0);
            ps.setLong(27, vo.isRender() ? 1 : 0);
            ps.setLong(28, vo.getId());            
            executeUpdateRetry(ps);
            if (log.isDebugEnabled()) {
                log.log(Level.DEBUG, "Transaction updated: " + vo);
            }
        } finally {
            closeStatement(ps);
        }
        return vo.getId();
    }
    
    
    public static long updateForStatus(Connection con, Transaction vo, long expectedTransactionStatus) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql =
                "UPDATE " +
	                "transactions t " +
	                "SET " +
	                "user_id = ?, " +
	                "credit_card_id = ?, " +
	                "type_id = ?, " +
	                "amount = ?, " +
	                "status_id = ?, " +
	                "description = ?, " +
	                "writer_id = ?, " +
	                "ip = ?, " +
	                "time_settled = ?, " +
	                "comments = ?, " +
	                "processed_writer_id = ?, " +
	                "reference_id = ?, " +
	                "cheque_id = ?, " +
	                "wire_id = ?, " +
	                "charge_back_id = ?, " +
	                "receipt_num = ?, " +
	                "auth_number = ?, " +
	                "xor_id_authorize = ?, " +
	                "utc_offset_settled = ?, " +
	                "clearing_provider_id = ?, " +
	                "fee_cancel = ?, " +
	                "xor_id_capture = ? , " +
	                "acquirer_response_id =?, " +
	                "is_rerouted = ? , " +
	                "rerouting_transaction_id = ?, " +
	                "is_3d = ?, " +
	                "capture_number = ? " +
                "WHERE " +
                	"id = ? "+ 
            		"AND status_id = ?";

            ps = con.prepareStatement(sql);

            ps.setLong(1, vo.getUserId());
            if (null == vo.getCreditCardId() || 0 == vo.getCreditCardId().longValue()) {
            	ps.setNull(2, Types.NUMERIC);
            } else {
            	ps.setBigDecimal(2,vo.getCreditCardId());
            }

            ps.setLong(3, vo.getTypeId());
            ps.setLong(4, vo.getAmount());
            ps.setLong(5, vo.getStatusId());
            ps.setString(6, vo.getDescription());
            ps.setLong(7, vo.getWriterId());
            ps.setString(8, vo.getIp());
            ps.setTimestamp(9, convertToTimestamp(vo.getTimeSettled()));
            ps.setString(10, vo.getComments());
            ps.setLong(11, vo.getProcessedWriterId());
            if (null == vo.getReferenceId() || 0 == vo.getReferenceId().longValue()) {
                ps.setNull(12, Types.NUMERIC);
            } else {
                ps.setBigDecimal(12, vo.getReferenceId());
            }
            if (null == vo.getChequeId() || 0 == vo.getChequeId().longValue()) {
                ps.setNull(13, Types.NUMERIC);
            } else {
                ps.setBigDecimal(13, vo.getChequeId());
            }
            if (null == vo.getWireId() || 0 == vo.getWireId().longValue()) {
                ps.setNull(14, Types.NUMERIC);
            } else {
                ps.setBigDecimal(14, vo.getWireId());
            }
            if (null == vo.getChargeBackId() || 0 == vo.getChargeBackId().longValue()) {
                ps.setNull(15, Types.NUMERIC);
            } else {
                ps.setBigDecimal(15, vo.getChargeBackId());
            }
            if (null == vo.getReceiptNum() || 0 == vo.getReceiptNum().longValue()) {
                ps.setNull(16, Types.NUMERIC);
            } else {
                ps.setBigDecimal(16, vo.getReceiptNum());
            }
            ps.setString(17, vo.getAuthNumber());
            ps.setString(18, vo.getXorIdAuthorize());
            ps.setString(19, vo.getUtcOffsetSettled());
            if (0 == vo.getClearingProviderId()) {
            	ps.setNull(20, Types.NUMERIC);
            } else {
            	ps.setLong(20, vo.getClearingProviderId());
            }
            if (vo.isFeeCancel()) {
                ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_YES);
            } else {
                ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_NO);
            }
            ps.setString(22, vo.getXorIdCapture());
            ps.setString(23, vo.getAcquirerResponseId());
            ps.setLong(24, vo.isRerouted() ? 1:0);
            ps.setLong(25, vo.getReroutingTranId());
            ps.setLong(26, vo.isIs3D() ? 1 : 0);
            ps.setString(27, vo.getCaptureNumber());
            ps.setLong(28, vo.getId());
            ps.setLong(29, expectedTransactionStatus);
            int k = executeUpdateRetry(ps);
            if(k != 1) {
          	  throw new SQLException("Failed to update trxID:"+ vo.getId() + " to status:"+expectedTransactionStatus);
            }
            if (log.isDebugEnabled()) {
                log.log(Level.DEBUG, "Transaction updated: " + vo);
            }
        } finally {
            closeStatement(ps);
        }
        return vo.getId();
    }
    
    
    public static long updateReverseWithdraw(Connection con, Transaction vo) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql =
                "UPDATE " +
                "transactions t " +
                "SET " +
                "user_id = ?, " +
                "credit_card_id = ?, " +
                "type_id = ?, " +
                "amount = ?, " +
                "status_id = ?, " +
                "description = ?, " +
                "writer_id = ?, " +
                "ip = ?, " +
                "time_settled = ?, " +
                "comments = ?, " +
                "processed_writer_id = ?, " +
                "reference_id = ?, " +
                "cheque_id = ?, " +
                "wire_id = ?, " +
                "charge_back_id = ?, " +
                "receipt_num = ?, " +
                "auth_number = ?, " +
                "xor_id_authorize = ?, " +
                "utc_offset_settled = ?, " +
                "clearing_provider_id = ?, " +
                "fee_cancel = ?, " +
                "xor_id_capture = ? , " +
                "acquirer_response_id =?, " +
                "is_rerouted = ? , " +
                "rerouting_transaction_id = ? " +
                "WHERE " +
                "id = ? " + 
                " and status_id in (" + TransactionsManagerBase.TRANS_STATUS_REQUESTED + ", " + TransactionsManagerBase.TRANS_STATUS_APPROVED + ", " + TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL + ")";

            ps = con.prepareStatement(sql);
            ps.setLong(1, vo.getUserId());
            if (null == vo.getCreditCardId() || 0 == vo.getCreditCardId().longValue()) {
            	ps.setNull(2, Types.NUMERIC);
            } else {
            	ps.setBigDecimal(2,vo.getCreditCardId());
            }

            ps.setLong(3, vo.getTypeId());
            ps.setLong(4, vo.getAmount());
            ps.setLong(5, vo.getStatusId());
            ps.setString(6, vo.getDescription());
            ps.setLong(7, vo.getWriterId());
            ps.setString(8, vo.getIp());
            ps.setTimestamp(9, convertToTimestamp(vo.getTimeSettled()));
            ps.setString(10, vo.getComments());
            ps.setLong(11, vo.getProcessedWriterId());
            if (null == vo.getReferenceId() || 0 == vo.getReferenceId().longValue()) {
                ps.setNull(12, Types.NUMERIC);
            } else {
                ps.setBigDecimal(12, vo.getReferenceId());
            }
            if (null == vo.getChequeId() || 0 == vo.getChequeId().longValue()) {
                ps.setNull(13, Types.NUMERIC);
            } else {
                ps.setBigDecimal(13, vo.getChequeId());
            }
            if (null == vo.getWireId() || 0 == vo.getWireId().longValue()) {
                ps.setNull(14, Types.NUMERIC);
            } else {
                ps.setBigDecimal(14, vo.getWireId());
            }
            if (null == vo.getChargeBackId() || 0 == vo.getChargeBackId().longValue()) {
                ps.setNull(15, Types.NUMERIC);
            } else {
                ps.setBigDecimal(15, vo.getChargeBackId());
            }
            if (null == vo.getReceiptNum() || 0 == vo.getReceiptNum().longValue()) {
                ps.setNull(16, Types.NUMERIC);
            } else {
                ps.setBigDecimal(16, vo.getReceiptNum());
            }
            ps.setString(17, vo.getAuthNumber());
            ps.setString(18, vo.getXorIdAuthorize());
            ps.setString(19, vo.getUtcOffsetSettled());
            if (0 == vo.getClearingProviderId()) {
            	ps.setNull(20, Types.NUMERIC);
            } else {
            	ps.setLong(20, vo.getClearingProviderId());
            }
            if (vo.isFeeCancel()) {
                ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_YES);
            } else {
                ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_NO);
            }
            ps.setString(22, vo.getXorIdCapture());
            ps.setString(23, vo.getAcquirerResponseId());
            ps.setLong(24, vo.isRerouted()?1:0);
            ps.setLong(25, vo.getReroutingTranId());
            ps.setLong(26, vo.getId());
            int updateCount = ps.executeUpdate();
            if (updateCount == 0) {
				throw new SQLException("No rows updated! Probably the user tries to reverse withdraw twice");
			}
            if (log.isDebugEnabled()) {
            	log.log(Level.DEBUG, "Transaction updated: " + vo);
            }
        } finally {
            closeStatement(ps);
        }
        return vo.getId();
    }

//	/**
//	 * Is first succeed deposit
//	 * @param con
//	 * @param userId
//	 * @return
//	 * @throws SQLException
//	 */
//	public static boolean isFirstDeposit(Connection con,long userId, long expectedValue) throws SQLException {
//
//		  PreparedStatement ps = null;
//		  ResultSet rs = null;
//
//		  try {
//			    String sql = " select count(*) num" +
//			    			 " from transactions t, transaction_types tt " +
//			    			 " where t.type_id = tt.id and t.user_id=? and "+
//			    			 " tt.class_type = ? AND "+
//			    			 " t.status_id in ("+ TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES +") ";
//
//				ps = con.prepareStatement(sql);
//				ps.setLong(1, userId);
//				ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
//
//				rs = ps.executeQuery();
//
//				if (rs.next()) {
//					long num = rs.getLong("num");
//					if (num > expectedValue) {
//						return false;
//					} else {
//						return true;
//					}
//
//				}
//				return false;
//			}
//
//			finally {
//				closeResultSet(rs);
//				closeStatement(ps);
//			}
//	}

    public static long getSummaryByDates(Connection conn, long userId, long classType, Date from, Date to) throws SQLException {
        long total = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "SUM(amount) total " +
                "FROM " +
                    "transactions, " +
                    "transaction_types tt " +
                "WHERE " +
                    "type_id = tt.id AND " +
                    "user_id = ? AND " +
                    "tt.class_type = ? AND " +
                    "status_id IN (" +
                        TransactionsManagerBase.TRANS_STATUS_SUCCEED + "," +
                        TransactionsManagerBase.TRANS_STATUS_PENDING +
                    ") AND " +
                    "trunc(time_created, 'DDD') >= trunc(?, 'DDD') AND " +
                    "trunc(time_created, 'DDD') <= trunc(?, 'DDD')";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, classType);
            pstmt.setTimestamp(3, new Timestamp(from.getTime()));
            pstmt.setTimestamp(4, new Timestamp(to.getTime()));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                total = rs.getLong("total");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return total;

    }

    /**
     * get user transactions by date range and statuses
     * @param con
     * @param id
     * @param from
     * @param to
     * @param status
     * @param internalType
     * @return
     * @throws SQLException
     */
	public static ArrayList<Transaction> getByUserAndDates(Connection con,long id,Date from,Date to,
			String status, int internalType, Integer startRow, Integer pageSize) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<Transaction> list = new ArrayList<Transaction>();
		  try {
			    String sql = "SELECT " +
			    				"* " +
			    			 "FROM " +
				    			 "(SELECT  " +
				    				"X.*, " +
				    				"rownum AS rownumIn " +
				    			 "FROM " +
					    			"(SELECT " +
									    "t.*, " +
						                "ty.description typename, " +
						                "ty.class_type classtype, " +
						                "ts.description statusdesc, " +
						                "u.currency_id currency_id, " +
						                "pt.name paymentMethodName, " +
						                "pt.description directProviderName " +
					                "FROM " +
					                	"transactions t " +
					                	"left join  bonus_users bu on t.bonus_user_id = bu.id " +
					                         "left join bonus_types bt on bt.id = bu.type_id " +
					                    "left join payment_methods pt on t.payment_type_id = pt.id, " +
					                    "transaction_types ty, " +
					                    "transaction_statuses ts, " +
					                    "users u " +
					                 "WHERE " +
					                 	"t.type_id = ty.id and " +
					                 	"t.status_id = ts.id and " +
					                 	"t.user_id = ? and " +
					                 	"t.user_id = u.id and " +
					                 	"t.type_id <> ? and " +
			    						"t.type_id <> ? and ";
					    	if (null != from && null != to) {
							    sql +=  "t.time_created >= ? and " +
							    		"t.time_created <= ? and ";
					    	}
							    sql +=	"t.status_id in (" + status + ") and " +
							    		"(bt.class_type_id is null or bt.class_type_id != ?) " +
							    	"ORDER BY " +
					                 	"t.time_created desc) X) ";

					    if (null != startRow && null != pageSize) {
					    	sql += "WHERE rownumIn > ? " + 
			   						"AND rownumIn <= ? ";
					    }

				ps = con.prepareStatement(sql);
				int idx = 1;
				ps.setLong(idx++, id);
				ps.setLong(idx++, internalType);
				ps.setLong(idx++, TransactionsManagerBase.TRANS_TYPE_CUP_INTERNAL_CREDIT);
				
				if (null != from && null != to) {
					ps.setTimestamp(idx++, convertToTimestamp(from));
					ps.setTimestamp(idx++, convertToTimestamp(to));
				}
				
				ps.setLong(idx++, ConstantsBase.BONUS_CLASS_TYPE_NEXT_INVEST_ON_US);
				
			    if (null != startRow && null != pageSize) {
					ps.setInt(idx++, startRow);
					ps.setInt(idx++, (startRow + pageSize));
			    }
				
				rs = ps.executeQuery();

				while (rs.next()) {
					Transaction t = getVO(con,rs);
					t.setPaymentTypeDesc(rs.getString("directProviderName"));
					t.setPaymentTypeName(rs.getString("paymentMethodName"));
				    list.add(t);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	  }

	/**
	 * Get transaction by status and classType
	 * @param con
	 * @param id
	 * @param status
	 * @param classType
	 * @param order
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Transaction> getByUserAndStatus(Connection con, long id, String status, long classType, String order) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<Transaction> list = new ArrayList<Transaction>();
		  try {
			    String sql =
			    		"SELECT " +
			    			"t.*," +
			    			"p.description typename," +
			    			"p.class_type classtype, " +
			    			"u.currency_id currency_id " +
			    		"FROM " +
			    			"transactions t," +
			    			"transaction_types p, " +
			    			"users u " +
			    		"WHERE " +
			    			"t.user_id = u.id AND " +
			    			"t.user_id=? AND " +
			    		    "t.status_id IN ( " + status + " ) AND " +
			    		    "t.type_id = p.id AND " +
			    		    "p.class_type = ? " +
		    		    "ORDER BY " +
		    		    	"t.time_created  " + order;

				ps = con.prepareStatement(sql);
				ps.setLong(1, id);
				ps.setLong(2, classType);
				rs = ps.executeQuery();

				while (rs.next()) {
					list.add(getVO(con,rs));
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	}

    public static Transaction getById(Connection con, long id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Transaction vo = null;
        try {
            String sql =
                "SELECT " +
                    "t.*, " +
                    "ty.description typename, " +
                    "ty.class_type classtype, " +
                    "u.currency_id currency_id, " +
                    "pt.description directProviderName, " +
                    "cp.id_descriptor clearingDescriptor " +
                "FROM " +
                    "transactions t, " +
                    "transaction_types ty, " +
                    "users u, " +
                    "direct_payment_types pt, " +
                    "clearing_providers cp " +
                "WHERE " +
                    "t.user_id = u.id AND " +
                    "t.type_id = ty.id AND " +
                    "t.payment_type_id = pt.id (+) AND " +
                    "t.clearing_provider_id = cp.id (+) AND " +
                    "t.id = ?";
            ps = con.prepareStatement(sql);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
            	vo = getVO(con, rs);
            	vo.setPaymentTypeName(rs.getString("directProviderName"));
            	vo.setBonusUserId(rs.getLong("bonus_user_id"));
            	if (vo.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT) {
            		 vo.setCc4digit(CreditCardsDAOBase.getById(con, Long.parseLong(String.valueOf(vo.getCreditCardId()))).getCcNumberLast4());
            	}
            	vo.setClearingProviderDescriptor(ClearingManagerBase.getClearingProviderDescriptor(rs.getLong("clearingDescriptor")));
                return vo;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return vo;
    }

    protected static Transaction getVO(Connection con, ResultSet rs) throws SQLException {
        Transaction vo = new Transaction();
        vo.setId(rs.getLong("id"));
        vo.setUserId(rs.getLong("user_id"));
        vo.setCreditCardId(rs.getBigDecimal("credit_card_id"));
        vo.setReferenceId(rs.getBigDecimal("reference_id"));
        vo.setTypeId(rs.getLong("type_id"));
        vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
        vo.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));
        vo.setAmount(rs.getLong("amount"));
        vo.setStatusId(rs.getLong("status_id"));
        vo.setDescription(rs.getString("description"));
        vo.setWriterId(rs.getLong("writer_id"));
        vo.setIp(rs.getString("ip"));
        vo.setComments(rs.getString("comments"));
        vo.setProcessedWriterId(rs.getLong("processed_writer_id"));
        vo.setChequeId(rs.getBigDecimal("cheque_id"));
        vo.setWireId(rs.getBigDecimal("wire_id"));
        vo.setChargeBackId(rs.getBigDecimal("charge_back_id"));
        vo.setReceiptNum(rs.getBigDecimal("receipt_num"));
        vo.setTypeName(rs.getString("typename"));
        vo.setClassType(rs.getLong("classtype"));
        vo.setAuthNumber(rs.getString("auth_number"));
        vo.setXorIdAuthorize(rs.getString("xor_id_authorize"));
        vo.setXorIdCapture(rs.getString("xor_id_capture"));
        vo.setCurrencyId(rs.getLong("currency_id"));
        //vo.setCurrency(CommonUtil.getAppData().getCurrencyById(rs.getInt("currency_id")));
        vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
        vo.setUtcOffsetSettled(rs.getString("utc_offset_settled"));
        vo.setClearingProviderId(rs.getLong("clearing_provider_id"));
        vo.setAcquirerResponseId(rs.getString("acquirer_response_id"));
        vo.setIsRerouted(rs.getLong("is_rerouted") == 1 ? true : false);
        vo.setReroutingTranId(rs.getLong("rerouting_transaction_id"));
        vo.setSelectorId(rs.getInt("selector_id"));
        vo.setRate(rs.getDouble("rate"));
        vo.setIs3D(rs.getBoolean("is_3d"));
        
        return vo;
    }

    /**
     * Get sum deposits of user this is for deposit limitation for anyoption
     * we take only riski transaction type, (is_risky=1)
     *
     * @param con Db connection
     * @param userId user id for taking his deposits
     * @return sum deposits of the user
     * @throws SQLException
     */
    public static long getSumDepositsForLimitation(Connection conn, long userId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        long sumDeposits = 0;
        try {
            String sql =
                "SELECT " +
                    "sum(t.amount) sum_deposits " +
                "FROM " +
                    "transactions t, " +
                    "transaction_types tt " +
                "WHERE " +
                    "t.user_id = ? AND " +
                    "t.type_id = tt.id AND " +
                    "tt.class_type = ? and " +
                    "tt.is_risky = 1 and " +
                    "t.status_id in (" +
                        TransactionsManagerBase.TRANS_STATUS_SUCCEED + "," +
                        TransactionsManagerBase.TRANS_STATUS_PENDING +
                    ")";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                sumDeposits = rs.getLong("sum_deposits");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return sumDeposits;
    }

    public static long getNextReceipt(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "seq_receipt.nextval val " +
                "FROM " +
                    "dual ";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getLong("val");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return 0;
    }

    public static void setTransactionBonuUsersId(Connection conn, long transactionId, long bonusUsersId) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    "UPDATE " +
                        "transactions " +
                    "SET " +
                        "bonus_user_id = ? " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, bonusUsersId);
            pstmt.setLong(2, transactionId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

//    public static boolean isFailureRepeat(Connection conn, long userId, long cardId, long amount , long transactionId, boolean firstDeposit) throws SQLException {
//    	boolean clearingFlag = false;
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        long failsNum = Long.valueOf(CommonUtil.getEnum("failure_transaction_range", "transactions_number")).longValue();
//        try {
//            String sql =
//                "SELECT " +
//                	" * " +
//                "FROM " +
//                	"(SELECT " +
//	                	"t.time_created trx_time_created, " +
//	                	"t.status_id, " +
//	                	"t.clearing_provider_id, " +
//	                	"t.amount, " +
//	                	"cc.time_modified " +
//		              "FROM " +
//		               	"transactions t, " +
//		               	"credit_cards cc " +
//		              "WHERE " +
//		                "t.credit_card_id = cc.id AND " +
//		               	"t.user_id = ? AND " +
//		               	"t.credit_card_id = ? AND " +
//		               	"t.type_id = ? AND " +
//		               	"t.id <> ? AND " + //don't count current transaction with status started
//		               	"t.description not like 'error.min.deposit' AND " +
//		               	"t.time_created >= SYSDATE - " +
//		               		"(SELECT value " +
//		              		  "FROM enumerators " +
//		               		  "WHERE enumerator = 'failure_transaction_range' AND code = 'minutes') " +
//		               		" * 1/24/60 " +
//		                "ORDER BY t.time_created DESC) " +
//		        "WHERE rownum <= ? "; //we always look at the last X trx" +
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, userId);
//            pstmt.setLong(2, cardId);
//            pstmt.setLong(3, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
//            pstmt.setLong(4, transactionId);
//            pstmt.setLong(5, failsNum);
//            rs = pstmt.executeQuery();
//            long minAmount = 0;
//            int counter = 0;
//            long clearing_provider_id = -1;
//            long status_id = 0;
//            long currAmount = 0;
//            while (rs.next()) {
//            	if (clearing_provider_id != 0 &&
//            			clearing_provider_id != -1 &&
//            			rs.getLong("clearing_provider_id") == 0){ // identifying request to provider to reset counting
//            		clearingFlag = true;
//            	}
//            	clearing_provider_id = (rs.getLong("clearing_provider_id"));
//            	status_id = rs.getLong("status_id");
//
//                if (status_id != TransactionsManagerBase.TRANS_STATUS_FAILED){
//                	break;
//                }
//
//                ++counter;
//                currAmount = rs.getLong("amount");
//                if (1 == counter) {
//                	Date ccTimeModified = convertToDate(rs.getTimestamp("time_modified"));
//                	Date trx_time_created = convertToDate(rs.getTimestamp("trx_time_created"));
//                	if (ccTimeModified.after(trx_time_created) && !firstDeposit){ //if cc modified after trx was created
//                		return false;
//                	}
//
//                	minAmount = currAmount;
//                } else if (currAmount < minAmount) {
//                		minAmount = currAmount;
//                }
//            }
//
//            if (counter >= failsNum && clearingFlag == true){ // if we identified request to provider we want to reset the counter
//            	return false;
//            }
//
//            if (counter >= failsNum && minAmount <= amount) {
//            	return true;
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return false;
//    }

    /**
     * Get last failed deposit
     * @param con
     * @param userId
     * @return
     * @throws SQLException
     */
    public static Transaction getLastFailedDeposit(Connection con, long userId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Transaction vo = null;
        try {
            String sql =
            	"SELECT " +
		        	"t.id, " +
		        	"t.amount " +
		        "FROM " +
		            "transactions t, " +
		            "transaction_types tt " +
		        "WHERE " +
		            "t.user_id = ? AND " +
		            "t.type_id = tt.id AND " +
		            "tt.class_type = ? AND " +
		            "t.status_id = ? " +
		        "ORDER BY " +
		                "t.time_created DESC " ;
            ps = con.prepareStatement(sql);
            ps.setLong(1, userId);
            ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
            ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_FAILED);
            rs = ps.executeQuery();
            if (rs.next()) {
            	vo = new Transaction();
            	vo.setId(rs.getLong("id"));
            	vo.setAmount(rs.getLong("amount"));
                return vo;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return vo;
    }

    /**
     * Get avg of deposits, used for population type noXdeposits.
     * @param con
     * @param userId
     * @return
     * @throws SQLException
     */
    public static long getAVGSucceedDeposit(Connection con, long userId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        long avgDeposits = 0;
        try {
            String sql =
            	" SELECT " +
		        	" SUM(t.amount) AS deposits, " +
		        	" COUNT(t.id) AS num_of_deposits " +
		        " FROM " +
		            " transactions t, " +
		            " transaction_types tt " +
		        " WHERE " +
		            " t.user_id = ? AND " +
		            " t.type_id = tt.id AND " +
		            " tt.class_type = ? AND " +
		            " t.status_id in ( " + TransactionsManagerBase.TRANS_STATUS_SUCCEED + ", " + TransactionsManagerBase.TRANS_STATUS_PENDING + " ) ";

            ps = con.prepareStatement(sql);
            ps.setLong(1, userId);
            ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
            rs = ps.executeQuery();
            if (rs.next()) {
            	avgDeposits = rs.getLong("deposits") / rs.getLong("num_of_deposits");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return avgDeposits;
    }

  //a new insert method is added to allow inserting transaction from the service (since the service is running under Jboss)
	public static void insertFromService(Connection con, Transaction vo) throws SQLException {
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  try {
		  	String rate = "trunc(convert_amount_to_usd(1,(select currency_id from users where id=?),sysdate),"+ ConstantsBase.RATE_PRECISISON +")";
			String sql="insert into transactions t(id,user_id,credit_card_id,type_id," +
					"amount,status_id,description,writer_id,ip,time_settled,comments,processed_writer_id," +
					"time_created,reference_id,cheque_id,wire_id,charge_back_id,receipt_num,fee_cancel," +
					"utc_offset_created, utc_offset_settled, bonus_user_id, rate, acquirer_response_id, login_id) " +
										"values(seq_transactions.nextval,?,?,?,?,?,?,?,?,?,?,?,sysdate,?,?,?,?,?,?,?,?,?,"+rate+", ?, ?)";

			ps = (PreparedStatement)con.prepareStatement(sql);
			ps.setLong(1, vo.getUserId());
			ps.setBigDecimal(2,vo.getCreditCardId());
			ps.setLong(3, vo.getTypeId());
			ps.setLong(4, vo.getAmount());
			ps.setLong(5, vo.getStatusId());
			ps.setString(6, vo.getDescription());  //In Orace this field is of type NCHAR hence will have problems inserting hebrew
			ps.setLong(7, vo.getWriterId());
			ps.setString(8, vo.getIp());
			ps.setTimestamp(9, CommonUtil.convertToTimeStamp(vo.getTimeSettled()));
			ps.setString(10, vo.getComments());	//In Orace this field is of type NCHAR hence will have problems inserting hebrew
			ps.setLong(11, vo.getProcessedWriterId());
			ps.setBigDecimal(12,vo.getReferenceId());
			ps.setBigDecimal(13,vo.getChequeId());
			ps.setBigDecimal(14,vo.getWireId());
			ps.setBigDecimal(15,vo.getChargeBackId());
			ps.setBigDecimal(16,vo.getReceiptNum());
			if (vo.isFeeCancel()) {
				ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_YES);
			}
			else {
				ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_NO);
			}
			ps.setString(18, vo.getUtcOffsetCreated());
			ps.setString(19, vo.getUtcOffsetSettled());
            if (vo.getBonusUserId() > 0) {
                ps.setLong(20, vo.getBonusUserId());
            } else {
                ps.setNull(20, Types.NUMERIC);
            }
			ps.setLong(21, vo.getUserId());
			ps.setString(22, vo.getAcquirerResponseId());
            if (vo.getLoginId() > 0) {
                ps.setLong(23, vo.getLoginId());
            } else {
                ps.setNull(23, Types.NUMERIC);
            }
			ps.executeUpdate();

			vo.setId(getSeqCurValue(con,"seq_transactions"));

			if (log.isDebugEnabled()) {
	            String ls = System.getProperty("line.separator");
	            log.log(Level.DEBUG, ls + "Transaction started:" + vo.toString());
			}
	  }
		finally
		{
			closeStatement(ps);
			closeResultSet(rs);
		}

  }

    /**
     * Check paypal email
     *
     * @param con
     * @param userId
     * @param paypalEmail
     * @return boolean - is valid paypal email
     * @throws SQLException
     */
    public static boolean isPaypalEmailValid(Connection con, long userId, String paypalEmail) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                " SELECT " +
                    " * " +
                " FROM " +
                    " transactions t " +
                " WHERE " +
                    " t.user_id = ? " +
                    " AND t.paypal_email = ? " +
                    " AND t.type_id = " + TransactionsManagerBase.TRANS_TYPE_PAYPAL_DEPOSIT + " " +
                    " AND t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ")";
            ps = con.prepareStatement(sql);
            ps.setLong(1, userId);
            ps.setString(2, paypalEmail);
            rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        } finally {
            closeStatement(ps);
        }
        return false;
    }

    /**
     * Check Moneybookers email
     *
     * @param con
     * @param userId
     * @param moneybookersEmail
     * @return boolean - is valid moneybookersEmail
     * @throws SQLException
     */
    public static boolean isMoneybookersEmailValid(Connection con, long userId, String moneybookersEmail) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                " SELECT " +
                    " * " +
                " FROM " +
                    " transactions t " +
                " WHERE " +
                    " t.user_id = ? " +
                    " AND t.MONEYBOOKERS_EMAIL = ? " +
                    " AND t.type_id = " + TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_DEPOSIT + " " +
                    " AND t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ")";
            ps = con.prepareStatement(sql);
            ps.setLong(1, userId);
            ps.setString(2, moneybookersEmail);
            rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        } finally {
            closeStatement(ps);
        }
        return false;
    }
	
	public static long getSumOfDepositsForRank(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long amount = 0;

		try {
			String sql = 
					" SELECT " +
					"		sum((t.amount/100) * t.rate) as userAmount " +
					" FROM " +
					"		transactions t, transaction_types tt " +
					" WHERE " +
					"		t.user_id = ? " +
					"		AND t.type_id = tt.id " +
					"		AND tt.class_type = ? " +
					"		AND t.status_id in (?,?,?)";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT);
			ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
			ps.setLong(4, TransactionsManagerBase.TRANS_STATUS_PENDING);
			ps.setLong(5, TransactionsManagerBase.TRANS_STATUS_CANCELED_ETRADER);
			rs = ps.executeQuery();

			if (rs.next()) {
				amount = rs.getLong("userAmount");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return amount;
	}
	

	public static long getConsecutiveFailuresByBin(Connection conn , ClearingRoute cr, long bin) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " SELECT count( distinct( cc.cc_number ) ) as num_of_failures " +
					 " FROM transactions t , credit_cards cc " +
					 " WHERE " +
					 "		t.credit_card_id = cc.id " +
					 " AND  t.type_id = ? " +
					 " AND t.status_id = ? " +
					 " AND t.time_created >=  ? " +
					 " AND cc.bin = ? ";
		try{
			ps = conn.prepareStatement(sql);
			ps.setLong(1, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
			ps.setLong(2, TransactionsManagerBase.TRANS_STATUS_FAILED);
			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(cr.getTimelastSuccess()));
			ps.setLong(4, bin);
			
			rs = ps.executeQuery();
			if(rs.next()){
				return rs.getLong("num_of_failures");
			}
			
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return 0;
	}

	public static void updateTimeLastSuccess(Connection con, TransactionBin vo) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " UPDATE " +
				 "		transactions_bins " +
				 " SET " +
				 "		TIME_LAST_SUCCESS = sysdate " +
				 " WHERE " +
				 "		id = ? ";
		try {
			ps = con.prepareStatement(sql);
			ps.setLong(1, vo.getId());
			ps.execute();
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	public static void updateTimeLastRerouteFailed(Connection con, TransactionBin vo) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " UPDATE " +
				 "		transactions_bins " +
				 " SET " +
				 "		time_last_failed_reroute = sysdate " +
				 " WHERE " +
				 "		id = ? ";
		try {
			ps = con.prepareStatement(sql);
			ps.setLong(1, vo.getId());
			ps.execute();
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	public static String velocityCheck(Connection con, long userID) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String resault = null;
		long velocityResault = 0;
		try {
		    String sql = "SELECT " +
							" TRANSACTIONS_MANAGER.VELOCITY_CHECK(?) as RES" +
						" FROM " +
							" DUAL ";
	
			ps = con.prepareStatement(sql);
			ps.setLong(1, userID);
			rs = ps.executeQuery();
			if (rs.next()) {   // user have monthly withdrawals
				velocityResault = rs.getLong("RES");
			}
			
			if(velocityResault == 1) {
				resault = ConstantsBase.ERROR_DEPOSIT_VELOCITY_AO_NO_DOC;
			} else if(velocityResault == 2) {
				resault = ConstantsBase.ERROR_DEPOSIT_VELOCITY_AO_HAVE_DOC;			
			} else if(velocityResault == 3) {
				resault = ConstantsBase.ERROR_DEPOSIT_VELOCITY_ET_NO_DOC;
			} else if(velocityResault == 4) {
				resault = ConstantsBase.ERROR_DEPOSIT_VELOCITY_ET_HAVE_DOC;
			} else {
				resault = null;
			}				
			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return resault;
	}

	public static long getCUPWithdrawalAmount(Connection con, long userId, boolean countBeforeCup) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			
			try {
				String sql = 
						
					" SELECT " +
							" nvl( deposit_amount - withdraw_amount,0) as available_to_withdraw " + 
					" FROM " +
					" (" +
							" SELECT " +
									" nvl(sum(t.amount - t.credit_amount),0) as deposit_amount " + 
							" FROM " +
									" transactions t, clearing_providers cp " + 
							" WHERE " +
									" t.user_id = ? " + 
							" AND " +
									" (cp.id = t.clearing_provider_id and cp.is_active = 1 ) " +
							" AND " +
									" t.type_id = 38 " + // CUP_DEPOSIT
							" AND " +
									" t.status_id in  ( 2,7,8 ) " + 
							" AND " +
									" t.selector_id > ?" +
							" AND " + 
						            " (t.amount - t.credit_amount) > 0 " + 
					" ), " + 
					" ( " +
							" SELECT" + 
									" NVL(SUM (t.amount - t.internals_amount),0)  AS WITHDRAW_AMOUNT" + 
							" FROM " +
									" transactions t " + 
							" WHERE " +
									" t.user_id = ? " +
							" AND " + 
									" t.type_id = 39 " + // CUP_WITHDRAW
							" AND " +
									" (t.status_id = 4 or t.status_id = 9) " + 
							" AND " +
									" t.selector_id > ? " +           
					")"; 	
	
				ps = con.prepareStatement(sql);
	
				ps.setLong(1, userId);
				if(countBeforeCup) { // selector_id = -1 are transactions before CUP
					ps.setLong(2, -2); // include transactions before CUP
				} else { 
					ps.setLong(2, -1); // exclude transactions before CUP
				}
				ps.setLong(3, userId);
				if(countBeforeCup) {
					ps.setLong(4, -2);
				} else { 
					ps.setLong(4, -1);
				};
				
	
				rs = ps.executeQuery();
	
				if(rs.next()) {
					return rs.getLong("available_to_withdraw");
				}
	
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return 0;
		}
	
	public static long getFirstTransactionId(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String sql = 
						" SELECT " +
							" u.first_deposit_id " +
						" FROM " +
							" users u " +
						" WHERE " +
							" u.id = ? ";
				           
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("first_deposit_id");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;	
	}

	public static boolean isFirstDepositWithCC(Connection con,long userId, long creditCardId) throws SQLException {
	
		  PreparedStatement ps = null;
		  ResultSet rs = null;
	
		  try {
			    String sql =" SELECT " +
			    					"COUNT(*) num" +
			    			" FROM " +
			    			 		" transactions t, transaction_types tt " +
			    			" WHERE " +
			    			 		" t.type_id = tt.id and t.user_id = ?" +
	  			 		" AND " +
			    			 		" tt.class_type = ?" +
			    			" AND " +
			    			 		" t.credit_card_id = ? " +
			    			" AND " +
			    			 		" t.status_id in ("+ TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES +") ";
	
				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
				ps.setLong(3, creditCardId);
	
				rs = ps.executeQuery();
	
				if (rs.next()) {
					long num = rs.getLong("num");
					return num == 1;
				}
	
				return true;
			}
	
			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
	}

	/**
	 * Method that returns the last user deposit
	 * 
	 * @param con
	 * @param userId
	 * @return the last user deposit
	 * @throws SQLException
	 */
	public static Transaction getLastUserDeposit(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " SELECT " +
							" t.* " +
		    				" ,tt.description typename " +
		    				" ,tt.class_type classtype" +
		    				" ,u.currency_id currency_id " +
		    			 " FROM " +
			    			 " transactions t, " +
			    			 " transaction_types tt, " +
			    			 " users u " +
		    			 "WHERE " +
			    			 " t.type_id = tt.id and t.user_id = ?  " +
			    			 " AND tt.class_type = ? " +
			    			 " AND u.id = t.user_id " +
			    			 " AND t.time_created >= sysdate - 2 "+
			    			 " AND t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
		    			 " ORDER BY t.id desc";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
			rs = ps.executeQuery();
			if (rs.next()) {
				return getVO(con, rs);
			} else {
				log.warn("No transaction found returning empty transaction");
				return new Transaction();
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	
	public static Transaction getLastUserDepositEver(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " SELECT " +
							" t.* " +
		    				" ,tt.description typename " +
		    				" ,tt.class_type classtype" +
		    				" ,u.currency_id currency_id " +
		    			 " FROM " +
			    			 " transactions t, " +
			    			 " transaction_types tt, " +
			    			 " users u " +
		    			 "WHERE " +
			    			 " t.type_id = tt.id and t.user_id = ?  " +
			    			 " AND tt.class_type = 1 " + // CLASS_DEPOSIT
			    			 " AND u.id = t.user_id " +
			    			 " AND t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
		    			 " ORDER BY t.id desc";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);

			rs = ps.executeQuery();
			if (rs.next()) {
				return getVO(con, rs);
			} else {
				return null;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}

	public static boolean isInatecNotNotifiedTrx(Connection con, long id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql =
				"SELECT " +
					"id " +
				"FROM " +
					"transactions " +
				"WHERE " +
					"type_id = ? AND " +
					"status_id = ? AND " +
					"id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT);
			ps.setLong(2, TransactionsManagerBase.TRANS_STATUS_AUTHENTICATE);
			ps.setLong(3, id);
			rs = ps.executeQuery();

			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}

	public static void updatePurchase(Connection conn, long transactionId, long statusId, String description, String comments,
										String purchaseAuthNumber, String utcOffset, long processedWriterId) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String sql =
	                "UPDATE " +
	                    "transactions " +
	                "SET " +
	                    "status_id = ?, " +
	                    "description = ?, " +
	                    "comments = comments || ?, " +
	                    "xor_id_capture = ? ";

			if (statusId == TransactionsManagerBase.TRANS_STATUS_SUCCEED || statusId == TransactionsManagerBase.TRANS_STATUS_FAILED) {
				sql += ",time_settled = sysdate, " +
                 	   "utc_offset_settled = ?, " +
                 	   "processed_writer_id = ? ";
			}
			sql += "WHERE " +
	                    "id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, statusId);
			pstmt.setString(2, description);
			pstmt.setString(3, comments);
			pstmt.setString(4, purchaseAuthNumber);
			if (statusId == TransactionsManagerBase.TRANS_STATUS_SUCCEED || statusId == TransactionsManagerBase.TRANS_STATUS_FAILED) {
				pstmt.setString(5, utcOffset);
				pstmt.setLong(6, processedWriterId);
				pstmt.setLong(7, transactionId);
			} else {
				pstmt.setLong(5, transactionId);
			}
			pstmt.executeUpdate();
		} finally {
			closeStatement(pstmt);
		}
	}
	
	/**
	 * @param conn
	 * @param fd
	 * @throws SQLException
	 */
	public static void updateOtherPayment(Connection conn, FinalizeDeposit fd) throws SQLException {
		PreparedStatement pstmt = null;
		int index = 1;
		try {
			String sql =
					"UPDATE " +
					"	transactions " +
					"SET " +
					"	status_id = ? " +
					"	,description = ? " +
					"	,comments = comments || ? " +
					"	,auth_number = ? " +
					"	,xor_id_capture = ? " +	
					"	,xor_id_authorize = ? " +
					"	,time_settled = sysdate " +
					"	,utc_offset_settled = ? " +
					"	,processed_writer_id = ? " +
					"WHERE " +
					"	id = ? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(index++, fd.getTransaction().getStatusId());
			pstmt.setString(index++, fd.getTransaction().getDescription());
			pstmt.setString(index++, fd.getTransaction().getComments());
			pstmt.setString(index++, fd.getTransaction().getAuthNumber());
			pstmt.setString(index++, fd.getTransaction().getXorIdCapture());
			pstmt.setString(index++, fd.getTransaction().getXorIdAuthorize());
			pstmt.setString(index++, fd.getUtcOffset());
			pstmt.setInt(index++, fd.getWriterId());
			pstmt.setLong(index++, fd.getTransaction().getId());
			pstmt.executeUpdate();
		} finally {
			closeStatement(pstmt);
		}
	}

	  /**
	 * @param con
	 * @param vo
	 * @return
	 * @throws SQLException
	 */
	public static boolean updateDeltaPayTransactionStatus(Connection con, Transaction vo) throws SQLException {
		  PreparedStatement ps = null;
		  String sql =
                "UPDATE " +
                    "transactions " +
                "SET " +
                	  "time_settled = ?, " + 			// 1
                	  "utc_offset_settled = ?, " +
                	  "status_id = ?, " +
                	  "comments = ?, " +
                	  "description = ?, " +				// 5
                	  "auth_number = ?, " +
                	  "xor_id_capture=? " +
                "WHERE " +
                    "id = ? " +
                    "AND status_id <> ?";
		  
		  try {
			  ps = con.prepareStatement(sql);
	          ps.setTimestamp(1, CommonUtil.convertToTimeStamp(vo.getTimeSettled()));
	          ps.setString(2, vo.getUtcOffsetSettled());
	          ps.setLong(3, vo.getStatusId());
	          ps.setString(4, vo.getComments());
	          ps.setString(5, vo.getDescription());
	          ps.setString(6, vo.getAuthNumber());
	          ps.setString(7, vo.getXorIdCapture());
	          ps.setLong(8, vo.getId());
	          ps.setLong(9, vo.getStatusId());
	          
	          int rowsUpdated = ps.executeUpdate();
	          if (log.isDebugEnabled()) {
	              String ls = System.getProperty("line.separator");
	              log.log(Level.DEBUG, ls + "Transaction updated:" + vo.toString());
	          }
	          
	          return rowsUpdated == 1;
		  } finally {
			  closeStatement(ps);
		  }
	  }
	
	public static void updateRefId(Connection con, long tranId, long refId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "update transactions set REFERENCE_ID=? where id=? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, refId);
			ps.setLong(2, tranId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

  public static ArrayList<Transaction> getAll(Connection con) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<Transaction> list=new ArrayList<Transaction>();

		  try
			{
			    String sql="select t.*,ty.description typename,ty.class_type classtype, u.currency_id currency_id from transactions t,transaction_types ty, users u " +
			    		" where t.user_id=u.id and t.type_id=ty.id order by t.time_created desc";
				ps = con.prepareStatement(sql);
				rs=ps.executeQuery();
				while (rs.next()) {
					Transaction t=getVO(con,rs);
					    list.add(t);
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;

	  }
  
  public static long getSummaryByDates(Connection con,long userId,long classType,String status,
		  Date from,Date to) throws SQLException {

	  PreparedStatement ps = null;
	  ResultSet rs = null;

	  try {
		    String sql = "select sum(amount) total " +
		    			  "from transactions , transaction_types tt " +
		    		     " where type_id = tt.id and user_id = ? and tt.class_type = ? and status_id in ("+status+") and " +
		    		     "trunc(time_created,'DDD')>=trunc(?,'DDD') and " +
		    		     " trunc(time_created,'DDD')<=trunc(?,'DDD') ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, classType);
			ps.setTimestamp(3, new Timestamp(from.getTime()));
			ps.setTimestamp(4, new Timestamp(to.getTime()));

			rs=ps.executeQuery();
			if (rs.next()) {
				  return rs.getLong("total");
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;

  }
  
  public static String getTransactionCreditCardCVV(Connection conn, long transactionId) throws SQLException {
      String cvv = null;
      PreparedStatement pstmt = null;
      ResultSet rs = null;
      try {
          String sql =
              "SELECT " +
                  "B.cc_pass " +
              "FROM " +
                  "transactions A, " +
                  "credit_cards B " +
              "WHERE " +
                  "A.credit_card_id = B.id AND " +
                  "A.id = ?";
          pstmt = conn.prepareStatement(sql);
          pstmt.setLong(1, transactionId);
          rs = pstmt.executeQuery();
          if (rs.next()) {
              cvv = rs.getString("cc_pass");
          }
      } finally {
          closeResultSet(rs);
          closeStatement(pstmt);
      }
      return cvv;
  }
  
  public static void updateComments(Connection conn, long transactionId, String comments) throws SQLException {
      PreparedStatement pstmt = null;
      try {
          String sql =
              "UPDATE " +
                  "transactions " +
              "SET " +
                  "comments = comments || ? " +
              "WHERE " +
                  "id = ?";
          pstmt = conn.prepareStatement(sql);
          pstmt.setString(1, comments);
      	pstmt.setLong(2, transactionId);
          pstmt.executeUpdate();
      } finally {
          closeStatement(pstmt);
      }
  }
 
  /**
   * Get deposit transactions for credit withdrawal
   * @param con db connection
   * @param userId userId that request the withdrawal
   * @param ccId credit card id
   * @return  HashMap  <trxId, creditAmount>
   * @throws SQLException
   */
  public static ArrayList<Transaction> getDepositTrxForCredit(Connection con, long userId, long ccId, String clearingProviderId, boolean isNot) throws SQLException {

  	PreparedStatement ps = null;
      ResultSet rs = null;
      ArrayList<Transaction> list = new ArrayList<Transaction>();

      try {
      	  String sql = " SELECT " +
      	  					" t.id, " +
      	  					" t.time_settled, " +
      	  					" xor_id_capture, " +
      	  					" auth_number, " +
      	  					" capture_number, " +
      	  					" clearing_provider_id, " +
      	  					" (t.amount - t.credit_amount) as credit_amount, " +
      	  					" cc.CC_NUMBER," +
      	  					" t.credit_card_id," +
      	  					" cc.type_id," +
      	  					" t.acquirer_response_id" +
      	  			   " FROM " +
      	  			   		" transactions t left join clearing_providers cp on t.clearing_provider_id = cp.id, " +
      	  			   		" CREDIT_CARDS cc " +
      	  			   " WHERE " +
      	  			   		" t.user_id = ? " +
      	  			   		" and cc.id = t.credit_card_id " +
      	  			   		" and t.credit_card_id = ? " +
      	  			   		" and t.type_id = ? " +
      	  			   		" and t.status_id = ? " +
      	  			   		" and (t.amount - t.credit_amount) > 0 " +
      	  			   		" and cp.payment_group_id <> " + TransactionsManagerBase.PAYMENT_GROUP_EPG + " ";

      	  				if (null != clearingProviderId && clearingProviderId.length() > 0 && !isNot) {
      	  					sql += "AND clearing_provider_id in (" + clearingProviderId + ") ";
      	  				} else if(null != clearingProviderId && clearingProviderId.length() > 0 && isNot){
      	  					sql += "AND clearing_provider_id not in (" + clearingProviderId + ") ";
      	  				}
      	  			    sql +=
      	  			    	"ORDER BY " +
      	  			    		"credit_amount";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setLong(2, ccId);
				ps.setLong(3, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
				ps.setLong(4, TransactionsManagerBase.TRANS_STATUS_SUCCEED);

				rs = ps.executeQuery();

				while (rs.next()) {
					Transaction t = new Transaction();
					t.setId(rs.getLong("id"));
					t.setXorIdCapture(rs.getString("xor_id_capture"));
					t.setAuthNumber(rs.getString("auth_number"));
					t.setCaptureNumber(rs.getString("capture_number"));
					t.setCreditAmount(rs.getLong("credit_amount"));
					t.setClearingProviderId(rs.getLong("clearing_provider_id"));
					t.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));
					t.setAcquirerResponseId(rs.getString("acquirer_response_id"));
					if (t.getClearingProviderId() == ClearingManagerConstants.XOR_AO_PROVIDER_ID) {
						/* && cc in AMEX */
						long ccNumber = 0;
						try {
							ccNumber = Long.valueOf(AESUtil.decrypt(rs.getString("CC_NUMBER")));
						} catch (Exception e) {
							log.error(" Problem in decrypt cc number, cc id: " + rs.getLong("credit_card_id"));
						}

						if (CreditCardsDAOBase.isCcInBinsList(con, ccNumber) ||	ccNumber == 0){
							continue;
						}
					}
					list.add(t);
				}
      } finally {
          closeResultSet(rs);
          closeStatement(ps);
      }
      return list;
  }

  /**
   * Get spesific deposit transactions for credit withdrawal
   * @param con db connection
   * @param trxId deposit transaction id
   * @return Transaction instance
   * @throws SQLException
   */
  public static Transaction getDepositTrxForCreditById(Connection con, long trxId) throws SQLException {

  	PreparedStatement ps = null;
      ResultSet rs = null;
      Transaction t = new Transaction();

      try {
      	  String sql = "select t.id, t.xor_id_capture, t.auth_number, t.capture_number, t.clearing_provider_id, t.acquirer_response_id, (t.amount - t.credit_amount) as credit_amount " +
      	  			   "from transactions t " +
      	  			   "where t.id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, trxId);

				rs = ps.executeQuery();

				if (rs.next()) {
					t.setId(rs.getLong("id"));
					t.setXorIdCapture(rs.getString("xor_id_capture"));
					t.setAuthNumber(rs.getString("auth_number"));
					t.setCaptureNumber(rs.getString("capture_number"));
					t.setCreditAmount(rs.getLong("credit_amount"));
					t.setClearingProviderId(rs.getLong("clearing_provider_id"));
					t.setAcquirerResponseId(rs.getString("acquirer_response_id"));
				}
      } finally {
          closeResultSet(rs);
          closeStatement(ps);
      }
      return t;
  }


	public static void updateDepositTrxCredit(Connection con, long tranId, long refId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "update transactions set CREDIT_AMOUNT=? " +
						 "where id =? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, refId);
			ps.setLong(2, tranId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

    public static Transaction getTransactionByEpacsRef(Connection con, String epacsReference) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Transaction vo = null;
        try {
            String sql =
                "SELECT " +
                    "t.*, " +
                    "ty.description typename, " +
                    "ty.class_type classtype, " +
                    "u.currency_id currency_id " +
                "FROM " +
                    "transactions t, " +
                    "transaction_types ty, " +
                    "users u " +
                "WHERE " +
                    "t.user_id = u.id AND " +
                    "t.type_id = ty.id AND " +
                    "t.auth_number = ? ";
            ps = con.prepareStatement(sql);
            ps.setString(1, epacsReference);
            rs = ps.executeQuery();
            if (rs.next()) {
            	vo = getVO(con, rs);
                return vo;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return vo;
    }

    /**
     * Get transaction amount by transaction id
     * @param con
     * @param trxId
     * @return
     * @throws SQLException
     */
    public static long getAmountById(Connection con, long trxId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        long amount = 0;
        try {
            String sql =
            	"SELECT " +
            		"amount " +
            	"FROM " +
            		"transactions " +
            	"WHERE " +
            		"id = ? ";
            ps = con.prepareStatement(sql);
            ps.setLong(1, trxId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	amount = rs.getLong("amount");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return amount;
    }

	public static void updateTransactionChargeBack(Connection con, long tranId, long cbId, boolean isUpdateStatus) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " update " +
							" transactions " +
						 " set " +
						 	" charge_back_id = ? ";
			if (isUpdateStatus){
				sql +=   	" ,status_id = " + TransactionsManagerBase.TRANS_STATUS_CHARGE_BACK + " ";
			}
			sql +=		 " where id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, cbId);
			ps.setLong(2, tranId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

    public static double getRateById(Connection con, long trxId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
            	"SELECT " +
            		"rate " +
            	"FROM " +
            		"transactions " +
            	"WHERE " +
            		"id = ? ";
            ps = con.prepareStatement(sql);
            ps.setLong(1, trxId);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getDouble("rate");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return 0;
    }
    public static boolean isFailureRepeat(Connection conn, long userId, BigDecimal cardId, long amount , long transactionId, boolean firstDeposit) throws SQLException {
    	boolean clearingFlag = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        long failsNum = Long.valueOf(CommonUtil.getEnum("failure_transaction_range", "transactions_number")).longValue();
        try {
            String sql =
                "SELECT " +
                	" * " +
                "FROM " +
                	"(SELECT " +
	                	"t.time_created trx_time_created, " +
	                	"t.status_id, " +
	                	"t.clearing_provider_id, " +
	                	"t.amount, " +
	                	"cc.time_modified " +
		              "FROM " +
		               	"transactions t, " +
		               	"credit_cards cc " +
		              "WHERE " +
		                "t.credit_card_id = cc.id AND " +
		               	"t.user_id = ? AND " +
		               	"t.credit_card_id = ? AND " +
		               	"t.type_id = ? AND " +
		               	"t.id <> ? AND " + //don't count current transaction with status started
		               	"t.description not like 'error.min.deposit' AND " +
		               	"t.time_created >= SYSDATE - " +
		               		"(SELECT value " +
		              		  "FROM enumerators " +
		               		  "WHERE enumerator = 'failure_transaction_range' AND code = 'minutes') " +
		               		" * 1/24/60 " +
		                "ORDER BY t.time_created DESC) " +
		        "WHERE rownum <= ? "; //we always look at the last X trx" +
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setBigDecimal(2, cardId);
            pstmt.setLong(3, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
            pstmt.setLong(4, transactionId);
            pstmt.setLong(5, failsNum);
            rs = pstmt.executeQuery();
            long minAmount = 0;
            int counter = 0;
            long clearing_provider_id = -1;
            long status_id = 0;
            long currAmount = 0;
            while (rs.next()) {
            	if (clearing_provider_id != 0 &&
            			clearing_provider_id != -1 &&
            			rs.getLong("clearing_provider_id") == 0){ // identifying request to provider to reset counting
            		clearingFlag = true;
            	}
            	clearing_provider_id = (rs.getLong("clearing_provider_id"));
            	status_id = rs.getLong("status_id");

                if (status_id != TransactionsManagerBase.TRANS_STATUS_FAILED){
                	break;
                }

                ++counter;
                currAmount = rs.getLong("amount");
                if (1 == counter) {
                	Date ccTimeModified = convertToDate(rs.getTimestamp("time_modified"));
                	Date trx_time_created = convertToDate(rs.getTimestamp("trx_time_created"));
                	if (ccTimeModified.after(trx_time_created) && !firstDeposit){ //if cc modified after trx was created
                		return false;
                	}

                	minAmount = currAmount;
                } else if (currAmount < minAmount) {
                		minAmount = currAmount;
                }
            }

            if (counter >= failsNum && clearingFlag == true){ // if we identified request to provider we want to reset the counter
            	return false;
            }

            if (counter >= failsNum && minAmount <= amount) {
            	return true;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return false;
    }

	/**
	 * Get direct deposit payment type id by name
	 * @param conn db connection
	 * @param paymentMethodName
	 * @return PaymentMethod Id
	 * @throws SQLException
	 */
    public static long getPaymentMethodId(Connection conn, String paymentMethodName) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =
                " SELECT " +
                    " id " +
                " FROM " +
                    " payment_methods " +
                " WHERE " +
                	" name like ? " +
                " ORDER BY " +
                	" id ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, paymentMethodName);
            rs = pstmt.executeQuery();

            if (rs.next()) {
            	return rs.getLong("id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return 0;
    }

    public static long insertNewPaymentMethod(Connection con,String paymentMethodName, boolean isDeposit) throws SQLException{
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
				String sql=	" insert into payment_methods(ID,TYPE_ID,CODE,DESCRIPTION,NAME,TIME_CREATED) " +
							" values(SEQ_P_METHODS.nextval,?,?,?,?,sysdate)";

				ps = con.prepareStatement(sql);

				ps.setInt(1, isDeposit?1:0);
				ps.setString(2, "PAYMENT_TYPE_" + paymentMethodName.toUpperCase().replace(' ', '_'));
				ps.setString(3, "payment.type." + paymentMethodName.toLowerCase().replace(' ', '.'));
				ps.setString(4, paymentMethodName);

				ps.executeUpdate();

				return getSeqCurValue(con,"SEQ_P_METHODS");

		  }	finally {
				closeStatement(ps);
				closeResultSet(rs);
		  }


	  }

    public static void updateAfterReverse(Connection conn, long transactionId, long statusId, long writerId, String utcOffset) throws SQLException {
        PreparedStatement pstmt = null;

        try {
            String sql =
                "UPDATE " +
                    "transactions " +
                "SET " +
                    "status_id = ?, " +
            		"time_settled = sysdate, " +
                    "utc_offset_settled = ?, " +
                    "processed_writer_id = ? " +
            	"WHERE " +
                   	"id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, statusId);
            pstmt.setString(2, utcOffset);
            pstmt.setLong(3, writerId);
            pstmt.setLong(4, transactionId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Get first transaction withdrawal by user id and credit card id
     * @param con
     * @param userId
     * @param ccId
     * @return
     * @throws SQLException
     */
    public static Transaction getFirstWithdrawal(Connection con, long userId, long ccId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Transaction t = null;

        try {
            String sql =
            	"SELECT " +
            		"t.amount, " +
            		"t.time_created, " +
            		"t.utc_offset_created, " +
            	    "c.cc_number, " +
            	    "c.holder_name, " +
            	    "ct.name " +
            	"FROM " +
            		"transactions t, " +
                    "credit_cards c, " +
                    "credit_card_types ct " +
            	"WHERE " +
        	        "t.user_id = ? AND " +
        	        "t.type_id = ?  AND " +
        	        "t.status_id in (?,?,?) AND " +
        	        "t.credit_card_id = ? AND " +
        	        "t.credit_card_id = c.id AND " +
        	        "c.type_id = ct.id " +
            	"ORDER BY " +
        	        "t.time_created ";

            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
            pstmt.setLong(3, TransactionsManagerBase.TRANS_STATUS_REQUESTED);
            pstmt.setLong(4, TransactionsManagerBase.TRANS_STATUS_APPROVED);
            pstmt.setLong(5, TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL);
            pstmt.setLong(6, ccId);
            rs = pstmt.executeQuery();

            if (rs.next()) {
            	t = new Transaction();
            	t.setAmount(rs.getLong("amount"));
            	t.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
            	t.setUtcOffsetCreated(rs.getString("utc_offset_created"));
            	CreditCard c = new CreditCard();
            	CreditCardType type = new CreditCardType();
            	type.setName(rs.getString("name"));
            	c.setType(type);
            	c.setHolderName(rs.getString("holder_name"));
				try {
					c.setCcNumber(Long.valueOf(AESUtil.decrypt(rs.getString("cc_number"))));
				} catch (Exception e) {
					log.error("Error in decryption", e);
				}
				t.setCreditCard(c);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return t;
    }

    /**
     * Get first transaction withdrawal by user id
     * @param con
     * @param userId
     * @return
     * @throws SQLException
     */
    public static Transaction getFirstWireWithdrawalByUser(Connection con, long userId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Transaction t = null;

        try {
            String sql = "SELECT " +
    		"t.id, " +
    		"t.amount, " +
    		"t.time_created, " +
    		"t.wire_id, " +
    		"t.type_id, " +
    		"t.moneybookers_email " +
    	"FROM " +
   		"transactions t " +
    	"WHERE " +
	        "t.user_id = ? AND " +
	        "t.type_id in (?,?,?,?,?,?,?) AND " +
	        "t.status_id in (?,?) " +
    	"ORDER BY " +
	        "t.time_created  DESC";
            pstmt = con.prepareStatement(sql);
            
            int index = 1;
            pstmt.setLong(index++, userId);
            
            pstmt.setLong(index++, TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW);
            pstmt.setLong(index++, TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW);
            pstmt.setLong(index++, TransactionsManagerBase.TRANS_TYPE_GIROPAY_WITHDRAW);
            pstmt.setLong(index++, TransactionsManagerBase.TRANS_TYPE_DIRECT24_WITHDRAW);
            pstmt.setLong(index++, TransactionsManagerBase.TRANS_TYPE_EPS_WITHDRAW);
            pstmt.setLong(index++, TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW);
            pstmt.setLong(index++, TransactionsManagerBase.TRANS_TYPE_IDEAL_WITHDRAW);
            
            pstmt.setLong(index++, TransactionsManagerBase.TRANS_STATUS_REQUESTED);
            pstmt.setLong(index++, TransactionsManagerBase.TRANS_STATUS_APPROVED);

            rs = pstmt.executeQuery();

            if (rs.next()) {
            	t = new Transaction();
            	t.setAmount(rs.getLong("amount"));
            	t.setId(rs.getLong("id"));
            	t.setWireId(rs.getBigDecimal("wire_id"));
            	t.setTypeId(rs.getLong("type_id"));
            	t.setMoneybookersEmail(rs.getString("moneybookers_email"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return t;
    }
    
    public static Long getWireByTransactionId(Connection con, long transactionId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =	"SELECT " +
					    		"t.wire_id " +
					    	"FROM " +
					    		"transactions t " +
					    	"WHERE " +
						        "t.id = ? " ;
            pstmt = con.prepareStatement(sql);
            
            pstmt.setLong(1, transactionId);

            rs = pstmt.executeQuery();

            if (rs.next()) {
            	return rs.getLong("wire_id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return null;
    }

    public static Long[] getDetailsByTransactionId(Connection con, long transactionId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =	"SELECT " +
					    		"t.credit_card_id, t.clearing_provider_id  " +
					    	"FROM " +
					    		"transactions t " +
					    	"WHERE " +
						        "t.id = ? " ;
            pstmt = con.prepareStatement(sql);
            
            pstmt.setLong(1, transactionId);

            rs = pstmt.executeQuery();

            if (rs.next()) {
            	Long[] l = {rs.getLong("credit_card_id"), rs.getLong("clearing_provider_id")};
            	return l;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return null;
    }

  

    
    public static Long getPaymentTypeIdByTransactionId(Connection con, long transactionId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =	"SELECT " +
					    		"t.payment_type_id " +
					    	"FROM " +
					    		"transactions t " +
					    	"WHERE " +
						        "t.id = ? " ;
            pstmt = con.prepareStatement(sql);
            
            pstmt.setLong(1, transactionId);

            rs = pstmt.executeQuery();

            if (rs.next()) {
            	return rs.getLong("payment_type_id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return null;
    }

	/**
	 * Get payment method by Id
	 * @param conn db connection
	 * @param paymentMethodId
	 * @return
	 * @throws SQLException
	 */
    public static PaymentMethod getPaymentMethodById(Connection conn, long paymentMethodId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        PaymentMethod vo = new PaymentMethod();

        try {
            String sql =
                " SELECT " +
                    " * " +
                " FROM " +
                    " payment_methods " +
                " WHERE " +
                	" id = ? ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, paymentMethodId);
            rs = pstmt.executeQuery();

            while (rs.next()) {
            	vo.setId(rs.getLong("id"));
            	vo.setName(rs.getString("name"));
            	vo.setDescription(rs.getString("description"));
            	vo.setEnvoyWithdrawCountryCode(rs.getString("envoy_withdraw_country_code"));
            	vo.setTypeId(rs.getInt("type_id"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return vo;
    }

	/**
	 * Get Envoy Account Numbers By UserId
	 * @param conn db connection
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
    public static ArrayList<String> getEnvoyAccountNumsByUserId(Connection conn, long userId, long paymentMethodId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<String> list = new ArrayList<String>();

        try {
            String sql =
                " SELECT " +
                    " t.envoy_account_num, " +
                    " max(t.time_created) " +
                " FROM " +
                    " transactions t " +
                " WHERE " +
                	" t.user_id = ? " +
                	" and t.envoy_account_num is not null " +
                	" and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
                	" and t.type_id = " + TransactionsManagerBase.TRANS_TYPE_ENVOY_ONLINE_DEPOSIT;
            if (paymentMethodId > 0){
            	sql += " and PAYMENT_TYPE_ID = ? ";
            }
            sql +=
            	" GROUP BY " +
            		" t.envoy_account_num " +
            	" ORDER BY " +
            		" max(t.time_created) desc ";


            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            if (paymentMethodId > 0){
            	  pstmt.setLong(2, paymentMethodId);
            }
            rs = pstmt.executeQuery();

            while (rs.next()) {
            	list.add(rs.getString("envoy_account_num"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

	/**
	 * Get payment methods with transactions
	 * @param con db connection
	 * @param paymentMethods list
	 * @return
	 * @throws SQLException
	 */
    public static ArrayList<PaymentMethod> getTransactionsPaymentMethodList(Connection con) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<PaymentMethod> list = new ArrayList<PaymentMethod>();

        try {
            String sql =
                " SELECT " +
                    " pm.*," +
                    " CASE WHEN pm.name is null THEN pm.CODE ELSE pm.name END pm_name " +
                " FROM " +
                    " payment_methods pm" +
                " WHERE " +
                	" exists (select * from transactions t where t.payment_type_id = pm.id)" +
                " ORDER BY " +
                	" pm.name ";

            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
            	PaymentMethod vo = new PaymentMethod();
            	vo.setId(rs.getLong("id"));
            	vo.setName(rs.getString("pm_name"));
            	vo.setDescription(rs.getString("description"));
            	vo.setEnvoyWithdrawCountryCode(rs.getString("envoy_withdraw_country_code"));
            	vo.setTypeId(rs.getInt("type_id"));
            	list.add(vo);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

	/**
	 * Update pixel run time by transaction id's
	 * Operated after running Google Analytics code
	 * @param con
	 * @param ids
	 * @throws SQLException
	 */
	public static void updatePixelRunTimeByList(Connection con, String ids) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
				"UPDATE " +
					"transactions " +
				"SET " +
					"pixel_run_time = sysdate " +
				"WHERE " +
					"id IN (" + ids + ") AND " +
					"pixel_run_time IS NULL ";
			ps = con.prepareStatement(sql);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	public static long getLastXCCTransactionsCountByUserAndDate(Connection con, long userId, long date) throws SQLException{
		PreparedStatement ps = null;
        ResultSet rs = null;
        long num = 0;

		try {
			String sql = "SELECT " +
            					" COUNT(*) as count" +
			             " FROM " +
			                    "transactions t " +
			             "WHERE " +
			             		" t.user_id = ? " +
			             		" AND (t.type_id = ? or t.type_id = ?) " +
			             		" AND t.status_id in (" + TransactionsManagerBase.TRANS_STATUS_SUCCEED + "," +
			             						TransactionsManagerBase.TRANS_STATUS_PENDING + ") " +
								" AND t.time_created > ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
			ps.setLong(3, TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
			ps.setDate(4, new java.sql.Date(date));
			rs = ps.executeQuery();
			if (rs.next()) {
				num = rs.getLong("count");
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);

		}
		return num;
	}

	/**
	 * get last Moneybookers email
	 * @param userId
	 * @param moneybookersEmail
	 * @return boolean - is valid moneybookersEmail
	 * @throws SQLException
	 */
	public static String getLastMoneybookersEmail(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
        ResultSet rs = null;

		try {
			String sql =
				" SELECT " +
					" t.moneybookers_email " +
				" FROM " +
					" transactions t " +
				" WHERE " +
					" t.user_id = ? " +
					" AND t.type_id = " + TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_DEPOSIT + " " +
					" AND t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
				" ORDER BY" +
					" t.time_created desc ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getString("moneybookers_email");
			}

		} finally {
			closeStatement(ps);
		}
		return ConstantsBase.EMPTY_STRING;
	}
	
	

	public static ArrayList<Transaction> getDepositListForCredit(Connection con, long user_id, long provider_id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Transaction> trans = new ArrayList<Transaction>();

		try {
			String sql = 
							" SELECT " +
								" t.id, " +
								" t.time_settled, " +
								" auth_number, " +
								" clearing_provider_id, " +
								" (t.amount - t.credit_amount) as credit_amount " +
						   " FROM " +
						   		" transactions t " +
						   " WHERE " +
						   		" t.user_id = ? " +
						   		" and t.clearing_provider_id = ? " +
						   		" and t.status_id = ? " +
						   		" and (t.amount - t.credit_amount) > 0 " +
					    	"ORDER BY " +
						    	"credit_amount";

			ps = con.prepareStatement(sql);
			ps.setLong(1, user_id);
			ps.setLong(2, provider_id);
			ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_SUCCEED);

			rs = ps.executeQuery();

			while (rs.next()) {
				Transaction t = new Transaction();
				t.setId(rs.getLong("id"));
				t.setAuthNumber(rs.getString("auth_number"));
				t.setCreditAmount(rs.getLong("credit_amount"));
				t.setClearingProviderId(rs.getLong("clearing_provider_id"));
				t.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));

				trans.add(t);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return trans;
	}
	
	

	public static ArrayList<Transaction> getDeltaPayDepositListForCredit(Connection con, long id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Transaction> trans = new ArrayList<Transaction>();

		try {
			String sql = " SELECT " +
				" t.id, " +
				" t.time_settled, " +
				" auth_number, " +
				" clearing_provider_id, " +
				" t.amount, " +
				" (t.amount - t.credit_amount) as credit_amount " +
		   " FROM " +
		   		" transactions t " +
		   " WHERE " +
		   		" t.user_id = ? " +
		   		" and t.type_id = ? " +
		   		" and t.status_id = ? " +
		   		" and (t.amount - t.credit_amount) > 0 " +
		    	"ORDER BY " +
		    		"credit_amount";

			ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			ps.setLong(2, TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_DEPOSIT);
			ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_SUCCEED);

			rs = ps.executeQuery();

			while (rs.next()) {
				Transaction t = new Transaction();
				t.setId(rs.getLong("id"));
				t.setAuthNumber(rs.getString("auth_number"));
				t.setCreditAmount(rs.getLong("credit_amount"));
				t.setAmount(rs.getLong("amount"));
				t.setClearingProviderId(rs.getLong("clearing_provider_id"));
				t.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));

				trans.add(t);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return trans;
	}
	
	public static Long getProvidersPriorityForUser(Connection con, long user_id) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long providerId = ClearingManagerConstants.DELTAPAY_CHINA_PROVIDER_ID;

		try {
			String sql = 
					" SELECT " +
						" t.clearing_provider_id " + 
					" FROM  " +
							" transactions t " +
					" WHERE " +
							" t.time_created = ( " +
												" SELECT " + 
														" MIN( t.time_created) " + 
												" FROM  " +
														" transactions t, clearing_providers cp " + 
												" WHERE " +
														 " user_id = ? " +
														 " and type_id = 38 " +  
														 " and status_id = 2 " +  
														 " and cp.id = t.clearing_provider_id " + 
														 " and cp.is_active = 1  " +
												" ) ";
			
			ps = con.prepareStatement(sql);
			ps.setLong(1, user_id);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				providerId = rs.getLong("clearing_provider_id");
			}
		}finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return providerId;
	}
	
	public static ArrayList<Transaction> getDepositListForProvider(Connection con, long user_id, long provider_id, boolean countBeforeCUP) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Transaction> trans = new ArrayList<Transaction>();

		try {
			String sql = " SELECT " +
				" t.id, " +
				" t.time_settled, " +
				" t.xor_id_capture, " +
				" t.auth_number, " +
				" t.clearing_provider_id, " +
				" t.amount, " +
				" (t.amount - t.credit_amount) as credit_amount " +
		   " FROM " +
		   		" transactions t " +
		   " WHERE " +
		   		" t.user_id = ? " +
		   		" and t.type_id = ? " +
		   		" and t.status_id = ? " +
		   		" and (t.amount - t.credit_amount) > 0 " +
		   		" and t.clearing_provider_id = ? " +
		   		" and t.selector_id > ? " +
		    	"ORDER BY " +
		    		"credit_amount";

			ps = con.prepareStatement(sql);
			ps.setLong(1, user_id);
			ps.setLong(2, TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_DEPOSIT);
			ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
			ps.setLong(4, provider_id);
			if(countBeforeCUP) { // selector_id = -1 are transactions before CUP
				ps.setLong(5, -2); // include transactions before CUP
			} else { 
				ps.setLong(5, -1); // exclude transactions before CUP
			}
			
			rs = ps.executeQuery();

			while (rs.next()) {
				Transaction t = new Transaction();
				t.setId(rs.getLong("id"));
				t.setAuthNumber(rs.getString("auth_number"));
				t.setCreditAmount(rs.getLong("credit_amount"));
				t.setAmount(rs.getLong("amount"));
				t.setClearingProviderId(rs.getLong("clearing_provider_id"));
				t.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));
				t.setXorIdCapture(rs.getString("xor_id_capture"));
				
				trans.add(t);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return trans;
	}
	public static void updateDeltaPayTransaction(Connection con, long transactionId, String authNumber, long statusId, String comments, String UtcOffset) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " UPDATE " +
						 "		transactions " +
						 " SET " +
						 "		auth_number = ?, " +
						 "		status_id = ?, " +
						 "		comments = ?, " +
						 "		time_settled = sysdate," +
						 "		utc_offset_settled = ? " +
						 " WHERE " +
						 "		id = ? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, authNumber);
			ps.setLong(2, statusId);
			ps.setString(3, comments);
			ps.setString(4, UtcOffset);
			ps.setLong(5, transactionId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	
	public static boolean insertLimit(Connection con, ClearingLimitaion cl) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sqlInsert = " INSERT INTO clearing_routing_limitations (id ";
			String sqlValue = " VALUES (SEQ_CLEARING_ROUTING_LIMIT.nextval " ;
			
			sqlInsert +=  " ,clearing_provider_group_id ";
			sqlValue  += " , " + String.valueOf(cl.getClearingProviderIdGroup());
			if (cl.getCurrencyId() > 0) {
				sqlInsert += " ,currency_id " ;
				sqlValue += " , " + String.valueOf(cl.getCurrencyId());
			}
			
			if (cl.getCreditCardTypeId() > 0) {
				sqlInsert += " ,credit_card_type_id " ;
				sqlValue += " , " +  String.valueOf(cl.getCreditCardTypeId());
			}
			
			if (cl.getBinId() > 0) {
				sqlInsert += " ,bin_id " ;
				sqlValue += " , " +  String.valueOf(cl.getBinId());
			}
			
			if (cl.getCountryId() > 0) {
				sqlInsert += " ,country_id " ;
				sqlValue += " , " +  String.valueOf(cl.getCountryId());
			}
			sqlInsert += " ) ";
			sqlValue += " ) ";
			
			String sql = sqlInsert + sqlValue;
			ps = con.prepareStatement(sql);
			ps.executeQuery();
		} finally {
			closeStatement(ps);
		} 
		return true;
	}
	public static ArrayList<ClearingLimitaion> getClearingLimitaionList(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<ClearingLimitaion> list = new ArrayList<ClearingLimitaion>();
		try {
			String sql =
						" SELECT " +
						"		* " +
						" FROM ( " +
						" SELECT " +
						"		crl.id, " +
						"		cpg.group_id, " +  
					    "		cpg.description as limit_description, " +
					    "		c.id as currency_id, " +
					    "		c.name_key as currency_name, " +
					    "		crl.credit_card_type_id, " +
					    "		cct.description as cc_name, " +
					    "		crl.bin_id, " +
					    "		crl.country_id, " + 
					    "		co.country_name as country_name, " + 
					    "		crl.is_active " +
					    " FROM " + 
					    "		clearing_routing_limitations crl " +
					    "		LEFT JOIN clearing_providers_groups cpg ON crl.clearing_provider_group_id = cpg.group_id " +
                        "       LEFT JOIN currencies c ON c.id = crl.currency_id " +
                        "       LEFT JOIN credit_card_types cct ON cct.id = crl.credit_card_type_id " +
                        "       LEFT JOIN countries co ON co.id = crl.country_id " +
                        "       LEFT JOIN bins b ON b.from_bin = crl.bin_id " +
                        " WHERE " +
                        "		crl.is_active = 1" +
                        " GROUP BY " +
                        "		crl.id, " +
						"		cpg.group_id, " +  
					    "		cpg.description, " +
					    "		c.id, " +
					    "		c.name_key, " +
					    "		crl.credit_card_type_id, " +
					    "		cct.description, " +
					    "		crl.bin_id, " +
					    "		crl.country_id, " + 
					    "		co.country_name, " + 
					    "		crl.is_active) " +
					    " ORDER BY id " ;
				           
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				ClearingLimitaion vo = new ClearingLimitaion();
				vo.setId(rs.getLong("id"));
				//vo.setClearingProviderGroupLimit(rs.getLong("group_id"), rs.getString("limit_description"));
				vo.setClearingProviderIdGroup(rs.getLong("group_id"));
				vo.setClearingProviderGroupName(rs.getString("limit_description"));
				vo.setCurrencyId(rs.getLong("currency_id"));
				vo.setCurrencyName(rs.getString("currency_name"));
				vo.setCreditCardTypeId(rs.getLong("credit_card_type_id"));
				vo.setCreditCardTypeName(rs.getString("cc_name"));
				vo.setBinId(rs.getLong("bin_id"));
				vo.setCountryId(rs.getLong("country_id"));
				vo.setCountryName(rs.getString("country_name"));
				vo.setActive(rs.getBoolean("is_active"));
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;	
	}
	
	public static boolean setLimitAsNotActive(Connection con, ClearingLimitaion clearingLimitaion) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " UPDATE " +
						 "		clearing_routing_limitations " +
						 " SET " +
						 "		is_active = 0 " +
						 " WHERE " +
						 "		id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, clearingLimitaion.getId());
			ps.executeQuery();
		} finally {
			closeStatement(ps);
		} 
		return true;
	}
	
	public static boolean setLimitAsActive(Connection con, ClearingLimitaion clearingLimitaion) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " UPDATE " +
						 "		clearing_routing_limitations " +
						 " SET " +
						 "		is_active = 1 " +
						 " WHERE " +
						 "		id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, clearingLimitaion.getId());
			ps.executeQuery();
		} finally {
			closeStatement(ps);
		} 
		return true;
	}
	
	public static long isLimitExists(Connection con, ClearingLimitaion cl) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " SELECT " +
					     "		* " +
					     " FROM " +
					     "		clearing_routing_limitations crl " +
					     " WHERE crl.clearing_provider_group_id = " + cl.getClearingProviderIdGroup();
			
			if (cl.getCurrencyId() > 0) {
				sql += " AND currency_id = " + String.valueOf(cl.getCurrencyId());
			}
			
			if (cl.getCreditCardTypeId() > 0) {
				sql += " AND credit_card_type_id = " + String.valueOf(cl.getCreditCardTypeId());
			}
			
			if (cl.getBinId() > 0) {
				sql += " AND bin_id = " + String.valueOf(cl.getBinId());
			}
			
			if (cl.getCountryId() > 0) {
				sql += " AND country_id = "  + String.valueOf(cl.getCountryId());
			}
													           
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("id");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;	
	}
	/**
	 * Check if user have wire deposit or cash deposit in status pending
	 * @param con
	 * @param transaction
	 * @return 
	 * @throws SQLException
	 */
	public static boolean IsHavePendingDeposit(Connection con, Transaction transaction) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean flag = false;
		try {
		    String sql = "SELECT " +
							" * " +
						" FROM " +
							" transactions t, " +
							" transaction_types ty, " +
							" users u " +
						" WHERE " +
							" t.type_id = ty.id " +
							" AND t.user_id = u.id " +
							" AND ty.class_type = ? " +
							" AND u.id = ? " +
							" AND t.status_id = ? " +
							" AND (t.type_id = ? OR t.type_id = ?)";
	
			ps = con.prepareStatement(sql);
			ps.setLong(1, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
			ps.setLong(2, transaction.getUserId());
			ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_PENDING);
			ps.setLong(4, TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT);
			ps.setLong(5, TransactionsManagerBase.TRANS_TYPE_CASH_DEPOSIT);
			rs = ps.executeQuery();
			if (rs.next()) {   // user have monthly withdrawals
				flag = true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return flag;
	}
    /**
     * @param con
     * @param id
     * @returns transaction status_id.
     * use for update to lock the row which selected. 
     * @throws SQLException
     */
    public static long checkForUpdateTransaction(Connection con, long id)  throws SQLException {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	long status = 0;
		try {
			  String sql =	" SELECT " +
			  				"	t.id, " +
			  				"	t.status_id " +
					  		" FROM " +
					  		" 	transactions t " +
					  		" WHERE " +
					  		"	t.id = ? " +
					  		" FOR UPDATE ";
				ps = con.prepareStatement(sql);
				ps.setLong(1, id);
				rs = ps.executeQuery();
				if (rs.next()) {
					status = rs.getLong("status_id");
				}
		 } finally {
				closeStatement(ps);
				closeResultSet(rs);
		 }
		 return status;
    }
    
	public static BigDecimal refNegTransaction(Connection conn, Long bonusUserId) throws SQLException {
		PreparedStatement ps = null;
    	ResultSet rs = null;
    	BigDecimal transactionId = null;
		try {
			  String sql =	" SELECT " +
			  				"	t.id " +
					  		" FROM " +
					  		" 	transactions t " +
					  		" WHERE " +
					  		"	t.type_id in ("+ TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW + ") AND " +
					  		"	t.bonus_user_id = ? ";
				ps = conn.prepareStatement(sql);
				ps.setLong(1, bonusUserId);
				rs = ps.executeQuery();
				if (rs.next()) {
					transactionId = rs.getBigDecimal("id");
				}
		 } finally {
				closeStatement(ps);
				closeResultSet(rs);
		 }
		 return transactionId;
	}
	
    public static Transaction getFTDInfoById(Connection con, long firstTimeDepositId)  throws SQLException {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	Transaction transaction = null;
		try {
			  String sql =	" SELECT " +
			  				"	t.amount, " +
			  				"	t.rate, " +
			  				" 	t.time_created " +
					  		" FROM " +
					  		" 	transactions t " +
					  		" WHERE " +
					  		"	t.id = ? ";
				ps = con.prepareStatement(sql);
				ps.setLong(1, firstTimeDepositId);
				rs = ps.executeQuery();
				if (rs.next()) {
					transaction = new Transaction();
					transaction.setAmount(rs.getLong("amount"));
					double rate = rs.getDouble("rate");
	                transaction.setBaseAmount(Math.round(rate * transaction.getAmount()));
	                transaction.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				}
		 } finally {
				closeStatement(ps);
				closeResultSet(rs);
		 }
		 return transaction;
    }
    
    public static long getSumOfRealDepositsUSD(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long amount = 0;

		try {
			String sql = 
					" SELECT " +
					"		sum(t.amount * t.rate) as userAmount " +
					" FROM " +
					"		transactions t, transaction_types tt " +
					" WHERE " +
					"		t.user_id = ? " +
					"		AND t.type_id = tt.id " +
					"		AND tt.class_type = ? " +
					"		AND t.status_id in (?,?,?)";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT);
			ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
			ps.setLong(4, TransactionsManagerBase.TRANS_STATUS_PENDING);
			ps.setLong(5, TransactionsManagerBase.TRANS_STATUS_CANCELED_ETRADER);
			rs = ps.executeQuery();

			if (rs.next()) {
				amount = rs.getLong("userAmount");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return amount;
	}

	/**
	 * @param connection
	 * @param info
	 * @param userUtcOffset
	 * @throws SQLException
	 */
	public static void updateTransactionCapture(Connection connection, ClearingInfo info, String userUtcOffset) throws SQLException {
		PreparedStatement ps = null;
		int index = 0; 
		
		if (info != null) {
			try {				
				String sql = 
						"UPDATE " +
						"	transactions t " +
						"SET " +
		                "	time_settled = sysdate " +
		                "	,status_id = ? " +
		       			"	,comments = ? " +
		     	 		"	,description = ? " +
		                "	,xor_id_capture = ? " +
		                "	,utc_offset_settled = ? " +
		                "WHERE " +
		                "	id = " + info.getTransactionId();
				ps = connection.prepareStatement(sql);
				ps.setInt(++index, info.isSuccessful() ? TransactionsManagerBase.TRANS_STATUS_SUCCEED : TransactionsManagerBase.TRANS_STATUS_FAILED);
				ps.setString(++index, info.getResult() + "|" + info.getMessage() + "|" + info.getUserMessage() + "|" + info.getProviderTransactionId());
				ps.setString(++index, info.isSuccessful() ? "" : ConstantsBase.ERROR_FAILED_CAPTURE);
				ps.setString(++index, info.getProviderTransactionId());	
				ps.setString(++index, userUtcOffset);
				ps.executeUpdate();
			} finally {
				closeStatement(ps);
			}	
		}
	}
	
	/**
	 * @param con
	 * @param tranId
	 * @param status
	 * @param settled
	 * @param writer
	 * @throws SQLException
	 */
	public static void updateStatusId(Connection connection, long tranId, long status, boolean settled, long writer) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = 
					"UPDATE " +
					"	transactions t " +
					"SET " +
					"	t.status_id = ? ";
			if (settled) {
				sql +=			
					"	,t.time_settled = sysdate ";
			}
			sql +=	"	,t.utc_offset_settled = " +
					"		( " + 
					"			SELECT " + 
					"				utc_offset " +
					"			FROM " +
					"				users u " +
					"			WHERE " +
					"				t.user_id = u.id " +
					"		) " +
					"	,t.processed_writer_id = ? " +
					"WHERE " +
					"	t.id = ? ";
			ps = connection.prepareStatement(sql);
			ps.setLong(1, status);
			ps.setLong(2, writer);
			ps.setLong(3, tranId);
			
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * @param connection
	 * @param info
	 * @return
	 * @throws SQLException
	 */
	public static EpgRequestDetails getEpgRequestAdditionalDetails(Connection connection, ClearingInfo info) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		EpgRequestDetails details = new EpgRequestDetails();

		try {
			String sql = 
					"SELECT " +
					"	 u.balance " +
					"	,u.tax_balance " +
					//"	,u.currency_id " +
					//"	,u.first_name " +
					//"	,u.last_name " +
					//"	,u.street " +
					//"	,u.city_name " +
					//"	,u.zip_code " +
					"	,u.time_created " +
					"	,u.is_active " +
					"	,p.name as platform " +
					//"	,u.skin_id " +
					//"	,u.id " +
					//"	,u.country_id " +
					"	,u.ip " +
					"	,utd.ftd_date " +
					"	,utd.total_sum_deposit " +		
					"	,utd.num_success_deposit " +
					"	,utd.num_success_withdraw " +	
					"	,ur.approved_regulation_step " +
					"	,utd.active_risk_issue " +
					"	,utd.has_blocked_cc " +
					"	,mcp.id as campaign_number " + 		
					"	,u.affiliate_key " +
					"	,cctd.first_success_deposit_date " +
					"	,cctd.upload_documents " +
					"	,cctd.highest_deposit " +
					"FROM " +
					"	 users u " +
					"		left join users_regulation ur on u.id = ur.user_id " +
					"		left join credit_cards cc on u.ID = cc.user_id and cc.id = ? " +
					"		left join cc_transaction_details cctd on cc.id = cctd.cc_id " +
					"		left join user_transaction_details utd on u.id = utd.user_id  " +	                
					"	,marketing_combinations mcb " +
					"	,marketing_campaigns mcp " +
					"	,platforms p " +
					"WHERE " +
					"	u.combination_id = mcb.id " +
					"	AND mcb.campaign_id = mcp.id " +
					"	AND u.platform_id = p.id " +
					"	AND u.id = ? ";
			ps = connection.prepareStatement(sql);
			ps.setLong(1, info.getCardId());
			ps.setLong(2, info.getUserId());
			rs = ps.executeQuery();

			if (rs.next()) {
				details.setBalance(String.valueOf(rs.getLong("balance")));
				details.setTaxBalance(String.valueOf(rs.getLong("tax_balance")));
				//details.setFirstName(rs.getString("first_name"));
				//details.setLastName(rs.getString("last_name"));
				//details.setStreet(rs.getString("street"));
				//details.setUserCityName(rs.getString("city_name"));
				//details.setZipCode(rs.getString("zip_code"));
				details.setRegistrationDate(String.valueOf(rs.getDate("time_created")));
				details.setIsActive(rs.getBoolean("is_active") ? "true" : "false");
				details.setPlatform(rs.getString("platform"));
				//details.setUserId(String.valueOf(userId));
				details.setRegistrationIP(rs.getString("ip"));				
				details.setFtdDate(String.valueOf(rs.getDate("ftd_date")));
				details.setTotalSumDeposit(String.valueOf(rs.getInt("total_sum_deposit")));
				details.setSuccessfulDepositCount(String.valueOf(rs.getInt("num_success_deposit")));
				details.setSuccessfulWithdrawalCount(String.valueOf(rs.getInt("num_success_withdraw")));
				details.setIsControlApproved(rs.getInt("approved_regulation_step") == UserRegulationBase.REGULATION_CONTROL_APPROVED_USER ? "true" : "false");
				details.setHasActiveRiskIssueInHighPriority(rs.getString("active_risk_issue"));
				details.setHasBlockedCC(rs.getBoolean("has_blocked_cc") ? "true" : "false");				
				details.setCampaignNumber(rs.getString("campaign_number"));
				details.setAffiliateKey(rs.getString("affiliate_key"));				
				details.setFirstSuccessfulDepositWithSameCC(rs.getString("first_success_deposit_date"));
				details.setIsUploadDocumentForThisCC(rs.getBoolean("upload_documents") ? "true" : "false");
				details.setIsHighestDepositMadeByTheUser(rs.getBoolean("highest_deposit") ? "true" : "false");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return details;
	}
	
	/**
	 * @param connection
	 * @param info
	 * @return
	 * @throws SQLException
	 */
	public static EpgDepositRequestDetails getEpgDepositRequestAdditionalDetails(Connection connection, ClearingInfo info) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		EpgDepositRequestDetails details = new EpgDepositRequestDetails();

		try {
			String sql = 
					"SELECT " +
					//"	 u.balance " +
					//"	,u.tax_balance " +
					"	u.time_created " +
					"	,sbc.code as skin_business_case_code " +
					"	,s.name as skin_name" +							
					"	,p.name as platform " +
					"	,c.country_name " +
					"	,u.ip " +	
					"	,t.writer_id " +		
					"	,u.affiliate_key " +	
					"	,bcpm.epg_product_id " +
					"FROM " +
					"	 users u " +
					"	,transactions t " +
					"	,platforms p " +
					"	,skins s " +
					"	,skin_business_cases sbc " +
					"	,countries c " +
					" 	,business_cases_platforms_map bcpm " +
					"WHERE " +
					"	u.id = t.user_id " +
					"	AND u.platform_id = p.id " +
					"	AND u.skin_id = s.id " +
					"	AND s.business_case_id = sbc.id " +
					"	AND u.country_id = c.id " +
					"	AND bcpm.platforms_id = p.id " +
					" 	AND bcpm.skin_business_cases_id = sbc.id " +
					"	AND u.id = ? " +
					"	AND t.id = ? ";
			ps = connection.prepareStatement(sql);
			ps.setLong(1, info.getUserId());
			ps.setLong(2, info.getTransactionId());
			rs = ps.executeQuery();

			if (rs.next()) {
				//details.setBalance(String.valueOf(rs.getLong("balance")));
				//details.setTaxBalance(String.valueOf(rs.getLong("tax_balance")));
				details.setRegistrationDate(String.valueOf(rs.getDate("time_created")));
				details.setBusinessCase(rs.getString("skin_business_case_code"));
				details.setSkinName(rs.getString("skin_name"));
				details.setPlatform(rs.getString("platform"));
				details.setCountryName(rs.getString("country_name"));
				//details.setTransactionAmountInUSD(""); TODO 2nd;
				details.setRegistrationIP(rs.getString("ip"));
				details.setTransactionWriterName(Writers.getWriterName(rs.getInt("writer_id")).getName());
				//details.setIsHighestDepositMadeByTheUser(""); TODO 2nd;
				//details.setCampaignName(""); TODO 2nd
				details.setAffiliateKey(rs.getString("affiliate_key"));	
				details.setProductId(rs.getString("epg_product_id"));
				//details.setCcFtdDate(""); TODO 2nd;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return details;
	}	
	
	/**
	 * @param connection
	 * @param info
	 * @return
	 * @throws SQLException
	 */
	//TODO unit test
	public static EpgWithdrawalRequestDetails getEpgWithdrawalRequestAdditionalDetails(Connection connection, ClearingInfo info) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		EpgWithdrawalRequestDetails details = new EpgWithdrawalRequestDetails();

		try {
			String sql = 
					"SELECT " +
					//"	 u.balance " +
					//"	u.tax_balance " +
					"	u.time_created " +
					"	,sbc.code as skin_business_case_code " +
					"	,s.name as skin_name" +							
					"	,p.name as platform " +
					"	,c.country_name " +
					"	,ur.approved_regulation_step " +
					"	,u.is_active " +
					"	,u.ip " +	
					"	,t.writer_id " +		
					"	,u.affiliate_key " +
					"	,bcpm.epg_product_id " +
					"FROM " +
					"	 users u " +
					"		left join users_regulation ur on u.id = ur.user_id " +
					"	,transactions t " +
					"	,platforms p " +
					"	,skins s " +
					"	,skin_business_cases sbc " +
					"	,countries c " +
					" 	,business_cases_platforms_map bcpm " +
					"WHERE " +
					"	u.id = t.user_id " +
					"	AND u.platform_id = p.id " +
					"	AND u.skin_id = s.id " +
					"	AND s.business_case_id = sbc.id " +
					"	AND u.country_id = c.id " +
					"	AND bcpm.platforms_id = p.id " +
					" 	AND bcpm.skin_business_cases_id = sbc.id " +
					"	AND u.id = ? " +
					"	AND t.id = ? ";
			ps = connection.prepareStatement(sql);
			ps.setLong(1, info.getUserId());
			ps.setLong(2, info.getTransactionId());
			rs = ps.executeQuery();

			if (rs.next()) {
				//details.setBalance(String.valueOf(rs.getLong("balance")));
				//details.setTaxBalance(String.valueOf(rs.getLong("tax_balance")));
				details.setRegistrationDate(String.valueOf(rs.getDate("time_created")));
				details.setBusinessCase(rs.getString("skin_business_case_code"));
				details.setSkinName(rs.getString("skin_name"));
				details.setPlatform(rs.getString("platform"));
				details.setCountryName(rs.getString("country_name"));
				details.setIsControlApproved(rs.getInt("approved_regulation_step") == UserRegulationBase.REGULATION_CONTROL_APPROVED_USER ? "true" : "false");		
				//details.setTransactionAmountInUSD("");// TODO 2nd;
				//details.setTotalSumDepositInUSD("");// TODO 2nd;
				//details.setSuccessfulDepositCount("");// TODO 2nd;
				//details.setSuccessfulWithdrawalCount("");// TODO 2nd;
				//details.setFtdDate("");// TODO 2nd;
				details.setIsActive(rs.getBoolean("is_active") ? "true" : "false");
				details.setRegistrationIP(rs.getString("ip"));
				details.setTransactionWriterName(Writers.getWriterName(rs.getInt("writer_id")).getName());
				//details.setHasActiveRiskIssueInHighPriority("");// TODO 2nd;
				//details.setHasBlockedCC("");// TODO 2nd;
				//details.setCampaignName("");// TODO 2nd
				details.setAffiliateKey(rs.getString("affiliate_key"));	
				details.setProductId(rs.getString("epg_product_id"));
				//details.setCcFtdDate(""); //TODO 2nd;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return details;
	}
		
	/**
	 * See captureWithdrawal job
	 * 
	 * Update transaction after captureWithdrawal
	 * @param info ClearingInfo instance of the current trx that captured
	 * @param hasFee true in case we need to create fee transaction
	 * @param feeAmount fee amount to take
	 * @param utcOffset user utcOffset
	 * @throws SQLException
	 */
	//TODO - clean it.
	//copy & past from capture withdrawal job 
	public static void updateTransactionAfterCaptureWithdrawal(Connection connection, ClearingInfo info, boolean hasFee, long feeAmount, String utcOffset, boolean isSuccessful,
											boolean splittedUpdate, boolean cftEnabled, String internalTransactions, long internalsAmount,
											boolean isSplittedCFT, long amountForLowWithdraw, Date transactionTimeCreated,
											String transactionUtcOffsetCreated) throws SQLException {
		PreparedStatement ps = null;
		int index = 1;
        try {
            String updateSql;
        	if (isSuccessful) { //handle success from provider
              	//update transaction status to success and amount to the amount after fee (in case there was fee)
        		updateSql =
                    "UPDATE " +
                        "transactions " +
                    "SET " +
                        "time_settled = sysdate, " +
                        "status_id =  ?, ";
        				if(!splittedUpdate) {
        					updateSql += "comments = ? || '|' || ? || '|' || ? || '|' || ? , " +
        								 "xor_id_capture = ? , ";
        				} else {
        					if(!isSplittedCFT){
        						updateSql += "comments =  ? || '|' || ? || '|' || ? || '| SplittedTrx to : ' || ? ,";
        					} else {
        						updateSql += "comments =  ? || '|' || ? || '|' || ? || '| SplittedTrx to CREDIT : ' || ? || '| SplittedTrx to CFT:' || ? ,";        					
        					}
        					
        				}

        				updateSql += "amount =  ?, " +
               						 "utc_offset_settled = ?, " +
               						 "clearing_provider_id = ? ";

						 if (splittedUpdate && cftEnabled) {  // in case we update spllited trx(not after capture) and the trx was not credit on creation.
							 updateSql += ",is_credit_withdrawal = 1 ";
						 }

						 updateSql += "WHERE " +
						 				"id = ? ";

						 ps = connection.prepareStatement(updateSql);
						 ps.setInt(index++, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
						 ps.setString(index++, info.getResult());
						 ps.setString(index++, info.getMessage());
						 ps.setString(index++, info.getUserMessage());
						 if (!splittedUpdate) {
							 ps.setString(index++, info.getProviderTransactionId());
							 ps.setString(index++, info.getProviderTransactionId());
							 ps.setLong(index++, info.getAmount());
							 ps.setString(index++, utcOffset);
							 ps.setLong(index++, info.getProviderId());
							 ps.setLong(index++, info.getTransactionId());

						 } else {
							 if(!isSplittedCFT){
								 ps.setString(index++, internalTransactions);
							 }else {
								 String[] a = internalTransactions.split(",");								 
								 String credit ="";
								 String cft = "";
								 int size = a.length;
								 a[0] = a[0].substring(1);
								 a[size-1] = a[size -1].substring(0, a[size-1].length()-1);
								 
								 Arrays.sort(a, new Comparator<String>()
										    {
										      public int compare(String s1, String s2)
										      {
										        return Integer.valueOf(s1.trim()).compareTo(Integer.valueOf(s2.trim()));
										      }
										    });
								 for(String s: a){
									 if(!s.equalsIgnoreCase(a[size-1])){
										 credit += s + ",";
									 }else {
										 cft += s;
									 }
								 }
								 if (credit.length() > 0) {
									 credit = credit.substring(1, credit.length()-1);
								 }
								 cft = cft.substring(0, cft.length());
								 ps.setString(index++, credit);
								 ps.setString(index++, cft);
							 }
							 ps.setLong(index++, info.getAmount());
							 ps.setString(index++, utcOffset);
							 ps.setLong(index++, info.getProviderId());
							 ps.setLong(index++, info.getTransactionId());
						 }

				//!!!										   !!!
				//!!CRITICAL SECTION - must be atomic statement!!!
				//!!!										   !!!
				//method assumption - make one time functionality for each transaction. 
				User user = UsersDAOBase.getUserById(connection, info.getUserId());
				WithdrawEntryResult finalWithdrawEntryResult = WithdrawUtil.finalWithdrawEntry(new WithdrawEntryInfo(user, info.getTransactionId()));	
				
				connection.setAutoCommit(false); //in order to support roll back once and update or an insert was not successful

               	//insert new fee transaction
               	if (hasFee) {
               		com.anyoption.common.beans.Transaction feeTransaction;
					feeTransaction = getFeeTransaction(info); // creating the fee transaction accoring to the orig transaction
					feeTransaction.setAmount(feeAmount);
                	feeTransaction.setUtcOffsetCreated(utcOffset);
                	feeTransaction.setUtcOffsetSettled(utcOffset);
    				TransactionsDAOBase.insert(connection, feeTransaction);
    				if (log.isInfoEnabled()) {
    					String ls = System.getProperty("line.separator");
    					log.info("fee transaction inserted: " + ls + feeTransaction.toString());
    				}

    				//update Transaction reference
    				TransactionsDAOBase.updateRefId(connection, info.getTransactionId(), feeTransaction.getId());
               	}
        	} else { //handle failed from provider
        		//update transaction status to failed
        		updateSql =
                    "UPDATE " +
                        "transactions " +
                    "SET " +
                        "time_settled = sysdate, " +
                        "status_id = ? , " +
      	 				"comments = ? || '|' || ? || '|' || ? || '|' || ? , " +
      	 				"description = ? , ";
        				if (!splittedUpdate) {
        					updateSql += "xor_id_capture = ?, ";
        				}
        				if (splittedUpdate) {
        					updateSql += "internals_amount = internals_amount + " + internalsAmount + ", ";
        				}
        				updateSql +=  "utc_offset_settled = ?, " +
                        			  "clearing_provider_id = ? " +
                        			  "WHERE " +
                        			  		"id = ? ";

       				 ps = connection.prepareStatement(updateSql);
					 ps.setInt(1, TransactionsManagerBase.TRANS_STATUS_FAILED);
					 ps.setString(2, info.getResult());
					 ps.setString(3, info.getMessage());
					 ps.setString(4, info.getUserMessage());
					 ps.setString(5, info.getProviderTransactionId());
					 ps.setString(6, ConstantsBase.ERROR_FAILED_CAPTURE);
					 if (!splittedUpdate) {
						 ps.setString(7, info.getProviderTransactionId());
						 ps.setString(8, utcOffset);
						 ps.setLong(9, info.getProviderId());
						 ps.setLong(10, info.getTransactionId());
					 } else {
						 ps.setString(7, utcOffset);
						 ps.setLong(8, info.getProviderId());
						 ps.setLong(9, info.getTransactionId());
					 }

        	}
        	ps.executeUpdate();
        	connection.commit();
        } catch (Exception e) {
			log.log(Level.ERROR, "ERROR! Can't update transaction!. ", e);
			try {
				connection.rollback();
			} catch (Exception er) {
				log.log(Level.ERROR, "Can't rollBack! ", er);
			}
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				log.error("Can't set back to autocommit.", e);
			}
			try {
				connection.close();
        	} catch (Exception e) {
        		log.log(Level.ERROR, "Can't close: conn2", e);
        	}
		}
	}
	
	private static com.anyoption.common.beans.Transaction getFeeTransaction(ClearingInfo info, int transTypeId) {
		com.anyoption.common.beans.Transaction t = new com.anyoption.common.beans.Transaction();
    	t.setUserId(info.getUserId());
    	t.setTypeId(transTypeId);
    	t.setTimeCreated(new Date());
    	t.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
    	t.setWriterId(Writer.WRITER_ID_AUTO);
    	t.setIp("IP NOT FOUND!");
    	t.setTimeSettled(new Date());
    	t.setProcessedWriterId(Writer.WRITER_ID_AUTO);
    	t.setReferenceId(new BigDecimal(info.getTransactionId()));
    	t.setAuthNumber(info.getAuthNumber());
    	t.setXorIdCapture(info.getProviderTransactionId());
    	return t;
	}
	
	/**
	 * This method create a fee transaction and sets it accoring to the clearing info
	 * @param info - Clearing info sent to Xor
	 * @return New fee transaction
	 */
	private static com.anyoption.common.beans.Transaction getFeeTransaction(ClearingInfo info) {
		return getFeeTransaction(info, TransactionsManagerBase.TRANS_TYPE_HOMO_FEE);
	}

	/**
	 * @param connection
	 * @param captureWithdrawal
	 * @throws SQLException
	 */
	public static void updateTransactionAfterCaptureWithdrawal(Connection connection, CaptureWithdrawal captureWithdrawal) throws SQLException {
		PreparedStatement ps = null;
		int index = 1;
		try {
			String sql =
					"UPDATE " +
					"	transactions t " +
					"SET " +
					"	t.time_settled = sysdate " +
					"	,t.status_id = ? " +
					"	,t.comments = t.comments || '|'  || ? " +
					"	,t.description = t.description || '|' || ? " +
					"	,t.xor_id_capture = ? " +
					"	,t.utc_offset_settled = ? " +
					"	,t.clearing_provider_id = ? " +
					"WHERE " +
					"	t.id = ? ";

			ps = connection.prepareStatement(sql);
			ps.setInt(index++, captureWithdrawal.getStatusId());
			ps.setString(index++, captureWithdrawal.getComment());
			ps.setString(index++, captureWithdrawal.getDescription());
			ps.setString(index++, captureWithdrawal.getProviderTransactionId());
			ps.setString(index++, captureWithdrawal.getUtcOffsetSettled());
			ps.setLong(index++, captureWithdrawal.getClearingProviderId());
			ps.setLong(index++, captureWithdrawal.getTransactionId());
			ps.executeUpdate();	
		} finally {
			closeStatement(ps);
		}
	}
	
	public static boolean isDisableCcDeposits(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " SELECT 'x' num "
							+ " FROM users_active_data t "
							+ " WHERE t.user_id = ? "
							+ " AND nvl(is_disable_cc_deposits,0) > 0 ";							
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return false;
	}
	
	public static void updateDisableCcDeposits(Connection connection, boolean isDisableCcDeposits, long userId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = 
					"UPDATE " +
					"	users_active_data t " +
					"SET " +
					" t.is_disable_cc_deposits = ? " +
					" where t.user_id = ? ";
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, isDisableCcDeposits);
			ps.setLong(2, userId);
			
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * Get first transaction id by user id
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static long getFirstTrxId(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long transactionId = 0;
		try {
			String sql = 	
						" SELECT " +
						" 	min(t.id) trx_id " +
						" FROM " +
						"	transactions t, " +
						"   transaction_types tt " +
						" WHERE " +
						"   t.user_id = ? " +
						"   AND tt.id = t.type_id " +
						"   AND tt.class_type = ? " +
						"   AND t.status_id IN ( "+ TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + " ) ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT);
			rs = ps.executeQuery();
			if (rs.next()) {
				transactionId = rs.getLong("trx_id");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return transactionId;
	}
	
    public static boolean isBlackListBin(Connection con, long binId) throws SQLException {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	long blb = 0;
    	boolean result = false;
    	try {
    		String sql = "select transactions_manager.IS_BLACK_LIST_BIN(?) as blb from dual ";    					 
    		ps = con.prepareStatement(sql);    	
    		ps.setLong(1, binId);
    		rs = ps.executeQuery();
    		if (rs.next()){
    			blb = rs.getLong("blb");
    		}    		
    		if (blb == 1 ){
    			result = true;
    		}    		
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
    	return result;
    }

	public static HashMap<MultiKey, EpgTransactionStatus> getEpgTransactionsStatus(
			Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<MultiKey, EpgTransactionStatus> transactionMap = new HashMap<MultiKey, EpgTransactionStatus>();
		try {
			String sql = 	
						"SELECT " +
							"t.id, " +
							"cp.private_key, " +
							"cp.user_name " +
						"FROM " +
							"transactions t, " +
							"transaction_types tt, " +
							"clearing_providers cp " +
						"WHERE " +
							"t.clearing_provider_id = " + ClearingManagerConstants.EPG_CHECKOUT_PROVIDER_ID +
							"AND t.status_id = " + TransactionsManagerBase.TRANS_STATUS_AUTHENTICATE +
							"AND t.time_created < sysdate - ((1/24) * 3) " +
							"AND t.clearing_provider_id = cp.id " +
							"AND t.type_id = tt.id " +
							"AND tt.class_type = " + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT;

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				MultiKey key = new MultiKey(rs.getString("user_name"), rs.getString("private_key"));
				EpgTransactionStatus transaction = transactionMap.get(key);
				if (null == transaction) {
					transaction = new EpgTransactionStatus(rs.getString("user_name"), rs.getString("id"), 
							rs.getString("private_key"));
					transactionMap.put(key, transaction);
					continue;
				}
				transaction.addTransaction(rs.getString("id"));;				
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return transactionMap;
	}

	/**
	 * @param connection
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static String getEPGPaymentDetails(Connection connection, long id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder result = new StringBuilder();
		try {
			String sql = 	
						" SELECT " +
						" 	trr.payment_details " +
						" FROM " +
						"	transaction_request_response trr " +
						" WHERE " +
						"   trr.transaction_id = ? ";
			ps = connection.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.append(rs.getString("payment_details") + " || ");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return result.toString();
	}

	public static WithdrawFee getWithdrawFee(Connection connection, long currencyId, long transactionTypeId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " SELECT * FROM withdraw_fees WHERE currency_id = ? AND transaction_type_id = ? ";
			ps = connection.prepareStatement(sql);
			ps.setLong(1, currencyId);
			ps.setLong(2, transactionTypeId);
			rs = ps.executeQuery();
			if (rs.next()) {
				WithdrawFee fee = new WithdrawFee();
				fee.setMinAmount(rs.getLong("fee_min_amount"));
				fee.setPercentage(rs.getFloat("fee_percentage"));
				return fee;
			} else {
				return null;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	public static GmWithdrawalPermissionToDisplayResult getGmDisplayPerm(GmWithdrawalPermissionToDisplayRequest request, GmWithdrawalPermissionToDisplayResult result, Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		int indexOut = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_transactions.get_perm_display(o_direct24 => ? " + 
																				  ",o_giropay => ? " +
																				  ",o_eps => ? " + 
																				  ",o_ideal => ? " + 
																				  ",i_user_id => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			cstmt.setLong(index++, request.getUserId());

			cstmt.executeQuery();
			
			result.setDisplayDirect24(cstmt.getInt(indexOut++) > 0 ? true : false);
			result.setDisplayGiropay(cstmt.getInt(indexOut++) > 0 ? true : false);
			result.setDisplayEPS(cstmt.getInt(indexOut++) > 0 ? true : false);
			result.setDisplayIdeal(cstmt.getInt(indexOut) > 0 ? true : false);
			return result;

		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
	}
	
	public static GmWithdrawalRequest getTotalDeposits(GmWithdrawalRequest request, Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		int indexOut = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_transactions.get_total_deposits(o_total_deposits => ? " + 
																			     ",o_total_withdrawals => ? " +
																			     ",i_gm_type => ? " +
																				 ",i_user_id => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			cstmt.setLong(index++, request.getGmType());
			cstmt.setLong(index, request.getUserId());

			cstmt.executeQuery();
			
			request.setAllDepositsAmount(cstmt.getInt(indexOut++));
			request.setAllWithdrawalAmount(cstmt.getInt(indexOut++));
			return request;

		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
	}
	
	
	  public static void insertGmWithdraw(GmWithdrawalRequest request, Connection conn) throws SQLException{
			
			CallableStatement cstmt = null;
			int index = 1;
			try {
				cstmt = conn.prepareCall("{call pkg_withdraw_backend.CREATE_GM_WITHDRAW(i_user_id => ? " + 
																					 	",i_amount => ? " +
																					 	",i_writer_id => ? " +
																					 	",i_withdraw_type_id => ? " +
																					 	",i_rate => ? " +
																					 	",i_beneficiary_name => ? " +
																					 	",i_swift_bic_code => ? " +
																					 	",i_iban => ? " +
																					 	",i_account_number => ? " +
																					 	",i_bankname => ? " +
																					 	",i_branch_number => ? " +
																					 	",i_branch_address => ? " +
																					 	",i_account_info => ? " +
																					 	",i_trn_status_id => ? " +
																					 	",i_trn_failed_desc => ? " +
																					 	",i_qm_answer_id => ? " +
																					 	",i_qm_text_answer => ?" +
																					 	",i_IP => ?" +
																						",i_fee_cancel => ?)}");
	
				cstmt.setLong(index++, request.getUserId());
				cstmt.setLong(index++, request.getConvertedRequestAmount());
				cstmt.setLong(index++, request.getWriterId());
				cstmt.setLong(index++, request.getGmType());
				cstmt.setDouble(index++, request.getRate());
				cstmt.setString(index++, request.getBeneficiaryName());
				setStatementValue(request.getSwiftBicCode(), index++, cstmt);
				setStatementValue(request.getIban(), index++, cstmt);
				cstmt.setLong(index++, request.getAccountNumber());
				cstmt.setString(index++, request.getBankName());
				cstmt.setLong(index++, request.getBranchNumber());
				cstmt.setString(index++, request.getBranchAddress());
				cstmt.setString(index++, request.getAccountInfo());
				cstmt.setLong(index++, request.getTrnStatusId());
				setStatementValue(request.getTrnFailDesc(), index++, cstmt);
				setStatementValue(request.getAnswerId(), index++, cstmt);
				setStatementValue(request.getTextAnswer(), index++, cstmt);
				setStatementValue(request.getIp(), index++, cstmt);
				cstmt.setBoolean(index++, request.isExemptFee());
	
				cstmt.executeUpdate();
	
			} finally {
				closeStatement(cstmt);
			}
		}
	  
		public static long getLastTransactionsId(Connection con, long userId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				String sql =
						"SELECT " +  
	                    	"max(t.id) ID " + 
			            "FROM  " + 
			                "transactions t, " +  
			                "users u " + 
			            "WHERE  " + 
			                "t.user_id = u.id AND  " +  
			                "t.user_id = ?  ";
				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);

				rs = ps.executeQuery();

				if (rs.next()) {
					return rs.getLong("ID");
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return 0;
		}
		
	    public static ArrayList<Transaction> getByUserAndStatus(Connection con,long userId,long transactionId,long classType, String statuses, String order) throws SQLException {

	    	  PreparedStatement ps=null;
	    	  ResultSet rs=null;
	    	  ArrayList<Transaction> list=new ArrayList<Transaction>();

	    	  try
	    		{
	    		    String sql= " select " +
	    		    		   		" t.*," +
	    		    		   		" p.description typename, " +
	    		    		   		" p.class_type classtype, " +
	    		    		   		" u.currency_id currency_id " +
	    		    		    " from " +
	    		    		   		" transactions t, " +
	    		    		   		" transaction_types p, " +
	    		    		   		" users u " +
	    		    		    " where " +
	    		    		    	" t.user_id = u.id " +
	    		    		    	" and t.status_id in ("+ statuses + ") " +
	    		    		    	" and t.type_id=p.id " +
	    		    		    	" and p.class_type = ? ";
	    		    if (userId > 0){
	    		    	sql += 	    " and t.user_id = ? ";
	    		    }
	    		    if (transactionId > 0){
	    		    	 sql +=     " and t.id = ? ";
	    		    }
	    		    sql +=
	    		    		    " order by " +
	    		    		    	" t.time_created " + order;

	    			ps = con.prepareStatement(sql);
	    			ps.setLong(1,classType);

	    		    if (userId > 0){
	    		    	ps.setLong(2, userId);
	    		    }
	    		    if (transactionId > 0){
	    		    	ps.setLong(2, transactionId);
	    		    }
	    			
	    			rs=ps.executeQuery();

	    			while (rs.next()) {
	    				    list.add(getVO(con,rs));
	    			}
	    		}

	    		finally
	    		{
	    			closeResultSet(rs);
	    			closeStatement(ps);
	    		}
	    		return list;

	      }
}

