package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;

public class UrlsDAO extends DAOBase {


	/**
	 * Get url id by the url string
	 * @param con
	 * 		connection to Db
	 * @param url
	 * @return
	 * 		url id
	 * @throws SQLException
	 */
	public static int getIdByUrl(Connection con, String url) throws SQLException {

		Statement ps = null;
		ResultSet rs = null;
		int id = 0;

		try
		{
		    String sql = "select id from urls "+
		    		     "where url like '%"+ url +"' ";

		    ps = con.createStatement();
			rs = ps.executeQuery(sql);

			if ( rs.next() ) {  //found url
				id = rs.getInt("id");
			}

		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}

		return id;
	}

	/**
	 * get our domains list
	 * @param con
	 * @return ArrayList<String> with our domains
	 * @throws SQLException
	 */
	public static ArrayList<String> getOurDomains(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> domains = new ArrayList<String>();
		try
		{
		    String sql = "SELECT " +
		    			 "	url_name " +
		    		     "FROM " +
		    		     "	urls ";

		    ps = con.prepareStatement(sql);
		    rs = ps.executeQuery();

			while (rs.next()) {
				domains.add(rs.getString("url_name"));
			}

		} finally {
			closeResultSet(rs);
	        closeStatement(ps);
		}

		return domains;
	}
}