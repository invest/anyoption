package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.anyoption.common.beans.Job;

public class JobsDAOBase extends DAOBase {
    /**
     * Load and lock job for run. The job should not be running and the time for the next run should have come.
     * 
     * @param conn
     * @param id
     * @param serverId
     * @param application
     * @return <code>Job</code> to be run or <code>null</code> if the job is currently running or it is not time for next execution yet.
     * @throws SQLException
     */
    public static Job getByIdWithLock(Connection conn, long id, String serverId, String application) throws SQLException {
        String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "jobs " +
                "WHERE " +
                    "id = ? " +
                    "AND last_run_time + run_interval * 1 / 1440 < sysdate " +
                    "AND (running = 0 OR server = ?) " +
                    "AND run_servers like ? " +
                    "AND run_applications like ? " +
                "FOR UPDATE";
        Job job = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            pstmt.setString(2, serverId);
            pstmt.setString(3, "%" + serverId + "%");
            pstmt.setString(4, "%" + application + "%");
            rs = pstmt.executeQuery();
            if (rs.next()) {
                job = getVO(rs);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return job;
    }
    
    private static Job getVO(ResultSet rs) throws SQLException {
        Job job = new Job();
        job.setId(rs.getLong("id"));
        job.setRunning(rs.getInt("running"));
        job.setLastRunTime(convertToDate(rs.getTimestamp("last_run_time")));
        job.setServer(rs.getString("server"));
        job.setRunInterval(rs.getInt("run_interval"));
        job.setDescription(rs.getString("description"));
        job.setJobClass(rs.getString("job_class"));
        job.setConfig(rs.getString("config"));
        return job;
    }
    
    public static int setRunningById(Connection conn, long id, int running, String serverId, boolean updateRunTime) throws SQLException {
        String sql =
                "UPDATE " +
                    "jobs " +
                "SET " +
                    "running = ?, " +
                    "server = ? " +
                    // add to the last_run_time X intervals so if the interval is for example 1 day it will still be running every day in that time even if the servers
                    // were down long time and the job runs on server startup (eg: last run was 3 days ago 1am and interval is 1 day and we start the servers at 10am we
                    // want tomorrow to run it again at 1am not at 10 am)
                    (running == Job.RUNNING_NO && updateRunTime ? ", last_run_time = last_run_time + (floor(((sysdate - last_run_time) / (1 / 1440)) / run_interval) * 1 / 1440 * run_interval) " : "") +
                "WHERE " +
                    "id = ?";
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, running);
            pstmt.setString(2, serverId);
            pstmt.setLong(3, id);
            return pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
    
    public static List<Long> getJobsIds(Connection conn) throws SQLException {
        String sql =
                "SELECT " +
                    "id " +
                "FROM " +
                    "jobs";
        List<Long> jobsIds = new LinkedList<Long>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                jobsIds.add(rs.getLong("id"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return jobsIds;
    }
}