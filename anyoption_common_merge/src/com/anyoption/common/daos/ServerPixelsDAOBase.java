package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.beans.ServerPixel;
import com.anyoption.common.managers.ServerPixelsManagerBase;

public class ServerPixelsDAOBase extends DAOBase {

	/**
	 * Insert into server pixels
	 * @param con
	 * @throws SQLException
	 */
	public static void insert(Connection con, ServerPixel serverPixel) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql= "INSERT INTO SERVER_PIXELS " +
						"( " +
							"time_created , " +
							"refrence_id , " +
							"server_pixel_type , " +
							"id, " +
							"server_pixels_publishers_id, " +
							"pixel " +
						") " +
						"VALUES " +
						"( " +
							"sysdate, " +
							"?, " +
							"?, " +
							"seq_server_pixels.nextval, " +
							"?, " +
							"? " +
						")";
			ps = con.prepareStatement(sql);
			ps.setLong(1, serverPixel.getRefrenceId());
			ps.setInt(2, serverPixel.getServerPixelType());
			ps.setInt(3, serverPixel.getServerPixelsPublisherId());
			ps.setString(4, serverPixel.getPixel());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * The function check if 'device unique id' has open app pixel in the last X days.
	 * @param con
	 * @param publisherId
	 * @param duid
	 * @param numOfDays
	 * @return true if exists
	 * @throws SQLException
	 */
	public static boolean isHasOpenAppPixel(Connection con, long publisherId, String duid, long numOfDays) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = " SELECT " +
							" sp.id " +
						" FROM " +
							" server_pixels sp " +
						" WHERE " +
							" sp.server_pixel_type = ? " +
							" and sp.time_created > sysdate - ? " + 
							" and sp.server_pixels_publishers_id = ? " + 
							" and sp.refrence_id = (SELECT " +
														" duis.id " +
													" FROM " +
														" device_unique_ids_skin duis " +
													" WHERE " +
														" duis.device_unique_id = ?) ";
			ps = con.prepareStatement(sql);
			ps.setInt(index++, ServerPixelsManagerBase.TYPE_OPEN_APP);
			ps.setLong(index++, numOfDays);
			ps.setLong(index++, publisherId);
			ps.setString(index++, duid);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}
}