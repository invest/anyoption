package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.managers.IssuesManagerBase;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author kirilim
 */
public class IssuesDAOBase extends DAOBase {
	
	private static final Logger logger = Logger.getLogger(IssuesDAOBase.class);

	public static void insertIssue(Connection con, Issue vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "INSERT INTO issues"
					+ "(id, user_id, subject_id, priority_id, status_id, population_entry_id, time_created, type, credit_card_id, contact_id, component_id) "
					+ "VALUES" + "(seq_issues.nextval,?,?,?,?,?,sysdate,?,?,?,?) ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, vo.getUserId());
			ps.setLong(2, Long.parseLong(vo.getSubjectId()));
			ps.setLong(3, Long.parseLong(vo.getPriorityId()));
			ps.setLong(4, Long.parseLong(vo.getStatusId()));

			long popEntryId = vo.getPopulationEntryId();
			if (popEntryId != 0) {
				ps.setLong(5, popEntryId);
			} else {
				ps.setString(5, null);
			}
			if (vo.getType() == 0) {
				ps.setInt(6, ConstantsBase.ISSUE_TYPE_REGULAR);
			} else {
				ps.setInt(6, vo.getType());
			}
			if (vo.getCreditCardId() != 0) {
				ps.setLong(7, vo.getCreditCardId());
			} else {
				ps.setString(7, null);
			}
			ps.setLong(8, vo.getContactId());
			ps.setInt(9, vo.getComponentId());
			ps.executeUpdate();
			vo.setId(getSeqCurValue(con, "seq_issues"));
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}
	
	public static void insertAction(Connection con, IssueAction vo)	throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			String sql = "INSERT INTO issue_actions"
					+ "(id, issue_id, issue_action_type_id, writer_id, action_time, action_time_offset, "
					+ "comments, is_significant,direction_id, callback_time, "
					+ "callback_time_offset, transaction_id, marketing_operation_id, chat_id, risk_reason_id, sms_id, template_id) "
					+ "VALUES"
					+ "(SEQ_ISSUE_ACTIONS.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";

			ps = con.prepareStatement(sql);

			ps.setLong(1, vo.getIssueId());
			ps.setLong(2, Long.parseLong(vo.getActionTypeId()));
			ps.setLong(3, vo.getWriterId());
			ps.setTimestamp(4, convertToTimestamp(vo.getActionTime()));
			ps.setString(5, vo.getActionTimeOffset());
			ps.setString(6, vo.getComments());
			ps.setInt(7, (vo.isSignificant()) ? 1 : 0);

			if (vo.getCallDirectionId() > 0) {
				ps.setLong(8, vo.getCallDirectionId());
			} else {
				ps.setString(8, null);
			}

			// If it's a call back action that was created from retention
			if (vo.isCallBack() && vo.getCallBackTimeOffset() != null) {
				ps.setTimestamp(9, convertToTimestamp(CommonUtil.getDateTimeFormat(vo.getCallBackDateAndTime(), vo.getCallBackTimeOffset())));
				ps.setString(10, vo.getCallBackTimeOffset());
			} else {
				ps.setString(9, null);
				ps.setString(10, null);
			}
			ps.setLong(11, vo.getTransactionId());
			ps.setLong(12, Long.valueOf(vo.getMarketingOperationId()));

			if (vo.getChatId() != 0) {
				ps.setLong(13, vo.getChatId());
			} else {
				ps.setString(13, null);
			}

			if (!CommonUtil.isParameterEmptyOrNull(vo.getRiskReasonId())) {
				ps.setLong(14, Long.valueOf(vo.getRiskReasonId()));
			} else {
				ps.setString(14, null);
			}

			if (null != vo.getChannelId() && vo.getChannelId().equals(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_SMS))) {
				ps.setLong(15, vo.getSmsId());
			} else if (null != vo.getChannelId()) {
				ps.setLong(15, Long.parseLong(vo.getChannelId()));
			} else {
				ps.setString(15, null);
			}
			ps.setLong(16, vo.getTemplateId());
			ps.executeUpdate();
			vo.setActionId(getSeqCurValue(con, "SEQ_ISSUE_ACTIONS"));
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	public static void updateIssue(Connection con,Issue vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql="update issues " +
					"set subject_id=?, priority_id=?, status_id=? " +
					"where id=?";

			ps = con.prepareStatement(sql);
			ps.setLong(1, Long.parseLong(vo.getSubjectId()));
			ps.setLong(2, Long.parseLong(vo.getPriorityId()));
			ps.setLong(3, Long.parseLong(vo.getStatusId()));
			ps.setLong(4,vo.getId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	public static Issue getIssueForUpdateCopyopUserStatus(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Issue vo = null;
		try {
			String sql = " SELECT * " +
		  				   " FROM issues i " +
						   " WHERE user_id = ? and subject_id = " + Issue.ISSUE_SUBJECTS_COPYOP_REMOVE +
						   			" and status_id = " + IssuesManagerBase.RISK_STATUS_NEW_ISSUE +
						   " ORDER BY id desc";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				vo = new Issue();
				vo.setId(rs.getLong("id"));
				vo.setUserId(rs.getLong("user_id"));
				vo.setSubjectId(rs.getString("subject_id"));
				vo.setPriorityId(rs.getString("priority_id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return vo;
	}
	
	public static Issue getUserIssueByIssueActionType(Connection con, long userId, List<Integer> issueActionTypes) throws SQLException {

		if (issueActionTypes == null || issueActionTypes.isEmpty()) {
			logger.info("Issue action type list is empty OR null");
			throw new NullPointerException();
		}
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Issue vo = null;
		try {
			String sql = " SELECT distinct (i.id), i.user_id, i.subject_id, i.status_id "
					+ " FROM issues i, issue_actions ia "
					+ " WHERE i.id = ia.issue_id "
					+ " AND i.user_id = ? "
					+ " AND ia.issue_action_type_id IN ( ";

			for (int i = 0; i < issueActionTypes.size(); i++) {
				sql += "?,";
			}
			
			sql = sql.substring(0, sql.length() - 1);
			sql += " )";

			ps = con.prepareStatement(sql);

			int index = 1;
			ps.setLong(index++, userId);

			Iterator<Integer> iterator = issueActionTypes.iterator();
			
			while (iterator.hasNext()) {
				int issueActionType = iterator.next();
				ps.setInt(index++, issueActionType);
			}

			rs = ps.executeQuery();
			if (rs.next()) {
				vo = new Issue();
				vo.setId(rs.getLong("id"));
				vo.setUserId(rs.getLong("user_id"));
				vo.setSubjectId(rs.getString("subject_id"));
				vo.setStatusId(rs.getString("status_id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return vo;
	}
	
// START ET REGULATION
	public static Issue getUserRegIssue(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Issue vo = null;
		try {
			String sql = " SELECT distinct (i.id), i.user_id, i.subject_id, i.status_id " +
						  " FROM issues i, issue_actions ia " +
							" WHERE i.id = ia.issue_id " +
							" AND i.user_id = ? " +
							" AND ia.issue_action_type_id IN ( " + IssuesManagerBase.ISSUE_ACTION_TYPE_BINARY_QUESTION_NO + " , " + IssuesManagerBase.ISSUE_ACTION_TYPE_BINARY_QUESTION_YES + " )";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				vo = new Issue();
				vo.setId(rs.getLong("id"));
				vo.setUserId(rs.getLong("user_id"));
				vo.setSubjectId(rs.getString("subject_id"));
				vo.setStatusId(rs.getString("status_id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return vo;
	}
	
	public static Issue getUserRegBlockedIssue(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Issue vo = null;
		try {
			String sql = " SELECT distinct (i.id), i.user_id, i.subject_id, i.status_id " +
						  " FROM issues i, issue_actions ia " +
							" WHERE i.id = ia.issue_id " +
							" AND i.user_id = ? " +
							" AND ia.issue_action_type_id = " + IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_LOSS_THRESHOLD_SUSPEND;
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				vo = new Issue();
				vo.setId(rs.getLong("id"));
				vo.setUserId(rs.getLong("user_id"));
				vo.setSubjectId(rs.getString("subject_id"));
				vo.setStatusId(rs.getString("status_id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return vo;
	}
	
	public static boolean isKnowledgeQuestionAnswerNO(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean isNO = true;
		try {
			String sql = " SELECT is_knowledge_question "+ 
							" FROM users_regulation " + 
							" WHERE user_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt("is_knowledge_question") == ConstantsBase.BO_KNOWLEDGE_QUESTION_ANSWER_IS_YES) {
					isNO = false;
				}
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return isNO;
	}
	
	public static void updateKnowledgeQuestionAnswer(Connection con, long userId, boolean isYesClicked) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			String sql = " UPDATE users_regulation " +
					 		" SET is_knowledge_question=? " +
					 		" WHERE user_id=?";

			ps = con.prepareStatement(sql);
			if (isYesClicked) {
				ps.setLong(1, ConstantsBase.BO_KNOWLEDGE_QUESTION_ANSWER_IS_YES);
			} else {
				ps.setLong(1, ConstantsBase.BO_KNOWLEDGE_QUESTION_ANSWER_IS_NO);
			}
			ps.setLong(2, userId);

			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}
	
	public static boolean isUserUnrestricted(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean isUserUnrestricted = false;
		try {
			String sql = "SELECT is_knowledge_question " +
						 "FROM users_regulation " +
					 	 "WHERE user_id=? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);

			rs = ps.executeQuery();
			if(rs.next()){
				if(rs.getInt("is_knowledge_question") == ConstantsBase.AO_REGULATION_USER_UNRESTRICTED){
					isUserUnrestricted = true;
				}
			}
			return isUserUnrestricted;
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}
	
	public static boolean insertLinkIssueAction(Connection conn, Issue issue, long userId, String comments, String issueActionTypeId, String channelId, long suspendedId, String priorityId)throws SQLException{
		try {
			IssueAction issueAction = new IssueAction();
			// Filling action fields
			issueAction.setActionTypeId(issueActionTypeId);
			issueAction.setWriterId(Writer.WRITER_ID_AUTO);
			issueAction.setSignificant(false);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(ConstantsBase.OFFSET_GMT);
			issueAction.setComments(comments);
			issueAction.setIssueId(issue.getId());
			issueAction.setChannelId(channelId);
			issue.setPriorityId(priorityId);
			
			IssuesDAOBase.insertAction(conn, issueAction);
			IssuesDAOBase.updateIssue(conn, issue);
			if(issueActionTypeId.equals(String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_ACTIVATION))) {
				try {
					UsersRegulationDAOBase.unsuspendUser(conn, userId, suspendedId);
				} catch (SQLException e) {
					logger.error("Could NOT unsuspend user with id: " + userId, e);
					return false;
				}
			}
			logger.info("Successfully insert action for issue with id " + issue.getId());
		} catch (SQLException e) {
			logger.error("Couldn't insert activation action for issue with id: "+ issue.getId(),e);
			return false;
		}
		return true;
	}
//// END ET REGULATION
	
	
	public static Issue getIssueIdForBlankTemplate(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Issue vo = null;
		try {
			String sql = "SELECT MAX(ID) as issue_id "+
					  		"FROM issues "+
					  	   "WHERE population_entry_id = "+
					        "(SELECT MAX(population_entry_id) FROM issues WHERE user_id = ?) "+
					       "AND user_id = ?";
			
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				vo = new Issue();
				vo.setId(rs.getLong("issue_id"));
				vo.setUserId(userId);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return vo;
	}
	
	  /**
	   * get issue by creditCard id
	   * @param con db connection
	   * @param creditCardid the credit card id of the issue
	   * @return Issue instance
	   * @throws SQLException
	   */
	  public static long getIssueIdByCreditCardId(Connection con,long creditCardId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  long issueId = 0;
		  try {
			  String sql = " SELECT id " +
		  				   " FROM issues i " +
						   " WHERE i.credit_card_id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, creditCardId);

				rs = ps.executeQuery();

				if (rs.next()) {
					issueId = rs.getLong("id");
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return issueId;
	  }
	  
      public static boolean isPendingDocIssuesForUser(Connection con, long userId) throws SQLException {
    		 
  		PreparedStatement stmnt = null;
  	     ResultSet rs = null;
  	     boolean result = false;
  	     try {
  	    	 String sql = "SELECT " +
  	    	 						" count(id) c " +
  	 					  " FROM " +
  	 								" issues i " +
  	    	 		      " WHERE " +
  	    	 		      			" subject_id = " + IssuesManagerBase.ISSUE_SUBJECT_REG_DOC +
  	    	 		      " AND " +
  	    	 		      			" user_id = " + userId;
  	    	 
  	    	 stmnt = con.prepareStatement(sql);
               rs = stmnt.executeQuery();
               if(rs.next()) {
                   int size = rs.getInt("c");
                   if(size >= 1) {
                  	 result = true;
                   }
               }
               
  	     }finally {
  	    	 closeStatement(stmnt);
               closeResultSet(rs);
  	     }
  		return result;
  	}
      
	  public static boolean InsertIssueRiskDocReq(Connection con, long issueActionId, String type, long ccId) throws SQLException {
	  	PreparedStatement ps = null;
	      ResultSet rs = null;
	      try {
	            String sql =" INSERT INTO " +
	                        " issue_risk_doc_req(ID, ISSUE_ACTION_ID, DOC_TYPE, CC_ID) " +
	                    " VALUES " +
	                        " (seq_issue_risk_doc_req.nextval,?,?,?) ";
	            
	            ps = con.prepareStatement(sql);
	
	            ps.setLong(1, issueActionId);
	            ps.setString(2, type);
	            if (ccId != 0){
	                ps.setLong(3, ccId);
	            } else {
	                ps.setString(3, null);
	            }
	
	            ps.executeUpdate();
	            
	      } finally {
	            closeStatement(ps);
	            closeResultSet(rs);
	        }
	      return true;
	  }
	  
	  public static long getPendingDocsIssueId (Connection con, long userId) throws SQLException {
		PreparedStatement stmnt = null;
		ResultSet rs = null;
		long result = 0;
		try {
			String sql = "SELECT " +
  	    	 						" id " +
  	 					  " FROM " +
  	 								" issues i " +
  	    	 		      " WHERE " +
  	    	 		      			" subject_id = " + IssuesManagerBase.ISSUE_SUBJECT_REG_DOC +
  	    	 		      " AND " +
  	    	 		      			" user_id = ? " +
	  		  			  " AND " +
  	    	 		      			" status_id = ? " + 
                          " AND " + 
  	    	 		      			" component_id = ? " +
                          " ORDER BY " +  
  	    	 		      			" TIME_CREATED DESC ";
	  	   
	  		int index = 1;
			stmnt = con.prepareStatement(sql);
			stmnt.setLong(index++, userId);
			stmnt.setLong(index++, ConstantsBase.ISSUE_STATUS_PENDING);
			stmnt.setInt(index++, ConstantsBase.ISSUE_COMPONENT_ETRADER_ANYOPTION);
			rs = stmnt.executeQuery();
			if (rs.next()) {
				result = rs.getLong("id");
			}
		} finally {
			closeStatement(stmnt);
			closeResultSet(rs);
		}
		return result;
	  }
}