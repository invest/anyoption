package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.anyoption.common.bl_vos.ChargeBack;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

public class ChargeBacksDAO extends DAOBase {
	
	  public static void insert(Connection con,ChargeBack vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {

				String sql=" insert into charge_backs(id," +
													" writer_id, " +
													" status_id, " +
													" TIME_created, " +
													" description, " +
													" utc_offset_created," +
													" ISSUE_ID, " +
													" ARN, " +
													" TIME_POS, " +
													" TIME_CHB, " +
													" TIME_REP, " +
													" TIME_SETTLED," +
													" STATE) " +
						   " values(seq_charge_backs.nextval,?,?,?,?,?,?,?,?,?,?,?,?) ";

				ps = con.prepareStatement(sql);

				ps.setLong(1, vo.getWriterId());
				ps.setLong(2, Long.valueOf(vo.getStatusId()).longValue());
				ps.setTimestamp(3, CommonUtil.convertToTimeStamp(vo.getTimeCreated()));
				ps.setString(4,vo.getDescription());
				ps.setString(5,vo.getUtcOffsetCreated());
				ps.setLong(6, vo.getIssueId());
				ps.setString(7, vo.getArn());
				ps.setTimestamp(8, CommonUtil.convertToTimeStamp(vo.getTimePos()));
				ps.setTimestamp(9, CommonUtil.convertToTimeStamp(vo.getTimeChb()));
				ps.setTimestamp(10, CommonUtil.convertToTimeStamp(vo.getTimeRep()));
				ps.setTimestamp(11, CommonUtil.convertToTimeStamp(vo.getTimeSettled()));
				ps.setInt(12, vo.getState());

				ps.executeUpdate();
				vo.setId(getSeqCurValue(con,"seq_charge_backs"));
		  }
			finally
			{
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }

	  public static void update(Connection con,ChargeBack vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql=" update charge_backs " +
						   " set STATUS_ID = ?, " +
						       " TIME_CHB = ?, " +
							   " TIME_REP = ?, " +
							   " TIME_SETTLED = ?," +
							   " STATE = ?," +
							   " TIME_STATE_CHANGED = ? " +
						   " where id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, vo.getStatusId());
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(vo.getTimeChb()));
				ps.setTimestamp(3, CommonUtil.convertToTimeStamp(vo.getTimeRep()));
				ps.setTimestamp(4, CommonUtil.convertToTimeStamp(vo.getTimeSettled()));
				ps.setInt(5, vo.getState());
				ps.setTimestamp(6, CommonUtil.convertToTimeStamp(vo.getTimeStateChanged()));
				ps.setLong(7, vo.getId());

				ps.executeUpdate();

		  }finally {
				closeStatement(ps);
				closeResultSet(rs);
		  }

	  }

	  public static ArrayList<ChargeBack> search(Connection con,long id,long statusId,
			  String userName, long classId, String tranId, String arn, int state, long currencyId, long providerId) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  ArrayList<ChargeBack> list=new ArrayList<ChargeBack>();

			  ChargeBack vo=null;
			  try{

				 String sql=" Select " +
				 				" cb.*," +
				 				" w.user_name writer," +
				 				" t.amount amount," +
				 				" t.id tranid," +
				 				" t.user_id userid," +
				 				" t.is_3d, " +
				 				" u.user_name username," +
				 				" u.first_name firstname," +
				 				" u.last_name lastname," +
				 				" u.currency_id," +
				 				" cp.name provider_name, " +
				 				" cbs.name status "+
				 			" from " +
				 				" transactions t left join clearing_providers cp on t.clearing_provider_id=cp.id," +
				 				" charge_backs cb, " +
				 				" charge_back_statuses cbs, " +
				 				" writers w, " +
				 				" users u " +
				 			" where " +
				 				" t.charge_back_id is not null " +
				 				" and t.charge_back_id = cb.id " +
				 				" and cb.writer_id = w.id " +
				 				" and t.user_id = u.id " +
				 				" and cbs.id = cb.status_id ";

				  if (id>0) {
					  sql+= 	" and t.user_id =" + id + " ";
				  }

				  if ( !CommonUtil.isParameterEmptyOrNull(userName)) {
					  sql+= " and u.user_name like '%" + userName.toUpperCase() + "%'";
				  }

				  if (statusId > 0){
					  sql+= " and cb.status_id = " + statusId;
				  }

				  if (state > 0){
					  sql+= " and cb.state = " + state;
				  }

				  if (!CommonUtil.isParameterEmptyOrNull(tranId)){
					  sql+= " and t.id = " + tranId;
				  }

				  if (!CommonUtil.isParameterEmptyOrNull(arn)){
					  sql+= " and cb.arn = " + arn;
				  }

				  if (currencyId > 0 ){
					  sql+= " and u.currency_id = " + currencyId;
				  }

				  if (providerId > 0 ){
					  sql+= " and t.clearing_provider_id = " + providerId;
				  }

				  if ( classId != ConstantsBase.USER_CLASS_ALL )   {

				  		if ( classId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
				  			sql+=" and (u.class_id="+ConstantsBase.USER_CLASS_PRIVATE+" "+
				  				 " or u.class_id="+ConstantsBase.USER_CLASS_COMPANY+")";
				  		}
				  		else {
				  				sql += " and u.class_id = "+classId + " ";
				  		}
				  }

				 sql += " order by " +
				 			" cb.time_created desc ";

				 ps = con.prepareStatement(sql);
				 rs = ps.executeQuery();

				while (rs.next()) {
					vo=getVO(rs);
					vo.setThreeD(rs.getLong("is_3d") == 1 ? true : false);
					vo.setWriter(rs.getString("writer"));
					vo.setAmount(rs.getLong("amount"));
					vo.setTransactionId(rs.getLong("tranid"));
					vo.setUserId(rs.getLong("userid"));
					vo.setUserName(rs.getString("username"));
					vo.setUserFirstName(rs.getString("firstname"));
					vo.setUserLastName(rs.getString("lastname"));
					vo.setCurrencyId(rs.getLong("currency_id"));
					vo.setStatusName(rs.getString("status"));
					vo.setClearingProviderName(rs.getString("provider_name"));
					list.add(vo);
				}
			}finally{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	  }

	  public static ChargeBack get(Connection con,long id) throws SQLException {
		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  try {
			  String sql = " select " +
			  					" cb.*, " +
			  					" w.user_name writer, " +
			  					" cbs.name status " +
		  				   " from " +
		  				   		" charge_backs cb, " +
		  				   		" charge_back_statuses cbs, " +
		  				   		" writers w "+
		  				   " where " +
		  				   		" cb.id =? " +
		  				   		" and cb.writer_id = w.id " +
		  				   		" and cbs.id = cb.status_id ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, id);
				rs = ps.executeQuery();
				if (rs.next()) {
					ChargeBack cb=getVO(rs);
					cb.setStatusName(rs.getString("status"));
					cb.setWriter(rs.getString("writer"));
					return cb;
				}
			}
			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return null;
	  }

	  public static boolean checkChargeBacks(Connection con,long userId) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  try
			{
			    String sql=" select * " +
			    		   " from   transactions " +
			    		   " where user_id=? " +
			    		   	 " and charge_back_id is not null ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);

				rs=ps.executeQuery();

				if (rs.next()) {
					return true;
				}
				return false;
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}

	  }

	  public static HashMap<Long,ChargeBack> getAllStatuses(Connection con) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  HashMap<Long,ChargeBack> hm = new HashMap<Long, ChargeBack>();

		  try
			{
			    String sql=" select " +
			    		   		" * " +
			    		   " from " +
			    		   		" charge_back_statuses " +
			    		   " where " +
			    		   		" id not in (?, ?, ?) " +
			    		   " order by name ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, ChargeBack.CHARGEBACK_STATUS_STARTED);
				ps.setLong(2, ChargeBack.CHARGEBACK_STATUS_POS);
				ps.setLong(3, ChargeBack.CHARGEBACK_STATUS_CLOSED);
				rs=ps.executeQuery();
				while (rs.next()) {
					ChargeBack vo = new ChargeBack();
					vo.setStatusId(rs.getLong("id"));
					vo.setStatusName(rs.getString("name"));
					hm.put(rs.getLong("id"), vo);
				}
				return hm;
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}

	  }


	  private static ChargeBack getVO(ResultSet rs) throws SQLException{
			ChargeBack vo=new ChargeBack();

			vo.setDescription(rs.getString("DESCRIPTION"));
			vo.setId(rs.getLong("ID"));
			vo.setWriterId(rs.getLong("WRITER_ID"));
			vo.setStatusId(rs.getLong("STATUS_ID"));
			vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
			vo.setUtcOffsetCreated(rs.getString("UTC_OFFSET_CREATED"));
			vo.setArn(rs.getString("ARN"));
			vo.setTimePos(convertToDate(rs.getTimestamp("TIME_POS")));
			vo.setTimeChb(convertToDate(rs.getTimestamp("TIME_CHB")));
			vo.setTimeRep(convertToDate(rs.getTimestamp("TIME_REP")));
			vo.setTimeSettled(convertToDate(rs.getTimestamp("TIME_SETTLED")));
			vo.setIssueId(rs.getLong("ISSUE_ID"));
			vo.setState(rs.getInt("STATE"));
			vo.setTimeStateChanged(convertToDate(rs.getTimestamp("TIME_STATE_CHANGED")));
			return vo;
	  }

	    public static ChargeBack getById(Connection conn, long id) throws SQLException {
	        PreparedStatement pstmt = null;
	        ResultSet rs = null;
	        try {
	            String sql = 
	                "SELECT " +
	                    "c.*, " +
	                    "w.user_name writer, " +
	                    "cs.name status_name " +
	                "FROM " +
	                    "charge_backs c, " +
	                    "writers w, "+
	                    "charge_back_statuses cs " +
	                "WHERE " +
	                    "c.id = ? AND " +
	                    "c.writer_id = w.id AND " +
	                    "c.status_id = cs.id";
	            pstmt = conn.prepareStatement(sql);
	            pstmt.setLong(1, id);
	            rs = pstmt.executeQuery();
	            if (rs.next()) {
	                ChargeBack cb = getVO(rs);
	                cb.setWriter(rs.getString("writer"));
	                cb.setStatusName(rs.getString("status_name"));
	                return cb;
	            }
	        } finally {
	            closeResultSet(rs);
	            closeStatement(pstmt);
	        }
	        return null;
	    }
}