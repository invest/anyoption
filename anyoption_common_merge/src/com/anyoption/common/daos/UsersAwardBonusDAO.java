package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.beans.UsersAwardBonus;

public class UsersAwardBonusDAO extends DAOBase{
	  public static boolean insert(Connection con, UsersAwardBonus vo) throws SQLException {
		  PreparedStatement ps = null;
		  boolean isInsert = false;
		  try {

				String sql = "INSERT " +
							 "INTO users_award_bonus " + 
							 "( " +
							      "is_bonus_granted, " + 
							      "user_id, " +
							      "users_award_bonus_groups_id, " + 
							      "id " +
							 ") " +
			                 "SELECT " +
			                 	"?, " +
			                 	"?, " +
			                 	"?, " +
			                 	"seq_users_award_bonus.nextVal " +
			                 "FROM " +
			                 	"DUAL " +
			                 "WHERE " +
			                 	"NOT EXISTS (" +
			                 					"SELECT " + 
			                 						"1 " + 
                                                "FROM " +
                                                	"users_award_bonus uab, " + 
                                                	"users_award_bonus_groups uabg " +
                                                "WHERE " +
                                                	"uab.user_id = ? AND " +
                                                	"uab.users_award_bonus_groups_id = uabg.id AND " +
                                                	"TRUNC(uabg.time_created) = TRUNC(sysdate) AND " +
                                                	"uabg.bonus_id = ? " +
                                 ") ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, (vo.isBonusGranted() ? 1 : 0));
				ps.setLong(2, vo.getUserId());
				ps.setLong(3, vo.getUsersAwardBonusGroup().getId());
				ps.setLong(4, vo.getUserId());
				ps.setLong(5, vo.getUsersAwardBonusGroup().getBonusId());
				int numbersOfRows = ps.executeUpdate();
				if (numbersOfRows > 0) {
					isInsert = true;
				}
				return isInsert;
		  } finally {
			  closeStatement(ps);
		  }
	  }
	  
	  /**
	   * update Users Award Bonus to Granted
	   * @param con 
	   * @param id the id of the Users Award Bonus
	   * @throws SQLException
	   */
	  public static void updateBonusGranted(Connection con, long id) throws SQLException {
			PreparedStatement ps = null;
			
			try {
				String sql = 	"UPDATE " +
									" users_award_bonus " +
								"SET " +
									" is_bonus_granted = 1 " +
								"WHERE " +
									" id = ? ";

				ps =  con.prepareStatement(sql);
				ps.setLong(1, id);
				ps.executeUpdate();

			} finally {
				closeStatement(ps);
			}
	  }
	  
	  
	  public static UsersAwardBonus getByUserId(Connection con, long userId) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			UsersAwardBonus usersAwardBonus = null;

			try {

				String sql = "SELECT " + 
								  "uab.*, " +
								  "uabg.time_created, " +
							      "uabg.description, " +
							      "uabg.redeem_until_date, " +
							      "uabg.id as uabg_id, " +
							      "uabg.bonus_id " +
							  "FROM " +
								  "users_award_bonus uab, " + 
								  "users_award_bonus_groups uabg " +
							  "WHERE " +
								  "uab.user_id = ? AND " +
								  "uab.is_bonus_granted = 0 AND " +
								  "uab.users_award_bonus_groups_id = uabg.id AND " +
								  "uabg.time_created <= SYSDATE AND " +
								  "uabg.redeem_until_date >= SYSDATE";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);

				rs = ps.executeQuery();

				if (rs.next()) {
					usersAwardBonus = getVO(rs);
					usersAwardBonus.setUsersAwardBonusGroup(
							UsersAwardBonusGroupDAO.getVO(rs, 
									usersAwardBonus.getUsersAwardBonusGroup(), "uabg_id"));
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}

			return usersAwardBonus;

		}

	private static UsersAwardBonus getVO(ResultSet rs) throws SQLException {
		UsersAwardBonus usersAwardBonus = new UsersAwardBonus();
		usersAwardBonus.setId(rs.getLong("id"));
		usersAwardBonus.setUserId(rs.getLong("user_id"));
		usersAwardBonus.setBonusGranted(rs.getLong("is_bonus_granted") == 1 ? true : false);
		usersAwardBonus.getUsersAwardBonusGroup().setId(rs.getLong("users_award_bonus_groups_id"));
		return usersAwardBonus;
	}

}
