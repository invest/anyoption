/**
 * 
 */
package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import com.anyoption.common.beans.UserActiveData;

/**
 * User specific DAO class for his balance steps
 * 
 * @author kirilim
 */
public class UserBalanceStepDAO extends BalanceStepsDAOBase {

	public static UserActiveData getUserActiveData(Connection con, long userId, long currencyId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		UserActiveData uad = null;

		try {
			String sql= "select uad.tx_sum/uad.tx_cnt tx_avg,uad.inv_sum/uad.inv_cnt inv_avg,uad.user_id, uad.def_inv_amount,uad.custom_amount, uad.custom_step_id, ubs.*, pag.amount, pag.step_priority"
						+ " from users_active_data uad"
						+ " left join users_balance_steps ubs on (uad.tx_sum/uad.tx_cnt between ubs.amount_from and ubs.amount_to and uad.custom_step_id is null) or( uad.custom_step_id is not null and  uad.custom_step_id = ubs.id)"
						+ " left join predefined_amount_groups pag on pag.users_balance_step_id = decode(uad.custom_step_id,null,ubs.id,uad.custom_step_id)"
						+ " WHERE ubs.CURRENCY_ID = ? AND uad.user_id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, currencyId);
			ps.setLong(2, userId);			
			rs = ps.executeQuery();
			while (rs.next()) {
				if (uad == null) {
					uad = getVO(rs);
					uad.setPredefValues(setPredefAmountValues(rs));
				} else {
					uad.getPredefValues().add(getPredefAmountValue(rs));
				}
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return uad;
	}

	public static boolean clearUserActiveData(Connection con, long userId, Long balanceAfterDeposit, Long depositId, Long investmentAmount,
												Long investmentId) throws SQLException {
		PreparedStatement ps = null;
		int index = 0;
		try {
			String sql = "update users_active_data set tx_sum = ?, tx_cnt = ?, last_tx_id = ?, inv_sum = ?, inv_cnt = ?, last_inv_id = ?, def_inv_amount = null"
							+ " where user_id = ?";
			ps = con.prepareStatement(sql);
			if (balanceAfterDeposit != null) {
				ps.setLong(++index, balanceAfterDeposit);
				ps.setLong(++index, 1);
				ps.setLong(++index, depositId);
				ps.setNull(++index, Types.NUMERIC);
				ps.setNull(++index, Types.NUMERIC); // parameter number 5
				ps.setNull(++index, Types.NUMERIC);
			} else if (investmentAmount != null) {
				ps.setNull(++index, Types.NUMERIC);
				ps.setNull(++index, Types.NUMERIC);
				ps.setNull(++index, Types.NUMERIC);
				ps.setLong(++index, investmentAmount);
				ps.setLong(++index, 1); // parameter number 5
				ps.setLong(++index, investmentId);
			} else { // above parameters cannot be both null
				return false;
			}
			ps.setLong(++index, userId);
			if (ps.executeUpdate() == 1) {
				return true;
			} else {
				return false;
			}
		} finally {
			closeStatement(ps);
		}
	}

	private static UserActiveData getVO(ResultSet rs) throws SQLException {
		UserActiveData vo = new UserActiveData();
		vo.setAvgBalance(rs.getLong("tx_avg"));
		vo.setAvgInvestment(rs.getLong("inv_avg"));
		vo.setCustomAmount(rs.getLong("custom_amount"));
		vo.setCustomStepId(rs.getLong("custom_step_id"));
		vo.setDefaultLastInvAmount(rs.getLong("def_inv_amount"));
		vo.setDefaultStepAmount(rs.getLong("default_amount"));
		return vo;
	}
}