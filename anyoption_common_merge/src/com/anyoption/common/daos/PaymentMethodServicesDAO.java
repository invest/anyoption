package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.PaymentMethodService;

/**
 * PaymentMethodServicesDAO
 * @author eyalo
 */
public class PaymentMethodServicesDAO extends DAOBase {
	private static final Logger log = Logger.getLogger(PaymentMethodServicesDAO.class);
	
	/**
	 * Get all services.
	 * @param con
	 * @return ArrayList<PaymentMethodService> 
	 * @throws SQLException
	 */
	public static ArrayList<PaymentMethodService> getAllServices(Connection con) throws SQLException {
		ArrayList<PaymentMethodService> paymentMethodServices = new ArrayList<PaymentMethodService>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " SELECT " +
							" * " +
						" FROM " +
							" payment_method_services ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				PaymentMethodService pms = new PaymentMethodService();
				pms.setId(rs.getLong("id"));
				pms.setTransactionClassTypeId(rs.getLong("transaction_class_type_id"));
				pms.setTransactionStatusId(rs.getLong("transaction_status_id"));
				pms.setOperationType(rs.getString("operation_type"));
				pms.setOperationStatus(rs.getString("operation_status"));
				pms.setMethodName(rs.getString("method_name"));
				paymentMethodServices.add(pms);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return paymentMethodServices;
	}
	
	/**
	 * Get Service
	 * @param con
	 * @param pms
	 * @return String
	 * @throws SQLException
	 */
	public static String getService(Connection con, PaymentMethodService pms) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = 
					" SELECT " +
						" * " +
					" FROM " +
						" payment_method_services pms " +
					" WHERE " +
						" pms.transaction_class_type_id = ? " +
						" AND pms.transaction_status_id = ? " +
						" AND UPPER(pms.operation_type) = UPPER(?) ";
						//" AND UPPER(pms.operation_status) = UPPER(?) ";
			ps = con.prepareStatement(sql);
			ps.setLong(index++, pms.getTransactionClassTypeId());
			ps.setLong(index++, pms.getTransactionStatusId());
			ps.setString(index++, pms.getOperationType());
			//ps.setString(index++, pms.getOperationStatus());
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("method_name");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return null;
	}
}
