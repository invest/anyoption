package com.anyoption.common.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import oracle.jdbc.OracleTypes;

public class ReroutingDao extends DAOBase {
	
	private static final Logger log = Logger.getLogger(ReroutingDao.class);	
	
	public static ArrayList<Long> getReroutingProviders(Connection conn, long transactionId) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		ArrayList<Long> res = new ArrayList<>();
		try {
			cstmt = conn.prepareCall("{call PKG_REROUTING.GET_REROUTING_PROVIDERS(   O_DATA => ? " +
																			      " ,I_TRANSACTION_ID => ? )}"); 
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			setStatementValue(transactionId, index++, cstmt);			
			cstmt.execute();			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()){
				res.add(rs.getLong("clearing_provider_id"));
			}
		} finally {
			closeStatement(cstmt);
		}
		return res;
	}
	
	public static HashMap<Long, Long> getProvidersClasses(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		HashMap<Long, Long> res = new HashMap<>();
		try {
			cstmt = conn.prepareCall("{call PKG_REROUTING.GET_PROVIDERS_CLASSES (O_DATA => ? )}"); 																					   
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);		
			cstmt.execute();			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()){
				res.put(rs.getLong("clearing_provider_id"), rs.getLong("class_type_id"));
			}
		} finally {
			closeStatement(cstmt);
		}
		return res;
	}
	
	public static HashMap<Long, Set<String>> getExceptErrCodes(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		HashMap<Long, Set<String>> res = new HashMap<>();
		try {
			cstmt = conn.prepareCall("{call PKG_REROUTING.GET_EXCEPT_ERR_CODES (O_DATA => ? )}"); 																					   
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);		
			cstmt.execute();			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()){
				long providerClassesId = rs.getLong("provider_classes_id");
				String errCode = rs.getString("error_code");				
				if(res.get(providerClassesId) == null){
					//init
					Set<String> errCodes = new HashSet<>();
					res.put(providerClassesId, errCodes);
				}								
				res.get(providerClassesId).add(errCode);				
			}
		} finally {
			closeStatement(cstmt);
		}
		return res;
	}
	
	public static Set<String> getExceptErrComments(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		Set<String> res = new HashSet<>();
		try {
			cstmt = conn.prepareCall("{call PKG_REROUTING.GET_EXCEPT_ERR_COMMENTS (O_DATA => ? )}"); 																					   
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);		
			cstmt.execute();			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()){
				res.add(rs.getString("error_comments"));								
			}
		} finally {
			closeStatement(cstmt);
		}
		return res;
	}
}