package com.anyoption.common.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.bl_vos.PromotionBannerSlider;
import com.anyoption.common.bl_vos.PromotionBannerSliderImage;

import oracle.jdbc.OracleTypes;

public class PromotionManagementDAOBase extends DAOBase {
		
	public static ArrayList<PromotionBannerSlider> getActiveSliders(Connection conn, String bannerPlace) throws SQLException {
		ArrayList<PromotionBannerSlider> bannerSliders = new ArrayList<PromotionBannerSlider>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ResultSet rsImg = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_promotion_management.get_banner_sliders_by_place(o_data => ? " + 
																   						",i_banner_place => ? " +
																   						",i_language_id => ? )}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			
			setStatementValue(bannerPlace, index++, cstmt);
			cstmt.setNull(index++,  OracleTypes.NULL);
			
			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);

			while (rs.next()) {
				PromotionBannerSlider sl = new PromotionBannerSlider();
				sl.setId(rs.getLong("id"));
				sl.setPosition(rs.getInt("position"));
				sl.setLinkTypeId(rs.getInt("link_type_id"));
				rsImg = (ResultSet) rs.getObject("sl_images");
				ArrayList<PromotionBannerSliderImage> sliderImages = new ArrayList<PromotionBannerSliderImage>();
				while (rsImg.next()) {
					PromotionBannerSliderImage img = new PromotionBannerSliderImage();
					loadBannerSliderImageVO(rsImg, img);
					sliderImages.add(img);
				}
				sl.setBannerSliderImages(sliderImages);
				bannerSliders.add(sl);
				closeResultSet(rsImg);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
			closeResultSet(rsImg);
		}
		return bannerSliders;
	}

	
	protected static void loadBannerSliderImageVO(ResultSet rs, PromotionBannerSliderImage sliderImage) throws SQLException {
		sliderImage.setId(rs.getLong("id"));
		sliderImage.setImageUrl(rs.getString("link_url"));
		sliderImage.setImageName(rs.getString("image_name"));
		sliderImage.setLanguageId(rs.getLong("language_id"));
	}
	

}
