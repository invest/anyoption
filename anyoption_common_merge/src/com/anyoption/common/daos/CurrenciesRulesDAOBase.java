package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.CurrenciesRules;
import com.anyoption.common.beans.base.Currency;

/**
 * CurrenciesRulesDAOBase
 * @author eyalo
 */
public class CurrenciesRulesDAOBase extends DAOBase {
	public static final Logger log = Logger.getLogger(CurrenciesRulesDAOBase.class);
	
	/**
	 * Get default currency id
	 * @param con
	 * @param currenciesRules
	 * @return currencyId as long
	 * @throws SQLException
	 */
	public static long getDefaultCurrencyId(Connection con, CurrenciesRules currenciesRules) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		long currencyId = Currency.CURRENCY_USD_ID;
		try {
			String sql = "SELECT " + 
							" curr_rules.* " +
						" FROM " +
							" (SELECT " +
								" cr.* " +
							" FROM " +
								" currencies_rules cr " +
							" WHERE " +
								" (cr.country_id is null OR cr.country_id = ?) " +
								" and (cr.skin_id is null OR cr.skin_id = ?) " +
							" ORDER BY " +
								" cr.country_id, " +
								" cr.skin_id) curr_rules " +
						" WHERE "
							+ " rownum = 1";
			ps = con.prepareStatement(sql);
			ps.setLong(index++, currenciesRules.getCountryId());
			ps.setLong(index++, currenciesRules.getSkinId());
			rs = ps.executeQuery();
			if (rs.next()) {
				currencyId = rs.getLong("default_currency_id");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return currencyId;
	}
}
