package com.anyoption.common.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;

import oracle.jdbc.OracleTypes;

public class WriterPermisionsDAOBase extends DAOBase {

	private static final Logger logger = Logger.getLogger(WriterPermisionsDAOBase.class);
	
	public static ArrayList<String> getWriterScreenPermissions(Connection conn, long writerId, String screenName) throws SQLException {
		ArrayList<String> writersPermissionsList = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = "{call pkg_writers.get_writer_screen_perms(?, ?, ?)}";
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, writerId);
			cstmt.setString(index++, screenName);
			cstmt.executeQuery();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				writersPermissionsList.add(rs.getString("screenname") + "_" + rs.getString("permission"));
			}
		} finally {
			closeStatement(cstmt);
		}
		return writersPermissionsList;
	}
}
