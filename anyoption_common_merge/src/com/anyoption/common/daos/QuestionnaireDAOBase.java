/**
 * 
 */
package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.anyoption.common.beans.base.QuestionnaireAnswer;
import com.anyoption.common.beans.base.QuestionnaireAnswer.UserAnswerType;
import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireQuestion.QuestionType;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.util.CommonUtil;

/**
 * @author pavelhe
 *
 */
public class QuestionnaireDAOBase extends DAOBase {
	
	/**
	 * @param conn
	 * @param questionId
	 * @return
	 * @throws SQLException 
	 */
	public static List<QuestionnaireAnswer> getAllAnswersByQuestion(
												Connection con,
												long questionId) throws SQLException {
		List<QuestionnaireAnswer> answers = new ArrayList<QuestionnaireAnswer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String sql = "SELECT * FROM qm_answers WHERE q_id = ? ORDER BY answer_order_id";
			ps = con.prepareStatement(sql);
			ps.setLong(1, questionId);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				answers.add(getAnswerVO(rs));
			}
			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return answers;
	}
	
	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private static QuestionnaireAnswer getAnswerVO(ResultSet rs) throws SQLException {
		QuestionnaireAnswer answer = new QuestionnaireAnswer();
		answer.setId(rs.getLong("ID"));
		answer.setQuestionId(rs.getLong("Q_ID"));
		answer.setOrderId(rs.getLong("ANSWER_ORDER_ID"));
		answer.setName(rs.getString("ANSWER_NAME"));
		answer.setScore(rs.getLong("SCORE"));
		answer.setAnswerType(UserAnswerType.get(rs.getInt("qm_answer_type")));
		return answer;
	}
	
	/**
	 * @param con
	 * @param groupId
	 * @return
	 * @throws SQLException
	 */
	public static List<QuestionnaireQuestion> getAllQuestionsByGroup(
													Connection con,
													long groupId) throws SQLException {
		List<QuestionnaireQuestion> questions = new ArrayList<QuestionnaireQuestion>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String sql = "SELECT * FROM qm_questions WHERE qm_group_id = ? AND status = 1 ORDER BY qm_question_screen, Q_ORDER_ID";
			ps = con.prepareStatement(sql);
			ps.setLong(1, groupId);
			rs = ps.executeQuery();
			while (rs.next()) {
				questions.add(getQuestionVO(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return questions;
	}
	
	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private static QuestionnaireQuestion getQuestionVO(ResultSet rs) throws SQLException {
		QuestionnaireQuestion question = new QuestionnaireQuestion();
		question.setId(rs.getLong("ID"));
		question.setGroupId(rs.getLong("QM_GROUP_ID"));
		question.setOrderId(rs.getLong("Q_ORDER_ID"));
		question.setScreen(rs.getInt("qm_question_screen"));
		question.setName(rs.getString("NAME"));
		question.setActive(rs.getBoolean("STATUS"));
		question.setMandatory(rs.getBoolean("MANDATORY"));
		question.setQuestionType(QuestionType.get(rs.getInt("qm_question_type")));
		question.setDefaultAnswerId(rs.getLong("default_answer"));
		return question;
	}
	
	/**
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static List<QuestionnaireGroup> getAllGroups(
												Connection con) throws SQLException {
		List<QuestionnaireGroup> groups = new ArrayList<QuestionnaireGroup>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String sql = "SELECT * FROM qm_groups WHERE status = 1";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				groups.add(getGroupVO(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return groups;
	}
	
	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private static QuestionnaireGroup getGroupVO(ResultSet rs) throws SQLException {
		QuestionnaireGroup group = new QuestionnaireGroup();
		group.setId(rs.getLong("ID"));
		group.setName(rs.getString("NAME"));
		group.setActive(rs.getBoolean("STATUS"));
		return group;
	}
	
	
	public static List<QuestionnaireUserAnswer> getUserAnswers(
													Connection con,
													long userId, 
													long groupId) throws SQLException {
		List<QuestionnaireUserAnswer> answers = new ArrayList<QuestionnaireUserAnswer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String sql = "SELECT qu.* " +
					" FROM qm_user_answers qu, " +
						 " qm_questions qq" +
					" WHERE qu.user_id  = ? " +
					  " AND qu.q_group_id = ? " +
					  " AND qu.q_id = qq.id" +
					" ORDER BY qq.q_order_id";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, groupId);
			rs = ps.executeQuery();
			while (rs.next()) {
				answers.add(getUserAnswerVO(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return answers;
	}
	
	public static List<QuestionnaireUserAnswer> getAllUserAnswers(Connection con, long userId, long groupId) throws SQLException {
		List<QuestionnaireUserAnswer> answers = new ArrayList<QuestionnaireUserAnswer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {	
			String sql = 
			"SELECT " +
				"Q.id Q_id, " +
			    "Q.name, " +
			    "Q.mandatory, " +
			    "Q.q_order_id, " +
			    "Q.qm_question_screen, " +
			    "A.ID answer_id, " +
			    "A.answer_name, " +
			    "UA.qm_answer_id, " +
			    "UA.text_answer, " +
			    "UA.time_created " +
			"FROM " +
			    "qm_questions Q, " +
			    "qm_answers A, " +
			    "qm_user_answers UA " +
			"WHERE " +
			    "qm_group_id         = ? " +
			    "AND UA.q_id       = q.ID " +
			    "AND UA.answer_id   = a.id(+) " +
			    "AND status          = 1 " +
			    "AND UA.user_id    = ? " +
			"ORDER BY qm_question_screen, Q_ORDER_ID";
			
			ps = con.prepareStatement(sql);
			ps.setLong(1, groupId);
			ps.setLong(2, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				QuestionnaireUserAnswer answer = new QuestionnaireUserAnswer();
				answer.setUserAnswerId(rs.getLong("QM_ANSWER_ID"));
				answer.setQuestionId(rs.getLong("Q_ID"));
				long answerId = rs.getLong("ANSWER_ID");
				answer.setAnswerId(rs.wasNull() ? null : answerId);
				String textAnswer = rs.getString("TEXT_ANSWER");
				answer.setTextAnswer(rs.wasNull() ? null : textAnswer);
				answer.setTimeCreated(new Date(rs.getTimestamp(("TIME_CREATED")).getTime()));
				answers.add(answer);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return answers;
	}
	
	private static QuestionnaireUserAnswer getUserAnswerVO(ResultSet rs) throws SQLException {
		QuestionnaireUserAnswer answer = new QuestionnaireUserAnswer();
		answer.setUserAnswerId(rs.getLong("QM_ANSWER_ID"));
		answer.setUserId(rs.getLong("USER_ID"));
		answer.setGroupId(rs.getLong("Q_GROUP_ID"));
		answer.setQuestionId(rs.getLong("Q_ID"));
		long answerId = rs.getLong("ANSWER_ID");
		answer.setAnswerId(rs.wasNull() ? null : answerId);
		answer.setTextAnswer(rs.getString("TEXT_ANSWER"));
		answer.setTimeCreated(new Date(rs.getTimestamp(("TIME_CREATED")).getTime()));
		answer.setWriterId(rs.getLong("WRITER_ID"));
		answer.setScore(rs.getLong("SCORE"));
		return answer;
	}
	
	public static void insertUserAnswer(Connection con,
										QuestionnaireUserAnswer userAnswer, boolean allColumns) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " INSERT INTO qm_user_answers "
							+ " (QM_ANSWER_ID, USER_ID, Q_GROUP_ID, Q_ID, TIME_CREATED, WRITER_ID";
							if (allColumns){
								sql +=  ", ANSWER_ID, TEXT_ANSWER, TRANSACTION_ID) ";
							} else {
								sql +=  ") ";
							}
						sql+= " VALUES "
							+ " (SEQ_QM_USR_ANS.NEXTVAL, ?, ?, ?, ?, ?";
						if (allColumns){
							sql+= ", ?, ?, ?) ";
						} else {
							sql+= ") ";
						}

			ps = con.prepareStatement(sql);
			ps.setLong(1, userAnswer.getUserId());
			ps.setLong(2, userAnswer.getGroupId());
			ps.setLong(3, userAnswer.getQuestionId());
			ps.setTimestamp(4, new Timestamp(userAnswer.getTimeCreated().getTime()));
			ps.setLong(5, userAnswer.getWriterId());
			if (allColumns) {
				ps.setLong(6, userAnswer.getAnswerId());
				if (!CommonUtil.isParameterEmptyOrNull(userAnswer.getTextAnswer())) {
					ps.setString(7, userAnswer.getTextAnswer());
				} else {
					ps.setString(7, null);
				}
				ps.setLong(8, userAnswer.getTransactionId());
			}
			ps.executeUpdate();
			userAnswer.setUserAnswerId(getSeqCurValue(con, "SEQ_QM_USR_ANS"));
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * @param con
	 * @param userAnswers
	 * @throws SQLException
	 */
	public static void updateUserAnswers(Connection con, List<QuestionnaireUserAnswer> userAnswers, boolean setScore) throws SQLException {
		PreparedStatement ps = null;
		String sql = " UPDATE qm_user_answers "
						+ " SET answer_id = ? "
						+ " , text_answer = ? "
						+ " , WRITER_ID   = ? ";
						if(setScore){
						sql += " , SCORE   = ? ";
						}
					sql += " WHERE qm_answer_id = ? ";
		try {
			ps = con.prepareStatement(sql);
			for (QuestionnaireUserAnswer answer : userAnswers) {
				if (answer.getAnswerId() != null) {
					ps.setLong(1, answer.getAnswerId());
				} else {
					ps.setNull(1, Types.NUMERIC);
				}
				
				if (answer.getTextAnswer() != null) {
					ps.setString(2, answer.getTextAnswer());
				} else {
					ps.setNull(2, Types.VARCHAR);
				}
				ps.setLong(3, answer.getWriterId());
				if(setScore){
					if (answer.getAnswerId() != null) {
						ps.setLong(4, answer.getScore());
					} else {
						ps.setNull(4, Types.NUMERIC);
					}					
					ps.setLong(5, answer.getUserAnswerId());
				} else{
					ps.setLong(4, answer.getUserAnswerId());
				}				
				ps.addBatch();
			}
			ps.executeBatch();
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * @param con
	 * @param userAnswerId
	 * @return
	 * @throws SQLException
	 */
	public static Long getGroupIdByUserAnswerId(Connection con, long userAnswerId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " SELECT q_group_id FROM qm_user_answers WHERE qm_answer_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userAnswerId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("q_group_id");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return null;
	}
	
	public static Long updateUserAnswersScoreGroup(Connection con, long score, long userId) throws SQLException {
		PreparedStatement ps = null;
		long scoreGroup = -1;
		String sql = " UPDATE users_regulation "
				+ " SET score = ? "
				+ " , score_group = ? " 
				+ " WHERE user_id = ? ";
		try {
			//Def. Score Group
			if(score <= UserRegulationBase.ET_REGULATION_LOW_SCORE_GROUP_CALC){
				scoreGroup = UserRegulationBase.ET_REGULATION_LOW_SCORE_GROUP_ID;
			} else if(score > UserRegulationBase.ET_REGULATION_LOW_SCORE_GROUP_ID && score <= UserRegulationBase.ET_REGULATION_MEDIUM_SCORE_GROUP_CALC){
				scoreGroup = UserRegulationBase.ET_REGULATION_MEDIUM_SCORE_GROUP_ID;
			} else {
				scoreGroup = UserRegulationBase.ET_REGULATION_HIGH_SCORE_GROUP_ID;
			}
			ps = con.prepareStatement(sql);
			ps.setLong(1, score);
			ps.setLong(2, scoreGroup);
			ps.setLong(3, userId);				
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
		
		return scoreGroup;
	}
	
	public static Long getScoreUserAnswers(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " select sum(score) as score from qm_user_answers where user_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("score");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return null;
	}
	
	public static boolean isAllCapitalAnswFilled(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean res = true;
		try {
			String sql = "SELECT 1 FROM qm_user_answers  " +
							" WHERE user_id  = ? " +
								" AND answer_id is null " +
								" AND q_group_id = 7 ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				res = false;
			}			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return res;
	}
	
	public static HashMap<Integer, Boolean> getUserIncorrectAnswers(Connection con, long userId) throws SQLException {
		HashMap<Integer, Boolean> answersHM = new HashMap<Integer, Boolean>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT qq.q_order_id as Q_ID, nvl(qa.score, 0)  Q_INC " +
						" FROM " + 
							" qm_user_answers ua, " + 
							" qm_questions qq, " +
							" qm_answers qa, " +
							" qm_groups gr " +
						" WHERE ua.q_id = qq.id " +
							" and ua.answer_id = qa.id(+) " +
							" and qq.qm_group_id = gr.id " +
							" and gr.name = '" + QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME + "' " + 
							" and ua.user_id = ? " +
						" ORDER BY q_order_id ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				boolean incorrectAnswers = false;
				if(rs.getLong("Q_INC") == -1l){
					incorrectAnswers = true;
				}
				answersHM.put(rs.getInt("Q_ID"), incorrectAnswers);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return answersHM;
	}
	
	public static void updateUserScoreGroupOnly(Connection con, UserRegulationBase ur) throws SQLException {
		PreparedStatement ps = null;
		String sql = " UPDATE users_regulation "
				+ " SET score_group = ? "
				+ " WHERE user_id = ? ";
		try {
			ps = con.prepareStatement(sql);
			ps.setLong(1, ur.getScoreGroup());
			ps.setLong(2, ur.getUserId());				
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	
	public static Date isUserAnswerNoOnPEPQuestion(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Date timeCreated = null;
		try {
			String sql = " SELECT time_created FROM qm_user_answers " + 
						 " WHERE answer_id = 390 " + 
						 " AND user_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				timeCreated = new Date(rs.getTimestamp(("TIME_CREATED")).getTime());
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return timeCreated;
	}
	
	public static void updateUserPepAnswer(Connection con, UserRegulationBase ur) throws SQLException {
		PreparedStatement ps = null;
		String sql = " UPDATE users_regulation "
				+ " SET pep_state = ? "
				+ " WHERE user_id = ? ";
		try {
			ps = con.prepareStatement(sql);
			ps.setLong(1, ur.getPepState());
			ps.setLong(2, ur.getUserId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	public static void updateCheckedPepAnswer(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		String sql = " UPDATE qm_user_answers q "
				+ " SET q.answer_id = 389 "
				+ " WHERE q.user_id = ? "
				+ " and q.q_id = 93 "
				+ " and nvl(q.answer_id,0) > 0";
		try {
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * This method returns the answer as HashMap (with 2 keys - answerName and textAnswer) of the withdraw survey to be shown in Backend 
	 * @param con
	 * @param transactionId
	 * @return withdrawSurveyAnswer
	 * @throws SQLException
	 */
	public static HashMap<String, String> getWithdrawSurveyAnswer(Connection con, long transactionId) throws SQLException {
		HashMap<String, String> withdrawSurveyAnswer = new HashMap<String, String>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String sql = " SELECT " +
					 	" answer_name, text_answer " +
					 " FROM " +
					 	" qm_answers qa, qm_user_answers qua " +
					 " WHERE " +
					 	" qua.answer_id = qa.id " +
					 	" AND transaction_id = ? ";

		try {
			ps = con.prepareStatement(sql);
			ps.setLong(1, transactionId);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				withdrawSurveyAnswer.put("answerName", rs.getString("answer_name"));
				String textAnswer = rs.getString("text_answer");
				if (!CommonUtil.isParameterEmptyOrNull(textAnswer)){
					withdrawSurveyAnswer.put("textAnswer", textAnswer);
				}
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return withdrawSurveyAnswer;
	}
}
