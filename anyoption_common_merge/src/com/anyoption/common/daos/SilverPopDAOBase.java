package com.anyoption.common.daos;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.SilverPopContacts;
import com.anyoption.common.beans.SilverPopUsers;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.external.silverpop.SilverPopRequestFactory;

public class SilverPopDAOBase extends DAOBase {

    private static final Logger logger = Logger.getLogger(SilverPopDAOBase.class);
    private static final int FETCH_ROWS = 100;
    private static final String FILE_NAME_PREFIX_USERS = "silverPopExportUsers_";
    private static final String FILE_NAME_PREFIX_CONTACTS = "silverPopExportContacts_";
    private static final String DELEMITER = ",";
    
    private static final String DATE_FORMATTER = "dd-MM-yyyy";

    public static String exportUsers(Connection con, String filePath, HashMap<String, String> currSymbols, String testEmail) throws SQLException {
        String res = null;
        SimpleDateFormat sd = new SimpleDateFormat("ddMMyyyy_HHmmss");
        String fileName = FILE_NAME_PREFIX_USERS + sd.format(new Date()) + ".CSV";
        String filePathName = filePath + fileName;
        File file;
        Writer output = null;
        int br = 0;        

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        con.setAutoCommit(false);

        try { 
            logger.info("Silver Pop Begin export file " + fileName);
            String sql = "SELECT "
                                + " user_id, " 
                                + "user_name, "
                                + "first_name, "
                                + "last_name, "
                                + "email, "
                                + "country_id, "
                                + "country_name, "
                                + "currency_id, "
                                + "symbol, "  
                                + "skin_id, "
                                + "TIME_CREATED, "
                                + "first_inv_time, "
                                + "last_inv_date, "
                                + "time_last_login, "
                                + "LAST_DEP_DATE, "
                                + "round(avg_inv_amount,2) as avg_inv_amount, "
                                + "round(avg_dep_amount,2) as avg_dep_amount , "
                                + "combination_id, "  
                                + "campaign_id, "  
                                + "affiliate_key, "
                                + "affiliate_system_user_time, " 
                                + "bonus_abuser, "
                                + "dynamic_param, "
                                + "writer_id, "
                                + "trans_status_failed_count, "
                                + "trans_status_succeeded_count, "
                                + "time_first_failed_attempt, "
                                + "time_first_succeeded_attempt, "
                                + "last_issue_action_type, "
                                + "status_id, "
                                + "rank_id, " 
                                + "First_Deposit_ID,  "
                                + "Balance, "
                                + "IS_CONTACT_BY_EMAIL, "
                                + "is_active, "
                                + "class_id, "
                                + "gender, "
                                + "time_birth_date, "
                                + "email_encrypt, "
                                + "bonus_balance, "
                                + "cash_balance "
                                + "from silverpop_users_export";
            if(testEmail != null) {
            	sql += " where email like \'"+testEmail+ "\'";
            }
            pstmt = con.prepareStatement(sql);
            pstmt.setFetchSize(FETCH_ROWS);

            rs = pstmt.executeQuery();
            SimpleDateFormat formatterDateTime = new SimpleDateFormat(SilverPopRequestFactory.DATE_FORMAT);
            SimpleDateFormat formatterDate = new SimpleDateFormat(DATE_FORMATTER);
            file = new File(filePathName);           
            output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
            output.append("user_ID");
            output.append(DELEMITER);
            output.append("USER_NAME");
            output.append(DELEMITER);
            output.append("first_name");
            output.append(DELEMITER);
            output.append("last_name");
            output.append(DELEMITER);
            output.append("Email");
            output.append(DELEMITER);
            output.append("country_ID");
            output.append(DELEMITER);
            output.append("country_name");
            output.append(DELEMITER);
            output.append("currency_ID");
            output.append(DELEMITER);
            output.append("currency_symbol");
            output.append(DELEMITER);
            output.append("skin_ID");
            output.append(DELEMITER);
            output.append("time_created");
            output.append(DELEMITER);
            output.append("first_inv_date");
            output.append(DELEMITER);
            output.append("last_inv_date");
            output.append(DELEMITER);
            output.append("last_login_date");
            output.append(DELEMITER);
            output.append("last_dep_date");
            output.append(DELEMITER);
            output.append("avg_inv_amount");
            output.append(DELEMITER);
            output.append("avg_dep_amount");
            output.append(DELEMITER);
            output.append("comb_ID");
            output.append(DELEMITER);
            output.append("campaing_ID");
            output.append(DELEMITER);
            output.append("affiliate_key");
            output.append(DELEMITER);
            output.append("Affiliate_system_user_time");
            output.append(DELEMITER);
            output.append("bonus_abuser");
            output.append(DELEMITER);
            output.append("dynamic_parameter");
            output.append(DELEMITER);
            output.append("writer_ID");
            output.append(DELEMITER);
            //output.append("Trans_Status_Failed_Count");//25
            //output.append(DELEMITER);
            //output.append("Trans_Status_Succeeded_Count");//26
            //output.append(DELEMITER);
            //output.append("Time_Created_First_Failed_Attempt");//27
            //output.append(DELEMITER);
            //output.append("Time_Created_First_Succeeded_Attempt");//28
            //output.append(DELEMITER);
            //output.append("Last_Issue_Action_Type");//29
            //output.append(DELEMITER);
            //output.append("Status_ID");//30
            //output.append(DELEMITER);
            //output.append("Rank_ID");//31
            //output.append(DELEMITER);            
            //output.append("First_Deposit_ID");//32
            //output.append(DELEMITER);
            output.append("Balance");//33
            output.append(DELEMITER);
            output.append("IS_CONTACT_BY_EMAIL");//34
            output.append(DELEMITER);
            output.append("is_active");
            output.append(DELEMITER);
            output.append("class_id");
            output.append(DELEMITER);
            output.append("gender");
            output.append(DELEMITER);
            output.append("time_birth_date");
            output.append(DELEMITER);
            output.append("email_encrypt");
            output.append(DELEMITER);
            output.append("bonus_balance");
            output.append(DELEMITER);
            output.append("cash_balance");
            output.append(DELEMITER);
            output.append("\n");

            while (rs.next()) {
                output.append(rs.getString("user_id"));
                output.append(DELEMITER);
                output.append(rs.getString("user_name"));
                output.append(DELEMITER);
                output.append(rs.getString("first_name"));
                output.append(DELEMITER);
                output.append(rs.getString("last_name"));
                output.append(DELEMITER);
                output.append(rs.getString("email"));
                output.append(DELEMITER);                
                output.append(rs.getString("country_id"));
                output.append(DELEMITER);
                output.append(rs.getString("country_name"));
                output.append(DELEMITER);
                output.append(rs.getString("currency_id"));
                output.append(DELEMITER);
                output.append(currSymbols.get(rs.getString("symbol")));
                output.append(DELEMITER);
                output.append(rs.getString("skin_id"));
                output.append(DELEMITER);
                output.append(formatterDateTime.format(rs.getTimestamp("TIME_CREATED")));
                output.append(DELEMITER);
                output.append(rs.getString("first_inv_time") == null ? ConstantsBase.EMPTY_STRING : formatterDateTime.format(rs.getTimestamp("first_inv_time")));
                output.append(DELEMITER);
                output.append(rs.getString("last_inv_date") == null ? ConstantsBase.EMPTY_STRING : formatterDateTime.format(rs.getTimestamp("last_inv_date")));
                output.append(DELEMITER);
                output.append(rs.getString("time_last_login") == null ? ConstantsBase.EMPTY_STRING :formatterDateTime.format(rs.getTimestamp("time_last_login")));
                output.append(DELEMITER);
                output.append(rs.getString("last_dep_date") == null ? ConstantsBase.EMPTY_STRING : formatterDateTime.format(rs.getTimestamp("last_dep_date")));
                output.append(DELEMITER);                
                output.append(String.valueOf(rs.getDouble("avg_inv_amount")));
                output.append(DELEMITER);
                output.append(rs.getString("avg_dep_amount") == null ? ConstantsBase.EMPTY_STRING : rs.getString("avg_dep_amount"));
                output.append(DELEMITER);
                output.append(rs.getString("combination_id"));
                output.append(DELEMITER);
                output.append(rs.getString("campaign_id"));
                output.append(DELEMITER);
                output.append(rs.getString("affiliate_key") == null ? ConstantsBase.EMPTY_STRING : rs.getString("affiliate_key"));
                output.append(DELEMITER);                
                output.append(rs.getString("affiliate_system_user_time") == null ? ConstantsBase.EMPTY_STRING : formatterDate.format(rs.getDate("affiliate_system_user_time")));
                output.append(DELEMITER);
                output.append(rs.getString("bonus_abuser"));
                output.append(DELEMITER);
                output.append(rs.getString("dynamic_param") == null ? ConstantsBase.EMPTY_STRING : rs.getString("dynamic_param"));
                output.append(DELEMITER);
                output.append(rs.getString("writer_id") == null ? ConstantsBase.EMPTY_STRING : rs.getString("writer_id"));
                output.append(DELEMITER);
                //output.append(rs.getString("trans_status_failed_count"));
                //output.append(DELEMITER);                
                //output.append(rs.getString("trans_status_succeeded_count"));
                //output.append(DELEMITER);
                //output.append(rs.getString("time_first_failed_attempt") == null ? ConstantsBase.EMPTY_STRING : rs.getString("time_first_failed_attempt"));
                //output.append(DELEMITER);
                //output.append(rs.getString("time_first_succeeded_attempt") == null ? ConstantsBase.EMPTY_STRING : rs.getString("time_first_succeeded_attempt"));
                //output.append(DELEMITER);
                //output.append(rs.getString("last_issue_action_type") == null ? ConstantsBase.EMPTY_STRING : rs.getString("last_issue_action_type"));
                //output.append(DELEMITER);
                //output.append(rs.getString("status_id"));
                //output.append(DELEMITER);
                //output.append(rs.getString("rank_id"));
                //output.append(DELEMITER);                
                //output.append(rs.getString("First_Deposit_ID") == null ? ConstantsBase.EMPTY_STRING : rs.getString("First_Deposit_ID"));
                //output.append(DELEMITER);
                output.append(rs.getString("Balance"));
                output.append(DELEMITER);
                output.append(rs.getString("IS_CONTACT_BY_EMAIL") == null ? ConstantsBase.EMPTY_STRING : rs.getString("IS_CONTACT_BY_EMAIL"));
                output.append(DELEMITER);
                output.append(rs.getString("is_active"));
                output.append(DELEMITER);
                output.append(rs.getString("class_id"));
                output.append(DELEMITER);
                output.append(rs.getString("gender") == null ? ConstantsBase.EMPTY_STRING : rs.getString("gender"));
                output.append(DELEMITER);
                output.append(rs.getString("time_birth_date") == null ? ConstantsBase.EMPTY_STRING : formatterDateTime.format(rs.getDate("time_birth_date")));
                output.append(DELEMITER);
                output.append(rs.getString("email_encrypt"));
                output.append(DELEMITER);
                output.append(rs.getString("bonus_balance"));
                output.append(DELEMITER);
                output.append(rs.getString("cash_balance"));
                output.append(DELEMITER);
                output.append("\n");
                br ++;
            }
        } catch (Exception e) {
            logger.error("SilverPop exp. Users Error when export csv file:", e);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
            try {
                output.flush();
                output.close();
            } catch (Exception e) {
                logger.error("SilverPop exp. Users Error Can't close file:", e);
            }           
            try {
                con.setAutoCommit(true);
            } catch (Exception e) {
                logger.error("SilverPop exp. Users Can't set back to autocommit.", e);
            }
        }
        res = fileName;
        logger.info("Silver Pop End export file " + res + " with " + br + " rows");
        return res;
    }
    
    public static String exportContacts(Connection con, String filePath, String testEmail) throws SQLException {
        String res = null;
        SimpleDateFormat sd = new SimpleDateFormat("ddMMyyyy_HHmmss");
        String fileName = FILE_NAME_PREFIX_CONTACTS + sd.format(new Date()) + ".CSV";
        String filePathName = filePath + fileName;
        File file;
        Writer output = null;
        int br = 0;        
        SimpleDateFormat formatterDateTime = new SimpleDateFormat(SilverPopRequestFactory.DATE_FORMAT);

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        con.setAutoCommit(false);

        try {
            logger.info("Silver Pop Begin export file " + fileName);
            String sql = "SELECT "
                        + "id, "
                        + "name, "
                        + "type, "
                        + "Contacts_type_name, "
                        + "email, "
                        + "country_id, "
                        + "country_name, "
                        + "time_created, "
                        + "combination_id, "
                        + "dynamic_parameter," 
                        + "IS_CONTACT_BY_EMAIL, "
                        + "class_id "
                    + "from " 
                        + "silverpop_contacts_export ";
            if(testEmail != null) {
            	sql += " where email like \'"+testEmail+ "\'";
            }
            pstmt = con.prepareStatement(sql);
            pstmt.setFetchSize(FETCH_ROWS);

            rs = pstmt.executeQuery();

            file = new File(filePathName);
            output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
            output.append("contact_ID");//1
            output.append(DELEMITER);
            output.append("name");//2
            output.append(DELEMITER);
            output.append("contact_type_id");//3
            output.append(DELEMITER);
            output.append("contact_type_name");//4
            output.append(DELEMITER);
            output.append("Email");//5
            output.append(DELEMITER);
            output.append("country_ID");//6
            output.append(DELEMITER);            
            output.append("country_name");//7
            output.append(DELEMITER);            
            output.append("time_created");//8
            output.append(DELEMITER);            
            output.append("combination_ID");//9
            output.append(DELEMITER);
            output.append("dynamic_parameter");//10
            output.append(DELEMITER);
            output.append("IS_CONTACT_BY_EMAIL");//11
            output.append(DELEMITER);
            output.append("class_id");//12
            output.append(DELEMITER);
            output.append("\n");

            while (rs.next()) {

                output.append(rs.getString("id"));
                output.append(DELEMITER);
                output.append(rs.getString("name"));
                output.append(DELEMITER);
                output.append(rs.getString("type"));
                output.append(DELEMITER);
                output.append(rs.getString("Contacts_type_name"));
                output.append(DELEMITER);
                output.append(rs.getString("email"));
                output.append(DELEMITER);
                output.append(rs.getString("country_id"));
                output.append(DELEMITER);                
                output.append(rs.getString("country_name"));
                output.append(DELEMITER);
                output.append(formatterDateTime.format(rs.getTimestamp("TIME_CREATED")));
                output.append(DELEMITER);
                output.append(rs.getString("combination_id"));
                output.append(DELEMITER);
                output.append(rs.getString("dynamic_parameter") == null ? ConstantsBase.EMPTY_STRING : rs.getString("dynamic_parameter"));
                output.append(DELEMITER);
                output.append(rs.getString("IS_CONTACT_BY_EMAIL") == null ? ConstantsBase.EMPTY_STRING : rs.getString("IS_CONTACT_BY_EMAIL"));
                output.append(DELEMITER);
                output.append(rs.getString("class_id"));
                output.append(DELEMITER);
                output.append("\n");
                br ++;
            }
        } catch (Exception e) {
            logger.error("SilverPop exp. Contacts Error when export csv file:", e);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
            try {
                output.flush();
                output.close();
            } catch (Exception e) {
                logger.error("SilverPop exp. Users Error Can't close file:", e);
            }           
            try {
                con.setAutoCommit(true);
            } catch (Exception e) {
                logger.error("SilverPop exp. Users Can't set back to autocommit.", e);
            }
        }
        res = fileName;
        logger.info("Silver Pop End export file " + res + " with " + br + " rows");
        return res;
    }
    
    public static void updatetUserContactByEmail(Connection con,  ArrayList<SilverPopUsers> usersList) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql = "update users "
                    + " set " 
                    + " is_contact_by_email = ? "
                    + "where id = ? ";
            pstmt = con.prepareStatement(sql);
            logger.debug("start loop");
            for (SilverPopUsers userId : usersList ){
                pstmt.setLong(1, userId.getIsContactByEmail());
                pstmt.setLong(2, userId.getUserId());
                pstmt.addBatch();
            }
            logger.debug("executeBatch");
            pstmt.executeBatch();
            logger.debug("finish executeBatch");
        } finally {
            closeStatement(pstmt);
        }
    }
    
    public static void updatetContactByEmail(Connection con, ArrayList<SilverPopContacts> contactsList) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql = "update contacts "
                    + " set " 
                    + " is_contact_by_email = ? "
                    + "where id = ? ";
            pstmt = con.prepareStatement(sql);
            logger.debug("start loop");
            for (SilverPopContacts contactId : contactsList ){               
                pstmt.setLong(1, contactId.getIsContactByEmail());
                pstmt.setLong(2, contactId.getContactId());
                pstmt.addBatch();
            }
            logger.debug("executeBatch");
            pstmt.executeBatch();
            logger.debug("finish executeBatch");
        } finally {
            closeStatement(pstmt);
        }
    }
}
