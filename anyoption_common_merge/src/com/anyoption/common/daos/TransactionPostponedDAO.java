package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import com.anyoption.common.beans.TransactionPostponed;

/**
 * @author liors
 *
 */
public class TransactionPostponedDAO extends DAOBase {
	private static final Logger logger = Logger.getLogger(TransactionPostponedDAO.class);
	
	public static void insert(Connection connection, TransactionPostponed transactionPostponed) throws SQLException {
		PreparedStatement ps = null;
		int index = 0;
		try {
			String sql = 
					"INSERT INTO " +
					"	transaction_postponed " +
					"	( " + 
					"		id " +
					" 		, time_created " +
					"		, number_of_days " +
					"		, writer_id " +
					"		, time_updated " +
					"		, transaction_id)" +
					"VALUES " + 
					"	( " +
					"		seq_transaction_postponed.nextval" + 
					"		, sysdate " + 
					"		, ? " +
					"		, ? " +
					"		, sysdate " +
					"		, ? " +
					"	) ";
			
			ps = connection.prepareStatement(sql);
			ps.setInt(++index, transactionPostponed.getNumberOfDays());
			ps.setLong(++index, transactionPostponed.getWriterId());
			ps.setLong(++index, transactionPostponed.getTransactionId());
			ps.executeUpdate();
			transactionPostponed.setId(getSeqCurValue(connection, "seq_transaction_postponed"));
		} finally {
			closeStatement(ps);
		}						
	}
	
	public static void updateNumOfDays(Connection con, TransactionPostponed transactionPostponed) throws SQLException {
		PreparedStatement ps = null;
		int index = 0;
		try {
			String sql = 
					"UPDATE " +
					"	transaction_postponed " +
					"SET " +
					"	number_of_days = ? " + 
					"	, writer_id = ? " +
					" 	, time_updated = sysdate " +
					"WHERE " +
					"	id = ?";
			
			ps = con.prepareStatement(sql);
			ps.setInt(++index, transactionPostponed.getNumberOfDays());
			ps.setLong(++index, transactionPostponed.getWriterId());
			ps.setLong(++index, transactionPostponed.getId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}	
	}
}
