package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import com.anyoption.common.bl_vos.WireBase;

public class WiresDAOBase extends DAOBase {
    /**
     * Insert Bank Wire
     * 
     * @param con Db connection
     * @param vo
     * @throws SQLException
     */
    public static void insertBankWire(Connection con, WireBase vo) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                "INSERT INTO wires " +
                    "(id, bank_id, branch, account_num, account_name, bank_name, swift, iban, beneficiary_name,branch_address, bank_code, branch_name, account_info) " +
                "VALUES " +
                    "(seq_wires.nextval,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = con.prepareStatement(sql);

            if (null == vo.getBankId() || vo.getBankId().isEmpty()) {
                ps.setNull(1, Types.NUMERIC);
            } else {
                ps.setLong(1, Long.parseLong(vo.getBankId()));
            }
            if (null == vo.getBranch() || vo.getBranch().isEmpty()) {
                ps.setNull(2, Types.NUMERIC);
            } else {
                ps.setLong(2, Long.parseLong(vo.getBranch()));
            }
            if (null == vo.getAccountNum() || vo.getAccountNum().isEmpty()) {
                ps.setNull(3, Types.NUMERIC);
            } else {
                ps.setLong(3, Long.parseLong(vo.getAccountNum()));
            }
            ps.setString(4,vo.getAccountName());
            ps.setString(5,vo.getBankNameTxt());
            ps.setString(6,vo.getSwift());
            ps.setString(7,vo.getIban());
            ps.setString(8,vo.getBeneficiaryName());
            ps.setString(9, vo.getBranchAddress());
            ps.setString(10, vo.getBankCode());
            ps.setString(11, vo.getBranchName());
            ps.setString(12, vo.getAccountInfo());
            ps.executeUpdate();
            vo.setId(getSeqCurValue(con, "seq_wires"));
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
    }

    public static WireBase getById(Connection conn, long id) throws SQLException {
        WireBase w = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "wires " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                w = getVO(rs);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return w;
    }
    
	/**
	 * Insert wire Deposit
	 * @param con
	 * @param vo
	 * @throws SQLException
	 */
	  public static void insert(Connection con,WireBase vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql="insert into Wires(id,bank_id,branch,account_num,account_name,bank_name,branch_address, account_info" +
						") values(seq_Wires.nextval,?,?,?,?,?,?,?) ";

				ps = con.prepareStatement(sql);

                //in AO there is no bank Id
                if (null!=vo.getBankId()) {
                	ps.setLong(1, Long.valueOf(vo.getBankId()).longValue());
                }

				ps.setLong(2, Long.valueOf(vo.getBranch()).longValue());
				ps.setLong(3, Long.valueOf(vo.getAccountNum()).longValue());
				ps.setString(4,vo.getAccountName());
				ps.setString(5,vo.getBankName());
				ps.setString(6, vo.getBranchAddress());
				ps.setString(7, vo.getAccountInfo());

				ps.executeUpdate();
				vo.setId(getSeqCurValue(con,"seq_Wires"));
		  } finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }

	  public static WireBase get(Connection con,long id) throws SQLException {
		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  try
			{
			  String sql="SELECT " +
                            "w.*, " +
                            "b.name as Bank_Withdrawal_Name " +
                         "FROM " +
                             "wires w " +
                                 "LEFT JOIN banks b ON w.withdrawal_source = b.id " +
                         "WHERE w.id=? ";
				ps = con.prepareStatement(sql);
				ps.setLong(1, id);
				rs= ps.executeQuery();
				if (rs.next()) {
					WireBase w = getVO(rs);
                    w.setBankWithdrawName(rs.getString("Bank_Withdrawal_Name"));
					return w;
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return null;
	  }

	private static WireBase getVO(ResultSet rs) throws SQLException{
		WireBase vo = new WireBase();
		
		vo.setId(rs.getLong("id"));
		vo.setBankId(rs.getString("bank_id"));
		vo.setBranch(rs.getString("branch"));
		vo.setAccountNum(rs.getString("account_num"));
		vo.setAccountName(rs.getString("account_name"));
		vo.setAccountInfo(rs.getString("account_info"));
		
		vo.setBeneficiaryName(rs.getString("beneficiary_name"));
		vo.setSwift(rs.getString("swift"));
		vo.setIban(rs.getString("iban"));
		vo.setBankNameTxt(rs.getString("bank_name"));
		vo.setBranchAddress(rs.getString("branch_address"));
		vo.setBankFeeAmount(rs.getLong("bank_fee_amount"));
		vo.setBranchName(rs.getString("branch_name"));
		return vo;
	}

	public static void updateBankFeeAmount(Connection con,Long wireId,Long bankFeeAmount) throws SQLException{
		PreparedStatement ps = null;
		try {

			String sql = "UPDATE" +
					     "	wires " +
					     "SET " +
					     "	bank_fee_amount = ? " +
					     "WHERE" +
					     "	id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1,bankFeeAmount);
			ps.setLong(2, wireId);

			ps.executeUpdate();

		}
		finally {
			closeStatement(ps);
		}
	}

	public static void updateBankWireWithdrawal(Connection con,Long wireId, Long bankFeeAmount) throws SQLException{
        PreparedStatement ps = null;
        try {

            String sql = "UPDATE" +
                         "  wires " +
                         "SET " +
                         "  withdrawal_source = ? " +
                         "WHERE" +
                         "  id = ? ";

            ps = con.prepareStatement(sql);
            ps.setLong(1,bankFeeAmount);
            ps.setLong(2, wireId);

            ps.executeUpdate();

        }
        finally {
            closeStatement(ps);
        }
	}
}