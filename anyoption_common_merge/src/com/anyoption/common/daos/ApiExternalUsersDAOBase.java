package com.anyoption.common.daos;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * ApiExternalUsersDAO class.
 *
 * @author Eyal G
 */
public class ApiExternalUsersDAOBase extends DAOBase {

	public static String getApiExternalUserReferenceById(Connection con, long id) throws SQLException {
		String reference = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try	{

		    String sql = " SELECT " +
		    				" aeu.reference " +
		    			 " FROM " +
		    			 	" api_external_users aeu " +
		    			 " WHERE " +
		    		  		" aeu.id = ? ";

		    ps = con.prepareStatement(sql);
	    	ps.setLong(1, id);

			rs = ps.executeQuery();

			if (rs.next()) {
				reference = rs.getString("reference");
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return reference;
	}
}
