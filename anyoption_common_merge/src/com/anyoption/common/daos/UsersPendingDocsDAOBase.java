/**
 * 
 */
package com.anyoption.common.daos;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class UsersPendingDocsDAOBase extends DAOBase {
	
	private static final Logger logger = Logger.getLogger(UsersPendingDocsDAOBase.class);

	public static void insertUsersPendingDocs(Connection con, long userId, long issueId, boolean isMainRegulationIssue) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " insert into users_pending_docs " +
					  " (id, user_id, issue_id, is_main_regulation_issue, pending_docs, uploaded_docs, reject_support, reject_control) " +
			" values " +
			          " (SEQ_USERS_PENDING_DOCS.NEXTVAL, ?, ?, ?, -1, -1, -1, -1) " ;

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, issueId);
			ps.setBoolean(3, isMainRegulationIssue);	
			
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}
	
    public static long getPendicDocsId(Connection conn, long issueId) throws SQLException {
        long id = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "id " +
                    "FROM " +
                        "users_pending_docs " +
                    "WHERE " +
                        "issue_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, issueId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                id = rs.getLong("id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return id;
    }
    
	public static void calculatePendingDocs(Connection con, long fileId, long issueId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		logger.debug("Try to CALCULATE_PENDING_DOCS_STATE");
		try {
			String sql = "Begin REGULATION.CALCULATE_PENDING_DOCS_STATE(?, ?);  End;" ;

			ps = con.prepareStatement(sql);
			ps.setLong(1, fileId);
			ps.setLong(2, issueId);	
			
			ps.executeUpdate();
			logger.debug("Finish CALCULATE_PENDING_DOCS_STATE");
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}
	
	public static void updateLastWriterActionTime(Connection con, long issueId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "update " +
							" users_pending_docs " +
						 " set " +
						    " last_action_time_by_writer = sysdate " +
						  " where " +
						  "  issue_id = ? " ;

			ps = con.prepareStatement(sql);
			ps.setLong(1, issueId);	
			
			ps.executeUpdate();
			logger.debug("updated Last Writer Action Time on users_pending_docs table");
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}
	
	public static void updateLastUploadFileTime(Connection con, long fileId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "update " +
							" users_pending_docs " +
						 " set " +
						    " last_upload_file_time = sysdate " +
						  " where " +
						  "  issue_id in (select ia.issue_id from files f, issue_actions ia where f.issue_action_id = ia.id and f.id = ? ) " ;

			ps = con.prepareStatement(sql);
			ps.setLong(1, fileId);	
			
			ps.executeUpdate();
			logger.debug("updated Last upload file Time on users_pending_docs table for fileId: " + fileId);
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}
}