package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.bl_vos.Cheque;

public class ChequesDAOBase extends DAOBase {
    public static Cheque getById(Connection conn, long id) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Cheque vo = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "cheques " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                vo = new Cheque();
                vo.setId(rs.getLong("id"));
                vo.setName(rs.getString("name"));
                vo.setStreet(rs.getString("street"));
                vo.setStreetNo(rs.getString("street_no"));
                vo.setCityId(rs.getLong("city_id"));
                vo.setZipCode(rs.getString("zip_code"));
                vo.setChequeId(rs.getString("cheque_id"));
                vo.setFeeCancel(rs.getBoolean("fee_cancel"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return vo;
    }
}