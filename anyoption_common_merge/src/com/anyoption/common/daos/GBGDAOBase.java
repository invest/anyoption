/**
 * 
 */
package com.anyoption.common.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;


public class GBGDAOBase extends DAOBase {
	
	  public static HashMap<Long, String> loadGBGCountries(Connection con) throws SQLException {		  	
			CallableStatement cstmt = null;
			ResultSet rs = null;
			int index = 1;
			HashMap<Long, String> gbgCountries = new HashMap<>();
			try {
				String sql = "{call pkg_userfiles.load_gbg_countries(?)}";
				cstmt = con.prepareCall(sql);
				cstmt.registerOutParameter(index++, OracleTypes.CURSOR);				
				cstmt.executeQuery();
				rs = (ResultSet) cstmt.getObject(1);
				while (rs.next()) {
					gbgCountries.put(rs.getLong("Ccountry_id"), rs.getString("profile_id"));
				}
			} finally {
				closeStatement(cstmt);			
			}
			return gbgCountries;
	  }
}
