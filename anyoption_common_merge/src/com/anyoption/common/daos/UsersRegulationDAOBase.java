package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.ActivationMail;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.UsersRegulationDueDocumentsDepositLimit;
import com.anyoption.common.beans.base.UserRegulationActivationMail;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.IssuesManagerBase;

public abstract class UsersRegulationDAOBase extends DAOBase {
	
	private static final Logger log = Logger.getLogger(UsersRegulationDAOBase.class);
	private static final int STATUS_UPDATE_OK = 1;
	private static final int STATUS_UPDATE_ERR = 0;
	private static final String DAYS_UNTIL_SUSPENSION = "30";
	
	public static void insertUserRegulation(Connection conn, UserRegulationBase ur) throws SQLException {
		
		if (ur.getApprovedRegulationStep() != UserRegulationBase.REGULATION_USER_REGISTERED) {
    		log.warn("User regulation step [" + ur.getApprovedRegulationStep()
	    				+ "] does not match the REGISTERED step ["
	    				+ UserRegulationBase.REGULATION_USER_REGISTERED + "]");
    	}
		
		PreparedStatement ps = null;
		try {
			String sql = " INSERT INTO " +
					     " USERS_REGULATION " +
					     	" (id, user_id, approved_regulation_step, WRITER_ID, SUSPENDED_REASON_ID, regular_report_mail, regulation_version) " +
					     " VALUES " +
					     	" (SEQ_USR_REG.nextval, ?, ?, ?, ?, ?, ?)";
			
			ps = conn.prepareStatement(sql);
			ps.setLong(1, ur.getUserId());
			ps.setInt(2, ur.getApprovedRegulationStep());
			ps.setLong(3, ur.getWriterId());
			ps.setInt(4, ur.getSuspendedReasonId());
			ps.setBoolean(5, ur.isRegularReportMail());
			ps.setInt(6, UserRegulationBase.REGULATION_USER_VERSION);
			ps.executeUpdate();
		} finally {
    		closeStatement(ps);
    	}		
	}
	
	public static void updateRegulationStep(Connection conn, UserRegulationBase ur) throws SQLException {		
		PreparedStatement ps = null;
		try {
	        String sql = " UPDATE " +
	        		        " users_regulation " +
	        		      " SET " +
	        		        " approved_regulation_step = ? " +
	        		        " , WRITER_ID = ? " +
	        		        " , COMMENTS = ? " +
	        		      " WHERE user_id = ? ";
	        
	        ps = conn.prepareStatement(sql);
	        ps.setInt(1, ur.getApprovedRegulationStep());
	        ps.setLong(2, ur.getWriterId());
	        if (ur.getComments() != null) {
				ps.setString(3, ur.getComments());
			} else {
				ps.setNull(3, Types.VARCHAR);
			}
	        ps.setLong(4, ur.getUserId());
	        ps.executeUpdate();
	        
		} finally {
			closeStatement(ps);
		}
	}
	
	public static void updateSuspended(Connection conn, UserRegulationBase ur) throws SQLException {
		PreparedStatement ps = null;
		try {
        String sql = " UPDATE " +
        		        " users_regulation " +
        		      " SET " +
        		      	" SUSPENDED_REASON_ID = ? " +
        		        " ,COMMENTS = ? " +
        		        " ,time_suspended_regulation = sysdate " +
        		        " ,WRITER_ID = ? " +
        		      " WHERE user_id = ? ";
        
        ps = conn.prepareStatement(sql);
        ps.setInt(1, ur.getSuspendedReasonId());
        ps.setString(2, ur.getComments());
        ps.setLong(3, ur.getWriterId());
        ps.setLong(4, ur.getUserId());
        ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}		
	}
	
	public static void updateWCApproval(Connection conn, UserRegulationBase ur) throws SQLException {
		PreparedStatement ps = null;
		try {
	        String sql = " UPDATE " +
	        		        " users_regulation " +
	        		      " SET " +
	        		        " approved_regulation_step = ? " +
	        		        " ,wc_approval = ? " +
	        		        " ,wc_comment = ? " +
	        		        " ,WRITER_ID = ? " +
	        		      " WHERE user_id = ? ";
	        
	        ps = conn.prepareStatement(sql);
	        ps.setInt(1, ur.getApprovedRegulationStep());
	        ps.setInt(2, ur.isWcApproval() ? 1 : 0);        
	        ps.setString(3, ur.getWcComment());
	        ps.setLong(4, ur.getWriterId());
	        ps.setLong(5, ur.getUserId());
	        ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}		
	}
	
	public static void updateOptionalQuestionnaireStatus(Connection conn, UserRegulationBase ur) throws SQLException {
		PreparedStatement ps = null;
		try {
	        String sql = " UPDATE users_regulation SET is_opt_quest_done = ?, WRITER_ID = ?, COMMENTS = ? WHERE user_id = ? ";
	        
	        ps = conn.prepareStatement(sql);
	        ps.setBoolean(1, ur.isOptionalQuestionnaireDone());
	        ps.setLong(2, ur.getWriterId());
	        if (ur.getComments() != null) {
				ps.setString(3, ur.getComments());
			} else {
				ps.setNull(3, Types.VARCHAR);
			}
	        ps.setLong(4, ur.getUserId());
	        ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}		
	}
	
	public static void  getUserRegulation(Connection conn, UserRegulationBase userRegulation) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " +
					      " * FROM " +
					           " users_regulation " +
					      " WHERE" +
					            " user_id = ?";
			ps = conn.prepareStatement(sql);
			ps.setLong(1, userRegulation.getUserId());
			rs = ps.executeQuery();
			
			while (rs.next()) {
				userRegulation.setApprovedRegulationStep(rs.getInt("approved_regulation_step"));
				userRegulation.setSuspendedReasonId(rs.getInt("SUSPENDED_REASON_ID"));
				userRegulation.setComments(rs.getString("COMMENTS"));
				userRegulation.setWcApproval(rs.getBoolean("wc_approval"));
				userRegulation.setWcComment(rs.getString("wc_comment"));
				userRegulation.setOptionalQuestionnaireDone(rs.getBoolean("IS_OPT_QUEST_DONE"));
				userRegulation.setWriterId(rs.getLong("WRITER_ID"));
				userRegulation.setQualified(rs.getBoolean("QUALIFIED"));
				userRegulation.setKnowledgeQuestion(rs.getBoolean("IS_KNOWLEDGE_QUESTION"));
				userRegulation.setRegularReportMail(rs.getBoolean("regular_report_mail"));
				userRegulation.setScoreGroup(rs.getLong("SCORE_GROUP"));
				userRegulation.setScore(rs.getLong("SCORE"));
				if(userRegulation.getSuspendedReasonId().equals(UserRegulationBase.ET_SUSPEND_TRESHEHOLD_LOW_SCORE_GROUP)
						|| userRegulation.getSuspendedReasonId().equals(UserRegulationBase.ET_SUSPEND_TRESHEHOLD_MEDIUM_SCORE_GROUP)){
					userRegulation.setTresholdLimit( new Long(userRegulation.getComments()));
				} else {
					userRegulation.setTresholdLimit(0L);
				}
				userRegulation.setRegulationVersion(rs.getInt("REGULATION_VERSION"));
				userRegulation.setQualifiedAmount(rs.getLong("QUALIFIED_AMOUNT"));
				userRegulation.setPepState(rs.getLong("PEP_STATE"));				
				userRegulation.setThresholdBlock(rs.getLong("THRESHOLD_BLOCK"));
			}
			
		}finally {
			
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	public static void  getUserRegulationBackend(Connection conn, UserRegulationBase userRegulation) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " +
					      " ur.*, urs.name Regulation_step " +
		                  ", tr.time_created FDT_Time_Created " + 
                          " , usr.reason_name" +
					      " FROM " +
					      " users u, users_regulation ur, users_regulation_steps urs,transactions tr,users_suspended_reasons usr " +
					      " WHERE u.id = ur.user_id " +
					      " AND ur.approved_regulation_step = urs.step_id " +
                          " AND tr.id(+) = u.first_deposit_id " +
                          " AND usr.id = ur.suspended_reason_id " +
					      " AND u.id = ? ";
			
			ps = conn.prepareStatement(sql);
			ps.setLong(1, userRegulation.getUserId());
			rs = ps.executeQuery();
			
			while (rs.next()) {
				userRegulation.setApprovedRegulationStep(rs.getInt("approved_regulation_step"));
				userRegulation.setSuspendedReasonId(rs.getInt("SUSPENDED_REASON_ID"));
				userRegulation.setComments(rs.getString("COMMENTS"));
				userRegulation.setWcApproval(rs.getBoolean("wc_approval"));
				userRegulation.setWcComment(rs.getString("wc_comment"));
				userRegulation.setOptionalQuestionnaireDone(rs.getBoolean("IS_OPT_QUEST_DONE"));
				userRegulation.setWriterId(rs.getLong("WRITER_ID"));
				userRegulation.setFDTTimeCreated(rs.getTimestamp("FDT_Time_Created"));
				userRegulation.setRegulationStepTxt(rs.getString("Regulation_step"));
				userRegulation.setSuspendedReasonTxt(rs.getString("reason_name"));
				userRegulation.setTimeSuspended(rs.getTimestamp("time_suspended_regulation"));
				userRegulation.setQualificationTime(rs.getDate("qualified_time"));
				userRegulation.setKnowledgeQuestion(rs.getBoolean("IS_KNOWLEDGE_QUESTION"));
				userRegulation.setRegularReportMail(rs.getBoolean("REGULAR_REPORT_MAIL"));
				userRegulation.setScoreGroup(rs.getLong("SCORE_GROUP"));
				userRegulation.setScore(rs.getLong("SCORE"));
				if(userRegulation.getSuspendedReasonId() == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_LOW_SCORE_GROUP
						|| userRegulation.getSuspendedReasonId() == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_MEDIUM_SCORE_GROUP){
					try{
						userRegulation.setTresholdLimit( new Long(userRegulation.getComments()));
					} catch (Exception e) {
						log.error("Unable to parse treshhold from table, setting default value");
					}
				} else {
					userRegulation.setTresholdLimit(0L);
				}
				userRegulation.setRegulationVersion(rs.getInt("REGULATION_VERSION"));
				userRegulation.setQualifiedAmount(rs.getLong("QUALIFIED_AMOUNT"));
				userRegulation.setPepState(rs.getLong("PEP_STATE"));
				userRegulation.setThresholdBlock(rs.getLong("THRESHOLD_BLOCK"));
				userRegulation.setControlApprovedDate(rs.getTimestamp("control_approved_date"));
				userRegulation.setQuestDoneDate(rs.getTimestamp("questionnaire_done_date"));				
			}
			
		}finally {
			
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	public static ArrayList<SelectItem> getRegulationStepsList(Connection con, long userId, long writerId) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> sList = new ArrayList<SelectItem>();
		Long currentStep = null;
		String sql = null;
		try {
			
			sql =   "SELECT " +
					"ur.approved_regulation_step " +
					"FROM users_regulation ur, users u " +
					"WHERE u.id = ? " +
					"AND u.id = ur.user_id " +
					"AND u.skin_id in " +
					"(SELECT skin_id " +
					" FROM writers_skin where writer_id = ? )";
			
			ps = con.prepareStatement(sql);
			ps.setLong(1,userId);
			ps.setLong(2,writerId);
			rs = ps.executeQuery();
			
			if(rs.next()) {
				
				currentStep = rs.getLong("approved_regulation_step");
			
			closeStatement(ps);
			closeResultSet(rs);
			
			 sql = "SELECT * " +
					     "FROM " +
					     "users_regulation_steps " ;
			 
			 ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
			
			while(rs.next()) {
				if(rs.getLong("step_id")> currentStep) {
				sList.add(new SelectItem(rs.getLong("step_id"),rs.getString("name")));
				}
		   	}
			
		 }
			
		}finally{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return sList;
	}
	
	public static ArrayList<Issue> getOpenedPendingDocsIssuesByUserId(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Issue> issuesList = new ArrayList<Issue>();
		String sql = null;
		try {

			sql = " SELECT " +
						" id, " +
						" subject_id" +
				  " FROM " +
				  		" issues " +
				  " WHERE " +
				  		" subject_id = " + IssuesManagerBase.ISSUE_SUBJECT_PENDING_DOC +
				  		" AND user_id = ? ";
			
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Issue issue = new Issue();
				issue.setId(rs.getLong("id"));
				issue.setSubjectId(rs.getLong("subject_id") + "");
				issuesList.add(issue);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return issuesList;
	}
	
public static int updateRegulationStepWithCheck(Connection conn, UserRegulationBase ur) throws SQLException {		
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = null;
		int currentStep = 0;
		int result = STATUS_UPDATE_ERR; 
		try {
			sql =  "SELECT " +
					"approved_regulation_step " +
					"FROM " +
					"users_regulation " +
					"WHERE " +
					"user_id = ? ";
			
			ps = conn.prepareStatement(sql);
			ps.setLong(1, ur.getUserId());
			rs = ps.executeQuery();
			
			if (rs.next()) {
				currentStep = rs.getInt("approved_regulation_step");
			}
			closeStatement(ps);
			if(currentStep < ur.getApprovedRegulationStep()) {
	        sql = " UPDATE " +
	        		        " users_regulation " +
	        		      " SET " +
	        		        " approved_regulation_step = ? " +
	        		        " , WRITER_ID = ? " +
	        		        " , COMMENTS = ? " +
	        		      " WHERE user_id = ? ";
	        
	        ps = conn.prepareStatement(sql);
	        ps.setInt(1, ur.getApprovedRegulationStep());
	        ps.setLong(2, ur.getWriterId());
	        if (ur.getComments() != null) {
				ps.setString(3, ur.getComments());
			} else {
				ps.setNull(3, Types.VARCHAR);
			}
	        ps.setLong(4, ur.getUserId());
	        ps.executeUpdate();
	        result = STATUS_UPDATE_OK;
			}
	        
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return result;
	}

		public static void updateRegulationStepKnowledgeQuestion(Connection conn, UserRegulationBase ur) throws SQLException {		
			PreparedStatement ps = null;
			try {
		        String sql = " UPDATE " +
		        		        " users_regulation " +
		        		      " SET " +
		        		        " approved_regulation_step = ? " +
		        		        " , WRITER_ID = ? " +
		        		        " , COMMENTS = ? " +
		        		        ", IS_KNOWLEDGE_QUESTION = ? " +
		        		      " WHERE user_id = ? ";
		        
		        ps = conn.prepareStatement(sql);
		        ps.setInt(1, ur.getApprovedRegulationStep());
		        ps.setLong(2, ur.getWriterId());
		        if (ur.getComments() != null) {
					ps.setString(3, ur.getComments());
				} else {
					ps.setNull(3, Types.VARCHAR);
				}
		        ps.setBoolean(4, ur.isKnowledgeQuestion());
		        ps.setLong(5, ur.getUserId());
		        ps.executeUpdate();
		        
			} finally {
				closeStatement(ps);
			}
		}		

	public static void updateRegularReportMail(Connection conn, UserRegulationBase ur) throws SQLException {
		PreparedStatement ps = null;
		try {
	        String sql = " UPDATE users_regulation SET regular_report_mail = ? WHERE user_id = ? ";
	        
	        ps = conn.prepareStatement(sql);
	        ps.setBoolean(1, ur.isRegularReportMail());
	        ps.setLong(2, ur.getUserId());
	       
	        ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}		
	}
	
	public static void insertHashKey(Connection conn, long userId, String hashKey, long hashType) throws SQLException {
		PreparedStatement ps = null;
        try {
            String sql = " INSERT INTO unsuspend_user_reg (id, user_id, hash_key, hash_type, time_created) " +
            			 " VALUES (SEQ_UNSUSPEND_USER_REG.nextval, ?, ?, ?, sysdate) ";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, userId);
            ps.setString(2, hashKey);
            ps.setLong(3, hashType);
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
	}
	
	public static Issue getSuspendedIssueId(Connection conn, long userId) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		Issue vo = null;
		try{
			String sql = "SELECT distinct i.id, i.subject_id, i.status_id" +
							" FROM issues i, issue_actions ia "+
							" WHERE i.id = ia.issue_id "+
							" AND i.user_id = ? "+
							" AND ia.issue_action_type_id in (" + IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_LOW_QUESTIONNARE_SCORE + "," 
																+ IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_LOSS_THRESHOLD_SUSPEND + ","
																+ IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_FAILED_QUESTIONNAIRE +
															") " +
				             " AND NOT EXISTS " +
				             " ( SELECT i.id " +
				             " FROM issue_actions iae " +
							  " WHERE i.id = iae.issue_id " +
                               " AND iae.issue_action_type_id  in (" + IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_ACTIVATION + ") " +
                              " )" ;
			ps = conn.prepareStatement(sql);
            ps.setLong(1, userId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	vo = new Issue();
            	vo.setId(rs.getLong("id"));
				vo.setSubjectId(rs.getString("subject_id"));
				vo.setStatusId(rs.getString("status_id"));
            }
		}finally{
			closeStatement(ps);
			closeResultSet(rs);
		}
		return vo;
	}
	
	public static void unsuspendUser(Connection conn, long userId, long suspendedId) throws SQLException{
		PreparedStatement ps = null;
		String sql = "";
		
		try {
			if(suspendedId == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_LOW_SCORE_GROUP || suspendedId == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_MEDIUM_SCORE_GROUP) {
				sql = "UPDATE users_regulation " +
						" SET suspended_reason_id = ?, " +
						" comments = ?, " +
						" approved_regulation_step = " + UserRegulationBase.ET_REGULATION_DONE + "" +
						" WHERE user_id = ? ";
			} else {
				sql = "UPDATE users_regulation " +
						" SET suspended_reason_id = ?, " +
						" comments = ? " +
						" WHERE user_id = ? ";
			}
			
			ps = conn.prepareStatement(sql);
            ps.setLong(1, UserRegulationBase.NOT_SUSPENDED);
            if(suspendedId == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_LOW_SCORE_GROUP || suspendedId == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_MEDIUM_SCORE_GROUP) {
            	ps.setString(2, "The user was UNSUSPENDED by low/medium treshold score group suspension.");
            } else {
            	ps.setString(2, "The user was UNSUSPENDED.");
            }
            ps.setLong(3, userId);
            ps.executeUpdate();
            
		}finally{
			closeStatement(ps);
		}
	}
	
	public static void unsuspendUserAfterControlApproved(Connection conn, long userId) throws SQLException{
		PreparedStatement ps = null;
		String sql = "";
		
		try {
				sql = "UPDATE users_regulation " +
					  " SET " +
					     " suspended_reason_id = ?, " +
						 " comments = ? " +
					  " WHERE user_id = ? " +
						 " and suspended_reason_id = ? " +
						 " and approved_regulation_step = ?";
			
			ps = conn.prepareStatement(sql);
            ps.setLong(1, UserRegulationBase.NOT_SUSPENDED);
            ps.setString(2, "The user was UNSUSPENDED after control approved");
            ps.setLong(3, userId);
            ps.setLong(4, UserRegulationBase.SUSPENDED_DUE_DOCUMENTS);
            ps.setLong(5, UserRegulationBase.REGULATION_CONTROL_APPROVED_USER);
            ps.executeUpdate();            
		}finally{
			closeStatement(ps);
		}
	}
	
	public static void setControlApprovedDate(Connection conn, long userId) throws SQLException{
		PreparedStatement ps = null;
		String sql = "";
		
		try {
				sql = "UPDATE users_regulation " +
					  " SET " +
						 " control_approved_date = sysdate " +
					  " WHERE user_id = ? " ;
			
			ps = conn.prepareStatement(sql);
            ps.setLong(1, userId);
            ps.executeUpdate();            
		}finally{
			closeStatement(ps);
		}
	}
	
	public static void setQuestionnaireDoneDate(Connection conn, long userId) throws SQLException{
		PreparedStatement ps = null;
		String sql = "";
		
		try {
				sql = "UPDATE users_regulation " +
					  " SET " +
						 " questionnaire_done_date = sysdate " +
					  " WHERE user_id = ? " ;
			
			ps = conn.prepareStatement(sql);
            ps.setLong(1, userId);
            ps.executeUpdate();            
		}finally{
			closeStatement(ps);
		}
	}
	
	public static ArrayList<UserRegulationActivationMail> getUserRegulationActivationMail(Connection con) throws SQLException {
		ArrayList<UserRegulationActivationMail> list = new ArrayList<UserRegulationActivationMail>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT m.*, u.skin_id  " +
						" FROM  " +
						    " USERS_REG_ACTIVATION_MAIL m," +
						    " USERS u " +
						" WHERE m.is_send = 0 " +
						    " AND u.id = m.user_id " ;

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				UserRegulationActivationMail rm = new UserRegulationActivationMail();
				rm.setId(rs.getLong("id"));
				rm.setUserId(rs.getLong("user_id"));
				rm.setSuspendedReasonId(rs.getLong("suspended_reason_id"));
				rm.setSkinId(rs.getLong("skin_id"));
				list.add(rm);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}
	
	public static void updateUserRegulationActivationMail(Connection con, List<UserRegulationActivationMail> activationMail) throws SQLException {
		PreparedStatement ps = null;
		String sql = " UPDATE USERS_REG_ACTIVATION_MAIL " +
						" SET is_send = 1 " +
					    " , time_send = sysdate " +
						" WHERE id = ? ";
		try {
			ps = con.prepareStatement(sql);
			for (UserRegulationActivationMail mail : activationMail) {				
				ps.setLong(1, mail.getId());
				ps.addBatch();
			}
			ps.executeBatch();
		} finally {
			closeStatement(ps);
		}
	}
	
	public static void updateErrorUserRegulationActivationMail(Connection con, List<UserRegulationActivationMail> activationMail) throws SQLException {
		PreparedStatement ps = null;
		String sql = " UPDATE USERS_REG_ACTIVATION_MAIL " +
						" SET is_send = 2 " +
					    " , time_error = sysdate " +
						" WHERE id = ? ";
		try {
			ps = con.prepareStatement(sql);
			for (UserRegulationActivationMail mail : activationMail) {				
				ps.setLong(1, mail.getId());
				ps.addBatch();
			}
			ps.executeBatch();
		} finally {
			closeStatement(ps);
		}
	}
	
	public static Date getTimeWhenQuestionnaireFinallySubmited(Connection conn, long userId, int step) throws SQLException{
		Date date = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			String sql = "SELECT time_created " +
							" FROM users_regulation_history "+
							" WHERE user_id = ? "+
							" AND approved_regulation_step = ? "+
							" ORDER BY id ";
			ps = conn.prepareStatement(sql);
            ps.setLong(1, userId);
            ps.setInt(2, step);
            rs = ps.executeQuery();
            if (rs.next()) {
            	date = rs.getDate("time_created");
            }
		}finally{
			closeStatement(ps);
			closeResultSet(rs);
		}
		return date;
	} 
	
	public static long getUserIdByHashKey(Connection conn, String hashKey, long hashType) throws SQLException {
		long id = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT user_id " +
							" FROM unsuspend_user_reg "+
							" WHERE hash_key = ? " +
							 "AND hash_type = ? " ;
			
			ps = conn.prepareStatement(sql);
            ps.setString(1, hashKey);
            ps.setLong(2, hashType);
            rs = ps.executeQuery();
            if(rs.next()){
            	id = rs.getLong("user_id");
            }
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return id;
	}
	
	public static ActivationMail getUserHashTypeAndIdByHashKey(Connection conn, String hashKey) throws SQLException {
		long hashTypeId = 0L;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ActivationMail activation = null;
		try {
			String sql = "SELECT hash_type, user_id " +
							" FROM unsuspend_user_reg "+
							" WHERE hash_key = ? ";
			
			ps = conn.prepareStatement(sql);
            ps.setString(1, hashKey);
            rs = ps.executeQuery();
            if(rs.next()){
            	activation = new ActivationMail();
            	activation.setUserId(rs.getLong("user_id"));
            	activation.setType(rs.getLong("hash_type"));
            }
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return activation;
	}
	
	public static void updateIsKnowledgeQuestion(Connection conn, long userId, boolean isKnowledgeQuestion, long writerId) throws SQLException {		
		PreparedStatement ps = null;
		try {
	        String sql = " UPDATE " +
	        		        " users_regulation " +
	        		      " SET " +
	        		        " IS_KNOWLEDGE_QUESTION = ? " +
	        		        " , WRITER_ID = ? " +
	        		      " WHERE user_id = ? ";
	        
	        ps = conn.prepareStatement(sql);
	        ps.setBoolean(1, isKnowledgeQuestion);
	        ps.setLong(2, writerId);
	        ps.setLong(3, userId);
	        ps.executeUpdate();	        
		} finally {
			closeStatement(ps);
		}
	}
	
	public static long getRegulationUserSuspendedId(Connection conn, long userId) throws SQLException{
		long suspendedReasonId = -1;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
			String sql = "SELECT suspended_reason_id " +
							" FROM users_regulation " +
							" WHERE user_id = ? ";
			
			ps = conn.prepareStatement(sql);
            ps.setLong(1, userId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	suspendedReasonId = rs.getLong("suspended_reason_id");
            }
		}finally{
			closeStatement(ps);
			closeResultSet(rs);
		}
		return suspendedReasonId;
	}

	public static boolean isRegulationSuspended(Connection conn, long userId) throws SQLException{
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
			String sql = "SELECT NVL(suspended_reason_id, 0) suspended_reason_id, NVL(threshold_block, 0) threshold_block" +
							" FROM users_regulation " +
							" WHERE user_id = ? ";
			
			ps = conn.prepareStatement(sql);
            ps.setLong(1, userId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	long suspendedReasonId	= rs.getLong("suspended_reason_id");
            	long thresholdBlock 	= rs.getLong("threshold_block");
            	if(suspendedReasonId > 0 || thresholdBlock > 0 ) {
            		log.debug("userId:"+ userId+" suspendedReasonId:" +suspendedReasonId + " thresholdBlock" +thresholdBlock);
            		return true;
            	} else {
            		return false;
            	}
            }
		}finally{
			closeStatement(ps);
			closeResultSet(rs);
		}
		return false;
	}
	
	public static void unsuspendUser(Connection conn, long userId) throws SQLException{
		PreparedStatement ps = null;
		
		try {
			
			String sql = "UPDATE users_regulation " +
							" SET suspended_reason_id = ? "+
							" WHERE user_id = ? ";
			
			ps = conn.prepareStatement(sql);
            ps.setLong(1, UserRegulationBase.NOT_SUSPENDED);
            ps.setLong(2, userId);
            ps.executeUpdate();
            
		}finally{
			closeStatement(ps);
		}
	}
	
	public static void unblockUser(Connection conn, long userId) throws SQLException{
		PreparedStatement ps = null;
		
		try {
			
			String sql = "UPDATE users_regulation " +
							" SET threshold_block = ? "+
							" WHERE user_id = ? ";
			
			ps = conn.prepareStatement(sql);
            ps.setLong(1, UserRegulationBase.AO_TRESHOLD_BLOCK_UNBLOCKED);
            ps.setLong(2, userId);
            ps.executeUpdate();
            
		}finally{
			closeStatement(ps);
		}
	}
	
	public static UsersRegulationDueDocumentsDepositLimit getUserDueDocumentsDepLimit(Connection conn, long userId) throws SQLException {
		UsersRegulationDueDocumentsDepositLimit res = new UsersRegulationDueDocumentsDepositLimit();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean foundLimit = false;
		try {
			String sql = "SELECT * FROM " + 
					" (select " + 
						" 1 as id, regulation.DEPOSIT_CHECK_SUM(?)*100 as dep, regulation.GET_FIRST_LIMIT_DEP_DOC as limit " +
						" from dual " +
					   " union all " +
					   	" select " + 
					   	" 2 as id, regulation.DEPOSIT_CHECK_SUM(?)*100 as dep, regulation.GET_SECOND_BLOCK_LIMIT_DEP_DOC as limit " + 
					   	" from dual " +
					  " ) lim " +
					" order by id desc ";
			
			ps = conn.prepareStatement(sql);
            ps.setLong(1, userId);
            ps.setLong(2, userId);
            rs = ps.executeQuery();
            while(!foundLimit && rs.next()){
            	res.setUserId(userId);
            	res.setId(0l);
            	long depositSum = rs.getLong("dep");
            	long depositLimit = rs.getLong("limit");
            	if(depositSum >= depositLimit){
            		res.setId(rs.getInt("id"));
            		res.setDepositSum(depositSum);
            		res.setDepositLimit(depositLimit);
            		foundLimit = true;
            	}
            }
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return res;
	}
	
	public static void updateQualified(Connection conn, UserRegulationBase ur) throws SQLException {
		PreparedStatement ps = null;
		try {
        String sql = " UPDATE " +
        		        " users_regulation " +
        		      " SET " +
        		      	" qualified = ? " +
        		        " ,qualified_amount = ? " +
        		        " ,qualified_time = decode(?,1,sysdate,qualified_time)" +
        		        " ,writer_id = ? " +
        		      " WHERE user_id = ? ";
        
        ps = conn.prepareStatement(sql);
        ps.setBoolean(1, ur.isQualified());
        ps.setLong(2, ur.getQualifiedAmount());
        ps.setBoolean(3, ur.isQualified());
        ps.setLong(4, ur.getWriterId());
        ps.setLong(5, ur.getUserId());
        ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}		
	}
	
	public static void insertSentMailAfterLimitDeposit(Connection conn, long userId, long suspendReasonId) throws SQLException {
		PreparedStatement ps = null;
		try {
        String sql = " insert into users_reg_activation_mail " +
        		      " (id, user_id, suspended_reason_id,  is_send) " +
				     " values " +
				      " (seq_users_reg_activation_mail.nextval, ?, ?,  0) ";
        
        ps = conn.prepareStatement(sql);
        ps.setLong(1, userId);
        ps.setLong(2, suspendReasonId);
        ps.executeUpdate();        
		} finally {
			closeStatement(ps);
		}		
	}
}