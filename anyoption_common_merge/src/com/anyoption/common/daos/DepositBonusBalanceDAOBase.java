/**
 *
 */
package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.beans.base.DepositBonusBalanceBase;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;


public class DepositBonusBalanceDAOBase extends DAOBase {

	public static DepositBonusBalanceBase getDepositBonusBalanceByInvestment(Connection conn, DepositBonusBalanceMethodRequest request) throws SQLException {
		DepositBonusBalanceBase dbb = new DepositBonusBalanceBase();		
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =
                "SELECT i.user_id, " + 
                	  " i.amount*contracts_count as amount, " +
                	  " i.amount*contracts_count - nvl(bi.bonus_amount,0)*contracts_count cash_inv, " +
                	  " nvl(bi.bonus_amount,0)*contracts_count as bonus_amount " +
            	" FROM " +
            		" (select ii.*, " +
            			" row_number() over(partition by ii.user_id, ii.opportunity_id, ii.type_id, ii.amount, ii.current_level, ii.time_created, ii.time_settled, ii.group_inv_id order by ii.id desc) id_order, " +
            			" count(*) over(partition by ii.user_id, ii.opportunity_id, ii.type_id, ii.amount, ii.current_level, ii.time_created, ii.time_settled, ii.group_inv_id) contracts_count " +
            		  " from investments ii " +
            		  " where  nvl(ii.id, 0) LIKE '%%') i, " +
            		 "  (select * from bonus_investments where type_id = 1) bi " +
            	" WHERE i.user_id = ? " +
            		" and i.id = ? " +
            		" and i.id = bi.investments_id(+)  ";
            pstmt = conn.prepareStatement(sql);
        	pstmt.setLong(1, request.getUserId());
        	pstmt.setLong(2, request.getInvestmentId());
            rs = pstmt.executeQuery();

            if (rs.next()) {
            	dbb.setTotalBalance(rs.getLong("amount"));
            	dbb.setBonusBalance(rs.getLong("bonus_amount"));
            	dbb.setDepositCashBalance(rs.getLong("cash_inv"));
            	dbb.setUserId(request.getUserId());
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return dbb;
    }
	
	public static DepositBonusBalanceBase getDepositBonusBalanceByReturn(Connection conn, DepositBonusBalanceMethodRequest request) throws SQLException {
		DepositBonusBalanceBase dbb = new DepositBonusBalanceBase();		
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =
                "SELECT i.user_id, " + 
                	      " (i.amount + i.win - i.lose)*contracts_count as returns, " +
                	      " (i.amount + i.win - i.lose)*contracts_count - nvl(bi.bonus_amount,0)*contracts_count as cash_retunrs, " +
                	      " nvl(bi.bonus_amount,0)*contracts_count as bonus_returns " +
            	" FROM " +
            		" (select ii.*, " +
            			" row_number() over(partition by ii.user_id, ii.opportunity_id, ii.type_id, ii.amount, ii.current_level, ii.time_created, ii.time_settled, ii.group_inv_id order by ii.id desc) id_order, " +
            			" count(*) over(partition by ii.user_id, ii.opportunity_id, ii.type_id, ii.amount, ii.current_level, ii.time_created, ii.time_settled, ii.group_inv_id) contracts_count " +
            		  " from investments ii " +
            		  " where  nvl(ii.id, 0) LIKE '%%') i, " +
            		 "  (select * from bonus_investments where type_id = 2) bi " +
            	" WHERE i.user_id = ? " +
            		" and i.id = ? " +
            		" and i.id = bi.investments_id(+)  ";
            pstmt = conn.prepareStatement(sql);
        	pstmt.setLong(1, request.getUserId());
        	pstmt.setLong(2, request.getInvestmentId());
            rs = pstmt.executeQuery();

            if (rs.next()) {
            	dbb.setTotalBalance(rs.getLong("returns"));
            	dbb.setBonusBalance(rs.getLong("bonus_returns"));
            	dbb.setDepositCashBalance(rs.getLong("cash_retunrs"));
            	dbb.setUserId(request.getUserId());
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return dbb;
    }
	
	public static DepositBonusBalanceBase getDepositBonusBalanceByTotalBalance(Connection conn, DepositBonusBalanceMethodRequest request) throws SQLException {
		DepositBonusBalanceBase dbb = new DepositBonusBalanceBase();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =
            		"SELECT u.id, "+
            		  "u.balance, "+
            		  "NVL(bu.bonus_amount - bu.bonus_winnings,0)   AS bonus_balance, "+
            		  "u.balance           - NVL(bu.bonus_amount,0) AS cash_balance, "+
            		  "NVL(bu.bonus_winnings,0 )                    AS bonus_winnings "+
            		"FROM users u, "+
            		  "(SELECT user_id, "+
            		    "SUM(bu.adjusted_amount) AS bonus_amount, "+
            		    "CASE "+
            		      "WHEN SUM(NVL(bu.adjusted_amount,0) - NVL(bu.bonus_amount,0)) > 0 "+
            		      "THEN SUM(NVL(bu.adjusted_amount,0) - NVL(bu.bonus_amount,0)) "+
            		      "ELSE 0 "+
            		    "END AS bonus_winnings "+
            		  "FROM bonus_users bu "+
            		  "WHERE bu.bonus_state_id IN (2,3) "+
            		  "GROUP BY user_id "+
            		  ") bu "+
            		"WHERE u.id = bu.user_id(+) "+
            		"AND u.id = ? ";
            
            if(!request.isRequestedFromBE()) {
            	sql += " AND u.is_active = 1";
            }

            pstmt = conn.prepareStatement(sql);
        	pstmt.setLong(1, request.getUserId());
            rs = pstmt.executeQuery();

            if (rs.next()) {
            	dbb.setTotalBalance(rs.getLong("balance"));
            	dbb.setBonusBalance(rs.getLong("bonus_balance"));
            	dbb.setDepositCashBalance(rs.getLong("cash_balance"));
            	dbb.setBonusWinnings(rs.getLong("bonus_winnings"));
            	dbb.setUserId(request.getUserId());
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return dbb;
    }

}
