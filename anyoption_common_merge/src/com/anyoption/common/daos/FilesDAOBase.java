package com.anyoption.common.daos;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.managers.FilesManagerBase;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.CommonUtil;

import oracle.jdbc.OracleTypes;

/**
 * @author kirilim
 */
public class FilesDAOBase extends DAOBase {
	
	
//	public static void insertFile(Connection con, long issueActionId, long ccId, long type, long userId, long fileStatusId, String fileName, long writerId) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//
//		try {
//			String sql = " INSERT INTO "
//					+ " files "
//					+ " (id, user_id, file_type_id, time_created, file_name, utc_offset_created, cc_id, is_approved, file_status_id,  issue_action_id, writer_id) "
//					+ " VALUES "
//					+ " (seq_files.nextval, ?, ?, sysdate, ?, 'GMT+02:00', ?, 0, ?, ?, ?) ";
//
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, userId);
//			ps.setLong(2, type);
//			if (fileName != null) {
//				ps.setString(3, fileName);
//			} else {
//				ps.setString(3, null);
//			}
//			if (ccId != 0) {
//				ps.setLong(4, ccId);
//			} else {
//				ps.setString(4, null);
//			}
//			ps.setLong(5, fileStatusId);
//			ps.setLong(6, issueActionId);
//			ps.setLong(7, writerId);
//			ps.executeUpdate();
//		} finally {
//			closeStatement(ps);
//			closeResultSet(rs);
//		}
//	}
	
	public static long insertFile(Connection conn, Long issueActionId, long ccId, long type, long userId, long fileStatusId, String fileName, long writerId) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		long fileId = 0;
		try {
			String sql = "{call pkg_userfiles.create_file(?, ?, ?, ?, ?, ?, ?)}";
			cstmt = conn.prepareCall(sql);
			
			//cstmt.setLong(index++, file.getUserId());

			cstmt.setLong(index++, userId);
			cstmt.setLong(index++, type);
			if (fileName != null) {
				cstmt.setString(index++, fileName);
			} else {
				cstmt.setString(index++, "");
			}
			if (ccId != 0) {
				cstmt.setLong(index++, ccId);
			} else {
				cstmt.setNull(index++, OracleTypes.INTEGER);
			}
			cstmt.setLong(index++, fileStatusId);
			setStatementValue(issueActionId, index++, cstmt);
			cstmt.setLong(index++, writerId);
			
			cstmt.executeQuery();								
		} finally {
			closeStatement(cstmt);
		}			
		return fileId;
	}
	
	public static long insertUploadFile(Connection conn, long userId, long typeId, long writerId, String fileName, long fileStatusId) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		long fileId = 0;
		try {
			String sql = "{call pkg_userfiles.create_file_return_id(?, ?, ?, ?, ?, ?, ?)}";
			cstmt = conn.prepareCall(sql);
	
			cstmt.registerOutParameter(index++, Types.INTEGER);
			cstmt.setLong(index++, userId);
			cstmt.setLong(index++, typeId);
			cstmt.setLong(index++, writerId);
			cstmt.setString(index++, fileName);
			cstmt.setLong(index++, fileStatusId);
			cstmt.setLong(index++, writerId);
			
			cstmt.executeQuery();	
			fileId = cstmt.getInt(1);
		} finally {
			closeStatement(cstmt);
		}			
		return fileId;
	}
	
	public static long insertUploadFileWithStatus(Connection conn, 
												 long userId, 
												 long typeId, 
												 long writerId, 
												 String fileName, 
												 long fileStatusId, 
												 Long ccId,
												 boolean isApproved,
												 Long approvedWriterId,
												 boolean isReject,
												 Long rejectReasonId,
												 Long rejectWriterId) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		long fileId = 0;
		try {
			String sql = "{call pkg_userfiles.create_file_return_id(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, Types.INTEGER);
			cstmt.setLong(index++, userId);
			cstmt.setLong(index++, typeId);
			cstmt.setLong(index++, writerId);
			cstmt.setString(index++, fileName);
			cstmt.setLong(index++, fileStatusId);
			cstmt.setLong(index++, writerId);
			setStatementValue(ccId, index++, cstmt);			
			setStatementValue(isApproved, index++, cstmt);
			setStatementValue(approvedWriterId, index++, cstmt);
			setStatementValue(isReject, index++, cstmt);
			setStatementValue(rejectReasonId, index++, cstmt);
			setStatementValue(rejectWriterId, index++, cstmt);			
			cstmt.executeQuery();	
			fileId = cstmt.getInt(1);
		} finally {
			closeStatement(cstmt);
		}			
		return fileId;
	}

	/**
	 * This method returns the user files from types utility bill, id, passport, driver's license,
	 * credit card, bankwire. It does not return files from manually created issues.
	 * @param con
	 * @param userId
	 * @return a list of user files
	 * @throws SQLException
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidKeyException 
	 * @throws InvalidAlgorithmParameterException 
	 */
	public static	List<com.anyoption.common.beans.File>
			getUserFiles(Connection con, long userId, Locale locale)	throws SQLException, InvalidKeyException, IllegalBlockSizeException,
																		BadPaddingException, NoSuchAlgorithmException,
																		NoSuchPaddingException, InvalidAlgorithmParameterException {
		return getUserFiles(con, userId, 0, 0, locale);
	}
	
	public static	List<com.anyoption.common.beans.File>
			getUserFilesByType(	Connection con, long userId, long fileType,
								Locale locale)	throws SQLException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
												NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
		return getUserFiles(con, userId, fileType, 0, locale);
	}

	protected static	List<com.anyoption.common.beans.File>
				getUserFiles(	Connection con, long userId, long fileType, long ccId,
								Locale locale)	throws SQLException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
												NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<File> list = new ArrayList<File>();
		// TODO add the cc types front and back also
		String fileTypes = (fileType != 0)	? String.valueOf(fileType)
											: FileType.USER_ID_COPY+ ", " + FileType.PASSPORT + ", " + FileType.BANKWIRE_CONFIRMATION
												+ ", " + FileType.DRIVER_LICENSE + ", " + FileType.UTILITY_BILL + ", "
												+ FileType.CC_COPY_FRONT + ", " + FileType.CC_COPY_BACK;
		File vo = null;
		try {
			String sql=
					  " select " +
					  		" f.*," +
					  		" t.name," +
					  		" w.user_name, " +
					  		" cc.cc_number," +
					  		" cct.name cc_type_name " +
					  " from " +
					  		" File_types t," +
					  		" writers w, " +
					  		" Files f " +
					  			" left join credit_cards cc on cc.id = f.cc_id " +
					  				" left join credit_card_types cct on cct.id = cc.type_id " +
					  					" left join issue_actions ia on f.issue_action_id = ia.id " +
					  " where " +
					  		" f.file_type_id=t.id " +
					  		" and f.writer_id=w.id " +
					  		" and f.user_id = ?" +
					  		" and f.file_type_id in (" + fileTypes + ")";
//					  		" and (ia.writer_id = 1 or f.issue_action_id = 0)"; // to make sure we look for files from auto issue, not manually created
//					  		" and f.writer_id = 1"; 
			if (ccId != 0) {
				sql += " and f.cc_id = " + ccId;
			}
			sql += " order by " + " f.id ";
			
			if (fileType == FileType.COMPLAINT_FILE) {
				sql += " desc ";
			}

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);

			rs = ps.executeQuery();

			while (rs.next()) {
				vo = getVO(rs, locale);
				vo.setWriterName(rs.getString("user_name"));
				String ccNumber = rs.getString("cc_number");

				if (!CommonUtil.isParameterEmptyOrNull(ccNumber)) {
					String ccName = CommonUtil.getMessage(locale, rs.getString("cc_type_name"), null);
					try {
						ccNumber = AESUtil.decrypt(ccNumber);
					} catch (CryptoException ce) {
						throw new SQLException(ce.getMessage());
					}

					int l = ccNumber.length();
					if (l > 4) {
						vo.setCcName(ccNumber.substring(l - 4, l) + "  " + ccName);
					}
				}
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}

	/**
	 * This method returns the user files from specific type. It does not return files from manually created issues.
	 * @param con
	 * @param userId
	 * @param fileType
	 * @param ccId 
	 * @return the file or null if the file doesn't exists
	 * @throws SQLException
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidKeyException 
	 * @throws InvalidAlgorithmParameterException 
	 */
	public static	com.anyoption.common.beans.File
			getUserFileByType(	Connection con, long userId, long fileType, long ccId,
								Locale locale)	throws SQLException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
												NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
		List<? extends com.anyoption.common.beans.File> list = getUserFiles(con, userId, fileType, ccId, locale);
		return list.isEmpty() ? null : list.get(0);
	}

	public static void update(Connection con, File vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;

		try {

			String sql = " UPDATE "
							+ " Files "
						+ " SET "
							+ " file_type_id = ?, "
							+ " writer_id = ? , "
							+ " reference = ?, "
							+ " file_name = ?, "
							+ " is_approved = ?, "
							+ " time_updated = sysdate, "
							+ " file_status_id = ?, "
							+ " id_number = ?, "
							+ " first_name = ?, "
							+ " last_name = ?, "
//							+ " exp_date = ?, "
							+ " country_id = ?, "
							+ " is_color = ?, "
							+ " is_approved_control = ?, "
							+ " is_control_rejected = ?, "
							+ " is_support_rejected = ?, "
							+ " reject_reason_id = ?, "
							+ " comments = ?, "
							+ " CONTROL_REJECTION_REASON_ID = ?, "
							+ " CONTROL_REJECTION_COMMENTS = ?, " 
//							+ " rejection_writer_id = ?, "
							+ " uploader_id = ?, "
							+ " TIME_UPLOADED = ? ";
//							+ " rejection_date = ? ";
							
			if (vo.getCcId() != 0) {
				sql += " ,cc_id = ? ";
			}
			sql += " WHERE " + " id = ?";

			ps = con.prepareStatement(sql);

			ps.setLong(index++, Long.valueOf(vo.getFileTypeId()).longValue());
			ps.setLong(index++, vo.getWriterId());
			ps.setString(index++, vo.getReference());
			ps.setString(index++, vo.getFileName());
			ps.setInt(index++, vo.isApproved() ? 1 : 0);
			//ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(vo.getTimeUpdated()));
			ps.setLong(index++, vo.getFileStatusId());
			ps.setString(index++, vo.getNumber());
			ps.setString(index++, CommonUtil.capitalizeFirstLetters(vo.getFirstName()));
			ps.setString(index++, CommonUtil.capitalizeFirstLetters(vo.getLastName()));
//			ps.setTimestamp(index++, vo.getExpDateTimestamp());
			ps.setLong(index++, vo.getCountryId());
			ps.setInt(index++, vo.isColor() ? 1 : 0);
			ps.setInt(index++, vo.isControlApproved() ? 1 : 0);
			ps.setInt(index++, vo.isControlReject() ? 1 : 0);
			ps.setInt(index++, vo.isSupportReject() ? 1 : 0);
			ps.setInt(index++, vo.getRejectReason());
			ps.setString(index++, vo.getComment());
			ps.setInt(index++, vo.getControlRejectReason());
			ps.setString(index++, vo.getControlRejectComment());
//			ps.setLong(index++, vo.getRejectionWriterId());
			ps.setLong(index++, vo.getUploaderId());
			ps.setTimestamp(index++, convertToTimestamp(vo.getUploadDate()));
//			ps.setTimestamp(index++, vo.getRejectionDateTimestamp());
			if (vo.getCcId() != 0) {
				ps.setLong(index++, vo.getCcId());
				ps.setLong(index++, vo.getId());
			} else {
				ps.setLong(index++, vo.getId());
			}

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}

	}
	
	public static void updateWhenUpload(Connection con, File vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;

		try {

			String sql = " UPDATE "
							+ " Files "
						+ " SET "
							+ " file_type_id = ?, "
							+ " writer_id = ? , "
							+ " reference = ?, "
							+ " file_name = ?, "
							+ " is_approved = ?, "
							+ " time_updated = sysdate, "
							+ " file_status_id = ?, "
							+ " id_number = ?, "
							+ " first_name = ?, "
							+ " last_name = ?, "
							+ " exp_date = ?, "
							+ " country_id = ?, "
							+ " is_color = ?, "
							+ " is_approved_control = ?, "
							+ " is_control_rejected = ?, "
							+ " is_support_rejected = ?, "
							+ " reject_reason_id = ?, "
							+ " comments = ?, "
							+ " CONTROL_REJECTION_REASON_ID = ?, "
							+ " CONTROL_REJECTION_COMMENTS = ?, "
							+ " rejection_writer_id = ?, "
							+ " uploader_id = ?, "
							+ " TIME_UPLOADED = ?, "
							+ " rejection_date = ?, "							
							+ " SUPPORT_APPROVED_WRITER_ID = ?, "
							+ " TIME_SUPPORT_APPROVED = ?, "
							+ " CONTROL_APPROVED_WRITER_ID = ?, "
							+ " TIME_CONTROL_APPROVED  = ?, "							
							+ " CONTROL_REJECT_WRITER_ID = ?, "
							+ " TIME_CONTROL_REJECTED = ? ";
							
			if (vo.getCcId() != 0) {
				sql += " ,cc_id = ? ";
			}
			sql += " WHERE " + " id = ?";

			ps = con.prepareStatement(sql);

			ps.setLong(index++, Long.valueOf(vo.getFileTypeId()).longValue());
			ps.setLong(index++, vo.getWriterId());
			ps.setString(index++, vo.getReference());
			ps.setString(index++, vo.getFileName());
			ps.setInt(index++, vo.isApproved() ? 1 : 0);
			//ps.setTimestamp(index++,CommonUtil.convertToTimeStamp(vo.getTimeUpdated()));
			ps.setLong(index++, vo.getFileStatusId());
			ps.setString(index++, vo.getNumber());
			ps.setString(index++, CommonUtil.capitalizeFirstLetters(vo.getFirstName()));
			ps.setString(index++, CommonUtil.capitalizeFirstLetters(vo.getLastName()));
			ps.setTimestamp(index++, convertToTimestamp(vo.getExpDate()));
			ps.setLong(index++, vo.getCountryId());
			ps.setInt(index++, vo.isColor() ? 1 : 0);
			ps.setInt(index++, vo.isControlApproved() ? 1 : 0);
			ps.setInt(index++, vo.isControlReject() ? 1 : 0);
			ps.setInt(index++, vo.isSupportReject() ? 1 : 0);
			ps.setInt(index++, vo.getRejectReason());
			ps.setString(index++, vo.getComment());			
			ps.setInt(index++, vo.getControlRejectReason());
			ps.setString(index++, vo.getControlRejectComment());			
			ps.setLong(index++, vo.getRejectionWriterId());
			ps.setLong(index++, vo.getUploaderId());
			ps.setTimestamp(index++, convertToTimestamp(vo.getUploadDate()));
			ps.setTimestamp(index++, convertToTimestamp(vo.getRejectionDate()));
			ps.setLong(index++, vo.getSupportApprovedWriterId());
			ps.setTimestamp(index++, convertToTimestamp(vo.getTimeSupportApproved()));
			ps.setLong(index++, vo.getControlApprovedWriterId());
			ps.setTimestamp(index++, convertToTimestamp(vo.getTimeControlApproved()));			
			ps.setLong(index++, vo.getControlRejectWriterId());
			ps.setTimestamp(index++, convertToTimestamp(vo.getTimeControlRejected()));
			if (vo.getCcId() != 0) {
				ps.setLong(index++, vo.getCcId());
				ps.setLong(index++, vo.getId());
			} else {
				ps.setLong(index++, vo.getId());
			}

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}

	}

	protected static File getVO(ResultSet rs, Locale locale) throws SQLException {
		File vo = new File();
		vo.setId(rs.getLong("id"));
		vo.setUserId(rs.getLong("user_id"));
		vo.setFileTypeId(rs.getLong("file_type_id"));
		vo.setWriterId(rs.getLong("writer_id"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
		vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
		vo.setReference(rs.getString("reference"));
		vo.setFileTypeName(CommonUtil.getMessage(locale, rs.getString("name"), null));
		vo.setFileName(rs.getString("file_name"));
		vo.setCcId(rs.getLong("cc_id"));
		vo.setApproved(rs.getInt("is_approved") == 1 ? true : false);
		vo.setColor(rs.getInt("is_color") == 1 ? true : false);
		vo.setControlApproved(rs.getInt("is_approved_control") == 1 ? true : false);
		vo.setControlReject(rs.getInt("is_control_rejected") == 1 ? true : false);
		vo.setSupportReject(rs.getInt("is_support_rejected") == 1 ? true : false);
		vo.setFileStatusId(rs.getInt("file_status_id"));
		vo.setTimeUpdated(convertToDate(rs.getTimestamp("time_updated")));
		vo.setIssueActionsId(rs.getLong("issue_action_id"));
		vo.setNumber(rs.getString("id_number"));
		vo.setFirstName(rs.getString("first_name"));
		vo.setLastName(rs.getString("last_name"));
		vo.setExpDate(convertToDate(rs.getTimestamp("exp_date")));
		vo.setCountryId(rs.getLong("country_id"));
		vo.setRejectReason(rs.getInt("reject_reason_id"));
		vo.setComment(rs.getString("comments"));
		vo.setControlRejectReason(rs.getInt("CONTROL_REJECTION_REASON_ID"));
		vo.setControlRejectComment(rs.getString("CONTROL_REJECTION_COMMENTS"));			
		vo.setRejectionWriterId(rs.getLong("rejection_writer_id"));
		vo.setRejectionDate(convertToDate(rs.getTimestamp("rejection_date")));
		vo.setUploaderId(rs.getLong("uploader_id"));
		vo.setUploadDate(rs.getTimestamp("TIME_UPLOADED"));
		vo.setSupportApprovedWriterId(rs.getLong("SUPPORT_APPROVED_WRITER_ID"));
		vo.setTimeSupportApproved(rs.getTimestamp("TIME_SUPPORT_APPROVED"));
		vo.setControlApprovedWriterId(rs.getLong("CONTROL_APPROVED_WRITER_ID"));
		vo.setTimeControlApproved(rs.getTimestamp("TIME_CONTROL_APPROVED"));
		vo.setControlRejectWriterId(rs.getLong("CONTROL_REJECT_WRITER_ID"));
		vo.setTimeControlRejected(rs.getTimestamp("TIME_CONTROL_REJECTED"));						
		if (null != vo.getExpDate()){
			FilesManagerBase.setExpDateParams(vo);
		}
		vo.setCurrent(rs.getLong("is_current") == 1 ? true : false);
		return vo;
	}
		 
	/**
	 * This method returns the key of the reject reason (if any) and the key of the file name for a file 
	 * @param con
	 * @param fileId
	 * @return HashMap<String, String> with file name and file reject reason
	 * @throws SQLException
	 */
	  public static HashMap<String, String> getReasonAndTypeByFileId(Connection con, long fileId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  HashMap<String, String> map = new HashMap<String, String>();
		  try {
				String sql = "SELECT " + 
									" frr.reason_mail_param, " +
									" ft.name " +
							 " FROM " + 
									" files f " +
										" LEFT JOIN file_reject_reasons frr ON frr.id = decode(f.control_rejection_reason_id, 0, "
																										+ " reject_reason_id,"
																											+ " control_rejection_reason_id) " +
										" LEFT JOIN file_types ft ON ft.id = f.file_type_id " +
							 " WHERE " +
							 		" f.id = ?";

				ps = con.prepareStatement(sql);
				ps.setLong(1, fileId);
				rs = ps.executeQuery();
				while (rs.next()) {
					map.put("reason", rs.getString("reason_mail_param"));
					map.put("type", rs.getString("name"));	
				}
			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}
			return map;
	  }	  
	  
	public static void clearKSFileData(Connection conn, long fileId) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			String sql = "{call pkg_userfiles.reupload_ks_file(?)}";
			cstmt = conn.prepareCall(sql);				
			cstmt.setLong(index++, fileId);
			
			cstmt.execute();
		} finally {
			closeStatement(cstmt);
		}			
	}
	
	public static ArrayList<File> getUserFilesByIdNoIssue(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<File> uf = new ArrayList<File>();
		try {
			String sql = "SELECT " + "* " + "FROM  " + "files "
					+ "WHERE user_id = ?" + "AND issue_action_id is null";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				uf.add(new File(rs.getLong("id"), rs.getLong("file_status_id"), rs.getLong("file_type_id")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return uf;
	}
	
	public static ArrayList<com.anyoption.common.beans.File> getUserFilesByIdNoIssue(Connection con, long userId, Locale locale) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<com.anyoption.common.beans.File> uf = new ArrayList<com.anyoption.common.beans.File>();
		try {

			String sql = "SELECT " + "* " + "FROM  " + "files "
					+ "WHERE user_id = ?" + "AND issue_action_id is null";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();

			if (rs.next()) {
				com.anyoption.common.beans.File f = getVO(rs, locale);
				uf.add(f);
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return uf;
	}
	
	public static void updateFileRecords(Connection con, File file,
			long issueActionId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "UPDATE " + "Files " + "set issue_action_id = ? "
					+ "WHERE " + "id= ?";

			ps = con.prepareStatement(sql);
			ps.setLong(1, issueActionId);
			ps.setLong(2, file.getId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}
	
	public static boolean isExistFileType(Connection conn, long userId, long fileType, long ccId) throws SQLException {
		CallableStatement cstmt = null;
		Boolean res ;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_userfiles.is_exist_file_type( o_is_exist   => ? " +
																			" ,i_user_id   => ? " +
																			" ,i_file_type => ? " +
																			" ,i_cc_id => ? )}"); 																					   
			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			setStatementValue(userId, index++, cstmt);
			setStatementValue(fileType, index++, cstmt);
			setStatementValue(ccId, index++, cstmt);
			cstmt.execute();			
			res = cstmt.getBoolean(1);			
		} finally {
			closeStatement(cstmt);
		}
		return res;
	}
}