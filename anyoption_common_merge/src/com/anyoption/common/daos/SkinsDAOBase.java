package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import com.anyoption.common.beans.Skin;
import com.anyoption.common.enums.SkinGroup;

public class SkinsDAOBase extends DAOBase {
	
	/**
	 * Return Skins instance by id
	 *
	 * @param con
	 *            connection to db
	 * @param id
	 *            skin id
	 * @return Skins object
	 * @throws SQLException
	 */
	public static Skin getById(Connection con, int id) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Skin skin = null;

		try {

			String sql = "select * from skins " + "where id = ? ";

			ps = con.prepareStatement(sql);
			ps.setInt(1, id);

			rs = ps.executeQuery();

			if (rs.next()) {
				skin = getVO(rs);
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return skin;

	}


    public static HashMap<Long, Long> getSkinCombination(Connection conn) throws SQLException {
    	HashMap<Long, Long>  ht = new HashMap<Long, Long>();
        Statement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "s.id, s.default_combination_id " +
                "FROM " +
                    "skins s";
            pstmt = conn.createStatement();
            rs = pstmt.executeQuery(sql);
            while (rs.next()) {
                ht.put(rs.getLong("id"), rs.getLong("default_combination_id"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return ht;
    }
    
    public static ArrayList<Long> getSkinsId(Connection conn) throws SQLException {
    	ArrayList<Long>  list = new ArrayList<Long>();
        Statement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "s.id " +
                "FROM " +
                    "skins s";
            pstmt = conn.createStatement();
            rs = pstmt.executeQuery(sql);
            while (rs.next()) {
                list.add(rs.getLong("id"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }
    /**
     * Get skin_id by country_id and url_id
     *
     * @param con connection to Db
     * @param countryId country id
     * @param urlId url id
     * @return skin id
     * @throws SQLException
     */
    public static int getSkinIdByCountryAndUrl(Connection con, long countryId, int urlId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int skinId = 0;
        try {
            String sql =
                "SELECT " +
                    "skin_id " +
                "FROM " +
                    "skin_url_country_map "+
                "WHERE " +
                    "country_id = ? AND " +
                    "url_id = ?";
            ps = con.prepareStatement(sql);
            ps.setLong(1, countryId);
            ps.setInt(2, urlId);
            rs = ps.executeQuery();
            if (rs.next()) {
                skinId = rs.getInt("skin_id");
            }
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return skinId;
    }
        /**
     * Return all skins
     *
     * @param conn DB connection
     * @return <code>Hashtable<Long, Skin></code> of all skins
     * @throws SQLException
     */
    public static Hashtable<Long, Skin> getAll(Connection conn) throws SQLException {
        Hashtable<Long, Skin> ht = new Hashtable<Long, Skin>();
        Statement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "skins";
            pstmt = conn.createStatement();
            rs = pstmt.executeQuery(sql);
            while (rs.next()) {
                ht.put(rs.getLong("id"), getVO(rs));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return ht;
    }
    
    /**
     * Fill Skins object
     *
     * @param rs ResultSet
     * @return <code>Skin</code> object
     * @throws SQLException
     */
    protected static Skin getVO(ResultSet rs) throws SQLException {
        Skin vo = new Skin();
        vo.setId(rs.getInt("id"));
        vo.setName(rs.getString("name"));
        vo.setDisplayName(rs.getString("display_name"));
        vo.setDefaultCountryId(rs.getInt("default_country_id"));
        vo.setDefaultLanguageId(rs.getInt("default_language_id"));
        vo.setDefaultXorId(rs.getInt("default_xor_id"));
        vo.setDefaultCurrencyId(rs.getInt("default_currency_id"));
        vo.setDefaultCombinationId(rs.getLong("default_combination_id"));
        vo.setSupportStartTime(rs.getString("support_start_time"));
        vo.setSupportEndTime(rs.getString("support_end_time"));
        vo.setTierQualificationPeriod(rs.getLong("tier_qualification_period"));
        vo.setBusinessCaseId(rs.getLong("business_case_id"));
        vo.setSupportEmail(rs.getString("support_email"));
        vo.setLocale(rs.getString("locale_subdomain"));
        vo.setIsRegulated(rs.getInt("is_regulated") == 1 ? true : false);
        vo.setSkinGroup(SkinGroup.getById(rs.getLong("SKIN_GROUP_ID")));
        vo.setActive(rs.getInt("is_active") == 1 ? true : false);
        return vo;
    }
    

    public static int getDefaultLanguageIdBySkinId(Connection con, long skinId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int defaultLanguageId = 0;
        try {
            String sql =
                "SELECT " +
                    "default_language_id " +
                "FROM " +
                    "skins "+
                "WHERE " +
                    "id = ? " ;

            ps = con.prepareStatement(sql);
            ps.setLong(1, skinId);

            rs = ps.executeQuery();
            if (rs.next()) {
            	defaultLanguageId = rs.getInt("default_language_id");
            }
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return defaultLanguageId;
    }
    
    
	public static final long VISIBLE_TYPE_REG = 1;
	public static final long VISIBLE_TYPE_NON_REG = 2;
	public static final long VISIBLE_TYPE_REG_AND_NON_REG = 3;
	public static final long VISIBLE_TYPE_NON_REG_GOLDBEAM = 4;
	
    public static HashMap<Long, ArrayList<Long>> getVisibleSkins(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        HashMap<Long, ArrayList<Long>> res = new HashMap<>();
        res.put(Skin.SKIN_LIST_OB, new ArrayList<>());
        res.put(Skin.SKIN_LIST_AOPS, new ArrayList<>());
        res.put(Skin.SKIN_LIST_BLOCK_COUNTRY, new ArrayList<>());
        res.put(Skin.SKIN_LIST_GOLDBEAM, new ArrayList<>());
        
        try {
            String sql =
                "SELECT " +
                    "id, visible_type_id " +
                "FROM " +
                    "skins "+
                "WHERE visible_type_id != 0" +
                    " order by 1 " ;

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while (rs.next()) {
            	long skinId = rs.getLong("id");
            	long visibleTypeId = rs.getLong("visible_type_id");
            	
            	if(visibleTypeId == VISIBLE_TYPE_REG){
            		res.get(Skin.SKIN_LIST_OB).add(skinId);
            	}
            	
            	if(visibleTypeId == VISIBLE_TYPE_NON_REG){
            		res.get(Skin.SKIN_LIST_AOPS).add(skinId);
            		res.get(Skin.SKIN_LIST_BLOCK_COUNTRY).add(skinId);
            	}
            	
            	if(visibleTypeId == VISIBLE_TYPE_REG_AND_NON_REG){
            		res.get(Skin.SKIN_LIST_OB).add(skinId);
            		res.get(Skin.SKIN_LIST_AOPS).add(skinId);
            	}
            	
            	if(visibleTypeId == VISIBLE_TYPE_NON_REG_GOLDBEAM){
            		res.get(Skin.SKIN_LIST_GOLDBEAM).add(skinId);
            	}
            }
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return res;
    }
}