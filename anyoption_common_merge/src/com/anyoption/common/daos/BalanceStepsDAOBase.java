/**
 * 
 */
package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.anyoption.common.beans.BalanceStep;
import com.anyoption.common.beans.base.BalanceStepPredefValue;

/**
 * General DAO class for balance steps
 * 
 * @author kirilim
 */
public class BalanceStepsDAOBase extends DAOBase {
	public static List<BalanceStep> getBalanceSteps(Connection con, long currencyId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<BalanceStep> list = new ArrayList<BalanceStep>();
		BalanceStep balanceStep = null;
		long stepNumber = 1;
		long oldId = 0;
		try {
			String sql = "SELECT ubs.*, pag.amount, pag.step_priority from users_balance_steps ubs, predefined_amount_groups pag"
							+ " where ubs.id = pag.users_balance_step_id and ubs.currency_id = ? order by ubs.amount_from, amount";
			ps = con.prepareStatement(sql);
			ps.setLong(1, currencyId);
			rs = ps.executeQuery();
			while (rs.next()) {
				long newId = rs.getLong("id");
				if (oldId != newId) {
					balanceStep = getVO(rs);
					balanceStep.setCurrencyId(currencyId);
					balanceStep.setId(stepNumber++);
					balanceStep.setPredefValues(setPredefAmountValues(rs));
					list.add(balanceStep);
					oldId = newId;
				} else {
					balanceStep.getPredefValues().add(getPredefAmountValue(rs));
				}
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return list;
	}

	public static Map<Long, BalanceStep> getCurrencyLogoutDefaultStep(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<Long, BalanceStep> map = new HashMap<Long, BalanceStep>();
		BalanceStep balanceStep = null;
		long oldCurrencyId = 0;
		long stepNumber = 1;
		try {
			String sql = "SELECT ubs.*, pag.amount, pag.step_priority from users_balance_steps ubs, predefined_amount_groups pag"
							+ " where ubs.id = pag.users_balance_step_id and ubs.is_logout_default = 1 order by ubs.currency_id, ubs.amount_from, amount";
			ps = con.prepareStatement(sql);			
			rs = ps.executeQuery();
			while (rs.next()) {
				long currencyId = rs.getLong("currency_id");
				if (oldCurrencyId != currencyId) {
					balanceStep = getVO(rs);
					balanceStep.setCurrencyId(currencyId);
					balanceStep.setId(stepNumber++);
					balanceStep.setPredefValues(setPredefAmountValues(rs));
					map.put(currencyId, balanceStep);
					oldCurrencyId = currencyId;
				} else {
					map.get(currencyId).getPredefValues().add(getPredefAmountValue(rs));
				}
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return map;
	}

	public static BalanceStep getNonDepositorsDefaultStep(Connection con, Long currencyId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		BalanceStep balanceStep = null;
		long stepNumber = 1;
		try {
			String sql = "SELECT ubs.*, pag.amount, pag.step_priority from users_balance_steps ubs, predefined_amount_groups pag"
							+ " where ubs.id = pag.users_balance_step_id and ubs.currency_id = ? and ubs.is_non_depositor = 1 order by ubs.amount_from, amount";
			ps = con.prepareStatement(sql);
			ps.setLong(1, currencyId);
			rs = ps.executeQuery();
			if (rs.next()) {
				balanceStep = getVO(rs);
				balanceStep.setCurrencyId(currencyId);
				balanceStep.setId(stepNumber++);
				balanceStep.setPredefValues(setPredefAmountValues(rs));
			} 
			while (rs.next()) {
				balanceStep.getPredefValues().add(getPredefAmountValue(rs));
			}
		} finally {
				closeResultSet(rs);
				closeStatement(ps);
		}
		
		return balanceStep;
	}

	protected static List<BalanceStepPredefValue> setPredefAmountValues(ResultSet rs) throws SQLException {
		List<BalanceStepPredefValue> predefAmountValues = new ArrayList<BalanceStepPredefValue>();
		predefAmountValues.add(getPredefAmountValue(rs));
		return predefAmountValues;
	}

	protected static BalanceStepPredefValue getPredefAmountValue(ResultSet rs) throws SQLException {
		BalanceStepPredefValue bspv = new BalanceStepPredefValue();
		bspv.setPredefValue(rs.getLong("amount"));
		bspv.setPriority(rs.getInt("step_priority"));
		return bspv;
	}

	private static BalanceStep getVO(ResultSet rs) throws SQLException {
		BalanceStep vo = new BalanceStep();
		vo.setRealStepId(rs.getLong("id"));
		vo.setAvgBalanceMinRange(rs.getLong("amount_from"));
		vo.setAvgBalanceMaxRange(rs.getLong("amount_to"));
		vo.setDefaultAmountValue(rs.getLong("default_amount"));
		return vo;
	}
}