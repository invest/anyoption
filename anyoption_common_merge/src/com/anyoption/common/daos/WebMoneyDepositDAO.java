package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.bl_vos.WebMoneyDeposit;

/**
 * @author EranL
 *
 */
public class WebMoneyDepositDAO extends DAOBase{
	/**
	 * Insert WebMoney Deposit
	 * @param con
	 * @param vo
	 * @throws SQLException
	 */
	  public static void insert(Connection con, WebMoneyDeposit vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {
			  String sql = "INSERT into " +
			  			   		"webmoney_deposit(id, transaction_id, time_created, lmi_payment_amount, lmi_mode, lmi_sys_invs_no, " +
			  			   							" lmi_sys_trans_no, lmi_payer_purse, lmi_payer_wm, lmi_hash, lmi_sys_trans_date) " +
			  			   "VALUES " +
			  			   		"(seq_webmoney_deposit.nextval,?,sysdate,?,?,?,?,?,?,?,?)";

			  ps = con.prepareStatement(sql);

			  ps.setLong(1, vo.getTransactionId());
			  ps.setString(2, vo.getLmi_payment_amount());
			  ps.setString(3, vo.getLmi_mode());
			  ps.setString(4, vo.getLmi_sys_invs_no());
			  ps.setString(5, vo.getLmi_sys_trans_no());
			  ps.setString(6, vo.getLmi_payer_purse());
			  ps.setString(7, vo.getLmi_payer_wm());
			  ps.setString(8, vo.getLmi_hash());
			  ps.setString(9, vo.getLmi_sys_trans_date());
			  ps.executeUpdate();
			  vo.setId(getSeqCurValue(con,"seq_webmoney_deposit"));
		  } finally {
			  closeStatement(ps);
		  }
	  }

	/**
	 * Get WebMoney deposit details (WebMoney response after success URL)
	 * @param con
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static WebMoneyDeposit get(Connection con,long id) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  WebMoneyDeposit webMoneyDeposit = null;
		  try {
			  String sql = " SELECT " +
			  			   		" * " +
			  			   " FROM " +
			  			   		" webmoney_deposit wd  " +
			  			   " WHERE " +
			  			   		" wd.transaction_id = ? ";
			  ps = con.prepareStatement(sql);
			  ps.setLong(1, id);
			  rs = ps.executeQuery();
			  if (rs.next()) {
				  webMoneyDeposit = new WebMoneyDeposit();
				  webMoneyDeposit.setLmi_sys_invs_no(rs.getString("LMI_SYS_INVS_NO"));
				  webMoneyDeposit.setLmi_sys_trans_no(rs.getString("LMI_SYS_TRANS_NO"));
				  webMoneyDeposit.setLmi_payer_purse(rs.getString("LMI_PAYER_PURSE"));
				  webMoneyDeposit.setLmi_payer_wm(rs.getString("LMI_PAYER_WM"));
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return webMoneyDeposit;
	  }
}