/**
 *
 */
package com.anyoption.common.daos;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.anyoption.common.beans.EmirReport;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketBean;
import com.anyoption.common.beans.MarketConfig;
import com.anyoption.common.beans.MarketFormula;
import com.anyoption.common.beans.MarketNameBySkin;
import com.anyoption.common.beans.MarketPauseDetails;
import com.anyoption.common.beans.MarketPriority;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.base.MarketGroup;
import com.anyoption.common.util.ConstantsBase;

import oracle.jdbc.OracleTypes;

/**
 * @author pavelhe
 *
 */
public class MarketsDAOBase extends DAOBase {

	public static Hashtable<Long, Market> getAll(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Hashtable<Long, Market> ht = new Hashtable<Long, Market>();
        try {
            String sql =
                "SELECT " +
                    "A.*, " +
                    "(SELECT " +
                        "COUNT(1) " +
                    "FROM " +
                        "opportunity_templates " +
                    "WHERE " +
                        "A.id = market_id AND " +
                        "scheduled in (3, 4) AND " +
                        "is_active = 1) AS haslongterm " +
                "FROM " +
                    "markets A";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            Market m = null;
            while (rs.next()) {
                m = getMarketVO(rs);
                m.setHasLongTerm(rs.getBoolean("haslongterm"));
                m.setLimitedForHighAmounts(rs.getBoolean("LIMITED_HIGH_AMNT"));
                ht.put(rs.getLong("id"), m);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return ht;
    }
	
    public static ArrayList<Market> getMarketsByType(Connection con, long type) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Market> list = new ArrayList<Market>();
        try {
            String sql = " SELECT " +
                            " id, name, is_suspended " +
                         " FROM " +
                            " markets " +
                         " WHERE " +
                            " type_id = ?";
            
            ps = con.prepareStatement(sql);
            ps.setLong(1, type);        
            rs = ps.executeQuery();
            
            while (rs.next()) {
            	Market vo = new Market();
				vo.setId(rs.getLong("id"));
				vo.setName(rs.getString("name"));
				vo.setSuspended(rs.getBoolean("is_suspended"));
				list.add(vo);				
           }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return list;
    }
    
    public static Map<Long, Map<Long, String>> getMarketNamesByTypeAndSkin(Connection con, long skinId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Map<Long, Map<Long, String>> map = new HashMap<Long, Map<Long,String>>();
        try {
            String sql = " select " +
            			 		" mns.name, " +
            			 		" m.id, " +
            			 		" m.type_id " +
            			 " from " +
            			 		" markets m, " +
            			 		" market_name_skin mns " +
            			 " where " + 
            			 		" m.id = mns.market_id " +
            			 		" and mns.skin_id = ? " + 
            			 		" and m.type_id <> ? ";
            
            ps = con.prepareStatement(sql);
            ps.setLong(1, skinId);
            ps.setLong(2, Market.PRODUCT_TYPE_BINARY0100);
            rs = ps.executeQuery();
            
            while (rs.next()) {
            	if (map.get(rs.getLong("type_id")) == null) {
            		Map<Long, String> mapNames = new HashMap<Long, String>();
            		map.put(rs.getLong("type_id"), mapNames);
            	}
            	map.get(rs.getLong("type_id")).put(rs.getLong("id"), rs.getString("name"));
           }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return map;
    }

	public static Market getMarketLimitFromAmount(Connection con, long marketId) throws SQLException {
		PreparedStatement ps = null;
        ResultSet rs = null;
        Market market = new Market();
        try {
        	String sql = "SELECT LIMITED_HIGH_AMNT, LIMITED_FROM_AMNT "
        					+ "FROM markets "
        					+ "WHERE id = ?";
        	ps = con.prepareStatement(sql);
        	ps.setLong(1, marketId);
        	rs = ps.executeQuery();
        	if (rs.next()) {
        		market.setLimitedForHighAmounts(rs.getBoolean("LIMITED_HIGH_AMNT"));
        		market.setLimitedFromAmount(rs.getLong("LIMITED_FROM_AMNT"));
        	}
        } finally {
        	closeResultSet(rs);
            closeStatement(ps);
        }
        return market;
	}

	public static Market getMarketVO(ResultSet rs) throws SQLException {
        Market vo = new Market();
        vo.setDecimalPoint(new Long(rs.getLong("decimal_Point")));
        vo.setDisplayNameKey(rs.getString("display_name"));
        vo.setFeedName(rs.getString("feed_Name"));
        vo.setId(rs.getLong("id"));
        vo.setName(rs.getString("name"));
        vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
        vo.setWriterId(rs.getLong("writer_id"));
        vo.setExchangeId(rs.getLong("exchange_Id"));
        vo.setInvestmentLimitsGroupId(rs.getLong("INVESTMENT_LIMITS_GROUP_ID"));
        vo.setUpdatesPause(rs.getInt("UPDATES_PAUSE"));
        vo.setSignificantPercentage(rs.getBigDecimal("SIGNIFICANT_PERCENTAGE"));
        vo.setRealSignificantPercentage(rs.getBigDecimal("REAL_SIGNIFICANT_PERCENTAGE"));
        vo.setRealUpdatesPause(rs.getInt("REAL_UPDATES_PAUSE"));
        vo.setRandomCeiling(rs.getBigDecimal("RANDOM_CEILING"));
        vo.setRandomFloor(rs.getBigDecimal("RANDOM_FLOOR"));
        vo.setFirstShiftParameter(rs.getBigDecimal("FIRST_SHIFT_PARAMETER"));
        vo.setAverageInvestments(rs.getBigDecimal("AVERAGE_INVESTMENTS"));
        vo.setPercentageOfAverageInvestments(rs.getBigDecimal("PERCENTAGE_OF_AVG_INVESTMENTS"));
        vo.setFixedAmountForShifting(rs.getBigDecimal("FIXED_AMOUNT_FOR_SHIFTING"));
        vo.setTypeOfShifting(rs.getLong("TYPE_OF_SHIFTING"));
        vo.setDisableAfterPeriod(rs.getLong("disable_after_period"));
        vo.setCurrentLevelAlertPerc(rs.getBigDecimal("CURENT_LEVEL_ALERT_PERC"));
        vo.setAcceptableDeviation(rs.getBigDecimal("ACCEPTABLE_LEVEL_DEVIATION"));
        vo.setHasFutureAgreement(rs.getInt("IS_HAS_FUTURE_AGREEMENT") == 1);
        vo.setNextAgreementSameDay(rs.getInt("IS_NEXT_AGREEMENT_SAME_DAY") == 1);
        vo.setBannersPriority(rs.getInt("BANNERS_PRIORITY"));
        vo.setAmountForDev2(rs.getFloat("AMOUNT_FOR_DEV2"));
        vo.setSecForDev2(rs.getInt("SEC_FOR_DEV2"));
        vo.setSecBetweenInvestments(rs.getInt("SEC_BETWEEN_INVESTMENTS"));
        vo.setMaxExposure(rs.getInt("MAX_EXPOSURE"));
        vo.setInsurancePremiaPercent(rs.getDouble("Insurance_Premia_Percent"));
        vo.setInsuranceQualifyPercent(rs.getDouble("insurance_qualify_percent"));
        vo.setRollUpQualifyPercent(rs.getDouble("roll_up_qualify_percent"));
        vo.setRollUp(rs.getInt("is_roll_up") == 1 ? true : false);
        vo.setGoldenMinutes((rs.getInt("is_golden_minutes")) == 1 ? true : false);
        vo.setRollUpPremiaPercent(rs.getDouble("roll_up_premia_percent"));
        vo.setOptionPlus(rs.getInt("option_plus_market_id") > 0);
        vo.setOptionPlusFee(rs.getLong("option_plus_fee"));
        vo.setAcceptableDeviation3(rs.getBigDecimal("acceptable_level_deviation_3"));
        vo.setSecBetweenInvestmentsSameIp(rs.getInt("sec_between_investments_ip"));
        vo.setAmountForDev3(rs.getFloat("amount_for_dev3"));
        vo.setDecimalPointSubtractDigits(rs.getInt("decimal_point_subtract_digits"));
        vo.setAmountForDev2Night(rs.getFloat("amount_for_dev2_night"));
        vo.setSecForDev2Mobile(rs.getInt("SEC_FOR_DEV2_MOB"));
        vo.setSecBetweenInvestmentsMobile(rs.getInt("SEC_BETWEEN_INVESTMENTS_MOB"));
        vo.setSecBetweenInvestmentsSameIpMobile(rs.getInt("SEC_BETWEEN_INVESTMENTS_IP_MOB"));
        vo.setInstantSpread(rs.getDouble("INSTANT_SPREAD"));
        vo.setProductTypeId(rs.getLong("type_id"));
        return vo;
    }
	
//	public static ArrayList<Market> getMarkets(Connection con) throws SQLException {
//
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<Market> list = new ArrayList<Market>();
//		try {
//			String sql = "select m.*, ot.TIME_FIRST_INVEST, ot.TIME_EST_CLOSING " +
//					 ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=4 and ot1.is_active=1 )  as monthly " +
//					 ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=3 and ot1.is_active=1 )  as weekly " +
//					 ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=2 and ot1.is_active=1 )  as dayly " +
//					 ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=1 and ot1.is_active=1 )  as hourly " +
//					 "from markets m, opportunity_templates ot " +
//					 "where ot.scheduled=2 and ot.is_active=1 and ot.is_full_day=1 and m.id = ot.market_id " +
//					 "order by m.id";
//
//			ps = con.prepareStatement(sql);
//			rs = ps.executeQuery();
//			
//			while(rs.next()) {
//				Market temp = getMarketVO(rs);
//				temp.setTimeFirstInvest(rs.getString("TIME_FIRST_INVEST"));
//				temp.setTimeEstClosing(rs.getString("TIME_EST_CLOSING"));
//				temp.setHaveHourly(rs.getInt("hourly") == 0 ? false : true);
//				temp.setHaveDaily(rs.getInt("dayly") == 0 ? false : true);
//				temp.setHaveWeekly(rs.getInt("weekly") == 0 ? false : true);
//				temp.setHaveMonthly(rs.getInt("monthly") == 0 ? false : true);
//
//				list.add(temp);
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}
	
	public static ArrayList<Long> getCurrentMarketsByFeedName(Connection conn, long skinId, boolean onlyOpened, String marketFeedName) throws SQLException {
	    ArrayList<Long> l = new ArrayList<Long>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
            	"SELECT " +
					"B.market_id " +
				"FROM " +
					"skin_market_groups A, " +
					"skin_market_group_markets B, " +
					"markets C, " +
					"opportunities D " +
				"WHERE " +
					"A.skin_id = ? AND " +
					"C.feed_name like ?  AND " + // Option+ markets
					"A.id = B.skin_market_group_id AND " +
					"B.market_id = C.id AND " +
					"B.market_id = D.market_id AND " +
					"D.is_published = 1 AND " +
					"C.is_suspended = 0 AND " +
					"D.closing_level IS NULL AND "+ 
					"D.is_suspended = 0 "; // not suspended opportunity
					
			if (onlyOpened) {
				sql += 
					"AND current_timestamp < D.time_last_invest ";
			}
			sql += 
				"GROUP BY " +
				"B.market_id, " +
				"B.home_page_priority " +
			"ORDER BY " +
				"B.home_page_priority";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, skinId);
            pstmt.setString(2, marketFeedName);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	l.add(rs.getLong("market_id"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
	}
	
	public static Hashtable<Long, MarketGroup> getSkinGroupsMarkets(Connection conn, long skinId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Hashtable<Long, MarketGroup> groups = new Hashtable<Long, MarketGroup>();
		try {
			String sql =
				"SELECT " +
			        "smgm.market_id, " +
			        "m.display_name as market_name, " +
			        "m.option_plus_market_id, " +
			        "smg.market_group_id, " +
			        "mg.display_name as group_name, " +
			        "m.exchange_id, " +
				    "m.decimal_point, " +
				    "m.decimal_point_subtract_digits, " +
				    "m.type_id m_type_id, " +
				    "m.time_created, " +
					"ai.is_24_7 " +
			    "FROM " +
			        "skin_market_group_markets smgm, " +
			        "markets m, " +
			        "market_groups mg, " +
			        "skin_market_groups smg, " +
			        "market_name_skin mns, " +
			        "asset_index ai " +
			    "WHERE " +
			        "smgm.market_id = m.id AND " +
			        "smgm.skin_market_group_id = smg.ID AND " +
			        "smg.MARKET_GROUP_ID = mg.ID AND " +
			        "smg.SKIN_ID = ? AND " +
			        "m.id = ai.market_id AND " +
			        "EXISTS (SELECT " +
			        			"1 " +
			        		"FROM " +
			        			"opportunity_templates ot " +
			        		"WHERE " +
			        			"m.id = ot.market_id AND " +
			        			"ot.is_active = 1 AND " +
			        			"(ot.scheduled < 5 OR ot.scheduled = " + Opportunity.SCHEDULED_QUARTERLY + ") AND " +
			        			"ot.opportunity_type_id NOT IN (" + Opportunity.TYPE_BINARY_0_100_ABOVE + ", " + Opportunity.TYPE_BINARY_0_100_BELOW + ", " + Opportunity.TYPE_BUBBLES + ")) AND " +
			        "mns.market_id = m.id AND " +
			        "mns.skin_id = smg.skin_id " +
			    "ORDER BY " +
			        "smg.market_group_id, " +
			        "upper(mns.name)";

			ps = conn.prepareStatement(sql);
			ps.setLong(1, skinId);
			rs = ps.executeQuery();

			while (rs.next()) {
				com.anyoption.common.beans.base.Market m = new com.anyoption.common.beans.base.Market();
				m.setDisplayName(rs.getString("market_name"));
				m.setId(rs.getLong("market_id"));
				m.setExchangeId(rs.getLong("exchange_id"));
				long groupId = rs.getLong("market_group_id");
				m.setDecimalPoint(rs.getLong("decimal_point"));
				m.setDecimalPointSubtractDigits(rs.getInt("decimal_point_subtract_digits"));
				m.setGroupId(groupId);
				m.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
				m.setTwentyFourSeven(rs.getBoolean("is_24_7"));
				long optionPlusMarketId = rs.getLong("option_plus_market_id");
				int marketType = rs.getInt("m_type_id");
				m.setProductTypeId(marketType);
				if (optionPlusMarketId > 0) { //we r in option+ market
					m.setOptionPlusMarket(true);
					groupId = ConstantsBase.MARKET_GROUP_OPTION_PLUS_ID;
				}
				if (marketType == OpportunityType.PRODUCT_TYPE_DYNAMICS ) {
					groupId = ConstantsBase.MARKET_GROUP_DYNAMICS_ID;
				}
				if (null == groups.get(groupId)) {
					MarketGroup mg = new MarketGroup();
					mg.setId(groupId);
					if (ConstantsBase.MARKET_GROUP_OPTION_PLUS_ID == groupId) { //if option +
						mg.setName(ConstantsBase.MARKET_GROUP_OPTION_PLUS_DISPLAY_NAME);
					} else if (ConstantsBase.MARKET_GROUP_DYNAMICS_ID == groupId) { 
						mg.setName(ConstantsBase.MARKET_GROUP_DYNAMICS_NAME);
					}else {
						mg.setName(rs.getString("group_name"));
					}
					mg.setMarkets(new ArrayList<com.anyoption.common.beans.base.Market>());
					groups.put(groupId, mg);
				}
				groups.get(groupId).getMarkets().add(m);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return groups;
	}
	
	/**
     * Get markets for skin by opportunity type and schedule
     * @param conn
     * @param skinId
     * @param opportunityTypeId
     * @return
     * @throws SQLException
     */
    public static Hashtable<Long, Market> getMarketsForSkinByOppType(Connection conn, long skinId, long opportunityTypeId, boolean onlyHorly) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Hashtable<Long, Market> ht = new Hashtable<Long, Market>();
        int index = 1;
        try {
        	String sql =" SELECT " +
        				"	m.id, " +
        				"   m.display_name, " +
        				"   m.market_group_id, " +
        				"   mg.display_name as market_group_name " +
        				" FROM " +
        				"   markets m, " +
        				"   market_groups mg, " +
        				"   skin_market_groups smg, " +
        				"   skin_market_group_markets smgm " +
        				" WHERE " +
        				"   mg.id = m.market_group_id " +
        				"   AND smgm.skin_market_group_id = smg.id " +
        				"   AND m.id = smgm.market_id " +
        				"   AND smg.skin_id = ? " +
        				"   AND EXISTS ( SELECT" +
        				"                  1 " +
        				"                FROM " +
        				"                  opportunity_templates ot1 " +
        				"                WHERE " +
        				"	                ot1.market_id = m.id " +
        				"                   AND ot1.is_active = 1 ";
        				if (onlyHorly) {
        		  sql +=" 		            AND ot1.scheduled = ? ";
        				}
        		  sql +="       	        AND ot1.opportunity_type_id = ?) ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(index++, skinId);
            if (onlyHorly) {
            	pstmt.setInt(index++, Opportunity.SCHEDULED_HOURLY);
            }
            pstmt.setLong(index++, opportunityTypeId);
            rs = pstmt.executeQuery();
            Market m = null;
            while (rs.next()) {
                m = new Market();
                m.setId(rs.getLong("id"));
                m.setDisplayName(rs.getString("display_name"));
                m.setGroupId(rs.getLong("market_group_id"));
                m.setGroupName(rs.getString("market_group_name"));
                ht.put(rs.getLong("id"), m);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return ht;
    }
    
    public static ArrayList<MarketPriority> getMarketsByPriority(Connection connection, long skinId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<MarketPriority> markets = new ArrayList<MarketPriority>();
        int index = 1;
        try {
        	String sql =
					   	  "SELECT " +
					              "smgm.id, " +
					              "m.name, " +
					              "smgm.market_id, " +
					              "smgm.home_page_priority " +
					        "FROM  " +
					              "skin_market_group_markets  smgm, " +
					              "skin_market_groups smg, " +
					              "markets m " +
					        "WHERE " +
					              "smgm.skin_market_group_id = smg.ID " +
					        "AND " +
					              "m.id = smgm.market_id " +
					        "AND " +
					              "smg.skin_id = ? " +
					        "ORDER BY " +
				        		  "home_page_priority";        	
            pstmt = connection.prepareStatement(sql);
            pstmt.setLong(index++, skinId);
            rs = pstmt.executeQuery();
            
            while (rs.next()) {
            	MarketPriority m = new MarketPriority();
            	m.setSkinMarketGroupMarketId(rs.getLong("id")); // the skin_market_group_markets Id 
            	m.setMarketName(rs.getString("name"));
            	m.setMarketId(rs.getLong("market_id"));
                markets.add(m);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return markets;
    }
    
    public static void updatetMarketPriority(Connection connection, long homePagePriority, long skinMarketGroupMarketId) throws SQLException {
        PreparedStatement pstmt = null;

        int index = 1;
        try {
        	String sql =
        			"UPDATE  " +
							"skin_market_group_markets " +
        			"SET " +
        					"home_page_priority = ? " +
		        	"WHERE " +
		        			"id = ? ";
            pstmt = connection.prepareStatement(sql);
            pstmt.setLong(index++, homePagePriority);
            pstmt.setLong(index++, skinMarketGroupMarketId);
            pstmt.executeUpdate();
            
        } finally {
            closeStatement(pstmt);
        }
    }
	
	public static void insertMarketPriority(Connection con, long selectedSkinId, ArrayList<Long> newMarketsIds,
			long homePagePriority) throws SQLException {
		PreparedStatement ps = null;
		int index = 1;
		   try {
        	for (Long marketId : newMarketsIds) {
	        	String sql = 
	        			"INSERT INTO " +
	        				" skin_market_group_markets " +
	        				" ( " +
	        					" id " + 
	        					" ,skin_market_group_id " + 
	        					" ,market_id " + 
	        					" ,group_priority" + 
	        					" ,home_page_priority " + 
	        					" ,skin_display_group_id " + 
	        				" VALUES " + 
	        					" (SEQ_SKIN_MARKET_GROUP_MARKETS.nextval, ?, ?, ?, ?, ?) "; 
	        	ps = con.prepareStatement(sql);
	        	ps.setLong(index++, getSkinMarketGroupId(con, getMarketGroupByMarketId(con, marketId), selectedSkinId));
	        	ps.setLong(index++, marketId);
	        	ps.setLong(index++, 9999);
	        	ps.setLong(index++, homePagePriority++);
	        	ps.setNull(index++, Types.NULL);
        	}
        } finally {
            closeStatement(ps);
        }
	}
    
	public static Long copyMarket(Connection conn, long marketId, long writerId) throws SQLException {
		Long res = null;
		CallableStatement cstmt = null;
		BigDecimal v ;
		int index = 1;
		try {
			String sql = "{call pkg_markets.create_market(?, ?, ?)}";
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.NUMERIC);
			cstmt.setLong(index++, marketId);
			cstmt.setLong(index++, writerId);
			cstmt.executeQuery();
			
			v = (BigDecimal) cstmt.getObject(1);
			res = v.longValue();
		} finally {
			closeStatement(cstmt);
		}
		return res;
	}

	public static MarketBean getMarketById(Connection conn, long marketId) throws SQLException {
		MarketBean market = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_markets.get_market(o_market => ? " +
					   												",i_market_id => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, marketId);

			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				market = new MarketBean();
				loadMarketBeanVO(rs, market);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return market;
	}
	
	public static void updateMarketById(Connection conn, MarketBean market) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_markets.update_market " +
											" ( " +
												   "  i_market_id                    => ? " +
												   " ,i_name                         => ? " +
												   " ,i_display_name                 => ? " +
												   " ,i_feed_name                    => ? " +
												   " ,i_writer_id                    => ? " +
												   " ,i_decimal_point                => ? " +
												   " ,i_market_group_id              => ? " +
												   " ,i_exchange_id                  => ? " +
												   " ,i_investment_limits_group_id   => ? " +
												   " ,i_updates_pause                => ? " +
												   " ,i_significant_percentage       => ? " +
												   " ,i_real_updates_pause           => ? " +
												   " ,i_real_significant_percentage  => ? " +
												   " ,i_random_ceiling               => ? " +
												   " ,i_random_floor                 => ? " +
												   " ,i_first_shift_parameter        => ? " +
												   " ,i_average_investments          => ? " +
												   " ,i_pct_of_avg_investments       => ? " +
												   " ,i_fixed_amount_for_shifting    => ? " +
												   " ,i_type_of_shifting             => ? " +
												   " ,i_disable_after_period         => ? " +
												   " ,i_curent_level_alert_perc      => ? " +
												   " ,i_is_suspended                 => ? " +
												   " ,i_suspended_message            => ? " +
												   " ,i_acceptable_level_deviation   => ? " +
												   " ,i_is_has_future_agreement      => ? " +
												   " ,i_is_next_agreement_same_day   => ? " +
												   " ,i_banners_priority             => ? " +
												   " ,i_sec_between_investments      => ? " +
												   " ,i_sec_for_dev2                 => ? " +
												   " ,i_amount_for_dev2              => ? " +
												   " ,i_max_exposure                 => ? " +
												   " ,i_roll_up_qualify_percent      => ? " +
												   " ,i_is_roll_up                   => ? " +
												   " ,i_is_golden_minutes            => ? " +
												   " ,i_insurance_premia_percent     => ? " +
												   " ,i_insurance_qualify_percent    => ? " +
												   " ,i_roll_up_premia_percent       => ? " +
												   " ,i_no_auto_settle_after         => ? " +
												   " ,i_shifting_group_id            => ? " +
												   " ,i_option_plus_fee              => ? " +
												   " ,i_option_plus_market_id        => ? " +
												   " ,i_acceptable_level_deviation_3 => ? " +
												   " ,i_sec_between_investments_ip   => ? " +
												   " ,i_amount_for_dev3              => ? " +
												   " ,i_dec_point_subtract_digits    => ? " +
												   " ,i_amount_for_dev2_night        => ? " +
												   " ,i_exposure_reached_amount      => ? " +
												   " ,i_raise_exposure_up_amount     => ? " +
												   " ,i_raise_exposure_down_amount   => ? " +
												   " ,i_raise_call_lower_put_percent => ? " +
												   " ,i_raise_put_lower_call_percent => ? " +
												   " ,i_sec_for_dev2_mob             => ? " +
												   " ,i_sec_between_investments_mob  => ? " +
												   " ,i_sec_btwn_investments_ip_mob  => ? " +
												   " ,i_quote_params                 => ? " +
												   " ,i_use_for_fake_investments     => ? " +
												   " ,i_asset_index_exp_levlel_calc  => ? " +
												   " ,i_bubbles_pricing_level        => ? " +
												   " ,i_type_id                      => ? " +
												   " ,i_worst_case_return            => ? " +
												   " ,i_limited_high_amnt            => ? " +
												   " ,i_instant_spread               => ? " +
												   " ,i_limited_from_amnt            => ? " +
											  " )" +
							 			" }");
			
			cstmt.setLong(index++, market.getId());
			
			setStatementValue(market.getName(), index++, cstmt);
			setStatementValue(market.getDisplayName(), index++, cstmt);
			setStatementValue(market.getFeedName(), index++, cstmt);
			setStatementValue(market.getWriterId(), index++, cstmt);
			setStatementValue(market.getDecimalPoint(), index++, cstmt);
			setStatementValue(market.getMarketGroupId(), index++, cstmt);
			setStatementValue(market.getExchangeId(), index++, cstmt);
			setStatementValue(market.getInvestmentLimitGroupId(), index++, cstmt);
			setStatementValue(market.getUpdatesPause(), index++, cstmt);
			setStatementValue(market.getSignificantPercentage(), index++, cstmt);
			setStatementValue(market.getRealUpdatesPause(), index++, cstmt);
			setStatementValue(market.getRealSignificantPercentage(), index++, cstmt);
			setStatementValue(market.getRandomCeiling(), index++, cstmt);
			setStatementValue(market.getRandomFloor(), index++, cstmt);
			setStatementValue(market.getFirstShiftParameter(), index++, cstmt);
			setStatementValue(market.getAverageInvestments(), index++, cstmt);
			setStatementValue(market.getPercentageOfAvgInvestments(), index++, cstmt);
			setStatementValue(market.getFixedAmountForShifting(), index++, cstmt);
			setStatementValue(market.getTypeOfShifting(), index++, cstmt);
			setStatementValue(market.getDisableAfterPeriod(), index++, cstmt);
			setStatementValue(market.getCurrentLevelAlertPerc(), index++, cstmt);
			setStatementValue(market.isSuspended(), index++, cstmt);
			setStatementValue(market.getSuspendedMessage(), index++, cstmt);
			setStatementValue(market.getAcceptableLevelDeviation(), index++, cstmt);
			setStatementValue(market.isHasFutureAgreement(), index++, cstmt);
			setStatementValue(market.isNextAgreementSameDay(), index++, cstmt);
			setStatementValue(market.getBannersPriority(), index++, cstmt);
			setStatementValue(market.getSecBetweenInvestments(), index++, cstmt);
			setStatementValue(market.getSecForDev2(), index++, cstmt);
			setStatementValue(market.getAmountForDev2(), index++, cstmt);
			setStatementValue(market.getMaxExposure(), index++, cstmt);
			setStatementValue(market.getRollUpQualifyPercent(), index++, cstmt);
			setStatementValue(market.getIsRollUp(), index++, cstmt);
			setStatementValue(market.getIsGoldenMinutes(), index++, cstmt);
			setStatementValue(market.getInsurancePremiaPercent(), index++, cstmt);
			setStatementValue(market.getInsuranceQualifyPercent(), index++, cstmt);
			setStatementValue(market.getRollUpPremiaPercent(), index++, cstmt);
			setStatementValue(market.getNoAutoSettleAfter(), index++, cstmt);
			setStatementValue(market.getShiftingGroupId(), index++, cstmt);
			setStatementValue(market.getOptionPlusFee(), index++, cstmt);
			setStatementValue(market.getOptionPlusMarketId(), index++, cstmt);
			setStatementValue(market.getAcceptableLevelDeviation3(), index++, cstmt);
			setStatementValue(market.getSecBetweenInvestmentsIp(), index++, cstmt);
			setStatementValue(market.getAmountForDev3(), index++, cstmt);
			setStatementValue(market.getDecimalPointSubtractDigits(), index++, cstmt);
			setStatementValue(market.getAmountForDev2Night(), index++, cstmt);
			setStatementValue(market.getExposureReachedAmount(), index++, cstmt);
			setStatementValue(market.getRaiseExposureUpAmount(), index++, cstmt);
			setStatementValue(market.getRaiseExposureDownAmount(), index++, cstmt);
			setStatementValue(market.getRaiseCallLowerPutPercent(), index++, cstmt);
			setStatementValue(market.getRaisePutLowerCallPercent(), index++, cstmt);
			setStatementValue(market.getSecForDev2Mob(), index++, cstmt);
			setStatementValue(market.getSecBetweenInvestmentsMob(), index++, cstmt);
			setStatementValue(market.getSecBetweenInvestmentsIpMob(), index++, cstmt);
			setStatementValue(market.getQuoteParams(), index++, cstmt);
			setStatementValue(market.getUseForFakeInvestments(), index++, cstmt);
			setStatementValue(market.getAssetIndexExpLevelCalc(), index++, cstmt);
			setStatementValue(market.getBubblesPricingLevel(), index++, cstmt);
			setStatementValue(market.getTypeId(), index++, cstmt);
			setStatementValue(market.getWorstCaseReturn(), index++, cstmt);
			setStatementValue(market.getLimitedHighAmount(), index++, cstmt);
			setStatementValue(market.getInstantSpread(), index++, cstmt);
			setStatementValue(market.getLimitedFromAmount(), index++, cstmt);
			
			cstmt.executeUpdate();
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static void updateMarketConfigFormulas(Connection conn, MarketConfig marketConfig, long marketId, long dataSourceId) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_markets.update_market_formulas " +
											" ( " +
												" i_market_id => ? , " +
												" i_market_data_source_id => ? , " +
												" i_hour_level_formula_id => ?, " +
												" i_hour_level_formula_params => ? , " +
												" i_hour_closing_formula_id => ?, " +
												" i_hour_closing_formula_params => ? , " +
												" i_day_closing_formula_id => ?, " +
												" i_day_closing_formula_params => ? , " +
												" i_long_term_formula_id => ?, " +
												" i_long_term_formula_params => ? , " +
												" i_real_level_formula_id => ?, " +
												" i_real_level_formula_params => ? , " +
												" i_market_params => ? , " +
												" i_market_disable_conditions => ? " +
											  " )" +
							 			" }");
			
			cstmt.setLong(index++, marketId);
			cstmt.setLong(index++, dataSourceId);
			setStatementValue((long) marketConfig.getHourLevelFormulaId(), index++, cstmt);
			setStatementValue(marketConfig.getHourLevelFormulaParams(), index++, cstmt);
			setStatementValue((long) marketConfig.getHourClosingFormulaId(), index++, cstmt);
			setStatementValue(marketConfig.getHourClosingFormulaParams(), index++, cstmt);
			setStatementValue((long) marketConfig.getDayClosingFormulaId(), index++, cstmt);
			setStatementValue(marketConfig.getDayClosingFormulaParams(), index++, cstmt);
			setStatementValue(	(marketConfig.getLongTermFormulaId() != null) ? Long.valueOf(marketConfig.getLongTermFormulaId()) : null,
								index++, cstmt);
			setStatementValue(marketConfig.getLongTermFormulaParams(), index++, cstmt);
			setStatementValue((long) marketConfig.getRealLevelFormulaId(), index++, cstmt);
			setStatementValue(marketConfig.getRealLevelFormulaParams(), index++, cstmt);
			setStatementValue(marketConfig.getMarketParams(), index++, cstmt);
			setStatementValue(marketConfig.getMarketDisableConditions(), index++, cstmt);
			cstmt.executeUpdate();
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
	}
	
	private static void loadMarketBeanVO(ResultSet rs, MarketBean market) throws SQLException {
		market.setId(rs.getLong("id"));
		market.setName(rs.getString("name"));
		market.setDisplayName(rs.getString("display_name"));
		market.setFeedName(rs.getString("feed_name"));
		//We are sending the date in milliseconds
		market.setTimeCreated(convertToDate(rs.getTimestamp("time_created")).getTime());
		market.setWriterId(rs.getLong("writer_id"));
		market.setDecimalPoint(rs.getLong("decimal_point"));
		market.setMarketGroupId(rs.getLong("market_group_id"));
		market.setExchangeId(rs.getLong("exchange_id"));
		market.setInvestmentLimitGroupId(rs.getLong("investment_limits_group_id"));
		market.setUpdatesPause(rs.getLong("updates_pause"));
		market.setSignificantPercentage(rs.getDouble("significant_percentage"));
		market.setRealUpdatesPause(rs.getLong("real_updates_pause"));
		market.setRealSignificantPercentage(rs.getDouble("real_significant_percentage"));
		market.setRandomCeiling(rs.getDouble("random_ceiling"));
		market.setRandomFloor(rs.getDouble("random_floor"));
		market.setFirstShiftParameter(rs.getDouble("first_shift_parameter"));
		market.setAverageInvestments(rs.getLong("average_investments"));
		market.setPercentageOfAvgInvestments(rs.getDouble("percentage_of_avg_investments"));
		market.setFixedAmountForShifting(rs.getLong("fixed_amount_for_shifting"));
		market.setTypeOfShifting(rs.getLong("type_of_shifting"));
		market.setDisableAfterPeriod(rs.getLong("disable_after_period"));
		market.setCurrentLevelAlertPerc(rs.getDouble("curent_level_alert_perc"));
		market.setSuspended(rs.getBoolean("is_suspended"));
		market.setSuspendedMessage(rs.getString("suspended_message"));
		market.setAcceptableLevelDeviation(rs.getDouble("acceptable_level_deviation"));
		market.setHasFutureAgreement(rs.getBoolean("is_has_future_agreement"));
		market.setNextAgreementSameDay(rs.getBoolean("is_next_agreement_same_day"));
		market.setBannersPriority(rs.getLong("banners_priority"));
		market.setSecBetweenInvestments(rs.getLong("sec_between_investments"));
		market.setSecForDev2(rs.getLong("sec_for_dev2"));
		market.setAmountForDev2(rs.getString("amount_for_dev2"));
		market.setMaxExposure(rs.getLong("max_exposure"));
		market.setRollUpQualifyPercent(rs.getDouble("roll_up_qualify_percent"));
		market.setIsRollUp(rs.getBoolean("is_roll_up"));
		market.setIsGoldenMinutes(rs.getBoolean("is_golden_minutes"));
		market.setInsurancePremiaPercent(rs.getDouble("insurance_premia_percent"));
		market.setInsuranceQualifyPercent(rs.getDouble("insurance_qualify_percent"));
		market.setRollUpPremiaPercent(rs.getDouble("roll_up_premia_percent"));
		market.setNoAutoSettleAfter(rs.getDouble("no_auto_settle_after"));
		market.setShiftingGroupId(rs.getLong("shifting_group_id"));
		market.setOptionPlusFee(rs.getLong("option_plus_fee"));
		market.setOptionPlusMarketId(rs.getLong("option_plus_market_id"));
		market.setAcceptableLevelDeviation3(rs.getDouble("acceptable_level_deviation_3"));
		market.setSecBetweenInvestmentsIp(rs.getLong("sec_between_investments_ip"));
		market.setAmountForDev3(rs.getLong("amount_for_dev3"));
		market.setDecimalPointSubtractDigits(rs.getLong("decimal_point_subtract_digits"));
		market.setAmountForDev2Night(rs.getLong("amount_for_dev2_night"));
		market.setExposureReachedAmount(rs.getLong("exposure_reached_amount"));
		market.setRaiseExposureUpAmount(rs.getLong("raise_exposure_up_amount"));
		market.setRaiseExposureDownAmount(rs.getLong("raise_exposure_down_amount"));
		market.setRaiseCallLowerPutPercent(rs.getDouble("raise_call_lower_put_percent"));
		market.setRaisePutLowerCallPercent(rs.getDouble("raise_put_lower_call_percent"));
		market.setSecForDev2Mob(rs.getLong("sec_for_dev2_mob"));
		market.setSecBetweenInvestmentsMob(rs.getLong("sec_between_investments_mob"));
		market.setSecBetweenInvestmentsIpMob(rs.getLong("sec_between_investments_ip_mob"));
		market.setQuoteParams(rs.getString("quote_params"));
		market.setUseForFakeInvestments(rs.getBoolean("use_for_fake_investments"));
		market.setAssetIndexExpLevelCalc(rs.getString("asset_index_exp_levlel_calc"));
		market.setBubblesPricingLevel(rs.getLong("bubbles_pricing_level"));
		market.setTypeId(rs.getLong("type_id"));
		market.setWorstCaseReturn(rs.getLong("worst_case_return"));
		market.setLimitedHighAmount(rs.getBoolean("limited_high_amnt"));
		market.setInstantSpread(rs.getDouble("instant_spread"));
		market.setLimitedFromAmount(rs.getLong("limited_from_amnt"));
	}

	public static MarketConfig getMarketConfigFormulas(Connection conn, long marketId, long dataSourceId) throws SQLException {
		MarketConfig marketConfig = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_markets.get_market_formulas(o_data => ? " +
					   												",i_market_id => ? , " +
																	" i_market_data_source_id => ?)}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, marketId);
			cstmt.setLong(index++, dataSourceId);

			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				marketConfig = new MarketConfig();
				marketConfig.setHourLevelFormulaId(rs.getInt("hour_level_formula_id"));
				marketConfig.setHourLevelFormulaParams(rs.getString("hour_level_formula_params"));
				marketConfig.setHourClosingFormulaId(rs.getInt("hour_closing_formula_id"));
				marketConfig.setHourClosingFormulaParams(rs.getString("hour_closing_formula_params"));
				marketConfig.setDayClosingFormulaId(rs.getInt("day_closing_formula_id"));
				marketConfig.setDayClosingFormulaParams(rs.getString("day_closing_formula_params"));
				marketConfig.setLongTermFormulaId(rs.getInt("long_term_formula_id"));
				marketConfig.setLongTermFormulaParams(rs.getString("long_term_formula_params"));
				marketConfig.setRealLevelFormulaId(rs.getInt("real_level_formula_id"));
				marketConfig.setRealLevelFormulaParams(rs.getString("real_level_formula_params"));
				marketConfig.setMarketParams(rs.getString("market_params"));
				marketConfig.setMarketDisableConditions(rs.getString("market_disable_conditions"));
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return marketConfig;
	}
	
	public static HashMap<Long, HashMap<Long, MarketNameBySkin>> getMarketNamesBySkin(Connection con)
			throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		HashMap<Long, HashMap<Long, MarketNameBySkin>> marketNamesBySkin = new HashMap<Long, HashMap<Long, MarketNameBySkin>>();
		try {
			String sql =
        				"SELECT " +
        						" * " +
        				" FROM " +
        						" MARKET_NAME_SKIN " +
        				" ORDER BY " +
        						" market_id, skin_id ";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				if (marketNamesBySkin.get(rs.getLong("market_id")) == null) {
					HashMap<Long, MarketNameBySkin> mapNamesBySkin = new HashMap<Long, MarketNameBySkin>();
					marketNamesBySkin.put(rs.getLong("market_id"), mapNamesBySkin);
				}
				MarketNameBySkin mn = new MarketNameBySkin();
				mn.setName(rs.getString("name"));
				mn.setShortName(rs.getString("short_name"));
				marketNamesBySkin.get(rs.getLong("market_id")).put(rs.getLong("skin_id"), mn);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return marketNamesBySkin;
	}
	
	public static String getMarketNameExtention(String id, String marketName, String marketNameKey) {
		return getMarketNameExtention(id, marketName, marketNameKey, 0);
	}
	
	public static String getMarketNameExtention(String id, String marketName, String marketNameKey, long typeId) {
		
		// Long term markets
		ArrayList<Long> longTermExceptions = new ArrayList<Long>();
		longTermExceptions.add(693L);
		longTermExceptions.add(709L);
		longTermExceptions.add(710L);
		longTermExceptions.add(711L);
		longTermExceptions.add(712L);
		if (id.contains(",")) {
			String[] parts = id.split(",");
			long marketId = new Long(parts[1]);
			if (marketId == ConstantsBase.MARKET_ID_TEVA_NY) {
				marketName = marketName + " NY";
			}
			if (marketId == ConstantsBase.MARKET_ID_TEVA_TA) {
				marketName = marketName + " TA";
			}
		}
		if (marketNameKey.contains(".1t")) {
			marketName = marketName + " 1T";
		}
		if (marketNameKey.contains(".dynamics") || typeId == Market.PRODUCT_TYPE_DYNAMICS) {
			marketName = marketName + " Dynamics";
		}
		if (marketNameKey.contains(".plus") || typeId == Market.PRODUCT_TYPE_OPTION_PLUS) {
			marketName = marketName + " +";
		}
		if (marketNameKey.contains(".zero") || typeId == Market.PRODUCT_TYPE_BINARY0100) {
			marketName = marketName + " 0-100";
		}
		if (marketNameKey.contains(".bubbles") || typeId == Market.PRODUCT_TYPE_BUBBLES) {
			marketName = marketName + " MB";
		}
		if (marketNameKey.contains(".LT") || longTermExceptions.contains(id)) {
			marketName = marketName + " LT";
		}
		return marketName;
	}
	
	public static ArrayList<MarketPauseDetails> getMarketPausesByMarketId(Connection conn, long marketId) 
																									throws SQLException {
		ArrayList<MarketPauseDetails> marketPauseDetailsList = new ArrayList<MarketPauseDetails>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_markets.get_market_pauses_by_market(o_pauses => ? ,i_market_id => ?)}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, marketId);

			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				MarketPauseDetails mpd = new MarketPauseDetails();
				mpd.setMarketPauseId(rs.getLong("id"));
				mpd.setMarketId((int) rs.getLong("market_id"));
				mpd.setScheduled((byte) rs.getLong("scheduled"));
				mpd.setHalfDay(rs.getLong("is_half_day") == 1 ? true : false);
				mpd.setPauseStart(rs.getString("pause_start"));
				mpd.setPauseEnd(rs.getString("pause_end"));
				mpd.setValidDays((byte) rs.getLong("valid_days"));
				mpd.setActive(rs.getLong("is_active") == 1 ? true : false);
				marketPauseDetailsList.add(mpd);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return marketPauseDetailsList;
	}
	
	public static void updateInsertMarketPause(Connection conn, MarketPauseDetails marketPauseDetails) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_markets.update_insert_market_pause(i_market_pause_id => ? " +
																				  " ,i_market_id => ? " + 
																				  " ,i_scheduled => ? " +
																				  " ,i_half_day => ? " +
																				  " ,i_pause_start => ? " +
																				  " ,i_pause_end => ? " +
																				  " ,i_valid_days => ? " +
																				  " ,i_is_active => ? )}");
			
			cstmt.setLong(index++, marketPauseDetails.getMarketPauseId());
			cstmt.setLong(index++, marketPauseDetails.getMarketId());
			cstmt.setLong(index++, marketPauseDetails.getScheduled());
			cstmt.setLong(index++, marketPauseDetails.isHalfDay() ? 1L : 0L);
			cstmt.setString(index++, marketPauseDetails.getPauseStart());
			cstmt.setString(index++, marketPauseDetails.getPauseEnd());
			cstmt.setLong(index++, marketPauseDetails.getValidDays());
			cstmt.setLong(index++, marketPauseDetails.isActive() ? 1L : 0L);
			cstmt.executeQuery();
		} finally {
			closeStatement(cstmt);
		}
	}


	public static Map<Integer, List<MarketFormula>> getMarketFormulas(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<Integer, List<MarketFormula>> formulas = new HashMap<>();
		try { // for now we don't include dynamics/0100 formulas
			String sql = "select id, name, formula_params, type, formula_description_key from market_formulas where type in (1,2)";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				int type = rs.getInt("type");
				List<MarketFormula> typeFormulas = formulas.get(type);
				if (typeFormulas == null) {
					typeFormulas = new ArrayList<>();
					formulas.put(type, typeFormulas);
				}
				MarketFormula mf = new MarketFormula();
				mf.setId(rs.getLong("id"));
				mf.setName(rs.getString("name"));
				mf.setParams(rs.getString("formula_params"));
				mf.setType(type);
				mf.setDescriptionKey(rs.getString("formula_description_key"));
				typeFormulas.add(mf);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return formulas;
	}
	
	public static MarketPauseDetails getMarketPauseById(Connection con, long marketPauseId) throws SQLException {
		MarketPauseDetails marketPause = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_markets.get_market_pause_by_id(o_pause => ? ,i_market_pause_id => ?)}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, marketPauseId);

			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);
			if (rs.next()) {
				marketPause = new MarketPauseDetails();
				marketPause.setMarketPauseId(rs.getLong("id"));
				marketPause.setMarketId((int) rs.getLong("market_id"));
				marketPause.setScheduled((byte) rs.getLong("scheduled"));
				marketPause.setHalfDay(rs.getLong("is_half_day") == 1 ? true : false);
				marketPause.setPauseStart(rs.getString("pause_start"));
				marketPause.setPauseEnd(rs.getString("pause_end"));
				marketPause.setValidDays((byte) rs.getLong("valid_days"));
				marketPause.setActive(rs.getLong("is_active") == 1 ? true : false);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return marketPause;
	}

	public static void insertMarketForEmirReport(Connection con, EmirReport emirReport) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_markets.insert_emir_report(i_asset_id => ? " +
																				  " ,i_commod_base => ? " + 
																				  " ,i_commod_det => ? " +
																				  " ,i_exchratebasis => ? " +
																				  " ,i_ncurr1 => ? " +
																				  " ,i_ncurr2 => ? " +
																				  " ,i_ospccy => ? " +
																				  " ,i_pac => ? " +
																				  " ,i_pidval1 => ? " +
																				  " ,i_uait => ? " +
																				  " ,i_uasset => ? )}");
			
			setStatementValue(emirReport.getAssetId(), index++, cstmt);
			cstmt.setString(index++, emirReport.getCommodBase());
			cstmt.setString(index++, emirReport.getCommodDet());
			cstmt.setString(index++, emirReport.getExchRateBasis());
			cstmt.setString(index++, emirReport.getNcurr1());
			cstmt.setString(index++, emirReport.getNcurr2());
			cstmt.setString(index++, emirReport.getOspccy());
			cstmt.setString(index++, emirReport.getPac());
			cstmt.setString(index++, emirReport.getPidval1());
			cstmt.setString(index++, emirReport.getUait());
			cstmt.setString(index++, emirReport.getUasset());
			
			cstmt.executeQuery();
		} finally {
			closeStatement(cstmt);
		}
	}

	public static EmirReport getMarketEmirReportById(Connection con, Long marketId) throws SQLException {
		EmirReport emirReport = new EmirReport();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			cstmt = con.prepareCall("{call pkg_markets.get_emir_report(o_report => ? , i_market_id => ?)}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, marketId);

			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);
			if (rs.next()) {
				emirReport.setAssetId(rs.getLong("asset_id"));
				emirReport.setCommodBase(rs.getString("commod_base"));
				emirReport.setCommodDet(rs.getString("commod_det"));
				emirReport.setExchRateBasis(rs.getString("exchratebasis"));
				emirReport.setNcurr1(rs.getString("ncurr1"));
				emirReport.setNcurr2(rs.getString("ncurr2"));
				emirReport.setOspccy(rs.getString("ospccy"));
				emirReport.setPac(rs.getString("pac"));
				emirReport.setPidval1(rs.getString("pidval1"));
				emirReport.setUait(rs.getString("uait"));
				emirReport.setUasset(rs.getString("uasset"));
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return emirReport;
	}
	
	public static long getMarketGroupByMarketId (Connection con, long marketId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long groupId = 0;
		try {
			String sql = 
						" SELECT " +
							" market_group_id " +
						" FROM " +
							" markets " +
						" WHERE " + 
							" id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, marketId);
			rs = ps.executeQuery();
			if (rs.next()) {
				groupId = rs.getLong("market_group_id");
			}
		} finally {
			closeStatement(ps);
		}
		return groupId;
	}
	
	public static long getSkinMarketGroupId (Connection con, long groupId, long skinId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long id = 0;
		try {
			String sql = 
						" SELECT " +
							" id " +
						" FROM " +
							" skin_market_groups " +
						" WHERE " + 
							" skin_id = ? " +
							" AND " + 
							" market_group_id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);
			ps.setLong(2, groupId);
			rs = ps.executeQuery();
			if (rs.next()) {
				groupId = rs.getLong("market_group_id");
			}
		} finally {
			closeStatement(ps);
		}
		return id;
	}
}