package com.anyoption.common.daos;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardUserHistory;
import com.anyoption.common.managers.RewardManager;
import com.anyoption.common.managers.RewardUsersHistoryManager;
import com.anyoption.common.util.Utils;

/**
 * @author liors
 *
 */
public class RewardUsersHistoryDAO extends DAOBase {
	private final static Logger logger = Logger.getLogger(RewardUsersHistoryDAO.class);
	
	//TODO unit test.
	public static boolean insertValueObject(Connection connection, RewardUserHistory rewardUserHistory) throws SQLException {
		PreparedStatement ps = null;
		int ind = 1;
		try {
			String sql =
						"INSERT INTO " +
						"	rwd_users_history " +
						"	( " +
						"		id" +
						"		,writer_id" +
						"		,user_id" +
						"		,rwd_tier_id" +
						"		,rwd_action_type_id" +
						"		,rwd_action_id" +
						"		,reference_id" +
						"		,rwd_his_type_id" +
						"		,balance_exp_points" +
						"		,comments" +
						"		,time_created" +						
						"		,amount_exp_points" +
						" 	) " +
						"VALUES	" +
						"	( " +
						"		seq_rwd_users_history.nextval" +
						"		,?" +
						"		,?" +
						"		,?" +
						"		,?" +
						"		,?" +
						"		,?" +
						"		,?" +
						"		,?" +
						"		,?" +
						"		,sysdate" +
						"		,?" +
						"	) ";

			ps = connection.prepareStatement(sql);
			ps.setInt(ind++, rewardUserHistory.getWriterId());
			ps.setLong(ind++, rewardUserHistory.getUserId());
			ps.setInt(ind++, rewardUserHistory.getRewardTierId());			
			ps.setInt(ind++, rewardUserHistory.getRewardActionTypeId());
			ps.setInt(ind++, rewardUserHistory.getRewardActionId());			
			ps.setLong(ind++, rewardUserHistory.getReferenceId());
			ps.setInt(ind++, rewardUserHistory.getRewardHistoryTypeId());			
			ps.setDouble(ind++, Utils.round(rewardUserHistory.getBalanceExperiencePoints(), 2, BigDecimal.ROUND_HALF_UP));
			ps.setString(ind++, rewardUserHistory.getComment());
			ps.setDouble(ind++, Utils.round(rewardUserHistory.getAmountExperiencePoints(), 2, BigDecimal.ROUND_HALF_UP));			
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}	
		return true;
	}
	
	public static void updateHistoryRemoveRewards(Connection conn, long userId, int tier, int writerId) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =
                    "INSERT INTO rwd_users_history " +
				    "( " +
					      "rwd_action_id, " +
					      "time_created, " +
					      "user_id, " +
					      "comments, " +
					      "rwd_action_type_id, " +
					      "id, " +
					      "reference_id, " +
					      "rwd_his_type_id, " +
					      "writer_id, " +
					      "rwd_tier_id, " +
					      "balance_exp_points " +
				    ") " +
					"SELECT " + 
					  "rub.id, " +
					  "sysdate, " +
					  "rub.user_id, " +
					  "null, " +
					  RewardManager.BENEFIT + ", " +
					  "seq_rwd_users_history.nextval, " +
					  "null, " +
					  RewardUsersHistoryManager.REWARD_REMOVED + ", " +
					  writerId + ", " +
					  "ru.rwd_tier_id, " +
					  "ru.exp_points " +
					"FROM " +
					  "rwd_benefits rb, " +
					  "rwd_user_benefits rub, " +
					  "rwd_users ru " +
					"WHERE " +
					  "ru.user_id = rub.user_id AND " +
					  "rub.user_id = ? AND " +
					  "rub.rwd_benefit_id = rb.id AND " +
					  "rb.rwd_tier_id > ? AND " +
					  "rb.is_affected_from_demotion = 1 AND " +
					  "rub.is_active = 1 AND " +
					  "rb.bonus_id is null";
            
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, tier);
            pstmt.executeUpdate();
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }	
	}
}
