package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.LimitationDeposits;
import com.anyoption.common.util.ConstantsBase;

public class LimitationDepositsDAOBase extends DAOBase {
	private static final Logger logger = Logger.getLogger(LimitationDepositsDAOBase.class);



    public static LimitationDeposits getLimitByUserId(Connection con, long userId, long remarktingId) throws SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            String sql = "SELECT " +
                             "ld.id, " +
                             "ld.minimum_first_deposit as min_f_dep, " +
                             "ld.minimum_deposit as min_dep, " +
                             "ld.default_deposit " +
                         "FROM " +
                             "limitation_deposits ld, " +
                             "marketing_combinations mco, " +
                             "marketing_campaigns mca, " +
                             "users u ";
                 if (remarktingId > 0) {
                      sql += ", remarketing_logins rl " +
                                    "LEFT JOIN marketing_affilates ma on ma.key = " +
												                                    "(CASE WHEN " +
												                                            "regexp_like(rl.dynamic_parameter, '^\\d{6}_.*') " +
												                                        "THEN " +
												                                            "TO_NUMBER(SUBSTR(rl.dynamic_parameter,1,6)) " + 
												                                        "ELSE " + 
												                                            "-999 " + //if its not number no need the join
												                                        "END)"; 
;
                 } else {
                          sql += "LEFT JOIN marketing_affilates ma ON ma.key = u.affiliate_key ";
                 }
                 sql += "WHERE " +
                             "u.id = ? AND " +
                             "mca.id = mco.campaign_id AND ";

                 if (remarktingId > 0) {
                      sql += "rl.id = " + remarktingId + " AND " +
                             "rl.combination_id = mco.id AND ";

                 } else {
                      sql += "ld.is_remarketing IS null AND " +
                             "u.combination_id = mco.id AND ";
                 }
                      sql += "(ld.affiliate_id IS null OR ld.affiliate_id = ma.id) AND " +
                             "(ld.campaign_id IS null OR ld.campaign_id = mco.campaign_id) AND " +
                             "(ld.country_id IS null OR ld.country_id = u.country_id) AND " +
                             "(ld.currency_id IS null OR ld.currency_id = u.currency_id) AND " +
                             "(ld.payment_recipient_id IS null OR ld.payment_recipient_id = mca.payment_recipient_id) AND " +
                             "(ld.skin_id IS null OR ld.skin_id = u.skin_id) AND " +
                             "ld.is_active = " + ConstantsBase.STRING_TRUE +
                         "ORDER BY " +
                             "ld.is_remarketing, " +
                             "ld.affiliate_id, " +
                             "ld.campaign_id, " +
                             "ld.payment_recipient_id, " +
                             "ld.country_id, " +
                             "ld.currency_id, " +
                             "ld.skin_id ";

            int index = 1;
            ps = con.prepareStatement(sql);
            ps.setLong(index++, userId);

            rs = ps.executeQuery();
            if (rs.next()) {
                LimitationDeposits ld = new LimitationDeposits();
                ld.setId(rs.getLong("id"));
                ld.setMinimumDeposit(rs.getLong("min_dep"));
                ld.setMinimumFirstDeposit(rs.getLong("min_f_dep"));
                ld.setDefaultDeposit(rs.getLong("default_deposit"));
                logger.debug("LIMITATION_DEPOSITS id = " + ld.getId() + " user id = " + userId + " remarketing id = " + remarktingId);
                return ld;
            }

        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return null;
    }


    /**
     * return 0 if cant find user in remarketing_login else return the row id
     * @param con
     * @param userId the user id
     * @return 0 or row id
     * @throws SQLException
     */
    public static long isUserInRemarketingByUserId(Connection con, long userId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT " +
                            "id " +
                         "FROM " +
                             "remarketing_logins " +
                         "WHERE " +
                            "user_id = ? AND " +
                            "time_created > SYSDATE - (SELECT " +
                                                        "value " +
                                                      "FROM " +
                                                          "enumerators e " +
                                                      "WHERE " +
                                                          "e.enumerator LIKE 'limitation deposits' AND " +
                                                          "e.code LIKE 'days') " +
                         "ORDER BY " +
                            "time_created DESC";

            int index = 1;
            ps = con.prepareStatement(sql);
            ps.setLong(index++, userId);

            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getLong("id");
            }

        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return 0;
    }
    
    
    public static LimitationDeposits getLimit(Connection con, long skinId, long currencyId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = " SELECT " +
                       			" * " +
                         " FROM " +
                   				" limitation_deposits " +
                         " WHERE " + 
                   				" currency_id = ? " +
                         " AND " +
                   				" ( skin_id = ? or skin_id is null ) ";
            ps = con.prepareStatement(sql);
            ps.setLong(1, currencyId);
            ps.setLong(2, skinId);
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
                LimitationDeposits ld = new LimitationDeposits();
                ld.setId(rs.getLong("id"));
                ld.setMinimumDeposit(rs.getLong("minimum_deposit"));
                ld.setMinimumFirstDeposit(rs.getLong("minimum_first_deposit"));
                ld.setDefaultDeposit(rs.getLong("default_deposit"));
                logger.debug("LIMITATION_DEPOSITS id = " + ld.getId() + " skinId = " + skinId + " currencyId = " + currencyId);
                return ld;
            }

        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return null;
    }
}
