package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import com.anyoption.common.beans.base.Language;
import com.anyoption.common.daos.DAOBase;

public class LanguagesDAOBase extends DAOBase {
    /**
     * Get All Languages into a hash table
     *
     * @param conn connection to DB
     * @return language code
     * @throws SQLException
     */
    public static Hashtable<Long, Language> getAllLanguages(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Hashtable<Long, Language> hm = new Hashtable<Long, Language>();
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "languages";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                hm.put(rs.getLong("id"), getVO(rs));
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return hm;
    }
    
	/**
	 * Get All Languages into a hash table
	 * @param con connection to DB
	 * @return language code
	 * @throws SQLException
	 */
	public static HashMap<Long,Language> getAllLanguagess(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long,Language> hm = new HashMap<Long,Language>();
		try
		{
		    String sql = "select * from languages";
		    ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while ( rs.next() ) {
				hm.put(rs.getLong("id"), getVO(rs));
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return hm;
	}

    /**
     * Fill Language object
     *
     * @param rs ResultSet
     * @return Language object
     * @throws SQLException
     */
    protected static Language getVO(ResultSet rs) throws SQLException {
        Language vo = new Language();
        vo.setId(rs.getInt("id"));
        vo.setCode(rs.getString("code"));
        vo.setDisplayName(rs.getString("display_name"));
        return vo;
    }
    
	public static String getCodeById(Connection con, long id) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		String code = "";

		try
		{
		    String sql = "select code from languages "+
		    		     "where id = ? ";

		    ps = con.prepareStatement(sql);
		    ps.setLong(1, id);

			rs = ps.executeQuery();

			if ( rs.next() ) {
				code = rs.getString("code");
			}

		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}

		return code;
	}
	
	/**
	 * Get All Languages
	 * @param con connection to DB
	 * @return language code
	 * @throws SQLException
	 */
	public static ArrayList<Language> getAll(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Language> list = new ArrayList<Language>();
		try
		{
		    String sql = "select * from languages";
		    ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while ( rs.next() ) {
				list.add(getVO(rs));
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
}