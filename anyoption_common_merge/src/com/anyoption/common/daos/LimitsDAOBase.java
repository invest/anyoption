package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.beans.Limit;

/**
 * @author kiril.mutafchiev
 */
public class LimitsDAOBase extends DAOBase {

	public static long getBDAPredefinedDepositAmount(Connection con, long currencyId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select default_deposit_amount from bda_limits where currency_id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, currencyId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("default_deposit_amount");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0l;
	}

	public static long getBDADefaultInvestmentAmount(Connection con, long currencyId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select default_inv_amount from bda_limits where currency_id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, currencyId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("default_inv_amount");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0l;
	}
	
    public static Limit getLimitById(Connection conn, long id) throws SQLException {
        Limit l = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "l.*, " +
                    "w.user_name writer " +
                "FROM " +
                    "limits l, " +
                    "writers w " +
                "WHERE " +
                    "l.id = ? AND " +
                    "l.writer_id = w.id";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                l = get(rs);
                l.setWriterName(rs.getString("writer"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }
    
    public static Limit get(ResultSet rs) throws SQLException {
        Limit vo = new Limit();
        vo.setId(rs.getLong("id"));
        vo.setName(rs.getString("name"));
        vo.setMinDeposit(rs.getLong("MIN_DEPOSIT"));
        vo.setMinWithdraw(rs.getLong("MIN_WITHDRAW"));
        vo.setMaxDeposit(rs.getLong("MAX_DEPOSIT"));
        vo.setMaxWithdraw(rs.getLong("MAX_WITHDRAW"));
        vo.setMaxDepositPerDay(rs.getLong("MAX_DEPOSIT_PER_DAY"));
        vo.setMaxDepositPerMonth(rs.getLong("MAX_DEPOSIT_PER_MONTH"));
        vo.setTimeLastUpdate(convertToDate(rs.getTimestamp("TIME_LAST_UPDATE")));
        vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
        vo.setWriterId(rs.getLong("WRITER_ID"));
        vo.setComments(rs.getString("COMMENTS"));
        vo.setMinBankWire(rs.getLong("min_bank_wire"));
        vo.setMaxBankWire(rs.getLong("max_bank_wire"));
        vo.setFreeInvestMin(rs.getLong("free_invest_min"));
        vo.setFreeInvestMax(rs.getLong("free_invest_max"));
        vo.setBankWireFee(rs.getLong("bank_wire_fee"));
        vo.setCcFee(rs.getLong("cc_fee"));
        vo.setChequeFee(rs.getLong("cheque_fee"));
        vo.setDepositDocsLimit1(rs.getLong("deposit_docs_limit1"));
        vo.setDepositDocsLimit2(rs.getLong("deposit_docs_limit2"));
        vo.setMinFirstDeposit(rs.getLong("min_first_deposit"));
        vo.setAmountForLowWithdraw(rs.getLong("amt_for_low_withdraw"));
        return vo;
  }
    
}