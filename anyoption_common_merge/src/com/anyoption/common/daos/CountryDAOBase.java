package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.faces.model.SelectItem;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.PaymentMethodCountry;
import com.anyoption.common.enums.CountryStatus;
import com.anyoption.common.util.ConstantsBase;

/**
 * Country DAO Base class.
 *
 * @author Kobi
 */
public class CountryDAOBase extends DAOBase {


	/**
	 * Get id by country_code
	 * @param con
	 * 		connrction to Db
	 * @param code
	 * 		country code
	 * @return
	 * 		id of the country
	 * @throws SQLException
	 */
	public static long getIdByCode(Connection con, String code) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		long id = 0;

		try
		{
		    String sql = " select id " +
		    			 " from countries "+
		    		     " where a2 like ? ";

		    ps = con.prepareStatement(sql);
		    ps.setString(1, code);

			rs = ps.executeQuery();

			if ( rs.next() ) {
				id = rs.getLong("id");
			}

		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}

		return id;
	}

	/**
	 * Returns a country name by country id
	 * @param con - DB connection
	 * @param id - country id
	 * @return country name
	 * @throws SQLException
	 */
	public static String getNameById(Connection con, int id) throws SQLException {

		String name = ConstantsBase.EMPTY_STRING;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try
		{
		    String sql = " select display_name " +
		    			 " from countries " +
		    			 " where id=? ";

		    ps = con.prepareStatement(sql);
		    ps.setInt(1, id);

			rs = ps.executeQuery();

			if ( rs.next() ) {
				name = rs.getString("display_name");
			}

		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}

		return name;
	}

	/**
	 * Get country code(A2) by id
	 * @param con
	 * 		connection to DB
	 * @param id
	 * 		country id
	 * @return
	 * 		country code
	 * @throws SQLException
	 */
	public static String getCodeById(Connection con, long id) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		String code = "";    // A2 code (2 letters)

		try
		{
		    String sql = "select a2 from countries "+
		    		     "where id = ? ";

		    ps = con.prepareStatement(sql);
		    ps.setLong(1, id);

			rs = ps.executeQuery();

			if ( rs.next() ) {
				code = rs.getString("a2");
			}

		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}

		return code;
	}


	/**
	 * Get all countries
	 * @param con
	 * 		connection to Db
	 * @return
	 * 		List of all countries
	 * @throws SQLException
	 */
	public static ArrayList<Country> getAll(Connection con) throws SQLException {

	  Statement ps = null;
	  ResultSet rs = null;
	  ArrayList<Country> list = new ArrayList<Country>();

	  try
		{
		    String sql = "select * from countries where id !=" + ConstantsBase.COUNTRY_NORTH_KOREA_ID + " order by country_name";

			ps = con.createStatement();
			rs = ps.executeQuery(sql);

			while ( rs.next() ) {
				list.add(getVO(rs));
			}
		}

		finally
		{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
   }


	/**
	 * Fill Country object
	 * @param rs
	 * 		ResultSet
	 * @return
	 * 		Country object
	 * @throws SQLException
	 */
	protected static Country getVO(ResultSet rs) throws SQLException {

		Country vo = new Country();

		vo.setId(rs.getLong("id"));
		vo.setName(rs.getString("country_name"));
		vo.setA2(rs.getString("a2"));
		vo.setA3(rs.getString("a3"));
		vo.setPhoneCode(rs.getString("phone_code"));
		vo.setDisplayName(rs.getString("display_name"));
		vo.setSupportPhone(rs.getString("support_phone"));
		vo.setSupportFax(rs.getString("support_fax"));
		vo.setGmtOffset(rs.getString("gmt_offset"));
		vo.setSmsAvailable(rs.getInt("is_sms_available")==1);
		vo.setHaveUkashSite(rs.getInt("is_have_ukash_site")==1);
		vo.setAlphaNumericSender(rs.getInt("is_alphanumeric_sender")==1);
        vo.setNetreferRegBlock(rs.getInt("is_netrefer_reg_block")==1);
        vo.setCapitalCity(rs.getString("CAPITAL_CITY"));
        vo.setCapitalCityLatitude(rs.getString("CAPITAL_CITY_LATITUDE"));
        vo.setCapitalCityLongitude(rs.getString("CAPITAL_CITY_LONGITUDE"));		
		vo.setRegulated(rs.getLong("is_regulated") == 1 ? true : false);
        vo.setCountryStatus(CountryStatus.getById(rs.getLong("is_blocked")));
		return vo;
	}

	public static ArrayList<SelectItem> getStatesSI(Connection con) throws SQLException{
		ArrayList<SelectItem> retVal = new ArrayList<SelectItem>();
		Statement ps = null;
		ResultSet rs = null;

		String sql = "select * from states order by state_name";
		ps = con.createStatement();
		try{
			rs = ps.executeQuery(sql);
			while(rs.next()){
				SelectItem si = new SelectItem();
				si.setValue(rs.getLong("id"));
				si.setLabel(rs.getString("state_name"));
				retVal.add(si);
			}

		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return retVal;
	}

	public static ArrayList<SelectItem> getStatesCodeSI(Connection con) throws SQLException{
		ArrayList<SelectItem> retVal = new ArrayList<SelectItem>();
		Statement ps = null;
		ResultSet rs = null;

		String sql = "select * from states order by state_name";
		ps = con.createStatement();
		try{
			rs = ps.executeQuery(sql);
			while(rs.next()){
				SelectItem si = new SelectItem();
				si.setValue(rs.getString("code"));
				si.setLabel(rs.getString("state_name"));
				retVal.add(si);
			}

		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return retVal;
	}

	/**
	 * get phone code by country id
	 * @param con
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static String getPhoneCodeById(Connection con, long id) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		String code = "";

		try	{
		    String sql =
		    	"SELECT " +
		    		"phone_code " +
		    	 "FROM " +
		    	 	"countries "+
		    	 "WHERE " +
		    	   	"id = ? ";

		    ps = con.prepareStatement(sql);
		    ps.setLong(1, id);
			rs = ps.executeQuery();
			if ( rs.next() ) {
				code = rs.getString("phone_code");
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return code;
	}

	/**
	 * Get all arab countries (select by default skin id)
	 * @param con
	 * 		connection to Db
	 * @return
	 * 		Set of all arab countries
	 * @throws SQLException
	 */
	public static Set<String> getAllArabCountriesId(Connection con) throws SQLException {

	  Statement ps = null;
	  ResultSet rs = null;
	  Set<String> set = new HashSet<String>();

	  try
		{
		    String sql = "SELECT country_id FROM skin_url_country_map WHERE skin_id=4";

			ps = con.createStatement();
			rs = ps.executeQuery(sql);

			while (rs.next()) {
				set.add(String.valueOf(rs.getLong("country_id")));
			}
		}

		finally
		{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return set;
   }

	/**
	 * Get map of all states (USA) (select by default skin id)
	 * @param con
	 * 		connection to Db
	 * @return
	 * 		Map of all states (USA) {stateId}{stateCode}
	 * @throws SQLException
	 */
	public static Map<Long, String> getAllUsStates(Connection con) throws SQLException {

	  Statement ps = null;
	  ResultSet rs = null;
	  Map<Long, String> map = new HashMap<Long, String>();

	  try
		{
		    String sql = "SELECT id, code FROM states";

			ps = con.createStatement();
			rs = ps.executeQuery(sql);

			while (rs.next()) {
				map.put(Long.valueOf(rs.getLong("id")), rs.getString("code"));
			}
		}

		finally
		{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return map;
   }

	/**
	 * Get available payment method by country id
	 * @param con
	 * @param countryId
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<Long, PaymentMethodCountry> getPaymentMethodsByCountryId(Connection con, long countryId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, PaymentMethodCountry> list = new HashMap<Long, PaymentMethodCountry>();

		try {
			String sql =
				"SELECT " +
					"* " +
				"FROM " +
					"payment_methods_countries " +
				"WHERE " +
					"country_id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, countryId);
			rs = ps.executeQuery();

			while(rs.next()) {
				PaymentMethodCountry vo = new PaymentMethodCountry();
				vo.setId(rs.getLong("id"));
				vo.setPaymentMethodId(rs.getLong("payment_id"));
				vo.setCountryId(rs.getLong("country_id"));
				vo.setExcluded(rs.getLong("exclude_ind") == 1 ? true : false);
				list.put(new Long(vo.getPaymentMethodId()), vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * Get all countries groups
	 * @param con
	 * 		connection to Db
	 * @return
	 *
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getCountriesGroupType(Connection con) throws SQLException {

	  Statement ps = null;
	  ResultSet rs = null;
	  ArrayList<SelectItem> list = new ArrayList<SelectItem>();

	  try
		{
		    String sql = "select * from countries_group_type order by name";

			ps = con.createStatement();
			rs = ps.executeQuery(sql);

			while ( rs.next() ) {
				list.add(new SelectItem(rs.getString("id"),rs.getString("name")));
			}
		}

		finally
		{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
   }

	public static Country getById(Connection conn, long countryId) throws SQLException {
	    Country c = null;
	    PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    try {
	        String sql =
	            "SELECT " +
	                "* " +
	            "FROM " +
	                "countries " +
	            "WHERE " +
	                "id = ?";
	        pstmt = conn.prepareStatement(sql);
	        pstmt.setLong(1, countryId);
	        rs = pstmt.executeQuery();
	        if (rs.next()) {
	            c = getVO(rs);
	        }
	    } finally {
	        closeResultSet(rs);
	        closeStatement(pstmt);
	    }
	    return c;
	}
	
	/**
	 * @param connection
	 * @return
	 * @throws SQLException
	 */
	public static Map<Integer, Country> getCountriesMaping(Connection connection) throws SQLException {
		Map<Integer, Country> countries = new LinkedHashMap<Integer, Country>();
	    PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    try {
	        String sql =
	            "SELECT " +
	            "	* " +
	            "FROM " +
	            "	countries ";
	        
	        pstmt = connection.prepareStatement(sql);
	        rs = pstmt.executeQuery();
	        
	        while (rs.next()) {
				countries.put(rs.getInt("id"), getVO(rs));
			}
	    } finally {
	        closeResultSet(rs);
	        closeStatement(pstmt);
	    }
	    
	    return countries;
	}
	
	/**
	 * Get countries group tiers SI
	 * @param con
	 * @return list as ArrayList<SelectItem>
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getCountriesGroupTiersSI(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		try {
			String sql = " SELECT " +
							" cgt.id, " +
							" cgt.display_name " +
						" FROM " +
							" countries_group_tiers cgt " +
						" ORDER BY " +
							" cgt.id ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new SelectItem(rs.getLong("id"), rs.getString("display_name")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
}