/**
 * 
 */
package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.anyoption.common.beans.Writer;

/**
 * @author kirilim
 *
 */
public class WritersDAOBase extends DAOBase {

	/**
	 * This method loads writer info specifically for the login logic
	 * @param conn
	 * @param writerName
	 * @param writer
	 * @throws SQLException
	 */
	public static void loadWriterByName(Connection conn, String writerName, Writer writer) throws SQLException {
		if (null == writerName || writerName.trim().equals("")) {
            return;
        }

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
        	String sql =
	            " SELECT " +
	                " w.* " +
	            " FROM " +
	                " writers w " +
	            " WHERE " +
	                " user_name = ? ";
        	pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, writerName.toUpperCase());
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	loadWriterVO(rs, writer);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
	}
	
	
	public static boolean writerHasRole(Connection con, long writerId, String role) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;
		try {
			pstmt = con.prepareStatement("SELECT r.ROLE FROM writers w, roles r WHERE w.id = ? AND w.USER_NAME=r.USER_NAME AND r.role = ? ");
			pstmt.setLong(1, writerId);
			pstmt.setString(2, role);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				return true;
			} else {
				return false;
			}
		} finally {
			closeResultSet(rs);
            closeStatement(pstmt);
		}
	}

	
	public static List<String> getWriterRoles(Connection con, String writer) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        LinkedList<String> res = new LinkedList<String>();
		try {
			pstmt = con.prepareStatement("SELECT role FROM roles WHERE user_name = ?");
			pstmt.setString(1, writer.toUpperCase());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				res.add(rs.getString("role"));
			}
		} finally {
			closeResultSet(rs);
            closeStatement(pstmt);
		}
		
		return res;
	}

	private static void loadWriterVO(ResultSet rs, Writer vo) throws SQLException {
		vo.setId(rs.getLong("id"));
    	vo.setUserName(rs.getString("USER_NAME"));
    	vo.setPassword(rs.getString("password"));
    	vo.setIsActive(rs.getLong("IS_ACTIVE"));
    	vo.setLastFailedTime(convertToDate(rs.getTimestamp("last_failed_time")));
    	vo.setFailedCount(rs.getInt("failed_count"));
        vo.setRoleId(rs.getLong("role_id"));
        vo.setPasswordReset(rs.getLong("is_password_reset") > 0 ? true : false);
	}
	
	public static void getSalesTypeDepartmentId(Connection con, long writerId, Writer writer) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = 
					" SELECT " +
						" w.sales_type_dept_id, " +
						" w.sales_type " +
					" FROM " +
						" writers w " +
					" WHERE " +
						" w.id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, writerId);
			rs = ps.executeQuery();
			if (rs.next()) {
				writer.setSalesTypeDepartmentId(rs.getLong("sales_type_dept_id"));
				writer.setSalesType(rs.getInt("sales_type"));
			}
		}
		finally
		{
			closeResultSet(rs);
			closeStatement(ps);
		}
	}

	public static String getWriterPassword(Connection con, long writerId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT password FROM writers WHERE id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, writerId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("password");
			} else {
				return "";
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	public static String getWriterName(Connection con,long id)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  try
			{
			    String sql="select user_name from writers where id=" + id;

				ps = con.prepareStatement(sql);

				rs=ps.executeQuery();

				if (rs.next()) {
					return rs.getString("user_name");
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return "WRITER " + id + " NOT EXIST!! ";
	}
	
	
	public static HashMap<Long, String> getAllDepartments(Connection con) throws SQLException {

		Statement st = null;
		ResultSet rs = null;
		HashMap<Long, String> departmentlist = new HashMap<>();

		try {

			String sql = "SELECT * FROM DEPARTMENTS D ORDER BY D.DEPT_NAME ";

			st = con.createStatement();
			rs = st.executeQuery(sql);

			while (rs.next()) {
				departmentlist.put(rs.getLong("ID"), rs.getString("DEPT_NAME"));
			}

		} finally {
			closeResultSet(rs);
			closeStatement(st);
		}

		return departmentlist;
	}
	
	public static Writer getAssignedWriter(Connection con, long userId)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  Writer assignedWriter = new Writer();

		  try
			{
			    String sql="SELECT " + 
			    				" w.email, " +
			    				" w.user_name, " +
								" w.first_name, " +
								" w.last_name " +
							" FROM  " +
								" population_users pu LEFT JOIN writers w ON w.id = pu.curr_assigned_writer_id " +
							" WHERE " +
								" pu.user_id = " + userId;

				ps = con.prepareStatement(sql);

				rs=ps.executeQuery();

				if (rs.next()) {
					assignedWriter.setEmail(rs.getString("email"));
					assignedWriter.setUserName(rs.getString("user_name"));
					assignedWriter.setFirstName(rs.getString("first_name"));
					assignedWriter.setLastName(rs.getString("last_name"));
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		  
		  return assignedWriter;
			
	}
}