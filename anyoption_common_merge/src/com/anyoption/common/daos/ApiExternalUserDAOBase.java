package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.base.ApiExternalUser;

/**
 * @author EranL
 *
 */
public class ApiExternalUserDAOBase extends DAOBase {
	
	/**
	 * Insert API external user
	 * @param con
	 * @param apiUserName
	 * @param apiExternalUserReference
	 * @param userName
	 * @return
	 * @throws SQLException
	 * @throws CryptoException
	 */
	public static void insertAPIExternalUser(Connection con, ApiExternalUser aeu) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql= " INSERT INTO" +
						" api_external_users(id, " +
						"					 api_user_id, " +
						"					 user_id, " +
						"					 reference, " +
						"					 ip, " +
						"					 utc_offset)" +
						" VALUES(seq_api_external_users.NEXTVAL, ?, ?, ?, ?, ?)";
			ps = con.prepareStatement(sql);
			ps.setLong(1, aeu.getApiUserId());
			ps.setLong(2, aeu.getUserId());
			ps.setString(3, aeu.getReference());
			ps.setString(4, aeu.getIp());
			ps.setString(5, aeu.getUtcOffset());
			ps.executeUpdate();
			aeu.setId(getSeqCurValue(con, "seq_api_external_users"));
		} finally {
			closeStatement(ps);
		}						
	}
	
	/**
	 * Get API external user
	 * @param con
	 * @param apiUserName
	 * @param apiExternalUserReference
	 * @return
	 * @throws SQLException
	 * @throws CryptoException
	 */
	public static ApiExternalUser getAPIExternalUser(Connection con, String apiExternalUserReference, long apiUserId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ApiExternalUser vo = null;
		try {
			String sql =" SELECT " +
						"	apu.* " +
						" FROM " +
						"	api_external_users apu, " +
						"   api_users au " +
						" WHERE " +
						"   au.id = apu.api_user_id " +						
						"   AND apu.reference = ? " +
						"   AND au.id = ? "; 
	        ps = con.prepareStatement(sql);
	        ps.setString(1, apiExternalUserReference);
	        ps.setLong(2, apiUserId);
	        rs = ps.executeQuery();
	        if (rs.next()) {
	        	vo = getVO(rs);
	        }
		} finally {
			closeResultSet(rs);
	        closeStatement(ps);
		}
		return vo;
	}
	
	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private static ApiExternalUser getVO(ResultSet rs) throws SQLException {
        ApiExternalUser user = new ApiExternalUser();
        user.setId(rs.getLong("id"));
        user.setApiUserId(rs.getLong("api_user_id"));
        user.setUserId(rs.getLong("user_id"));
        user.setReference(rs.getString("reference"));
        return user;
	}
		
}
