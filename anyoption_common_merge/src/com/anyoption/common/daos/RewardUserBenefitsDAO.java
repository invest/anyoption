package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardBenefit;
import com.anyoption.common.beans.RewardBenefitManagement;
import com.anyoption.common.beans.RewardUserBenefit;
import com.anyoption.common.rewards.RewardUtil;

/**
 * @author liors
 *
 */
public class RewardUserBenefitsDAO extends DAOBase {
	private final static Logger logger = Logger.getLogger(RewardUserBenefitsDAO.class);
		
	public static int insertValueObject(Connection connection, RewardUserBenefit rewardUserBenefit) throws SQLException {
		PreparedStatement preparedStatement = null;
		int index = 1;
		try {
			String sql =
						"INSERT INTO rwd_user_benefits" +
						"	(" +
						"		id" +
						"		,user_id" +
						"		,rwd_benefit_id" +
						"		,is_active" +
						"		,time_created" +
						"	)" +
						"	VALUES" +
						"	(" +
						"		seq_rwd_user_benefits.nextval" +
						"		,?" +
						"		,?" +
						"		,?" +
						"		,sysdate" +
						"	)";
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setLong(index++, rewardUserBenefit.getUserId());
			preparedStatement.setInt(index++, rewardUserBenefit.getRewardBenefitId());
			preparedStatement.setBoolean(index++, rewardUserBenefit.isActive());
			preparedStatement.executeUpdate();
		} finally {
			closeStatement(preparedStatement);
		}
		
		return RewardUtil.safeLongToInt(getSeqCurValue(connection, "seq_rwd_user_benefits"));
	}
	
	public static boolean update(Connection connection, long id, boolean isActive) throws SQLException {
		PreparedStatement preparedStatement = null;
		int index = 1;
		try {
			String sql = 
					"UPDATE " +
					"	rwd_user_benefits " +
					"SET" +
					"	is_active = ? " +
					"WHERE" +
					"	id = ?";
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setBoolean(index++, isActive);
			preparedStatement.setLong(index++, id);
			preparedStatement.executeUpdate();
		} finally {
			closeStatement(preparedStatement);
		}
		
		return true;
	}
	
	public static ArrayList<RewardBenefitManagement> getUserBenefitToGrant(Connection connection, long userId, int tierId, boolean isSkipped) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int index = 1;
		ArrayList<RewardBenefitManagement> list = new ArrayList<RewardBenefitManagement>();
		try {   
    		String sql =	
    				"SELECT" +
    				"	rb.id as rb_id" +
    				"	,rb.bonus_id" +
    				"	,rub.id as rub_id " +
    				"FROM" +
    				"	rwd_benefits rb" +
    				"		LEFT JOIN rwd_user_benefits rub " +
    				"				ON rb.id = rub.rwd_benefit_id " +
    				"					AND rub.user_id = ? " +
    				"WHERE" +
    				"	(rub.id IS NULL " +
    				"		OR (rub.is_active = 0 " +
    				"			AND rb.is_grant_once = 0))" +
    				"	AND ((rb.rwd_tier_id <= ?" +
    				"		AND rb.bonus_id IS NULL)" +
    				"	OR (rb.rwd_tier_id = ?" +
    				"		AND rb.bonus_id IS NOT NULL" +
    				"		AND EXISTS (SELECT 1" +
    				"					FROM users u" +
    				"					WHERE u.bonus_abuser = 0" +
    				"						AND u.id = ?)";
			if (isSkipped) {
				sql += "	AND rb.is_grant_when_skip = ?";
			}
    			   sql += "))"; 
    				
				            
		    pstmt = connection.prepareStatement(sql);
		    pstmt.setLong(index++, userId);
		    pstmt.setInt(index++, tierId);
		    pstmt.setInt(index++, tierId);
		    pstmt.setLong(index++, userId);
		    if (isSkipped) {
		    	pstmt.setInt(index++, 1);
		    }
		    resultSet = pstmt.executeQuery();

		    while (resultSet.next()) {
		    	RewardBenefitManagement benefit = new RewardBenefitManagement();
		    	benefit.setBenefitId(resultSet.getInt("rb_id"));
		    	benefit.setBonusId(resultSet.getLong("bonus_id"));
		    	benefit.setUserBenefitId(resultSet.getInt("rub_id"));
		    	list.add(benefit);
		    }
    	} finally {
		   closeStatement(pstmt);
		   closeResultSet(resultSet);
		}
		return list;	
	}
	
	public static RewardUserBenefit getRewardUserBenefitValueObject(ResultSet resultSet) throws SQLException {
		RewardUserBenefit valueObject = new RewardUserBenefit();
		//TODO
		return valueObject;
	}
	
	public static void updateRemoveRewards(Connection conn, long userId, int tier) throws SQLException {
		PreparedStatement ps = null;

        try {
            String sql = "UPDATE " +
						 	"rwd_user_benefits rub " +
						 "SET " +
							"rub.is_active = 0 " +
						 "WHERE " +
							"rub.user_id = ? AND " +
							"rub.rwd_benefit_id in (SELECT " +
												   	  "id " +
												   "FROM " +
												  	  "rwd_benefits rb " +
												   "WHERE " +
												  	  "rb.id = rub.rwd_benefit_id AND " +
												  	  "rb.rwd_tier_id > ? AND " +
												  	  "rb.is_affected_from_demotion = 1 AND " +
												  	  "rb.bonus_id is null " +
											  	  ")";
            ps = conn.prepareStatement(sql);
            int index = 1;
            ps.setLong(index++, userId);
            ps.setLong(index++, tier);
            
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
		
	}
	
	public static long getUserMaxAcademyBenefit(Connection connection, long userId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int index = 1;
		long benefitId = 0;
		try {   
    		String sql =	
    				"SELECT " +
    				"	MAX(rub.rwd_benefit_id) as rwd_benefit_id " +
    				"FROM " +
    				"	rwd_user_benefits rub " +
    				"WHERE " +
    				"	rub.user_id = ? AND " +
    				"	rub.rwd_benefit_id IN (" + RewardBenefit.ANYOPTION_ACADEMY_1ST_2ND_RESTRICTION_LEVELS + ", " + RewardBenefit.ACADEMY_3RD_RESTRICTION_LEVEL + ", " + RewardBenefit.ANYOPTION_ACADEMY_4TH_RESTRICTION_LEVEL + ") AND " +
    				"	rub.is_active = 1 ";
    				
				            
		    pstmt = connection.prepareStatement(sql);
		    pstmt.setLong(index++, userId);
		    resultSet = pstmt.executeQuery();

		    if (resultSet.next()) {
		    	benefitId = resultSet.getLong("rwd_benefit_id");
		    }
    	} finally {
		   closeStatement(pstmt);
		   closeResultSet(resultSet);
		}
		return benefitId;
	}
}
