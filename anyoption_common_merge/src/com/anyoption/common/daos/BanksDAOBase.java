package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import javax.faces.model.SelectItem;

import com.anyoption.common.beans.base.BankBranches;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.BankBase;
import com.anyoption.common.util.ConstantsBase;

public class BanksDAOBase extends DAOBase {
	
    public static Hashtable<Long, BankBase> getBanks(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Hashtable<Long, BankBase> hm = new Hashtable<Long, BankBase>();
        try {
            String sql=
                "SELECT " +
                    "* " +
                "FROM " +
                    "banks " +
                "ORDER BY " +
                    "id";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                BankBase vo = getVO(conn, rs);
                hm.put(vo.getId(), vo);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return hm;
    }

    protected static BankBase getVO(Connection con, ResultSet rs) throws SQLException {
        BankBase vo = new BankBase();
        vo.setId(rs.getLong("id"));
        vo.setName(rs.getString("name"));
        vo.setBankCode(rs.getLong("bank_code"));
        return vo;
    }
    
    public static HashMap<Long,BankBase> getBanksET(Connection con, int bankType, long businessCase ) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashMap<Long, BankBase> hm = new HashMap<Long, BankBase>();
        try {
            String sql = " SELECT " +
            				" * " +
            			" FROM " +
                       		" banks b" +
                       	" WHERE " +
                       		" b.business_case_id = " + businessCase + " AND ";
            			if (bankType == ConstantsBase.BANK_TYPE_DEPOSIT){
            				sql +=     " b.deposit = 1 ";
               		   	} else {
               		   		sql +=     " b.withdrawal = 1 ";
               		   	}
               			sql += " AND NOT EXISTS (SELECT " +
               										" * " +
               									" FROM " +
               										" skin_banks sb " +
               									" WHERE " +
               										" sb.bank_id = b.id) " +
               									" ORDER BY " +
               										" b.id ";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                BankBase vo = getVO(con,rs);
                hm.put(vo.getId(), vo);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return hm;
    }
    
    public static ArrayList<BankBranches> getBanksBranches(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<BankBranches> list = new ArrayList<BankBranches>();
        try {
            String sql="SELECT " +
                           "* " +
                       "FROM " +
                           "bank_branches " +
                       "ORDER BY " +
                           "id";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                BankBranches vo = getVOBankBranches(con,rs);
                list.add(vo);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return list;
    }    


    
    /**
     * Get bank wire withdraw by skin
     * @param con
     * @param skinId
     * @return bankList as ArrayList<Bank>
     * @throws SQLException
     */
    public static ArrayList<BankBase> getBankWireWithdrawBySkin(Connection con, long skinId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<BankBase> bankList = new ArrayList<BankBase>();
		try {
			String sql = " SELECT " +
							" b.* " +
						" FROM " +
							" banks b, " +
							" skin_banks sb " +
						" WHERE " +
							" b.id = sb.bank_id " +
							" AND b.business_case_id = " + Skin.SKIN_BUSINESS_ANYOPTION +
							" AND b.withdrawal = 1 " +
							" AND sb.skin_id = ? " +
						" ORDER BY " +
							" b.id";
			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);
			rs = ps.executeQuery();
			while( rs.next() ) {
				BankBase bank = new BankBase();
				bank.setId(rs.getLong("id"));
				bank.setName(rs.getString("name"));
				bankList.add(bank);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return bankList;
	}
    
    public static HashMap<Long,BankBase> getBanks(Connection con, int bankType, long businessCase ) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashMap<Long, BankBase> hm = new HashMap<Long, BankBase>();
        try {
            String sql = " SELECT " +
                            " * " +
                       " FROM " +
                           " banks b" +
                       " WHERE " +
                           " b.business_case_id = " + businessCase + " AND ";
                       if (bankType == ConstantsBase.BANK_TYPE_DEPOSIT){
                  sql +=     " b.deposit = 1 ";
                       } else {
                  sql +=     " b.withdrawal = 1 ";
                       }
                  sql += " AND NOT EXISTS (SELECT " +
                		  					" * " +
                  						" FROM " +
                  							" skin_banks sb " +
                  						" WHERE " +
                  							" sb.bank_id = b.id) " +
                  						" ORDER BY " +
                  							" b.id ";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                BankBase vo = getVO(con,rs);
                hm.put(vo.getId(), vo);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return hm;
    }

    protected static BankBranches getVOBankBranches(Connection con,ResultSet rs) throws SQLException{
        BankBranches vo = new BankBranches();
        vo.setId(rs.getLong("id"));
        vo.setBranchName(rs.getString("branch_name"));
        vo.setBranchCode(rs.getLong("branch_code"));
        vo.setBankId(rs.getLong("bank_id"));
        return vo;
    }
    
  public static ArrayList<SelectItem> getBankWireWithdrawBySkinSI(Connection con, long skinId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		try {
			String sql = " SELECT " +
							" b.* " +
						" FROM " +
							" banks b, " +
							" skin_banks sb " +
						" WHERE " +
							" b.id = sb.bank_id " +
							" AND b.business_case_id = " + Skin.SKIN_BUSINESS_ANYOPTION +
							" AND b.withdrawal = 1 " +
							" AND sb.skin_id = ? " +
						" ORDER BY " +
							" b.id";
			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);
			rs = ps.executeQuery();
			while( rs.next() ) {
				list.add(new SelectItem (rs.getLong("id"), rs.getString("name")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
}