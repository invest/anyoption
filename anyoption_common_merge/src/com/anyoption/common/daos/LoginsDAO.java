package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Login;
import com.anyoption.common.beans.ServerConfiguration;

/**
 * @author kirilim
 */
public class LoginsDAO extends DAOBase {
	private static final Logger log = Logger.getLogger(LoginsDAO.class);
	
	public static long insert(Connection conn, String userName, Login login) throws SQLException {
		PreparedStatement ps = null;
		int res;
		int i = 1;
		try {
            String sql =
            	"INSERT INTO logins (id, user_id, login_time, login_offset, user_agent, device_unique_id, is_success, login_from, writer_id, server_id, finger_print, os_version, device_type, app_version, device_family, combination_id, dynamic_param, aff_sub1, aff_sub2, aff_sub3) " +
            	"SELECT seq_logins.nextval, id, sysdate, ?, ?, ?, ?, ? , ?, ?, ?, ?, ?, ?, ?, ? ,? ,? ,? ,? FROM users WHERE user_name LIKE ?";

            ps = conn.prepareStatement(sql);
            ps.setString(i++, login.getLoginOffset());
            ps.setString(i++, login.getUserAgent());
            ps.setString(i++, login.getDeviceUniqueId());
            ps.setBoolean(i++, login.isSuccess());
            ps.setLong(i++, login.getLoginFrom());
            ps.setLong(i++, login.getWriterId());
            ps.setString(i++, ServerConfiguration.getInstance().getServerName());
            ps.setLong(i++, login.getFingerPrint());
            ps.setString(i++, login.getOsVersion());
            ps.setString(i++, login.getDeviceType());
            ps.setString(i++, login.getAppVersion());
            ps.setLong(i++, login.getDeviceFamilyId());            
            ps.setLong(i++, login.getCombinationId());
            ps.setString(i++, login.getDynamicParam());
            ps.setString(i++, login.getAffSub1());
            ps.setString(i++, login.getAffSub2());
            ps.setString(i++, login.getAffSub3());            
            ps.setString(i++, userName.toUpperCase());
            res = ps.executeUpdate();
            
            log.debug("login params: " + login.toString());
        } finally {
            closeStatement(ps);
        }
        if (false == login.isSuccess() || res == 0) { // isSuccess == false -> no login || userName is not in users table at all
        	return -1;
        }
        return getSeqCurValue(conn, "seq_logins");
	}
}