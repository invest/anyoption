package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Task;
import com.anyoption.common.beans.TaskGroup;
import com.anyoption.common.daos.DAOBase;

/**
 * @author LioR SoLoMoN
 *
 */
public class TaskDAO extends DAOBase {
	private static final Logger logger = Logger.getLogger(TaskDAO.class);
    
	
	/**
	 * @param connection
	 * @param task
	 * @throws SQLException
	 */
	public static void insertTask(Connection connection, Task task) throws SQLException {
		int index = 0;
		String sql =
				"INSERT INTO " +
			 	"	task_subjects " +
				"	(" +
				"	id " +
				"	,reference_id " +
				"	,status " +
				"	,time_created " +
				"	,task_subjects_group_id " +
				"	,writer_id " +
				"	,time_updated " +
				"	)" +
				"VALUES" +
				"	(" +
				"	seq_task_subjects.nextval " +
				"	,? " +
				"	,? " +
				"	,sysdate " +
				"	,? " +
				"	,? " +
				"	,sysdate " +
				"	)";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(++index, task.getReferenceId());
			ps.setLong(++index, task.getStatus().getId());
			ps.setLong(++index, task.getTaskSubjectsGroupId());
			ps.setLong(++index, task.getWriterId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * @param connection
	 * @param taskGroup
	 * @throws SQLException
	 */
	public static void insertTaskGroup(Connection connection, TaskGroup taskGroup) throws SQLException {
		String sql = 
				"INSERT INTO " +
				"	task_subjects_group " +
				"	( " +
				"		id " +
				"		,task_subjects_types_id " +
				"		,parameters " +
				//"		,priority " +
				"		,time_created " +
				"	) " +
				"VALUES " +
				"	( " +
				"		seq_task_subjects_group.nextval " +
				"		,? " +
				"		,? " +
				//"		,? " +
				"		,sysdate " +
				"	)";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, taskGroup.getTaskSubjectsTypesId());
			ps.setString(2, taskGroup.getParameters());
			//ps.setInt(3, taskGroup.getPriority());
			ps.executeUpdate();
			taskGroup.setId((int)getSeqCurValue(connection, "seq_task_subjects_group"));
		} finally {
			closeStatement(ps);
		}
	}
}