package com.anyoption.common.daos;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

public class DAOBase {
    private static final Logger logger = Logger.getLogger(DAOBase.class);

    public static String ORDER_ASC = "asc";
    public static String ORDER_DESC = "desc";

    protected static void updateQuery(Connection conn, String sql) throws SQLException {
        Statement st = null;
        try {
            st = conn.createStatement();
            st.executeUpdate(sql);
        } finally {
            closeStatement(st);
        }
    }

    protected static long getSeqCurValue(Connection con, String seq) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement("select " + seq + ".currval from dual");
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getLong(1);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return 0;
    }

	  public static long getSequenceNextVal(Connection con,String seq) throws SQLException {
		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  try {
			  String sql = "SELECT " + seq + ".nextval from dual";

			  ps = con.prepareStatement(sql);
			  rs = ps.executeQuery();
				if (rs.next()) {
					return rs.getLong(1);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return 0;
	  }


    public static void closeStatement(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                logger.error(stmt, ex);
            }
        }
    }

    public static void closeResultSet(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                logger.error(rs, ex);
            }
        }
    }

    public static Date convertToDate(Timestamp t) {
        if (null == t) {
            return null;
        }
        return new Date(t.getTime());
    }

    public static Timestamp convertToTimestamp(Date d) {
        return convertToTimestamp(d, false);
    }

    public static Timestamp convertToTimestamp(Date d, boolean endOfSecond) {
        if (d == null) {
            return null;
        }
        Timestamp ts = new Timestamp(d.getTime());
        if (endOfSecond) {
            ts.setNanos(999999999);
        }
        return ts;
    }

    public static Date getTimeWithTimezone(String time) {
        Date d = null;
        if (null != time) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
            try {
                d = df.parse(time);
            } catch (ParseException pe) {
                logger.error("Can't parse time with timezone.", pe);
            }
        }
        return d;
    }
    
    public static int getDelaySecondConnetction(Connection con) throws SQLException {
		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  try {
			  String sql = "SELECT value from config where key = 'second_database_delay_milsec'";

			  ps = con.prepareStatement(sql);
			  rs = ps.executeQuery();
				if (rs.next()) {
					return rs.getInt(1);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return 0;
	  }

    public static String count (String sql ){
        String rowLimitedSql = "SELECT count(*) FROM ( " + sql + ")";
        return rowLimitedSql;
    }
    
    public static String rowLimit (String sql, long rowNum){
        String rowLimitedSql = "SELECT res.* FROM ( " + sql + ") res where rownum <= " + rowNum;
        return rowLimitedSql;
    }
    
    protected static Array getPreparedSqlArray(Connection con, List<?> arr) {
        ArrayDescriptor arrDesc = null;
        Array array = null;
		try {
			if(arr.size() > 0){
				if(arr.get(0) instanceof String){
					arrDesc = ArrayDescriptor.createDescriptor("STRING_TABLE", con);
			        array = new ARRAY(arrDesc, con, arr.toArray(new String[0]));
				} else if(arr.get(0) instanceof Long) {
					arrDesc = ArrayDescriptor.createDescriptor("NUMBER_TABLE", con);
			        array = new ARRAY(arrDesc, con, arr.toArray(new Long[0]));
				} else if(arr.get(0) instanceof Integer){
					arrDesc = ArrayDescriptor.createDescriptor("NUMBER_TABLE", con);
			        array = new ARRAY(arrDesc, con, arr.toArray(new Integer[0]));
				} else if(arr.get(0) instanceof Date){
					arrDesc = ArrayDescriptor.createDescriptor("DATE_TABLE", con);
			        array = new ARRAY(arrDesc, con, arr.toArray(new Date[0]));
				} else {
					Log.error("Can't get SQL PreparedArray! Object type is not defined!");
				}
			} else {
				Log.debug("List is empty!");
			}
				
		} catch (Exception e) {
			Log.error("Can't get SQL PreparedArray", e);
		}
         return array;		
	}
	
	public static void setStatementValue(String value, int index, CallableStatement cstmt) throws SQLException {
		if (value == null) {
			cstmt.setNull(index, OracleTypes.VARCHAR);
		} else {
			cstmt.setString(index, (String) value);
		}
	}
	
	public static void setStatementValue(Long value, int index, CallableStatement cstmt) throws SQLException {
		if (value == null) {
			cstmt.setNull(index, OracleTypes.NUMBER);
		} else {
			cstmt.setLong(index, (Long) value);
		}
	}
	
	public static void setStatementValue(Boolean value, int index, CallableStatement cstmt) throws SQLException {
		if (value == null) {
			cstmt.setNull(index, OracleTypes.NUMBER);
		} else {
			if ((Boolean) value) {
				cstmt.setInt(index, 1);
			} else {
				cstmt.setInt(index, 0);
			}
		}
	}
	
	public static void setStatementValue(Timestamp value, int index, CallableStatement cstmt) throws SQLException {
		if (value == null || ((Timestamp)value).getNanos() == 0) {
			cstmt.setNull(index, OracleTypes.TIMESTAMP);
		} else {
			cstmt.setTimestamp(index, (Timestamp) value);
		}
	}
	
	public static void setStatementValue(Double value, int index, CallableStatement cstmt) throws SQLException {
		if (value == null) {
			cstmt.setNull(index, OracleTypes.NUMBER);
		} else {
			cstmt.setDouble(index, (Double) value);
		}
	}
	
	public static void setStatementValue(Float value, int index, CallableStatement cstmt) throws SQLException {
		if (value == null) {
			cstmt.setNull(index, OracleTypes.NUMBER);
		} else {
			cstmt.setFloat(index, (Float) value);
		}
	}
	
	public static int executeUpdateRetry( PreparedStatement ps) throws SQLException {
		int k = 0;
		return executeUpdateRetry(ps, k) ;
	}
	
	private static int executeUpdateRetry( PreparedStatement ps, int k) throws SQLException {
        if (ps != null) {
            try {
                return ps.executeUpdate();
            } catch (SQLException sqle) {
            	if(sqle.getErrorCode() == 4068 && k < 2) {
            		logger.debug(sqle.getMessage());
            		logger.debug("executeUpdateRetry retry:" + k);
            		return executeUpdateRetry(ps, k++);
            	} else {
            		throw sqle;
            	}
            }
        } return 0;
	}
	
	public static void setArrayVal(ArrayList<?> value, int index, CallableStatement cstmt, Connection conn) throws SQLException {
		if (value == null) {
			cstmt.setNull(index, Types.ARRAY, "NUMBER_TABLE");
		} else {
			cstmt.setArray(index, getPreparedSqlArray(conn, value));
		}
	}
}