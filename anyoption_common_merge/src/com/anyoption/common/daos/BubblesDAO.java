package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.faces.model.SelectItem;

import com.anyoption.common.managers.MarketsManagerBase;

public class BubblesDAO extends DAOBase {

	public static Map<Long, String> getBubblesLevels(Connection con) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Map<Long, String> bubblesLevels = new LinkedHashMap<Long, String>();

		try {
			String sql = "select " +
							" * " +
						 "from " +
						 	"bubbles_pricing_levels " +
						 "order by " +
						 	"id";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				bubblesLevels.put(rs.getLong("id"), rs.getString("pricing_level_name"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return bubblesLevels;
	}
	
	
	public static ArrayList<SelectItem> getBubbleMarkets(Connection con, long skinId) throws SQLException{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		ArrayList<SelectItem> bubbleMarkets = new ArrayList<SelectItem>();
		
		try{
			String query = "SELECT m.id, m.display_name " +
						   "FROM markets m " +
						   "WHERE m.type_id = 5 ";
			pstmt = con.prepareStatement(query);
			rs = pstmt.executeQuery();
//			bubbleMarkets.add(new SelectItem(new Long(0), "lastlevels.selectAsset"));
			while(rs.next()){
				bubbleMarkets.add(new SelectItem(new Long(rs.getLong("id")), MarketsManagerBase.getMarketName(skinId, rs.getLong("id"))));
			}
		}finally{
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return bubbleMarkets;
	}

}
