package com.anyoption.common.daos;


import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.AssetIndexMarket;
import com.anyoption.common.beans.AssetIndexsTempData;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.base.AssetIndexBase;
import com.anyoption.common.beans.base.AssetIndexInfo;
import com.anyoption.common.beans.base.AssetIndexLevelCalc;
import com.anyoption.common.enums.AssetIndexGroupType;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.util.CommonUtil;

public class AssetIndexMarketDAO extends DAOBase {
	
	private static final Logger log = Logger.getLogger(AssetIndexMarketDAO.class);

	public static ArrayList<AssetIndexMarket> getAssetIndexMarkets(Connection con, long skinId, Map<Long, List<AssetIndexMarket>> groupAssetIndexMarket) throws SQLException {
		log.debug("Begin init AssetIndexMarkets for SkinID:" + skinId);
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<AssetIndexMarket> list = new ArrayList<AssetIndexMarket>();
		try {
            String sql = 
            		"SELECT  " +
						" market_description, " +
						" case when order_data.market_group_id = 2 and order_data.opp_type = 1 then 1 " + 
							" when order_data.market_group_id = 4 and order_data.opp_type = 1 then 2 " +
							" when order_data.market_group_id in (3,6) and order_data.opp_type = 1 then 4 " +
							" when order_data.opp_type = 3 then 5 " +
							" when order_data.opp_type = 4 then 6 end as indx_type, " +
						
						" case when order_data.country_groups = 6 and order_data.opp_type = 1 then -1 " +
							" when order_data.country_groups = 1 and order_data.opp_type = 1 then 1 " +
							" when order_data.country_groups = 2 and order_data.opp_type = 1 then 2 " +
							" when order_data.country_groups = 4 and order_data.opp_type = 1 then 3 " +
							" when order_data.country_groups = 3 and order_data.opp_type = 1 then 4 " +
							" when order_data.country_groups = 5 and order_data.opp_type = 1 then 5 " +
							" else 6 end as ORDERED_LIST_TWO, " +
						
            		      "  order_data.* " +
            		"FROM " +
	            		"( SELECT base_date.*, case when base_date.is_BO = 1 then 1 when  base_date.is_oplus = 1 then 3 else 4 end as opp_type " +
	            		" FROM " +
		                    "(SELECT DISTINCT " +                    
		                    " m.display_name as market_display_name, " +
		                    " m.feed_Name  as market_feed_Name, " +
		                    " m.id as marketId, " +
		                    " m.name as marketName, " +
		                    " m.TIME_CREATED as MARKET_TIME_CREATED, " +
		                    "ot.time_zone, " +
		                    "( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=4 and ot1.is_active=1 )  as monthly " +
		                    ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=3 and ot1.is_active=1 )  as weekly " +
		                    ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=2 and ot1.is_active=1 )  as dayly " +
		                    ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=1 and ot1.is_active=1 )  as hourly, " +
		                    "sm.market_group_id, " +
		                    "mg.display_name as group_name, " +
		                    "mdg.ASSET_DISPLAY_NAME as display_group_name_key, " +
		                    "nvl(smg.skin_display_group_id,0) as country_groups, " +
		                    "(SELECT " +
		                    	"count(1) " +
		                     "FROM " +
		                     	"skin_market_group_markets smgm2, " +
		                     	"opportunity_templates ot1 " +
		                     "WHERE " +
		                     	"smgm2.skin_market_group_id = smg.skin_market_group_id AND " +
		                     	"smgm2.market_id = ot1.market_id AND " +
		                     	"ot1.scheduled = 2 AND " +
		                     	"ot1.is_active = 1 AND " +
		                     	"(ot1.sun_f + ot1.mon_f + ot1.tue_f + ot1.wed_f + ot1.thu_f + ot1.fri_f + ot1.sat_f) > 1 " +
		                    ") AS group_markets_counter, " +
		                    "mns.name as market_name_by_skin, " +
		                    "m.option_plus_market_id " +
		                    " , case when ot.opportunity_type_id  in (1) then 1 else 0 end is_BO " +
		                    " , case when ot.opportunity_type_id  in (3) then 1 else 0 end is_oplus " +
		                    " , case when ot.opportunity_type_id  in (4,5) then 1 else 0 end is_0_100, " +
		                    " aii.id as indexs_info_id " +
		                    ", (select i.is_24_7 from  asset_index i where i.market_id = m.id) as is_24_7 " +
		                    ", sm.skin_id as indx_skin_id " +
	                    	" ,aii.additional_text, aii.ID, aii.market_id, aii.page_description, aii.page_keywords, aii.page_title, aii.skin_id "+
		                    //" , nvl(aii.MARKET_DESCRIPTION_PAGE,MARKET_DESCRIPTION2) as MARKET_DESCRIPTION_IN_PAGE " + // NVL not working with CLOB
		                    " , aii.MARKET_DESCRIPTION_PAGE MARKET_DESCRIPTION_IN_PAGE " +
		                 "FROM " +
		                    "markets m, " +
		                    "opportunity_templates ot, " +
		                    "skin_market_groups sm, " +
		                    "skin_market_group_markets smg " +
		                        "LEFT JOIN market_display_groups mdg ON smg.skin_display_group_id = mdg.id, " +
		                    "market_groups mg, " +
		                    "market_name_skin mns," +
		                    " asset_indexs_info aii  " +
		                 "WHERE " +
		                    "(ot.scheduled = 2 OR ot.opportunity_type_id = 3 OR ot.opportunity_type_id in (4,5)) AND " +
		                    "ot.is_active = 1 AND " +
		                    "m.id = ot.market_id AND " +
		                    "sm.id = smg.skin_market_group_id AND " +
		                    "sm.skin_id = ? AND " +
		                    "smg.market_id = m.id AND " +
		                    "mg.id = sm.market_group_id AND " +
		                    "mns.skin_id = sm.skin_id AND " +
		                    "mns.market_id = m.id AND " +
		                    "m.id= aii.market_id AND " +
		                    "sm.skin_id = aii.skin_id ) base_date" +
		                    ") order_data, asset_indexs_info ai WHERE   ai.market_id = order_data.marketid and ai.skin_id = order_data.skin_id  " +
		             
				    " ORDER BY " +
				    " case when order_data.market_group_id = 2 and order_data.opp_type = 1 then 1 " + 
					" when order_data.market_group_id = 4 and order_data.opp_type = 1 then 2 " +
					" when order_data.market_group_id = 5 and order_data.opp_type = 1 then 3 " +
					" when order_data.market_group_id in (3,6) and order_data.opp_type = 1 then 4 " +
					" when order_data.opp_type = 3 then 5 " +
					" when order_data.opp_type = 4 then 6 end, " +
				
				" case when order_data.country_groups = 6 and order_data.opp_type = 1 then -1 " +
					" when order_data.country_groups = 1 and order_data.opp_type = 1 then 1 " +
					" when order_data.country_groups = 2 and order_data.opp_type = 1 then 2 " +
					" when order_data.country_groups = 4 and order_data.opp_type = 1 then 3 " +
					" when order_data.country_groups = 3 and order_data.opp_type = 1 then 4 " +
					" when order_data.country_groups = 5 and order_data.opp_type = 1 then 5 " +
					" else 6 end , " +
			    " UPPER(market_name_by_skin) " ;

			ps = con.prepareStatement(sql);
            ps.setLong(1, skinId);
			rs = ps.executeQuery();
			
			long firtsOrder = 0L;
			long secondOrder = 0L;
			boolean isAddedOP = false;
			boolean isAddedZeroOneH = false;			
			
			while(rs.next()) {
				
				
				if(OpportunityType.PRODUCT_TYPE_BINARY == rs.getInt("OPP_TYPE")){
					if (firtsOrder != rs.getLong("indx_type")){
						AssetIndexMarket assetIndexTypeGroup = new AssetIndexMarket();
						assetIndexTypeGroup.setAssetIndexGroupType(AssetIndexGroupType.TYPE_GROUP.getId());
						assetIndexTypeGroup.setAssetIndexGroupTypeName(rs.getString("GROUP_NAME"));
						assetIndexTypeGroup.setGroupType(rs.getLong("indx_type"));
						list.add(assetIndexTypeGroup);
						firtsOrder = rs.getLong("indx_type");
					}
					
					if (secondOrder != rs.getLong("ORDERED_LIST_TWO")){
						AssetIndexMarket assetIndexCountryGroup = new AssetIndexMarket();
						assetIndexCountryGroup.setAssetIndexGroupType(AssetIndexGroupType.COUNTRY_GROUP.getId());
						assetIndexCountryGroup.setAssetIndexGroupTypeName(rs.getString("display_group_name_key"));	
						assetIndexCountryGroup.setGroupType(rs.getLong("indx_type"));
						list.add(assetIndexCountryGroup);
						secondOrder = rs.getLong("ORDERED_LIST_TWO");
					}					
				}
				
				if(OpportunityType.PRODUCT_TYPE_OPTION_PLUS == rs.getInt("OPP_TYPE") && !isAddedOP){
					AssetIndexMarket assetIndexOP = new AssetIndexMarket();
					assetIndexOP.setAssetIndexGroupType(AssetIndexGroupType.TYPE_GROUP.getId());
					assetIndexOP.setAssetIndexGroupTypeName("assetIndex.option.plus");
					assetIndexOP.setGroupType(rs.getLong("indx_type"));
					list.add(assetIndexOP);
					isAddedOP = true;
				}
				
				if(OpportunityType.PRODUCT_TYPE_BINARY_0_100 == rs.getInt("OPP_TYPE") && !isAddedZeroOneH){
					AssetIndexMarket assetIndexZOH = new AssetIndexMarket();
					assetIndexZOH.setAssetIndexGroupType(AssetIndexGroupType.TYPE_GROUP.getId());
					assetIndexZOH.setAssetIndexGroupTypeName("home.page.tab.4");
					assetIndexZOH.setGroupType(rs.getLong("indx_type"));
					list.add(assetIndexZOH);
					isAddedZeroOneH = true;
				}
				
				AssetIndexMarket assetIndex = new AssetIndexMarket();
				Market market = new Market();
				AssetIndexInfo assetIndexInfo = new AssetIndexInfo();
				
				market.setDisplayNameKey(MarketsManagerBase.getMarketName(skinId, rs.getLong("marketId")));
				market.setFeedName(getDisplayFeedName(rs.getString("market_feed_Name")));
				market.setId(rs.getLong("marketId"));
				market.setName(rs.getString("marketName"));
				market.setTimeCreated(convertToDate(rs.getTimestamp("MARKET_TIME_CREATED")));
				market.setOptionPlusMarketId(rs.getLong("option_plus_market_id"));								
				market.setHaveHourly(rs.getInt("hourly") == 0 ? false : true);
				market.setHaveDaily(rs.getInt("dayly") == 0 ? false : true);
				market.setHaveWeekly(rs.getInt("weekly") == 0 ? false : true);
				market.setHaveMonthly(rs.getInt("monthly") == 0 ? false : true);
				market.setTimeZone(rs.getString("time_zone"));
                market.setGroupId(rs.getLong("market_group_id"));
                market.setGroupName(rs.getString("group_name"));
                market.setDisplayGroupNameKey(rs.getString("display_group_name_key"));
                market.setGroupMarketsCounter(rs.getInt("group_markets_counter"));
                market.setOptionPlus(rs.getInt("option_plus_market_id") > 0 ? true : false);
                
            	//0100 markets has option_plus_market_id
            	if (market.getFeedName().indexOf("Binary0-100") > -1) {
            		market.setFeedName(market.getFeedName().substring(0, market.getFeedName().length() - 12));
            	} else if (market.isOptionPlus()) {
            		market.setFeedName(market.getFeedName().substring(0, market.getFeedName().length() - 8));
                }
                
                assetIndex.setMarket(market);
                
                assetIndexInfo.setId(rs.getLong("indexs_info_id"));
                assetIndexInfo.setMarketId(rs.getLong("marketId"));
                assetIndexInfo.setSkinId(skinId);
                assetIndexInfo.setMarketDescription(CommonUtil.clobToString(rs.getClob(("MARKET_DESCRIPTION"))));
                assetIndexInfo.setAdditionalText(rs.getString("ADDITIONAL_TEXT"));
                assetIndexInfo.setPageTitle(rs.getString("PAGE_TITLE"));
                assetIndexInfo.setPageKeyWords(rs.getString("PAGE_KEYWORDS"));
                assetIndexInfo.setPageDescription(rs.getString("PAGE_DESCRIPTION"));
                assetIndexInfo.setMarketDescriptionPage(rs.getString("MARKET_DESCRIPTION_IN_PAGE"));
                if(assetIndexInfo.getMarketDescriptionPage() == null) { // instead of NVL
                	assetIndexInfo.setMarketDescriptionPage(assetIndexInfo.getMarketDescription());
                }
                assetIndexInfo.setAssetIndexLevelCalc(getAssetIndexsLevelCalc(con, rs.getLong("indexs_info_id")));
                assetIndexInfo.setIs24h7(rs.getInt("is_24_7") == 0 ? false : true);
                assetIndexInfo.setOpportunityType(rs.getLong("OPP_TYPE"));
                
                assetIndex.setAssetIndexInfo(assetIndexInfo);
                assetIndex.setAssetIndexGroupType(AssetIndexGroupType.MARKET_GROUP.getId());
                assetIndex.setGroupType(rs.getLong("indx_type"));                
                
                list.add(assetIndex);                
			}
			
			setAssetIndexMap(groupAssetIndexMarket, list);

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		log.debug("End init AssetIndexMarkets");
		return list;
	}

	public static String getDisplayFeedName(String feedName) {
		String returnFeedName = feedName;
		if( feedName.equals("NQ") || feedName.equals("TRF30") || feedName.equals("FKLI") || feedName.equals(".KLSE") || feedName.equals("SPX")){
		  feedName += "c1";
		  returnFeedName = feedName;
		  }
		else if (feedName.equals("SI") || feedName.equals("GC") || feedName.equals("CL") || feedName.equals("HG") || feedName.equals("S") || feedName.equals("NG")) {
		    feedName += "v1";
		    returnFeedName = feedName;
		  }
		return returnFeedName;
	}

	private static void setAssetIndexMap( Map<Long, List<AssetIndexMarket>> groupAssetIndexMarket, ArrayList<AssetIndexMarket> list) {
		long currentGroup = 1L;
		int fromListIndex = 0;
		int toListIndex = 0;
		boolean isLastPartOfTheList = false;
		
		for (AssetIndexMarket assetIndexMap : list) {
			
			
			if((toListIndex + 1) == list.size()){
				isLastPartOfTheList = true;
				toListIndex ++;
			}
			
			
			if (currentGroup != assetIndexMap.getGroupType() || isLastPartOfTheList) {
				groupAssetIndexMarket.put(currentGroup, new ArrayList<AssetIndexMarket>(list.subList(fromListIndex, toListIndex)));
				fromListIndex = toListIndex ;
			}
			currentGroup = assetIndexMap.getGroupType();
			toListIndex++;
		}	
	}
	
	public static ArrayList<AssetIndexLevelCalc> getAssetIndexsLevelCalc(Connection con, long assetIndexInfoId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<AssetIndexLevelCalc> list = new ArrayList<AssetIndexLevelCalc>();
		try {
            String sql = " SELECT " +
		            	       " * " +
		            	    " FROM " +
		            	       " asset_indexs_level_calc " +
		            	     " WHERE " +
		            	       " info_id = ? " + 
		            	       " ORDER BY " +
		            	       " length(expiry_type) ";

			ps = con.prepareStatement(sql);
            ps.setLong(1, assetIndexInfoId);
			rs = ps.executeQuery();
			int cnt = 0;
			while(rs.next()) {
				AssetIndexLevelCalc assetIndexLevelCalc = new AssetIndexLevelCalc();				
				assetIndexLevelCalc.setId(rs.getLong("id"));
				assetIndexLevelCalc.setInfoId(assetIndexInfoId);
				assetIndexLevelCalc.setMarketId(rs.getLong("market_id"));
				assetIndexLevelCalc.setSkinId(rs.getLong("skin_id"));
				assetIndexLevelCalc.setExpiryTypeText(rs.getString("EXPIRY_TYPE"));
				assetIndexLevelCalc.setReutersFieldText(rs.getString("REUTERS_FIELD"));
				assetIndexLevelCalc.setExpiryFormulaText(rs.getString("EXPIRY_FORMULA"));
				assetIndexLevelCalc.setSameNextRow(false);
				list.add(assetIndexLevelCalc);
				
				if(cnt > 0 && list.get(0).getReutersFieldText().contains(assetIndexLevelCalc.getReutersFieldText())){					
					list.get(cnt -1 ).setSameNextRow(true);
					list.get(cnt).setSameNextRow(true);
				}				
				cnt ++;
			}			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
	
	public static ArrayList<AssetIndexsTempData> getAssetIndexsTempData(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<AssetIndexsTempData> list = new ArrayList<AssetIndexsTempData>();
		try {
            String sql = " SELECT " +
		            	       " ass.* , m.feed_name " +
		            	    " FROM " +
		            	       " asset_indexs_temp_data ass," +
		            	       " markets m " +
		            	     " WHERE " +
		            	       " ass.market_id=m.id ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while(rs.next()) {
				AssetIndexsTempData assetIndexTemp = new AssetIndexsTempData();
				
				assetIndexTemp.setMarketId(rs.getLong("MARKET_ID"));
				assetIndexTemp.setMarketName(rs.getString("MARKET_NAME"));
				assetIndexTemp.setMarketDescription(CommonUtil.clobToString(rs.getClob(("MARKET_DESCRIPTION"))));
				assetIndexTemp.setHourly(rs.getInt("HOURLY") == 0 ? false : true);
				assetIndexTemp.setEndOfDay(rs.getInt("END_OF_DAY") == 0 ? false : true);
				assetIndexTemp.setEndOfWeek(rs.getInt("END_OF_WEEK") == 0 ? false : true);
				assetIndexTemp.setEndOfMonth(rs.getInt("END_OF_MONTH") == 0 ? false : true);
				assetIndexTemp.setExpFormulaFirst(rs.getString("EXPIRY_FORMULA_FIRST"));
				assetIndexTemp.setExpFormulaSecond(rs.getString("EXPIRY_FORMULA_SECOND"));
				assetIndexTemp.setAdditionalText(rs.getString("ADDITIONAL_TEXT"));
				assetIndexTemp.setLastValueHourly(rs.getInt("LAST_VALUE_HOURLY") == 0 ? false : true);
				assetIndexTemp.setLastValueEndOfDay(rs.getInt("LAST_VALUE_END_DAY") == 0 ? false : true);
				assetIndexTemp.setLastValueEndOfWeek(rs.getInt("LAST_VALUE_END_WEEK") == 0 ? false : true);
				assetIndexTemp.setLastValueEndOfMonth(rs.getInt("LAST_VALUE_END_MONTH") == 0 ? false : true);
				assetIndexTemp.setClosingLevelHourly(rs.getInt("CLOSING_LEVEL_HOURLY") == 0 ? false : true);
				assetIndexTemp.setClosingLevelEndOfDay(rs.getInt("CLOSING_LEVEL_END_DAY") == 0 ? false : true);
				assetIndexTemp.setClosingLevelEndOfWeek(rs.getInt("CLOSING_LEVEL_END_WEEK") == 0 ? false : true);
				assetIndexTemp.setClosingLevelEndOfMonth(rs.getInt("CLOSING_LEVEL_END_MONTH") == 0 ? false : true);
				assetIndexTemp.setReutersFieald(rs.getString("REUTERS_FIELD"));
				assetIndexTemp.setMarketFeedName(rs.getString("FEED_NAME"));
	
				list.add(assetIndexTemp);
			}			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
	
	public static void insertAssetIndexsMarketInfoTemp(Connection conn, List<AssetIndexInfo> assetIndexInfoList) throws SQLException {
		
		for (AssetIndexInfo info : assetIndexInfoList) {
			insertAssetIndexsMarketInfoTemp(conn, info);
		}
	}
	
	
	public static void insertAssetIndexsMarketInfoTemp(Connection conn, AssetIndexInfo info) throws SQLException {
		PreparedStatement ps = null;
		int res;
			

		try {
            	
				log.debug("Begin Insert MARKET:" + info.getMarketId() +  "and skin:" + info.getSkinId());
				String sql =
		            	"INSERT INTO " +
		            			" asset_indexs_info (id, market_id, skin_id, market_description, additional_text, page_title, page_keywords, page_description, market_description_page) " +
		            	" VALUES " +
		            			" (seq_asset_indexs_info.nextval, ?, ?, ?, ?, ?, ?, ?, ?) ";
	            
				ps = conn.prepareStatement(sql);
	            ps.setLong(1, info.getMarketId());
	            ps.setLong(2, info.getSkinId());
	            ps.setCharacterStream(3,  new StringReader(info.getMarketDescription()), info.getMarketDescription().length());
	            ps.setString(4, info.getAdditionalText());
	            ps.setString(5, info.getPageTitle());
	            ps.setString(6, info.getPageKeyWords());
	            ps.setString(7, info.getPageDescription());
	            ps.setString(8, info.getMarketDescriptionPage());
	            res = ps.executeUpdate();
	            closeStatement(ps);
	            
	            long seqId = getSeqCurValue(conn,"seq_asset_indexs_info");
	            	for (AssetIndexLevelCalc calc : info.getAssetIndexLevelCalc()){
	    				String sqlCalc =
	    		            	"INSERT INTO " +
	    		            			" asset_indexs_level_calc (id, info_id, market_id, skin_id, expiry_type, reuters_field, expiry_formula) " +
	    		            	" VALUES " +
	    		            			" (seq_asset_indexs_level_calc.nextval," + seqId + ", ?, ?, ?, ?, ? ) ";
	    				ps = conn.prepareStatement(sqlCalc);
	    	            ps.setLong(1, calc.getMarketId());
	    	            ps.setLong(2, calc.getSkinId());
	    	            ps.setString(3, calc.getExpiryTypeText());
	    	            ps.setString(4, calc.getReutersFieldText());
	    	            ps.setString(5, calc.getExpiryFormulaText());
	    				res = ps.executeUpdate();
	    				closeStatement(ps);
	            	}	            	
			
        } finally {
            closeStatement(ps);            
        }
	}
	
	  public static HashMap<Long, AssetIndexBase> getAssetyByMarketHM(Connection con, long skinId) throws SQLException {
		  log.debug("Begin init AssetIndexMarketsJson for SkinID:" + skinId);
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  AssetIndexBase vo = null;
		  HashMap<Long, AssetIndexBase> hm = new HashMap<Long, AssetIndexBase>();
		  try {
	        String sql =
	        				"SELECT " +
	        							"inf.market_description, oldsql.* " +
	        				" FROM " +
	        						" ( " +
					        			"SELECT " +
												" distinct ai.*, sm.market_group_id, ot.time_zone, m.option_plus_market_id, m.exchange_id, " +
												" case when m.id =15 then 'ILS=' else m.feed_Name end as feed_Name, " +
												//" inf.market_description, " +
												" lev.reuters_field, " +
												" lev.expiry_formula " +
					                     "FROM " +
												"asset_index ai, " +
												"skin_market_groups sm, " +
												"skin_market_group_markets smg, " +
												"markets m, " +
												"opportunity_templates ot, " +
												//"asset_indexs_info inf," +
												" (select * " +
												" from asset_indexs_level_calc " + 
												" where skin_id = " + skinId +
												" and expiry_type_id = 1 " +
												" union all " + 
												" select * " + 
												" from asset_indexs_level_calc " + 
												" where skin_id = " + skinId +
												" and market_id not in(select market_id " + 
												" from asset_indexs_level_calc " + 
												" where skin_id = " + skinId +
												" and expiry_type_id = 1)) lev " +
					                     "WHERE " +
												" sm.id = smg.skin_market_group_id AND " +
												" ot.is_active = 1 AND " +
												" sm.skin_id = " + skinId + " AND " +
												" smg.market_id = ai.market_id AND " +
												" m.id = ai.market_id AND " +
												" m.id = ot.market_id AND" +
												//" inf.market_id = m.id AND " +
												//" inf.skin_id = sm.skin_id AND " +
												" lev.market_id = m.id AND " +
												" lev.skin_id = sm.skin_id  " +
		                            ") oldsql, asset_indexs_info inf " +
	                         " WHERE " +
		                            " inf.market_id = oldsql.market_ID AND" + 
		                            " inf.skin_id = " + skinId ;

	        ps = con.prepareStatement(sql);
	        rs = ps.executeQuery();

	        while (rs.next()) {
	        	vo = new AssetIndexBase();
	        	vo.setId(rs.getLong("market_id"));
	        	vo.setTradingDays(rs.getString("trading_days"));
	        	if(null != rs.getString("friday_time")) {
	        		vo.setFridayTime(rs.getString("friday_time"));
	            }
	            vo.setStartTime(rs.getString("start_time"));
	            vo.setEndTime(rs.getString("end_time"));
	            vo.setMarketGroupId(rs.getLong("market_group_id"));
	            vo.setFeedName(rs.getString("feed_name"));
	            vo.setTimeZoneString(rs.getString("time_zone"));
	            vo.setOptionPlusMarketId(rs.getLong("option_plus_market_id"));
	            vo.setExchangeId(rs.getLong("exchange_id"));
	            
	            vo.setReutersField(rs.getString("reuters_field"));
	            vo.setExpiryFormula(rs.getString("expiry_formula"));
	            vo.setDescription(CommonUtil.clobToString(rs.getClob(("market_description"))));
	            
	            hm.put(vo.getId(), vo);
	        }
		  } finally {
	          closeResultSet(rs);
	          closeStatement(ps);
		  }

		return hm;
	  }
}
