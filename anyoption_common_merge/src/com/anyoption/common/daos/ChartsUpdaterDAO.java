package com.anyoption.common.daos;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import com.anyoption.common.beans.ChartsUpdaterMarket;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.enums.SkinGroup;

public class ChartsUpdaterDAO extends DAOBase {
    public static ArrayList<ChartsUpdaterMarket> getChartsUpdaterMarkets(Connection conn, long opportunityTypeId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<ChartsUpdaterMarket> l = new ArrayList<ChartsUpdaterMarket>();
        try {
            String sql =
            		" SELECT " +
            			" x.*, " +
            	        " TO_CHAR(CASE WHEN x.open_opp_est_closing IS NOT NULL " +
            	                 " THEN x.open_opp_est_closing " +
            	                 " ELSE x.open_opp_est_closing " +
            	                 " END, " +
            	                 " 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') open_opp_est_closing_str, " +
            	        " TO_CHAR(CASE WHEN x.time_first_invest IS NOT NULL " +
            	                 " THEN x.time_first_invest " +
            	                 " ELSE x.time_est_closing " +
            	                 " END, " + 
            	                 " 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') last_hour_opening_time " +
            	    " FROM (SELECT m.id, " +
            	               " m.display_name, " +
            	               " m.decimal_point, " +
            	               " op1.min_time_first_invest time_first_invest, " +
            	               " op1.max_time_est_closing time_est_closing, " +
            	               " op1.open_opp_est_closing, " +
            	               " CASE WHEN op1.open_opp_est_closing IS NOT NULL " +
            	               " THEN 1 " +
            	               " ELSE  0 " +
            	               " END opened, " +
            	               " CASE WHEN t1.has_hourly > 0 " +
            	               " THEN 1 " +
            	               " ELSE 0 " +
            	               " END hourly, " +
            	               " op1.over_odds_win * 100 odds_win " +
            	            " FROM markets m " +
            	            " JOIN (SELECT market_id, " +
            	                      " COUNT(CASE WHEN scheduled = 1 " +
            	                              " THEN 1 " +
            	                              " END) has_hourly " +
            	                 " FROM opportunity_templates " +
            	                 " WHERE " +
            	                 	" opportunity_type_id = ? " +
            	                 	" AND is_active = 1 " +
            	                 " GROUP BY market_id) t1 " +
            	                 " ON t1.market_id = m.id " +
            	                 " LEFT JOIN (SELECT " +
            	                 		   		" o.market_id, " +
            	                 		   		" ot.over_odds_win, " +
            	                 		   		" MIN(CASE WHEN o.scheduled = 1 " +
            	                 		   			" THEN o.time_first_invest " +
            	                 		   		 	" END) min_time_first_invest, " +
            	                 		   		" MAX(CASE WHEN o.opportunity_type_id = ? AND SYS_EXTRACT_UTC(o.time_act_closing) > SYSDATE - 7 " +
            	                 		   			" THEN o.time_est_closing " +
            	                 		   			" END) max_time_est_closing, " +
            	                 		   		" MIN(CASE WHEN SYS_EXTRACT_UTC(o.time_est_closing) >= TRUNC(SYSDATE) - 1 AND o.opportunity_type_id = ? " +
            	                 		   			" THEN o.time_est_closing " +
            	                 		   			" END) open_opp_est_closing " +
            	                 		   	" FROM opportunities o " +
            	                 		   	" JOIN opportunity_odds_types ot " +
            	                 		   	" ON ot.id = o.odds_type_id " +
            	                 		   	" WHERE o.is_published = 1 " +
            	                 		   	" GROUP BY o.market_id, ot.over_odds_win) op1 " +
            	                 " ON op1.market_id = m.id) x ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, opportunityTypeId);
            pstmt.setLong(2, opportunityTypeId);
            pstmt.setLong(3, opportunityTypeId);
            rs = pstmt.executeQuery();
            ChartsUpdaterMarket m = null;
            while(rs.next()) {
            	m = new ChartsUpdaterMarket();
        		m.setId(rs.getLong("id"));
        		m.setDecimalPoint(new Long(rs.getLong("decimal_Point")));
        		m.setDisplayNameKey(rs.getString("display_name"));
                m.setLastHourClosingTime(getTimeWithTimezone(rs.getString("last_hour_opening_time")));
                m.setOpened(rs.getBoolean("opened"));
                m.setHaveHourly(rs.getBoolean("hourly"));
                m.setOddsWin(rs.getInt("odds_win"));               
                m.setOpenOppEstClosing(getTimeWithTimezone(rs.getString("open_opp_est_closing_str")));                
                l.add(m);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

    /**
     * Load market rates.
     *
     * @param conn
     * @param marketId the market for which to load the rates for
     * @param marketDecimalPoint decimal point digit rounding for the corresponding market
     * @param after if <code>null</code> load all. else load rates after that time
     * @return <code>ArrayList<MarketRate></code>
     * @throws SQLException
     */
    public static Map<SkinGroup, List<MarketRate>> getMarketRates(
			    										Connection conn,
			    										long marketId,
			    										int marketDecimalPoint,
			    										Date after) throws SQLException {
        Map<SkinGroup, List<MarketRate>> ratesMap = new EnumMap<SkinGroup, List<MarketRate>>(SkinGroup.class);
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "market_rates " +
                    "WHERE " +
                        "market_id = ? AND " +
                        (null != after ? "rate_time > ? AND " : "") +
//                        "rate_time <= current_date AND " + // "rate_time <= current_date - 5 / 1440 AND " + (5 min b4)
                        "rate > 0 " +
                    "ORDER BY " +
                        "rate_time";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            if (null != after) {
                pstmt.setTimestamp(2, new Timestamp(after.getTime()));
            }
            rs = pstmt.executeQuery();
            MarketRate mr = null;
            while (rs.next()) {
                mr = new MarketRate();
                mr.setId(rs.getLong("id"));
                mr.setMarketId(marketId);
                mr.setRate(rs.getBigDecimal("rate").setScale(marketDecimalPoint, BigDecimal.ROUND_HALF_UP));
                mr.setRateTime(new Date(rs.getTimestamp("rate_time").getTime()));
                mr.setGraphRate(rs.getBigDecimal("graph_rate"));
                mr.setDayRate(rs.getBigDecimal("day_rate"));
                mr.setSkinGroup(SkinGroup.getById(rs.getLong("SKIN_GROUP_ID")));
                mr.setLast(rs.getBigDecimal("last"));
                mr.setAsk(rs.getBigDecimal("ask"));
                mr.setBid(rs.getBigDecimal("bid"));
                
                List<MarketRate> rates = ratesMap.get(mr.getSkinGroup());
                if (rates == null) {
                	rates = new ArrayList<MarketRate>();
                	ratesMap.put(mr.getSkinGroup(), rates);
                }
                rates.add(mr);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return ratesMap;
    }
}