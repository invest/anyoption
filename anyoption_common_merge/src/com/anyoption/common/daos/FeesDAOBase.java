package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class FeesDAOBase extends DAOBase {

	public static Map<Long, Long> getMaintenanceFeeMap(Connection conn, long feeId)
		throws SQLException {
		Map<Long, Long> maintenanceFee = new HashMap<Long, Long>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT currency_id, amount FROM FEE_CURRENCY_MAP where fee_id = ?";
			ps = conn.prepareStatement(sql);
			ps.setLong(1, feeId);
			rs = ps.executeQuery();
			while (rs.next()) {
				maintenanceFee.put(rs.getLong("currency_id"), rs.getLong("amount"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return maintenanceFee;
	}
	
	public static Map<Long, Map<Long, Long>> getAllFeesMap(Connection conn) throws SQLException {
		Map<Long, Map<Long, Long>> allFeesMap = new HashMap<Long, Map<Long,Long>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT fee_id, currency_id, amount FROM FEE_CURRENCY_MAP";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				long feeId = rs.getLong("fee_id");
				insertFeeCurrencyAmountMap(allFeesMap, feeId, rs.getLong("currency_id"), rs.getLong("amount"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return allFeesMap;
	}
	
	public static void insertFeeCurrencyAmountMap(Map<Long, Map<Long, Long>> allFeesMap, long feeId, long currencyId, long amount){
		if (!allFeesMap.containsKey(feeId)) {
			HashMap<Long, Long> hm = new HashMap<Long, Long>();
    		hm.put(currencyId, amount);
    		allFeesMap.put(feeId, hm);
		} else {
    		Map<Long, Long> hm = allFeesMap.get(feeId);
    		hm.put(currencyId, amount);
    	}
	}
}