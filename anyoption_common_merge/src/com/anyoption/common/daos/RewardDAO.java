package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.anyoption.common.beans.RewardTier;
import com.anyoption.common.beans.RewardUser;
import com.anyoption.common.beans.RewardUserBenefit;

/**
 * @author liors
 *
 */
public class RewardDAO extends DAOBase {
	
	public static HashMap<Integer, RewardTier> getTiers(Connection connection) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		HashMap<Integer, RewardTier> rewardTiers = new HashMap<Integer, RewardTier>();
		try {
			String sql = 
					"SELECT" +
					"	*" +
					"FROM" +
					"	rwd_tiers";
			pstmt = connection.prepareStatement(sql);
			resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				RewardTier rewardTier = new RewardTier();
				rewardTier = getRewardTierValueObject(resultSet);
				rewardTiers.put(rewardTier.getId(), rewardTier);
			}
		} finally {
			closeStatement(pstmt);
		}
		return rewardTiers;
	}
	
	/**
	 * get Minimum Experience Points by given tier id. 
	 * 
	 * @param connection
	 * @param tierId
	 * @return  Minimum Experience Points (if there isn't => return 0)
	 * @throws SQLException
	 */
	public static int getMinExperiencePoints(Connection connection, int tierId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int minExperiencePoints = 0;
		try {
			String sql = 
					"SELECT" +
					"	min_exp_points " +
					"FROM" +
					"	rwd_tiers " +
					"WHERE" +
					"	id = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, tierId);
			resultSet = pstmt.executeQuery();
			if (resultSet.next()) {
				minExperiencePoints = resultSet.getInt("min_exp_points");	
			}
		} finally {
			closeStatement(pstmt);
		}
		return minExperiencePoints;
	}
	
	/**
	 * Get tier id by experience points 
	 * @param connection
	 * @param experiencePoints
	 * @return
	 * @throws SQLException
	 */
	public static int getTierIdByExperiencePoints(Connection connection, double experiencePoints) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int tierId = 0;
		try {
			String sql =" SELECT " +
						"	max(rt.id) as tier_id " +
						" FROM " +
						"	rwd_tiers rt " +
						" WHERE " +
						"	rt.min_exp_points <= ? ";
			pstmt = connection.prepareStatement(sql);
			pstmt.setDouble(1, experiencePoints);
			resultSet = pstmt.executeQuery();
			if (resultSet.next()) {
				tierId = resultSet.getInt("tier_id");	
			}
		} finally {
			closeStatement(pstmt);
		}
		return tierId;		
	}
			
	/**
	 * Check whether the customer has this benefit
	 * @param connection
	 * @param rwdBenefitId
	 * @param userId
	 * @return true if the customer has this benefit
	 * @throws SQLException
	 */
	public static boolean isHasBenefit(Connection connection, String rwdBenefitsId, long userId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int index = 1;
		try {
			String sql = "SELECT " +
							" rub.id " +
						" FROM " +
							" RWD_USER_BENEFITS rub " +
						" WHERE " +
							" rub.rwd_benefit_id in ( " + rwdBenefitsId + " ) " +
							" AND rub.is_active = ? " +
							" AND rub.user_id = ? ";
			pstmt = connection.prepareStatement(sql);
			pstmt.setLong(index++, RewardUserBenefit.REWARD_USER_BENEFIT_ACTIVE);
			pstmt.setLong(index++, userId);
			resultSet = pstmt.executeQuery();
			if (resultSet.next()) {
				return true;
			}
		} finally {
			closeStatement(pstmt);
		}
		return false;
	}
		
	/**
	 * Get Reward Users After Tier Change
	 * @param connection
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardUser> getRewardUsersAfterTierChange(Connection connection) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ArrayList<RewardUser> list = new ArrayList<RewardUser>(); 
		try {
			String sql =" SELECT " +
						"	ru.* " +
						" FROM " +
						"	rwd_users ru " +
						" WHERE " +
						"	ru.rwd_tier_id > 0 " +
						"   AND (ru.RWD_TIER_ID <> " +
						"					       (SELECT " +
						"				               MAX(rt.id) AS tier_id " +
						"				            FROM " +
						"				              rwd_tiers rt " +
						"				            WHERE " +
						"				               rt.min_exp_points <= ru.exp_points " +
						"							) " +
						"	) ";
			pstmt = connection.prepareStatement(sql);
			resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				RewardUser rewardUser = new RewardUser();
				rewardUser.setId(resultSet.getInt("id"));
				rewardUser.setUserId(resultSet.getLong("user_id"));
				rewardUser.setTierId(resultSet.getInt("rwd_tier_id"));
				rewardUser.setExperiencePoints(resultSet.getDouble("exp_points"));
				list.add(rewardUser);
			}
		} finally {
			closeStatement(pstmt);
		}
		return list;
	}
	
	/**
	 * Get Reward User After Tier Change if null there is no need to change tier for this user
	 * @param connection
	 * @return
	 * @throws SQLException
	 */
	public static RewardUser getRewardUserAfterTierChange(Connection connection, long userId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		RewardUser rewardUser = null;
		try {
			String sql =" SELECT " +
						"	ru.* " +
						" FROM " +
						"	rwd_users ru " +
						" WHERE " +
						"	ru.rwd_tier_id > 0 " +
						"   AND (ru.RWD_TIER_ID <> " +
						"					       (SELECT " +
						"				               MAX(rt.id) AS tier_id " +
						"				            FROM " +
						"				              rwd_tiers rt " +
						"				            WHERE " +
						"				               rt.min_exp_points <= ru.exp_points " +
						"							) " +
						"	) " +
						"	AND ru.user_id = ? ";
			pstmt = connection.prepareStatement(sql);			
			pstmt.setLong(1, userId);			
			resultSet = pstmt.executeQuery();
			if (resultSet.next()) {
				rewardUser = new RewardUser();
				rewardUser.setId(resultSet.getInt("id"));
				rewardUser.setUserId(resultSet.getLong("user_id"));
				rewardUser.setTierId(resultSet.getInt("rwd_tier_id"));
				rewardUser.setExperiencePoints(resultSet.getDouble("exp_points"));				
			}
		} finally {
			closeStatement(pstmt);
		}
		return rewardUser;
	}
	
	public static RewardTier getRewardTierValueObject(ResultSet resultSet) throws SQLException {
		RewardTier valueObject = new RewardTier();
		valueObject.setId(resultSet.getInt("id"));
		valueObject.setName(resultSet.getString("name"));
		valueObject.setMinExperiencePoints(resultSet.getInt("min_exp_points"));
		valueObject.setInActiveDays(resultSet.getInt("in_active_days"));
		valueObject.setExperiencePointsToReduce(resultSet.getInt("exp_points_to_reduce"));
		valueObject.setDaysToDemote(resultSet.getInt("days_to_demote"));
		return valueObject;
	}
	
}
