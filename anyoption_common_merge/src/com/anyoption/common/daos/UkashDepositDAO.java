package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.bl_vos.UkashDeposit;

public class UkashDepositDAO extends DAOBase{

	/**
	 * Insert Ukash Deposit
	 * @param con
	 * @param vo
	 * @throws SQLException
	 */
	  public static void insert(Connection con,UkashDeposit vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			  String sql = "INSERT into " +
			  			   		"ukash_deposit(id, transaction_id, voucher_number, voucher_value, voucher_currency_id, time_created, status) " +
			  			   "VALUES " +
			  			   		"(seq_ukash_deposit.nextval,?,?,?,?,sysdate,?)";

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, vo.getTransactionId());
			  ps.setString(2, vo.getVoucherNumber());
			  ps.setLong(3, vo.getVoucherValueLong());
			  ps.setLong(4, vo.getVoucherCurrencyId());
			  ps.setInt(5, vo.getStatus());
			  ps.executeUpdate();
			  vo.setId(getSeqCurValue(con,"seq_ukash_deposit"));
		  }
		  finally
		  {
			  closeStatement(ps);
			  closeResultSet(rs);
		  }
	  }


	  public static UkashDeposit get(Connection con,long id) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  UkashDeposit ukashDeposit = null;

		  try {
			  String sql = "SELECT " +
			  			   		"ud.*, u.currency_id userCurrency, t.amount " +
			  			   "FROM " +
			  			   		"ukash_deposit ud, transactions t, users u " +
			  			   "WHERE " +
			  			   		"u.id = t.user_id " +
			  			   		"AND t.id = ud.transaction_id " +
			  			   		"AND transaction_id = ?";
			  ps = con.prepareStatement(sql);
			  ps.setLong(1, id);
			  rs=ps.executeQuery();
			  if (rs.next()) {
				  ukashDeposit = getVO(rs);
			  }
		  }
		  finally
		  {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return ukashDeposit;
	  }

	  private static UkashDeposit getVO(ResultSet rs) throws SQLException{
		  UkashDeposit vo = new UkashDeposit();

		  vo.setId(rs.getLong("id"));
		  vo.setTransactionId(rs.getLong("transaction_id"));
		  vo.setVoucherNumber(rs.getString("voucher_number"));
		  vo.setVoucherValue(rs.getString("voucher_value"));
		  vo.setConvertedAmount(rs.getLong("amount"));
		  vo.setUserCurrency(rs.getString("userCurrency"));
		  vo.setStatus(rs.getInt("status"));
		  vo.setVoucherCurrencyId(rs.getLong("voucher_currency_id"));

		  return vo;
	  }
}

