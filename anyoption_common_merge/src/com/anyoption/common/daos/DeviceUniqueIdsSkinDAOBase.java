package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.beans.DeviceUniqueIdSkin;
import com.anyoption.common.daos.DAOBase;

public class DeviceUniqueIdsSkinDAOBase extends DAOBase {
    

    public static DeviceUniqueIdSkin getByDeviceId(Connection con, String deviceId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        DeviceUniqueIdSkin record = null;
        try {
            String sql =
            	"SELECT " +
            		"id, " +
            		"skin_id, " +
            		"combination_id, " +
            		"app_version, " +
            		"c2dm_registration_id, " +
                    "dynamic_param " +
            	"FROM " +
            		"device_unique_ids_skin " +
            	"WHERE " +
            		"device_unique_id = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, deviceId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	record = new DeviceUniqueIdSkin();
            	record.setId(rs.getLong("id"));
            	record.setSkinId(rs.getLong("skin_id"));
            	record.setCombId(rs.getLong("combination_id"));
            	record.setAppVer(rs.getString("app_version"));
            	record.setC2dmRegistrationId(rs.getString("c2dm_registration_id"));
                record.setDynamicParam(rs.getString("dynamic_param"));
            }
        } finally {
        	closeResultSet(rs);
        	closeStatement(ps);
        }
        return record;
    }
}