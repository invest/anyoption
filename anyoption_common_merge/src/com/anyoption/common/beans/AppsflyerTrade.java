package com.anyoption.common.beans;

public class AppsflyerTrade {
	
	private AppsflyerEvent appsflyerEvent;
	private long userId;
	public AppsflyerEvent getAppsflyerEvent() {
		return appsflyerEvent;
	}
	public void setAppsflyerEvent(AppsflyerEvent appsflyerEvent) {
		this.appsflyerEvent = appsflyerEvent;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	} 

}
