/**
 * 
 */
package com.anyoption.common.beans;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.annotations.AnyoptionNoJSON;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.managers.WritersManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.Sha1;

/**
 * @author kirilim
 *
 */
public class User extends com.anyoption.common.beans.base.User {

	private static final long serialVersionUID = 4999582024225108606L;
	private static final Logger log = Logger.getLogger(User.class);
	@AnyoptionNoJSON
	protected String isActive;
	@AnyoptionNoJSON
	protected Date lastFailedTime;
	@AnyoptionNoJSON
	protected int failedCount;
	@AnyoptionNoJSON
	protected boolean docsRequired;   // if true - a trigger for lock account job
	@AnyoptionNoJSON
	protected long statusId;
	@AnyoptionNoJSON
	private RewardUser rewardUser;
	@AnyoptionNoJSON
	private UserActiveData userActiveData;
	@AnyoptionNoJSON
	protected Country country;
//    @AnyoptionNoJSON
//    protected Date timeLastLogin;
    //use to comper last login date with now to know if to show banner
    @AnyoptionNoJSON
    protected Date timeLastLoginFromDb;
    @AnyoptionNoJSON
    protected Date timeModified;
    @AnyoptionNoJSON
    protected String comments;
    @AnyoptionNoJSON
    protected String ip;
    @AnyoptionNoJSON
    protected long limitId;
    @AnyoptionNoJSON
    protected String companyName;
    @AnyoptionNoJSON
    protected BigDecimal companyId;
    @AnyoptionNoJSON
    protected boolean taxExemption;
    @AnyoptionNoJSON
    protected BigDecimal referenceId;
    @AnyoptionNoJSON
    protected long languageId;
    @AnyoptionNoJSON
    protected String utcOffsetModified;
    @AnyoptionNoJSON
    protected String utcOffsetCreated;
    @AnyoptionNoJSON
    protected boolean idDocVerify;    // if true - user send us documents
    @AnyoptionNoJSON
    protected boolean isFalseAccount;
    @AnyoptionNoJSON
    protected long writerId;
    @AnyoptionNoJSON
    protected String skinName;
    @AnyoptionNoJSON
    protected boolean isContactForDaa;
    @AnyoptionNoJSON
    protected long totalWinLose;
    @AnyoptionNoJSON
    protected long subAffiliateKey;
    
    @AnyoptionNoJSON
    protected boolean isETRegulation;
    @AnyoptionNoJSON
    protected boolean isNonRegSuspend;
	
    
    public boolean isDocsRequired() {
        return docsRequired;
    }

    public void setDocsRequired(boolean docsRequired) {
        this.docsRequired = docsRequired;
    }

    public int getFailedCount() {
        return failedCount;
    }

    public void setFailedCount(int failedCount) {
        this.failedCount = failedCount;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

	public boolean isActive() {
		if (isActive == null) {
			return false;
		}
		if (isActive.equals("1")) {
			return true;
		} else {
			return false;
		}
	}

    public Date getLastFailedTime() {
        return lastFailedTime;
    }

    public void setLastFailedTime(Date lastFailedTime) {
        this.lastFailedTime = lastFailedTime;
    }

    /**
	 * @return the statusId
	 */
	public long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	public RewardUser getRewardUser() {
		return rewardUser;
	}

	public void setRewardUser(RewardUser rewardUser) {
		this.rewardUser = rewardUser;
	}

	/**
	 * @return the userActiveData
	 */
	public UserActiveData getUserActiveData() {
		return userActiveData;
	}

	/**
	 * @param userActiveData the userActiveData to set
	 */
	public void setUserActiveData(UserActiveData userActiveData) {
		this.userActiveData = userActiveData;
	}

	/**
	 * @return true if the user is not restricted from seen the bonus tab.
	 */
	public boolean isBonusTabDisplayAllow() {
		if (isBonusTabDisplayAllow == null) {
			isBonusTabDisplayAllow = true;
			try {
				if (this.skinId == Skin.SKIN_ETRADER) {
					isBonusTabDisplayAllow = !CommonUtil.isBonuseUserRestricted(this) && UsersManagerBase.isUserEngagedBonus(this.id);
				}
			} catch (Exception e) {
				log.error(e);
			}
		}
		return isBonusTabDisplayAllow;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
	public boolean isETRegulation() {
		return isETRegulation;
	}

	public void setETRegulation(boolean isETRegulation) {
		this.isETRegulation = isETRegulation;
	}
//
//	public Date getTimeLastLogin() {
//		return timeLastLogin;
//	}
//
//	public void setTimeLastLogin(Date timeLastLogin) {
//		this.timeLastLogin = timeLastLogin;
//	}

	public Date getTimeLastLoginFromDb() {
		return timeLastLoginFromDb;
	}

	public void setTimeLastLoginFromDb(Date timeLastLoginFromDb) {
		this.timeLastLoginFromDb = timeLastLoginFromDb;
	}

	public Date getTimeModified() {
		return timeModified;
	}

	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public long getLimitId() {
		return limitId;
	}

	public void setLimitId(long limitId) {
		this.limitId = limitId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public BigDecimal getCompanyId() {
		return companyId;
	}

	public void setCompanyId(BigDecimal companyId) {
		this.companyId = companyId;
	}

	public boolean isTaxExemption() {
		return taxExemption;
	}

	public void setTaxExemption(boolean taxExemption) {
		this.taxExemption = taxExemption;
	}

	public BigDecimal getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(BigDecimal referenceId) {
		this.referenceId = referenceId;
	}

	public long getLanguageId() {
		return languageId;
	}

	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	public String getUtcOffsetModified() {
		return utcOffsetModified;
	}

	public void setUtcOffsetModified(String utcOffsetModified) {
		this.utcOffsetModified = utcOffsetModified;
	}

	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	public boolean isIdDocVerify() {
		return idDocVerify;
	}

	public void setIdDocVerify(boolean idDocVerify) {
		this.idDocVerify = idDocVerify;
	}

	public boolean isFalseAccount() {
		return isFalseAccount;
	}

	public void setFalseAccount(boolean isFalseAccount) {
		this.isFalseAccount = isFalseAccount;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public String getSkinName() {
		return skinName;
	}

	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}

	public boolean isContactForDaa() {
		return isContactForDaa;
	}

	public void setContactForDaa(boolean isContactForDaa) {
		this.isContactForDaa = isContactForDaa;
	}

	public long getTotalWinLose() {
		return totalWinLose;
	}

	public void setTotalWinLose(long totalWinLose) {
		this.totalWinLose = totalWinLose;
	}
	
	public String bubblesSignture(String token, long webSkinId) {
		try {
			String s = CommonJSONService.concatBubblesRequestParams(id, webSkinId, CommonUtil.getUtcOffsetInMin(utcOffset), token) + CommonJSONService.BUBBLES_REQUEST_PARAMETER_SIGNATURE_SEPARATOR + WritersManagerBase.getWriterPassword(Writer.WRITER_ID_BUBBLES_WEB);
			return Sha1.encode(s);
		} catch (Exception e) {
			log.debug("Can't get bubbles signture", e);
		}
		return null;
	}

	/**
	 * @return the subAffiliateKey
	 */
	public long getSubAffiliateKey() {
		return subAffiliateKey;
	}

	/**
	 * @param subAffiliateKey the subAffiliateKey to set
	 */
	public void setSubAffiliateKey(long subAffiliateKey) {
		this.subAffiliateKey = subAffiliateKey;
	}

	public boolean isNonRegSuspend() {
		return isNonRegSuspend;
	}

	public void setNonRegSuspend(boolean isNonRegSuspend) {
		this.isNonRegSuspend = isNonRegSuspend;
	}
	
} 