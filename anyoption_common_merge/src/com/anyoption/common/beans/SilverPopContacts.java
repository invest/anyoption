package com.anyoption.common.beans;

public class SilverPopContacts implements java.io.Serializable {
    private static final long serialVersionUID = -5405154423494882647L;
    private long contactId;
    private long isContactByEmail;
    /**
     * @return the contactId
     */
    public long getContactId() {
        return contactId;
    }
    /**
     * @param contactId the contactId to set
     */
    public void setContactId(long contactId) {
        this.contactId = contactId;
    }
    /**
     * @return the isContactByEmail
     */
    public long getIsContactByEmail() {
        return isContactByEmail;
    }
    /**
     * @param isContactByEmail the isContactByEmail to set
     */
    public void setIsContactByEmail(long isContactByEmail) {
        this.isContactByEmail = isContactByEmail;
    }
}


