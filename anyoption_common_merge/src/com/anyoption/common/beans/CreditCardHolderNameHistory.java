/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author pavel.tabakov
 *
 */
public class CreditCardHolderNameHistory implements Serializable {

	private static final long serialVersionUID = -514876724748113145L;
	private String holderName;
	private Date dateUpdated;
	private long operationId;

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public long getOperationId() {
		return operationId;
	}

	public void setOperationId(long operationId) {
		this.operationId = operationId;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "CreditCardHolderNameHistory" + ls
				  + "holderName: " + holderName + ls
				  + "dateUpdated: "+ dateUpdated + ls
				  + "operationId: " + operationId + ls;
	}

}
