/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;

/**
 * @author pavelt
 *
 */
public class TournamentType implements Serializable {

	private static final long serialVersionUID = -6951264560974954367L;

	private long id;
	private String name;
	private String proccessClass;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProccessClass() {
		return proccessClass;
	}

	public void setProccessClass(String proccessClass) {
		this.proccessClass = proccessClass;
	}

	@Override
	public String toString() {
		return "TournamentType [id=" + id + ", name=" + name
				+ ", proccessClass=" + proccessClass + "]";
	}
}
