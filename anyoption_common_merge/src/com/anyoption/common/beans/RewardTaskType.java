package com.anyoption.common.beans;

/**
 * @author liors
 *
 */
public class RewardTaskType implements java.io.Serializable {

	private static final long serialVersionUID = -3476522460029892149L;

	public static final long REWARD_TASK_TYPE_OPEN_ACCOUNT = 1;
	public static final long REWARD_TASK_TYPE_FTD_AMOUNT = 2;
	public static final long REWARD_TASK_TYPE_DOCS = 3;
	public static final long REWARD_TASK_TYPE_INV_PNTS = 4;
	public static final long REWARD_TASK_TYPE_INV_COUNT_BONUS = 5;
	public static final long REWARD_TASK_TYPE_INV_ABOVE = 6;
	public static final long REWARD_TASK_TYPE_FRIENDS_INVITE = 7;
	public static final long REWARD_TASK_TYPE_SHOWOFF = 8;
	public static final long REWARD_TASK_TYPE_INV_VIA_APP = 9;
	public static final long REWARD_TASK_TYPE_NEW_PLATFORMS = 10;
	public static final long REWARD_TASK_TYPE_INVITED_FRIEND_INVESTED = 11;
	
	public static final int GROUP_TYPE_USERS = 1;
	public static final int GROUP_TYPE_TRANSACTIONS = 2;
	public static final int GROUP_TYPE_INVESTMENTS = 3;
	public static final int GROUP_TYPE_SHOW_OFF = 4;
	public static final int GROUP_TYPE_DOCS = 5;
	public static final int GROUP_TYPE_INVESTMENTS_PREMIA = 6;
}
