package com.anyoption.common.beans;

public class MarketDynamicsQuoteParams implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private double sp;
	private double spMax;
	private double tU;
	private double tD;
	private double amountU;
	private double amountD;
	private double t; // Trader param
	
	public double getSp() {
		return sp;
	}

	public void setSp(double sp) {
		this.sp = sp;
	}

	public double getSpMax() {
		return spMax;
	}

	public void setSpMax(double spMax) {
		this.spMax = spMax;
	}

	public double gettU() {
		return tU;
	}

	public void settU(double tU) {
		this.tU = tU;
	}

	public double gettD() {
		return tD;
	}

	public void settD(double tD) {
		this.tD = tD;
	}

	public double getAmountU() {
		return amountU;
	}

	public void setAmountU(double amountU) {
		this.amountU = amountU;
	}

	public double getAmountD() {
		return amountD;
	}

	public void setAmountD(double amountD) {
		this.amountD = amountD;
	}
	
	public double getT() {
		return t;
	}

	public void setT(double t) {
		this.t = t;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls
				+ "sp: " + sp + ls
				+ "spMax: " + spMax + ls
				+ "tU: " + tU + ls
				+ "tD: " + tD + ls
				+ "amountU: " + amountU + ls
				+ "amountD: " + amountD + ls
				+ "t: " + t + ls;
	}
	
	public String getDifferences(MarketDynamicsQuoteParams qp) {
		StringBuilder sb = new StringBuilder();
		if (this.sp != qp.getSp()) {
			sb.append("sp=").append(qp.getSp());
		}
		if (this.spMax != qp.getSpMax()) {
			sb.append("spMax=").append(qp.getSpMax());
		}
		if (this.tU != qp.gettU()) {
			sb.append("tU=").append(qp.gettU());
		}
		if (this.tD != qp.gettD()) {
			sb.append("tD=").append(qp.gettD());
		}
		if (this.amountU != qp.getAmountU()) {
			sb.append("amountU=").append(qp.getAmountU());
		}
		if (this.amountD != qp.getAmountD()) {
			sb.append("amountD=").append(qp.getAmountD());
		}
		if (this.t != qp.getT()) {
			sb.append("t=").append(qp.getT());
		}
		return sb.toString();
	}
}