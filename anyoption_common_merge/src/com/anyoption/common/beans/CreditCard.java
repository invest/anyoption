package com.anyoption.common.beans;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.event.ValueChangeEvent;

import com.anyoption.common.annotations.AnyoptionNoJSON;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.util.CommonUtil;

public class CreditCard extends com.anyoption.common.beans.base.CreditCard {

    /**
	 * 
	 */
	private static final long serialVersionUID = -977071640546176020L;

    @AnyoptionNoJSON
	private Date timeModified;
    @AnyoptionNoJSON
    private String holderIdNum;
    @AnyoptionNoJSON
    private boolean visible;
    @AnyoptionNoJSON
    protected Date timeCreated;
    @AnyoptionNoJSON
    protected String utcOffsetCreated;
    @AnyoptionNoJSON
    protected String utcOffsetModified;
    @AnyoptionNoJSON
    private boolean isDocumentsSent;
    @AnyoptionNoJSON
    private boolean toCopy;
    @AnyoptionNoJSON
	protected boolean creditEnabled; // true in case the cc allow credit(bookBack)
    @AnyoptionNoJSON
    private String recurringTransaction; //empty_string if the card haven't used in TBI yet
    @AnyoptionNoJSON
    private long bin;
    @AnyoptionNoJSON
    private boolean isBelongsToAccountHolder;
    @AnyoptionNoJSON
	private boolean isPowerOfAttorney;
    @AnyoptionNoJSON
	private boolean isVerificationCall;
    @AnyoptionNoJSON
	private boolean isSentHolderIdCopy;
    @AnyoptionNoJSON
	private boolean isSentCcCopy;
    @AnyoptionNoJSON
	private boolean isUserHaveIdCopy;
    @AnyoptionNoJSON
	private boolean isUserSentWithdrawalForm;
    @AnyoptionNoJSON
    private long sumDeposits;
    @AnyoptionNoJSON
	private Date depositsFrom;
    @AnyoptionNoJSON
	private Date depositsTo;
    
    //risk issue
    @AnyoptionNoJSON
  	private boolean isSucceedDeposit;
    @AnyoptionNoJSON
  	private RequestedFiles reqRiskIssueScreen;
    @AnyoptionNoJSON
  	private RequestedFiles request;
    @AnyoptionNoJSON
  	private String filesRiskStatus;
    @AnyoptionNoJSON
  	private int filesRiskStatusId;
    @AnyoptionNoJSON
  	private boolean needUpdateStatus;
    @AnyoptionNoJSON
  	private String comment;
    @AnyoptionNoJSON
  	private boolean changedB;
    @AnyoptionNoJSON
  	private boolean changedBD;
    @AnyoptionNoJSON
  	private boolean unblockCC;
    @AnyoptionNoJSON
	private String countryName;
    @AnyoptionNoJSON
	private String typeNameByBin;
    @AnyoptionNoJSON
	private boolean isManuallyAllowed;
    
    private ArrayList<File> ccFiles;
    private Bins ccBins;
    
	private boolean ccAllowed;
	private boolean ccNotAllowedDepositOnly;
	private ArrayList<CreditCardHolderNameHistory> ccHolderNameHistory;
	private boolean docsAreNotRequired;
	@AnyoptionNoJSON
	private boolean is3DAttempt;
	/**
     * 
     */
    public CreditCard() {
    	id = 0;
    }

    public long getBin() {
    	return bin;
    }
    
    public void setBin(long bin) {
    	this.bin = bin;
    }
    
    /**
     * @param cc - Credit Card
     */
    public CreditCard(com.anyoption.common.beans.base.CreditCard cc) {
    	this.id = cc.getId();
    	this.userId = cc.getUserId();
    	this.typeId = cc.getTypeId();
    	this.ccNumber = cc.getCcNumber();
    	this.ccPass = cc.getCcPass();
    	this.expMonth = cc.getExpMonth();
    	this.expYear = cc.getExpYear();
    	this.holderName = cc.getHolderName();
    	this.holderIdNum = cc.getHolderId(); 
    }

	public void setType(CreditCardType type) {
		this.type = type;
	}

	/**
	 * @return
	 */
	public String getCcNumberLast4() {
		String ccnum = String.valueOf(ccNumber);
		return ccnum.substring(ccnum.length() - 4);
	}

	/**
	 * @return
	 */
	public String getCcNumberXXX() {
		String ccnum = String.valueOf(ccNumber);
		String out = "";
		for (int i = 0;i < ccnum.length() - 4; i++) {
			out += "*";
        }
		return out + ccnum.substring(ccnum.length() - 4);
	}

	/**
	 * @return
	 */
	public String getHolderIdNum() {
		return holderIdNum;
	}

	/**
	 * @param holderIdNum
	 */
	public void setHolderIdNum(String holderIdNum) {
		this.holderIdNum = holderIdNum;
	}

	/**
	 * @return
	 */
	public Date getTimeModified() {
		return timeModified;
	}

	/**
	 * @param timeModified
	 */
	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}

    /**
     * @return
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * @param visible
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

	/**
	 * @return
	 */
	public boolean getIsToCopyBoolean() {
		return toCopy;
	}

	/**
	 * @return the utcOffsetCreated
	 */
	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	/**
	 * @param utcOffsetCreated the utcOffsetCreated to set
	 */
	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	/**
	 * @return the utcOffsetModified
	 */
	public String getUtcOffsetModified() {
		return utcOffsetModified;
	}

	/**
	 * @param utcOffsetModified the utcOffsetModified to set
	 */
	public void setUtcOffsetModified(String utcOffsetModified) {
		this.utcOffsetModified = utcOffsetModified;
	}

	/**
	 * @return the creditEnabled
	 */
	public boolean isCreditEnabled() {
		return creditEnabled;
	}

	/**
	 * @param creditEnabled the creditEnabled to set
	 */
	public void setCreditEnabled(boolean creditEnabled) {
		this.creditEnabled = creditEnabled;
	}

	/**
	 * @return
	 */
	public boolean isDocumentsSent() {
		return isDocumentsSent;
	}

	/**
	 * @param isDocumentsSent
	 */
	public void setDocumentsSent(boolean isDocumentsSent) {
		this.isDocumentsSent = isDocumentsSent;
	}

    /**
     * @return
     */
    public String toStringForComments() {
        return "CVV = " + this.ccPass + " | " +
             "expMonth = " + this.expMonth + " | " +
             "expYear = " + this.expYear + " | " +
             "holderIdNum = " + this.holderIdNum;
    }

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	/**
	 * @return
	 */
	public String getBinStr(){
		return String.valueOf(ccNumber).subSequence(0, 6).toString();
	}
	
	public String getLast4DigitsStr() {
		String str = String.valueOf(ccNumber);
		if (str.length() < 4) {
			return str;
		} else {
			return str.substring(str.length() - 4);
		}
	}
	
	public String getRecurringTransaction() {
		return recurringTransaction;
	}

	public void setRecurringTransaction(String recurringTransaction) {
		this.recurringTransaction = recurringTransaction;
	}

	/**
	 * @return the isBelongsToAccountHolder
	 */
	public boolean isBelongsToAccountHolder() {
		return isBelongsToAccountHolder;
	}

	/**
	 * @param isBelongsToAccountHolder the isBelongsToAccountHolder to set
	 */
	public void setBelongsToAccountHolder(boolean isBelongsToAccountHolder) {
		this.isBelongsToAccountHolder = isBelongsToAccountHolder;
	}

	public boolean isPowerOfAttorney() {
		return isPowerOfAttorney;
	}

	public void setPowerOfAttorney(boolean isPowerOfAttorney) {
		this.isPowerOfAttorney = isPowerOfAttorney;
	}

	public boolean isVerificationCall() {
		return isVerificationCall;
	}

	public void setVerificationCall(boolean isVerificationCall) {
		this.isVerificationCall = isVerificationCall;
	}

	public boolean isSentHolderIdCopy() {
		return isSentHolderIdCopy;
	}

	public void setSentHolderIdCopy(boolean isSentHolderIdCopy) {
		this.isSentHolderIdCopy = isSentHolderIdCopy;
	}

	public boolean isSentCcCopy() {
		return isSentCcCopy;
	}

	public void setSentCcCopy(boolean isSentCcCopy) {
		this.isSentCcCopy = isSentCcCopy;
	}

	public boolean isUserHaveIdCopy() {
		return isUserHaveIdCopy;
	}

	public void setUserHaveIdCopy(boolean isUserHaveIdCopy) {
		this.isUserHaveIdCopy = isUserHaveIdCopy;
	}

	public boolean isUserSentWithdrawalForm() {
		return isUserSentWithdrawalForm;
	}

	public void setUserSentWithdrawalForm(boolean isUserSentWithdrawalForm) {
		this.isUserSentWithdrawalForm = isUserSentWithdrawalForm;
	}

	public long getSumDeposits() {
		return sumDeposits;
	}

	public void setSumDeposits(long sumDeposits) {
		this.sumDeposits = sumDeposits;
	}

	public Date getDepositsFrom() {
		return depositsFrom;
	}

	public void setDepositsFrom(Date depositsFrom) {
		this.depositsFrom = depositsFrom;
	}

	public Date getDepositsTo() {
		return depositsTo;
	}

	public void setDepositsTo(Date depositsTo) {
		this.depositsTo = depositsTo;
	}

	public boolean isSucceedDeposit() {
		return isSucceedDeposit;
	}

	public void setSucceedDeposit(boolean isSucceedDeposit) {
		this.isSucceedDeposit = isSucceedDeposit;
	}

	public String getFilesRiskStatus() {
		return filesRiskStatus;
	}

	public void setFilesRiskStatus(String filesRiskStatus) {
		this.filesRiskStatus = filesRiskStatus;
	}

	public int getFilesRiskStatusId() {
		return filesRiskStatusId;
	}

	public void setFilesRiskStatusId(int filesRiskStatusId) {
		this.filesRiskStatusId = filesRiskStatusId;
	}

	public boolean isNeedUpdateStatus() {
		return needUpdateStatus;
	}

	public void setNeedUpdateStatus(boolean needUpdateStatus) {
		this.needUpdateStatus = needUpdateStatus;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isChangedB() {
		return changedB;
	}

	public void setChangedB(boolean changed) {
		this.changedB = changed;
	}

	public boolean isChangedBD() {
		return changedBD;
	}

	public void setChangedBD(boolean changedBD) {
		this.changedBD = changedBD;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getTypeNameByBin() {
		return typeNameByBin;
	}

	public void setTypeNameByBin(String typeNameByBin) {
		this.typeNameByBin = typeNameByBin;
	}

	public boolean isManuallyAllowed() {
		return isManuallyAllowed;
	}

	public void setManuallyAllowed(boolean isManuallyAllowed) {
		this.isManuallyAllowed = isManuallyAllowed;
	}

	public RequestedFiles getReqRiskIssueScreen() {
		if (reqRiskIssueScreen == null) {
			reqRiskIssueScreen = new RequestedFiles();
		}
		return reqRiskIssueScreen;
	}

	public void setReqRiskIssueScreen(RequestedFiles reqRiskIssueScreen) {
		this.reqRiskIssueScreen = reqRiskIssueScreen;
	}

	public RequestedFiles getRequest() {
		if (request == null) {
			request = new RequestedFiles();
		}
		return request;
	}

	public void setRequest(RequestedFiles request) {
		this.request = request;
	}
	
	public String getSucceedDeposits(){
		if (isSucceedDeposit) {
			return "YES";
		}
		return "NO";
	}
	
	public void updateChangeB(ValueChangeEvent event) throws Exception {
		changedB = !changedB;
		changedBD = false;
		Boolean blocked = (Boolean)event.getNewValue();
		previousPermission = permission;
		permission = blocked ? NOT_ALLOWED:ALLOWED;
		if (!blocked) {
			unblockCC = true;
		} else {
			unblockCC = false;
		}
	}

	public void updateChangeBD(ValueChangeEvent event) throws Exception {
		changedBD = !changedBD;
		changedB = false;
		Boolean blocked = (Boolean)event.getNewValue();
		previousPermission = permission;
		permission = blocked ? NOT_ALLOWED_DEPOSIT:ALLOWED;
		if (!blocked) {
			unblockCC = true;
		} else {
			unblockCC = false;
		}
	}
	
	public boolean isChanged() {
		return changedB || changedBD || unblockCC;
	}

	/* (non-Javadoc)
	 * @see com.anyoption.beans.base.CreditCard#toString()
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "CreditCard" + ls
            + super.toString() + ls
            + "id: " + id + ls
            + "userId: " + userId + ls
            + "typeId: " + typeId + ls
            + "ccNumber: " + CommonUtil.getNumberXXXX(String.valueOf(ccNumber)) + ls
            + "ccPass: " + "*****" + ls
            + "timeCreated: " + timeCreated + ls
            + "timeModified: " + timeModified + ls
            + "expMonth: " + expMonth + ls
            + "expYear: " + expYear + ls
            + "holderName: " + holderName + ls
            + "holderIdNum: " + holderIdNum + ls
            + "visible: " + visible + ls
            + "allowed: " + (permission == ALLOWED) + ls
            + "type: " + type + ls
            + "docsAreNotRequired: " + docsAreNotRequired + ls
            + "countryName: " + countryName + ls
            + "ccNotAllowedDepositOnly: " + ccNotAllowedDepositOnly + ls;
    }

	public ArrayList<File> getCcFiles() {
		return ccFiles;
	}

	public void setCcFiles(ArrayList<File> ccFiles) {
		this.ccFiles = ccFiles;
	}

	public Bins getCcBins() {
		return ccBins;
	}

	public void setCcBins(Bins ccBins) {
		this.ccBins = ccBins;
	}

	public boolean isCcAllowed() {
		return ccAllowed;
	}

	public void setCcAllowed(boolean ccAllowed) {
		this.ccAllowed = ccAllowed;
	}

	public boolean isCcNotAllowedDepositOnly() {
		return ccNotAllowedDepositOnly;
	}

	public void setCcNotAllowedDepositOnly(boolean ccNotAllowedDepositOnly) {
		this.ccNotAllowedDepositOnly = ccNotAllowedDepositOnly;
	}

	public ArrayList<CreditCardHolderNameHistory> getCcHolderNameHistory() {
		return ccHolderNameHistory;
	}

	public void setCcHolderNameHistory(ArrayList<CreditCardHolderNameHistory> ccHolderNameHistory) {
		this.ccHolderNameHistory = ccHolderNameHistory;
	}

	public boolean isDocsAreNotRequired() {
		return docsAreNotRequired;
	}

	public void setDocsAreNotRequired(boolean docsAreNotRequired) {
		this.docsAreNotRequired = docsAreNotRequired;
	}

	public boolean isIs3DAttempt() {
		return is3DAttempt;
	}

	public void setIs3DAttempt(boolean is3dAttempt) {
		is3DAttempt = is3dAttempt;
	}
}