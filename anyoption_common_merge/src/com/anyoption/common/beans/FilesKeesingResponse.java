package com.anyoption.common.beans;

import java.util.Date;

public class FilesKeesingResponse implements java.io.Serializable {

	
	
	public FilesKeesingResponse(long ksResponseId, boolean isActive, String firstName, String lastName,
			String nationality, String issuingCountry, String docNumber, Date expireDate, Long personalId,
			Date dateOfBirth, String gender, Long docId, Long checkStatus, Long reasonNOK) {
		super();
		this.ksResponseId = ksResponseId;
		this.isActive = isActive;
		this.firstName = firstName;
		this.lastName = lastName;
		this.nationality = nationality;
		this.issuingCountry = issuingCountry;
		this.docNumber = docNumber;
		this.expireDate = expireDate;
		this.personalId = personalId;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.docId = docId;
		this.checkStatus = checkStatus;
		this.reasonNOK = reasonNOK;
	}

	private static final long serialVersionUID = 1L;
	
	private long ksResponseId;
	private boolean isActive;
	private String firstName;
	private String lastName;
	private String nationality;
	private String issuingCountry;
	private String docNumber;
	private Date expireDate;
	private Long personalId;
	private Date dateOfBirth;
	private String gender;
	private Long docId;
	private Long checkStatus;
	private Long reasonNOK;
	
	public long getKsResponseId() {
		return ksResponseId;
	}
	public void setKsResponseId(long ksResponseId) {
		this.ksResponseId = ksResponseId;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getIssuingCountry() {
		return issuingCountry;
	}
	public void setIssuingCountry(String issuingCountry) {
		this.issuingCountry = issuingCountry;
	}
	public String getDocNumber() {
		return docNumber;
	}
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}
	public Date getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}
	public Long getPersonalId() {
		return personalId;
	}
	public void setPersonalId(Long personalId) {
		this.personalId = personalId;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Long getDocId() {
		return docId;
	}
	public void setDocId(Long docId) {
		this.docId = docId;
	}
	public Long getCheckStatus() {
		return checkStatus;
	}
	public void setCheckStatus(Long checkStatus) {
		this.checkStatus = checkStatus;
	}
	public Long getReasonNOK() {
		return reasonNOK;
	}
	public void setReasonNOK(Long reasonNOK) {
		this.reasonNOK = reasonNOK;
	}
	
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "FilesKeesingMatch: " + ls
				+ super.toString() + ls
				+ "ksResponseId: " + ksResponseId + ls
				+ "isActive: " + isActive + ls
				+ "firstName: " + firstName + ls
				+ "lastName: " + lastName + ls
				+ "nationality: " + nationality + ls
				+ "issuingCountry: " + issuingCountry + ls				
				+ "docNumber: " + docNumber + ls
				+ "expireDate: " + expireDate + ls
				+ "personalId: " + personalId + ls
				+ "dateOfBirth: " + dateOfBirth + ls
				+ "gender: " + gender + ls				
				+ "docId: " + docId + ls
				+ "checkStatus: " + checkStatus + ls
				+ "reasonNOK: " + reasonNOK + ls;
	}
}