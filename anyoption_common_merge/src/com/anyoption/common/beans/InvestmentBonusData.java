package com.anyoption.common.beans;

public class InvestmentBonusData {
	double oddsWin;
	double oddsLose;
	double bonusWinOdds;
	double bonusLoseOdds;

	long investmentId;
	long bonusUserId;
	long amount;
	long insuranceAmountRU;

	
	
	public InvestmentBonusData(	double oddsWin, double oddsLose,
								double bonusWinOdds, double bonusLoseOdds, 
								long investmentId, long bonusUserId,
								long amount, long insuranceAmountRU) {
		super();
		this.oddsWin = oddsWin;
		this.oddsLose = oddsLose;
		this.bonusWinOdds = bonusWinOdds;
		this.bonusLoseOdds = bonusLoseOdds;
		this.investmentId = investmentId;
		this.bonusUserId = bonusUserId;
		this.amount = amount;
		this.insuranceAmountRU = insuranceAmountRU;
	}

	public double getOddsWin() {
		return oddsWin;
	}

	public void setOddsWin(double oddsWin) {
		this.oddsWin = oddsWin;
	}

	public double getOddsLose() {
		return oddsLose;
	}

	public void setOddsLose(double oddsLose) {
		this.oddsLose = oddsLose;
	}

	public double getBonusWinOdds() {
		return bonusWinOdds;
	}

	public void setBonusWinOdds(double bonusWinOdds) {
		this.bonusWinOdds = bonusWinOdds;
	}

	public double getBonusLoseOdds() {
		return bonusLoseOdds;
	}

	public void setBonusLoseOdds(double bonusLoseOdds) {
		this.bonusLoseOdds = bonusLoseOdds;
	}

	public long getBonusUserId() {
		return bonusUserId;
	}

	public void setBonusUserId(long bonusUserId) {
		this.bonusUserId = bonusUserId;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public long getInsuranceAmountRU() {
		return insuranceAmountRU;
	}

	public void setInsuranceAmountRU(long insuranceAmountRU) {
		this.insuranceAmountRU = insuranceAmountRU;
	}

	public long getInvestmentId() {
		return investmentId;
	}

	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}

}
