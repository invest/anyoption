package com.anyoption.common.beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

/**
 *
 * @author Eyal g
 *
 */
public class TransactionPostponed implements Serializable {
	
	private static final long serialVersionUID = 1498121932788262251L;
	private transient static final Logger log = Logger.getLogger(TransactionPostponed.class);
	private static final int NUM_OF_DAYS = 3;

	public long id;
	public Date timeCreated; 
	public int numberOfDays; 
	public long writerId; 
	public Date timeUpdated;
	public long transactionId;
		
	/**
	 * @param date
	 * @return
	 */
	public ArrayList<SelectItem> getPostponedDates(Transaction tran) {
		Date date = tran.getTimeCreated();
		ArrayList<SelectItem> selectItems = new ArrayList<SelectItem>();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		Calendar jobTime = Calendar.getInstance(); //TODO get from db/scheduler.
		jobTime.set(Calendar.HOUR_OF_DAY, 23);
		jobTime.set(Calendar.MINUTE, 00);
		int dayToAdd = 1;
		
		if (date.after(jobTime.getTime())) {
			dayToAdd = 2;
		}

		for (int i = 0; i < NUM_OF_DAYS; i++) {
			calendar.add(Calendar.DAY_OF_MONTH, dayToAdd);	
			selectItems.add(new SelectItem(i, simpleDateFormat.format(calendar.getTime())));
			dayToAdd = 1;
		}
		return selectItems;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public Date getTimeCreated() {
		return timeCreated;
	}
	
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	public int getNumberOfDays() {
		return numberOfDays;
	}
	
	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}
	
	public long getWriterId() {
		return writerId;
	}
	
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	
	public Date getTimeUpdated() {
		return timeUpdated;
	}
	
	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
}
