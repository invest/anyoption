package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.Map;

public class MarketTranslations implements Serializable {

	private static final long serialVersionUID = 7113791182706676736L;
	
	Map<Long, String> marketName;
	Map<Long, String> marketShortName;
	Map<Long, String> marketDescription;
	Map<Long, String> additionalText;
	Map<Long, String> pageDescription;
	Map<Long, String> pageKeywords;
	Map<Long, String> pageTitle;
	Map<Long, String> descriptionInPage;
	
	public Map<Long, String> getMarketName() {
		return marketName;
	}
	
	public void setMarketName(Map<Long, String> marketName) {
		this.marketName = marketName;
	}
	
	public Map<Long, String> getMarketShortName() {
		return marketShortName;
	}
	
	public void setMarketShortName(Map<Long, String> marketShortName) {
		this.marketShortName = marketShortName;
	}
	
	public Map<Long, String> getMarketDescription() {
		return marketDescription;
	}

	public void setMarketDescription(Map<Long, String> marketDescription) {
		this.marketDescription = marketDescription;
	}
	
	public Map<Long, String> getAdditionalText() {
		return additionalText;
	}

	public void setAdditionalText(Map<Long, String> additionalText) {
		this.additionalText = additionalText;
	}

	public Map<Long, String> getPageDescription() {
		return pageDescription;
	}
	
	public void setPageDescription(Map<Long, String> pageDescription) {
		this.pageDescription = pageDescription;
	}
	
	public Map<Long, String> getPageKeywords() {
		return pageKeywords;
	}
	
	public void setPageKeywords(Map<Long, String> pageKeywords) {
		this.pageKeywords = pageKeywords;
	}
	
	public Map<Long, String> getPageTitle() {
		return pageTitle;
	}
	
	public void setPageTitle(Map<Long, String> pageTitle) {
		this.pageTitle = pageTitle;
	}
	
	public Map<Long, String> getDescriptionInPage() {
		return descriptionInPage;
	}
	
	public void setDescriptionInPage(Map<Long, String> descriptionInPage) {
		this.descriptionInPage = descriptionInPage;
	}
}
