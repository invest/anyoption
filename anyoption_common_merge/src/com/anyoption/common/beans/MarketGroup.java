package com.anyoption.common.beans;

//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;

import java.io.Serializable;
import java.util.Date;

import com.anyoption.common.annotations.AnyoptionNoJSON;

/**
 * Market group VO.
 */
public  class MarketGroup extends com.anyoption.common.beans.base.MarketGroup implements Serializable {

	@AnyoptionNoJSON
    private String displayName;
	@AnyoptionNoJSON
    private String displayNameKey;
	@AnyoptionNoJSON
    private Date timeCreated;
	@AnyoptionNoJSON
    private String url; //group web page url
    
    protected long writerId;

    /**
     * @return Returns the displayName.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName The displayName to set.
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return Returns the timeCreated.
     */
    public Date getTimeCreated() {
        return timeCreated;
    }

    /**
     * @param timeCreated The timeCreated to set.
     */
    public void setTimeCreated(Date timeCreated) {
        this.timeCreated = timeCreated;
    }

    /**
     * @return Returns the writerId.
     */
    public long getWriterId() {
        return writerId;
    }

    /**
     * @param writerId The writerId to set.
     */
    public void setWriterId(long writerId) {
        this.writerId = writerId;
    }

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "MarketGroup ( "
	        + super.toString() + TAB
	        + "displayName = " + this.displayName + TAB
	        + "timeCreated = " + this.timeCreated + TAB
	        + "url = " + this.url + TAB
	        + "writerId = " + this.writerId + TAB
	        + " )";

	    return retValue;
	}
	public String getDisplayNameKey() {
		return displayNameKey;
	}
	public void setDisplayNameKey(String displayNameKey) {
		this.displayNameKey = displayNameKey;
	}
    /**
     * @return Returns the market url.
     */
	public String getUrl() {
		return url;
	}
	/**
     * @param url The url to set.
     */
	public void setUrl(String url) {
		this.url = url;
	}



}