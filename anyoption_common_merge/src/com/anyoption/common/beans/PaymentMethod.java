package com.anyoption.common.beans;

import java.io.Serializable;

public class PaymentMethod extends com.anyoption.common.beans.base.PaymentMethod implements  Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	/**
	 * Override Clone function for PaymentMethod Object
	 * @return cloned PaymentMethod instance
	 */
	public Object clone() throws CloneNotSupportedException {
		return (PaymentMethod) super.clone();
	}
}