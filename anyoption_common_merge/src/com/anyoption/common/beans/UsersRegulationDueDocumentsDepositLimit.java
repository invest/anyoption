package com.anyoption.common.beans;

public class UsersRegulationDueDocumentsDepositLimit {
	public static final long WARNING_LIMIT = 1;
	public static final long SUSPEND_LIMIT = 2;
	
	private long id;
	private long userId;
	private long depositSum;
	private long depositLimit;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public String toString(){
		final String ls = System.getProperty("line.separator");
		String retValue = "";
		retValue = "UsersRegulationDueDocumentsDepositLimit ( "
		        + super.toString() + ls
		        + "id = " + this.id + ls
		        + "userId = " + this.userId + ls
		        + "depositSum = " + this.depositSum + ls
		        + "depositLimit = " + this.depositLimit + ls
		        + " )";
		return retValue;
	}

	public long getDepositSum() {
		return depositSum;
	}

	public void setDepositSum(long depositSum) {
		this.depositSum = depositSum;
	}

	public long getDepositLimit() {
		return depositLimit;
	}

	public void setDepositLimit(long depositLimit) {
		this.depositLimit = depositLimit;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
}