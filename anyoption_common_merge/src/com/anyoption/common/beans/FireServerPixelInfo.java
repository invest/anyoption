package com.anyoption.common.beans;

/**
 * 
 * @author eyal.ohana
 *
 */
public class FireServerPixelInfo {
	private int serverPixelsPublisherId;
	private int serverPixelsTypeId;
	private String publisherIdentifier;
	private long userId;
	private long transactionId;
	private String deviceId;
	private long platformId;
	private String orderId;
	
	/**
	 * @return the serverPixelsPublisherId
	 */
	public int getServerPixelsPublisherId() {
		return serverPixelsPublisherId;
	}
	
	/**
	 * @param serverPixelsPublisherId the serverPixelsPublisherId to set
	 */
	public void setServerPixelsPublisherId(int serverPixelsPublisherId) {
		this.serverPixelsPublisherId = serverPixelsPublisherId;
	}
	
	/**
	 * @return the serverPixelsTypeId
	 */
	public int getServerPixelsTypeId() {
		return serverPixelsTypeId;
	}
	
	/**
	 * @param serverPixelsTypeId the serverPixelsTypeId to set
	 */
	public void setServerPixelsTypeId(int serverPixelsTypeId) {
		this.serverPixelsTypeId = serverPixelsTypeId;
	}
	
	/**
	 * @return the publisherIdentifier
	 */
	public String getPublisherIdentifier() {
		return publisherIdentifier;
	}
	
	/**
	 * @param publisherIdentifier the publisherIdentifier to set
	 */
	public void setPublisherIdentifier(String publisherIdentifier) {
		this.publisherIdentifier = publisherIdentifier;
	}
	
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}
	
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * @return the platformId
	 */
	public long getPlatformId() {
		return platformId;
	}

	/**
	 * @param platformId the platformId to set
	 */
	public void setPlatformId(long platformId) {
		this.platformId = platformId;
	}

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
