package com.anyoption.common.beans;

import java.util.Date;

/**
 * @author liors
 *
 */
public class RewardUser implements java.io.Serializable {
	private static final long serialVersionUID = -8654441747326982252L;
	private int id;
	private long userId;
	private int tierId;
	private double experiencePoints;
	private Date timeLastActivity;
	private Date timeLastReducedPoints;
	private int experiencePointsDelta; // use when changing tier
	
	/**
	 * @param id
	 * @param userId
	 * @param tierId
	 * @param experiencePoints
	 * @param timeLastActivity
	 * @param timeLastReducedPoints
	 */
	public RewardUser(int id, long userId, int tierId, int experiencePoints,
			Date timeLastActivity, Date timeLastReducedPoints) {
		this.id = id;
		this.userId = userId;
		this.tierId = tierId;
		this.experiencePoints = experiencePoints;
		this.timeLastActivity = timeLastActivity;
		this.timeLastReducedPoints = timeLastReducedPoints;
	}

	public RewardUser() {
		this.timeLastActivity = null;
		this.timeLastReducedPoints = null;
	}
	
	/**
	 * tier = 0
	 * experiencePoints = 0.
	 * @param userId
	 * @param experiencePoints
	 */
	public RewardUser(long userId) {
		this.userId = userId;		
		this.experiencePoints = 0;
		this.tierId = RewardTier.TIER_NOOB_0;
		this.timeLastActivity = null;
		this.timeLastReducedPoints = null;
	}
	
	/**
	 * @param userId
	 * @param tierId
	 * @param experiencePoints
	 */
	public RewardUser(long userId, int tierId, int experiencePoints) {
		this.userId = userId;		
		this.experiencePoints = experiencePoints;
		this.tierId = tierId;
		this.timeLastActivity = null;
		this.timeLastReducedPoints = null;
	}
	
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the timeLastActivity
	 */
	public Date getTimeLastActivity() {
		return timeLastActivity;
	}
	/**
	 * @param timeLastActivity the timeLastActivity to set
	 */
	public void setTimeLastActivity(Date timeLastActivity) {
		this.timeLastActivity = timeLastActivity;
	}
	/**
	 * @return the timeLastReducedPoints
	 */
	public Date getTimeLastReducedPoints() {
		return timeLastReducedPoints;
	}
	/**
	 * @param timeLastReducedPoints the timeLastReducedPoints to set
	 */
	public void setTimeLastReducedPoints(Date timeLastReducedPoints) {
		this.timeLastReducedPoints = timeLastReducedPoints;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the tierId
	 */
	public int getTierId() {
		return tierId;
	}
	/**
	 * @param tierId the tierId to set
	 */
	public void setTierId(int tierId) {
		this.tierId = tierId;
	}
	
	/**
	 * @return the experiencePointsDelta
	 */
	public int getExperiencePointsDelta() {
		return experiencePointsDelta;
	}

	/**
	 * @param experiencePointsDelta the experiencePointsDelta to set
	 */
	public void setExperiencePointsDelta(int experiencePointsDelta) {
		this.experiencePointsDelta = experiencePointsDelta;
	}

	public double getExperiencePoints() {
		return experiencePoints;
	}

	public void setExperiencePoints(double experiencePoints) {
		this.experiencePoints = experiencePoints;
	}

	@Override
	public String toString() {
		return "RewardUser [id=" + id + ", userId=" + userId + ", tierId="
				+ tierId + ", experiencePoints=" + experiencePoints
				+ ", timeLastActivity=" + timeLastActivity
				+ ", timeLastReducedPoints=" + timeLastReducedPoints + "]";
	}
}
