package com.anyoption.common.beans;

import java.io.Serializable;

/**
 * @author eranl
 *
 */
public class MarketingAffiliate implements Serializable {

	private static final long serialVersionUID = 6496666608582492415L;
	
    protected long id;
    protected String name;
    protected String key;
    protected long writerId;
    protected String writerName;
    protected String specialCode;
    protected String priorityId;
    protected boolean isAllowAffilate;
    protected boolean active;
    
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the writerName
	 */
	public String getWriterName() {
		return writerName;
	}
	/**
	 * @param writerName the writerName to set
	 */
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}
	/**
	 * @return the specialCode
	 */
	public String getSpecialCode() {
		return specialCode;
	}
	/**
	 * @param specialCode the specialCode to set
	 */
	public void setSpecialCode(String specialCode) {
		this.specialCode = specialCode;
	}
	/**
	 * @return the priorityId
	 */
	public String getPriorityId() {
		return priorityId;
	}
	/**
	 * @param priorityId the priorityId to set
	 */
	public void setPriorityId(String priorityId) {
		this.priorityId = priorityId;
	}
	/**
	 * @return the isAllowAffilate
	 */
	public boolean isAllowAffilate() {
		return isAllowAffilate;
	}
	/**
	 * @param isAllowAffilate the isAllowAffilate to set
	 */
	public void setAllowAffilate(boolean isAllowAffilate) {
		this.isAllowAffilate = isAllowAffilate;
	}
	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MarketingAffiliate [id=" + id + ", name=" + name + ", key="
				+ key + ", writerId=" + writerId + ", writerName=" + writerName
				+ ", specialCode=" + specialCode + ", priorityId=" + priorityId
				+ ", isAllowAffilate=" + isAllowAffilate + ", active=" + active
				+ "]";
	}
    
}
