/**
 * 
 */
package com.anyoption.common.beans;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import com.anyoption.common.beans.base.Currency;

/**
 * @author pavelt
 *
 */
public class Tournament implements Serializable {

	private static final long serialVersionUID = -7273143537892247212L;
	
	private static final Logger logger = Logger.getLogger(Tournament.class);
	
	public static final String TOURNAMENTS_DIR_NAME = "tournaments";
	public static final long TOURNAMENT_TYPE_BY_TRADES = 1;
	public static final long TOURNAMENT_TYPE_BY_TRADE_VOLUME = 2;

	public static final int ACTIVE = 1;
	
	private long id;
	private TournamentType type;
	private String name;
	private Date timeStart;
	private Date timeEnd;
	private int active;
	private long minInvAmount;
	private long writerId;
	private Date timeUpdated;
	private TournamentNumberOfUsers numberOfUsersToShow;
	private String skinsToParticipate;
	private List<Long> skins;
	private Currency currency;
	private Date timeCreated;
	private TournamentUsersRank[] Users;
	private ArrayList<TournamentSkinsLanguages> tournamentSkinsLangList;
	
	public Tournament() {
		type = new TournamentType();
		numberOfUsersToShow = new TournamentNumberOfUsers();
		currency = new Currency();
		currency.setId(Currency.CURRENCY_USD_ID); //for now we use only $
		currency.setCode("USD");
		active = ACTIVE;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public TournamentType getType() {
		return type;
	}
	public void setType(TournamentType type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getTimeStart() {
		return timeStart;
	}
	public void setTimeStart(Date timeStart) {
		this.timeStart = timeStart;
	}
	public Date getTimeEnd() {
		return timeEnd;
	}
	public void setTimeEnd(Date timeEnd) {
		this.timeEnd = timeEnd;
	}
	public boolean isActive() {
		if (active == ACTIVE) {
			return true;
		} else {
			return false;
		}
	}
	public int getActive(){
		return active;
	}
	public void setActive(int active) {
			this.active = active;
	}
	public long getMinInvAmount() {
		return minInvAmount;
	}
	public void setMinInvAmount(long minInvAmount) {
		this.minInvAmount = minInvAmount;
	}
	public long getWriterId() {
		return writerId;
	}
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	public Date getTimeUpdated() {
		return timeUpdated;
	}
	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}
	public TournamentNumberOfUsers getNumberOfUsersToShow() {
		return numberOfUsersToShow;
	}
	public void setNumberOfUsersToShow(TournamentNumberOfUsers numberOfUsersToShow) {
		this.numberOfUsersToShow = numberOfUsersToShow;
	}
	public String getSkinsToParticipate() {
		return skinsToParticipate;
	}
	public void setSkinsToParticipate(String skinsToParticipate) {
		this.skinsToParticipate = skinsToParticipate;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public List<Long> getSkins() {
		return skins;
	}

	public void setSkins(List<Long> skins) {
		this.skins = skins;
	}
	
	/**
	 * from string to List<Long> 
	 * @param skinsStr string with , as separator between skin number
	 */
	public void setSkinsFromDB(String skinsStr) {
		skins = new ArrayList<Long>();
		if (skins != null) {
			String[] skinArr = skinsStr.split(",");			
			for (String skin : skinArr) {
				skins.add(Long.valueOf(skin));
			}
		}
	}
	
	/**
	 * from list of skins to string with , as separator between skin number
	 * @return string of skins with , as separator between skin number
	 */
	public String getSkinsToDB() {
		String tmp = "";
		for (int i=0; i < skins.size(); i++) {
			tmp += skins.get(i) + ",";
		}
		return tmp.substring(0, tmp.length() - 1);
	}
	
	/**
	 * check if the tournament is after start time and before end time
	 * @return true if open false if not
	 */
	public boolean isOpen() {
		return timeStart.before(new Date()) && timeEnd.after(new Date());
	}

	public TournamentUsersRank[] getUsers() {
		return Users;
	}

	public void setUsers(TournamentUsersRank[] users) {
		Users = users;
	}

	@Override
	public String toString() {
		return "Tournament [id=" + id + ", type=" + type + ", name=" + name
				+ ", timeStart=" + timeStart + ", timeEnd=" + timeEnd
				+ ", active=" + active + ", minInvAmount=" + minInvAmount
				+ ", writerId=" + writerId + ", timeUpdated=" + timeUpdated
				+ ", numberOfUsersToShow=" + numberOfUsersToShow
				+ ", skinsToParticipate=" + skinsToParticipate + ", skins="
				+ skins + ", prize="
				+ ", currency=" + currency + ", timeCreated=" + timeCreated
				+ ", Users=" + Arrays.toString(Users) +  "]";
	}
	
	public long getMinInvAmountCents() {
		return minInvAmount * 100;
	}

	public ArrayList<TournamentSkinsLanguages> getTournamentSkinsLangList() {
		return tournamentSkinsLangList;
	}

	public void setTournamentSkinsLangList(ArrayList<TournamentSkinsLanguages> tournamentSkinsLangList) {
		this.tournamentSkinsLangList = tournamentSkinsLangList;
	}

}
