package com.anyoption.common.beans;

import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BanksManagerBase;

/**
 * Bank bean wrapper
 *
 * @author KobiM
 *
 */
public class BankBranches extends com.anyoption.common.beans.base.BankBranches {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5235072555156086184L;

	private static final Logger log = Logger.getLogger(BankBranches.class);

	private static ArrayList<com.anyoption.common.beans.base.BankBranches> banksBranches;

    public static ArrayList<com.anyoption.common.beans.base.BankBranches> getBanksBranches(long id) {
        if (null == banksBranches) {
            try {
            	banksBranches = BanksManagerBase.getBanksBranches();
            } catch (SQLException sqle) {
                log.error("Can't load banks branches.", sqle);
            }
        }
        return banksBranches;
    }
}
