/**
 *
 */
package com.anyoption.common.beans;

public class FileType implements java.io.Serializable {

	private static final long serialVersionUID = -7373348147298260826L;

	public static final long USER_ID_COPY = 1;
	public static final long PASSPORT = 2;
	public static final long CC_COPY = 4;
	public static final long PROTOCOL = 9;
	public static final long BANKWIRE_CONFIRMATION = 12;
	public static final long WITHDRAW_FORM = 13;
	public static final long POWER_OF_ATTORNEY = 16;
	public static final long ACCOUNT_CLOSING = 17;
	public static final long CC_HOLDER_ID = 18;
	public static final long SSN = 20;
	public static final long DRIVER_LICENSE = 21;
	public static final long UTILITY_BILL = 22;
	public static final long CC_COPY_FRONT = 23;
	public static final long CC_COPY_BACK = 24;
	public static final long COMPLAINT_FILE = 26;
	public static final long BANK_WIRE_PROTOCOL_FILE = 35;
	public static final long GBG_FILE = 36;
	public static final long CNMV_FILE = 38;

	private long id;
	private String name;
	private boolean isCreditCardFile;
	private FilesAdditionalInfo filesAdditionalInfo;
	private boolean isKeesingType;
	
	public FileType(){
		
	}
	public FileType(String name){
		this.name = name;
	}
	public boolean isCreditCardFile() {
		return isCreditCardFile;
	}
	public void setCreditCardFile(boolean isCreditCardFile) {
		this.isCreditCardFile = isCreditCardFile;
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	public FilesAdditionalInfo getFilesAdditionalInfo() {
		return filesAdditionalInfo;
	}
	public void setFilesAdditionalInfo(FilesAdditionalInfo filesAdditionalInfo) {
		this.filesAdditionalInfo = filesAdditionalInfo;
	}
	public boolean isKeesingType() {
		return isKeesingType;
	}
	public void setKeesingType(boolean isKeesingType) {
		this.isKeesingType = isKeesingType;
	}

}
