/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author pavelhe
 *
 */
public class OpportunitySettle implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1943456423980502870L;
	
	private Long id;
	private Date timeEstimatedClosing;
	private long typeId;
	private long marketId;
	private String closingLevel;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the timeEstimatedClosing
	 */
	public Date getTimeEstimatedClosing() {
		return timeEstimatedClosing;
	}
	/**
	 * @param timeEstimatedClosing the timeEstimatedClosing to set
	 */
	public void setTimeEstimatedClosing(Date timeEstimatedClosing) {
		this.timeEstimatedClosing = timeEstimatedClosing;
	}
	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}
	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}
	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	/**
	 * @return the closingLevel
	 */
	public String getClosingLevel() {
		return closingLevel;
	}
	/**
	 * @param closingLevel the closingLevel to set
	 */
	public void setClosingLevel(String closingLevel) {
		this.closingLevel = closingLevel;
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return id.equals(obj);
	}
	
	@Override
	public String toString() {
		return "OpportunitySettle [id=" + id + ", timeEstimatedClosing="
				+ timeEstimatedClosing + ", typeId=" + typeId + ", marketId="
				+ marketId + ", closingLevel=" + closingLevel + "]";
	}
	
}
