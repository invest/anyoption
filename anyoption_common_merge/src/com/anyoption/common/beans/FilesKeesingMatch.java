package com.anyoption.common.beans;

import java.util.Date;

public class FilesKeesingMatch implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	public FilesKeesingMatch(Long  ksResponseId, String userFirstName, String keesingFirstName, Integer flagFirstName, String userLastName,
			String keesingLastName, Integer flagLastName, String userCountry, String keesingCountry,
			Integer flagCountry, Date userDOB, Date keesingDOB, Integer flagDOB, String userGender,
			String keesingGender, Integer flagGender) {
		super();
		this.ksResponseId = ksResponseId;
		this.userFirstName = userFirstName;
		this.keesingFirstName = keesingFirstName;
		this.flagFirstName = flagFirstName;
		this.userLastName = userLastName;
		this.keesingLastName = keesingLastName;
		this.flagLastName = flagLastName;
		this.userCountry = userCountry;
		this.keesingCountry = keesingCountry;
		this.flagCountry = flagCountry;
		this.userDOB = userDOB;
		this.keesingDOB = keesingDOB;
		this.flagDOB = flagDOB;
		this.userGender = userGender;
		this.keesingGender = keesingGender;
		this.flagGender = flagGender;
	}
	
	private Long  ksResponseId;
	private String userFirstName;
	private String keesingFirstName;
	private Integer flagFirstName;

	private String userLastName;
	private String keesingLastName;
	private Integer flagLastName;
	
	private String userCountry;
	private String keesingCountry;
	private Integer flagCountry;
	
	private Date userDOB;
	private Date keesingDOB;
	private Integer flagDOB;
	
	private String userGender;
	private String keesingGender;
	private Integer flagGender;

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getKeesingFirstName() {
		return keesingFirstName;
	}

	public void setKeesingFirstName(String keesingFirstName) {
		this.keesingFirstName = keesingFirstName;
	}

	public Integer getFlagFirstName() {
		return flagFirstName;
	}

	public void setFlagFirstName(Integer flagFirstName) {
		this.flagFirstName = flagFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getKeesingLastName() {
		return keesingLastName;
	}

	public void setKeesingLastName(String keesingLastName) {
		this.keesingLastName = keesingLastName;
	}

	public Integer getFlagLastName() {
		return flagLastName;
	}

	public void setFlagLastName(Integer flagLastName) {
		this.flagLastName = flagLastName;
	}

	public String getUserCountry() {
		return userCountry;
	}

	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}

	public String getKeesingCountry() {
		return keesingCountry;
	}

	public void setKeesingCountry(String keesingCountry) {
		this.keesingCountry = keesingCountry;
	}

	public Integer getFlagCountry() {
		return flagCountry;
	}

	public void setFlagCountry(Integer flagCountry) {
		this.flagCountry = flagCountry;
	}

	public Date getUserDOB() {
		return userDOB;
	}

	public void setUserDOB(Date userDOB) {
		this.userDOB = userDOB;
	}

	public Date getKeesingDOB() {
		return keesingDOB;
	}

	public void setKeesingDOB(Date keesingDOB) {
		this.keesingDOB = keesingDOB;
	}

	public Integer getFlagDOB() {
		return flagDOB;
	}

	public void setFlagDOB(Integer flagDOB) {
		this.flagDOB = flagDOB;
	}

	public String getUserGender() {
		return userGender;
	}

	public void setUserGender(String userGender) {
		this.userGender = userGender;
	}

	public Integer getFlagGender() {
		return flagGender;
	}

	public void setFlagGender(Integer flagGender) {
		this.flagGender = flagGender;
	}

	public String getKeesingGender() {
		return keesingGender;
	}

	public void setKeesingGender(String keesingGender) {
		this.keesingGender = keesingGender;
	}
	
	public Long getKsResponseId() {
		return ksResponseId;
	}

	public void setKsResponseId(Long ksResponseId) {
		this.ksResponseId = ksResponseId;
	}
	
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "FilesKeesingMatch: " + ls
				+ super.toString() + ls
				+ "ksResponseId: " + ksResponseId + ls
				+ "userFirstName: " + userFirstName + ls
				+ "keesingFirstName: " + keesingFirstName + ls
				+ "flagFirstName: " + flagFirstName + ls
				+ "userLastName: " + userLastName + ls
				+ "keesingLastName: " + keesingLastName + ls				
				+ "flagLastName: " + flagLastName + ls
				+ "userCountry: " + userCountry + ls
				+ "flagCountry: " + flagCountry + ls
				+ "userDOB: " + userDOB + ls
				+ "keesingDOB: " + keesingDOB + ls				
				+ "flagDOB: " + flagDOB + ls
				+ "userGender: " + userGender + ls
				+ "keesingGender: " + keesingGender + ls				
				+ "flagGender: " + flagGender + ls;
	}
}