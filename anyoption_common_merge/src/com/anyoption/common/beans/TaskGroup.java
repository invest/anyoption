package com.anyoption.common.beans;

/**
 * @author LioR SoLoMoN
 *
 */
public class TaskGroup {
	private int id;
	private int taskSubjectsTypesId;
	private String parameters;
	private int priority;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TaskGroup [id=" + id + ", taskSubjectsTypesId="
				+ taskSubjectsTypesId + ", parameters=" + parameters
				+ ", priority=" + priority + "]";
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the taskSubjectsTypesId
	 */
	public int getTaskSubjectsTypesId() {
		return taskSubjectsTypesId;
	}
	/**
	 * @param taskSubjectsTypesId the taskSubjectsTypesId to set
	 */
	public void setTaskSubjectsTypesId(int taskSubjectsTypesId) {
		this.taskSubjectsTypesId = taskSubjectsTypesId;
	}
	/**
	 * @return the parameters
	 */
	public String getParameters() {
		return parameters;
	}
	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}
}
