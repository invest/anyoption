package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Eyal G
 *
 */
public class LimitationDeposits implements Serializable {

	private static final long serialVersionUID = 1L;

    public static final long AFFILATE = 1;
    public static final long CAMPAIGN = 2;
    public static final long COUNTRY = 3;
    public static final long CURRENCY = 4;
    public static final long PAYMENT_RECIPIENT = 5;
    public static final long SKIN = 6;
    public static final long REMARKETING = 7;

	private long id;
    private long affiliateId;
    private String affiliateTxt;
    private long campaignId;
    private String campaignTxt;
    private String comments;
    private long countryId;
    private String countryTxt;
    private long currencyId;
    private String currencyTxt;
    private boolean isActive;
    private long minimumDeposit;
    private long minimumFirstDeposit;
    private long defaultDeposit;
    private long paymentRecipientId;
    private String paymentRecipientTxt;
    private long skinId;
    private String skinTxt;
    private Date timeUpdated;
    private long writerId;
    private String writerTxt;
    private long groupId;
    private boolean isUpdateByGroup;
    private boolean isRemarketing;

    public LimitationDeposits() {
        isUpdateByGroup = true;
    }

    /**
     * @return the affiliateId
     */
    public long getAffiliateId() {
        return affiliateId;
    }

    /**
     * @param affiliateId the affiliateId to set
     */
    public void setAffiliateId(long affiliateId) {
        this.affiliateId = affiliateId;
    }

    /**
     * @return the campaignId
     */
    public long getCampaignId() {
        return campaignId;
    }

    /**
     * @param campaignId the campaignId to set
     */
    public void setCampaignId(long campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the countryId
     */
    public long getCountryId() {
        return countryId;
    }

    /**
     * @param countryId the countryId to set
     */
    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    /**
     * @return the currencyId
     */
    public long getCurrencyId() {
        return currencyId;
    }

    /**
     * @param currencyId the currencyId to set
     */
    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the isActive
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * @param isActive the isActive to set
     */
    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return the minimumDeposit
     */
    public long getMinimumDeposit() {
        return minimumDeposit;
    }

    /**
     * @param minimumDeposit the minimumDeposit to set
     */
    public void setMinimumDeposit(long minimumDeposit) {
        this.minimumDeposit = minimumDeposit;
    }

    /**
     * @return the minimumFirstDeposit
     */
    public long getMinimumFirstDeposit() {
        return minimumFirstDeposit;
    }

    /**
     * @param minimumFirstDeposit the minimumFirstDeposit to set
     */
    public void setMinimumFirstDeposit(long minimumFirstDeposit) {
        this.minimumFirstDeposit = minimumFirstDeposit;
    }

    /**
     * @return
     */
    public long getDefaultDeposit() {
		return defaultDeposit;
	}

	/**
	 * @param defaultDeposit
	 */
	public void setDefaultDeposit(long defaultDeposit) {
		this.defaultDeposit = defaultDeposit;
	}

	/**
     * @return the paymentRecipientId
     */
    public long getPaymentRecipientId() {
        return paymentRecipientId;
    }

    /**
     * @param paymentRecipientId the paymentRecipientId to set
     */
    public void setPaymentRecipientId(long paymentRecipientId) {
        this.paymentRecipientId = paymentRecipientId;
    }

    /**
     * @return the skinId
     */
    public long getSkinId() {
        return skinId;
    }

    /**
     * @param skinId the skinId to set
     */
    public void setSkinId(long skinId) {
        this.skinId = skinId;
    }

    /**
     * @return the writerId
     */
    public long getWriterId() {
        return writerId;
    }

    /**
     * @param writerId the writerId to set
     */
    public void setWriterId(long writerId) {
        this.writerId = writerId;
    }

    /**
     * @return the timeUpdated
     */
    public Date getTimeUpdated() {
        return timeUpdated;
    }

    /**
     * @param timeUpdated the timeUpdated to set
     */
    public void setTimeUpdated(Date timeUpdated) {
        this.timeUpdated = timeUpdated;
    }

    /**
     * @return the affiliateTxt
     */
    public String getAffiliateTxt() {
        return affiliateTxt;
    }

    /**
     * @param affiliateTxt the affiliateTxt to set
     */
    public void setAffiliateTxt(String affiliateTxt) {
        this.affiliateTxt = affiliateTxt;
    }

    /**
     * @return the campaignTxt
     */
    public String getCampaignTxt() {
        return campaignTxt;
    }

    /**
     * @param campaignTxt the campaignTxt to set
     */
    public void setCampaignTxt(String campaignTxt) {
        this.campaignTxt = campaignTxt;
    }

    /**
     * @return the countryTxt
     */
    public String getCountryTxt() {
        return countryTxt;
    }

    /**
     * @param countryTxt the countryTxt to set
     */
    public void setCountryTxt(String countryTxt) {
        this.countryTxt = countryTxt;
    }

    /**
     * @return the currencyTxt
     */
    public String getCurrencyTxt() {
        return currencyTxt;
    }

    /**
     * @param currencyTxt the currencyTxt to set
     */
    public void setCurrencyTxt(String currencyTxt) {
        this.currencyTxt = currencyTxt;
    }

    /**
     * @return the paymentRecipientTxt
     */
    public String getPaymentRecipientTxt() {
        return paymentRecipientTxt;
    }

    /**
     * @param paymentRecipientTxt the paymentRecipientTxt to set
     */
    public void setPaymentRecipientTxt(String paymentRecipientTxt) {
        this.paymentRecipientTxt = paymentRecipientTxt;
    }

    /**
     * @return the skinTxt
     */
    public String getSkinTxt() {
        return skinTxt;
    }

    /**
     * @param skinTxt the skinTxt to set
     */
    public void setSkinTxt(String skinTxt) {
        this.skinTxt = skinTxt;
    }

    /**
     * @return the writerTxt
     */
    public String getWriterTxt() {
        return writerTxt;
    }

    /**
     * @param writerTxt the writerTxt to set
     */
    public void setWriterTxt(String writerTxt) {
        this.writerTxt = writerTxt;
    }

    /**
     * @return the groupId
     */
    public long getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the isUpdateByGroup
     */
    public boolean isUpdateByGroup() {
        return isUpdateByGroup;
    }

    /**
     * @param isUpdateByGroup the isUpdateByGroup to set
     */
    public void setUpdateByGroup(boolean isUpdateByGroup) {
        this.isUpdateByGroup = isUpdateByGroup;
    }

    /**
     * @return the isRemarketing
     */
    public boolean isRemarketing() {
        return isRemarketing;
    }

    /**
     * @param isRemarketing the isRemarketing to set
     */
    public void setRemarketing(boolean isRemarketing) {
        this.isRemarketing = isRemarketing;
    }

}
