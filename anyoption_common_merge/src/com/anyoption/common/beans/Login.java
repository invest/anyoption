package com.anyoption.common.beans;

import java.util.Date;

import org.apache.log4j.Logger;


/**
 * Login bean wrapper
 *
 * @author EyalG
 *
 */
public class Login extends com.anyoption.common.beans.base.Login {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5670488754164775837L;

	private static final Logger log = Logger.getLogger(Login.class);

	private int id;
	private int userId;
	private Date loginTime;
	private String loginOffset;
	private boolean isSuccess;
	private Date logoutTime;	
	private String userAgent;	
	private String deviceUniqueId;
	private long loginFrom;
	private long writerId;
	private String serverId;	
	private String osVersion;
	private String deviceType;
	private String appVersion;
	private long fingerPrint;
	private long deviceFamilyId;
	
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public Date getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
	
	public String getLoginOffset() {
		return loginOffset;
	}
	
	public void setLoginOffset(String loginOffset) {
		this.loginOffset = loginOffset;
	}
	
	public boolean isSuccess() {
		return isSuccess;
	}
	
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	
	public Date getLogoutTime() {
		return logoutTime;
	}
	
	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}
	
	public String getUserAgent() {
		return userAgent;
	}
	
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	
	public String getDeviceUniqueId() {
		return deviceUniqueId;
	}
	
	public void setDeviceUniqueId(String deviceUniqueId) {
		this.deviceUniqueId = deviceUniqueId;
	}
	
	public long getLoginFrom() {
		return loginFrom;
	}

	public void setLoginFrom(long loginFrom) {
		this.loginFrom = loginFrom;
	}
	
	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public String getServerId() {
		return serverId;
	}
	
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public String getOsVersion() {
		return osVersion;
	}
	
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	
	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	public String getAppVersion() {
		return appVersion;
	}
	
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	
	public long getFingerPrint() {
		return fingerPrint;
	}

	public void setFingerPrint(long fingerPrint) {
		this.fingerPrint = fingerPrint;
	}

	public long getDeviceFamilyId() {
		return deviceFamilyId;
	}

	public void setDeviceFamilyId(long deviceFamilyId) {
		this.deviceFamilyId = deviceFamilyId;
	}

	@Override
	public String toString() {
		return "Login [id=" + id + ", userId=" + userId + ", loginTime="
				+ loginTime + ", loginOffset=" + loginOffset + ", isSuccess="
				+ isSuccess + ", logoutTime=" + logoutTime + ", userAgent="
				+ userAgent + ", deviceUniqueId=" + deviceUniqueId
				+ ", loginFrom=" + loginFrom + ", writerId=" + writerId
				+ ", serverId=" + serverId + ", osVersion=" + osVersion
				+ ", deviceType=" + deviceType + ", appVersion=" + appVersion
				+ ", fingerPrint=" + fingerPrint + ", deviceFamilyId="
				+ deviceFamilyId + ", combinationId=" + combinationId
				+ ", dynamicParam=" + dynamicParam + ", affSub1=" + affSub1
				+ ", affSub2=" + affSub2 + ", affSub3=" + affSub3 + "]";
	}
}
