/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;

/**
 * @author pavelt
 *
 */
public class TournamentNumberOfUsers implements Serializable {

	private static final long serialVersionUID = -356804431874687232L;

	private long id;
	private long usersToShow;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUsersToShow() {
		return usersToShow;
	}

	public void setUsersToShow(long usersToShow) {
		this.usersToShow = usersToShow;
	}

	@Override
	public String toString() {
		return "TournamentNumberOfUsers [id=" + id + ", usersToShow="
				+ usersToShow + "]";
	}

}
