/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.anyoption.common.beans.base.BalanceStepPredefValue;
import com.copyop.common.enums.base.UserStateEnum;

/**
 * Bean containing specific user data regarding his balance step
 * 
 * @author kirilim
 */
public class UserActiveData implements Serializable {

	private static final long serialVersionUID = 1802761677128484522L;

	/**
	 * The average of the user's balances after deposit
	 */
	private long avgBalance;

	/**
	 * The average of the user investments
	 */
	private long avgInvestment;

	/**
	 * The custom amount set from the backend
	 */
	private Long customAmount;

	/**
	 * The balance step default amount
	 */
	private long defaultStepAmount;

	/**
	 * The user last investment amount
	 */
	private Long defaultLastInvAmount;

	/**
	 * The balance step set from the backend
	 */
	private Long customStepId;

	/**
	 * The predefined value suggestions for this user
	 */
	private List<BalanceStepPredefValue> predefValues;
	
	/**
	 * The total sum of deposits (only deposits with status 2,7,8) - small amount converted to USD
	 */
	private long sumDeposits;

	private UserStateEnum copyopUserStatus;
	
	private Date statusUpdateDate;

	private int bubblesPricingLevel;
	private long duisId;
	private long userId;

	/**
	 * @return the avgBalance
	 */
	public long getAvgBalance() {
		return avgBalance;
	}

	/**
	 * @param avgBalance the avgBalance to set
	 */
	public void setAvgBalance(long avgBalance) {
		this.avgBalance = avgBalance;
	}

	/**
	 * @return the avgInvestment
	 */
	public long getAvgInvestment() {
		return avgInvestment;
	}

	/**
	 * @param avgInvestment the avgInvestment to set
	 */
	public void setAvgInvestment(long avgInvestment) {
		this.avgInvestment = avgInvestment;
	}

	/**
	 * @return the customAmount
	 */
	public Long getCustomAmount() {
		return customAmount;
	}

	/**
	 * @param customAmount the customAmount to set
	 */
	public void setCustomAmount(Long customAmount) {
		this.customAmount = customAmount;
	}

	/**
	 * @return the defaultStepAmount
	 */
	public long getDefaultStepAmount() {
		return defaultStepAmount;
	}

	/**
	 * @param defaultStepAmount the defaultStepAmount to set
	 */
	public void setDefaultStepAmount(long defaultStepAmount) {
		this.defaultStepAmount = defaultStepAmount;
	}

	/**
	 * @return the defaultLastInvAmount
	 */
	public Long getDefaultLastInvAmount() {
		return defaultLastInvAmount;
	}

	/**
	 * @param defaultLastInvAmount the defaultLastInvAmount to set
	 */
	public void setDefaultLastInvAmount(Long defaultLastInvAmount) {
		this.defaultLastInvAmount = defaultLastInvAmount;
	}

	/**
	 * @return the customStepId
	 */
	public Long getCustomStepId() {
		return customStepId;
	}

	/**
	 * @param customStepId the customStepId to set
	 */
	public void setCustomStepId(Long customStepId) {
		this.customStepId = customStepId;
	}

	/**
	 * @return the predefValues
	 */
	public List<BalanceStepPredefValue> getPredefValues() {
		return predefValues;
	}

	/**
	 * @param predefValues the predefValues to set
	 */
	public void setPredefValues(List<BalanceStepPredefValue> predefValues) {
		this.predefValues = predefValues;
	}

	/**
	 * @return the sumDeposits
	 */
	public long getSumDeposits() {
		return sumDeposits;
	}

	/**
	 * @param sumDeposits the sumDeposits to set
	 */
	public void setSumDeposits(long sumDeposits) {
		this.sumDeposits = sumDeposits;
	}

	public UserStateEnum getCopyopUserStatus() {
		return copyopUserStatus;
	}

	public void setCopyopUserStatus(UserStateEnum copyopUserStatus) {
		this.copyopUserStatus = copyopUserStatus;
	}

	public Date getStatusUpdateDate() {
		return statusUpdateDate;
	}

	public void setStatusUpdateDate(Date statusUpdateDate) {
		this.statusUpdateDate = statusUpdateDate;
	}

	public int getBubblesPricingLevel() {
		return bubblesPricingLevel;
	}

	public void setBubblesPricingLevel(int bubblesPricingLevel) {
		this.bubblesPricingLevel = bubblesPricingLevel;
	}

	/**
	 * @return the duisId
	 */
	public long getDuisId() {
		return duisId;
	}

	/**
	 * @param duisId the duisId to set
	 */
	public void setDuisId(long duisId) {
		this.duisId = duisId;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

}