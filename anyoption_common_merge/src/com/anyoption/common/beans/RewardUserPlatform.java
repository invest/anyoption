package com.anyoption.common.beans;

/**
 * @author liors
 *
 */
public class RewardUserPlatform implements java.io.Serializable {
	private static final long serialVersionUID = -1052477419125469384L;	
	private long id;
	private long userId;
	private int productTypeId;
	private String productTypeName;
	private long turnover;
	private double experiencePoints;
	private int numOfInvestments;
	private int rowType;
	
	public static final int ROW_TYPE_VAL = 0;
	public static final int ROW_TYPE_TOTAL = 1;
		
	/**
	 * 
	 */
	public RewardUserPlatform() {
		this.rowType = ROW_TYPE_VAL;
	}
	
	/**
	 * @param rowType
	 */
	public RewardUserPlatform(int rowType) {
		this.rowType = rowType;
	}
	
	/**
	 * @param id
	 * @param userId
	 * @param productTypeId
	 * @param turnover
	 * @param experiencePoints
	 * @param numOfInvestments
	 */
	public RewardUserPlatform(long id, long userId, int productTypeId,
			long turnover, double experiencePoints, int numOfInvestments) {
		this.id = id;
		this.userId = userId;
		this.productTypeId = productTypeId;
		this.turnover = turnover;
		this.experiencePoints = experiencePoints;
		this.numOfInvestments = numOfInvestments;
	}
	
	@Override
	public String toString() {
		return "RewardUserPlatform [id=" + id + ", userId=" + userId
				+ ", productTypeId=" + productTypeId + ", turnover=" + turnover
				+ ", experiencePoints=" + experiencePoints
				+ ", numOfInvestments=" + numOfInvestments + "]";
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the productTypeId
	 */
	public int getProductTypeId() {
		return productTypeId;
	}
	/**
	 * @param productTypeId the productTypeId to set
	 */
	public void setProductTypeId(int productTypeId) {
		this.productTypeId = productTypeId;
	}
	/**
	 * @return the turnover
	 */
	public long getTurnover() {
		return turnover;
	}
	/**
	 * @param turnover the turnover to set
	 */
	public void setTurnover(long turnover) {
		this.turnover = turnover;
	}
	/**
	 * @return the experiencePoints
	 */
	public double getExperiencePoints() {
		return experiencePoints;
	}
	/**
	 * @param experiencePoints the experiencePoints to set
	 */
	public void setExperiencePoints(double experiencePoints) {
		this.experiencePoints = experiencePoints;
	}
	/**
	 * @return the numOfInvestments
	 */
	public int getNumOfInvestments() {
		return numOfInvestments;
	}
	/**
	 * @param numOfInvestments the numOfInvestments to set
	 */
	public void setNumOfInvestments(int numOfInvestments) {
		this.numOfInvestments = numOfInvestments;
	}
	/**
	 * @return the productTypeName
	 */
	public String getProductTypeName() {
		return productTypeName;
	}
	/**
	 * @param productTypeName the productTypeName to set
	 */
	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}
	/**
	 * @return the rowType
	 */
	public int getRowType() {
		return rowType;
	}
	/**
	 * @param rowType the rowType to set
	 */
	public void setRowType(int rowType) {
		this.rowType = rowType;
	}
	/**
	 * @return
	 */
	public boolean getIsVal() {
		return ROW_TYPE_VAL == rowType;
	}
	/**
	 * @return
	 */
	public boolean getIsTotal() {
		return ROW_TYPE_TOTAL == rowType;
	}	
	
}
