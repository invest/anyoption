package com.anyoption.common.beans;

import java.io.Serializable;

/**
 * @author EranL
 *
 */
public class MarketWidgetData implements Serializable {


	private static final long serialVersionUID = 1565594661360264723L;
	private int pageOddsWin;
	private int pageOddsLose;
	private long timeEstClosingMillsec;
	
	/**
	 * @return the pageOddsWin
	 */
	public int getPageOddsWin() {
		return pageOddsWin;
	}
	/**
	 * @param pageOddsWin the pageOddsWin to set
	 */
	public void setPageOddsWin(int pageOddsWin) {
		this.pageOddsWin = pageOddsWin;
	}
	/**
	 * @return the pageOddsLose
	 */
	public int getPageOddsLose() {
		return pageOddsLose;
	}
	/**
	 * @param pageOddsLose the pageOddsLose to set
	 */
	public void setPageOddsLose(int pageOddsLose) {
		this.pageOddsLose = pageOddsLose;
	}
	/**
	 * @return the timeEstClosingMillsec
	 */
	public long getTimeEstClosingMillsec() {
		return timeEstClosingMillsec;
	}
	/**
	 * @param timeEstClosingMillsec the timeEstClosingMillsec to set
	 */
	public void setTimeEstClosingMillsec(long timeEstClosingMillsec) {
		this.timeEstClosingMillsec = timeEstClosingMillsec;
	}
	
}
