package com.anyoption.common.beans;

import com.anyoption.common.annotations.AnyoptionNoJSON;

public class MailBoxTemplate extends com.anyoption.common.beans.base.MailBoxTemplate {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2843191739214048411L;
	@AnyoptionNoJSON
	private String senderName;
	@AnyoptionNoJSON
	private String typeName;
	@AnyoptionNoJSON
	private String skinName;
	@AnyoptionNoJSON
	private String languageName;
	@AnyoptionNoJSON
	private boolean display;          // Just for Backend use, display the html page

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	
	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public String getSkinName() {
		return skinName;
	}

	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}

	public boolean isDisplay() {
		return display;
	}

	public void setDisplay(boolean display) {
		this.display = display;
	}
}
