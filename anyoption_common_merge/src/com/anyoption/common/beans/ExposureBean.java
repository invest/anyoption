package com.anyoption.common.beans;

/**
 * @author kirilim
 */
public class ExposureBean {

	private Double exposure;
	private Double maxExposure;
	private boolean maxExposureReached;
	private String errorMessage;
	private String errorMessageKey;
	private Object[] errorMessageParams;

	public ExposureBean() {

	}

	public ExposureBean(double exposure, double maxExposure) {
		this.exposure = exposure;
		this.maxExposure = maxExposure * 100;
	}

	public Double getExposure() {
		return exposure;
	}

	public void setExposure(Double exposure) {
		this.exposure = exposure;
	}

	public Double getMaxExposure() {
		return maxExposure;
	}

	public void setMaxExposure(Double maxExposure) {
		this.maxExposure = maxExposure;
	}

	public boolean isMaxExposureReached() {
		return maxExposureReached;
	}

	public void setMaxExposureReached(boolean maxExposureReached) {
		this.maxExposureReached = maxExposureReached;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessageKey() {
		return errorMessageKey;
	}

	public void setErrorMessageKey(String errorMessageKey) {
		this.errorMessageKey = errorMessageKey;
	}

	public Object[] getErrorMessageParams() {
		return errorMessageParams;
	}

	public void setErrorMessageParams(Object[] errorMessageParams) {
		this.errorMessageParams = errorMessageParams;
	}
}