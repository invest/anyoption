package com.anyoption.common.beans;

import java.math.BigDecimal;

/**
 * @author EyalG
 *
 */
public class ServerSidePixelEvent implements java.io.Serializable {
	private static final long serialVersionUID = 8581173331117506564L;
	
	private String duid;
	//publisher unique identifier 
	private String publisherUID;
	private int publisherId;
	private int pixelTypeId;
	private long transactionId;
	private long userId;
	private String pixelUrl;
	private String transactionBaseAmount;
	private String orderId;

	public ServerSidePixelEvent(String duid, String publisherUID,
			int publisherId, int pixelTypeId, long transactionId, long userId,
			String pixelUrl, String orderId) {
		super();
		this.duid = duid;
		this.publisherUID = publisherUID;
		this.publisherId = publisherId;
		this.pixelTypeId = pixelTypeId;
		this.transactionId = transactionId;
		this.userId = userId;
		this.pixelUrl = pixelUrl;
		this.orderId = orderId;
	}

	public String getDuid() {
		return duid;
	}
	
	public void setDuid(String duid) {
		this.duid = duid;
	}
	
	public String getPublisherUID() {
		return publisherUID;
	}
	
	public void setPublisherUID(String publisherUID) {
		this.publisherUID = publisherUID;
	}
	
	
	public int getPublisherId() {
		return publisherId;
	}
	
	public void setPublisherId(int publisherId) {
		this.publisherId = publisherId;
	}
	
	public int getPixelTypeId() {
		return pixelTypeId;
	}
	
	public void setPixelTypeId(int pixelTypeId) {
		this.pixelTypeId = pixelTypeId;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public String getPixelUrl() {
		return pixelUrl;
	}

	public void setPixelUrl(String pixelUrl) {
		this.pixelUrl = pixelUrl;
	}
	
	public String getTransactionBaseAmount() {
		return transactionBaseAmount;
	}

	public void setTransactionBaseAmount(String transactionBaseAmount) {
		this.transactionBaseAmount = transactionBaseAmount;
	}

	@Override
	public String toString() {
		return "ServerSidePixelEvent [duid=" + duid + ", publisherUID="
				+ publisherUID + ", publisherId=" + publisherId
				+ ", pixelTypeId=" + pixelTypeId + ", transactionId="
				+ transactionId + ", userId=" + userId + ", pixelUrl="
				+ pixelUrl + ", transactionBaseAmount=" + transactionBaseAmount
				+ "]";
	}

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}