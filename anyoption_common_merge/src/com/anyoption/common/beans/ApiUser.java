package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author EranL
 *
 */
public class ApiUser extends com.anyoption.common.beans.base.ApiUser implements Serializable {

	private static final long serialVersionUID = 1L; 
	private long id;
	private Date timeCreated;
	private int type;
	private long affiliateCode;
	private boolean isActive;
	private long roleId;
	
	public final static int API_USER_TYPE_TESTER	= 0;
	public final static int API_USER_TYPE_AFFILIATE	= 1;
	public final static int API_USER_TYPE_OTHER		= 2;
	public final static int API_USER_TYPE_EXTERNAL	= 3;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
		
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * @return the affiliateCode
	 */
	public long getAffiliateCode() {
		return affiliateCode;
	}
	/**
	 * @param affiliateCode the affiliateCode to set
	 */
	public void setAffiliateCode(long affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
	    String ls = "\n";
	    return ls + "API user:" + ls
	        + super.toString() + ls
	        + "id: " + id + ls
	        + "userName: " + userName + ls
	        + "password: " + "*****" + ls
	        + "timeCreated: " + timeCreated + ls
	        + "type: " + type + ls
	        + "affiliateCode: " + affiliateCode + ls
	    	+ "isActive: " + isActive + ls;
	}
	
	/**
	 * @return the roleId
	 */
	public long getRoleId() {
		return roleId;
	}
	
	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}
	
}
