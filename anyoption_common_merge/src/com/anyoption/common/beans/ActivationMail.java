package com.anyoption.common.beans;

import java.io.Serializable;

public class ActivationMail implements Serializable {
	
	private long userId;
	private long type;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getType() {
		return type;
	}
	public void setType(long type) {
		this.type = type;
	}
	
	public String toString() {
	    String ls = "\n";
	    return ls + "Activation Mail:" + ls
	        + super.toString() + ls
	        + "UserId: " + userId + ls
	        + "type: " + type + ls;
	}
}
