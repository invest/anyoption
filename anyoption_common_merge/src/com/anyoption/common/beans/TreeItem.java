package com.anyoption.common.beans;

import com.anyoption.common.annotations.AnyoptionNoJSON;



public class TreeItem extends com.anyoption.common.beans.base.TreeItem {

	@AnyoptionNoJSON
	private int marketGroupId;
	@AnyoptionNoJSON
	private int groupPriority;
	@AnyoptionNoJSON
	private int skinId;

    public boolean getIsCommodityOrArabicMarket() {
    	if (super.getMarketId() == 137 ||
    			super.getMarketId() == 20 ||
    					super.getMarketId() == 138 ||
    							super.getMarketId() == 21 ||
    									super.getMarketId() == 248 ||
    											super.getMarketId() == 249 ||
    													super.getMarketId() == 250 ||
    															super.getMarketId() == 371) {
    		return true;
    	} else {
    		return false;
    	}
    }

    /**
	 * @return the groupPriority
	 */
	public int getGroupPriority() {
		return groupPriority;
	}

	/**
	 * @param groupPriority the groupPriority to set
	 */
	public void setGroupPriority(int groupPriority) {
		this.groupPriority = groupPriority;
	}

	/**
	 * @return the marketGroupId
	 */
	public int getMarketGroupId() {
		return marketGroupId;
	}

	/**
	 * @param marketGroupId the marketGroupId to set
	 */
	public void setMarketGroupId(int marketGroupId) {
		this.marketGroupId = marketGroupId;
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}
	
	   /**
	 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
     one time return ArrayList<SelectItem> from DB and second time 
     return ArrayList<TreeItem>
	 */
	public int getGroup_priority() {
		return this.groupPriority;
	}

	   /**
	 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
     one time return ArrayList<SelectItem> from DB and second time 
     return ArrayList<TreeItem>
	 */
	public void setGroup_priority(int group_priority) {
		this.groupPriority = group_priority;
	}
	
	   /**
	 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
     one time return ArrayList<SelectItem> from DB and second time 
     return ArrayList<TreeItem>
	 */
	public int getMarket_group_id() {
		return this.marketGroupId;
	}

	   /**
	 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
     one time return ArrayList<SelectItem> from DB and second time 
     return ArrayList<TreeItem>
	 */
	public void setMarket_group_id(int market_group_id) {
		this.marketGroupId = market_group_id;
	}
	
	   /**
	 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
     one time return ArrayList<SelectItem> from DB and second time 
     return ArrayList<TreeItem>
	 */
	public int getSkin_id() {
		return this.skinId;
	}

	   /**
	 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
     one time return ArrayList<SelectItem> from DB and second time 
     return ArrayList<TreeItem>
	 */
	public void setSkin_id(int skin_id) {
		this.skinId = skin_id;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "TreeItem:" + ls +
            super.toString() + ls +
            "market id = " +super.getMarketId() + ls +
            "market disaply name = " + super.getMarketName() + ls +
            "market group id = " + marketGroupId + ls +
            "market group name = " + super.getGroupName() + ls +
            "market group priority = " + groupPriority + ls +
            "skin id = " + skinId + ls +
            "market subGroup display name = " + super.getMarketSubGroupDisplayName() + ls +
            "printGroup = " + super.isPrintGroup() + ls +
            "pringGeoGroup = " + super.isPrintGeoGroup() + ls;
    }
}