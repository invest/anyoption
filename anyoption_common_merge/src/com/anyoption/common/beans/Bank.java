package com.anyoption.common.beans;

import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.anyoption.common.bl_vos.BankBase;
import com.anyoption.common.managers.BanksManagerBase;

/**
 * Bank bean wrapper
 *
 * @author KobiM
 *
 */
public class Bank extends com.anyoption.common.beans.base.Bank {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5670488754164775837L;

	private static final Logger log = Logger.getLogger(Bank.class);

	private static Hashtable<Long, BankBase> banks;

    public static BankBase getBank(long id) {
        if (null == banks) {
            try {
                banks = BanksManagerBase.getBanks();
            } catch (SQLException sqle) {
                log.error("Can't load banks.", sqle);
            }
        }
        return banks.get(id);
    }
}
