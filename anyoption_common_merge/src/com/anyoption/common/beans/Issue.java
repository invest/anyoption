/**
 * 
 */
package com.anyoption.common.beans;

import java.util.ArrayList;
import java.util.Date;

import com.anyoption.common.managers.IssuesManagerBase;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author kirilim
 *
 */
public class Issue implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7583157461834009623L;
	
	public static final String ISSUE_SUBJECTS_COPYOP_BLOCK = "68";
	public static final String ISSUE_SUBJECTS_COPYOP_REMOVE = "69";
	public static final int ISSUE_REASON_TYPE_BLOCK_COPYOP_USER = 16;
	public static final int ISSUE_REASON_TYPE_REMOVE_COPYOP_USER = 17;
	
	private long id;
	private long userId;
	private String subjectId;
	private String priorityId;
	private String statusId;
	private String username;
	private String skin;
	private String smsStatus;
	private long creditCardId;

	private IssueSubject subject;
	private IssuePriority priority;
	private IssueStatus status;
	private long populationEntryId;

	// Fields that hold the values of the last action
	private Date lastActionTime;
	private String lastActionTimeOffset;
	private boolean lastSignificant;
	private String lastComments;
	private String lastChannelId;
	private String lastReachedStatusId;
	private long lastCallDirectionId;
	private String lastIssueActionType;
	private long issueActionsCount;
	
	private String lastActivityName;

	private IssueChannel lastChannel;
	private IssueReachedStatus lastReachedStatus;
	private IssueReaction lastReaction;

	private ArrayList<IssueAction> issueActionsList;

	private String chat;
	private String chatType;

	private Date timeCreated;
	private int daysInProgress;
	private int daysWithoutTreatment;
	private String country;
	private int expandCollapseRI;
	private int type;
	private String filesRiskStatusId;
	private Date lastReachedCallDate;
	private Date lastCallDate;
	private String riskReasons;
	private long contactId;
	private int componentId;
	private String componentName;
	
	private boolean isMainRegulationIssue;

	public Issue() {
		id=0;
		issueActionsList = new ArrayList<IssueAction>();
		componentId = IssueComponent.ISSUE_COMPONENT_ET_AO;
	}

	/**
	 * Costractor that copys an issue to compare fields that had been changed.
	 */
	public Issue(Issue issueToKeep) {
		id = issueToKeep.id;
		subjectId = new String(issueToKeep.getSubjectId());
		priorityId = new String(issueToKeep.getPriorityId());
		statusId = new String(issueToKeep.getStatusId());
	}
	
	/**
	 * @return the lastChannel
	 */
	public IssueChannel getLastChannel() {
		return lastChannel;
	}

	/**
	 * @param lastChannel the lastChannel to set
	 */
	public void setLastChannel(IssueChannel lastChannel) {
		this.lastChannel = lastChannel;
	}

	/**
	 * @return the lastChannelId
	 */
	public String getLastChannelId() {
		return lastChannelId;
	}

	/**
	 * @param lastChannelId the lastChannelId to set
	 */
	public void setLastChannelId(String lastChannelId) {
		this.lastChannelId = lastChannelId;
	}

	/**
	 * @return the lastComments
	 */
	public String getLastComments() {
		return lastComments;
	}

	/**
	 * @return the lastComments
	 */
	public String getLastCommentsWithBr() {
		return lastComments.replace("\n", "<br/>");
	}

	/**
	 * @param lastComments the lastComments to set
	 */
	public void setLastComments(String lastComments) {
		this.lastComments = lastComments;
	}

	/**
	 * @return the lastReachedStatus
	 */
	public IssueReachedStatus getLastReachedStatus() {
		return lastReachedStatus;
	}

	/**
	 * @param lastReachedStatus the lastReachedStatus to set
	 */
	public void setLastReachedStatus(IssueReachedStatus lastReachedStatus) {
		this.lastReachedStatus = lastReachedStatus;
	}

	/**
	 * @return the lastReachedStatusId
	 */
	public String getLastReachedStatusId() {
		return lastReachedStatusId;
	}

	/**
	 * @param lastReachedStatusId the lastReachedStatusId to set
	 */
	public void setLastReachedStatusId(String lastReachedStatusId) {
		this.lastReachedStatusId = lastReachedStatusId;
	}

	/**
	 * @return the lastReaction
	 */
	public IssueReaction getLastReaction() {
		return lastReaction;
	}

	/**
	 * @param lastReaction the lastReaction to set
	 */
	public void setLastReaction(IssueReaction lastReaction) {
		this.lastReaction = lastReaction;
	}

	/**
	 * @return the lastSignificant
	 */
	public boolean isLastSignificant() {
		return lastSignificant;
	}

	/**
	 * @param lastSignificant the lastSignificant to set
	 */
	public void setLastSignificant(boolean lastSignificant) {
		this.lastSignificant = lastSignificant;
	}

	public IssuePriority getPriority() {
		return priority;
	}
	public void setPriority(IssuePriority priority) {
		this.priority = priority;
	}
	public String getPriorityId() {
		return priorityId;
	}
	public void setPriorityId(String priorityId) {
		this.priorityId = priorityId;
	}

	public IssueStatus getStatus() {
		return status;
	}
	public void setStatus(IssueStatus status) {
		this.status = status;
	}
	public IssueSubject getSubject() {
		return subject;
	}
	public void setSubject(IssueSubject subject) {
		this.subject = subject;
	}

	public String getCommentsLittleThenMax() {
		String temp_comments = lastComments;
		if ( temp_comments == null ) {
			temp_comments = "";
		}
		else if ( temp_comments.length() == 0 ) {
			temp_comments = "";
		}
		else if ( temp_comments.length() > ConstantsBase.MAX_COMMENTS )  {
			temp_comments = temp_comments.substring(0, ConstantsBase.MAX_COMMENTS-1 ) + "...";
		}
		return temp_comments;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "Issue ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "userId = " + this.userId + TAB
	        + "subjectId = " + this.subjectId + TAB
	        + "priorityId = " + this.priorityId + TAB
	        + "statusId = " + this.statusId + TAB
	        + "subject = " + this.subject + TAB
	        + "priority = " + this.priority + TAB
	        + "status = " + this.status + TAB
	        + "populationEntryId = " + this.populationEntryId + TAB
	        + "daysInProgress = " + this.daysInProgress + TAB
	        + "country = " + this.country + TAB
	        + " )";

	    return retValue;
	}

	public String getSkin() {
		return skin;
	}

	public void setSkin(String skin) {
		this.skin = skin;
	}

	/**
	 * @return the actionsList
	 */
	public ArrayList<IssueAction> getIssueActionsList() {
		return issueActionsList;
	}

	/**
	 * @param actionsList the actionsList to set
	 */
	public void setIssueActionsList(ArrayList<IssueAction> issueActionsList) {
		this.issueActionsList = issueActionsList;
	}

	/**
	 * @param actionsList the actionsList to set
	 */
	public void addActionToIssueActionsList(IssueAction issueAction) {
		this.issueActionsList.add(issueAction);
	}

	/**
	 * @return the lastActionTime
	 */
	public Date getLastActionTime() {
		return lastActionTime;
	}

	/**
	 * @param lastActionTime the lastActionTime to set
	 */
	public void setLastActionTime(Date lastActionTime) {
		this.lastActionTime = lastActionTime;
	}

	/**
	 * @return the lastActionTimeOffset
	 */
	public String getLastActionTimeOffset() {
		return lastActionTimeOffset;
	}

	/**
	 * @param lastActionTimeOffset the lastActionTimeOffset to set
	 */
	public void setLastActionTimeOffset(String lastActionTimeOffset) {
		this.lastActionTimeOffset = lastActionTimeOffset;
	}

	/**
	 * @return the lastCallDirectionId
	 */
	public long getLastCallDirectionId() {
		return lastCallDirectionId;
	}

	/**
	 * @param lastCallDirectionId the lastCallDirectionId to set
	 */
	public void setLastCallDirectionId(long lastCallDirectionId) {
		this.lastCallDirectionId = lastCallDirectionId;
	}

	public String getLastCallDirectionName() {
		if (lastCallDirectionId == IssueAction.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL) {
			return "issues.actions.call.direction.out";
		} else if (lastCallDirectionId == IssueAction.ISSUE_DIRECTION_IN_BOUND_PHONE_CALL) {
			return "issues.actions.call.direction.in";
		} else if (lastCallDirectionId == IssueAction.ISSUE_DIRECTION_MEMBER_REQUESTED_PHONE_CALL) {
			return "issues.actions.call.direction.mr";
		}
		return null;
	}
	
	/**
	 * @return the populationEntryId
	 */
	public long getPopulationEntryId() {
		return populationEntryId;
	}

	/**
	 * @param populationEntryId the populationEntryId to set
	 */
	public void setPopulationEntryId(long populationEntryId) {
		this.populationEntryId = populationEntryId;
	}

	public String getHighLightRecord(long populationCurrentEntryId){
		if(subject.getId() == ConstantsBase.ISSUE_SUBJECT_PENDING_DOC){
			return  ConstantsBase.COLOR_ISSUE_PENDING_DOCS;
		}
		if (status.getType() == IssueStatus.ISSUE_STATUS_TYPE_RISK && status.getId() != IssuesManagerBase.RISK_STATUS_CLOSED){//risk issue thats still in progress
			return ConstantsBase.COLOR_RISK_HIGHLIGHT_RECORD;
		} else if (0 != populationEntryId && populationCurrentEntryId == populationEntryId){
			return ConstantsBase.COLOR_HIGHLIGHT_RECORD;
		}	
		return null;
	}

	public String getLastRepForIssue(){
		return issueActionsList.get(0).getWriterName();
	}
	
	/**
	 * @return the lastActivityName
	 */
	public String getLastActivityName() {
		return lastActivityName;
	}

	/**
	 * @param lastActivityName the lastActivityName to set
	 */
	public void setLastActivityName(String lastActivityName) {
		this.lastActivityName = lastActivityName;
	}

	public String getChat() {
		return chat;
	}

	public void setChat(String chat) {
		this.chat = chat;
	}

	/**
	 * @return the lastIssueActionType
	 */
	public String getLastIssueActionType() {
		return lastIssueActionType;
	}

	/**
	 * @param lastIssueActionType the lastIssueActionType to set
	 */
	public void setLastIssueActionType(String lastIssueActionType) {
		this.lastIssueActionType = lastIssueActionType;
	}

	public String getChatType() {
		return chatType;
	}

	public void setChatType(String chatType) {
		this.chatType = chatType;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the daysInProgress
	 */
	public int getDaysInProgress() {
		return daysInProgress;
	}

	/**
	 * @param daysInProgress the daysInProgress to set
	 */
	public void setDaysInProgress(int daysInProgress) {
		this.daysInProgress = daysInProgress;
	}

	/**
	 * @return the daysWithoutTreatment
	 */
	public int getDaysWithoutTreatment() {
		return daysWithoutTreatment;
	}

	/**
	 * @param daysWithoutTreatment the daysWithoutTreatment to set
	 */
	public void setDaysWithoutTreatment(int daysWithoutTreatment) {
		this.daysWithoutTreatment = daysWithoutTreatment;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the expandCollapseRI
	 */
	public int getExpandCollapseRI() {
		return expandCollapseRI;
	}

	/**
	 * @param expandCollapseRI the expandCollapseRI to set
	 */
	public void setExpandCollapseRI(int expandCollapseRI) {
		this.expandCollapseRI = expandCollapseRI;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the filesRiskStatusId
	 */
	public String getFilesRiskStatusId() {
		return filesRiskStatusId;
	}

	/**
	 * @param filesRiskStatusId the filesRiskStatusId to set
	 */
	public void setFilesRiskStatusId(String filesRiskStatusId) {
		this.filesRiskStatusId = filesRiskStatusId;
	}

	/**
	 * @return the lastCallDate
	 */
	public Date getLastCallDate() {
		return lastCallDate;
	}

	/**
	 * @param lastCallDate the lastCallDate to set
	 */
	public void setLastCallDate(Date lastCallDate) {
		this.lastCallDate = lastCallDate;
	}

	/**
	 * @return the lastReachedCallDate
	 */
	public Date getLastReachedCallDate() {
		return lastReachedCallDate;
	}

	/**
	 * @param lastReachedCallDate the lastReachedCallDate to set
	 */
	public void setLastReachedCallDate(Date lastReachedCallDate) {
		this.lastReachedCallDate = lastReachedCallDate;
	}

	/**
	 * @return the riskReasons
	 */
	public String getRiskReasons() {
		return riskReasons;
	}

	/**
	 * @param riskReasons the riskReasons to set
	 */
	public void setRiskReasons(String riskReasons) {
		this.riskReasons = riskReasons;
	}

	public String getSmsStatus(){
		return smsStatus;
	}

	public void setSmsStatus(String smsStatus) {
		this.smsStatus = smsStatus;
	}

	public long getCreditCardId() {
		return creditCardId;
	}

	public void setCreditCardId(long creditCardId) {
		this.creditCardId = creditCardId;
	}

	/**
	 * @return the contactId
	 */
	public long getContactId() {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	/**
	 * @return the componentId
	 */
	public int getComponentId() {
		return componentId;
	}

	/**
	 * @param componentId the componentId to set
	 */
	public void setComponentId(int componentId) {
		this.componentId = componentId;
	}

	/**
	 * @return the componentName
	 */
	public String getComponentName() {
		return componentName;
	}

	/**
	 * @param componentName the componentName to set
	 */
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public boolean isMainRegulationIssue() {
		return isMainRegulationIssue;
	}

	public void setMainRegulationIssue(boolean isMainRegulationIssue) {
		this.isMainRegulationIssue = isMainRegulationIssue;
	}

	public long getIssueActionsCount() {
		return issueActionsCount;
	}

	public void setIssueActionsCount(long issueActionsCount) {
		this.issueActionsCount = issueActionsCount;
	}
		
}