package com.anyoption.common.beans;

import java.util.Date;

import org.apache.myfaces.custom.fileupload.UploadedFile;

import com.anyoption.common.annotations.AnyoptionNoJSON;

public class File extends com.anyoption.common.beans.base.File {

	private static final long serialVersionUID = 1L;
	public static final int REJECT_REASON_NOT_REJECTED = 0;
	
	@AnyoptionNoJSON
	private FileType fileType;
	@AnyoptionNoJSON
	private long id;
	@AnyoptionNoJSON
	private long userId;
	@AnyoptionNoJSON
	private long writerId;
	@AnyoptionNoJSON
	protected Date timeCreated;
	@AnyoptionNoJSON
	protected String utcOffsetCreated;
	@AnyoptionNoJSON
	private String reference;
	@AnyoptionNoJSON
	private boolean isColor;
	@AnyoptionNoJSON
	private boolean isSupportReject;
	@AnyoptionNoJSON
	private boolean isControlReject;
	@AnyoptionNoJSON
	private Integer rejectReason;
	@AnyoptionNoJSON
	private String comment;
	@AnyoptionNoJSON
	private String fileTypeName;
	@AnyoptionNoJSON
	private long uploaderId;
	@AnyoptionNoJSON
	private Date uploadDate;
	@AnyoptionNoJSON
	private String uploaderName;
	@AnyoptionNoJSON
	private UploadedFile file;
	@AnyoptionNoJSON
	private long rejectionWriterId;
	@AnyoptionNoJSON
	private Date rejectionDate;
	@AnyoptionNoJSON
	private String writerName;
	@AnyoptionNoJSON
	private String expYear;
	@AnyoptionNoJSON
	private String expMonth;
	@AnyoptionNoJSON
	private String expDay;
	@AnyoptionNoJSON
	private FilesAdditionalInfo filesAdditionalInfo;

	// Risk Issues
//	protected long fileStatusId;
	@AnyoptionNoJSON
	private Date timeUpdated;
	@AnyoptionNoJSON
	private long issueActionsId;
	@AnyoptionNoJSON
	private long countryId;

	// Files additional info
	@AnyoptionNoJSON
	private String number;
	@AnyoptionNoJSON
	private String firstName;
	@AnyoptionNoJSON
	private String lastName;
	@AnyoptionNoJSON
	protected Date expDate;
	@AnyoptionNoJSON
	private boolean includeInUpdateStatus;
	
	@AnyoptionNoJSON
	private long supportApprovedWriterId;
	@AnyoptionNoJSON
	private Date timeSupportApproved;
	@AnyoptionNoJSON
	private long controlApprovedWriterId;
	@AnyoptionNoJSON
	private Date timeControlApproved;
	@AnyoptionNoJSON
	private long controlRejectWriterId;
	@AnyoptionNoJSON
	private Date timeControlRejected;
	@AnyoptionNoJSON
	private Integer controlRejectReason;
	@AnyoptionNoJSON
	private String controlRejectComment;
	@AnyoptionNoJSON
	private String supportRejectReasonTxt;
	@AnyoptionNoJSON
	private String controlRejectReasonTxt;
	@AnyoptionNoJSON
	private String supportWriter;
	@AnyoptionNoJSON
	private String controlWriter;
	@AnyoptionNoJSON
	private long ksStatusId;
	@AnyoptionNoJSON
	private Date ksStatusDate;
	@AnyoptionNoJSON
	private FilesKeesingMatch filesKeesingMatch;
	@AnyoptionNoJSON
	private FilesKeesingResponse filesKeesingResponse;
	@AnyoptionNoJSON
	private boolean isCurrent;

	public File() {}

	public File(long id, long fileStatusId, long fileTypeId) {
		this.id = id;
		this.fileStatusId = fileStatusId;
		this.setFileTypeId(fileTypeId);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	public boolean isColor() {
		return isColor;
	}

	public void setColor(boolean isColor) {
		this.isColor = isColor;
	}

	public boolean isSupportReject() {
		return isSupportReject;
	}

	public void setSupportReject(boolean isSupportReject) {
		this.isSupportReject = isSupportReject;
	}

	public boolean isControlReject() {
		return isControlReject;
	}

	public void setControlReject(boolean isControlReject) {
		this.isControlReject = isControlReject;
	}

	public long getIssueActionsId() {
		return issueActionsId;
	}

	public void setIssueActionsId(long issueActionsId) {
		this.issueActionsId = issueActionsId;
	}

	public Date getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getExpDate() {
		return expDate;
	}

	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public Integer getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(Integer rejectReason) {
		this.rejectReason = rejectReason;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getFileTypeName() {
		return fileTypeName;
	}

	public void setFileTypeName(String fileTypeName) {
		this.fileTypeName = fileTypeName;
	}

	public long getUploaderId() {
		return uploaderId;
	}

	public void setUploaderId(long uploaderId) {
		this.uploaderId = uploaderId;
	}

	public boolean isNotNeeded() {
		return fileStatusId == STATUS_NOT_NEEDED;
	}

	public void setNotNeeded(boolean notNeeded) {
		if (notNeeded) {
			fileStatusId = STATUS_NOT_NEEDED;
		} else {
			if(fileStatusId == STATUS_NOT_NEEDED){
				fileStatusId = STATUS_REQUESTED;
			}
		}
	}

	public String getUploaderName() {
		return uploaderName;
	}

	public void setUploaderName(String uploaderName) {
		this.uploaderName = uploaderName;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public long getRejectionWriterId() {
		return rejectionWriterId;
	}

	public void setRejectionWriterId(long rejectionWriterId) {
		this.rejectionWriterId = rejectionWriterId;
	}

	public Date getRejectionDate() {
		return rejectionDate;
	}

	public void setRejectionDate(Date rejectionDate) {
		this.rejectionDate = rejectionDate;
	}

	public String getWriterName() {
		return writerName;
	}

	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	public String getExpDay() {
		return expDay;
	}

	public void setExpDay(String expDay) {
		this.expDay = expDay;
	}

	public String getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}

	public String getExpYear() {
		return expYear;
	}

	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}

	public FilesAdditionalInfo getFilesAdditionalInfo() {
		return filesAdditionalInfo;
	}

	public void setFilesAdditionalInfo(FilesAdditionalInfo filesAdditionalInfo) {
		this.filesAdditionalInfo = filesAdditionalInfo;
	}
	
	public boolean getIncludeInUpdateStatus() {
		return includeInUpdateStatus;
	}

	public void setIncludeInUpdateStatus(boolean includeInUpdateStatus) {
		this.includeInUpdateStatus = includeInUpdateStatus;
	}



	public long getSupportApprovedWriterId() {
		return supportApprovedWriterId;
	}

	public void setSupportApprovedWriterId(long supportApprovedWriterId) {
		this.supportApprovedWriterId = supportApprovedWriterId;
	}

	public Date getTimeSupportApproved() {
		return timeSupportApproved;
	}

	public void setTimeSupportApproved(Date timeSupportApproved) {
		this.timeSupportApproved = timeSupportApproved;
	}

	public long getControlApprovedWriterId() {
		return controlApprovedWriterId;
	}

	public void setControlApprovedWriterId(long controlApprovedWriterId) {
		this.controlApprovedWriterId = controlApprovedWriterId;
	}

	public Date getTimeControlApproved() {
		return timeControlApproved;
	}

	public void setTimeControlApproved(Date timeControlApproved) {
		this.timeControlApproved = timeControlApproved;
	}

	public long getControlRejectWriterId() {
		return controlRejectWriterId;
	}

	public void setControlRejectWriterId(long controlRejectWriterId) {
		this.controlRejectWriterId = controlRejectWriterId;
	}

	public Date getTimeControlRejected() {
		return timeControlRejected;
	}

	public void setTimeControlRejected(Date timeControlRejected) {
		this.timeControlRejected = timeControlRejected;
	}

	public Integer getControlRejectReason() {
		return controlRejectReason;
	}

	public void setControlRejectReason(Integer controlRejectReason) {
		this.controlRejectReason = controlRejectReason;
	}

	public String getControlRejectComment() {
		return controlRejectComment;
	}

	public void setControlRejectComment(String controlRejectComment) {
		this.controlRejectComment = controlRejectComment;
	}

	public String getSupportRejectReasonTxt() {
		return supportRejectReasonTxt;
	}

	public void setSupportRejectReasonTxt(String supportRejectReasonTxt) {
		this.supportRejectReasonTxt = supportRejectReasonTxt;
	}

	public String getControlRejectReasonTxt() {
		return controlRejectReasonTxt;
	}

	public void setControlRejectReasonTxt(String controlRejectReasonTxt) {
		this.controlRejectReasonTxt = controlRejectReasonTxt;
	}

	public String getSupportWriter() {
		return supportWriter;
	}

	public void setSupportWriter(String supportWriter) {
		this.supportWriter = supportWriter;
	}

	public String getControlWriter() {
		return controlWriter;
	}

	public void setControlWriter(String controlWriter) {
		this.controlWriter = controlWriter;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}
	
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "com.anyoption.common.beans.File: "
				+ super.toString() + ls
				+ "id:" + id + ls
				+ "fileType: " + fileType + ls
				+ "fileTypeId: " + getFileTypeId() + ls
				+ "userId: " + userId + ls
				+ "writerId: " + writerId + ls
				+ "timeCreated: " + timeCreated + ls
				+ "utcOffsetCreated: " + utcOffsetCreated + ls
				+ "reference: " + reference + ls
				+ "isColor: " + isColor + ls
				+ "isSupportReject: " + isSupportReject + ls
				+ "isControlReject: " + isControlReject + ls
				+ "rejectReason: " + rejectReason + ls
				+ "comment: " + comment + ls
				+ "fileStatusId: " + fileStatusId + ls
				+ "timeUpdated: " + timeUpdated + ls
				+ "issueActionsId: " + issueActionsId + ls
				+ "countryId: " + countryId + ls
				+ "number: " + number + ls
				+ "firstName: " + firstName + ls
				+ "lastName: " + lastName + ls
				+ "expDate: " + expDate + ls
				+ "fileTypeName: " + fileTypeName + ls
				+ "uploaderId: " + uploaderId + ls
				+ "uploadDate: " + uploadDate + ls
				+ "uploaderName: " + uploaderName + ls
				+ "file: " + file + ls
				+ "rejectionWriterId: " + rejectionWriterId + ls
				+ "rejectionDate: " + rejectionDate + ls
				+ "writerName: " + writerName + ls
				+ "expYear: " + expYear + ls
				+ "expMonth: " + expMonth + ls
				+ "expDay: " + expDay + ls
				+ "filesAdditionalInfo: " + filesAdditionalInfo + ls 
				+ "includeInUpdateStatus: " + includeInUpdateStatus + ls
				+ "controlRejectReason: " + controlRejectReason + ls
				+ "controlRejectComment: " + controlRejectComment + ls
				+ "filesKeesingMatch: " + filesKeesingMatch + ls
				+ "filesKeesingResponse: " + filesKeesingResponse + ls;
		
		
	}

	public long getKsStatusId() {
		return ksStatusId;
	}

	public void setKsStatusId(long ksStatusId) {
		this.ksStatusId = ksStatusId;
	}

	public Date getKsStatusDate() {
		return ksStatusDate;
	}

	public void setKsStatusDate(Date ksStatusDate) {
		this.ksStatusDate = ksStatusDate;
	}

	public FilesKeesingMatch getFilesKeesingMatch() {
		return filesKeesingMatch;
	}

	public void setFilesKeesingMatch(FilesKeesingMatch filesKeesingMatch) {
		this.filesKeesingMatch = filesKeesingMatch;
	}

	public FilesKeesingResponse getFilesKeesingResponse() {
		return filesKeesingResponse;
	}

	public void setFilesKeesingResponse(FilesKeesingResponse filesKeesingResponse) {
		this.filesKeesingResponse = filesKeesingResponse;
	}

	public boolean isCurrent() {
		return isCurrent;
	}

	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}
}