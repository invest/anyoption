package com.anyoption.common.beans;

import java.io.Serializable;

import com.anyoption.common.encryption.SecureAlgorithms;

/**
 * @author EyalG
 *
 */

public class EpgTransactionStatus implements Serializable {

	private static final long serialVersionUID = 4117537354187378377L;
	
	private String marchentId;
	/**
	 * all the transaction ids separate by ;
	 */
	private String tranIds;
	private String password;

	public EpgTransactionStatus() {
	}

	public EpgTransactionStatus(String marchentId, String tranIds, String password) {
		super();
		this.marchentId = marchentId;
		this.tranIds = tranIds;
		this.password = password;
	}

	public String getMarchentId() {
		return marchentId;
	}

	public void setMarchentId(String marchentId) {
		this.marchentId = marchentId;
	}

	public String getTranIds() {
		return tranIds;
	}

	public void setTranIds(String tranIds) {
		this.tranIds = tranIds;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void addTransaction(String transactionId) {
		if (tranIds == null) {
			tranIds = transactionId;
		} else {
			tranIds += ";" + transactionId;
		}		
	}
	
	public String getEpgRequestParams() throws Exception {
		return "merchantId=" + marchentId + 
				"&transactions=" + tranIds +
				"&postback=1" +
				"&token=" + SecureAlgorithms.MD5.encode(marchentId + "." + tranIds + "." + password);
	}

	@Override
	public String toString() {
		return "EpgTransactionStatus [marchentId=" + marchentId + ", tranIds="
				+ tranIds + ", password=" + password + "]";
	}
	
	
}
