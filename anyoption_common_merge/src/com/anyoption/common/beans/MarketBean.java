package com.anyoption.common.beans;

/**
 * 
 * @author Ivan Petkov
 *
 */
public class MarketBean implements java.io.Serializable, Cloneable {

	private static final long serialVersionUID = -6548204075614367388L;
	
	protected long id;
	protected String name;
	protected String displayName;
	protected String feedName;
	protected Long timeCreated;
	protected Long writerId;
	protected Long decimalPoint;
	protected Long marketGroupId;
	protected Long exchangeId;
	protected Long investmentLimitGroupId;
	protected Long updatesPause;
	protected Double significantPercentage;
	protected Long realUpdatesPause;
	protected Double realSignificantPercentage;
	protected Double randomCeiling;
	protected Double randomFloor;
	protected Double firstShiftParameter;
	protected Long averageInvestments;
	protected Double percentageOfAvgInvestments;
	protected Long fixedAmountForShifting;
	protected Long typeOfShifting;
	protected Long disableAfterPeriod;
	protected Double currentLevelAlertPerc;
	protected Boolean isSuspended;
	protected String suspendedMessage;
	protected Double acceptableLevelDeviation;
	protected Boolean isHasFutureAgreement;
	protected Boolean isNextAgreementSameDay;
	protected Long bannersPriority;
	protected Long secBetweenInvestments;
	protected Long secForDev2;
	protected String amountForDev2;
	protected Long maxExposure;
	protected Double rollUpQualifyPercent;
	protected Boolean isRollUp; 
	protected Boolean isGoldenMinutes;
	protected Double insurancePremiaPercent;
	protected Double insuranceQualifyPercent;
	protected Double rollUpPremiaPercent;
	protected Double noAutoSettleAfter;
	protected Long shiftingGroupId;
	protected Long optionPlusFee;
	protected Long optionPlusMarketId;
	protected Double acceptableLevelDeviation3;
	protected Long secBetweenInvestmentsIp;
	protected Long amountForDev3;
	protected Long decimalPointSubtractDigits;
	protected Long amountForDev2Night;
	protected Long exposureReachedAmount;
	protected Long raiseExposureUpAmount;
	protected Long raiseExposureDownAmount;
	protected Double raiseCallLowerPutPercent;
	protected Double raisePutLowerCallPercent;
	protected Long secForDev2Mob;
	protected Long secBetweenInvestmentsMob;
	protected Long secBetweenInvestmentsIpMob;
	protected String quoteParams; 
	protected Boolean useForFakeInvestments; 
	protected String assetIndexExpLevelCalc; 
	protected Long bubblesPricingLevel;
	protected Long typeId;
	protected Long worstCaseReturn;
	protected Boolean limitedHighAmount; 
	protected Double instantSpread;
	protected Long limitedFromAmount;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public String getFeedName() {
		return feedName;
	}
	
	public void setFeedName(String feedName) {
		this.feedName = feedName;
	}
	
	public Long getTimeCreated() {
		return timeCreated;
	}
	
	public void setTimeCreated(Long timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	public Long getWriterId() {
		return writerId;
	}
	
	public void setWriterId(Long writerId) {
		this.writerId = writerId;
	}
	
	public Long getDecimalPoint() {
		return decimalPoint;
	}
	
	public void setDecimalPoint(Long decimalPoint) {
		this.decimalPoint = decimalPoint;
	}
	
	public Long getMarketGroupId() {
		return marketGroupId;
	}
	
	public void setMarketGroupId(Long marketGroupId) {
		this.marketGroupId = marketGroupId;
	}
	
	public Long getExchangeId() {
		return exchangeId;
	}
	
	public void setExchangeId(Long exchangeId) {
		this.exchangeId = exchangeId;
	}
	
	public Long getInvestmentLimitGroupId() {
		return investmentLimitGroupId;
	}
	
	public void setInvestmentLimitGroupId(Long investmentLimitGroupId) {
		this.investmentLimitGroupId = investmentLimitGroupId;
	}
	
	public Long getUpdatesPause() {
		return updatesPause;
	}
	
	public void setUpdatesPause(Long updatesPause) {
		this.updatesPause = updatesPause;
	}
	
	public Double getSignificantPercentage() {
		return significantPercentage;
	}
	
	public void setSignificantPercentage(Double significantPercentage) {
		this.significantPercentage = significantPercentage;
	}
	
	public Long getRealUpdatesPause() {
		return realUpdatesPause;
	}
	
	public void setRealUpdatesPause(Long realUpdatesPause) {
		this.realUpdatesPause = realUpdatesPause;
	}
	
	public Double getRealSignificantPercentage() {
		return realSignificantPercentage;
	}
	
	public void setRealSignificantPercentage(Double realSignificantPercentage) {
		this.realSignificantPercentage = realSignificantPercentage;
	}
	
	public Double getRandomCeiling() {
		return randomCeiling;
	}
	
	public void setRandomCeiling(Double randomCeiling) {
		this.randomCeiling = randomCeiling;
	}
	
	public Double getRandomFloor() {
		return randomFloor;
	}
	
	public void setRandomFloor(Double randomFloor) {
		this.randomFloor = randomFloor;
	}
	
	public Double getFirstShiftParameter() {
		return firstShiftParameter;
	}
	
	public void setFirstShiftParameter(Double firstShiftParameter) {
		this.firstShiftParameter = firstShiftParameter;
	}
	
	public Long getAverageInvestments() {
		return averageInvestments;
	}
	
	public void setAverageInvestments(Long averageInvestments) {
		this.averageInvestments = averageInvestments;
	}
	
	public Double getPercentageOfAvgInvestments() {
		return percentageOfAvgInvestments;
	}
	
	public void setPercentageOfAvgInvestments(Double percentageOfAvgInvestments) {
		this.percentageOfAvgInvestments = percentageOfAvgInvestments;
	}
	
	public Long getFixedAmountForShifting() {
		return fixedAmountForShifting;
	}
	
	public void setFixedAmountForShifting(Long fixedAmountForShifting) {
		this.fixedAmountForShifting = fixedAmountForShifting;
	}
	
	public Long getTypeOfShifting() {
		return typeOfShifting;
	}
	
	public void setTypeOfShifting(Long typeOfShifting) {
		this.typeOfShifting = typeOfShifting;
	}
	
	public Long getDisableAfterPeriod() {
		return disableAfterPeriod;
	}
	
	public void setDisableAfterPeriod(Long disableAfterPeriod) {
		this.disableAfterPeriod = disableAfterPeriod;
	}
	
	public Double getCurrentLevelAlertPerc() {
		return currentLevelAlertPerc;
	}
	
	public void setCurrentLevelAlertPerc(Double currentLevelAlertPerc) {
		this.currentLevelAlertPerc = currentLevelAlertPerc;
	}
	
	public Boolean isSuspended() {
		return isSuspended;
	}
	
	public void setSuspended(Boolean isSuspended) {
		this.isSuspended = isSuspended;
	}
	
	public String getSuspendedMessage() {
		return suspendedMessage;
	}
	
	public void setSuspendedMessage(String suspendedMessage) {
		this.suspendedMessage = suspendedMessage;
	}
	
	public Double getAcceptableLevelDeviation() {
		return acceptableLevelDeviation;
	}
	
	public void setAcceptableLevelDeviation(Double acceptableLevelDeviation) {
		this.acceptableLevelDeviation = acceptableLevelDeviation;
	}
	
	public Boolean isHasFutureAgreement() {
		return isHasFutureAgreement;
	}
	
	public void setHasFutureAgreement(Boolean isHasFutureAgreement) {
		this.isHasFutureAgreement = isHasFutureAgreement;
	}
	
	public Boolean isNextAgreementSameDay() {
		return isNextAgreementSameDay;
	}
	
	public void setNextAgreementSameDay(Boolean isNextAgreementSameDay) {
		this.isNextAgreementSameDay = isNextAgreementSameDay;
	}
	
	public Long getBannersPriority() {
		return bannersPriority;
	}
	
	public void setBannersPriority(Long bannersPriority) {
		this.bannersPriority = bannersPriority;
	}
	
	public Long getSecBetweenInvestments() {
		return secBetweenInvestments;
	}
	
	public void setSecBetweenInvestments(Long secBetweenInvestments) {
		this.secBetweenInvestments = secBetweenInvestments;
	}
	
	public Long getSecForDev2() {
		return secForDev2;
	}
	
	public void setSecForDev2(Long secForDev2) {
		this.secForDev2 = secForDev2;
	}
	
	public String getAmountForDev2() {
		return amountForDev2;
	}
	
	public void setAmountForDev2(String amountForDev2) {
		this.amountForDev2 = amountForDev2;
	}
	
	public Long getMaxExposure() {
		return maxExposure;
	}
	
	public void setMaxExposure(Long maxExposure) {
		this.maxExposure = maxExposure;
	}
	
	public Double getRollUpQualifyPercent() {
		return rollUpQualifyPercent;
	}
	
	public void setRollUpQualifyPercent(Double rollUpQualifyPercent) {
		this.rollUpQualifyPercent = rollUpQualifyPercent;
	}
	
	public Boolean getIsRollUp() {
		return isRollUp;
	}
	
	public void setIsRollUp(Boolean isRollUp) {
		this.isRollUp = isRollUp;
	}
	
	public Boolean getIsGoldenMinutes() {
		return isGoldenMinutes;
	}
	
	public void setIsGoldenMinutes(Boolean isGoldenMinutes) {
		this.isGoldenMinutes = isGoldenMinutes;
	}
	
	public Double getInsurancePremiaPercent() {
		return insurancePremiaPercent;
	}
	
	public void setInsurancePremiaPercent(Double insurancePremiaPercent) {
		this.insurancePremiaPercent = insurancePremiaPercent;
	}
	
	public Double getInsuranceQualifyPercent() {
		return insuranceQualifyPercent;
	}
	
	public void setInsuranceQualifyPercent(Double insuranceQualifyPercent) {
		this.insuranceQualifyPercent = insuranceQualifyPercent;
	}
	
	public Double getRollUpPremiaPercent() {
		return rollUpPremiaPercent;
	}
	
	public void setRollUpPremiaPercent(Double rollUpPremiaPercent) {
		this.rollUpPremiaPercent = rollUpPremiaPercent;
	}
	
	public Double getNoAutoSettleAfter() {
		return noAutoSettleAfter;
	}
	
	public void setNoAutoSettleAfter(Double noAutoSettleAfter) {
		this.noAutoSettleAfter = noAutoSettleAfter;
	}
	
	public Long getShiftingGroupId() {
		return shiftingGroupId;
	}
	
	public void setShiftingGroupId(Long shiftingGroupId) {
		this.shiftingGroupId = shiftingGroupId;
	}
	
	public Long getOptionPlusFee() {
		return optionPlusFee;
	}
	
	public void setOptionPlusFee(Long optionPlusFee) {
		this.optionPlusFee = optionPlusFee;
	}
	
	public Long getOptionPlusMarketId() {
		return optionPlusMarketId;
	}
	
	public void setOptionPlusMarketId(Long optionPlusMarketId) {
		this.optionPlusMarketId = optionPlusMarketId;
	}
	
	public Double getAcceptableLevelDeviation3() {
		return acceptableLevelDeviation3;
	}
	
	public void setAcceptableLevelDeviation3(Double acceptableLevelDeviation3) {
		this.acceptableLevelDeviation3 = acceptableLevelDeviation3;
	}
	
	public Long getSecBetweenInvestmentsIp() {
		return secBetweenInvestmentsIp;
	}
	
	public void setSecBetweenInvestmentsIp(Long secBetweenInvestmentsIp) {
		this.secBetweenInvestmentsIp = secBetweenInvestmentsIp;
	}
	
	public Long getAmountForDev3() {
		return amountForDev3;
	}
	
	public void setAmountForDev3(Long amountForDev3) {
		this.amountForDev3 = amountForDev3;
	}
	
	public Long getDecimalPointSubtractDigits() {
		return decimalPointSubtractDigits;
	}
	
	public void setDecimalPointSubtractDigits(Long decimalPointSubtractDigits) {
		this.decimalPointSubtractDigits = decimalPointSubtractDigits;
	}
	
	public Long getAmountForDev2Night() {
		return amountForDev2Night;
	}
	
	public void setAmountForDev2Night(Long amountForDev2Night) {
		this.amountForDev2Night = amountForDev2Night;
	}
	
	public Long getExposureReachedAmount() {
		return exposureReachedAmount;
	}
	
	public void setExposureReachedAmount(Long exposureReachedAmount) {
		this.exposureReachedAmount = exposureReachedAmount;
	}
	
	public Long getRaiseExposureUpAmount() {
		return raiseExposureUpAmount;
	}
	
	public void setRaiseExposureUpAmount(Long raiseExposureUpAmount) {
		this.raiseExposureUpAmount = raiseExposureUpAmount;
	}
	
	public Long getRaiseExposureDownAmount() {
		return raiseExposureDownAmount;
	}
	
	public void setRaiseExposureDownAmount(Long raiseExposureDownAmount) {
		this.raiseExposureDownAmount = raiseExposureDownAmount;
	}
	
	public Double getRaiseCallLowerPutPercent() {
		return raiseCallLowerPutPercent;
	}
	
	public void setRaiseCallLowerPutPercent(Double raiseCallLowerPutPercent) {
		this.raiseCallLowerPutPercent = raiseCallLowerPutPercent;
	}
	
	public Double getRaisePutLowerCallPercent() {
		return raisePutLowerCallPercent;
	}
	
	public void setRaisePutLowerCallPercent(Double raisePutLowerCallPercent) {
		this.raisePutLowerCallPercent = raisePutLowerCallPercent;
	}
	
	public Long getSecForDev2Mob() {
		return secForDev2Mob;
	}
	
	public void setSecForDev2Mob(Long secForDev2Mob) {
		this.secForDev2Mob = secForDev2Mob;
	}
	
	public Long getSecBetweenInvestmentsMob() {
		return secBetweenInvestmentsMob;
	}
	
	public void setSecBetweenInvestmentsMob(Long secBetweenInvestmentsMob) {
		this.secBetweenInvestmentsMob = secBetweenInvestmentsMob;
	}
	
	public Long getSecBetweenInvestmentsIpMob() {
		return secBetweenInvestmentsIpMob;
	}
	
	public void setSecBetweenInvestmentsIpMob(Long secBetweenInvestmentsIpMob) {
		this.secBetweenInvestmentsIpMob = secBetweenInvestmentsIpMob;
	}
	
	public String getQuoteParams() {
		return quoteParams;
	}
	
	public void setQuoteParams(String quoteParams) {
		this.quoteParams = quoteParams;
	}
	
	public Boolean getUseForFakeInvestments() {
		return useForFakeInvestments;
	}
	
	public void setUseForFakeInvestments(Boolean useForFakeInvestments) {
		this.useForFakeInvestments = useForFakeInvestments;
	}
	
	public String getAssetIndexExpLevelCalc() {
		return assetIndexExpLevelCalc;
	}
	
	public void setAssetIndexExpLevelCalc(String assetIndexExpLevelCalc) {
		this.assetIndexExpLevelCalc = assetIndexExpLevelCalc;
	}
	
	public Long getBubblesPricingLevel() {
		return bubblesPricingLevel;
	}
	
	public void setBubblesPricingLevel(Long bubblesPricingLevel) {
		this.bubblesPricingLevel = bubblesPricingLevel;
	}
	
	public Long getTypeId() {
		return typeId;
	}
	
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	
	public Long getWorstCaseReturn() {
		return worstCaseReturn;
	}
	
	public void setWorstCaseReturn(Long worstCaseReturn) {
		this.worstCaseReturn = worstCaseReturn;
	}
	
	public Boolean getLimitedHighAmount() {
		return limitedHighAmount;
	}
	
	public void setLimitedHighAmount(Boolean limitedHighAmount) {
		this.limitedHighAmount = limitedHighAmount;
	}
	
	public Double getInstantSpread() {
		return instantSpread;
	}
	
	public void setInstantSpread(Double instantSpread) {
		this.instantSpread = instantSpread;
	}
	
	public Long getLimitedFromAmount() {
		return limitedFromAmount;
	}
	
	public void setLimitedFromAmount(Long limitedFromAmount) {
		this.limitedFromAmount = limitedFromAmount;
	}
}
