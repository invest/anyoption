package com.anyoption.common.beans;


/**
 * SkinCurrency vo class
 * 
 * @author Kobi
 */
public class SkinCurrency implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7146279256360043775L;
	
	private long id;
	private long skinId;
	private long currencyId;
	private String displayName;
	private double cashToPointsRate;
	private long limitId;

	public SkinCurrency() {
		displayName = "";
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the cashToPointsRate
	 */
	public double getCashToPointsRate() {
		return cashToPointsRate;
	}

	/**
	 * @param cashToPointsRate the cashToPointsRate to set
	 */
	public void setCashToPointsRate(double cashToPointsRate) {
		this.cashToPointsRate = cashToPointsRate;
	}

	public String toString() {
	    String ls = System.getProperty("line.separator");
	    return ls + "SkinCurrency" + ls
	        + super.toString() + ls
	        + "skinId: " + skinId + ls
	        + "currencyId: " + currencyId + ls
	        + "displayName: " + displayName + ls
	        + "cashToPointsRate: " + cashToPointsRate + ls
	        + "limitId: " + limitId + ls;
	}

	/**
	 * @return the limitId
	 */
	public long getLimitId() {
		return limitId;
	}

	/**
	 * @param limitId the limitId to set
	 */
	public void setLimitId(long limitId) {
		this.limitId = limitId;
	}
	
}