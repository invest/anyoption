package com.anyoption.common.beans;

public class WithdrawFee {
	long minAmount;
	float percentage;
	
	public float getPercentage() {
		return percentage;
	}
	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
	public long getMinAmount() {
		return minAmount;
	}
	public void setMinAmount(long minAmount) {
		this.minAmount = minAmount;
	}
	
	@Override
	public String toString() {
		return "WithdrawFee [minAmount=" + minAmount + ", percentage=" + percentage + "]";
	}
}
