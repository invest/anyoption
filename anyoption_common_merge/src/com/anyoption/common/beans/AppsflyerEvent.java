package com.anyoption.common.beans;

import java.util.Date;


/**
 * @author eranl
 *
 */
public class AppsflyerEvent implements java.io.Serializable {

	private static final long serialVersionUID = 162517952857149573L;
	
	private String appsflyerId;
	private String ip;
	private String appId;
	private String devKey;
	private Date timeCreated;
	private double amount;
	private int osTypeId;
	
	/**
	 * @return the appsflyerId
	 */
	public String getAppsflyerId() {
		return appsflyerId;
	}
	/**
	 * @param appsflyerId the appsflyerId to set
	 */
	public void setAppsflyerId(String appsflyerId) {
		this.appsflyerId = appsflyerId;
	}
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	/**
	 * @return the devKey
	 */
	public String getDevKey() {
		return devKey;
	}
	/**
	 * @param devKey the devKey to set
	 */
	public void setDevKey(String devKey) {
		this.devKey = devKey;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the osTypeId
	 */
	public int getOsTypeId() {
		return osTypeId;
	}
	/**
	 * @param osTypeId the osTypeId to set
	 */
	public void setOsTypeId(int osTypeId) {
		this.osTypeId = osTypeId;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AppsflyerEvent [appsflyerId=" + appsflyerId + ", ip=" + ip
				+ ", appId=" + appId + ", devKey=" + devKey + ", timeCreated="
				+ timeCreated + ", amount=" + amount + ", osTypeId=" + osTypeId
				+ "]";
	}

}