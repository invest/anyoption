package com.anyoption.common.beans;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.annotations.AnyoptionNoJSON;
import com.anyoption.common.enums.SkinGroup;

/**
 * @author kirilim
 */
public class Investment extends com.anyoption.common.beans.base.Investment {

	private static final long serialVersionUID = 5739405713539525372L;
	// TODO the logger is needed for the binary 0100 methods, it should be removed when 0100 finally dies
	private static final Logger log = Logger.getLogger(Investment.class);

    public static HashMap<Long, String> TYPE_NAMES = new HashMap<Long, String>();
    public static HashMap<Integer, String> SCHEDULED_NAMES = new HashMap<Integer, String>();
    static {
        TYPE_NAMES.put(new Long(1), "Call");
        TYPE_NAMES.put(new Long(2), "Put");
        TYPE_NAMES.put(new Long(3), "One touch");

        SCHEDULED_NAMES.put(1, "Hourly");
        SCHEDULED_NAMES.put(2, "Daily");
        SCHEDULED_NAMES.put(3, "Weekly");
        SCHEDULED_NAMES.put(4, "Monthly");
        SCHEDULED_NAMES.put(5, "One touch");
        SCHEDULED_NAMES.put(6, "Quarterly");
    }
    // TODO: load that config from db cache it in the manager and send it to flash
    public static final int[] BINARY0100_CONTRACTS_STEPS = {0, 10, 2, 2, 2, 4, 50, 10, 2000, 20, 2, 25, 50, 8}; // Pad with 0 position 0 since currencies IDs start from 1
    public static final int BINARY0100_CONTRACTS_STEPS_COUNT = 10;

	@AnyoptionNoJSON
	protected long userId;
	@AnyoptionNoJSON
	protected long baseAmount;
	@AnyoptionNoJSON
	protected long currencyId;
	@AnyoptionNoJSON
	protected BigDecimal wwwLevel;
	@AnyoptionNoJSON
	protected Double realLevel;
	@AnyoptionNoJSON
	protected long writerId;
	@AnyoptionNoJSON
	protected BigDecimal canceledWriterId;
	@AnyoptionNoJSON
	protected String ip;
	@AnyoptionNoJSON
	protected long win;
	@AnyoptionNoJSON
	protected long lose;
	@AnyoptionNoJSON
	protected long houseResult;
	@AnyoptionNoJSON
	protected int isSettled;
	@AnyoptionNoJSON
	protected Date timeCanceled;
	@AnyoptionNoJSON
	protected int isVoid;
	@AnyoptionNoJSON
	protected int bonusOddsChangeTypeId;
	@AnyoptionNoJSON
	protected String utcOffsetCreated;
	@AnyoptionNoJSON
	protected String utcOffsetSettled;
	@AnyoptionNoJSON
	protected String utcOffsetCancelled;
	@AnyoptionNoJSON
	protected long insuranceAmountGM;
	@AnyoptionNoJSON
	protected long referenceInvestmentId;
	@AnyoptionNoJSON
	protected double rate;
	@AnyoptionNoJSON
	protected Integer insuranceFlag;
	@AnyoptionNoJSON
	protected Integer insuranceFlagAdditional;
	@AnyoptionNoJSON
	protected boolean acceptedSms;
	@AnyoptionNoJSON
	protected long copyOpInvId;
	@AnyoptionNoJSON
	private int copyOpTypeId;
	@AnyoptionNoJSON
	private boolean isLikeHourly;
	@AnyoptionNoJSON
	protected Double closingLevel;
	@AnyoptionNoJSON
	protected long bonusUserId;
	@AnyoptionNoJSON
	protected double insurancePremiaPercent;
	@AnyoptionNoJSON
	protected double rollUpPremiaPercent;
	@AnyoptionNoJSON
	protected long decimalPoint;
	@AnyoptionNoJSON
	protected String typeName;
	@AnyoptionNoJSON
	protected double bonusWinOdds;
	@AnyoptionNoJSON
	protected double bonusLoseOdds;
	@AnyoptionNoJSON
	protected double oppCurrentLevel;
	@AnyoptionNoJSON
	protected long baseWin;
	@AnyoptionNoJSON
	protected long baseLose;
	@AnyoptionNoJSON
	protected int isFree;
	@AnyoptionNoJSON
	protected String utcOffsetEstClosing;
	@AnyoptionNoJSON
	protected String utcOffsetActClosing;
	@AnyoptionNoJSON
	protected Date timeLastInvest;
	@AnyoptionNoJSON
	protected Date timeActClosing;
	@AnyoptionNoJSON
	protected Date timeFirstInvest;
	@AnyoptionNoJSON
	protected boolean selected;
	@AnyoptionNoJSON
	protected String userName;
	@AnyoptionNoJSON
	protected String userFirstName;
	@AnyoptionNoJSON
	protected String userLastName;
	@AnyoptionNoJSON
	protected int totalLine;
	@AnyoptionNoJSON
	protected boolean isPositive;
	@AnyoptionNoJSON
	protected String userCountryCode;
	@AnyoptionNoJSON
	protected int oneTouchDecimalPoint;
	@AnyoptionNoJSON
	protected String skin;
	@AnyoptionNoJSON
	protected double insuranceQualifyPercent;
	@AnyoptionNoJSON
	protected double currentInsuranceQualifyPercent;
	@AnyoptionNoJSON
	protected double insuranceCurrentLevel;
	@AnyoptionNoJSON
	protected boolean showInsurance;
	@AnyoptionNoJSON
	protected int insurancePosition;
	@AnyoptionNoJSON
	protected double roolUpQualifyPercent;
	@AnyoptionNoJSON
	protected int insuranceType; // 1 = GM , 2 = RU
	@AnyoptionNoJSON
	protected double rollUpQualifyPercent;
	@AnyoptionNoJSON
	protected boolean isRollUp;
	@AnyoptionNoJSON
	protected boolean isGoldenMinutes;
	@AnyoptionNoJSON
	protected long rolledInvId;
	@AnyoptionNoJSON
	protected double marketLastDayClosingLevel;
	@AnyoptionNoJSON
	protected boolean isOpenGraph;
	@AnyoptionNoJSON
	protected long sumInvest;// sum of investments for one opportunity (my invest page) for caleculate the bonus amount if its nextInvestOnUs
							// from type 2 or 3
	@AnyoptionNoJSON
	private Date timeQuoted;
	@AnyoptionNoJSON
	private boolean isHaveHourly;
	@AnyoptionNoJSON
	protected long defaultAmountValue;
	@AnyoptionNoJSON
	protected long transactionAmount;
	@AnyoptionNoJSON
	private boolean hasShowOff;
	@AnyoptionNoJSON
	protected int shSen = -1; // show off sentence
	@AnyoptionNoJSON
	private long countryId;
	@AnyoptionNoJSON
	private String showOffName;
	@AnyoptionNoJSON
	private boolean isOptionPlus;
	@AnyoptionNoJSON
	private boolean isDynamics;
	@AnyoptionNoJSON
	private boolean isSeen;// true if this investment was seen in the golden minutes banner
	@AnyoptionNoJSON
	private long showOffId;
	@AnyoptionNoJSON
	private String showOffFacebookText;
	@AnyoptionNoJSON
	private String showOffText;
	@AnyoptionNoJSON
	private String showOffTwitterText;
	@AnyoptionNoJSON
	private Date showOffTime;
	@AnyoptionNoJSON
	protected SpecialCare specialCare;
	@AnyoptionNoJSON
	private String lastShowOffMessegeKey;
	@AnyoptionNoJSON
	protected String writerUserName;
	@AnyoptionNoJSON
	protected long investmentsCount; // for binary0100
	@AnyoptionNoJSON
	protected long selectedInvestmentsCount; // for binary0100
	@AnyoptionNoJSON
	protected int contractsStep; // for binary0100
	@AnyoptionNoJSON
	protected String countryName; // Ao live
	@AnyoptionNoJSON
	private String nameOpportunityType; // Ao live
	@AnyoptionNoJSON
	protected long profitUser; // Ao live
	@AnyoptionNoJSON
	private String cityName; // Ao live
	@AnyoptionNoJSON
	private int fromGraph;
	@AnyoptionNoJSON
	protected long userBalance; // for daily screen
	@AnyoptionNoJSON
	private boolean isHaveReutersQuote;
	@AnyoptionNoJSON
	protected Map<SkinGroup, String> insuranceCurrentLevels;
	@AnyoptionNoJSON
	private String apiExternalUserReference;

	public Investment() {

	}

	public Investment(int totalLine2) {
		totalLine = totalLine2;
		specialCare = null;
	}

//    public boolean isUpInvestment() {
//        return oneTouchUpDown == INVESTMENT_ONE_TOUCH_UP;
//    }

//    public boolean isDownInvestment() {
//        return oneTouchUpDown == INVESTMENT_ONE_TOUCH_DOWN;
//    }

//    public boolean getIsOneTouchInv() {
//    	return typeId == INVESTMENT_TYPE_ONE;
//    }

//	public boolean getIsCallOption() {
//		return typeId == INVESTMENT_TYPE_CALL;
//	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Double getRealLevel() {
		return realLevel;
	}

	public void setRealLevel(Double realLevel) {
		this.realLevel = realLevel;
	}

	public BigDecimal getWwwLevel() {
		return wwwLevel;
	}

	public void setWwwLevel(BigDecimal wwwLevel) {
		this.wwwLevel = wwwLevel;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public BigDecimal getCanceledWriterId() {
		return canceledWriterId;
	}

	public void setCanceledWriterId(BigDecimal canceledWriterId) {
		this.canceledWriterId = canceledWriterId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public long getWin() {
		return win;
	}

	public void setWin(long win) {
		this.win = win;
	}

	public long getLose() {
		return lose;
	}

	public void setLose(long lose) {
		this.lose = lose;
	}

	public long getHouseResult() {
		return houseResult;
	}

	public void setHouseResult(long houseResult) {
		this.houseResult = houseResult;
	}

	public int getIsSettled() {
		return isSettled;
	}

	public void setIsSettled(int isSettled) {
		this.isSettled = isSettled;
	}

	public Date getTimeCanceled() {
		return timeCanceled;
	}

	public void setTimeCanceled(Date timeCanceled) {
		this.timeCanceled = timeCanceled;
	}

	public int getIsVoid() {
		return isVoid;
	}

	public void setIsVoid(int isVoid) {
		this.isVoid = isVoid;
	}

	public int getBonusOddsChangeTypeId() {
		return bonusOddsChangeTypeId;
	}

	public void setBonusOddsChangeTypeId(int bonusOddsChangeTypeId) {
		this.bonusOddsChangeTypeId = bonusOddsChangeTypeId;
	}

	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	public String getUtcOffsetSettled() {
		return utcOffsetSettled;
	}

	public void setUtcOffsetSettled(String utcOffsetSettled) {
		this.utcOffsetSettled = utcOffsetSettled;
	}

	public String getUtcOffsetCancelled() {
		return utcOffsetCancelled;
	}

	public void setUtcOffsetCancelled(String utcOffsetCancelled) {
		this.utcOffsetCancelled = utcOffsetCancelled;
	}

	public long getInsuranceAmountGM() {
		return insuranceAmountGM;
	}

	public void setInsuranceAmountGM(long insuranceAmountGM) {
		this.insuranceAmountGM = insuranceAmountGM;
	}

	public long getReferenceInvestmentId() {
		return referenceInvestmentId;
	}

	public void setReferenceInvestmentId(long referenceInvestmentId) {
		this.referenceInvestmentId = referenceInvestmentId;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public Integer getInsuranceFlag() {
		return insuranceFlag;
	}

	public void setInsuranceFlag(Integer insuranceFlag) {
		this.insuranceFlag = insuranceFlag;
	}

	public boolean isAcceptedSms() {
		return acceptedSms;
	}

	public void setAcceptedSms(boolean acceptedSms) {
		this.acceptedSms = acceptedSms;
	}

	public long getCopyopInvId() {
		return copyOpInvId;
	}

	public void setCopyOpInvId(long copyOpInvId) {
		this.copyOpInvId = copyOpInvId;
	}

	public int getCopyOpTypeId() {
		return copyOpTypeId;
	}

	public void setCopyOpTypeId(int copyOpTypeId) {
		this.copyOpTypeId = copyOpTypeId;
	}

	public boolean isLikeHourly() {
		return isLikeHourly;
	}

	public void setLikeHourly(boolean isLikeHourly) {
		this.isLikeHourly = isLikeHourly;
	}

	public Double getClosingLevel() {
		return closingLevel;
	}

	public void setClosingLevel(Double closingLevel) {
		this.closingLevel = closingLevel;
	}

	public long getBonusUserId() {
		return bonusUserId;
	}

	public void setBonusUserId(long bonusUserId) {
		this.bonusUserId = bonusUserId;
	}

	public double getInsurancePremiaPercent() {
		return insurancePremiaPercent;
	}

	public void setInsurancePremiaPercent(double insurancePremiaPercent) {
		this.insurancePremiaPercent = insurancePremiaPercent;
	}

	public double getRollUpPremiaPercent() {
		return rollUpPremiaPercent;
	}

	public void setRollUpPremiaPercent(double rollUpPremiaPercent) {
		this.rollUpPremiaPercent = rollUpPremiaPercent;
	}

	public long getDecimalPoint() {
		return decimalPoint;
	}

	public void setDecimalPoint(long decimalPoint) {
		this.decimalPoint = decimalPoint;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public double getBonusLoseOdds() {
		return bonusLoseOdds;
	}

	public void setBonusLoseOdds(double bonusLoseOdds) {
		this.bonusLoseOdds = bonusLoseOdds;
	}

	public double getBonusWinOdds() {
		return bonusWinOdds;
	}

	public void setBonusWinOdds(double bonusWinOdds) {
		this.bonusWinOdds = bonusWinOdds;
	}

	public double getOppCurrentLevel() {
		return oppCurrentLevel;
	}

	public void setOppCurrentLevel(double oppCurrentLevel) {
		this.oppCurrentLevel = oppCurrentLevel;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public long getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(long baseAmount) {
		this.baseAmount = baseAmount;
	}

	public long getBaseLose() {
		return baseLose;
	}

	public void setBaseLose(long baseLose) {
		this.baseLose = baseLose;
	}

	public long getBaseWin() {
		return baseWin;
	}

	public void setBaseWin(long baseWin) {
		this.baseWin = baseWin;
	}

	public int getIsFree() {
		return isFree;
	}

	public void setIsFree(int isFree) {
		this.isFree = isFree;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Date getTimeActClosing() {
		return timeActClosing;
	}

	public void setTimeActClosing(Date timeActClosing) {
		this.timeActClosing = timeActClosing;
	}

	public Date getTimeLastInvest() {
		return timeLastInvest;
	}

	public void setTimeLastInvest(Date timeLastInvest) {
		this.timeLastInvest = timeLastInvest;
	}

	public Date getTimeFirstInvest() {
		return timeFirstInvest;
	}

	public void setTimeFirstInvest(Date timeFirstInvest) {
		this.timeFirstInvest = timeFirstInvest;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getTotalLine() {
		return totalLine;
	}

	public void setTotalLine(int totalLine) {
		this.totalLine = totalLine;
	}

	public String getUtcOffsetActClosing() {
		return utcOffsetActClosing;
	}

	public void setUtcOffsetActClosing(String utcOffsetActClosing) {
		this.utcOffsetActClosing = utcOffsetActClosing;
	}

	public String getUtcOffsetEstClosing() {
		return utcOffsetEstClosing;
	}

	public void setUtcOffsetEstClosing(String utcOffsetEstClosing) {
		this.utcOffsetEstClosing = utcOffsetEstClosing;
	}

	public String getUserCountryCode() {
		return userCountryCode;
	}

	public void setUserCountryCode(String userCountryCode) {
		this.userCountryCode = userCountryCode;
	}

	public int getOneTouchDecimalPoint() {
		return oneTouchDecimalPoint;
	}

	public void setOneTouchDecimalPoint(int oneTouchDecimalPoint) {
		this.oneTouchDecimalPoint = oneTouchDecimalPoint;
	}

	public String getSkin() {
		return skin;
	}

	public void setSkin(String skin) {
		this.skin = skin;
	}

	public double getCurrentInsuranceQualifyPercent() {
		return currentInsuranceQualifyPercent;
	}

	public void setCurrentInsuranceQualifyPercent(double currentInsuranceQualifyPercent) {
		this.currentInsuranceQualifyPercent = currentInsuranceQualifyPercent;
	}

	public double getInsuranceQualifyPercent() {
		return insuranceQualifyPercent;
	}

	public void setInsuranceQualifyPercent(double insuranceQualifyPercent) {
		this.insuranceQualifyPercent = insuranceQualifyPercent;
	}

	public double getInsuranceCurrentLevel() {
		return insuranceCurrentLevel;
	}

	public void setInsuranceCurrentLevel(double insuranceCurrentLevel) {
		this.insuranceCurrentLevel = insuranceCurrentLevel;
	}

	public boolean isShowInsurance() {
		return showInsurance;
	}

	public void setShowInsurance(boolean showInsurance) {
		this.showInsurance = showInsurance;
	}

	public int getInsurancePosition() {
		return insurancePosition;
	}

	public void setInsurancePosition(int insurancePosition) {
		this.insurancePosition = insurancePosition;
	}

	public double getRoolUpQualifyPercent() {
		return roolUpQualifyPercent;
	}

	public void setRoolUpQualifyPercent(double roolUpQualifyPercent) {
		this.roolUpQualifyPercent = roolUpQualifyPercent;
	}

	public int getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(int insuranceType) {
		this.insuranceType = insuranceType;
	}

	public boolean isGoldenMinutes() {
		return isGoldenMinutes;
	}

	public void setGoldenMinutes(boolean isGoldenMinutes) {
		this.isGoldenMinutes = isGoldenMinutes;
	}

	public boolean isRollUp() {
		return isRollUp;
	}

	public void setRollUp(boolean isRollUp) {
		this.isRollUp = isRollUp;
	}

	public double getRollUpQualifyPercent() {
		return rollUpQualifyPercent;
	}

	public void setRollUpQualifyPercent(double rollUpQualifyPercent) {
		this.rollUpQualifyPercent = rollUpQualifyPercent;
	}

	public double getMarketLastDayClosingLevel() {
		return marketLastDayClosingLevel;
	}

	public void setMarketLastDayClosingLevel(double marketLastDayClosingLevel) {
		this.marketLastDayClosingLevel = marketLastDayClosingLevel;
	}

	public long getSumInvest() {
		return sumInvest;
	}

	public void setSumInvest(long sumInvest) {
		this.sumInvest = sumInvest;
	}

	public Date getTimeQuoted() {
		return timeQuoted;
	}

	public void setTimeQuoted(Date timeQuoted) {
		this.timeQuoted = timeQuoted;
	}

	public boolean isHaveHourly() {
		return isHaveHourly;
	}

	public void setHaveHourly(boolean isHaveHourly) {
		this.isHaveHourly = isHaveHourly;
	}

	public boolean isPositive() {
		return isPositive;
	}

	public void setPositive(boolean isPositive) {
		this.isPositive = isPositive;
	}

	public long getRolledInvId() {
		return rolledInvId;
	}

	public void setRolledInvId(long rolledInvId) {
		this.rolledInvId = rolledInvId;
	}

	public boolean isOpenGraph() {
		return isOpenGraph;
	}

	public void setOpenGraph(boolean isOpenGraph) {
		this.isOpenGraph = isOpenGraph;
	}

	public long getDefaultAmountValue() {
		return defaultAmountValue;
	}

	public void setDefaultAmountValue(long defaultAmountValue) {
		this.defaultAmountValue = defaultAmountValue;
	}

	public long getCopyOpInvId() {
		return copyOpInvId;
	}

	public long getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(long transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public boolean isHasShowOff() {
		return hasShowOff;
	}

	public void setHasShowOff(boolean hasShowOff) {
		this.hasShowOff = hasShowOff;
	}

	public int getShSen() {
		return shSen;
	}

	public void setShSen(int shSen) {
		this.shSen = shSen;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public String getShowOffName() {
		return showOffName;
	}

	public void setShowOffName(String showOffName) {
		this.showOffName = showOffName;
	}

	public boolean isOptionPlus() {
		return isOptionPlus;
	}

	public void setOptionPlus(boolean isOptionPlus) {
		this.isOptionPlus = isOptionPlus;
	}
	
	public boolean isDynamics() {
		return isDynamics;
	}

	public void setDynamics(boolean isDynamics) {
		this.isDynamics = isDynamics;
	}

	public boolean isSeen() {
		return isSeen;
	}

	public void setSeen(boolean isSeen) {
		this.isSeen = isSeen;
	}

	public long getShowOffId() {
		return showOffId;
	}

	public void setShowOffId(long showOffId) {
		this.showOffId = showOffId;
	}

	public void setShowOffFacebookText(String showOffFacebookText) {
		this.showOffFacebookText = showOffFacebookText;
	}

	public String getShowOffText() {
		return showOffText;
	}

	public void setShowOffText(String showOffText) {
		this.showOffText = showOffText;
	}

	public String getShowOffTwitterText() {
		return showOffTwitterText;
	}

	public void setShowOffTwitterText(String showOffTwitterText) {
		this.showOffTwitterText = showOffTwitterText;
	}

	public Date getShowOffTime() {
		return showOffTime;
	}

	public void setShowOffTime(Date showOffTime) {
		this.showOffTime = showOffTime;
	}

	public String getShowOffFacebookText() {
		return showOffFacebookText;
	}

	public SpecialCare getSpecialCare() {
		return specialCare;
	}

	public void setSpecialCare(SpecialCare specialCare) {
		this.specialCare = specialCare;
	}

	public String getLastShowOffMessegeKey() {
		return lastShowOffMessegeKey;
	}

	public void setLastShowOffMessegeKey(String lastShowOffMessegeKey) {
		this.lastShowOffMessegeKey = lastShowOffMessegeKey;
	}

	public String getWriterUserName() {
		return writerUserName;
	}

	public void setWriterUserName(String writerUserName) {
		this.writerUserName = writerUserName;
	}

	public long getInvestmentsCount() {
		return investmentsCount;
	}

	public void setInvestmentsCount(long investmentsCount) {
		this.investmentsCount = investmentsCount;
	}

	public long getSelectedInvestmentsCount() {
		return selectedInvestmentsCount;
	}

	public void setSelectedInvestmentsCount(long selectedInvestmentsCount) {
		this.selectedInvestmentsCount = selectedInvestmentsCount;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getNameOpportunityType() {
		return nameOpportunityType;
	}

	public void setNameOpportunityType(String nameOpportunityType) {
		this.nameOpportunityType = nameOpportunityType;
	}

	public long getProfitUser() {
		return profitUser;
	}

	public void setProfitUser(long profitUser) {
		this.profitUser = profitUser;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public int getContractsStep() {
        return contractsStep;
    }

    public void setContractsStep(int contractsStep) {
        this.contractsStep = contractsStep;
    }

	public int getFromGraph() {
		return fromGraph;
	}

	public void setFromGraph(int fromGraph) {
		this.fromGraph = fromGraph;
	}

    public long getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(long userBalance) {
        this.userBalance = userBalance;
    }

	public boolean isHaveReutersQuote() {
		return isHaveReutersQuote;
	}

	public void setHaveReutersQuote(boolean isHaveReutersQuote) {
		this.isHaveReutersQuote = isHaveReutersQuote;
	}

	public Map<SkinGroup, String> getInsuranceCurrentLevels() {
		return insuranceCurrentLevels;
	}

	public void setInsuranceCurrentLevels(Map<SkinGroup, String> insuranceCurrentLevels) {
		this.insuranceCurrentLevels = insuranceCurrentLevels;
	}

	public String getApiExternalUserReference() {
		return apiExternalUserReference;
	}

	public void setApiExternalUserReference(String apiExternalUserReference) {
		this.apiExternalUserReference = apiExternalUserReference;
	}

    public double getCurrentLevelValue() {
        return currentLevel;
    }
    
    public Integer getInsuranceFlagAdditional() {
		return insuranceFlagAdditional;
	}

	public void setInsuranceFlagAdditional(Integer insuranceFlagAdditional) {
		this.insuranceFlagAdditional = insuranceFlagAdditional;
	}

	public String toString() {
    	String ls = System.getProperty("line.separator");
		return ls + "com.anyoption.common.beans.Investment: " + ls
				+ super.toString() + ls
				+ "userId: " + userId + ls
				+ "baseAmount: " + baseAmount + ls
				+ "currencyId: " + currencyId + ls
				+ "wwwLevel: " + wwwLevel + ls
				+ "realLevel: " + realLevel + ls
				+ "writerId: " + writerId + ls
				+ "canceledWriterId: " + canceledWriterId + ls
				+ "ip: " + ip + ls
				+ "win: " + win + ls
				+ "lose: " + lose + ls
				+ "houseResult: " + houseResult + ls
				+ "isSettled: " + isSettled + ls
				+ "timeCanceled: " + timeCanceled + ls
				+ "isVoid: " + isVoid + ls
				+ "bonusOddsChangeTypeId: " + bonusOddsChangeTypeId + ls
				+ "utcOffsetCreated: " + utcOffsetCreated + ls
				+ "utcOffsetSettled: " + utcOffsetSettled + ls
				+ "utcOffsetCancelled: " + utcOffsetCancelled + ls
				+ "insuranceAmountGM: " + insuranceAmountGM + ls
				+ "referenceInvestmentId: " + referenceInvestmentId + ls
				+ "rate: " + rate + ls
				+ "insuranceFlag: " + insuranceFlag + ls
				+ "insuranceFlagAdditional: " + insuranceFlagAdditional + ls				
				+ "acceptedSms: " + acceptedSms + ls
				+ "copyOpInvId: " + copyOpInvId + ls
				+ "copyOpTypeId: " + copyOpTypeId + ls
				+ "isLikeHourly: " + isLikeHourly + ls
				+ "closingLevel: " + closingLevel + ls
				+ "bonusUserId: " + bonusUserId + ls
				+ "insurancePremiaPercent: " + insurancePremiaPercent + ls
				+ "rollUpPremiaPercent: " + rollUpPremiaPercent + ls
				+ "decimalPoint: " + decimalPoint + ls
				+ "typeName: " + typeName + ls
				+ "bonusWinOdds: " + bonusWinOdds + ls
				+ "bonusLoseOdds: " + bonusLoseOdds + ls
				+ "oppCurrentLevel: " + oppCurrentLevel + ls
				+ "scheduledId: " + scheduledId + ls
				+ "baseWin: " + baseWin + ls
				+ "baseLose: " + baseLose + ls
				+ "isFree: " + isFree + ls
				+ "utcOffsetEstClosing: " + utcOffsetEstClosing + ls
				+ "utcOffsetActClosing: " + utcOffsetActClosing + ls
				+ "timeLastInvest: " + timeLastInvest + ls
				+ "timeActClosing: " + timeActClosing + ls
				+ "timeFirstInvest: " + timeFirstInvest + ls
				+ "selected: " + selected + ls
				+ "userName: " + userName + ls
				+ "userFirstName: " + userFirstName + ls
				+ "userLastName: " + userLastName + ls
				+ "totalLine: " + totalLine + ls
				+ "isPositive: " + isPositive + ls
				+ "userCountryCode: " + userCountryCode + ls
				+ "oneTouchDecimalPoint: " + oneTouchDecimalPoint + ls
				+ "skin: " + skin + ls
				+ "insuranceQualifyPercent: " + insuranceQualifyPercent + ls
				+ "currentInsuranceQualifyPercent: " + currentInsuranceQualifyPercent + ls
				+ "insuranceCurrentLevel: " + insuranceCurrentLevel + ls
				+ "showInsurance: " + showInsurance + ls
				+ "insurancePosition: " + insurancePosition + ls
				+ "roolUpQualifyPercent: " + roolUpQualifyPercent + ls
				+ "insuranceType: " + insuranceType + ls
				+ "rollUpQualifyPercent: " + rollUpQualifyPercent + ls
				+ "isRollUp: " + isRollUp + ls
				+ "isGoldenMinutes: " + isGoldenMinutes + ls
				+ "rolledInvId: " + rolledInvId + ls
				+ "marketLastDayClosingLevel: " + marketLastDayClosingLevel + ls
				+ "isOpenGraph: " + isOpenGraph + ls
				+ "sumInvest: " + sumInvest + ls
				+ "timeQuoted: " + timeQuoted + ls
				+ "isHaveHourly: " + isHaveHourly + ls
				+ "defaultAmountValue: " + defaultAmountValue + ls
				+ "transactionAmount: " + transactionAmount + ls
				+ "hasShowOff: " + hasShowOff + ls
				+ "shSen: " + shSen + ls
				+ "countryId: " + countryId + ls
				+ "showOffName: " + showOffName + ls
				+ "isOptionPlus: " + isOptionPlus + ls
				+ "isSeen: " + isSeen + ls
				+ "showOffId: " + showOffId + ls
				+ "showOffFacebookText: " + showOffFacebookText + ls
				+ "showOffText: " + showOffText + ls
				+ "showOffTwitterText: " + showOffTwitterText + ls
				+ "showOffTime: " + showOffTime + ls
				+ "specialCare: " + specialCare + ls
				+ "lastShowOffMessegeKey: " + lastShowOffMessegeKey + ls
				+ "writerUserName: " + writerUserName + ls
				+ "investmentsCount: " + investmentsCount + ls
				+ "selectedInvestmentsCount: " + selectedInvestmentsCount + ls
				+ "contractsStep: " + contractsStep + ls
				+ "countryName: " + countryName + ls
				+ "nameOpportunityType: " + nameOpportunityType + ls
				+ "profitUser: " + profitUser + ls 
				+ "cityName: " + cityName + ls 
				+ "fromGraph: " + fromGraph + ls
				+ "userBalance: " + userBalance + ls
				+ "isHaveReutersQuote: " + isHaveReutersQuote + ls
				+ "insuranceCurrentLevels: " + insuranceCurrentLevels + ls
				+ "apiExternalUserReference: " + apiExternalUserReference + ls;
    }

	// TODO remove this method when 0100 is dead
	public String getSelectedInvestmentsCountTxt() {
		return String.valueOf(selectedInvestmentsCount);
	}

	// TODO remove this method when 0100 is dead
	public void setSelectedInvestmentsCountTxt(String selectedInvestmentsCount) {
		try {
			this.selectedInvestmentsCount = Long.parseLong(selectedInvestmentsCount);
		} catch (Exception e) {
			log.error("Can't set selectedInvestmentsCount.", e);
			this.selectedInvestmentsCount = 1;
		}
	}

	// TODO remove this method when 0100 is dead
	public void setSelectedAndInvestmentsCount(long selectedInvestmentsCount) {
		this.selectedInvestmentsCount = selectedInvestmentsCount;
		this.investmentsCount = selectedInvestmentsCount;
	}
}