package com.anyoption.common.beans;

//import il.co.etrader.util.CommonUtil;

import java.util.Date;

/**
 * Opportunity type VO.
 *
 * @author Tony
 */
public class OpportunityType implements java.io.Serializable {
	
	private static final long serialVersionUID = -4276653773175976464L;
	
	private long id;
    private long maxExposure;
    private int isRunWithMarket;
    private int isUseMargin;
    private float marginPercentageUp;
    private float marginPercentageDown;
    private Date timeCreated;
    private String description;
    private long writerId;
    private long productTypeId;

    public static final int PRODUCT_TYPE_BINARY = 1;
    public static final int PRODUCT_TYPE_ONE_TOUCH = 2;
    public static final int PRODUCT_TYPE_OPTION_PLUS = 3;
    public static final int PRODUCT_TYPE_BINARY_0_100 = 4;
    public static final int PRODUCT_TYPE_BUBBLES = 5;
    public static final int PRODUCT_TYPE_DYNAMICS = 6;
    
    public boolean getRunWithMarketBoolean() {
    	return isRunWithMarket==1;
    }
    /**
     * @return Returns the id.
     */
    public long getId() {
        return id;
    }

    /**
     * @param id The id to set.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return Returns the maxExposure.
     */
    public long getMaxExposure() {
        return maxExposure;
    }

    /**
     * @param maxExposure The maxExposure to set.
     */
    public void setMaxExposure(long maxExposure) {
        this.maxExposure = maxExposure;
    }

    /**
     * @return Returns the isRunWithMarket.
     */
    public int getIsRunWithMarket() {
        return isRunWithMarket;
    }

    /**
     * @param isRunWithMarket The isRunWithMarket to set.
     */
    public void setIsRunWithMarket(int isRunWithMarket) {
        this.isRunWithMarket = isRunWithMarket;
    }

    /**
     * @return Returns the isUseMargin.
     */
    public int getIsUseMargin() {
        return isUseMargin;
    }

    /**
     * @param isUseMargin The isUseMargin to set.
     */
    public void setIsUseMargin(int isUseMargin) {
        this.isUseMargin = isUseMargin;
    }

    /**
     * @return Returns the marginPercentageUp.
     */
    public float getMarginPercentageUp() {
        return marginPercentageUp;
    }

    /**
     * @param marginPercentageUp The marginPercentageUp to set.
     */
    public void setMarginPercentageUp(float marginPercentageUp) {
        this.marginPercentageUp = marginPercentageUp;
    }

    /**
     * @return Returns the marginPercentageDown.
     */
    public float getMarginPercentageDown() {
        return marginPercentageDown;
    }

    /**
     * @param marginPercentageDown The marginPercentageDown to set.
     */
    public void setMarginPercentageDown(float marginPercentageDown) {
        this.marginPercentageDown = marginPercentageDown;
    }

    /**
     * @return Returns the timeCreated.
     */
    public Date getTimeCreated() {
        return timeCreated;
    }

    /**
     * @param timeCreated The timeCreated to set.
     */
    public void setTimeCreated(Date timeCreated) {
        this.timeCreated = timeCreated;
    }

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return Returns the writerId.
     */
    public long getWriterId() {
        return writerId;
    }

    /**
     * @param writerId The writerId to set.
     */
    public void setWriterId(long writerId) {
        this.writerId = writerId;
    }
//    public String getWriterName() throws SQLException{
//		return CommonUtil.getWriterName(writerId);
//	}
	public long getProductTypeId() {
		return productTypeId;
	}
	public void setProductTypeId(long productTypeId) {
		this.productTypeId = productTypeId;
	}
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "OpportunityType ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "maxExposure = " + this.maxExposure + TAB
	        + "isRunWithMarket = " + this.isRunWithMarket + TAB
	        + "isUseMargin = " + this.isUseMargin + TAB
	        + "marginPercentageUp = " + this.marginPercentageUp + TAB
	        + "marginPercentageDown = " + this.marginPercentageDown + TAB
	        + "timeCreated = " + this.timeCreated + TAB
	        + "description = " + this.description + TAB
	        + "writerId = " + this.writerId + TAB
	        + "productTypeId = " + this.productTypeId + TAB
	        + " )";

	    return retValue;
	}


}