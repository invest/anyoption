package com.anyoption.common.beans;


import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

/**
 * @author LioR SoLoMoN
 *
 */
public class IssueActionTaskBase {
	private static final Logger logger = Logger.getLogger(IssueActionTaskBase.class);
	protected Issue issue;
	protected IssueAction issueAction;

	/**
	 * @return the issue
	 */
	public Issue getIssue() {
		return issue;
	}

	/**
	 * @param issue the issue to set
	 */
	public void setIssue(Issue issue) {
		this.issue = issue;
	}

	/**
	 * @return the issueAction
	 */
	public IssueAction getIssueAction() {
		return issueAction;
	}

	/**
	 * @param issueAction the issueAction to set
	 */
	public void setIssueAction(IssueAction issueAction) {
		this.issueAction = issueAction;
	}	
}
