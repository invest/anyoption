package com.anyoption.common.beans;

import java.util.Date;

public class ServerPixel {
	
	public static final int SERVER_PIXELS_PUBLISHER_APPSFLYER = 1;
	public static final int SERVER_PIXELS_PUBLISHER_BIDALGO = 2;
	public static final int SERVER_PIXELS_PUBLISHER_ADQUANT = 3;
		
	private int id;
	private int serverPixelType;
	private long refrenceId;
	private Date timeCreated;
	private Date timeSent;
	private int serverPixelsPublisherId;
	private String pixel;
	
	public ServerPixel(int id, int serverPixelType, long refrenceId,
			Date timeCreated, Date timeSent) {
		super();
		this.id = id;
		this.serverPixelType = serverPixelType;
		this.refrenceId = refrenceId;
		this.timeCreated = timeCreated;
		this.timeSent = timeSent;
	}
	
	public ServerPixel(int serverPixelType, long refrenceId, int serverPixelsPublisherId) {
		super();
		this.serverPixelType = serverPixelType;
		this.refrenceId = refrenceId;
		this.serverPixelsPublisherId = serverPixelsPublisherId;
	}
	
	public ServerPixel(int serverPixelType, long refrenceId, int serverPixelsPublisherId, String pixel) {
		this(serverPixelType, refrenceId, serverPixelsPublisherId);
		this.pixel = pixel;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getServerPixelType() {
		return serverPixelType;
	}

	public void setServerPixelType(int serverPixelType) {
		this.serverPixelType = serverPixelType;
	}

	public long getRefrenceId() {
		return refrenceId;
	}

	public void setRefrenceId(long refrenceId) {
		this.refrenceId = refrenceId;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeSent() {
		return timeSent;
	}

	public void setTimeSent(Date timeSent) {
		this.timeSent = timeSent;
	}

	public int getServerPixelsPublisherId() {
		return serverPixelsPublisherId;
	}

	public void setServerPixelsPublisherId(int serverPixelsPublisherId) {
		this.serverPixelsPublisherId = serverPixelsPublisherId;
	}

	public String getPixel() {
		return pixel;
	}

	public void setPixel(String pixel) {
		this.pixel = pixel;
	}
}