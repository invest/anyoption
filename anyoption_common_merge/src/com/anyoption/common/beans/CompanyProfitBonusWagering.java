package com.anyoption.common.beans;

/**
 * @author liors
 *
 */
public class CompanyProfitBonusWagering {
	private double sumInvAmount;
	private double sumHouseResult;
	
	public double getSumInvAmount() {
		return sumInvAmount;
	}
	
	public void setSumInvAmount(double sumInvAmount) {
		this.sumInvAmount = sumInvAmount;
	}
	
	public double getSumHouseResult() {
		return sumHouseResult;
	}
	
	public void setSumHouseResult(double sumHouseResult) {
		this.sumHouseResult = sumHouseResult;
	}
}
