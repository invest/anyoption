/**
 *
 */
package com.anyoption.common.beans;

/**
 * @author pavelhe
 *
 */
public class MarketNameBySkin {

	private int marketId;
	private int skinId;
	private String name;
	private String shortName;
	/**
	 * @return the marketId
	 */
	public int getMarketId() {
		return marketId;
	}
	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(int marketId) {
		this.marketId = marketId;
	}
	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}
	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}
	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MarketNameBySkin [marketId=").append(marketId)
				.append(", skinId=").append(skinId)
				.append(", name=").append(name)
				.append(", shortName=").append(shortName)
				.append("]");
		return builder.toString();
	}

}
