package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

/**
 * @author liors
 *
 */
public class CopyopUserCopy implements Serializable {
	private static final long serialVersionUID = -4863962322684499213L;
	private long userId;
	private Date startDate; 
	private Date endDate;
	private String endReason;
	private String nickname; 
	private long copyTradingProfit;
	private int invCount;
	private int invCountReached;
	private int amount; 
	private boolean isSpecific;
	private long turnover;
	private long balance; 
	private boolean isFrozen;
	private int coinsGenerated;
	private int currencyId;
	private Set<Long> assets;
	private int skinId;
	private ArrayList<String> specificAssets;
	private boolean isCanDelete;

	@Override
	public String toString() {
		return "CopyopUserCopy [userId=" + userId + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", endReason=" + endReason
				+ ", nickname=" + nickname + ", copyTradingProfit="
				+ copyTradingProfit + ", invCount=" + invCount
				+ ", invCountReached=" + invCountReached + ", amount=" + amount
				+ ", isSpecific=" + isSpecific + ", turnover=" + turnover
				+ ", balance=" + balance + ", isFrozen=" + isFrozen
				+ ", coinsGenerated=" + coinsGenerated + ", currencyId="
				+ currencyId + ", assets=" + assets + ", skinId=" + skinId
				+ ", isCanDelete=" + isCanDelete
				+ "]";
	}
	
	public String getInvLeftCopy() {
		String res = "";
		String invCountS = String.valueOf(invCount);
		if (invCount == 0) {
			invCountS = "MAX"; //TODO property msg.
		}
		res = String.valueOf(invCountReached) + " / " + invCountS;
		return res;
	}
	
	public boolean isCanDelete() {
		return endDate == null ? true : false;	
	}
	
	public void setCanDelete(boolean isCanDelete) {
		this.isCanDelete = isCanDelete;
	}
	
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * @return the endReason
	 */
	public String getEndReason() {
		return endReason;
	}
	
	/**
	 * @param endReason the endReason to set
	 */
	public void setEndReason(String endReason) {
		this.endReason = endReason;
	}
	
	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}
	
	/**
	 * @param nickname the nickName to set
	 */
	public void setNickName(String nickname) {
		this.nickname = nickname;
	}
	
	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}
	
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	/**
	 * @return the isSpecific
	 */
	public boolean isSpecific() {
		return isSpecific;
	}
	
	/**
	 * @param isSpecific the isSpecific to set
	 */
	public void setSpecific(boolean isSpecific) {
		this.isSpecific = isSpecific;
	}
	
	/**
	 * @return the turnover
	 */
	public long getTurnover() {
		return turnover;
	}
	
	/**
	 * @param turnover the turnover to set
	 */
	public void setTurnover(long turnover) {
		this.turnover = turnover;
	}
	
	/**
	 * @return the balance
	 */
	public long getBalance() {
		return balance;
	}
	
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(long balance) {
		this.balance = balance;
	}
	
	/**
	 * @return the isFrozen
	 */
	public boolean isFrozen() {
		return isFrozen;
	}
	
	/**
	 * @param isFrozen the isFrozen to set
	 */
	public void setFrozen(boolean isFrozen) {
		this.isFrozen = isFrozen;
	}
	
	/**
	 * @return the coinsGenerated
	 */
	public int getCoinsGenerated() {
		return coinsGenerated;
	}
	
	/**
	 * @param coinsGenerated the coinsGenerated to set
	 */
	public void setCoinsGenerated(int coinsGenerated) {
		this.coinsGenerated = coinsGenerated;
	}

	/**
	 * @return the copyTradingProfit
	 */
	public long getCopyTradingProfit() {
		return copyTradingProfit;
	}

	/**
	 * @param copyTradingProfit the copyTradingProfit to set
	 */
	public void setCopyTradingProfit(long copyTradingProfit) {
		this.copyTradingProfit = copyTradingProfit;
	}

	/**
	 * @return the currencyId
	 */
	public int getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the invCount
	 */
	public int getInvCount() {
		return invCount;
	}

	/**
	 * @param invCount the invCount to set
	 */
	public void setInvCount(int invCount) {
		this.invCount = invCount;
	}

	/**
	 * @return the invCountReached
	 */
	public int getInvCountReached() {
		return invCountReached;
	}

	/**
	 * @param invCountReached the invCountReached to set
	 */
	public void setInvCountReached(int invCountReached) {
		this.invCountReached = invCountReached;
	}

	/**
	 * @return the assets
	 */
	public Set<Long> getAssets() {
		return assets;
	}

	/**
	 * @param assets the assets to set
	 */
	public void setAssets(Set<Long> assets) {
		this.assets = assets;
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	public ArrayList<String> getSpecificAssets() {
		if (null == specificAssets) {
			specificAssets = new ArrayList<String>();
		}
		return specificAssets;
	}

	public void setSpecificAssets(ArrayList<String> specificAssets) {
		this.specificAssets = specificAssets;
	}
}
