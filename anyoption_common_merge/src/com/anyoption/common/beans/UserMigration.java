package com.anyoption.common.beans;

import java.io.Serializable;

public class UserMigration implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2932303944090910617L;
	long userId;
	long balance;
	int targetPlatform;
	boolean acceptedTransfer;
	boolean acceptedMailing;
	boolean partial;
	String firstName;
	String lastName;
	String email;
	String phone;
	String country;
	String language;
	String currency;
	
	String address;
	String city;
	String gender;
	String state;
	String zip;
	String registered;
	String ip;
	
	public UserMigration() {
		
	}
	
	
	public UserMigration(long userId, long balance, boolean acceptedTransfer,
			String firstName, String lastName, String email, String phone, String country, String currency) {
		super();
		this.userId = userId;
		this.balance = balance;
		this.acceptedTransfer = acceptedTransfer;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.country = country;
		this.currency = currency;
	}

	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getBalance() {
		return balance;
	}
	public void setBalance(long balance) {
		this.balance = balance;
	}
	public int getTargetPlatform() {
		return targetPlatform;
	}
	public void setTargetPlatform(int targetPlatform) {
		this.targetPlatform = targetPlatform;
	}
	public boolean isAcceptedTransfer() {
		return acceptedTransfer;
	}
	public void setAcceptedTransfer(boolean acceptedTransfer) {
		this.acceptedTransfer = acceptedTransfer;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public boolean isAcceptedMailing() {
		return acceptedMailing;
	}
	public void setAcceptedMailing(boolean acceptedMailing) {
		this.acceptedMailing = acceptedMailing;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String toCsv() {
		
		return  email + "," + 		//1 
				firstName + "," + 	//2
				lastName + "," + 	//3
				phone + "," + 		//4
				language+ "," + 	//5
				address+ "," + 		//6
				city+ "," + 		//7
				state+ "," + 		//8
				zip+ "," + 			//9
				country + "," +  	//10
				registered + ","  + //11
				ip + "," + 			//12
				gender+ "," +		//13
				balance + "," + 	//14
				currency + "," + 	//15
				acceptedMailing + "," + //16
				"1," +				//17
				"1," +				//18
				userId + "," +		//19
				partial;			// 20
	}
	
	public static String headerCsv() {
		
		return  "email," + 			//1
				"firstName," + 		//2
				"lastName," + 		//3
				"phone," + 			//4
				"language," + 		//5
				"address," + 		//6
				"city," + 			//7
				"state," + 			//8
				"zip," + 			//9
				"country," +  		//10
				"registered,"  + 	//11
				"ip," + 			//12
				"gender," +			//13
				"balance," + 		//14
				"currency," + 		//15
				"acceptedMailing," + //16
				"acceptedTOC,"  +	//17
				"acceptedTransfer," + //18
				"userId," +			//19
				"partial";			//20
	}	

	@Override
	public String toString() {
		return "UserMigration [userId=" + userId + ", balance=" + balance + ", firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + ", phone=" + phone + ", country=" + country + ", language=" + language
				+ "]";
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getZip() {
		return zip;
	}


	public void setZip(String zip) {
		this.zip = zip;
	}


	public String getRegistered() {
		return registered;
	}


	public void setRegistered(String registered) {
		this.registered = registered;
	}


	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}

	public boolean isPartial() {
		return partial;
	}


	public void setPartial(boolean partial) {
		this.partial = partial;
	}
}
