package com.anyoption.common.beans;

import java.math.BigDecimal;
import java.util.Date;

public class ReutersQuotes implements java.io.Serializable {
	private static final long serialVersionUID = -6923580562243314918L;

	private long id;
	private long opportunityId;
	private Date time;
	private BigDecimal last;
	private BigDecimal ask;
	private BigDecimal bid;
	private int decimalPoint;

	public ReutersQuotes() {

	}
	
	public ReutersQuotes(Date time, BigDecimal last, BigDecimal ask, BigDecimal bid) {
		this.time = time;
		this.last = last;
		this.ask = ask;
		this.bid = bid;
	}

	public ReutersQuotes(Date time, double last) {
		this.time = time;
		this.last = new BigDecimal(last);
		this.ask = null;
		this.bid = null;
	}

	/**
	 * @return the ask
	 */
	public BigDecimal getAsk() {
		return ask;
	}

	/**
	 * Format value to 3 digits after the point.
	 * @return formatted string with 3 digits after the point.
	 */
	public String getAskFormatted() {
		if (null != ask) {
			return ask.setScale(decimalPoint, BigDecimal.ROUND_HALF_UP).toString();
		}
		return "";
	}

	/**
	 * @param ask the ask to set
	 */
	public void setAsk(BigDecimal ask) {
		this.ask = ask;
	}

	/**
	 * @return the bid
	 */
	public BigDecimal getBid() {
		return bid;
	}

	/**
	 * Format value to 3 digits after the point.
	 * @return formatted string with 3 digits after the point.
	 */
	public String getBidFormatted() {
		if (null != bid) {
			return bid.setScale(decimalPoint, BigDecimal.ROUND_HALF_UP).toString();
		}
		return "";
	}

	/**
	 * @param bid the bid to set
	 */
	public void setBid(BigDecimal bid) {
		this.bid = bid;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the last
	 */
	public BigDecimal getLast() {
		return last;
	}

	/**
	 * Format value to 3 digits after the point.
	 * @return formatted string with 3 digits after the point.
	 */
	public String getLastFormatted() {
		if (null != last) {
			return last.setScale(decimalPoint, BigDecimal.ROUND_HALF_UP).toString();
		}
		return "";
	}

	/**
	 * @param last the last to set
	 */
	public void setLast(BigDecimal last) {
		this.last = last;
	}

	/**
	 * @return the opportunityId
	 */
	public long getOpportunityId() {
		return opportunityId;
	}

	/**
	 * @param opportunityId the opportunityId to set
	 */
	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}

	/**
	 * @return the time
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(Date time) {
		this.time = time;
	}

	/**
	 * @return the decimalPoint
	 */
	public int getDecimalPoint() {
		return decimalPoint;
	}

	/**
	 * @param decimalPoint the decimalPoint to set
	 */
	public void setDecimalPoint(int decimalPoint) {
		this.decimalPoint = decimalPoint;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString() {
	    final String TAB = " \n ";

	    String retValue = "id = " + this.id + TAB
	        + "opportunityId = " + this.opportunityId + TAB
	        + "time = " + this.time + TAB
	        + "last = " + (this.last != null ? this.last.doubleValue() : null) + TAB
	        + "ask = " + (this.ask != null ? this.ask.doubleValue() : null) + TAB
	        + "bid = " + (this.bid != null ? this.bid.doubleValue() : null) + TAB
	        + "decimal point = " + (this.decimalPoint);

	    return retValue;
	}
}
