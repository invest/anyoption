package com.anyoption.common.beans;

import java.io.Serializable;

/**
 * TierSkin class
 *
 * @author Kobi.
 */
public class TierPointConversion implements Serializable {
    private static final long serialVersionUID = 1L;

    protected long id;
    protected long fromNumPoints;
    protected long toNumPoints;
    protected long percent;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the fromNumPoints
	 */
	public long getFromNumPoints() {
		return fromNumPoints;
	}

	/**
	 * @param fromNumPoints the fromNumPoints to set
	 */
	public void setFromNumPoints(long fromNumPoints) {
		this.fromNumPoints = fromNumPoints;
	}

	/**
	 * @return the percent
	 */
	public long getPercent() {
		return percent;
	}

	/**
	 * @param percent the percent to set
	 */
	public void setPercent(long percent) {
		this.percent = percent;
	}

	/**
	 * @return the toNumPoints
	 */
	public long getToNumPoints() {
		return toNumPoints;
	}

	/**
	 * @param toNumPoints the toNumPoints to set
	 */
	public void setToNumPoints(long toNumPoints) {
		this.toNumPoints = toNumPoints;
	}

}