package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.Date;
import com.anyoption.common.clearing.ClearingInfo;

/**
 * TransactionsEpg 
 * @author eyalo
 */
public class TransactionRequestResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5495737315174475970L;
	
	private long id;
	private long transactionId;
	private String request;
	private String response;	
	private Date timeCreated;
	private Date timeUpdated;
	private String paymentSolution;
	private ClearingInfo clearingInfo;
	private PaymentMethodService paymentMethodService;
	private String paymentDetails;
	private Transaction transaction;

	public TransactionRequestResponse() {
		
	}
	
	/**
	 * @param transactionId
	 * @param request
	 * @param response
	 * @param paymentSolution
	 */
	public TransactionRequestResponse(long transactionId, String request, 
			String response, String paymentSolution) {
		this.transactionId = transactionId;
		this.request = request;
		this.response = response;
		this.paymentSolution = paymentSolution;
		this.paymentDetails = "";
	}
	
	/**
	 * @param transactionId
	 * @param request
	 * @param response
	 * @param paymentSolution
	 * @param paymentDetails
	 */
	public TransactionRequestResponse(long transactionId, String request, 
			String response, String paymentSolution, String paymentDetails) {
		this.transactionId = transactionId;
		this.request = request;
		this.response = response;
		this.paymentSolution = paymentSolution;
		this.paymentDetails = paymentDetails;
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the request
	 */
	public String getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(String request) {
		this.request = request;
	}

	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the timeUpdated
	 */
	public Date getTimeUpdated() {
		return timeUpdated;
	}

	/**
	 * @param timeUpdated the timeUpdated to set
	 */
	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}

	/**
	 * @return the clearingInfo
	 */
	public ClearingInfo getClearingInfo() {
		return clearingInfo;
	}

	/**
	 * @param clearingInfo the clearingInfo to set
	 */
	public void setClearingInfo(ClearingInfo clearingInfo) {
		this.clearingInfo = clearingInfo;
	}

	/**
	 * @return the paymentSolution
	 */
	public String getPaymentSolution() {
		return paymentSolution;
	}

	/**
	 * @param paymentSolution the paymentSolution to set
	 */
	public void setPaymentSolution(String paymentSolution) {
		this.paymentSolution = paymentSolution;
	}
	
	/**
	 * @return the paymentMethodService
	 */
	public PaymentMethodService getPaymentMethodService() {
		return paymentMethodService;
	}

	/**
	 * @param paymentMethodService the paymentMethodService to set
	 */
	public void setPaymentMethodService(PaymentMethodService paymentMethodService) {
		this.paymentMethodService = paymentMethodService;
	}
	
	/**
	 * @return the transaction
	 */
	public Transaction getTransaction() {
		return transaction;
	}

	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	
	@Override
	public String toString() {
		String ls = "\n";
		return "TransactionRequestResponse " + ls
				+ " [id=" + id + ls
				+ " transactionId=" + transactionId + ls
				+ " request=" + request + ls
				+ " response=" + response + ls
				+ " timeCreated=" + timeCreated + ls
				+ " timeUpdated=" + timeUpdated + ls
				+ " paymentMethodService=" + paymentMethodService + "]";
	}

	/**
	 * @return the paymentDetails
	 */
	public String getPaymentDetails() {
		return paymentDetails;
	}

	/**
	 * @param paymentDetails the paymentDetails to set
	 */
	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
}
