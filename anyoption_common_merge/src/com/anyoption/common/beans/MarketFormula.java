package com.anyoption.common.beans;

import java.io.Serializable;

/**
 * @author kiril.mutafchiev
 */
public class MarketFormula implements Serializable {

	private static final long serialVersionUID = 7541780118293915077L;
	private long id;
	private String name;
	private String description;
	private String params;
	private int type;
	private String descriptionKey;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getDescriptionKey() {
		return descriptionKey;
	}

	public void setDescriptionKey(String descriptionKey) {
		this.descriptionKey = descriptionKey;
	}

	@Override
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "MarketFormula: " + ls
				+ super.toString() + ls
				+ "id: " + id + ls
				+ "name: " + name + ls
				+ "description: " + description + ls
				+ "params: " + params + ls
				+ "type: " + type + ls
				+ "descriptionKey: " + descriptionKey + ls;
	}
}