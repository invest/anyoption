package com.anyoption.common.beans.base;
import java.util.Date;

public class UserRegulationBase implements java.io.Serializable {
	private static final long serialVersionUID = 2140243955747592634L;
	/* Approved regulation steps [ */
	public static final int REGULATION_USER_REGISTERED = 0; 
	public static final int REGULATION_WC_APPROVAL = 1;
	public static final int REGULATION_FIRST_DEPOSIT = 2;
	public static final int REGULATION_MANDATORY_QUESTIONNAIRE_DONE = 3;
	public static final int REGULATION_ALL_DOCUMENTS_RECEIVED = 5;
	public static final int REGULATION_ALL_DOCUMENTS_SUPPORT_APPROVAL = 6;
	public static final int REGULATION_CONTROL_APPROVED_USER = 7;
	
	public static final int ET_REGULATION_KNOWLEDGE_QUESTION_USER = 103;
	public static final int ET_CAPITAL_MARKET_QUESTIONNAIRE = 104;
	public static final int ET_REGULATION_DONE = 105;
	
	public static final long ET_REGULATION_LOW_SCORE_GROUP_CALC = 9;
	public static final long ET_REGULATION_MEDIUM_SCORE_GROUP_CALC = 17;
	
	public static final long ET_REGULATION_UNSUSPENDED_SCORE_GROUP_ID = 0;
	public static final long ET_REGULATION_HIGH_SCORE_GROUP_ID = 1;
	public static final long ET_REGULATION_MEDIUM_SCORE_GROUP_ID = 2;
	public static final long ET_REGULATION_LOW_SCORE_GROUP_ID = 3;	
	
	public static final long AO_REGULATION_RESTRICTED_SCORE_GROUP_ID = 4;
	public static final long AO_REGULATION_RESTRICTED_LOW_X_SCORE_GROUP_ID = 5;
	public static final long AO_REGULATION_RESTRICTED_HIGH_Y_SCORE_GROUP_ID = 6;
	public static final long AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID = 7;
	/* ] Approved regulation steps */
	
	/* Suspended reasons [ */
	public static final int NOT_SUSPENDED = 0;
	public static final int SUSPENDED_BY_WORLD_CHECK = 1;
	public static final int SUSPENDED_MANDATORY_QUESTIONNAIRE_NOT_FILLED = 2;
	public static final int SUSPENDED_DUE_DOCUMENTS = 3;
	public static final int SUSPENDED_MANUALLY = 4;
	public static final int SUSPENDED_AUTO_WD = 6;
	public static final int SUSPENDED_INV_MIGRATION = 7;
	public static final int SUSPENDED_CNMV_RESTRICTED = 15;
	@Deprecated
	public static final int SUSPENDED_MANDATORY_QUESTIONNAIRE_FILLED_INCORRECT = 5;
	
	//ET REGULATION
	public static final int ET_SUSPEND_LOW_SCORE_GROUP = 10;
	public static final int ET_SUSPEND_TRESHEHOLD_LOW_SCORE_GROUP = 11;
	public static final int ET_SUSPEND_TRESHEHOLD_MEDIUM_SCORE_GROUP = 12;
	
	public static final int REGULATION_USER_VERSION = 2; 
	
	/* ] Suspended reasons */
	
	public static final int DAYS_FROM_QUALIFIED_TIME = 30;
	
	//PEP STATES
	public static final long PEP_NON_PROHIBITED = 0;
	public static final long PEP_AUTO_PROHIBITED = 1;
	public static final long PEP_WRITER_PROHIBITED = 2;
	public static final long PEP_APPROVED_BY_COMPLIANCE = 3;
	public static final long PEP_FALSE_CLASSIFICATION = 4;
	
	public static final long HASH_TYPE_ET_UNSUSPEND = 1;
	public static final long HASH_TYPE_AO_UNRESTRICTED = 2;
	public static final long HASH_TYPE_AO_UNBLOCKTRESHOLD = 3;
	
	public static final long AO_TRESHOLD_BLOCK_UNBLOCKED = -1;
	
	private long userId;
	private Integer approvedRegulationStep;
	private boolean isWcApproval;
	private String comments;
	private String wcComment;
	private boolean optionalQuestionnaireDone;
	private boolean isQualified;
	/* !!! Always set this on insert and update !!! */
	private long writerId = -1L;
	private Integer suspendedReasonId;
	private String regulationStepTxt;
	private String suspendedReasonTxt;
	private Date fDTTimeCreated;
	private Date timeSuspended;
	private Date qualificationTime;
	private boolean isKnowledgeQuestion;
	private boolean regularReportMail;
	private Long scoreGroup;
	private Long tresholdLimit;
	private Long score;
	private Integer regulationVersion;
	private Long qualifiedAmount;
	private Long pepState;
	private Long thresholdBlock;
	private Date controlApprovedDate;
	private Date questDoneDate;
	
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the approvedRegulationStep
	 */
	public Integer getApprovedRegulationStep() {
		return approvedRegulationStep;
	}

	/**
	 * @param approvedRegulationStep the approvedRegulationStep to set
	 */
	public void setApprovedRegulationStep(Integer approvedRegulationStep) {
		this.approvedRegulationStep = approvedRegulationStep;
	}

	/**
	 * @return the isWcApproval
	 */
	public boolean isWcApproval() {
		return isWcApproval;
	}

	/**
	 * @param isWcApproval the isWcApproval to set
	 */
	public void setWcApproval(boolean isWcApproval) {
		this.isWcApproval = isWcApproval;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the wcComment
	 */
	public String getWcComment() {
		return wcComment;
	}

	/**
	 * @param wcComment the wcComment to set
	 */
	public void setWcComment(String wcComment) {
		this.wcComment = wcComment;
	}

	/**
	 * @return the optionalQuestionnaireDone
	 */
	public boolean isOptionalQuestionnaireDone() {
		return optionalQuestionnaireDone;
	}

	/**
	 * @param optionalQuestionnaireDone the optionalQuestionnaireDone to set
	 */
	public void setOptionalQuestionnaireDone(boolean optionalQuestionnaireDone) {
		this.optionalQuestionnaireDone = optionalQuestionnaireDone;
	}
	
	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the suspendedReasonId
	 */
	public Integer getSuspendedReasonId() {
		return suspendedReasonId;
	}

	/**
	 * @param suspendedReasonId the suspendedReasonId to set
	 */
	public void setSuspendedReasonId(Integer suspendedReasonId) {
		this.suspendedReasonId = suspendedReasonId;
	}

	@Override
	public String toString() {
		return "UserRegulationBase [userId=" + userId
				+ ", approvedRegulationStep=" + approvedRegulationStep
				+ ", isWcApproval=" + isWcApproval + ", comments="
				+ comments + ", wcComment=" + wcComment
				+ ", optionalQuestionnaireDone=" + optionalQuestionnaireDone
				+ ", isQualified=" + isQualified
				+ ", qualifiedAmount=" + qualifiedAmount
				+ ", score=" + score
				+ ", writerId=" + writerId + ", suspendedReasonId=" + suspendedReasonId 
				+ ", isKnowledgeQuestion=" + isKnowledgeQuestion + ", scoreGroup=" + scoreGroup 
				+ ", tresholdLimit=" + tresholdLimit 
				+ ", pepState=" + pepState 
				+ ", thresholdBlock=" + thresholdBlock + "]";
	}

	public String getRegulationStepTxt() {
		return regulationStepTxt;
	}

	public void setRegulationStepTxt(String regulationStepTxt) {
		this.regulationStepTxt = regulationStepTxt;
	}

	public String getSuspendedReasonTxt() {
		return suspendedReasonTxt;
	}

	public void setSuspendedReasonTxt(String suspendedReasonTxt) {
		this.suspendedReasonTxt = suspendedReasonTxt;
	}

	public Date getTimeSuspended() {
		return timeSuspended;
	}

	public void setTimeSuspended(Date timeSuspended) {
		this.timeSuspended = timeSuspended;
	}

	public Date getFDTTimeCreated() {
		return fDTTimeCreated;
	}

	public void setFDTTimeCreated(Date fDTTimeCreated) {
		this.fDTTimeCreated = fDTTimeCreated;
	}

    /**
     * @return the isQualified
     */
    public boolean isQualified() {
        return isQualified;
    }

    /**
     * @param isQualified the isQualified to set
     */
    public void setQualified(boolean isQualified) {
        this.isQualified = isQualified;
    }

	public Date getQualificationTime() {
		return qualificationTime;
	}

	public void setQualificationTime(Date qualificationTime) {
		this.qualificationTime = qualificationTime;
	}

	public boolean isKnowledgeQuestion() {
		return isKnowledgeQuestion;
	}

	public void setKnowledgeQuestion(boolean isKnowledgeQuestion) {
		this.isKnowledgeQuestion = isKnowledgeQuestion;
	}
	
	public boolean isRegularReportMail() {
		return regularReportMail;
	}

	public void setRegularReportMail(boolean regularReportMail) {
		this.regularReportMail = regularReportMail;
	}

	public Long getScoreGroup() {
		return scoreGroup;
	}

	public void setScoreGroup(Long scoreGroup) {
		this.scoreGroup = scoreGroup;
	}

	public Long getTresholdLimit() {
		return tresholdLimit;
	}

	public void setTresholdLimit(Long tresholdLimit) {
		this.tresholdLimit = tresholdLimit;
	}

	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public Integer getRegulationVersion() {
		return regulationVersion;
	}

	public void setRegulationVersion(Integer regulationVersion) {
		this.regulationVersion = regulationVersion;
	}
	
	public Long getQualifiedAmount() {
		return qualifiedAmount;
	}

	public void setQualifiedAmount(Long qualifiedAmount) {
		this.qualifiedAmount = qualifiedAmount;
	}
	
	/**
	 * @return the suspended
	 */
	public boolean isSuspended() {
		return suspendedReasonId != null
				&& suspendedReasonId != NOT_SUSPENDED;
	}

	/**
	 * @return the suspendedDueDocuments
	 */
	public boolean isSuspendedDueDocuments() {
		return suspendedReasonId != null && approvedRegulationStep != null
				&& suspendedReasonId == SUSPENDED_DUE_DOCUMENTS
				&& approvedRegulationStep < REGULATION_CONTROL_APPROVED_USER;
	}
	
	public boolean isRestricted() {
		return scoreGroup != null
				&& (scoreGroup == AO_REGULATION_RESTRICTED_SCORE_GROUP_ID 
						|| scoreGroup == AO_REGULATION_RESTRICTED_LOW_X_SCORE_GROUP_ID
								|| scoreGroup == AO_REGULATION_RESTRICTED_HIGH_Y_SCORE_GROUP_ID
									|| scoreGroup == AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID)
				&& !isKnowledgeQuestion;
	}
	
	public boolean isRestrictedGroup() {
		return scoreGroup != null
				&& (scoreGroup == AO_REGULATION_RESTRICTED_SCORE_GROUP_ID 
						|| scoreGroup == AO_REGULATION_RESTRICTED_LOW_X_SCORE_GROUP_ID
								|| scoreGroup == AO_REGULATION_RESTRICTED_HIGH_Y_SCORE_GROUP_ID
									|| scoreGroup == AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID);
	}
	
	public boolean isSuspendedCNMVDocuments() {
		return suspendedReasonId != null && approvedRegulationStep != null
				&& suspendedReasonId == SUSPENDED_CNMV_RESTRICTED
				&& approvedRegulationStep < REGULATION_CONTROL_APPROVED_USER;
	}

	@Deprecated
	public boolean isQuestionaireIncorrect() {
		return suspendedReasonId != null
				&& suspendedReasonId == SUSPENDED_MANDATORY_QUESTIONNAIRE_FILLED_INCORRECT;
	}

	public boolean showWarningDueDocuments(){
		return qualifiedAmount != null && approvedRegulationStep != null
				&& qualifiedAmount > 0 && !isQualified && approvedRegulationStep < REGULATION_CONTROL_APPROVED_USER;
	}
	
	public boolean showPopUpWarningDueDocuments(){
		return approvedRegulationStep != null && !isQualified 
				 && approvedRegulationStep >= REGULATION_MANDATORY_QUESTIONNAIRE_DONE && approvedRegulationStep < REGULATION_CONTROL_APPROVED_USER;
	}
	
	public boolean isTresholdBlock() {
		return scoreGroup != null && thresholdBlock > 0;
	}

	public Long getPepState() {
		return pepState;
	}

	public void setPepState(Long pepState) {
		this.pepState = pepState;
	}

	public Long getThresholdBlock() {
		return thresholdBlock;
	}

	public void setThresholdBlock(Long thresholdBlock) {
		this.thresholdBlock = thresholdBlock;
	}

	public Date getControlApprovedDate() {
		return controlApprovedDate;
	}

	public void setControlApprovedDate(Date controlApprovedDate) {
		this.controlApprovedDate = controlApprovedDate;
	}

	public Date getQuestDoneDate() {
		return questDoneDate;
	}

	public void setQuestDoneDate(Date questDoneDate) {
		this.questDoneDate = questDoneDate;
	}	
}
