package com.anyoption.common.beans.base;

import java.io.Serializable;

import com.anyoption.common.enums.SkinGroup;


public class Skin implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2824479755172324244L;
	
    public static final int SKINS_DEFAULT = 2;    // english skin
	public static final int SKINS_DEFAULT_ET = 1;    // hebrew skin
	public static final int  SKINS_ALL_VALUE = 0;
	public static final String SKINS_ALL = "0";
	
	public static final int SKIN_ETRADER_INT = 1;
    public static final long SKIN_ETRADER = 1;
    public static final long SKIN_ENGLISH = 2;
    public static final long SKIN_TURKISH = 3;
    public static final long SKIN_SPAIN = 5;
    public static final long SKIN_ARABIC = 4;
    public static final long SKIN_GERMAN = 8;
    public static final long SKIN_ITALIAN = 9;
    public static final long SKIN_API = 7;
    public static final long SKIN_SPAINMI = 6;
    public static final long SKIN_RUSSIAN = 10;
    public static final long SKIN_TLV = 11;
    public static final long SKIN_FRANCE = 12;
    public static final long SKIN_EN_US = 13;
    public static final long SKIN_ES_US = 14;
    public static final long SKIN_CHINESE = 15;
    public static final long SKIN_REG_EN = 16; //english skin regulated
    public static final long SKIN_KOREAN = 17; 
    public static final long SKIN_REG_ES = 18; //spanish skin regulated
    public static final long SKIN_REG_DE = 19; //german skin regulated
    public static final long SKIN_REG_IT = 20; //italian skin regulated
    public static final long SKIN_REG_FR = 21; //french skin regulated
    public static final long SKIN_CHINESE_VIP = 22; // VIP168
    public static final long SKIN_DUTCH_REG = 23; // dutch skin regulated
    public static final long SKIN_SWEDISH_REG = 24; // swedish skin regulated
    public static final long SKIN_ENGLISH_NON_REG = 25;
    public static final long SKIN_SPAIN_NON_REG = 26;
    public static final long SKIN_CZECH_REG = 27;
    public static final long SKIN_POLISH_REG = 28;
    public static final long SKIN_PORTUGAL_REG = 29;
    
    //LOCALE_SUBDOMAIN
    public static final String LOCALE_SUBDOMAIN_ETRADER = "iw";
    public static final String LOCALE_SUBDOMAIN_ENGLISH = "en";
    public static final String LOCALE_SUBDOMAIN_TURKISH = "tr";
    public static final String LOCALE_SUBDOMAIN_ARABIC = "ar";
    public static final String LOCALE_SUBDOMAIN_SPAIN = "es";
    public static final String LOCALE_SUBDOMAIN_GERMAN = "de";
    public static final String LOCALE_SUBDOMAIN_ITALIAN = "it";
    public static final String LOCALE_SUBDOMAIN_RUSSIAN = "ru";
    public static final String LOCALE_SUBDOMAIN_FRANCE = "fr";
    public static final String LOCALE_SUBDOMAIN_EN_US = "en-us";
    public static final String LOCALE_SUBDOMAIN_ES_US = "es-us";
    public static final String LOCALE_SUBDOMAIN_CHINESE = "zh";
    public static final String LOCALE_SUBDOMAIN_DUTCH = "nl";
    public static final String LOCALE_SUBDOMAIN_SWEDISH = "SV";
    public static final String LOCALE_SUBDOMAIN_CZECH = "cs";
    public static final String LOCALE_SUBDOMAIN_POLISH = "pl";
    public static final String LOCALE_SUBDOMAIN_PORTUGAL = "pt";

    public static final long SKIN_BUSINESS_ETRADER = 1;
    public static final long SKIN_BUSINESS_ANYOPTION = 2;
    public static final long SKIN_BUSINESS_PARNTER = 3;
    public static final long SKIN_BUSINESS_OUROBOROS = 4;
    public static final long SKIN_BUSINESS_GOLDBEAM = 5;
    
    public static final long SKIN_LIST_OB = 1;
    public static final long SKIN_LIST_AOPS = 2;
    public static final long SKIN_LIST_BLOCK_COUNTRY = 3;
    public static final long SKIN_LIST_GOLDBEAM = 4;

    protected int id;
    protected String name;
    protected String displayName;
    protected int defaultCountryId;
    protected int defaultLanguageId;
    protected int defaultXorId;
    protected int defaultCurrencyId;
    protected long defaultCombinationId;
    protected long tierQualificationPeriod;
    protected long businessCaseId;
    private String supportStartTime;
    private String supportEndTime;
    private String keySubdomain;
    private String subdomain;
    private boolean isRegulated;
    private String supportEmail;
    private long controlCounting;
    private long sumControlGroup; // use only for report.
    private String locale;
    private String style;
    private String templates;
    private SkinGroup skinGroup;
    private boolean isActive;
    
	public Skin() {
        name = "";
        displayName = "";
    }

	 /**
	 * @return the supportStartTime
	 */
	public String getSupportStartTime() {
		return supportStartTime;
	}

	/**
	 * @param supportStartTime the supportStartTime to set
	 */
	public void setSupportStartTime(String supportStartTime) {
		this.supportStartTime = supportStartTime;
	}

	/**
	 * @return the supportEndTime
	 */
	public String getSupportEndTime() {
		return supportEndTime;
	}

	/**
	 * @param supportEndTime the supportEndTime to set
	 */
	public void setSupportEndTime(String supportEndTime) {
		this.supportEndTime = supportEndTime;
	}

    /**
     * This MUST return long!!!
     * 
     * @return the defaultCountryId
     */
    public long getDefaultCountryId() {
        return defaultCountryId;
    }

    /**
     * @param defaultCountryId the defaultCountryId to set
     */
    public void setDefaultCountryId(int defaultCountryId) {
        this.defaultCountryId = defaultCountryId;
    }

    /**
     * @return the defaultLanguageId
     */
    public int getDefaultLanguageId() {
        return defaultLanguageId;
    }

    /**
     * @param defaultLanguageId the defaultLanguageId to set
     */
    public void setDefaultLanguageId(int defaultLanguageId) {
        this.defaultLanguageId = defaultLanguageId;
    }


    public int getDefaultXorId() {
        return defaultXorId;
    }

    public void setDefaultXorId(int defaultXorId) {
        this.defaultXorId = defaultXorId;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the defaultCurrencyId
     */
    public int getDefaultCurrencyId() {
        return defaultCurrencyId;
    }

    /**
     * @param defaultCurrencyId the defaultCurrencyId to set
     */
    public void setDefaultCurrencyId(int defaultCurrencyId) {
        this.defaultCurrencyId = defaultCurrencyId;
    }

    /**
     * @return the defaultCombinationId
     */
    public long getDefaultCombinationId() {
        return defaultCombinationId;
    }

    /**
     * @param defaultCombinationId the defaultCombinationId to set
     */
    public void setDefaultCombinationId(long defaultCombinationId) {
        this.defaultCombinationId = defaultCombinationId;
    }

   /**
     * @return the tierQualificationPeriod
     */
    public long getTierQualificationPeriod() {
        return tierQualificationPeriod;
    }

    /**
     * @param tierQualificationPeriod the tierQualificationPeriod to set
     */
    public void setTierQualificationPeriod(long tierQualificationPeriod) {
        this.tierQualificationPeriod = tierQualificationPeriod;
    }

   /**
     * @return the businessCaseId
     */
    public long getBusinessCaseId() {
        return businessCaseId;
    }

    /**
     * @param businessCaseId the businessCaseId to set
     */
    public void setBusinessCaseId(long businessCaseId) {
        this.businessCaseId = businessCaseId;
    }
    
    /**
	 * @return the keySubdomain
	 */
	public String getKeySubdomain() {
		return keySubdomain;
	}

	/**
	 * @param keySubdomain the keySubdomain to set
	 */
	public void setKeySubdomain(String keySubdomain) {
		this.keySubdomain = keySubdomain;
	}

	/**
	 * @return the subdomain
	 */
	public String getSubdomain() {
		return subdomain;
	}

	/**
	 * @param subdomain the subdomain to set
	 */
	public void setSubdomain(String subdomain) {
		this.subdomain = subdomain;
	}

	/**
	 * @return the isRegulated
	 */
	public boolean isRegulated() {
		return isRegulated;
	}

	/**
	 * @param isRegulated the isRegulated to set
	 */
	public void setIsRegulated(boolean isRegulated) {
		this.isRegulated = isRegulated;
	}
	
	public String getSupportEmail() {
		return supportEmail;
	}

	/**
	 * @param supportEmail the supportEmail to set
	 */
	public void setSupportEmail(String supportEmail) {
		this.supportEmail = supportEmail;
	}
	
	/**
	 * @return the controlCounting
	 */
	public long getControlCounting() {
		return controlCounting;
	}

	/**
	 * @param controlCounting the controlCounting to set
	 */
	public void setControlCounting(long controlCounting) {
		this.controlCounting = controlCounting;
	}
	
	/**
	 * @return the sumControlGroup
	 */
	public long getSumControlGroup() {
		return sumControlGroup;
	}

	/**
	 * @param sumControlGroup the sumControlGroup to set
	 */
	public void setSumControlGroup(long sumControlGroup) {
		this.sumControlGroup = sumControlGroup;
	}
	
	/**
     * get business case id for transactions_bins table
     * Special case: only for clearing providers system,
     * users with business case partner will be the same like anyoption in transactions_bins table.
     * @return the businessCaseId
     */
    public long getBusinessCaseIdForTranBins() {
        if (businessCaseId == SKIN_BUSINESS_PARNTER) {
            return SKIN_BUSINESS_ANYOPTION;
        }
        return businessCaseId;
    }
    
 
	/**
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getTemplates() {
		return templates;
	}

	public void setTemplates(String templates) {
		this.templates = templates;
	}

	/**
	 * @return the skinGroup
	 */
	public SkinGroup getSkinGroup() {
		return skinGroup;
	}

	/**
	 * @param skinGroup the skinGroup to set
	 */
	public void setSkinGroup(SkinGroup skinGroup) {
		this.skinGroup = skinGroup;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "skin" + ls
            + super.toString() + ls
            + "id: " + id + ls
            + "name: " + name + ls
            + "displayName: " + displayName + ls
            + "defaultCountryId: " + defaultCountryId + ls
            + "defaultLanguageId: " + defaultLanguageId + ls
            + "defaultXorId: " + defaultXorId + ls
            + "defaultCurrencyId: " + defaultCurrencyId + ls
            + "controlCounting: " + controlCounting + ls
            + "style: " + style + ls
            + "templates: " + templates + ls
            + "skinGroup: " + skinGroup + ls;
    }

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

}