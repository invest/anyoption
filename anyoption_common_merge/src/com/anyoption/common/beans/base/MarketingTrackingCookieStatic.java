package com.anyoption.common.beans.base;

import java.util.Date;

public class MarketingTrackingCookieStatic implements java.io.Serializable{

	private static final long serialVersionUID = 5651106788162437452L;
	public static final String EMPTY_STRING = "";
	
	private String ms;
	private String cs;
	private String http;
	private String dp;
    private String aff_sub1;
    private String aff_sub2;
    private String aff_sub3;
	private Date ts;
	private String utmSource;
	/**
	 * @return the ms
	 */
	public String getMs() {
		return ms;
	}
	/**
	 * @param ms the ms to set
	 */
	public void setMs(String ms) {
		this.ms = ms;
	}
	/**
	 * @return the cs
	 */
	public String getCs() {
		return cs;
	}
	/**
	 * @param cs the cs to set
	 */
	public void setCs(String cs) {
		this.cs = cs;
	}
	/**
	 * @return the http
	 */
	public String getHttp() {
		return http;
	}
	/**
	 * @param http the http to set
	 */
	public void setHttp(String http) {
		this.http = http;
	}
	/**
	 * @return the dp
	 */
	public String getDp() {
		return dp;
	}
	/**
	 * @param dp the dp to set
	 */
	public void setDp(String dp) {
	    if(dp == null || dp.contains("null")){
            this.dp = EMPTY_STRING;
        } else {
            this.dp = dp;   
        }
	}
	/**
	 * @return the ts
	 */
	public Date getTs() {
		return ts;
	}
	/**
	 * @param ts the ts to set
	 */
	public void setTs(Date ts) {
		this.ts = ts;
	}
	
	public String getUtmSource() {
		return utmSource;
	}
	
	public void setUtmSource(String utmSource) {
		this.utmSource = utmSource;
	}
	
	public String getAff_sub1() {
		return aff_sub1;
	}
	
	public void setAff_sub1(String aff_sub1) {
		this.aff_sub1 = aff_sub1;
	}
	
	public String getAff_sub2() {
		return aff_sub2;
	}
	
	public void setAff_sub2(String aff_sub2) {
		this.aff_sub2 = aff_sub2;
	}
	
	public String getAff_sub3() {
		return aff_sub3;
	}
	
	public void setAff_sub3(String aff_sub3) {
		this.aff_sub3 = aff_sub3;
	}
	@Override
	public String toString() {
		return "MarketingTrackingCookieStatic [ms=" + ms + ", cs=" + cs
				+ ", http=" + http + ", dp=" + dp + ", aff_sub1=" + aff_sub1
				+ ", aff_sub2=" + aff_sub2 + ", aff_sub3=" + aff_sub3 + ", ts="
				+ ts + ", utmSource=" + utmSource + "]";
	}
	
	
}
