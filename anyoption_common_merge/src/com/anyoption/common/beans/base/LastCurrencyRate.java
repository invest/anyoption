package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.Expose;

/**
 * @author EranL
 *
 */
public class LastCurrencyRate implements Serializable {

	private static final long serialVersionUID = 1L;
	@Expose
	private double rate;
	private Date timeCreated;
	@Expose
	private String timeCreatedStr;
	@Expose
	private String currencyCode;	
	
	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}
	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	/**
	 * @return the rate
	 */
	public double getRate() {
		return rate;
	}
	/**
	 * @param rate the rate to set
	 */
	public void setRate(double rate) {
		this.rate = rate;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the timeCreatedStr
	 */
	public String getTimeCreatedStr() {
		return timeCreatedStr;
	}
	/**
	 * @param timeCreatedStr the timeCreatedStr to set
	 */
	public void setTimeCreatedStr(String timeCreatedStr) {
		this.timeCreatedStr = timeCreatedStr;
	}

}
