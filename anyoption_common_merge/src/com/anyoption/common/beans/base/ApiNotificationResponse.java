package com.anyoption.common.beans.base;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

/**
 * @author EranL
 *
 */
public class ApiNotificationResponse implements Serializable {

	private static final long serialVersionUID = 1L;
    @Expose
	private long id;
    @Expose
	private long status;
	private String comments;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the status
	 */
	public long getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(long status) {
		this.status = status;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

}
