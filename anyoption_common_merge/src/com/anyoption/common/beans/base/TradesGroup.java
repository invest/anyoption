package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author eranl
 *
 */
public class TradesGroup implements Serializable {

	private static final long serialVersionUID = -7789169008304807000L;
	private int startIndex;
	private int endIndex;
	private int hitRate;
	/**
	 * @return the startIndex
	 */
	public int getStartIndex() {
		return startIndex;
	}
	/**
	 * @param startIndex the startIndex to set
	 */
	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}
	/**
	 * @return the endIndex
	 */
	public int getEndIndex() {
		return endIndex;
	}
	/**
	 * @param endIndex the endIndex to set
	 */
	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}
	/**
	 * @return the hitRate
	 */
	public int getHitRate() {
		return hitRate;
	}
	/**
	 * @param hitRate the hitRate to set
	 */
	public void setHitRate(int hitRate) {
		this.hitRate = hitRate;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TradesGroup [startIndex=" + startIndex + ", endIndex="
				+ endIndex + ", hitRate=" + hitRate + "]";
	}
	
}
