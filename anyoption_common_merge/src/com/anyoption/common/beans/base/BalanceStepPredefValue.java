/**
 * 
 */
package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * Balance step predefined value suggestion
 * 
 * @author kirilim
 */
public class BalanceStepPredefValue implements Serializable {

	private static final long serialVersionUID = -4925241973221277904L;

	/**
	 * The predefined value suggestion
	 */
	private Long predefValue;

	/**
	 * The value suggestion priority
	 */
	private int priority;

	public BalanceStepPredefValue() {
		predefValue = 0l;
		priority = 0;
	}

	public BalanceStepPredefValue(int priority) {
		predefValue = 0l;
		this.priority = priority;
	}

	public BalanceStepPredefValue(long predefValue) {
		this.predefValue = predefValue;
		priority = 0;
	}

	/**
	 * @return the predefValue
	 */
	public Long getPredefValue() {
		return predefValue;
	}

	/**
	 * @param predefValue the predefValue to set
	 */
	public void setPredefValue(Long predefValue) {
		this.predefValue = predefValue;
	}

	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}
}