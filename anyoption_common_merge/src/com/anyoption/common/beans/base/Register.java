package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class contains all the information that the system need to have on the user registration like user name,
 * email, first name, last name, city, country, date of birth, mobile phone and etc.
 * This class implements all the setters and getters for this members.
 *
 *
 *
 * @author liors
 *
 */
public class Register implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1452691038644844549L;
	/**
     *
     */
    private String userName;
    /**
     *
     */
    private String password;
    /**
     *
     */
    @Deprecated
    private String password2;
    /**
     *
     */
    private String email;
    /**
     *
     */
    private long currencyId;
    /**
     *
     */
    private String firstName;
    /**
     *
     */
    private String lastName;
    /**
     *
     */
    private String street;
    /**
     *
     */
    private String streetNo;
    /**
     *
     */
    private String cityName;
    /**
     *
     */
    private long cityId;
    /**
     *
     */
    private String zipCode;
    /**
     *
     */
    private long countryId;
    /**
     *
     */
    private long languageId;
    /**
     *
     */
    private String birthYear;
    /**
     *
     */
    private String birthMonth;
    /**
     *
     */
    private String birthDay;
    /**
     *
     */
    private String mobilePhonePrefix;
    /**
     *
     */
    private String mobilePhone;
    /**
     *
     */
    private String landLinePhonePrefix;
    /**
     *
     */
    private String landLinePhone;
    /**
     *
     */
    private String gender;
    /**
     *
     */
    private String idNum;
    /**
     *
     */
    private boolean contactByEmail;
    /**
     *
     */
    private boolean contactBySms;
    /**
     *
     */
    private boolean terms;
    /**
     *
     */
    private long skinId;
    /**
     *
     */
    private String cityNameAO;
    /**
     *
     */
    private String utcOffsetCreated;
    /**
     *
     */
    private long stateId;
    /**
     *
     */
    private boolean calcalistGame;
    /**
     *
     */
    private Long combinationId;
    /**
     *
     */
    private String dynamicParam;
    /**
     *
     */
    private long contactId;
    /**
     *
     */
    private String ip;
    /**
     *
     */
    private Date timeFirstVisit;
    /**
     *
     */
    private String deviceUniqueId;
    /**
     *
     */
    private long registerAttemptId;
    /**
     *
     */
    private String userAgent;
    /**
     *
     */
    private String httpReferer;
    /**
     *
     */
	private String timeFirstVisitTxt;
    /**
    *
    */
	private String countryName;
    /**
    *
    */
	private String currencyCode;
	/**
    *
    */
	private int platformId;
	/**
    *
    */
	private String aff_sub1;
	/**
    *
    */
	private String aff_sub2;
	/**
    *
    */
	private String aff_sub3;

    /**
     * @return birthDay
     */
    public String getBirthDay() {
        return birthDay;
    }

    /**
     * @param birthDay
     */
    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    /**
     * @return userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return
     */
    @Deprecated
    public String getPassword2() {
        return password2;
    }

    /**
     * @param password2
     */
    @Deprecated
    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    /**
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return
     */
    public long getCurrencyId() {
        return currencyId;
    }

    /**
     * @param currencyId
     */
    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    /**
     * @return firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return
     */
    public String getStreetNo() {
        return streetNo;
    }

    /**
     * @param streetNo
     */
    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    /**
     * @return
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * @param cityName
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * @return
     */
    public long getCityId() {
        return cityId;
    }

    /**
     * @param cityId
     */
    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    /**
     * @return
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * @param zipCode
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * @return
     */
    public long getCountryId() {
        return countryId;
    }

    /**
     * @param countryId
     */
    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    /**
     * @return
     */
    public long getLanguageId() {
        return languageId;
    }

    /**
     * @param languageId
     */
    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }

    /**
     * @return
     */
    public String getBirthYear() {
        return birthYear;
    }

    /**
     * @param birthYear
     */
    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    /**
     * @return
     */
    public String getBirthMonth() {
        return birthMonth;
    }

    /**
     * @param birthMonth
     */
    public void setBirthMonth(String birthMonth) {
        this.birthMonth = birthMonth;
    }

    /**
     * @return
     */
    public String getMobilePhonePrefix() {
        return mobilePhonePrefix;
    }

    /**
     * @param mobilePhonePrefix
     */
    public void setMobilePhonePrefix(String mobilePhonePrefix) {
        this.mobilePhonePrefix = mobilePhonePrefix;
    }

    /**
     * @return
     */
    public String getMobilePhone() {
        return mobilePhone;
    }

    /**
     * @param mobilePhone
     */
    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    /**
     * @return
     */
    public String getLandLinePhonePrefix() {
        return landLinePhonePrefix;
    }

    /**
     * @param landLinePhonePrefix
     */
    public void setLandLinePhonePrefix(String landLinePhonePrefix) {
        this.landLinePhonePrefix = landLinePhonePrefix;
    }

    /**
     * @return
     */
    public String getLandLinePhone() {
        return landLinePhone;
    }

    /**
     * @param landLinePhone
     */
    public void setLandLinePhone(String landLinePhone) {
        this.landLinePhone = landLinePhone;
    }

    /**
     * @return
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return
     */
    public String getIdNum() {
        return idNum;
    }

    /**
     * @param idNum
     */
    public void setIdNum(String idNum) {
        this.idNum = idNum;
    }

    /**
     * @return
     */
    public boolean isContactByEmail() {
        return contactByEmail;
    }

    /**
     * @param contactByEmail
     */
    public void setContactByEmail(boolean contactByEmail) {
        this.contactByEmail = contactByEmail;
    }

    /**
     * @return
     */
    public boolean isContactBySms() {
        return contactBySms;
    }

    /**
     * @param contactBySms
     */
    public void setContactBySms(boolean contactBySms) {
        this.contactBySms = contactBySms;
    }

    /**
     * @return
     */
    public boolean isTerms() {
        return terms;
    }

    /**
     * @param terms
     */
    public void setTerms(boolean terms) {
        this.terms = terms;
    }

    /**
     * @return
     */
    public long getSkinId() {
        return skinId;
    }

    /**
     * @param skinId
     */
    public void setSkinId(long skinId) {
        this.skinId = skinId;
    }

    /**
     * @return
     */
    public Long getCombinationId() {
        return combinationId;
    }

    /**
     * @param combinationId
     */
    public void setCombinationId(Long combinationId) {
        this.combinationId = combinationId;
    }

    /**
     * @return
     */
    public String getCityNameAO() {
        return cityNameAO;
    }

    /**
     * @param cityNameAO
     */
    public void setCityNameAO(String cityNameAO) {
        this.cityNameAO = cityNameAO;
    }

    /**
     * @return
     */
    public String getUtcOffsetCreated() {
        return utcOffsetCreated;
    }

    /**
     * @param utcOffsetCreated
     */
    public void setUtcOffsetCreated(String utcOffsetCreated) {
        this.utcOffsetCreated = utcOffsetCreated;
    }

    /**
     * @return
     */
    public long getStateId() {
        return stateId;
    }

    /**
     * @param stateId
     */
    public void setStateId(long stateId) {
        this.stateId = stateId;
    }

    /**
     * @return
     */
    public boolean isCalcalistGame() {
        return calcalistGame;
    }

    /**
     * @param calcalistGame
     */
    public void setCalcalistGame(boolean calcalistGame) {
        this.calcalistGame = calcalistGame;
    }

    /**
     * @return
     */
    public String getDynamicParam() {
        return dynamicParam;
    }

    /**
     * @param dynamicParam
     */
    public void setDynamicParam(String dynamicParam) {
        this.dynamicParam = dynamicParam;
    }

    /**
     * @return
     */
    public long getContactId() {
        return contactId;
    }

    /**
     * @param contactId
     */
    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    /**
     * @return
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return
     */
    public Date getTimeFirstVisit() {
    	if (this.timeFirstVisit == null) {
    		if (!(this.timeFirstVisitTxt == null || this.timeFirstVisitTxt.length() == 0)) {
    			SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yy");
    			Date timeFirstVisitFromTxt = null;
    			try {
    				timeFirstVisitFromTxt = dateFormat.parse(this.timeFirstVisitTxt);
        			this.timeFirstVisit = timeFirstVisitFromTxt;
    			} catch (ParseException e1) {

    			}
    		}
    	}
        return this.timeFirstVisit;
    }

    /**
     * @param timeFirstVisit
     */
    public void setTimeFirstVisit(Date timeFirstVisit) {
        this.timeFirstVisit = timeFirstVisit;
    }

    /**
     * Removing leading zero
     *
     * @param phone
     * @return
     */
    public String handleLeadingZero(String phone) {
        if (null != phone && phone.indexOf("0") == 0) { // leading zero (index 0)
            phone = phone.substring(1);
        }
        return phone;
    }

    /**
	 * @return the deviceUniqueId
	 */
	public String getDeviceUniqueId() {
		return deviceUniqueId;
	}

	/**
	 * @param deviceUniqueId the deviceUniqueId to set
	 */
	public void setDeviceUniqueId(String deviceUniqueId) {
		this.deviceUniqueId = deviceUniqueId;
	}

	/**
	 * @return the registerAttemptId
	 */
	public long getRegisterAttemptId() {
		return registerAttemptId;
	}

	/**
	 * @param registerAttemptId the registerAttemptId to set
	 */
	public void setRegisterAttemptId(long registerAttemptId) {
		this.registerAttemptId = registerAttemptId;
	}

	/**
	 * @return the userAgent
	 */
	public String getUserAgent() {
		return userAgent;
	}

	/**
	 * @param userAgent the userAgent to set
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	/**
	 * @return the httpReferer
	 */
	public String getHttpReferer() {
		return httpReferer;
	}

	/**
	 * @param httpReferer the httpReferer to set
	 */
	public void setHttpReferer(String httpReferer) {
		this.httpReferer = httpReferer;
	}
	

	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	/**
	 * @return the platformId
	 */
	public int getPlatformId() {
		return platformId;
	}

	/**
	 * @param platformId the platformId to set
	 */
	public void setPlatformId(int platformId) {
		this.platformId = platformId;
	}

	public String getAff_sub1() {
		return aff_sub1;
	}

	public void setAff_sub1(String aff_sub1) {
		this.aff_sub1 = aff_sub1;
	}

	public String getAff_sub2() {
		return aff_sub2;
	}

	public void setAff_sub2(String aff_sub2) {
		this.aff_sub2 = aff_sub2;
	}

	public String getAff_sub3() {
		return aff_sub3;
	}

	public void setAff_sub3(String aff_sub3) {
		this.aff_sub3 = aff_sub3;
	}

	@Override
	public String toString() {
		return "Register [userName=" + userName + ", password=" + "*****"
				+ ", password2=" + "*****" + ", email=" + email
				+ ", currencyId=" + currencyId + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", street=" + street
				+ ", streetNo=" + streetNo + ", cityName=" + cityName
				+ ", cityId=" + cityId + ", zipCode=" + zipCode
				+ ", countryId=" + countryId + ", languageId=" + languageId
				+ ", birthYear=" + birthYear + ", birthMonth=" + birthMonth
				+ ", birthDay=" + birthDay + ", mobilePhonePrefix="
				+ mobilePhonePrefix + ", mobilePhone=" + mobilePhone
				+ ", landLinePhonePrefix=" + landLinePhonePrefix
				+ ", landLinePhone=" + landLinePhone + ", gender=" + gender
				+ ", idNum=" + idNum + ", contactByEmail=" + contactByEmail
				+ ", contactBySms=" + contactBySms + ", terms=" + terms
				+ ", skinId=" + skinId + ", cityNameAO=" + cityNameAO
				+ ", utcOffsetCreated=" + utcOffsetCreated + ", stateId="
				+ stateId + ", calcalistGame=" + calcalistGame
				+ ", combinationId=" + combinationId + ", dynamicParam="
				+ dynamicParam + ", contactId=" + contactId + ", ip=" + ip
				+ ", timeFirstVisit=" + timeFirstVisit + ", deviceUniqueId="
				+ deviceUniqueId + ", registerAttemptId=" + registerAttemptId
				+ ", userAgent=" + userAgent + ", httpReferer=" + httpReferer
				+ ", timeFirstVisitTxt=" + timeFirstVisitTxt + ", countryName="
				+ countryName + ", currencyCode=" + currencyCode
				+ ", platformId=" + platformId + ", aff_sub1=" + aff_sub1
				+ ", aff_sub2=" + aff_sub2 + ", aff_sub3=" + aff_sub3 + "]";
	}


}