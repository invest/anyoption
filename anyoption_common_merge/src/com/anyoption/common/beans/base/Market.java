package com.anyoption.common.beans.base;

import java.util.Date;

import com.google.gson.annotations.Expose;

public class Market implements java.io.Serializable, Cloneable {
	/**
	 *
	 */
	private static final long serialVersionUID = -3415585044852383959L;

	@Expose
	protected long id;
	@Expose
	protected String displayName;
	protected boolean isOptionPlusMarket;
	@Expose
	protected Long groupId;
	@Expose
	protected String groupName;
    @Expose
    protected Long exchangeId;
    /**
     * Indicates whether the market is among the top ones in its group or not
     */
    @Expose
    protected boolean topMarketForGroup;
    @Expose
    protected Long decimalPoint;
    @Expose
    protected int decimalPointSubtractDigits;
    protected int bubblesPricingLevel;
    private long productTypeId;
    @Expose
    protected Date timeCreated;
    @Expose
    protected boolean twentyFourSeven;
    private boolean newMarket;
    protected String displayNameKey;
	protected String feedName;
	protected String name;
    protected boolean haveHourly;
    protected boolean haveDaily;
    protected boolean haveWeekly;
    protected boolean haveMonthly;
    protected boolean haveQuarterly;
    protected long optionPlusMarketId;
    protected String displayGroupNameKey;

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public boolean isOptionPlusMarket() {
		return isOptionPlusMarket;
	}

	public void setOptionPlusMarket(boolean isOptionPlusMarket) {
		this.isOptionPlusMarket = isOptionPlusMarket;
	}

	@Override
	public String toString() {
		if (null != displayName) {
			return displayName + (isOptionPlusMarket ? "+" : "");
		}
		return "";
	}

	/**
	 * @return the groupId
	 */
	public Long getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

    /**
     * @return the exchangeId
     */
    public Long getExchangeId() {
        return exchangeId;
    }

    /**
     * @param exchangeId the exchangeId to set
     */
    public void setExchangeId(Long exchangeId) {
        this.exchangeId = exchangeId;
    }

	/**
	 * @return the topMarketForGroup
	 */
	public boolean isTopMarketForGroup() {
		return topMarketForGroup;
	}

	/**
	 * @param topMarketForGroup the topMarketForGroup to set
	 */
	public void setTopMarketForGroup(boolean topMarketForGroup) {
		this.topMarketForGroup = topMarketForGroup;
	}

	public Long getDecimalPoint() {
		return decimalPoint;
	}

	public void setDecimalPoint(Long decimalPoint) {
		this.decimalPoint = decimalPoint;
	}

	/**
	 * @return the decimalPointSubtractDigits
	 */
	public int getDecimalPointSubtractDigits() {
		return decimalPointSubtractDigits;
	}

	/**
	 * @param decimalPointSubtractDigits the decimalPointSubtractDigits to set
	 */
	public void setDecimalPointSubtractDigits(int decimalPointSubtractDigits) {
		this.decimalPointSubtractDigits = decimalPointSubtractDigits;
	}

	public int getBubblesPricingLevel() {
		return bubblesPricingLevel;
	}

	public void setBubblesPricingLevel(int bubblesPricingLevel) {
		this.bubblesPricingLevel = bubblesPricingLevel;
	}

	public long getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(long productTypeId) {
		this.productTypeId = productTypeId;
	}

    public boolean isNewMarket() {
    	return newMarket;
    }

	public void setNewMarket(boolean newMarket) {
		this.newMarket = newMarket;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public boolean isTwentyFourSeven() {
		return twentyFourSeven;
	}

	public void setTwentyFourSeven(boolean twentyFourSeven) {
		this.twentyFourSeven = twentyFourSeven;
	}

	public String getDisplayNameKey() {
		return displayNameKey;
	}

	public void setDisplayNameKey(String displayNameKey) {
		this.displayNameKey = displayNameKey;
	}

	public String getFeedName() {
		return feedName;
	}

	public void setFeedName(String feedName) {
		this.feedName = feedName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isHaveHourly() {
		return haveHourly;
	}

	public void setHaveHourly(boolean haveHourly) {
		this.haveHourly = haveHourly;
	}

	public boolean isHaveDaily() {
		return haveDaily;
	}

	public void setHaveDaily(boolean haveDaily) {
		this.haveDaily = haveDaily;
	}

	public boolean isHaveWeekly() {
		return haveWeekly;
	}

	public void setHaveWeekly(boolean haveWeekly) {
		this.haveWeekly = haveWeekly;
	}

	public boolean isHaveMonthly() {
		return haveMonthly;
	}

	public void setHaveMonthly(boolean haveMonthly) {
		this.haveMonthly = haveMonthly;
	}

	public long getOptionPlusMarketId() {
		return optionPlusMarketId;
	}

	public void setOptionPlusMarketId(long optionPlusMarketId) {
		this.optionPlusMarketId = optionPlusMarketId;
	}

	public String getDisplayGroupNameKey() {
		return displayGroupNameKey;
	}

	public void setDisplayGroupNameKey(String displayGroupNameKey) {
		this.displayGroupNameKey = displayGroupNameKey;
	}

	public boolean isHaveQuarterly() {
		return haveQuarterly;
	}

	public void setHaveQuarterly(boolean haveQuarterly) {
		this.haveQuarterly = haveQuarterly;
	}
}