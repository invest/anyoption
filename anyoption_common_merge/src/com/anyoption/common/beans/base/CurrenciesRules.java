package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.util.Date;

/**
 * CurrenciesRules
 * @author eyalo
 */
public class CurrenciesRules implements Serializable{

	private static final long serialVersionUID = 4659260447375124622L;
	private long id;
	private long countryId;
	private long skinId;
	private long defaultCurrencyId;
	private String comments;
	private Date timeCreated;
	
	/**
	 * CurrenciesRules
	 */
	public CurrenciesRules() {
		
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}
	
	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	
	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}
	
	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	
	/**
	 * @return the defaultCurrencyId
	 */
	public long getDefaultCurrencyId() {
		return defaultCurrencyId;
	}
	
	/**
	 * @param defaultCurrencyId the defaultCurrencyId to set
	 */
	public void setDefaultCurrencyId(long defaultCurrencyId) {
		this.defaultCurrencyId = defaultCurrencyId;
	}
	
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
}
