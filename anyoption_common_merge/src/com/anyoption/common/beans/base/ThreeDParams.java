package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 *
 * @author Eyal g
 *
 */
public class ThreeDParams implements Serializable {

	private static final long serialVersionUID = 92040560998352234L;

	private String acsUrl;
	private String paReq;
	private String termUrl;
	private String MD;
	
	private String form;
	
	public String getAcsUrl() {
		return acsUrl;
	}
	public void setAcsUrl(String acsUrl) {
		this.acsUrl = acsUrl;
	}
	public String getPaReq() {
		return paReq;
	}
	public void setPaReq(String paReq) {
		this.paReq = paReq;
	}
	public String getTermUrl() {
		return termUrl;
	}
	public void setTermUrl(String termUrl) {
		this.termUrl = termUrl;
	}
	public String getMD() {
		return MD;
	}
	public void setMD(String mD) {
		MD = mD;
	}
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
}
