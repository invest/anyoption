package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author EranL
 *
 */
public class ApiUser implements Serializable {

	private static final long serialVersionUID = 1L; 
	protected String userName;
	protected String password;
		
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
}
