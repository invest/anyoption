package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author EranL
 *
 */
public class ApiUsersGroup implements Serializable {

	private static final long serialVersionUID = 1L; 
	private long id;
	private long apiUserId;
	private long userId;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the apiUserId
	 */
	public long getApiUserId() {
		return apiUserId;
	}
	/**
	 * @param apiUserId the apiUserId to set
	 */
	public void setApiUserId(long apiUserId) {
		this.apiUserId = apiUserId;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
		
}
