package com.anyoption.common.beans.base;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

/**
 * @author EranL
 *
 */
public class FTDUser implements Serializable {

	private static final long serialVersionUID = 1L;
	@Expose
	private long userId;
	@Expose
	private long amountInUSD;
	@Expose
	private String dynamicParameter;
	@Expose
	private long transactionId;
	@Expose
	private long subAffId;
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the amountInUSD
	 */
	public long getAmountInUSD() {
		return amountInUSD;
	}
	/**
	 * @param amountInUSD the amountInUSD to set
	 */
	public void setAmountInUSD(long amountInUSD) {
		this.amountInUSD = amountInUSD;
	}
	/**
	 * @return the dynamicParameter
	 */
	public String getDynamicParameter() {
		return dynamicParameter;
	}
	/**
	 * @param dynamicParameter the dynamicParameter to set
	 */
	public void setDynamicParameter(String dynamicParameter) {
		this.dynamicParameter = dynamicParameter;
	}
	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the subAffId
	 */
	public long getSubAffId() {
		return subAffId;
	}
	/**
	 * @param subAffId the subAffId to set
	 */
	public void setSubAffId(long subAffId) {
		this.subAffId = subAffId;
	}
	
}
