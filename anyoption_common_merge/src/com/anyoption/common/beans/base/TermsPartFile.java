package com.anyoption.common.beans.base;

public class TermsPartFile {
	
	public TermsPartFile(long id, long orderNum, String fileName, String title, String html, long typeId) {
		super();
		this.id = id;
		this.orderNum = orderNum;
		this.fileName = fileName;
		this.title = title;
		this.html = html;
		this.typeId = typeId;
	}
	
	public TermsPartFile() {
	
	}
	
	
	private long id;
	private long orderNum;
	private String fileName;
	private String title;
	private String html;
	private long typeId;
	private boolean isActive;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(long orderNum) {
		this.orderNum = orderNum;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}

	public long getTypeId() {
		return typeId;
	}

	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
}