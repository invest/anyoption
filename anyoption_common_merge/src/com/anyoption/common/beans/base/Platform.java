package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author liors - one of The authors! :)
 *
 */
public class Platform implements Serializable {
	private static final long serialVersionUID = 7579598333423451607L;
	public static final int ETRADER = 1;
	public static final int ANYOPTION = 2;
	public static final int COPYOP = 3;
	public static final int OUROBORS = 4;
	
	private int id;
	private String name;
	private String comment;
	
	public Platform() {
		
	}
	
	public Platform(int id, String name, String comment) {
		this.id = id;
		this.name = name;
		this.comment = comment;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
}
