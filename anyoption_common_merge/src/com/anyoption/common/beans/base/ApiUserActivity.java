package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.util.Date;

/**
 * @author EranL
 *
 */
public class ApiUserActivity implements Serializable {

	private static final long serialVersionUID = 1L; 
	private long id;
	private int typeId;
	private long keyValue;
	private long apiUserId;
	private Date timeCreated;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the typeId
	 */
	public int getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	/**
	 * @return the keyValue
	 */
	public long getKeyValue() {
		return keyValue;
	}
	/**
	 * @param keyValue the keyValue to set
	 */
	public void setKeyValue(long keyValue) {
		this.keyValue = keyValue;
	}
	/**
	 * @return the apiUserId
	 */
	public long getApiUserId() {
		return apiUserId;
	}
	/**
	 * @param apiUserId the apiUserId to set
	 */
	public void setApiUserId(long apiUserId) {
		this.apiUserId = apiUserId;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
	    String ls = "\n";
	    return ls + "ApiUserActivity:" + ls
	        + super.toString() + ls
	        + "apiUserId: " + apiUserId + ls
	        + "keyValue: " + keyValue + ls
	    	+ "typeId: " + typeId + ls;
	}
	
}
