package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author pavelhe
 *
 */
public class QuestionnaireQuestion implements Serializable {

	public static final long WITHDRAW_SURVEY_QUESTION = 94;
	
	
	public enum QuestionType { 
		DROPDOWN, CHECKBOX, INPUT;
		public static QuestionType get(int k) {
			switch(k){
				case 1:
					return DROPDOWN;
				case 2:
					return CHECKBOX;
				case 3:
					return INPUT;
				default:
					return null;
			}
		}
	};
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	private long groupId;
	private String name;
	private String translatation;
	private boolean active;
	private boolean mandatory;
	private long orderId;
	private int screen;
	private long defaultAnswerId;
	private QuestionType questionType;
	
	private List<QuestionnaireAnswer> answers = new ArrayList<QuestionnaireAnswer>();
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the groupId
	 */
	public long getGroupId() {
		return groupId;
	}
	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	/**
	 * @return the mandatory
	 */
	public boolean isMandatory() {
		return mandatory;
	}
	/**
	 * @param mandatory the mandatory to set
	 */
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	/**
	 * @return the orderId
	 */
	public long getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the answers
	 */
	public List<QuestionnaireAnswer> getAnswers() {
		return answers;
	}
	/**
	 * @param answers the answers to set
	 */
	public void setAnswers(List<QuestionnaireAnswer> answers) {
		this.answers = answers;
	}
	
	public String getTranslatation() {
		return translatation;
	}
	public void setTranslatation(String translatation) {
		this.translatation = translatation;
	}
	public int getScreen() {
		return screen;
	}
	public void setScreen(int screen) {
		this.screen = screen;
	}
	public QuestionType getQuestionType() {
		return questionType;
	}
	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}
	public long getDefaultAnswerId() {
		return defaultAnswerId;
	}
	public void setDefaultAnswerId(long defaultAnswerId) {
		this.defaultAnswerId = defaultAnswerId;
	}
	@Override
	public String toString() {
		return "QuestionnaireQuestion [id=" + id + ", groupId=" + groupId + ", name=" + name + ", translatation="
				+ translatation + ", active=" + active + ", mandatory=" + mandatory + ", orderId=" + orderId
				+ ", screen=" + screen + ", defaultAnswerId=" + defaultAnswerId + ", questionType=" + questionType
				+ ", answers=" + answers + "]";
	}
	

}
