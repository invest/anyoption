package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.google.gson.annotations.Expose;

/**
 * @author kirilim
 */
public class Investment implements Serializable {

	private static final long serialVersionUID = 8082591603388756872L;

	// TODO kirilim refactor all investment constants
    public static final int INVESTMENT_ONE_TOUCH_WIN = 1;
    public static final long INVESTMENT_ONE_TOUCH_UP = 1;
    public static final long INVESTMENT_ONE_TOUCH_DOWN = 0;
    public static final long INVESTMENT_TYPE_ONE_DECIMAL = 2;
    
	public static final int INVESTMENT_TYPE_CALL = 1;
    public static final int INVESTMENT_TYPE_PUT = 2;
    public static final int INVESTMENT_TYPE_ONE = 3;
    public static final int INVESTMENT_ONE_TOUCH_PUT = 0;
    public static final int INVESTMENT_ONE_TOUCH_CALL = 1;
	public static final int INVESTMENT_TYPE_BUY = 4;
	public static final int INVESTMENT_TYPE_SELL = 5;
	public static final int INVESTMENT_TYPE_BUBBLES = 6;
	public static final int INVESTMENT_TYPE_DYNAMICS_BUY = 7;
	public static final int INVESTMENT_TYPE_DYNAMICS_SELL = 8;

    //insurance types
    public static int INSURANCE_TAKE_PROFIT = 1;
    public static int INSURANCE_ROLL_FORWARD = 2;

    //additional info
    public final static int INVESTMENT_ADDITIONAL_INFO_RU = 1;
    public final static int INVESTMENT_ADDITIONAL_INFO_RU_BOUGHT = 2;
    public final static int INVESTMENT_ADDITIONAL_INFO_GM = 3;
    public final static int INVESTMENT_ADDITIONAL_INFO_BONUS = 4;
    public final static int INVESTMENT_ADDITIONAL_INFO_OPTION_PLUS = 5;
    public final static int INVESTMENT_ADDITIONAL_INFO_NOT_HOURLY = 6; //one touch or daily/weekly/monthly
    public static final long TYPE_BUBBLE = 6;

    @Expose
    protected long id;
    protected long opportunityId;
    @Expose
    protected long typeId;
    protected long amount;
    @Expose
    protected Double currentLevel;
    @Expose
    private double eventLevel;
    protected double oddsWin;
    protected double oddsLose;
    protected Date timeCreated;
    @Expose
    protected long marketId;
    @Expose
    protected String marketName;
    protected Date timeEstClosing;
    @Expose
    protected String timeEstClosingTxt;
	protected long insuranceAmountRU;
	@Expose
    protected String asset; // etrader_common ?
    @Expose
    protected String level; // etrader_common ?
    @Expose
    protected String expiryLevel;
    protected Calendar timePurchased;
    @Expose
    protected String timePurchaseTxt;
    @Expose
    protected String amountTxt;
    @Expose
    protected String amountReturnWF;
    @Expose
    protected String currentLevelTxt;
    protected int oneTouchUpDown = -1;
    protected long optionPlusFee;
    protected String additionalInfoText;  // GM/RU/BONUS/O+
    protected int additionalInfoType; // GM/RU/BONUS/O+
    protected String bonusDisplayName;
    protected long opportunityTypeId;
	protected long scheduledId;
    
    @Expose
    protected long timeEstClosingMillsec;
    @Expose
    protected long timePurchasedMillsec;

//  for api external user
    private long apiExternalUserId;
    
    protected Date timeSettled;
    protected CopyOpInvTypeEnum copyopType;
    
    // bubble
    
    private double bubbleLowLevel;
    private double bubbleHighLevel;
    private Date bubbleStartTime;
    private Date bubbleEndTime;
    private double price;
	protected int isCanceled;
	
	private Double bubblesClosingLevel;

	/**
     * @return the oneTouchUpDown
     */
    public int getOneTouchUpDown() {
        return oneTouchUpDown;
    }

    /**
	 * @return the optionPlusFee
	 */
	public long getOptionPlusFee() {
		return optionPlusFee;
	}

	/**
	 * @param optionPlusFee the optionPlusFee to set
	 */
	public void setOptionPlusFee(long optionPlusFee) {
		this.optionPlusFee = optionPlusFee;
	}

	/**
     * @param oneTouchUpDown the oneTouchUpDown to set
     */
    public void setOneTouchUpDown(int oneTouchUpDown) {
        this.oneTouchUpDown = oneTouchUpDown;
    }

    /**
	 * @return the currentLevelTxt
	 */
	public String getCurrentLevelTxt() {
		return currentLevelTxt;
	}

	/**
	 * @param currentLevelTxt the currentLevelTxt to set
	 */
	public void setCurrentLevelTxt(String currentLevelTxt) {
		this.currentLevelTxt = currentLevelTxt;
	}

	// Mobile client related
    protected float chartXPosition;


    public String getTimeEstClosingTxt() {
		return timeEstClosingTxt;
	}

	public void setTimeEstClosingTxt(String timeEstClosingTxt) {
		this.timeEstClosingTxt = timeEstClosingTxt;
	}

    public String getAsset() {
		return asset;
	}

	public void setAsset(String asset) {
		this.asset = asset;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getExpiryLevel() {
		return expiryLevel;
	}

	public void setExpiryLevel(String expiryLevel) {
		this.expiryLevel = expiryLevel;
	}

	public String getAmountTxt() {
		return amountTxt;
	}

	public void setAmountTxt(String amountTxt) {
		this.amountTxt = amountTxt;
	}

	public String getAmountReturnWF() {
		return amountReturnWF;
	}

	public void setAmountReturnWF(String amountReturnWF) {
		this.amountReturnWF = amountReturnWF;
	}

	public String getTimePurchaseTxt() {
		return timePurchaseTxt;
	}

	public void setTimePurchaseTxt(String timePurchaseTxt) {
		this.timePurchaseTxt = timePurchaseTxt;
	}

	public Calendar getTimePurchased() {
		return timePurchased;
	}

	public void setTimePurchased(Calendar timePurchased) {
		this.timePurchased = timePurchased;
	}

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityId(long opportunityId) {
        this.opportunityId = opportunityId;
    }

    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public Double getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(Double currentLevel) {
        this.currentLevel = currentLevel;
    }

    public double getOddsWin() {
        return oddsWin;
    }

    public void setOddsWin(double oddsWin) {
        this.oddsWin = oddsWin;
    }

    public double getOddsLose() {
        return oddsLose;
    }

    public void setOddsLose(double oddsLose) {
        this.oddsLose = oddsLose;
    }

    public Date getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Date timeCreated) {
        this.timeCreated = timeCreated;
    }

    public long getMarketId() {
        return marketId;
    }

    public void setMarketId(long marketId) {
        this.marketId = marketId;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public Date getTimeEstClosing() {
        return timeEstClosing;
    }

    public void setTimeEstClosing(Date timeEstClosing) {
        this.timeEstClosing = timeEstClosing;
    }

    public long getInsuranceAmountRU() {
        return insuranceAmountRU;
    }

    public void setInsuranceAmountRU(long insuranceAmountRU) {
        this.insuranceAmountRU = insuranceAmountRU;
    }

    public float getChartXPosition() {
        return chartXPosition;
    }

    public void setChartXPosition(float chartXPosition) {
        this.chartXPosition = chartXPosition;
    }

	public String getAdditionalInfoText() {
		return additionalInfoText;
	}

	public void setAdditionalInfoText(String additionalInfoText) {
		this.additionalInfoText = additionalInfoText;
	}

	public int getAdditionalInfoType() {
		return additionalInfoType;
	}

	public void setAdditionalInfoType(int additionalInfoType) {
		this.additionalInfoType = additionalInfoType;
	}

	public String getBonusDisplayName() {
		return bonusDisplayName;
	}

	public void setBonusDisplayName(String bonusDisplayName) {
		this.bonusDisplayName = bonusDisplayName;
	}

	public long getOpportunityTypeId() {
		return opportunityTypeId;
	}

	public void setOpportunityTypeId(long opportunityTypeId) {
		this.opportunityTypeId = opportunityTypeId;
	}
	
	/**
	 * @return the timeEstClosingMillsec
	 */
	public long getTimeEstClosingMillsec() {
		return timeEstClosingMillsec;
	}

	/**
	 * @param timeEstClosingMillsec the timeEstClosingMillsec to set
	 */
	public void setTimeEstClosingMillsec(long timeEstClosingMillsec) {
		this.timeEstClosingMillsec = timeEstClosingMillsec;
	}

	/**
	 * @return the timePurchasedMillsec
	 */
	public long getTimePurchasedMillsec() {
		return timePurchasedMillsec;
	}

	/**
	 * @param timePurchasedMillsec the timePurchasedMillsec to set
	 */
	public void setTimePurchasedMillsec(long timePurchasedMillsec) {
		this.timePurchasedMillsec = timePurchasedMillsec;
	}


	/**
	 * @return the apiExternalUserId
	 */
	public long getApiExternalUserId() {
		return apiExternalUserId;
	}

	/**
	 * @param apiExternalUserId the apiExternalUserId to set
	 */
	public void setApiExternalUserId(long apiExternalUserId) {
		this.apiExternalUserId = apiExternalUserId;
	}

	public Date getTimeSettled() {
		return timeSettled;
	}

	public void setTimeSettled(Date timeSettled) {
		this.timeSettled = timeSettled;
	}

	/**
	 * @return the copyopTypeId
	 */
	public CopyOpInvTypeEnum getCopyopType() {
		return copyopType;
	}

	/**
	 * @param copyopTypeId the copyopTypeId to set
	 */
	public void setCopyopType(CopyOpInvTypeEnum copyopType) {
		this.copyopType = copyopType;
	}

	public double getBubbleLowLevel() {
		return bubbleLowLevel;
	}

	public void setBubbleLowLevel(Double bubbleLowLevel) {
		this.bubbleLowLevel = bubbleLowLevel;
	}

	public double getBubbleHighLevel() {
		return bubbleHighLevel;
	}

	public void setBubbleHighLevel(Double bubbleHighLevel) {
		this.bubbleHighLevel = bubbleHighLevel;
	}

	public Date getBubbleStartTime() {
		return bubbleStartTime;
	}

	public void setBubbleStartTime(Date bubbleStartTime) {
		this.bubbleStartTime = bubbleStartTime;
	}

	public Date getBubbleEndTime() {
		return bubbleEndTime;
	}

	public void setBubbleEndTime(Date bubbleEndTime) {
		this.bubbleEndTime = bubbleEndTime;
	}

	public double getBubbleReturnToWin() {
		return amount * (1 + getOddsWin());
	}

	public double getBubbleReturnToLose() {
		return amount * (1 - getOddsLose());
	}
	
	public double getEventLevel() {
		return eventLevel;
	}

	public void setEventLevel(double eventLevel) {
		this.eventLevel = eventLevel;
	}
	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getIsCanceled() {
		return isCanceled;
	}

	public void setIsCanceled(int isCanceled) {
		this.isCanceled = isCanceled;
	}

	
	
	public Double getBubblesClosingLevel() {
		return bubblesClosingLevel;
	}

	public void setBubblesClosingLevel(Double bubblesClosingLevel) {
		this.bubblesClosingLevel = bubblesClosingLevel;
	}

	public long getScheduledId() {
		return scheduledId;
	}

	public void setScheduledId(long scheduledId) {
		this.scheduledId = scheduledId;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "com.anyoption.common.beans.base.Investment: " + ls
				+ super.toString() + ls
				+ "id: " + id + ls
				+ "opportunityId: " + opportunityId + ls
				+ "typeId: " + typeId + ls
				+ "amount: " + amount + ls
				+ "isCanceled: " + isCanceled + ls
				+ "currentLevel: " + currentLevel + ls
				+ "currentLevel: " + eventLevel + ls
				+ "oddsWin: " + oddsWin + ls
				+ "oddsLose: " + oddsLose + ls
				+ "timeCreated: " + timeCreated + ls
				+ "marketId: " + marketId + ls
				+ "marketName: " + marketName + ls
				+ "timeEstClosing: " + timeEstClosing + ls
				+ "timeEstClosingTxt: " + timeEstClosingTxt + ls
				+ "insuranceAmountRU: " + insuranceAmountRU + ls
				+ "asset: " + asset + ls
				+ "level: " + level + ls
				+ "expiryLevel: " + expiryLevel + ls
				+ "timePurchased: " + timePurchased + ls
				+ "timePurchaseTxt: " + timePurchaseTxt + ls
				+ "amountTxt: " + amountTxt + ls
				+ "amountReturnWF: " + amountReturnWF + ls
				+ "currentLevelTxt: " + currentLevelTxt + ls
				+ "oneTouchUpDown: " + oneTouchUpDown + ls
				+ "optionPlusFee: " + optionPlusFee + ls
				+ "additionalInfoText: " + additionalInfoText + ls
				+ "additionalInfoType: " + additionalInfoType + ls
				+ "bonusDisplayName: " + bonusDisplayName + ls
				+ "opportunityTypeId: " + opportunityTypeId + ls
				+ "timeEstClosingMillsec: " + timeEstClosingMillsec + ls
				+ "timePurchasedMillsec: " + timePurchasedMillsec + ls
				+ "apiExternalUserId: " + apiExternalUserId + ls
				+ "timeSettled: " + timeSettled + ls
	            + "copyopType: " + copyopType + ls
	            + "bubbleLowLevel: " + bubbleLowLevel + ls
	            + "bubbleHighLevel: " + bubbleHighLevel + ls
	            + "bubbleStartTime: " + bubbleStartTime + ls
	            + "bubbleEndTime: " + bubbleEndTime + ls
	            + "bubblesClosingLevel : " + bubblesClosingLevel + ls
	            + "price:" + price + ls
	            + "scheduledId:" + scheduledId + ls;
	}
}