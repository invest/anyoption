package com.anyoption.common.beans.base;

public class WithdrawUserDetails {
	
	private Long id;
	private Long userId;
	private String beneficiaryName;
	private String swift;
	private String iban;
	private Long accountNum;
	private String bankName;
	private Long branchNumber;
	private String branchAddress;
	private Long fileId;
	private String fileName;
	private String skrillEmail;
	private Long createdWriterId;
	private Long updatedWriterId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getSwift() {
		return swift;
	}

	public void setSwift(String swift) {
		this.swift = swift;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public Long getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(Long accountNum) {
		this.accountNum = accountNum;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Long getBranchNumber() {
		return branchNumber;
	}

	public void setBranchNumber(Long branchNumber) {
		this.branchNumber = branchNumber;
	}

	public String getBranchAddress() {
		return branchAddress;
	}

	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getSkrillEmail() {
		return skrillEmail;
	}

	public void setSkrillEmail(String skrillEmail) {
		this.skrillEmail = skrillEmail;
	}

	public Long getCreatedWriterId() {
		return createdWriterId;
	}

	public void setCreatedWriterId(Long createdWriterId) {
		this.createdWriterId = createdWriterId;
	}
	
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "WithdrawUserDetails: "
            + super.toString() + ls
            + "id: " + id + ls
            + "userId: " + userId + ls
            + "beneficiaryName: " + beneficiaryName + ls
            + "swift: " + swift + ls
            + "iban: " + iban + ls
            + "accountNum: " + accountNum + ls
            + "bankName: " + bankName + ls
            + "branchNumber: " + branchNumber + ls
            + "branchAddress: " + branchAddress + ls
            + "fileId: " + fileId + ls
            + "skrillEmail: " + skrillEmail + ls
            + "fileName: " + fileName + ls
            + "createdWriterId: " + createdWriterId + ls;
	}

	public Long getUpdatedWriterId() {
		return updatedWriterId;
	}

	public void setUpdatedWriterId(Long updatedWriterId) {
		this.updatedWriterId = updatedWriterId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
