package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * State vo class
 *
 * @author Kobi
 */
public class State implements Serializable {
	protected long id;
	protected String name;

	public State() {}

	public State(long id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}