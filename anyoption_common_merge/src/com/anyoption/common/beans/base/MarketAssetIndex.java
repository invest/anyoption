package com.anyoption.common.beans.base;

import java.io.Serializable;

import com.anyoption.common.beans.base.Market;

public class MarketAssetIndex  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Market market;
	private long opportunityTypeId;
	private long marketGroupId;
	private long marketDisplayGroupId;
	private MarketAssetIndexInfo marketAssetIndexInfo;
	
	public Market getMarket() {
		return market;
	}
	
	public void setMarket(Market market) {
		this.market = market;
	}
	
	public long getOpportunityTypeId() {
		return opportunityTypeId;
	}
	
	public void setOpportunityTypeId(long opportunityTypeId) {
		this.opportunityTypeId = opportunityTypeId;
	}
	
	public long getMarketGroupId() {
		return marketGroupId;
	}
	
	public void setMarketGroupId(long marketGroupId) {
		this.marketGroupId = marketGroupId;
	}
	
	public long getMarketDisplayGroupId() {
		return marketDisplayGroupId;
	}
	
	public void setMarketDisplayGroupId(long marketDisplayGroupId) {
		this.marketDisplayGroupId = marketDisplayGroupId;
	}

	public MarketAssetIndexInfo getMarketAssetIndexInfo() {
		return marketAssetIndexInfo;
	}
	
	public void setMarketAssetIndexInfo(MarketAssetIndexInfo marketAssetIndexInfo) {
		this.marketAssetIndexInfo = marketAssetIndexInfo;
	}
}
