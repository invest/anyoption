package com.anyoption.common.beans.base;

import java.io.Serializable;

public class Language implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected long id;
    protected String code;
    protected String displayName;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
}