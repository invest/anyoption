package com.anyoption.common.beans.base;

import java.util.ArrayList;

public class AcademyArticle {

	private long id;
	private ArrayList<Long> topicIds;
	private Long homePagePosition;
	private String imageUrl;
	private Long timeCreated;
	private Long timeUpdated;
	private boolean isActive;
	private boolean isDeleted;
	private ArrayList<AcademyArticleTranslation> translations;
	
	public AcademyArticle() {
		topicIds = new ArrayList<Long>();
		translations = new ArrayList<AcademyArticleTranslation>();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ArrayList<Long> getTopicIds() {
		return topicIds;
	}

	public void setTopicIds(ArrayList<Long> topicIds) {
		this.topicIds = topicIds;
	}
	
	public Long getHomePagePosition() {
		return homePagePosition;
	}

	public void setHomePagePosition(Long homePagePosition) {
		this.homePagePosition = homePagePosition;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Long getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Long timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Long getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Long timeUpdated) {
		this.timeUpdated = timeUpdated;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public ArrayList<AcademyArticleTranslation> getTranslations() {
		return translations;
	}

	public void setTranslations(ArrayList<AcademyArticleTranslation> translations) {
		this.translations = translations;
	}
}
