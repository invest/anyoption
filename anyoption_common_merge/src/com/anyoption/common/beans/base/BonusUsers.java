package com.anyoption.common.beans.base;

import java.util.ArrayList;
import java.util.Date;

import com.anyoption.common.util.ConstantsBase;


/**
 * BonusUsers vo class.
 * @author Kobi
 */
public class BonusUsers implements java.io.Serializable{
	protected static final long serialVersionUID = 5289496849546211085L;

	protected long id;
	protected long userId;
	protected long bonusId;
	protected long userActionId;
	protected Date startDate;
	protected Date endDate;
	protected long bonusStateId;
	protected long bonusAmount;
	protected double bonusPercent;
	protected long minDepositAmount;
	protected long maxDepositAmount;
	protected long sumInvQualify;
	protected long writerId;
	protected Date timeCreated;
	protected String utcOffsetUser;
	protected long sumInvQualifyReached;
	protected long sumInvWithdrawal;
	protected long sumInvWithdrawalReached;
	protected Date timeActivated;
	protected Date timeUsed;
	protected Date timeDone;
	protected Date timeRefused;
	protected Date timeCanceled;
	protected Date timeWageringWaived;
	protected Date timeWithdrawn;
	protected long typeId;
	protected long wageringParam;
	protected long numOfActions;
	protected long numOfActionsReached;
	protected double oddsWin;
	protected double oddsLose;
	protected long minInvestAmount;
	protected long maxInvestAmount;
	protected long sumDepositsReached;
	protected long sumDeposits;
	protected long activatedTransactionId;
	protected long activatedInvestmentId;
	protected long wageringEendInvestmentId;
	protected long turnOverParamOptionPlus;
	protected long turnOverParamBinaryOneHundred;

	protected String bonusName;
	protected String bonusTypeName;
	protected String bonusStateName;
	protected String userName;
	protected String userAction;
	protected String writerName;
	protected String bonusStateDescription;
	protected Currency currency;

	protected long bonusClassType;
	protected long investmentId;
	protected boolean stateExpired;
	protected boolean isHasSteps;
	protected ArrayList<BonusUsersStep> bonusSteps;
	
	protected long turnoverParam;
	protected long adjustedAmount;
	protected long userSkinId;
	protected long writerIdCancel;
	// bonus deposit
	protected String comments;
	protected boolean bonusOddsWin;

	protected String bonusStateTxt;
	protected String bonusDescriptionTxt;
	protected String endDateTxt;
	protected String startDateTxt;
	protected String generalTermsCaption;
	
	public BonusUsers() {
		utcOffsetUser = null;
		setBonusOddsWin(false);
	}
	
	public String getBonusStateTxt() {
		return bonusStateTxt;
	}
	public void setBonusStateTxt(String bonusStateTxt) {
		this.bonusStateTxt = bonusStateTxt;
	}
	public String getBonusDescriptionTxt() {
		return bonusDescriptionTxt;
	}
	public void setBonusDescriptionTxt(String bonusDescriptionTxt) {
		this.bonusDescriptionTxt = bonusDescriptionTxt;
	}
	public String getEndDateTxt() {
		return endDateTxt;
	}
	public void setEndDateTxt(String endDateTxt) {
		this.endDateTxt = endDateTxt;
	}
	public String getStartDateTxt() {
		return startDateTxt;
	}
	public void setStartDateTxt(String startDateTxt) {
		this.startDateTxt = startDateTxt;
	}
	public String getGeneralTermsCaption() {
		return generalTermsCaption;
	}
	public void setGeneralTermsCaption(String generalTermsCaption) {
		this.generalTermsCaption = generalTermsCaption;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the bonusStateDescription
	 */
	public String getBonusDescription() {
		return bonusStateDescription;
	}

	/**
	 * @param bonusStateDescription the bonusStateDescription to set
	 */
	public void setBonusStateDescription(String bonusStateDescription) {
		this.bonusStateDescription = bonusStateDescription;
	}

	/**
	 * @return the bonusAmount
	 */
	public long getBonusAmount() {
		return bonusAmount;
	}


	/**
	 * @param bonusAmount the bonusAmount to set
	 */
	public void setBonusAmount(long bonusAmount) {
		this.bonusAmount = bonusAmount;
	}


	/**
	 * @return the bonusId
	 */
	public long getBonusId() {
		return bonusId;
	}


	/**
	 * @param bonusId the bonusId to set
	 */
	public void setBonusId(long bonusId) {
		this.bonusId = bonusId;
	}


	/**
	 * @return the bonusPercent
	 */
	public double getBonusPercent() {
		return bonusPercent;
	}


	/**
	 * @param bonusPercent the bonusPercent to set
	 */
	public void setBonusPercent(double bonusPercent) {
		this.bonusPercent = bonusPercent;
	}


	/**
	 * @return the bonusStateId
	 */
	public long getBonusStateId() {
		return bonusStateId;
	}


	/**
	 * @param bonusStateId the bonusStateId to set
	 */
	public void setBonusStateId(long bonusStateId) {
		this.bonusStateId = bonusStateId;
	}


	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}


	/**
	 * @return the maxDepositAmount
	 */
	public long getMaxDepositAmount() {
		return maxDepositAmount;
	}


	/**
	 * @param maxDepositAmount the maxDepositAmount to set
	 */
	public void setMaxDepositAmount(long maxDepositAmount) {
		this.maxDepositAmount = maxDepositAmount;
	}

	/**
	 * @return the minDepositAmount
	 */
	public long getMinDepositAmount() {
		return minDepositAmount;
	}

	/**
	 * @param minDepositAmount the minDepositAmount to set
	 */
	public void setMinDepositAmount(long minDepositAmount) {
		this.minDepositAmount = minDepositAmount;
	}


	/**
	 * @return the numOfActions
	 */
	public long getNumOfActions() {
		return numOfActions;
	}


	/**
	 * @param numOfActions the numOfActions to set
	 */
	public void setNumOfActions(long numOfActions) {
		this.numOfActions = numOfActions;
	}


	/**
	 * @return the numOfActionsReached
	 */
	public long getNumOfActionsReached() {
		return numOfActionsReached;
	}


	/**
	 * @param numOfActionsReached the numOfActionsReached to set
	 */
	public void setNumOfActionsReached(long numOfActionsReached) {
		this.numOfActionsReached = numOfActionsReached;
	}


	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	/**
	 * @return the sumInvQualify
	 */
	public long getSumInvQualify() {
		return sumInvQualify;
	}


	/**
	 * @param sumInvQualify the sumInvQualify to set
	 */
	public void setSumInvQualify(long sumInvQualify) {
		this.sumInvQualify = sumInvQualify;
	}


	/**
	 * @return the sumInvQualifyReached
	 */
	public long getSumInvQualifyReached() {
		return sumInvQualifyReached;
	}


	/**
	 * @param sumInvQualifyReached the sumInvQualifyReached to set
	 */
	public void setSumInvQualifyReached(long sumInvQualifyReached) {
		this.sumInvQualifyReached = sumInvQualifyReached;
	}


	/**
	 * @return the sumInvWithdrawal
	 */
	public long getSumInvWithdrawal() {
		return sumInvWithdrawal;
	}


	/**
	 * @param sumInvWithdrawal the sumInvWithdrawal to set
	 */
	public void setSumInvWithdrawal(long sumInvWithdrawal) {
		this.sumInvWithdrawal = sumInvWithdrawal;
	}


	/**
	 * @return the sumInvWithdrawalReached
	 */
	public long getSumInvWithdrawalReached() {
		return sumInvWithdrawalReached;
	}


	/**
	 * @param sumInvWithdrawalReached the sumInvWithdrawalReached to set
	 */
	public void setSumInvWithdrawalReached(long sumInvWithdrawalReached) {
		this.sumInvWithdrawalReached = sumInvWithdrawalReached;
	}


	/**
	 * @return the timeActivated
	 */
	public Date getTimeActivated() {
		return timeActivated;
	}

	/**
	 * @param timeActivated the timeActivated to set
	 */
	public void setTimeActivated(Date timeActivated) {
		this.timeActivated = timeActivated;
	}


	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}


	/**
	 * @return the timeDone
	 */
	public Date getTimeDone() {
		return timeDone;
	}

	/**
	 * @param timeDone the timeDone to set
	 */
	public void setTimeDone(Date timeDone) {
		this.timeDone = timeDone;
	}


	/**
	 * @return the timeUsed
	 */
	public Date getTimeUsed() {
		return timeUsed;
	}

	/**
	 * @param timeUsed the timeUsed to set
	 */
	public void setTimeUsed(Date timeUsed) {
		this.timeUsed = timeUsed;
	}


	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}


	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}


	/**
	 * @return the userActionId
	 */
	public long getUserActionId() {
		return userActionId;
	}


	/**
	 * @param userActionId the userActionId to set
	 */
	public void setUserActionId(long userActionId) {
		this.userActionId = userActionId;
	}


	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}


	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}


	/**
	 * @return the wageringParam
	 */
	public long getWageringParam() {
		return wageringParam;
	}


	/**
	 * @param wageringParam the wageringParam to set
	 */
	public void setWageringParam(long wageringParam) {
		this.wageringParam = wageringParam;
	}


	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}


	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}


	/**
	 * @return the bonusName
	 */
	public String getBonusName() {
		return bonusName;
	}


	/**
	 * @param bonusName the bonusName to set
	 */
	public void setBonusName(String bonusName) {
		this.bonusName = bonusName;
	}


	/**
	 * @return the bonusStateName
	 */
	public String getBonusStateName() {
		return bonusStateName;
	}


	/**
	 * @param bonusStateName the bonusStateName to set
	 */
	public void setBonusStateName(String bonusStateName) {
		this.bonusStateName = bonusStateName;
	}


	/**
	 * @return the bonusTypeName
	 */
	public String getBonusTypeName() {
		return bonusTypeName;
	}


	/**
	 * @param bonusTypeName the bonusTypeName to set
	 */
	public void setBonusTypeName(String bonusTypeName) {
		this.bonusTypeName = bonusTypeName;
	}


	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}


	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}


	/**
	 * @return the userAction
	 */
	public String getUserAction() {
		return userAction;
	}


	/**
	 * @param userAction the userAction to set
	 */
	public void setUserAction(String userAction) {
		this.userAction = userAction;
	}


	/**
	 * @return the writerName
	 */
	public String getWriterName() {
		return writerName;
	}


	/**
	 * @param writerName the writerName to set
	 */
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	/**
	 * Can cancel bonuse
	 * @return
	 */
	public boolean isCanCanceled() {
		if (bonusStateId == ConstantsBase.BONUS_STATE_USED || 
				bonusStateId == ConstantsBase.BONUS_STATE_GRANTED || 
				bonusStateId == ConstantsBase.BONUS_STATE_PENDING ||
				bonusStateId == ConstantsBase.BONUS_STATE_ACTIVE &&
				typeId != ConstantsBase.BONUS_TYPE_CONVERT_POINTS_TO_CASH) {
			return true;
		}
		return false;
	}

	/**
	 * @return the currency
	 */
	public Currency getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	/**
	 * @return the bonusClassType
	 */
	public long getBonusClassType() {
		return bonusClassType;
	}

	/**
	 * @param bonusClassType the bonusClassType to set
	 */
	public void setBonusClassType(long bonusClassType) {
		this.bonusClassType = bonusClassType;
	}

	/**
	 * @return the investmentId
	 */
	public long getInvestmentId() {
		return investmentId;
	}

	/**
	 * @param investmentId the investmentId to set
	 */
	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}

	/**
	 * @return the stateExpired
	 */
	public boolean isStateExpired() {
		return stateExpired;
	}

	/**
	 * @param stateExpired the stateExpired to set
	 */
	public void setStateExpired(boolean stateExpired) {
		this.stateExpired = stateExpired;
	}

	/**
	 * @param stateExpired the stateExpired to set
	 */
	public void setStateExpired() {
		stateExpired = endDate.before(new Date()) && this.bonusStateId < 3;
	}

	public boolean isBonusExpired(){
		return this.bonusStateId == 6;
	}

	/**
	 * @return the isHasSteps
	 */
	public boolean isHasSteps() {
		return isHasSteps;
	}

	/**
	 * @param isHasSteps the isHasSteps to set
	 */
	public void setHasSteps(boolean isHasSteps) {
		this.isHasSteps = isHasSteps;
	}

	public ArrayList<BonusUsersStep> getBonusSteps() {
		return bonusSteps;
	}

	public void setBonusSteps(ArrayList<BonusUsersStep> bonusSteps) {
		this.bonusSteps = bonusSteps;
	}

	public String getBonusStepsForHTML(){
		String retVal = "";
		if(this.isHasSteps() && bonusSteps != null){
			for(BonusUsersStep bs:this.bonusSteps){
				String tmp = bs.getBonusPercent()+"#"+bs.getMinDepositAmount()+"#"+bs.getMaxDepositAmount();
				retVal += tmp+",";
			}
			retVal = retVal.substring(0, retVal.length()-1);
		}

		return retVal;
	}

	/**
	 * @return the maxInvestAmount
	 */
	public long getMaxInvestAmount() {
		return maxInvestAmount;
	}

	/**
	 * @param maxInvestAmount the maxInvestAmount to set
	 */
	public void setMaxInvestAmount(long maxInvestAmount) {
		this.maxInvestAmount = maxInvestAmount;
	}

	/**
	 * @return the minInvestAmount
	 */
	public long getMinInvestAmount() {
		return minInvestAmount;
	}

	/**
	 * @param minInvestAmount the minInvestAmount to set
	 */
	public void setMinInvestAmount(long minInvestAmount) {
		this.minInvestAmount = minInvestAmount;
	}

	/**
	 * @return the oddsLose
	 */
	public double getOddsLose() {
		return oddsLose;
	}

	/**
	 * @param oddsLose the oddsLose to set
	 */
	public void setOddsLose(double oddsLose) {
		this.oddsLose = oddsLose;
	}

	/**
	 * @return the oddsWin
	 */
	public double getOddsWin() {
		return oddsWin;
	}

	/**
	 * @param oddsWin the oddsWin to set
	 */
	public void setOddsWin(double oddsWin) {
		this.oddsWin = oddsWin;
	}


	/**
	 * @return the sumDepositsReached
	 */
	public long getSumDepositsReached() {
		return sumDepositsReached;
	}

	/**
	 * @param sumDepositsReached the sumDepositsReached to set
	 */
	public void setSumDepositsReached(long sumDepositsReached) {
		this.sumDepositsReached = sumDepositsReached;
	}


	/**
	 * @return the sumDeposits
	 */
	public long getSumDeposits() {
		return sumDeposits;
	}

	/**
	 * @param sumDeposits the sumDeposits to set
	 */
	public void setSumDeposits(long sumDeposits) {
		this.sumDeposits = sumDeposits;
	}

	public String getUtcOffsetUser() {
		return utcOffsetUser;
	}

	public void setUtcOffsetUser(String utcOffsetCreated) {
		this.utcOffsetUser = utcOffsetCreated;
	}

	public long getActivatedInvestmentId() {
		return activatedInvestmentId;
	}

	public void setActivatedInvestmentId(long activatedInvestmentId) {
		this.activatedInvestmentId = activatedInvestmentId;
	}

	public long getActivatedTransactionId() {
		return activatedTransactionId;
	}

	public void setActivatedTransactionId(long activatedTransactionId) {
		this.activatedTransactionId = activatedTransactionId;
	}

	public long getWageringEendInvestmentId() {
		return wageringEendInvestmentId;
	}

	public void setWageringEendInvestmentId(long wageringEendInvestmentId) {
		this.wageringEendInvestmentId = wageringEendInvestmentId;
	}

	/**
	 * @return the timeCanceled
	 */
	public Date getTimeCanceled() {
		return timeCanceled;
	}

	/**
	 * @param timeCanceled the timeCanceled to set
	 */
	public void setTimeCanceled(Date timeCanceled) {
		this.timeCanceled = timeCanceled;
	}

	/**
	 * @return the timeRefused
	 */
	public Date getTimeRefused() {
		return timeRefused;
	}

	/**
	 * @param timeRefused the timeRefused to set
	 */
	public void setTimeRefused(Date timeRefused) {
		this.timeRefused = timeRefused;
	}


	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	/**
	 * 
	 * @return the turnoverParam
	 */
	public long getTurnoverParam() {
		return turnoverParam;
	}

	/**
	 * 
	 * @param turnoverParam the turnoverParam to set
	 */
	public void setTurnoverParam(long turnoverParam) {
		this.turnoverParam = turnoverParam;
	}

	/**
	 * @return the adjustedAmount
	 */
	public long getAdjustedAmount() {
		return adjustedAmount;
	}

	/**
	 * @param adjustedAmount the adjustedAmount to set
	 */
	public void setAdjustedAmount(long adjustedAmount) {
		this.adjustedAmount = adjustedAmount;
	}
	
	/**
	 * @return the timeWageringWaived
	 */
	public Date getTimeWageringWaived() {
		return timeWageringWaived;
	}

	/**
	 * @param timeWageringWaived the timeWageringWaived to set
	 */
	public void setTimeWageringWaived(Date timeWageringWaived) {
		this.timeWageringWaived = timeWageringWaived;
	}

	/**
	 * @return the timeWithdrawn
	 */
	public Date getTimeWithdrawn() {
		return timeWithdrawn;
	}

	/**
	 * @param timeWithdrawn the timeWithdrawn to set
	 */
	public void setTimeWithdrawn(Date timeWithdrawn) {
		this.timeWithdrawn = timeWithdrawn;
	}

	/**
	 * @return the userSkinId
	 */
	public long getUserSkinId() {
		return userSkinId;
	}

	/**
	 * @param userSkinId the userSkinId to set
	 */
	public void setUserSkinId(long userSkinId) {
		this.userSkinId = userSkinId;
	}

	/**
	 * @return the writerIdCancel
	 */
	public long getWriterIdCancel() {
		return writerIdCancel;
	}

	/**
	 * @param writerIdCancel the writerIdCancel to set
	 */
	public void setWriterIdCancel(long writerIdCancel) {
		this.writerIdCancel = writerIdCancel;
	}

	/**
	 * @return the bonusOddsWin
	 */
	public boolean isBonusOddsWin() {
		return bonusOddsWin;
	}

	/**
	 * @param bonusOddsWin the bonusOddsWin to set
	 */
	public void setBonusOddsWin(boolean bonusOddsWin) {
		this.bonusOddsWin = bonusOddsWin;
	}
	
	public long getTurnOverParamOptionPlus() {
		return turnOverParamOptionPlus;
	}

	public void setTurnOverParamOptionPlus(long turnOverParamOptionPlus) {
		this.turnOverParamOptionPlus = turnOverParamOptionPlus;
	}

	public long getTurnOverParamBinaryOneHundred() {
		return turnOverParamBinaryOneHundred;
	}

	public void setTurnOverParamBinaryOneHundred(long turnOverParamBinaryOneHundred) {
		this.turnOverParamBinaryOneHundred = turnOverParamBinaryOneHundred;
	}


}
