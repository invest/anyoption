package com.anyoption.common.beans;

import java.util.Date;

/**
 * 
 * @author liors
 *
 */
public class DeviceUniqueIdSkin implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	private String duid;
	private long skinId;
	private String userAgent;
	private Date timeCreated;
	private Date timeModified;
	private String ip;
	private long countryId;
	private long combId;
	private String appVer;
	private String c2dmRegistrationId;
    private String dynamicParam;
    private String idfa;
    private String advertisingId;
    private int platformId;

	/**
	 * @return the duid
	 */
	public String getDuid() {
		return duid;
	}

	/**
	 * @param duid the duid to set
	 */
	public void setDuid(String duid) {
		this.duid = duid;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the timeModified
	 */
	public Date getTimeModified() {
		return timeModified;
	}

	/**
	 * @param timeModified the timeModified to set
	 */
	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}

	/**
	 * @return the userAgent
	 */
	public String getUserAgent() {
		return userAgent;
	}

	/**
	 * @param userAgent the userAgent to set
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the combId
	 */
	public long getCombId() {
		return combId;
	}

	/**
	 * @param combId the combId to set
	 */
	public void setCombId(long combId) {
		this.combId = combId;
	}

	/**
	 * @return the appVer
	 */
	public String getAppVer() {
		return appVer;
	}

	/**
	 * @param appVer the appVer to set
	 */
	public void setAppVer(String appVer) {
		this.appVer = appVer;
	}

	/**
	 * @return the c2dmRegistrationId
	 */
	public String getC2dmRegistrationId() {
		return c2dmRegistrationId;
	}

	/**
	 * @param registrationId the c2dmRegistrationId to set
	 */
	public void setC2dmRegistrationId(String registrationId) {
		c2dmRegistrationId = registrationId;
	}

    public String getDynamicParam() {
        return dynamicParam;
    }

    public void setDynamicParam(String dynamicParam) {
        this.dynamicParam = dynamicParam;
    }

	/**
	 * @return the idfa
	 */
	public String getIdfa() {
		return idfa;
	}

	/**
	 * @param idfa the idfa to set
	 */
	public void setIdfa(String idfa) {
		this.idfa = idfa;
	}

	/**
	 * @return the advertisingId
	 */
	public String getAdvertisingId() {
		return advertisingId;
	}

	/**
	 * @param advertisingId the advertisingId to set
	 */
	public void setAdvertisingId(String advertisingId) {
		this.advertisingId = advertisingId;
	}

	/**
	 * @return the platformId
	 */
	public int getPlatformId() {
		return platformId;
	}

	/**
	 * @param platformId the platformId to set
	 */
	public void setPlatformId(int platformId) {
		this.platformId = platformId;
	}

}