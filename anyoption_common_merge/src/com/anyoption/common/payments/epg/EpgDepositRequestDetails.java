package com.anyoption.common.payments.epg;

/**
 * @author liors
 *
 */
public class EpgDepositRequestDetails {
	//public String balance;
	//public String taxBalance;
	public String registrationDate;
	public String businessCase;
	public String skinName;
	public String platform;
	public String countryName;
	public String transactionAmountInUSD;
	public String registrationIP;
	public String transactionWriterName;
	//public String isHighestDepositMadeByTheUser;
	public String campaignName;
	public String affiliateKey;
	private String productId;
	//public String ccFtdDate; //Date of first successful deposit with same CC
	
	/**
	 * @return the balance
	 *//*
	public String getBalance() {
		return balance;
	}
	*//**
	 * @param balance the balance to set
	 *//*
	public void setBalance(String balance) {
		this.balance = balance;
	}
	*//**
	 * @return the taxBalance
	 *//*
	public String getTaxBalance() {
		return taxBalance;
	}
	*//**
	 * @param taxBalance the taxBalance to set
	 *//*
	public void setTaxBalance(String taxBalance) {
		this.taxBalance = taxBalance;
	}*/
	/**
	 * @return the registrationDate
	 */
	public String getRegistrationDate() {
		return registrationDate;
	}
	/**
	 * @param registrationDate the registrationDate to set
	 */
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	/**
	 * @return the businessCase
	 */
	public String getBusinessCase() {
		return businessCase;
	}
	/**
	 * @param businessCase the businessCase to set
	 */
	public void setBusinessCase(String businessCase) {
		this.businessCase = businessCase;
	}
	/**
	 * @return the skinName
	 */
	public String getSkinName() {
		return skinName;
	}
	/**
	 * @param skinName the skinName to set
	 */
	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}
	/**
	 * @return the platform
	 */
	public String getPlatform() {
		return platform;
	}
	/**
	 * @param platform the platform to set
	 */
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}
	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	/**
	 * @return the transactionAmountInUSD
	 */
	public String getTransactionAmountInUSD() {
		return transactionAmountInUSD;
	}
	/**
	 * @param transactionAmountInUSD the transactionAmountInUSD to set
	 */
	public void setTransactionAmountInUSD(String transactionAmountInUSD) {
		this.transactionAmountInUSD = transactionAmountInUSD;
	}
	/**
	 * @return the registrationIP
	 */
	public String getRegistrationIP() {
		return registrationIP;
	}
	/**
	 * @param registrationIP the registrationIP to set
	 */
	public void setRegistrationIP(String registrationIP) {
		this.registrationIP = registrationIP;
	}
	/**
	 * @return the isHighestDepositMadeByTheUser
	 *//*
	public String getIsHighestDepositMadeByTheUser() {
		return isHighestDepositMadeByTheUser;
	}
	*//**
	 * @param isHighestDepositMadeByTheUser the isHighestDepositMadeByTheUser to set
	 *//*
	public void setIsHighestDepositMadeByTheUser(
			String isHighestDepositMadeByTheUser) {
		this.isHighestDepositMadeByTheUser = isHighestDepositMadeByTheUser;
	}*/
	/**
	 * @return the campaignName
	 */
	public String getCampaignName() {
		return campaignName;
	}
	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	/**
	 * @return the affiliateKey
	 */
	public String getAffiliateKey() {
		return affiliateKey;
	}
	/**
	 * @param affiliateKey the affiliateKey to set
	 */
	public void setAffiliateKey(String affiliateKey) {
		this.affiliateKey = affiliateKey;
	}
	/**
	 * @return the ccFtdDate
	 *//*
	public String getCcFtdDate() {
		return ccFtdDate;
	}
	*//**
	 * @param ccFtdDate the ccFtdDate to set
	 *//*
	public void setCcFtdDate(String ccFtdDate) {
		this.ccFtdDate = ccFtdDate;
	}*/
	/**
	 * @return the transactionWriterName
	 */
	public String getTransactionWriterName() {
		return transactionWriterName;
	}
	/**
	 * @param transactionWriterName the transactionWriterName to set
	 */
	public void setTransactionWriterName(String transactionWriterName) {
		this.transactionWriterName = transactionWriterName;
	}
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
}
