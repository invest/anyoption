package com.anyoption.common.payments.epg;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author LioR SoLoMoN
 *
 */
@XmlRootElement(name = "checkout-response")
public class ResponseCheckout {
	private PaymentSolution paymentSolution;

	/**
	 * @return the paymentSolutions
	 */
	public PaymentSolution getPaymentSolutions() {
		return paymentSolution;
	}

	/**
	 * @param paymentSolutions the paymentSolutions to set
	 */
	@XmlElement
	public void setPaymentSolutions(PaymentSolution paymentSolution) {
		this.paymentSolution = paymentSolution;
	}
}
