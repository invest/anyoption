package com.anyoption.common.payments.epg;

/**
 * @author LioR SoLoMoN
 *
 */
public enum PaymentType {
	CREDIT_CARD("creditcards")
	, Neteller("neteller");
	
	private String value;
	
	private PaymentType(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}
}
