package com.anyoption.common.payments.epg;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author LioR SoLoMoN
 *
 */
@XmlRootElement(namespace = "com.anyoption.common.payments.epg.Response")  
public class Operation {
	private String amount;
	private String currency;
	private String details;
	private String merchantTransactionId;
	private String message;
	private String operationType;
	private String payFrexTransactionId;
	private String paySolTransactionId;
	private PaymentDetails paymentDetails;
	private String originalPayFrexTransactionId;
	private String paymentSolution;
	private String status;

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Operation [amount=" + amount + ", currency=" + currency
				+ ", details=" + details + ", merchantTransactionId="
				+ merchantTransactionId + ", message=" + message
				+ ", operationType=" + operationType
				+ ", payFrexTransactionId=" + payFrexTransactionId
				+ ", paySolTransactionId=" + paySolTransactionId
				+ ", originalPayFrexTransactionId="
				+ originalPayFrexTransactionId + ", paymentSolution="
				+ paymentSolution + ", status=" + status + "]";
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	
	/**
	 * @param amount the amount to set
	 */
	@XmlElement
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	
	/**
	 * @param currency the currency to set
	 */
	@XmlElement
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	/**
	 * @return the merchantTransactionId
	 */
	public String getMerchantTransactionId() {
		return merchantTransactionId;
	}
	
	/**
	 * @param merchantTransactionId the merchantTransactionId to set
	 */
	@XmlElement
	public void setMerchantTransactionId(String merchantTransactionId) {
		this.merchantTransactionId = merchantTransactionId;
	}
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * @param message the message to set
	 */
	@XmlElement
	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * @return the operationType
	 */
	public String getOperationType() {
		return operationType;
	}
	
	/**
	 * @param operationType the operationType to set
	 */
	@XmlElement
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	
	/**
	 * @return the payFrexTransactionId
	 */
	public String getPayFrexTransactionId() {
		return payFrexTransactionId;
	}
	
	/**
	 * @param payFrexTransactionId the payFrexTransactionId to set
	 */
	@XmlElement
	public void setPayFrexTransactionId(String payFrexTransactionId) {
		this.payFrexTransactionId = payFrexTransactionId;
	}
	

	
	/**
	 * @return the originalPayFrexTransactionId
	 */
	public String getOriginalPayFrexTransactionId() {
		return originalPayFrexTransactionId;
	}
	
	/**
	 * @param originalPayFrexTransactionId the originalPayFrexTransactionId to set
	 */
	@XmlElement
	public void setOriginalPayFrexTransactionId(String originalPayFrexTransactionId) {
		this.originalPayFrexTransactionId = originalPayFrexTransactionId;
	}
	
	/**
	 * @return the paymentSolution
	 */
	public String getPaymentSolution() {
		return paymentSolution;
	}
	
	/**
	 * @param paymentSolution the paymentSolution to set
	 */
	public void setPaymentSolution(String paymentSolution) {
		this.paymentSolution = paymentSolution;
	}
	
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * @param status the status to set
	 */
	@XmlElement
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the paySolTransactionId
	 */
	public String getPaySolTransactionId() {
		return paySolTransactionId;
	}

	/**
	 * @param paySolTransactionId the paySolTransactionId to set
	 */
	@XmlElement
	public void setPaySolTransactionId(String paySolTransactionId) {
		this.paySolTransactionId = paySolTransactionId;
	}

	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	@XmlElement
	public void setDetails(String details) {
		this.details = details;
	}

	/**
	 * @return the paymentDetails
	 */
	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}

	/**
	 * @param paymentDetails the paymentDetails to set
	 */
	@XmlElement
	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
}