package com.anyoption.common.payments;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.User;

/**
 * @author LioR SoLoMoN
 *
 */
public class FinalizeDeposit {
	private boolean isSuccessful;
	private String authNumber;
	private String result;
	private String message;
	private String userMessage;
	private String providerTransactionId;
	private int writerId; 
	private long loginId;
	private Transaction transaction;
	private String utcOffset;
	private User user;
	

	/**
	 * @param isSuccessful
	 * @param authNumber
	 * @param result
	 * @param message
	 * @param userMessage
	 * @param providerTransactionId
	 * @param writerId
	 * @param loginId
	 * @param transaction
	 */
	public FinalizeDeposit(boolean isSuccessful, String authNumber,
			String result, String message, String userMessage,
			String providerTransactionId, int writerId, String utcOffset,
			long loginId, Transaction transaction) {
		this.isSuccessful = isSuccessful;
		this.authNumber = authNumber;
		this.result = result;
		this.message = message;
		this.userMessage = userMessage;
		this.providerTransactionId = providerTransactionId;
		this.writerId = writerId;
		this.loginId = loginId;
		this.transaction = transaction;
		this.utcOffset = utcOffset;
	}

	/**
	 * @param user
	 * @param isSuccessful
	 * @param authNumber
	 * @param result
	 * @param message
	 * @param userMessage
	 * @param providerTransactionId
	 * @param writerId
	 * @param loginId
	 * @param transaction
	 */
	public FinalizeDeposit(User user, boolean isSuccessful, String authNumber,
			String result, String message, String userMessage,
			String providerTransactionId, int writerId,
			long loginId, Transaction transaction) {
		this.user = user;
		this.isSuccessful = isSuccessful;
		this.authNumber = authNumber;
		this.result = result;
		this.message = message;
		this.userMessage = userMessage;
		this.providerTransactionId = providerTransactionId;
		this.writerId = writerId;
		this.loginId = loginId;
		this.transaction = transaction;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FinalizeDeposit [isSuccessful=" + isSuccessful
				+ ", authNumber=" + authNumber + ", result=" + result
				+ ", message=" + message + ", userMessage=" + userMessage
				+ ", providerTransactionId=" + providerTransactionId
				+ ", writerId=" + writerId + ", loginId=" + loginId
				+ ", transaction=" + transaction + "]";
	}
	
	/**
	 * @return the isSuccessful
	 */
	public boolean isSuccessful() {
		return isSuccessful;
	}
	/**
	 * @param isSuccessful the isSuccessful to set
	 */
	public void setSuccessful(boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}
	/**
	 * @return the authNumber
	 */
	public String getAuthNumber() {
		return authNumber;
	}
	/**
	 * @param authNumber the authNumber to set
	 */
	public void setAuthNumber(String authNumber) {
		this.authNumber = authNumber;
	}
	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the userMessage
	 */
	public String getUserMessage() {
		return userMessage;
	}
	/**
	 * @param userMessage the userMessage to set
	 */
	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}
	/**
	 * @return the providerTransactionId
	 */
	public String getProviderTransactionId() {
		return providerTransactionId;
	}
	/**
	 * @param providerTransactionId the providerTransactionId to set
	 */
	public void setProviderTransactionId(String providerTransactionId) {
		this.providerTransactionId = providerTransactionId;
	}
	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}
	
	/**
	 * @return the loginId
	 */
	public long getLoginId() {
		return loginId;
	}
	/**
	 * @param loginId the loginId to set
	 */
	public void setLoginId(long loginId) {
		this.loginId = loginId;
	}
	/**
	 * @return the transaction
	 */
	public Transaction getTransaction() {
		return transaction;
	}
	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the utcOffset
	 */
	public String getUtcOffset() {
		return utcOffset;
	}

	/**
	 * @param utcOffset the utcOffset to set
	 */
	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}
}
