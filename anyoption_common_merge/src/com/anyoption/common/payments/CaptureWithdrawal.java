package com.anyoption.common.payments;

import com.anyoption.common.beans.base.User;

/**
 * @author liors
 *
 */
public class CaptureWithdrawal {
	private User user;
	private int statusId;
	private String comment;
	private String description;
	private long transactionId; //anyoption id.
	private String providerTransactionId;
	private String utcOffsetSettled;
	private long clearingProviderId;
	
	
	/**
	 * 
	 */
	public CaptureWithdrawal() {		
	
	}
	
	/**
	 * @param statusId
	 * @param comment
	 * @param description
	 * @param transactionId
	 * @param providerTransactionId
	 * @param utcOffsetSettled
	 * @param clearingProviderId
	 */
	public CaptureWithdrawal(int statusId, String comment, 
			String description, long transactionId, String providerTransactionId,
			String utcOffsetSettled, int clearingProviderId) {		
		this.statusId = statusId;
		this.comment = comment;
		this.description = description;
		this.transactionId = transactionId;
		this.providerTransactionId = providerTransactionId;
		this.utcOffsetSettled = utcOffsetSettled;
		this.clearingProviderId = clearingProviderId; 
	}
	
	/**
	 * @param user
	 * @param statusId
	 * @param comment
	 * @param description
	 * @param transactionId
	 * @param providerTransactionId
	 * @param utcOffsetSettled
	 * @param clearingProviderId
	 */
	public CaptureWithdrawal(User user, int statusId, String comment, 
			String description, long transactionId, String providerTransactionId,
			String utcOffsetSettled, int clearingProviderId) {		
		this.user = user;
		this.statusId = statusId;
		this.comment = comment;
		this.description = description;
		this.transactionId = transactionId;
		this.providerTransactionId = providerTransactionId;
		this.utcOffsetSettled = utcOffsetSettled;
		this.clearingProviderId = clearingProviderId; 
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CaptureWithdrawal [statusId=" + statusId + ", comment="
				+ comment + ", description=" + description + ", transactionId="
				+ transactionId + ", providerTransactionId="
				+ providerTransactionId + ", utcOffsetSettled="
				+ utcOffsetSettled + ", clearingProviderId="
				+ clearingProviderId + "]";
	}
	/**
	 * @return the statusId
	 */
	public int getStatusId() {
		return statusId;
	}
	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the providerTransactionId
	 */
	public String getProviderTransactionId() {
		return providerTransactionId;
	}
	/**
	 * @param providerTransactionId the providerTransactionId to set
	 */
	public void setProviderTransactionId(String providerTransactionId) {
		this.providerTransactionId = providerTransactionId;
	}
	/**
	 * @return the utcOffsetSettled
	 */
	public String getUtcOffsetSettled() {
		return utcOffsetSettled;
	}
	/**
	 * @param utcOffsetSettled the utcOffsetSettled to set
	 */
	public void setUtcOffsetSettled(String utcOffsetSettled) {
		this.utcOffsetSettled = utcOffsetSettled;
	}
	/**
	 * @return the clearingProviderId
	 */
	public long getClearingProviderId() {
		return clearingProviderId;
	}
	/**
	 * @param clearingProviderId the clearingProviderId to set
	 */
	public void setClearingProviderId(long clearingProviderId) {
		this.clearingProviderId = clearingProviderId;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}	
}
