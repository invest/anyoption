package com.anyoption.common.jmx.cassandra;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import com.copyop.common.managers.ManagerBase;


public class JMXCassandraConnectionReset implements JMXCassandraConnectionResetMBean {

public static final Logger log = Logger.getLogger(JMXCassandraConnectionReset.class);
	

	protected String jmxName;

	public boolean closeSession() {
		try {
			ManagerBase.closeSession();
			return true;
		} catch (Exception e) {
			log.error("Cannot close Cassandra session!", e);
			return false;
		}
	}
	
	public boolean closeCluster() {
		try {
			ManagerBase.closeCluster();
			return true;
		} catch (Exception e) {
			log.error("Cannot close Cassandra cluster!", e);
			return false;
		}
	}
	
	/**
     * Register the instance in the platform MBean server under given name.
     *
     * @param name the name under which to register
     */
    public void registerJMX(String name) {
        jmxName = name;
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName on = new ObjectName(jmxName);
            mbs.registerMBean(this, on);
            if (log.isInfoEnabled()) {
                log.info("JMXCassandraConnectionReset MBean registered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to register in the MBean server.", e);
        }
    }

    /**
     * Unregister the instance from the platform MBean server. Use the name
     * under which it was registered.
     */
    public void unregisterJMX() {
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName on = new ObjectName(jmxName);
            mbs.unregisterMBean(on);
            if (log.isInfoEnabled()) {
                log.info("JMXCassandraConnectionReset MBean unregistered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to unregister from the MBean server.", e);
        }
    }
	
}
