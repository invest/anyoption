package com.anyoption.common.jmx.cassandra;


public interface JMXCassandraConnectionResetMBean {
	
	public boolean closeSession();
	
	public boolean closeCluster();

}
