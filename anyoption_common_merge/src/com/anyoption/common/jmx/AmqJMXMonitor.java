package com.anyoption.common.jmx;

import java.util.UUID;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.anyoption.common.jmx.base.JMXBase;
import com.copyop.common.enums.base.ProfileLinkCommandEnum;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.events.ProfileManageEvent;

public class AmqJMXMonitor extends JMXBase implements AmqJMXMonitorMBean {
	
	final static String jmxName = "com.anyoption.common.jmx:type=AmqJMXMonitor,name=AmqJMXMonitor";
	
	public AmqJMXMonitor(String jmxSuffix) {
		super(jmxName, jmxSuffix);
	}

	public static final Logger log = Logger.getLogger(AmqJMXMonitor.class);
	
	public static final String JNDI_NAME_ACTIVEMQ			= "java:/AMQConnectionFactory";
	public static final String JNDI_NAME_ACTIVEMQ_TOMCAT	= "java:/comp/env/AMQConnectionFactory";
	
	private static ConnectionFactory amqConnectionFactory;
    
	public static void initConnectionFactory() {
		if(null != amqConnectionFactory) {
			return;
		}
		
		try {
			Context initCtx = new InitialContext();
			amqConnectionFactory = (ConnectionFactory) initCtx.lookup(JNDI_NAME_ACTIVEMQ);
			return;
		} catch(NamingException ex){
				/* try next context */
		}
		try {
			Context initCtx = new InitialContext();
			amqConnectionFactory = (ConnectionFactory) initCtx.lookup(JNDI_NAME_ACTIVEMQ_TOMCAT);
		} catch(NamingException ex){
			log.error("No context found for ActiveMQ factory");
		}
	}
	
	public String getAMQConnectionStatus() {
		if(!isEnabled()) {
			return DISABLED;
		}
		Connection con = null;
		try{
			String uid = UUID.randomUUID().toString();

			con = amqConnectionFactory.createConnection();
			Session session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue destination = session.createQueue(CopyOpEventSender.MONITOR_PING);
			MessageProducer producer = session.createProducer(destination);
			ObjectMessage msg = session.createObjectMessage();
			ProfileManageEvent pme = new ProfileManageEvent(ProfileLinkCommandEnum.PING);
			msg.setJMSCorrelationID(uid);
			msg.setStringProperty("src", getSuffix());
			msg.setObject(pme);
			producer.send(msg, DeliveryMode.NON_PERSISTENT, 4, 3000);
			log.debug("Pinged AMQ." );	
			return OK;
		} catch (Exception e1) {
			log.debug("while sending ping", e1);
			return e1.getClass().getName();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			} catch ( Exception e) { /* ignore */ }
		}
	}
}
