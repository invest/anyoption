package com.anyoption.common.jmx;

import com.anyoption.common.jmx.base.JMXBaseMBean;

public interface AmqJMXMonitorMBean extends JMXBaseMBean {

    public String getAMQConnectionStatus() ;

}
