package com.anyoption.common.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface PrintLogAnnotations {
	boolean stopPrintDebugLog();
}
