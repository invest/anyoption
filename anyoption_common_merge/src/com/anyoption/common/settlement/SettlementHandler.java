package com.anyoption.common.settlement;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.InvestmentBonusData;
import com.anyoption.common.beans.User;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlerFactory;
import com.anyoption.common.bonus.BonusHandlersException;
import com.anyoption.common.daos.CountryDAOBase;
import com.anyoption.common.managers.BonusManagerBase;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.SMSManagerBase;
import com.anyoption.common.sms.SMS;
import com.anyoption.common.sms.SMSException;
import com.anyoption.common.util.BonusFormulaDetails;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author kirilim
 */
public abstract class SettlementHandler implements SettlementHandlerIfc {

	private static final Logger log = Logger.getLogger(SettlementHandler.class);

	@Override
	public void calculateCommon(Connection con, Investment investment, long premiaC, long winLoseSubtrahend, boolean win, boolean voidBet,
								long netResult, long result, User user, SettlementHandlerResult sHRslt, long bonusAmount) {
		long winLose = 0;
		boolean isNextInvestOnUs = investment.getBonusOddsChangeTypeId() == 1 && !win;
		long tax = 0;
		// winlose ?
		if (CommonUtil.isHebrewSkin(user.getSkinId())) {
			winLose = user.getTotalWinLose();
			log.info("User :" + user.getId() + " win lose for the year :" + winLose);
			// in case of a losing "next invest on us" investment the netResult is not added to the W\L
			if (!isNextInvestOnUs) {
				winLose += netResult + bonusAmount - winLoseSubtrahend;
			}
			if (winLose > 0) {
				// TODO: Tony: find a nice way to avoid hardcoding the tax %
				tax = Math.round(winLose * ConstantsBase.TAX_PERCENTAGE);
			}
		}
		if (log.isDebugEnabled()) {
			String ls = System.getProperty("line.separator");
			log.debug(ls+ "Settle investment: " + investment.getId() + ls + " win: " + win + ls + " nextInvestOnUs: " + isNextInvestOnUs
						+ ls + " voidBet: " + voidBet + ls + " result: " + result + ls + " netResult: " + netResult + ls + " winLose: "
						+ winLose + ls + " tax: " + tax + ls);
		}
		investment.setUtcOffsetSettled(user.getUtcOffset());
		long winC = 0;
		long loseC = 0;
		if (netResult > 0) {  // win
			winC = netResult;
			loseC = premiaC;
		} else if (netResult < 0) { // lose
			winC = 0;
			loseC = (-netResult) + premiaC;
		} else {
			winC = 0;
			loseC = premiaC;
		}
		investment.setWin(winC);
		investment.setLose(loseC);
		sHRslt.setTax(tax);
		sHRslt.setWinLose(winLose);
		sHRslt.setNextInvestOnUs(isNextInvestOnUs);
		sHRslt.setBonusAmount(bonusAmount);
	}

	@Override
	public long handleBonusOnSettlement(Connection con, Investment investment, boolean win) {
		long bonusTypeId = investment.getBonusOddsChangeTypeId();
		long bonusAmount = 0l;
		if (bonusTypeId > 0) {
			BonusHandlerBase bh = BonusHandlerFactory.getInstance(bonusTypeId);
			if (null != bh) {
				try {
					InvestmentBonusData ibd = new InvestmentBonusData(	investment.getOddsWin(), investment.getOddsLose(),
																		investment.getBonusWinOdds(), investment.getBonusLoseOdds(),
																		investment.getId(), investment.getBonusUserId(),
																		investment.getAmount(), investment.getInsuranceAmountRU());
					bonusAmount = bh.handleBonusOnSettleInvestment(con, ibd, win);
				} catch (BonusHandlersException e) {
					log.error("Error in getBonusAmountOnSettleInvestment for investment " + investment.getId(), e);
				}
			}
		}
		log.debug("settlemnt inv id: " + investment.getId() + " bonusAmount " + bonusAmount);
		return bonusAmount;
	}

	@Override
	public void handleCommonBonusManagementSystemActions(	Connection con, Investment investment, boolean win, boolean isNextInvestOnUs,
															long bonusAmount, long netResult, long writerId, long loginId,
															double coefficient) throws SQLException {
		boolean bonusOddsWithTran = false;
		// in case of a "next invest on us" losing investment or (odds change with bonus amount > 0) the user is granted with a bonus.
		if (isNextInvestOnUs || (investment.getBonusOddsChangeTypeId() > 1 && bonusAmount > 0)) {
			long amount = isNextInvestOnUs ? -netResult : bonusAmount;
			if (log.isInfoEnabled()) {
				log.info("granting bonus. Investment id: " + investment.getId() + " Bonus amount: " + amount);
			}
			InvestmentsManagerBase.grantBonus(con, investment, writerId, amount, loginId);
			bonusOddsWithTran = true;
		}
		if (investment.getBonusOddsChangeTypeId() > 0 && win && !bonusOddsWithTran) {
			log.debug("BMS, bonusTypeId (&& win && !bonusOddsWithTran): " + investment.getBonusOddsChangeTypeId());
			BonusManagerBase.updateAdjustedAmount(con, investment.getBonusUserId(), investment.getWin());
		}
		ArrayList<BonusFormulaDetails> bonusFormulaD = BonusManagerBase.getBonusAmounts(con, investment.getId(), false);
		if (!bonusFormulaD.isEmpty()) {
			log.debug("BMS, coefficient: "+ coefficient + "=" + investment.getAmount() + "+" + investment.getWin() + "-"
						+ investment.getLose());
			if (investment.getAmount() != 0) {
				for (BonusFormulaDetails item : bonusFormulaD) {
					log.debug("\n\nBMS, settle investment formula \n*****************************\n"+ "" + item.getAdjustedAmount() + "+("
								+ (double) item.getAmount() + "/" + (double) investment.getAmount() + ")*" + coefficient
								+ "=\n (double)item.getAmount() / (double)investment.getAmount() = "
								+ (double) item.getAmount() / (double) investment.getAmount()
								+ " ((double)item.getAmount() / (double)investment.getAmount()) * coefficient "
								+ ((double) item.getAmount() / (double) investment.getAmount()) * coefficient
								+ " BigDecimal.valueOf(((double)item.getAmount() / (double)investment.getAmount()) * coefficient).setScale(0, RoundingMode.HALF_UP).longValue() "
								+ BigDecimal.valueOf(((double) item.getAmount() / (double) investment.getAmount()) * coefficient)
											.setScale(0, RoundingMode.HALF_UP).longValue());
					long adjustedAmount = item.getAdjustedAmount() + BigDecimal
																				.valueOf(((double) item.getAmount()
																							/ (double) investment.getAmount())
																						* coefficient)
																				.setScale(0, RoundingMode.HALF_UP).longValue();
					log.debug("\nBMS, adjusted_amount to be update at bonus_users: (id,adjustedAmount)"+ "(" + item.getBonusUsersId() + ","
								+ adjustedAmount + ")");
					BonusManagerBase.updateAdjustedAmount(con, item.getBonusUsersId(), adjustedAmount);
					BonusManagerBase.insertBonusInvestments(con, investment.getId(), item.getBonusUsersId(),
															BigDecimal	.valueOf(((double) item.getAmount() / (double) investment.getAmount())
																				* coefficient)
																		.setScale(0, RoundingMode.HALF_UP).longValue(),
															adjustedAmount, ConstantsBase.BONUS_INVESTMENTS_TYPE_SETTLED_INV);
				}
			}
		}
	}

	@Override
	public void handleSMSOnSettlement(	Connection con, Investment inv, User u, SettlementHandlerResult sHRslt,
										long investmentFeeAmount) throws SQLException {
		// add sms message to db queue if needed
		if (inv.isAcceptedSms()) {
			log.info("Going to queued sms for invId: " + inv.getId() + ", userId: " + inv.getUserId());
			Country country = CountryDAOBase.getById(con, u.getCountryId());
			String phoneCode = country.getPhoneCode();
			if (null != u.getMobilePhone() && u.getMobilePhone().indexOf("0") == 0) { // leading zero (index 0)
				u.setMobilePhone(u.getMobilePhone().substring(1));
			}
			String[] params = new String[3];
			inv.setCurrencyId(u.getCurrencyId());
			if (investmentFeeAmount != 0) { // GM : add premia to the amount
				inv.setAmount(inv.getAmount() + investmentFeeAmount);
			}
			
			String SenderName = "anyoption";
			String separator = " ";
			String langCode = ConstantsBase.LOCALE_DEFAULT;
			if (country.isAlphaNumericSender()) {
				SenderName = country.getSupportPhone();
				SenderName = SenderName.replaceAll("[^0-9]", "");
				if (SenderName.length() > ConstantsBase.MAX_SIZE_SENDER_NAME) {
					SenderName = SenderName.substring(country.getPhoneCode().length());
				}
			} else if (CommonUtil.isHebrewSkin(u.getSkinId())) {
				SenderName = "etrader";
				separator = "|";
				langCode = ConstantsBase.ETRADER_LOCALE;
			}
			String senderNumber = CountryManagerBase.getById(u.getCountryId()).getSupportPhone();
			Locale locale = new Locale(langCode);
			String refundTxt = CommonUtil.displayAmount(InvestmentsManagerBase.getRefund(inv)
														+ (sHRslt.isNextInvestOnUs() ? -sHRslt.getNetResult() : sHRslt.getBonusAmount()),
														false, u.getCurrencyId(), true);
			if (u.getCurrencyId() == ConstantsBase.CURRENCY_ILS_ID || u.getCurrencyId() == ConstantsBase.CURRENCY_TRY_ID) {
				refundTxt = refundTxt + CommonUtil.getMessage(locale, CommonUtil.getCurrencySymbolSMS(u.getCurrencyId()), params);
			} else {
				refundTxt = CommonUtil.getMessage(locale, CommonUtil.getCurrencySymbolSMS(u.getCurrencyId()), params) + refundTxt;
			}
			params[0] = MarketsManagerBase.getMarketShortName(u.getSkinId(), inv.getMarketId()) + separator
						+ CommonUtil.formatLevelByMarket(inv.getCurrentLevel(), inv.getMarketId(), inv.getDecimalPoint()) + separator
						+ CommonUtil.getMessage(locale, inv.getTypeName(), null) + separator
						+ CommonUtil.getTimeFormat(inv.getTimeEstClosing(), inv.getUtcOffsetCreated()) + separator;
			params[1] = CommonUtil.formatLevelByMarket(inv.getClosingLevel(), inv.getMarketId(), inv.getDecimalPoint());
			params[2] = refundTxt;
			String msg = CommonUtil.getMessage(locale, "sms.settelment.message", params);
			try {
				SMSManagerBase.sendTextMessage(	SenderName, senderNumber, phoneCode + u.getMobilePhone(), msg, con,
												inv.getId(), SMSManagerBase.SMS_KEY_TYPE_INVESTMENT, ConstantsBase.MOBIVATE_PROVIDER, SMS.DESCRIPTION_EXPIRY, u.getMobileNumberValidation());
			} catch (SMSException smse) {
				log.warn("Failed to send SMS.", smse);
			}
		}
	}
}