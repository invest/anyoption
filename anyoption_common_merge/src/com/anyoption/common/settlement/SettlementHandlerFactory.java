package com.anyoption.common.settlement;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;

/**
 * @author kirilim
 */
public abstract class SettlementHandlerFactory {

	private static final Logger log = Logger.getLogger(SettlementHandlerFactory.class);

	public static SettlementHandler getInstance(long productType) {
		log.debug("Getting instance for productType: " + productType);
		if (productType ==  Opportunity.TYPE_REGULAR) {
			return new BinarySettlementHandler();
		} else if (productType ==  Opportunity.TYPE_ONE_TOUCH) {
			return new OneTouchSettlementHandler();
		} else if (productType ==  Opportunity.TYPE_OPTION_PLUS) {
			return new OptionPlusSettlementHandler();
		} else if (productType ==  Opportunity.TYPE_PRODUCT_BINARY_0100 || productType ==  Opportunity.TYPE_BINARY_0_100_BELOW) {
			return new Binary0100SettlementHandler();
		} else if (productType ==  Opportunity.TYPE_BUBBLES) {
			return new BubblesSettlementHandler();
		} else if (productType ==  Opportunity.TYPE_DYNAMICS) {
			return new DynamicsSettlementHandler();
		} else {
			log.error("No suitable settlement handler for product type: " + productType);
			return null;
		}
	}
}