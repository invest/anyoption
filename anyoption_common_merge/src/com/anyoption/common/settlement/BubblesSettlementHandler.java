package com.anyoption.common.settlement;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.User;

/**
 * @author kirilim
 */
public class BubblesSettlementHandler extends SettlementHandler implements SettlementHandlerIfc {

	private static final Logger log = Logger.getLogger(BubblesSettlementHandler.class);

	@Override
	public SettlementHandlerResult calculate(Connection con, Investment investment, long investmentFeeAmount, long sellPrice, User user) {
		// It seems that the sellPrice send is value greater than 0 for win and lower than 0 for lose, it's not the actual sell price
		SettlementHandlerResult rslt = new SettlementHandlerResult();
		boolean win = false;
		if (sellPrice > 0) {
			win = true;
		}
		log.debug("settlemnt inv id: "+ investment.getId() + " getBonusOddsChangeTypeId " + investment.getBonusOddsChangeTypeId() + " win " + win);
		long result;
		long netResult;
		if (win) {
			result = Math.round(investment.getAmount() * (1 + investment.getOddsWin()));
			netResult = Math.round(investment.getAmount() * investment.getOddsWin());
		} else {
			result = Math.round(investment.getAmount() * (1 - investment.getOddsLose()));
			netResult = Math.round(-investment.getAmount() * investment.getOddsLose());
		}
		calculateCommon(con, investment, 0l, 0l, win, false, netResult, result, user, rslt, 0l);

		rslt.setResult(result);
		rslt.setNetResult(netResult);
		rslt.setWin(win);
		return rslt;
	}

	@Override
	public void handleBonusManagementSystemActions(	Connection con, Investment investment, boolean win, boolean isNextInvestOnUs,
													long bonusAmount, long netResult, long sellPrice, long writerId,
													long loginId) throws SQLException {
		// coefficient - multiplicative factor for the formula
		double coefficient = investment.getAmount() + investment.getWin() - investment.getLose();
		handleCommonBonusManagementSystemActions(	con, investment, win, isNextInvestOnUs, bonusAmount, netResult, writerId, loginId,
													coefficient);
	}

	@Override
	public void handleBalanceLogCmd(int balanceLogCmd, Investment investment, long result, long GMInsuranceAmount,
									User user) throws SQLException {
		return;
	}
}