package com.anyoption.common.settlement;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.User;

/**
 * @author kirilim
 */
public interface SettlementHandlerIfc {

	public SettlementHandlerResult calculate(Connection con, Investment investment, long investmentFeeAmount, long sellPrice, User user);

	public void calculateCommon(Connection con, Investment investment, long premiaC, long winLoseSubtrahend, boolean win, boolean voidBet,
								long netResult, long result, User user, SettlementHandlerResult sHRslt, long bonusAmount);

	public long handleBonusOnSettlement(Connection con, Investment investment, boolean win);

	public void handleBonusManagementSystemActions(	Connection con, Investment investment, boolean win, boolean isNextInvestOnUs,
													long bonusAmount, long netResult, long sellPrice, long writerId,
													long loginId) throws SQLException;

	public void handleCommonBonusManagementSystemActions(	Connection con, Investment investment, boolean win, boolean isNextInvestOnUs,
															long bonusAmount, long netResult, long writerId, long loginId,
															double coefficient) throws SQLException;

	public void handleBalanceLogCmd(int balanceLogCmd, Investment investment, long result, long GMInsuranceAmount,
									User user) throws SQLException;

	public void handleSMSOnSettlement(	Connection con, Investment inv, User u, SettlementHandlerResult sHRslt,
										long GMInsuranceAmount) throws SQLException;
}