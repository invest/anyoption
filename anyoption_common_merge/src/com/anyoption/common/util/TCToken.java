package com.anyoption.common.util;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import org.apache.commons.codec.binary.Base64;

public class TCToken {

	private static final String TCTOKEN_SECRET_KEY = "OyY1d0RbUiNidjp9JDBUMQ==";
	private static final byte[] key = org.apache.commons.codec.binary.Base64.decodeBase64(TCTOKEN_SECRET_KEY.getBytes());

	public static final String TCTOKEN_ID = "AnyOption";
	public static final String TCTOKEN_SEPARATOR = ",";

	static public void main(String args[]) throws Exception {
		String testString="AnyOption,VWUaI,fr,1473936700";
		byte[] plainData = testString.getBytes(StandardCharsets.UTF_8);
		System.out.println("!!!"+new String(key, StandardCharsets.UTF_8)+"!!!");
		System.out.println("!!!"+new String(encrypt(plainData, key), StandardCharsets.UTF_8)+"!!!");
		System.out.println("!!!"+new String(decrypt(encrypt(plainData, key), key), StandardCharsets.UTF_8)+"!!!");
		System.out.println("!!!"+getToken(testString)+"!!!");
	}

	private static byte[] encrypt(byte[] input, byte[] key) throws Exception {
		Cipher cipher = Cipher.getInstance("Blowfish/ECB/PKCS5Padding");
		SecretKeySpec keySpec = new SecretKeySpec(key, "Blowfish");
		cipher.init(Cipher.ENCRYPT_MODE, keySpec);
		return new Base64().encode(cipher.doFinal(input));
	}

	private static byte[] decrypt(byte[] input, byte[] key) throws Exception {
		Cipher cipher = Cipher.getInstance("Blowfish/ECB/PKCS5Padding");
		SecretKeySpec keySpec = new SecretKeySpec(key, "Blowfish");
		cipher.init(Cipher.DECRYPT_MODE, keySpec);
		return cipher.doFinal(new Base64().decode(input));
	}

	public static synchronized String encryptStringToString(String data) throws Exception {
		byte[] plainData = data.getBytes(StandardCharsets.UTF_8);
		return new String(encrypt(plainData, key), StandardCharsets.UTF_8);
	}

	public static synchronized String decryptStringToString(String data) throws Exception {
		byte[] encryptedData = data.getBytes(StandardCharsets.UTF_8);
		return new String(decrypt(encryptedData, key), StandardCharsets.UTF_8);
	}

	public static synchronized String getToken(String parts) {
		try {
			return URLEncoder.encode(encryptStringToString(parts), "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
			return "unable to process token";
		}
	}

}
