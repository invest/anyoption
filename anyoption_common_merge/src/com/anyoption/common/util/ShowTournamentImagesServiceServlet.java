package com.anyoption.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Tournament;

public class ShowTournamentImagesServiceServlet  extends HttpServlet {

	
    private static final long serialVersionUID = -6233831873948879567L;
    private static final Logger log = Logger.getLogger(ShowTournamentImagesServiceServlet.class);  
    private static final String slash = "/";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        String uri = request.getRequestURI();
        if (log.isDebugEnabled()) {
            log.debug("URI requested: " + uri);
        }
        
        int lastSlash = uri.lastIndexOf(slash);
        if (lastSlash != -1 && lastSlash < uri.length()) {
        	String fileName = uri.substring(lastSlash + 1);        	
        	
        	if(fileName == null){
        		log.debug("Can't finde fileName:" + fileName);
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return;
        	}
        	File file = new File(CommonUtil.getProperty(ConstantsBase.FILES_PATH) + Tournament.TOURNAMENTS_DIR_NAME + "/" + fileName);
			FileInputStream fis = new FileInputStream(file);

			long length = file.length();

			if (null == fis || length == 0) {  // file not found
                if (log.isDebugEnabled()) {
                    log.debug("404 for " + fileName);
                }
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
		    }

			 //	Create the byte array to hold the data
	         byte[] bytes = new byte[(int)length];

	         // Read in the bytes
	         int offset = 0;
	         int numRead = 0;
	         while ( (offset < bytes.length) &&
	        		 	( (numRead = fis.read(bytes, offset, bytes.length-offset)) >= 0) ) {

	            offset += numRead;
	        }

	        // Ensure all the bytes have been read in
	        if (offset < bytes.length) {
	           log.warn("Not all bytes have been readed!");
	        }
	        fis.close();
	        
            response.setHeader("Cache-control", "max-age=60, must-revalidate");
            response.setContentType("image/jpeg");
            response.setContentLength(bytes.length);

            OutputStream os = response.getOutputStream();
            os.write(bytes);
            os.flush();
            os.close();        	
        }
    }     
}