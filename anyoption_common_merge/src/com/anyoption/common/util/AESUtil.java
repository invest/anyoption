package com.anyoption.common.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

//"C:\Program Files\Java\jdk1.8.0_102\jre\bin\keytool.exe" -genseckey -alias "encryption_key" -keyalg "AES" -keysize 256 -storetype "jceks" -keystore "test_keystore" -storepass "123456"
//"C:\Program Files\Java\jdk1.8.0_102\jre\bin\keytool.exe" -list -keystore "C:\work\tools\tomcat-7.0.12\conf\test_keystore" -storetype "jceks" -alias "encryption_key" -storepass "123456"
public class AESUtil {

	private static final Logger log = Logger.getLogger(AESUtil.class);
	private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
	private static final IvParameterSpec IV = new IvParameterSpec(new byte[] {	53, 65, -127, 34, 94, -27, -2, 98, 93, -90, -53, 12, -121,
																				-47, 93, -45});
//	private static final String secretKey = "dh3dilb68";
	private static final String PASSWORD = "lpzU5d9LwTkmFrsjk2ZL82AeFLGCijUtc8B9VO4AA6we";
	private static final String KEY_ALIAS = "encryption_key";
	private static final String KEYSTORE_FILE_PATH = "keystore.file.path";
	private static final String SYSTEM_ENCRYPTION = "system.encryption";
	private static java.security.Key secretKeySpec;
	static {
		InputStream stream = null;
		try {
			KeyStore store = KeyStore.getInstance("JCEKS");
			String keyStorePath = System.getProperty(KEYSTORE_FILE_PATH);
			if (keyStorePath == null) { // default place
				keyStorePath = System.getProperty("catalina.base") + "/conf/keystore";
			}
			stream = new FileInputStream(keyStorePath);
			store.load(stream, PASSWORD.toCharArray());
			secretKeySpec = store.getKey(KEY_ALIAS, PASSWORD.toCharArray());
		} catch (NoSuchAlgorithmException | IOException | CertificateException | KeyStoreException | UnrecoverableKeyException e) {
			log.error("Unable to load keystore or key", e);
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				log.warn("Unable to close file stream", e);
			}
		}
	}
	private static SupportedEncryption systemEncryption;
	static {
		try {
			String encryption = System.getProperty(SYSTEM_ENCRYPTION);
			if (encryption != null) {
				systemEncryption = SupportedEncryption.valueOf(encryption);
			} else {
				systemEncryption = SupportedEncryption.AES;
			}
		} catch (IllegalArgumentException e) {
			log.error("Unable to load system encryption. Initializing default AES encryption", e);
			systemEncryption = SupportedEncryption.AES;
		}
	}

	private enum SupportedEncryption {
		AES, BLOWFISH;
	}

	@SuppressWarnings("deprecation")
	public static String encrypt(String plainText)	throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
													IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException,
													CryptoException, InvalidAlgorithmParameterException {
		switch (systemEncryption) {
		case BLOWFISH:
			return Encryptor.encryptStringToString(plainText);
		case AES:
			/* falls through */
		default:
			return encryptRaw(plainText);

		}
	}

	public static String encrypt(long number)	throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
												IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException,
												CryptoException, InvalidAlgorithmParameterException {
		return encrypt(String.valueOf(number));
	}

	@SuppressWarnings("deprecation")
	public static String decrypt(String encryptedString)	throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
															NoSuchAlgorithmException, NoSuchPaddingException, CryptoException,
															InvalidAlgorithmParameterException {
		switch (systemEncryption) {
		case BLOWFISH:
			return Encryptor.decryptStringToString(encryptedString);
		case AES:
			/* falls through */
		default:
			return decryptRaw(encryptedString);
		}
	}

	private static String encryptRaw(String plainText)	throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
														IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException,
														InvalidAlgorithmParameterException {
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, IV);
		byte[] encrypted = cipher.doFinal(plainText.getBytes("UTF-8"));
		return HexUtil.byteArrayToHexString(encrypted);
	}

	private static String decryptRaw(String encryptedString)	throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
																NoSuchAlgorithmException, NoSuchPaddingException,
																InvalidAlgorithmParameterException {
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, IV);
		byte[] original = cipher.doFinal(HexUtil.hexStringToByteArray(encryptedString));
		return new String(original);
	}

	@SuppressWarnings("deprecation")
	public static String migrateEncryption(String encryptedString)	throws InvalidKeyException, NoSuchAlgorithmException,
																	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																	UnsupportedEncodingException, CryptoException,
																	InvalidAlgorithmParameterException {
		return encryptRaw(Encryptor.decryptStringToString(encryptedString));
	}
	public static void main(String[] args) throws Exception {
		if (args.length == 1) {
			String str = encrypt(args[0]);
			System.out.println("System encryption: " + systemEncryption);
			System.out.println(args[0] + ":" + str);
			System.out.println(str + ":" + decrypt(str));
		} else {
			System.out.println("USAGE: java AESUtil string-to-encrypt");
		}
	}
}