package com.anyoption.common.util;

import java.util.Map;

public class InvestmentValidatorParams {
    private int secForDev2;
    private int secBetweenInvestmentsSameIp;
    private int secBetweenInvestments;
    private boolean dev2Second;
    private Map<String, Object> requestParams;
    private Map<?, ?> sessionParams;
    
    public int getSecForDev2() {
        return secForDev2;
    }
    
    public void setSecForDev2(int secForDev2) {
        this.secForDev2 = secForDev2;
    }
    
    public int getSecBetweenInvestmentsSameIp() {
        return secBetweenInvestmentsSameIp;
    }
    
    public void setSecBetweenInvestmentsSameIp(int secBetweenInvestmentsSameIp) {
        this.secBetweenInvestmentsSameIp = secBetweenInvestmentsSameIp;
    }
    
    public int getSecBetweenInvestments() {
        return secBetweenInvestments;
    }
    
    public void setSecBetweenInvestments(int secBetweenInvestments) {
        this.secBetweenInvestments = secBetweenInvestments;
    }

	public Map<String, Object> getRequestParams() {
		return requestParams;
	}

	public void setRequestParams(Map<String, Object> requestParams) {
		this.requestParams = requestParams;
	}

	public boolean isDev2Second() {
		return dev2Second;
	}

	public void setDev2Second(boolean dev2Second) {
		this.dev2Second = dev2Second;
	}

	public Map<?, ?> getSessionParams() {
		return sessionParams;
	}

	public void setSessionParams(Map<?, ?> sessionParams) {
		this.sessionParams = sessionParams;
	}
}