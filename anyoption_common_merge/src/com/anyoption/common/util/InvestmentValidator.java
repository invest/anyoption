package com.anyoption.common.util;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.ExposureBean;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityMiniBean;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.ValidatorParamNIOU;
import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.ApiExternalUsersManagerBase;
import com.anyoption.common.managers.InvestmentRejectsManagerBase;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.OpportunitiesManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.UsersManagerBase;

public class InvestmentValidator extends InvestmentValidatorAbstract {

    private static final Logger log = Logger.getLogger(InvestmentValidator.class);
    public static final String DEV2_PARAMETER_NAME = "DEV2";
    public static final String REQUEST_PARAMETER_ID = "id:";
    public static final String REQUEST_PARAMETER_AMOUNT = "amount";
    public static final String REQUEST_PARAMETER_PAGE_LEVEL = "pageLevel";
    public static final String REQUEST_PARAMETER_PAGE_ODDS_WIN = "pageOddsWin";
    public static final String REQUEST_PARAMETER_PAGE_ODDS_LOSE = "pageOddsLose";
    public static final String REQUEST_PARAMETER_CHOICE = "choice";
    public static final String REQUEST_PARAMETER_TIME_BEGIN = "timeBegin";
    /*
     * 	D3 alert mail should stay active as usual only for investments above 150$ (in any currency).
 	*	for every 10 D3 investment below 150$ per user (same opp'),
 	*	send 1 alert mail (for the first D3 inv' send an alert mail, for the next 9 do not)
 	*	also in web !
     */
     private static int d3Threshold = 15000;// dollars * 100

    protected InvestmentValidatorParams params;

    public InvestmentValidator(InvestmentValidatorParams params) {
        this.params = params;
    }

    @Override
    public MessageToFormat validate(Connection conn, User user, Investment inv, OpportunityCacheBean o, String sessionId, WebLevelsCache levelsCache, WebLevelLookupBean levelLookupBean, double convertAmount, InvestmentRejects invRej, double rate, double requestAmount) throws SQLException {
    	MessageToFormat msg = validateCurrentLevel(inv.getCurrentLevel());
    	OpportunityMiniBean oppMiniBean = null;
    	if (msg == null) {
    		oppMiniBean = OpportunitiesManagerBase.getOpportunityMiniBean(o.getId());
    		msg = validateOpportunityOpened(oppMiniBean, user, sessionId, invRej);
    	}
        if (null == msg) {
            msg = validateUsersBalance(conn, user, inv);
        }
        if (null == msg) {
            long s = System.currentTimeMillis();
            msg = validateInvestment(conn, user, o, inv.getAmount(), inv.getOddsWin(), inv.getOddsLose(), invRej, inv.getUtcOffsetCreated());
            if (log.isDebugEnabled()) {
                log.debug("TIME: validateInvestmentLimits " + (System.currentTimeMillis() - s));
            }
        }

        SkinGroup userSkinGroup = SkinsManagerBase.getSkin(user.getSkinId()).getSkinGroup();

        WebLevelLookupBean wllb = null;
        Market market = MarketsManagerBase.getMarket(o.getMarketId());
        if (null == msg && !params.isDev2Second()) {
            wllb = new WebLevelLookupBean();
            wllb.setSkinGroup(userSkinGroup);
            msg = deviationCheck(levelsCache, wllb, o, inv, user.getUserName(), sessionId, "", invRej, market);
        }
        if (null == msg && convertAmount > market.getAmountForDev3() * 100 && UsersManagerBase.isUserMarketDisabled(user.getId(), o.getMarketId(), o.getScheduled(), true, inv.getApiExternalUserId())) {
            wllb = new WebLevelLookupBean();
            wllb.setSkinGroup(userSkinGroup);
            msg = deviationCheck(levelsCache, wllb, o, inv, user.getUserName(), sessionId, "3", invRej, market);
            if (null != msg) {
            	if(convertAmount > d3Threshold || InvestmentsManagerBase.sendD3Alert(user.getId(), o.getId(), rate, d3Threshold) ) {
            		sendNotifyMail(user, convertAmount, inv, ConstantsBase.CURRENCY_USD_ID, "deviation3", wllb.getRealLevel(), o, market);
            	}
            }
        }
        if (null == msg) {
        	wllb = new WebLevelLookupBean();
        	wllb.setSkinGroup(userSkinGroup);
            msg = determineDeviation2CheckState(market, convertAmount, wllb, requestAmount, o, inv, levelsCache, user, invRej, sessionId);
        }
        if (null == msg && UsersManagerBase.isUserMarketDisabled(user.getId(), o.getMarketId(), o.getScheduled(), false, inv.getApiExternalUserId())) {
            log.warn("User Market Disabled..." + printDetails(user.getUserName(), sessionId, o, inv, wllb));
            invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_USER_MARKET_DISABELD);
            invRej.setRejectAdditionalInfo("User Market Disabled");
            InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
            msg = new MessageToFormat("error.investment.disabled", null);
        }
        Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(user.getSkinId()).getDefaultLanguageId()).getCode());
        ExposureBean exposureBean = null;
        if (null == msg && (exposureBean = InvestmentsManagerBase.checkInvestmentExposure(o.getId(), inv.getAmount(), convertAmount, (int) inv.getTypeId(), invRej, userSkinGroup, o.getMarketId(), locale, user.getCurrencyId(), o.getScheduled(), user.getSkinId())).getErrorMessage() != null) {
        	msg = new MessageToFormat(exposureBean.getErrorMessageKey(), exposureBean.getErrorMessageParams());
        	if (exposureBean.isMaxExposureReached()) {
	            log.warn("Exposure reached" + printDetails(user.getUserName(), sessionId, o, inv, wllb));
	            sendNotifyMail(user, convertAmount, inv, ConstantsBase.CURRENCY_USD_ID, "exposure", null, o, market);
        	}
        }
        if (null == msg && params.getSecBetweenInvestmentsSameIp() > 0 &&
                UsersManagerBase.hasTheSameInvestmentInTheLastSecSameIp(inv.getIp(), params.getSecBetweenInvestmentsSameIp(), invRej)) {
            log.info("Submitting investment twice in less than " + params.getSecBetweenInvestmentsSameIp() + " sec from same ip:" +
                    " oppId: " + inv.getOpportunityId() +
                    " choice: " + inv.getTypeId() +
                    " user: " + user.getUserName() +
                    " sessionId: " + sessionId +
                    " ip: " + inv.getIp());
            msg = new MessageToFormat("error.investment.twicein10sec", null);
        }
        invRej.setSecBetweenInvestments(params.getSecBetweenInvestments());
        if (null == msg && UsersManagerBase.hasTheSameInvestmentInTheLastSec(user.getId(), inv.getOpportunityId(), inv.getTypeId(), invRej)) {
            log.info("Submitting the same investment twice in less than " + params.getSecBetweenInvestments() + " sec:" +
                    " oppId: " + inv.getOpportunityId() +
                    " choice: " + inv.getTypeId() +
                    " user: " + user.getUserName() +
                    " sessionId: " + sessionId);
            msg = new MessageToFormat("error.investment.twicein10sec", null);
        }
        if (null == msg) {
            msg = InvestmentsManagerBase.validateOpportunity(user, o, oppMiniBean, inv.getOddsWin(), inv.getOddsLose(), invRej);
        }

        //NIOU validation
       	if (null == msg) {
       		String msgNIOU = InvestmentsManagerBase.validateNIOUTypeWithCashBalance(
       				new ValidatorParamNIOU(user, 0, 0, o.getOpportunityTypeId(), inv.getAmount(), 0, 0), invRej);       		
       		if (msgNIOU != null) {
       			msg = new MessageToFormat(msgNIOU, null);
       		}
       	}
        
        if (null == msg) {
            // balance check before insert investment to avoid negative balance when doing simultaneous investments
            msg = validateUsersBalance(conn, user, inv);
        }
        if (null == msg) {
            levelLookupBean.setDevCheckLevel(wllb.getDevCheckLevel());
            levelLookupBean.setRealLevel(wllb.getRealLevel());
            levelLookupBean.setGroupClose(wllb.getGroupClose());
        }
        return msg;
    }

	private MessageToFormat determineDeviation2CheckState(	Market market, double convertAmount, WebLevelLookupBean wllb,
															double requestAmount, OpportunityCacheBean o, Investment inv,
															WebLevelsCache levelsCache, User user, InvestmentRejects invRej,
															String sessionId) {
		wllb.setOppId(o.getId());
		wllb.setFeedName(market.getFeedName());
		levelsCache.getLevels(wllb);
		MessageToFormat msg = null;
		float amountForDev2 = market.getAmountForDev2();
		Calendar currentTime = Calendar.getInstance();
		Calendar nightStartTime = Calendar.getInstance();
		Calendar nightEndTime = Calendar.getInstance();
		nightStartTime.set(Calendar.HOUR_OF_DAY, 22);
		nightStartTime.set(Calendar.MINUTE, 00);
		nightStartTime.set(Calendar.SECOND, 00);
		nightEndTime.set(Calendar.HOUR_OF_DAY, 06);
		nightEndTime.set(Calendar.MINUTE, 00);
		nightEndTime.set(Calendar.SECOND, 00);
		if (nightStartTime.before(currentTime) || nightEndTime.after(currentTime)) { // if we are at night time mean btw 22:00 to 06:00 gmt
			amountForDev2 = market.getAmountForDev2Night();
		}
		if (convertAmount > amountForDev2 * 100) {
			if (params.isDev2Second()) { // the second request should wait for dev2 time and make another deviation check
				boolean valid = false;
				Map<?, ?> sessionParams = params.getSessionParams();
				long timeBegin = System.currentTimeMillis();
				synchronized (sessionParams) {
					if (sessionParams != null) {
						if (sessionParams.get(REQUEST_PARAMETER_AMOUNT) != null
							&& ((Double) sessionParams.get(REQUEST_PARAMETER_AMOUNT)) == requestAmount
							&& sessionParams.get(REQUEST_PARAMETER_PAGE_LEVEL) != null
							&& ((Double) sessionParams.get(REQUEST_PARAMETER_PAGE_LEVEL)).doubleValue() == inv.getCurrentLevel()
							&& sessionParams.get(REQUEST_PARAMETER_PAGE_ODDS_WIN) != null
							&& ((Double) sessionParams.get(REQUEST_PARAMETER_PAGE_ODDS_WIN)) == inv.getOddsWin()
							&& sessionParams.get(REQUEST_PARAMETER_PAGE_ODDS_LOSE) != null
							&& ((Double) sessionParams.get(REQUEST_PARAMETER_PAGE_ODDS_LOSE)) == inv.getOddsLose()
							&& sessionParams.get(REQUEST_PARAMETER_CHOICE) != null
							&& ((Long) sessionParams.get(REQUEST_PARAMETER_CHOICE)) == inv.getTypeId()
							&& sessionParams.get(REQUEST_PARAMETER_TIME_BEGIN) != null) {
							valid = true;
							timeBegin = (Long) sessionParams.get(REQUEST_PARAMETER_TIME_BEGIN);
						} else {
							log.debug("Dev 2 params not valid. sessionParams: " + sessionParams);
						}
						sessionParams.clear();
					}
				}
				if (valid) {
					long time = market.getSecForDev2() * 1000;
					if (params.isDev2Second()) {
						time -= System.currentTimeMillis() - timeBegin; // sec for dev2 minus elapsed time
					}
					if (time > 0) {
						try {
							Thread.sleep(time);
						} catch (InterruptedException ie) {
							// do nothing
						}
					}
					if (log.isDebugEnabled()) {
						log.debug("Second deviation check.");
					}
					long s = System.currentTimeMillis();
					if (log.isDebugEnabled()) {
						log.log(Level.DEBUG, "TIME: get current level " + (System.currentTimeMillis() - s));
					}
					msg = deviationCheck(levelsCache, wllb, o, inv, user.getUserName(), sessionId, "2", invRej, market);
				} else {
					msg = new MessageToFormat("error.investment", null);
				}
			} else { // the first request should return the actual seconds for dev2
				Map<String, Object> requestParams = new HashMap<String, Object>();
				params.setRequestParams(requestParams);
				requestParams.put(REQUEST_PARAMETER_AMOUNT, requestAmount);
				requestParams.put(REQUEST_PARAMETER_PAGE_LEVEL, inv.getCurrentLevel());
				requestParams.put(REQUEST_PARAMETER_PAGE_ODDS_WIN, inv.getOddsWin());
				requestParams.put(REQUEST_PARAMETER_PAGE_ODDS_LOSE, inv.getOddsLose());
				requestParams.put(REQUEST_PARAMETER_CHOICE, inv.getTypeId());
				requestParams.put(REQUEST_PARAMETER_TIME_BEGIN, System.currentTimeMillis());
				msg = new MessageToFormat(DEV2_PARAMETER_NAME, new Integer[] {market.getSecForDev2()});
			}
		}
		return msg;
	}

	protected static MessageToFormat deviationCheck(WebLevelsCache levelsCache, WebLevelLookupBean wllb, OpportunityCacheBean o, Investment inv, String userName, String sessionId, String checkNumber, InvestmentRejects invRej, Market market) {
        long s = System.currentTimeMillis();
        wllb.setOppId(inv.getOpportunityId());
        wllb.setFeedName(market.getFeedName());
        levelsCache.getLevels(wllb);
        if (log.isDebugEnabled()) {
            log.debug("TIME: get current level " + (System.currentTimeMillis() - s));
        }
        if (wllb.getDevCheckLevel() != 0 && wllb.getRealLevel() != 0) { // check if we have levels (else we have conn prob)
            double checkLevel = wllb.getDevCheckLevel();
            BigDecimal deviation = market.getAcceptableDeviation();
            if (checkNumber.equalsIgnoreCase("3")) { //dev 3 check
                checkLevel = wllb.getRealLevel();
                deviation = market.getAcceptableDeviation3();
            }
            double crrSpread = Math.abs(checkLevel - inv.getCurrentLevel());
            double acceptableSpread = checkLevel * deviation.doubleValue();
            if (log.isDebugEnabled()) {
                log.debug("realLevel: " + wllb.getRealLevel() +
                        " pageLevel: " + inv.getCurrentLevel() +
                        " wwwLevel: " + wllb.getDevCheckLevel() +
                        " crr spread: " + crrSpread +
                        " deviation: " + deviation +
                        " acceptable spread: " + acceptableSpread);
            }
            if ((crrSpread <= acceptableSpread) ||
                    (checkLevel < inv.getCurrentLevel() && inv.getTypeId() == InvestmentType.CALL) ||
                    (checkLevel > inv.getCurrentLevel() && inv.getTypeId() == InvestmentType.PUT)) {

            	if (log.isInfoEnabled() && crrSpread > acceptableSpread && checkLevel < inv.getCurrentLevel() && inv.getTypeId() == InvestmentType.CALL) {
                    log.info("Skip calculating deviation. CALL on higher level.");
                }
                if (log.isInfoEnabled() && crrSpread > acceptableSpread && checkLevel > inv.getCurrentLevel() && inv.getTypeId() == InvestmentType.PUT) {
                    log.info("Skip calculating deviation. PUT on lower level.");
                }

                return null;
            } else {
                log.warn("Unacceptable deviation" + checkNumber + "." +
                        " user: " + userName +
                        " sessionId: " + sessionId +
                        " market name: " + o.getMarketDisplayName() +
                        " exp time: " + o.getTimeEstClosing() +
                        " type id (CALL = 1, PUT = 2): " + inv.getTypeId() +
                        " amount: " + inv.getAmount() +
                        " return: " + inv.getOddsWin());
                String msgStart = "Unacceptable Deviation " + checkNumber;
                String msgEnd = "";
                if (checkNumber.length() == 0) { //dev 1 check
                    invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_DEVIATION);
                } else if (checkNumber.equalsIgnoreCase("2")) { //dev 2 check
                    msgEnd = " , AmountForDev2: " + market.getAmountForDev2() + " , SecForDev2: " + market.getSecForDev2();
                    invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_DEVIATION_2);
                } else { //dev 3 check
                    invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_DEVIATION_3);
                }
                DecimalFormat sd = new DecimalFormat("###,###,##0.000000");
                invRej.setRejectAdditionalInfo(msgStart + ":, crrSpread: " + sd.format(crrSpread) + " , deviation: " + deviation + " , acceptableSpread: " + sd.format(acceptableSpread) + msgEnd);
                invRej.setRealLevel(wllb.getRealLevel());
                invRej.setWwwLevel(wllb.getDevCheckLevel());
                InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
                return new MessageToFormat("error.investment.deviation", null);
            }
        } else {
            log.warn("No www level or real level. user: " + userName + " sessionId: " + sessionId);
        }
        return new MessageToFormat("error.investment", null);
    }

    public static void sendNotifyMail(User user, double convertedAmount, Investment inv, long currencyId, String emailType, Double realLevel, OpportunityCacheBean o, Market market){
        log.info("Sending mail about " + emailType.toUpperCase() + " to group... ");
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
        String titleType = user.getClassId() == ConstantsBase.USER_CLASS_TEST ? "TEST " : "";
        titleType += emailType.equalsIgnoreCase("deviation3") ? "D3: " : emailType + " REACHED: ";
        String apiexternalreference = user.getUserName();
        if (inv.getApiExternalUserId() > 0) {
        	try {
				apiexternalreference = ApiExternalUsersManagerBase.getApiExternalUserReferenceById(inv.getApiExternalUserId()) + "(" + apiexternalreference + ")";
			} catch (SQLException e) {
				log.error("cant get api external user reference for api external user id = " + inv.getApiExternalUserId(), e);
			}
		}
		String marketName = MarketsManagerBase.getMarketName(Skin.SKIN_REG_EN, market.getId());
		if (o.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS) {
			marketName += " +";
		}
		serverProperties.put("url", CommonUtil.getConfig("email.server"));
        serverProperties.put("auth", "true");
        serverProperties.put("user", CommonUtil.getConfig("email.uname"));
        serverProperties.put("pass", CommonUtil.getConfig("email.pass"));

        Hashtable<String, String> emailProperties = new Hashtable<String, String>();
        emailProperties.put("from", CommonUtil.getConfig("email.from"));
        emailProperties.put("to", CommonUtil.getConfig(emailType + ".mailgroup"));
        emailProperties.put("subject", titleType + " - " + apiexternalreference + " - " + Investment.TYPE_NAMES.get(inv.getTypeId()) + " - " + marketName + " - " + Investment.SCHEDULED_NAMES.get(o.getScheduled()));
        DecimalFormat sd = new DecimalFormat("###,###,##0.000000");
        String body = new String();
        body += " user: " + apiexternalreference +"<br/>";
        body += " market name: " + marketName +"<br/>";
        body += " scheduled: " + Investment.SCHEDULED_NAMES.get(o.getScheduled()) +"<br/>";
        body += " exp time: " + o.getTimeEstClosing() +"<br/>";
        body += " type: " + Investment.TYPE_NAMES.get(inv.getTypeId()) + "<br/>";
        body += " amount: " + CommonUtil.formatCurrencyAmountAO(convertedAmount, true, currencyId) + "<br/>" ;
        body += " return: " + inv.getOddsWin() +"<br/>";
        body += " level: " + inv.getCurrentLevel() +"<br/>";
        if (null != realLevel) { //dev 3 email
            body += " reuters level: " + sd.format(realLevel) +"<br/>";
            double crrSpread = Math.abs(realLevel - inv.getCurrentLevel());
            body += " spread: " + sd.format(crrSpread) +"<br/>";
            body += " acceptable level dev 3: " + sd.format(market.getAcceptableDeviation3().doubleValue()) +"<br/>";
        }
        emailProperties.put("body", body);

        CommonUtil.sendEmail(serverProperties, emailProperties, null);
    }

    @Override
    protected String printDetails(String userName, String sessionId, OpportunityCacheBean o, Investment inv, WebLevelLookupBean wllb) {
		String details = super.printDetails(userName, sessionId, o, inv, wllb);
		details = " type id (CALL = 1, PUT = 2): " + inv.getTypeId() +
				" realLevel: " + wllb.getRealLevel() +
				" wwwLevel: " + wllb.getDevCheckLevel();
    	return details;
    }
}