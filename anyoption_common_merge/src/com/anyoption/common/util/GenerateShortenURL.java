package com.anyoption.common.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

/**
 * Class that retrieve shorten URL from goo.gl
 * For reference see http://goo.gl/ 
 * @author eranl
 *
 */
public class GenerateShortenURL implements Serializable {

	private static final long serialVersionUID = -6772993321286752382L;
	private static final Logger log = Logger.getLogger(GenerateShortenURL.class);	
	private static final Gson gson = new Gson();

	public static String shorten(String longUrl) {
		String result = new String();
		GsonGooGl gsonGooGl = new GsonGooGl(longUrl);
		try {			
			String gooGLURL = CommonUtil.getEnum("short_url", "short_url_goo_gl");
			log.debug("Going to short URL: " + longUrl + " with goo.gl API: " + gooGLURL);
			URL url = new URL(gooGLURL);
			URLConnection urlConn = url.openConnection();
			urlConn.setDoInput(true); // Let the run-time system (RTS) know that
										// we want input.
			urlConn.setDoOutput(true); // Let the RTS know that we want to do
										// output.
			urlConn.setUseCaches(false); // No caching
			urlConn.setRequestProperty("Content-Type", "application/json"); // Specify the content type.
			DataOutputStream printout = new DataOutputStream(urlConn.getOutputStream()); // Send POST output.
			String content = gson.toJson(gsonGooGl);
			printout.writeBytes(content);
			printout.flush();
			printout.close();
			DataInputStream input = new DataInputStream(urlConn.getInputStream()); // Get response data.
			Scanner sc = new Scanner(input);
			while (sc.hasNext()) {
				result += sc.next();
			}
			GooGlResult gooGlResult = gson.fromJson(result, GooGlResult.class);
			log.debug("Finish to generate short URL longURL: " + longUrl + " to shortURL: " +  gooGlResult.getId());
			return gooGlResult.getId();
		} catch (Exception e) {
			log.error("Error while trying to get shorten URL from goo.gl", e);
			return null;
		}
	}
}

class GsonGooGl {
	private String longUrl;	
	public GsonGooGl() {	
	}
	public GsonGooGl(String longUrl) {
		this.longUrl = longUrl;
	}
	/**
	 * @return the longUrl
	 */
	public String getLongUrl() {
		return longUrl;
	}
	/**
	 * @param longUrl the longUrl to set
	 */
	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}

}

class GooGlResult {
	private String kind;
	private String id;
	private String longUrl;
	
	public GooGlResult() {		
	}

	/**
	 * @return the kind
	 */
	public String getKind() {
		return kind;
	}

	/**
	 * @param kind the kind to set
	 */
	public void setKind(String kind) {
		this.kind = kind;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the longUrl
	 */
	public String getLongUrl() {
		return longUrl;
	}

	/**
	 * @param longUrl the longUrl to set
	 */
	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}

}
