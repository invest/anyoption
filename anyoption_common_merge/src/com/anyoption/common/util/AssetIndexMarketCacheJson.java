package com.anyoption.common.util;

import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.AssetIndexBase;
import com.anyoption.common.managers.AssetIndexMarketManagerBase;


public class AssetIndexMarketCacheJson {
    private static final Logger log = Logger.getLogger(AssetIndexMarketCacheJson.class);
    
    private static HashMap<Long, HashMap<Long, AssetIndexBase>> assetIndexMarket = new HashMap<Long, HashMap<Long, AssetIndexBase>>();

	public static void init() {		
		try {
			assetIndexMarket = AssetIndexMarketManagerBase.getAssetyByMarketHM();
		} catch (SQLException e) {
			log.error("Can't init AssetIndex Cache", e);
			
		}
	}

	public static HashMap<Long, HashMap<Long, AssetIndexBase>> getAssetIndexMarket() {
		return assetIndexMarket;
	}

	public static void setAssetIndexMarket(HashMap<Long, HashMap<Long, AssetIndexBase>> assetIndexMarket) {
		AssetIndexMarketCacheJson.assetIndexMarket = assetIndexMarket;
	}
    

}