package com.anyoption.common.util;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.MarketingTracking;
import com.anyoption.common.beans.base.MarketingTrackingCookieStatic;
import com.anyoption.common.managers.MarketingTrackingManager;
import com.anyoption.common.managers.SkinsManagerBase;
import com.copyop.common.Constants;

public class MarketingTracker extends MarketingTrackerBase{

	private static final Logger log = Logger.getLogger(MarketingTracker.class);
	
	private static final String MARKETING_TRACKING_STATIC_COOKIE = "smt";
	private static final String DELETED_COOKIE_VALUES = "deleted";
	private static final String DEFAUL_STATIC_COOKIE = "dfs";
	private static final long STATIC = 0;
	private static final long DYNAMIC = 1;	
	
	private static String defaultCookie = "";
	private static String requestHeadCookie = "";
			
	private static MarketingTrackingCookieStatic getStaticCookiePart(Cookie marketingTrackerCookie, HttpServletRequest request, HttpServletResponse response) {

		MarketingTrackingCookieStatic mtcStatic = new MarketingTrackingCookieStatic();
		mtcStatic = MarketingTrackerUtil.getStaticParsedValue(marketingTrackerCookie.getValue());		
		if (mtcStatic == null){
			mtcStatic = setDeafultMarketingCookie(request, response);
			log.info("Marketing Tracker getStaticCookiePart is null and set Default Value:" + mtcStatic + " and Header cookies value:" + requestHeadCookie );
		}		
		return mtcStatic;
	}	
	
	private static MarketingTrackingCookieStatic setDeafultMarketingCookie(HttpServletRequest request, HttpServletResponse response){
		MarketingTrackingCookieStatic mtcStatic = new MarketingTrackingCookieStatic();
		try{			
			HttpSession session = request.getSession();
			String mId = DEFAUL_STATIC_COOKIE + session.getId();			
			String combId = (String) request.getParameter(ConstantsBase.COMBINATION_ID);
			if (null == combId) {
				combId = Long.toString( SkinsManagerBase.getCombinationIdtBySkins(Utils.getSkinId(session, request))); 
			}
			String dynamicParam = (String) request.getParameter(ConstantsBase.DYNAMIC_PARAM);
			String aff_sub1 = (String) request.getParameter(Constants.AFF_SUB1);
			String aff_sub2 = (String) request.getParameter(Constants.AFF_SUB2);
			String aff_sub3 = (String) request.getParameter(Constants.GCLID);
			String httpRefferer = (String) request.getHeader(ConstantsBase.HTTP_REFERE);
			Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
			String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(date);
			String utmSource = (String) request.getParameter(ConstantsBase.UTM_SOURCE);
			
			mtcStatic.setMs(mId);
			mtcStatic.setCs(combId);
			mtcStatic.setHttp(httpRefferer);
			mtcStatic.setDp(dynamicParam);
			mtcStatic.setTs(date);
				
			String params = MarketingTrackerBase.getStaticCookieData(mId, combId, httpRefferer, dynamicParam, dateForDisplay, utmSource, true, aff_sub1, aff_sub2, aff_sub3);
			deleteMarketingTrackerCookie(request, response);
			CommonUtil.addCookie(MARKETING_TRACKING_STATIC_COOKIE, params, request, response, MARKETING_TRACKING_STATIC_COOKIE);
			if (log.isDebugEnabled()) {
				log.debug("Marketing Tracker Cookie create default cookie with params : " + params);
			}
			setCookieInRequestAttribute(request, params);
		} catch (Exception e) {
			log.error("Marketing Tracker setDeafultMarketingCookie", e);
		}		
		return mtcStatic;		
	}	
	
	private static ArrayList<MarketingTracking> getMarketingTrackerList(Cookie marketingTracker, HttpServletRequest request, HttpServletResponse response, 
			long contactId, long userId, long type, String midByUserId) {
		
		ArrayList<MarketingTracking> list = new ArrayList<MarketingTracking>();
		try {			
			MarketingTrackingCookieStatic mtcStatic = getStaticCookiePart(marketingTracker, request, response);
			if(mtcStatic != null){
				// Add Activity				
					MarketingTracking mtActivity = new MarketingTracking();
					mtActivity.setmId(mtcStatic.getMs());
					
					//try to cast combId to Long
					Long combId;
					try {
					    combId = new Long(mtcStatic.getCs());
					} catch (Exception e) {
					    combId = DEFAULT_COMBINATION_ID;
					    log.error("Marketing Tracker CombId error set default:" + mtcStatic.getCs() + " Exc: ", e);
					}
					
					mtActivity.setCombinationId(combId);
					mtActivity.setTimeStatic(mtcStatic.getTs());
					mtActivity.setHttpReferer(mtcStatic.getHttp());
					mtActivity.setDyanmicParameter(mtcStatic.getDp());
					mtActivity.setUtmSource(mtcStatic.getUtmSource());
					mtActivity.setAff_sub1(mtcStatic.getAff_sub1());
					mtActivity.setAff_sub2(mtcStatic.getAff_sub2());
					mtActivity.setAff_sub3(mtcStatic.getAff_sub3());
					mtActivity.setCombinationIdDynamic(0);
					mtActivity.setTimeDynamic(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
					mtActivity.setHttpRefererDynamic("");
					mtActivity.setDyanmicParameterDynamic("");
					mtActivity.setUtmSourceDynamic("");
					mtActivity.setContactId(contactId);
					mtActivity.setUserId(userId);
					mtActivity.setMarketingTrackingActivityId(type);
	
					list.add(mtActivity);
			}
		} catch (Exception e) {
			log.error("Marketing Tracker getMarketingTrackerList with Header cookies value:" + requestHeadCookie + " Exc: ", e);
		}
		return list;
	}	
	
	private static boolean afterFourtyFiveDays(Cookie marketingTracker, HttpServletRequest request, HttpServletResponse response) {
		long daysBetween = 0;
		boolean result = false;
		try {
			MarketingTrackingCookieStatic mtcStatic = getStaticCookiePart(marketingTracker, request, response);
			Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
			long millsPerDay = 1000 * 60 * 60 * 24;
			long d2 = date.getTime() / millsPerDay;
			long d1 = mtcStatic.getTs().getTime() / millsPerDay;
			daysBetween = d2 - d1;
			if (daysBetween > FOURTY_FIVE_CHECK_SUM) {
				if (!MarketingTrackingManager.isHaveActivity(mtcStatic.getMs())) {
					result = true;
				}
			}
		} catch (Exception e) {
			log.error(
					"Marketing Tracker afterFourtyFiveDays with Header cookies value:" + requestHeadCookie + " and cookie : " + marketingTracker.getValue() + " Exc: ", e);
		}
		return result;
	}
	
	private static void checkCookieAfterLogin(HttpServletRequest request, HttpServletResponse response, String midByUserId, Cookie marketingTracker, long contactId, long userId) {
		String newStaticRow = "";
		try {
			MarketingTrackingCookieStatic mtcStatic = getStaticCookiePart(marketingTracker, request, response);			
			if (!midByUserId.equals(mtcStatic.getMs())) {
				mtcStatic.setMs(midByUserId);
				String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(mtcStatic.getTs());				
				newStaticRow = MarketingTrackerBase.getStaticCookieData( mtcStatic.getMs(), mtcStatic.getCs(), mtcStatic.getHttp(), mtcStatic.getDp(), dateForDisplay, 
						mtcStatic.getUtmSource(), true, mtcStatic.getAff_sub1(), mtcStatic.getAff_sub2(), mtcStatic.getAff_sub3());
				deleteMarketingTrackerCookie(request, response);
				CommonUtil.addCookie(MARKETING_TRACKING_STATIC_COOKIE, newStaticRow, request, response, MARKETING_TRACKING_STATIC_COOKIE);
				setCookieInRequestAttribute(request, newStaticRow);
				// Add record for noExist users														
				ArrayList<MarketingTracking> list = getMarketingTrackerListForClick(newStaticRow, null);
				if (list.size() > 0) {
					list.get(0).setContactId(contactId);
					list.get(0).setUserId(userId);
					MarketingTrackingManager.insertMarketingTracking(list);								
				}
				
			}
		} catch (Exception e) {
			log.error("Marketing Tracker checkCookieAfterLogin with Header cookies value:" + requestHeadCookie + " and cookie : " + marketingTracker.getValue() + " Exc: ", e);
		}
	}
	
	protected static Cookie getMarketingTracker(HttpServletRequest request, String cookieName) {
		
		Cookie marketingTracker = null;		
		try {
		    marketingTracker = CommonUtil.getCookie(request, cookieName);
		} catch (Exception e) {
			log.error("Marketing Tracker getMarketingTracker :" + requestHeadCookie  +" Exc: ", e);
		}
		return marketingTracker;
	}
	
	private static void createMarketingTrackerCookie(HttpServletRequest request, HttpServletResponse response) {
		String params = getParams(request, STATIC);
		CommonUtil.addCookie(MARKETING_TRACKING_STATIC_COOKIE, params, request, response, MARKETING_TRACKING_STATIC_COOKIE);
		if (log.isDebugEnabled()) {
			log.debug("Marketing Tracker Cookie create with params : " + params);
		}
		setCookieInRequestAttribute(request, params);		
	}
	
	private static void setCookieInRequestAttribute(HttpServletRequest request, String params) {
		Cookie cookie = new Cookie(MARKETING_TRACKING_STATIC_COOKIE, null);		
		try {
			Cookie mtCookie = new Cookie(MARKETING_TRACKING_STATIC_COOKIE, params);
			mtCookie.setPath("/");
			mtCookie.setMaxAge(365 * 24 * 60 * 60 * 1000); // about year
			// sub domain
			String property = CommonUtil.getConfig(ConstantsBase.COOKIE_DOMAIN, null);
			if (null != property) {
				mtCookie.setDomain(property);
			}			
			cookie = mtCookie;
		} catch (Exception e) {
			log.error("Marketing Tracker setCookieInRequestAttribute with params : " + params , e);
		}
		request.setAttribute(MARKETING_TRACKING_STATIC_COOKIE, cookie);
	}
	
	private static String getParams(HttpServletRequest request, long type) {
		String params = null;
		try{			
			HttpSession session = request.getSession();
			String mId = defaultCookie + session.getId();			
			String combId = (String) request.getParameter(ConstantsBase.COMBINATION_ID);
			if (null == combId) {
				combId =Long.toString( SkinsManagerBase.getCombinationIdtBySkins(Utils.getSkinId(session, request)));
			}
			String dynamicParam = (String) request.getParameter(ConstantsBase.DYNAMIC_PARAM);
			String affSub1 = (String) request.getParameter(Constants.AFF_SUB1);
			String affSub2 = (String) request.getParameter(Constants.AFF_SUB2);
			String affSub3 = (String) request.getParameter(Constants.GCLID);

			String httpRefferer = (String) request.getHeader(ConstantsBase.HTTP_REFERE);
	        if (CommonUtil.isParameterEmptyOrNull(httpRefferer) || !httpRefferer.contains(".") ){
	                httpRefferer = request.getRequestURL().toString();
	                if (request.getQueryString() != null){
	                    httpRefferer = httpRefferer + "?" + request.getQueryString();
	                }	                
	            }
	         
			Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
			String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(date);
			String utmSource = (String) request.getParameter(ConstantsBase.UTM_SOURCE);
	
			if (type == STATIC) {				
				params = MarketingTrackerBase.getStaticCookieData(mId, combId, httpRefferer, dynamicParam, dateForDisplay, utmSource, false, affSub1, affSub2, affSub3);
			} else {
				params = "HTTP{" + httpRefferer + "^CD{" + combId + "^TD{" + dateForDisplay + "^DP{" + dynamicParam + "^UTM{" + utmSource+ "^aff_sub1{" + affSub1 + "^aff_sub2{" + affSub2 + "^aff_sub3{" + affSub3;
	
			}
			params = HexUtil.byteArrayToHexString(params.getBytes("UTF-8"));			
			
		} catch (Exception e) {
			log.error("Marketing Tracker getParams", e);
		}
		return params;
	}
	
	private static void deleteMarketingTrackerCookie(HttpServletRequest request, HttpServletResponse response) {				
		Cookie[] cs = request.getCookies();
		try {
			if (cs != null) {

				for (int i = 0; i < cs.length; i++) {					
					if (cs[i].getName().contains(MARKETING_TRACKING_STATIC_COOKIE)){
						cs[i].setPath("/");
						// sub domain
						String property = CommonUtil.getConfig(ConstantsBase.COOKIE_DOMAIN, null);
						if (null != property) {
							cs[i].setDomain(property);
						}
						cs[i].setValue(DELETED_COOKIE_VALUES);
						CommonUtil.deleteCookie(cs[i], response);					
					}
				}
			}
		} catch (Exception e) {
			log.error("Marketing Tracker getMarketingTracker :" + requestHeadCookie  +" Exc: ", e);
		}
	}	
	
	private static void deleteOldCookie(HttpServletRequest request, HttpServletResponse response) {
		String[] cookiesForDelete = { "mtc", "TRC" };		
		for (String cookieName : cookiesForDelete) {
			Cookie oldCookie = getMarketingTracker(request, cookieName);
			try {
				if (oldCookie != null) {
					oldCookie.setPath("/");
					// sub domain
					String property = CommonUtil.getConfig(ConstantsBase.COOKIE_DOMAIN, null);
					if (null != property) {
						oldCookie.setDomain(property);
					}
					oldCookie.setValue("");
					CommonUtil.deleteCookie(oldCookie, response);
				}
			} catch (Exception e) {
				log.error("Marketing Tracker deleteOldCookie: ", e);
			}
		}
	}	
	
	private static void replaceMarketingTrackerCookieAfterDaysCheck(Cookie marketingTracker, HttpServletRequest request, HttpServletResponse response) throws SQLException {				
		MarketingTrackingCookieStatic staticPartFromCookie = getStaticCookiePart(marketingTracker, request, response);
		MarketingTrackingCookieStatic staticPartFromDB = MarketingTrackingManager.getMarketingTrackingCookieStatic(staticPartFromCookie.getMs());
		if (staticPartFromDB.getMs() != null && !MarketingTrackerUtil.isAfterCheckDatesOrNull(staticPartFromDB.getTs())) {
			String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(staticPartFromDB.getTs());			

			String params = MarketingTrackerBase.getStaticCookieData(staticPartFromDB.getMs(), staticPartFromDB.getCs(), staticPartFromDB.getHttp(), staticPartFromDB.getDp(), 
					dateForDisplay, staticPartFromDB.getUtmSource(), true, staticPartFromDB.getAff_sub1(), staticPartFromDB.getAff_sub2(), staticPartFromDB.getAff_sub3());

			deleteMarketingTrackerCookie(request, response);
			CommonUtil.addCookie(MARKETING_TRACKING_STATIC_COOKIE, params, request, response, MARKETING_TRACKING_STATIC_COOKIE);
			if (log.isDebugEnabled()) {
				log.debug("Marketing Tracker Cookie replaceMarketingTrackerCookieAfterActivity params : " + params);
			}
			setCookieInRequestAttribute(request, params);
		} else {
			deleteMarketingTrackerCookie(request, response);
			createMarketingTrackerCookie(request, response);
		}
	}	
	
	private static void replaceMarketingTrackerCookieAfterActivity(HttpServletRequest request, HttpServletResponse response, String mId) throws SQLException {		
		
		MarketingTrackingCookieStatic staticPart = MarketingTrackingManager.getMarketingTrackingCookieStatic(mId);
		if (staticPart != null) {
			String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(staticPart.getTs());
			
			String params = MarketingTrackerBase.getStaticCookieData(staticPart.getMs(), staticPart.getCs(), staticPart.getHttp(), staticPart.getDp(), dateForDisplay, 
					staticPart.getUtmSource(), true, staticPart.getAff_sub1(), staticPart.getAff_sub2(), staticPart.getAff_sub3());

			deleteMarketingTrackerCookie(request, response);
			CommonUtil.addCookie(MARKETING_TRACKING_STATIC_COOKIE, params, request, response, MARKETING_TRACKING_STATIC_COOKIE);
			if (log.isDebugEnabled()) {
				log.debug("Marketing Tracker Cookie replaceMarketingTrackerCookieAfterActivity params : " + params);
			}
			setCookieInRequestAttribute(request, params);
		}
	}	

	private static void addClickMarketingTrackerCookie(Cookie marketingTracker, HttpServletRequest request) throws SQLException {		
		String staticParts  = marketingTracker.getValue();
		String dynamicParts = getParams(request, DYNAMIC);
		insertMarketingTrackerClick(staticParts, dynamicParts);
	}	


	public static MarketingTracking getStaticMarketingTracker(HttpServletRequest request,HttpServletResponse response) {
		MarketingTracking mtStatic = new MarketingTracking();
		Cookie marketingTracker = getMarketingTracker(request, MARKETING_TRACKING_STATIC_COOKIE);	
		try { 			
			MarketingTrackingCookieStatic mtcStatic = getStaticCookiePart(marketingTracker, request, response);
			
			mtStatic.setmId(mtcStatic.getMs());
			mtStatic.setCombinationId(new Long(mtcStatic.getCs()));
			mtStatic.setHttpReferer(mtcStatic.getHttp());
			mtStatic.setDyanmicParameter(mtcStatic.getDp());
			mtStatic.setUtmSource(mtcStatic.getUtmSource());
		} catch (Exception e) {
			log.error("Marketing Tracker getStaticMarketingTracker with Header cookies value:" + requestHeadCookie + " and cookie : " + marketingTracker.getValue() + " Exc: ", e);
		}
		return mtStatic;		
	}
	
	public static void checkMarketingTrackerCookie(HttpServletRequest request, HttpServletResponse response, boolean doNotAddClickIfCookieExist) {
		Cookie marketingTracker = getMarketingTracker(request, MARKETING_TRACKING_STATIC_COOKIE);
		try {
			requestHeadCookie = request.getHeader("Cookie");
			deleteOldCookie(request, response);

			if (null != marketingTracker) {
				
				if (marketingTracker.getValue().contains((DELETED_COOKIE_VALUES))){
					marketingTracker = (Cookie) request.getAttribute(MARKETING_TRACKING_STATIC_COOKIE);
				}
				
				if (!doNotAddClickIfCookieExist) {
					if (log.isDebugEnabled()) {
						log.debug("checkMarketingTrackerCookie  Begin addClickMarketingTrackerCookie: ");
					}
					addClickMarketingTrackerCookie(marketingTracker, request);
				}

				if (log.isDebugEnabled()) {
					log.debug("checkMarketingTrackerCookie  Begin fourtyFiveDaysCheck: ");
				}
				if (afterFourtyFiveDays(marketingTracker, request, response)) {
					replaceMarketingTrackerCookieAfterDaysCheck(marketingTracker, request, response);
				}
			} else {
				if (log.isDebugEnabled()) {
					log.debug("checkMarketingTrackerCookie  Begin createMarketingTrackerCookie: ");
				}
				createMarketingTrackerCookie(request, response);
			}
		} catch (Exception e) {
			log.error(
					"Marketing Tracker checkMarketingTrackerCookie with Header cookies value:" + requestHeadCookie + " and cookie :  " + marketingTracker.getValue() + " Exc: ", e);
		}
	}
	
	public static void checkExistCookieWhenInsertContact(HttpServletRequest request, HttpServletResponse response, String email, String phone){
	       try {
	           requestHeadCookie = request.getHeader("Cookie");
	           Cookie marketingTracker = getMarketingTracker(request, MARKETING_TRACKING_STATIC_COOKIE);
	           String newCookie = checkStaticPartByContactDetailsInsertingContact(email, phone, marketingTracker.getValue());
	           if(newCookie != null){
	               deleteMarketingTrackerCookie(request, response);	               
	               CommonUtil.addCookie(MARKETING_TRACKING_STATIC_COOKIE, newCookie, request, response, MARKETING_TRACKING_STATIC_COOKIE);
	               setCookieInRequestAttribute(request, newCookie);
	           }
	        } catch (Exception e) {
	            log.error("Marketing Tracker checkExistCookieWhenInsertContact with Header cookies value:" + requestHeadCookie + " Exc: ", e);
	        }	    
	}
	
	   public static void checkExistCookieWhenInsertUser(HttpServletRequest request, HttpServletResponse response, String email, String mobilePhone, String landLinePhone, long contactId, long userId){
           try {
               requestHeadCookie = request.getHeader("Cookie");
               Cookie marketingTracker = getMarketingTracker(request, MARKETING_TRACKING_STATIC_COOKIE);
               String newCookie = checkStaticPartByContactDetailsInsertingUser(email, mobilePhone, landLinePhone, contactId, userId, marketingTracker.getValue());
               if(newCookie != null){
                   deleteMarketingTrackerCookie(request, response);                
                   CommonUtil.addCookie(MARKETING_TRACKING_STATIC_COOKIE, newCookie, request, response, MARKETING_TRACKING_STATIC_COOKIE);
                   setCookieInRequestAttribute(request, newCookie);
               }
            } catch (Exception e) {
                log.error("Marketing Tracker checkExistCookieWhenInsertUser with Header cookies value:" + requestHeadCookie + " Exc: ", e);
            }       
    }

	public static void insertMarketingTracker(HttpServletRequest request, HttpServletResponse response, long contactId, long userId, long type)
			throws SQLException {
		
		Cookie marketingTracker = getMarketingTracker(request, MARKETING_TRACKING_STATIC_COOKIE);
		boolean cookieForChange = false;
		try {
			requestHeadCookie = request.getHeader("Cookie");
			deleteOldCookie(request, response);
			
			if (marketingTracker ==  null) {				
				defaultCookie = "DEF";
				createMarketingTrackerCookie(request, response);
				defaultCookie = "";
				if (log.isDebugEnabled()) {
					log.debug("InsertMarketingTracker add default cookie and set Atrribute");
				}
				marketingTracker = (Cookie) request.getAttribute(MARKETING_TRACKING_STATIC_COOKIE);
			} else if(marketingTracker.getValue().contains((DELETED_COOKIE_VALUES))) {
				marketingTracker = (Cookie) request.getAttribute(MARKETING_TRACKING_STATIC_COOKIE);
			}			
			
			String midByUserId = null;									
			if (type == ConstantsBase.MARKETING_TRACKING_LOGIN) {
				midByUserId = MarketingTrackingManager.getMidByUserId(userId);
				checkCookieAfterLogin(request, response, midByUserId, marketingTracker, contactId, userId ); 
				return;
			}
			
			if (log.isDebugEnabled()) {
				log.debug("InsertMarketingTracker try to getMarketingTrackerList with type: " + type);
			}
						
			ArrayList<MarketingTracking> list = getMarketingTrackerList(marketingTracker, request, response, contactId, userId, type, midByUserId);
			if (list.size() > 0 && type != ConstantsBase.MARKETING_TRACKING_LOGIN) {
				cookieForChange = afterFourtyFiveDays(marketingTracker, request, response );
				
				MarketingTrackingManager.insertMarketingTracking(list);
				if (cookieForChange) {					
					replaceMarketingTrackerCookieAfterActivity(request, response, list.get(0).getmId());
				}
				
			}
		} catch (Exception e) {
			log.error("Marketing Tracker insertMarketingTracker with Header cookies value: " + requestHeadCookie +" Exc: ", e);
		}
	}
}
