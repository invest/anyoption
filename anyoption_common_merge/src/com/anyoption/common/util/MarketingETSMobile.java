package com.anyoption.common.util;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.SkinsManagerBase;

public class MarketingETSMobile extends MarketingETS {

    private static final Logger log = Logger.getLogger(MarketingETSMobile.class);

    public static String insertMarketingETSClickFirstOpen(String etsMId, Long combinationId, String dynamicParam, long skinId) throws SQLException {
        String resultmId = null;
        boolean isGenerateMId = false;
        try {

            if (CommonUtil.isParameterEmptyOrNull(etsMId)) {
                Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
                String dateForDisplay = new SimpleDateFormat("ddMMyyyyHHmmss").format(date);
                etsMId = MarketingTrackerUtil.generateRandomUUId("JsonM");
                etsMId = dateForDisplay + "|" + etsMId;
                isGenerateMId = true;
            }

            if (combinationId == null) {
                combinationId = SkinsManagerBase.getCombinationIdtBySkins(skinId);
            }

            if (isGenerateMId) {
                resultmId = etsMId;
            }
            
            etsOrganicClikcServletCall(etsMId, combinationId.toString(), dynamicParam, "mobile", "mobile FO", "mobile FO", true);
        } catch (Exception e) {
            log.error("Marketing Tracker insertMarketingTrackerClickFirtsOpen with mId : " + etsMId + " and CombinationId : " + combinationId
                    + " and DynamicParam : " + dynamicParam + " Exc: ", e);
        }

        return resultmId;
    }

    public static void etsMarketingMobileActivity(String etsMid, long objId, long marketingActivity, String ip) throws SQLException {
        try {
            etsActivityServletCall(objId, marketingActivity, etsMid, ip);
        } catch (Exception e) {
            log.error("Marketing ETS when insert Mobile Activity Exc: ", e);
        }
    }    
}
