package com.anyoption.common.util;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.UrlsManagerBase;

/**
 * 
 * @author eranl
 * AR-1518 - AO + CO - Support FB continued flow (Web + Mobile)
 *
 */
public abstract class FBContinuedFlowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(FBContinuedFlowServlet.class);
	
	// marketing tracking params
	public static final String COMBINATION_ID = "combid";
	public static final String AFF_SUB1 = "aff_sub1";
	public static final String AFF_SUB2 = "aff_sub2";
	public static final String AFF_SUB3 = "aff_sub3";
	public static final String DYNAMIC_PARAM = "dp";
	public static final String GCLID = "gclid";
	//contact params
	public static final String FIRST_NAME = "first_name";
	public static final String LAST_NAME = "last_name";
	public static final String EMAIL = "email";
	// URLs
	public static final int ANYOPTION_URL = 2;
	public static final int COPYOP_URL = 16;

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
    	saveData(request, response);
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
    	saveData(request, response);
    }

	/**
	 * Abstract function to save contact
	 * @param email
	 * @param firstName
	 * @param lastName
	 * @param countryId
	 * @param skinId
	 * @param request
	 * @param response
	 * @param ip
	 * @param combId
	 * @param dynamicParam
	 * @param affSub1
	 * @param affSub2
	 * @param affSub3
	 * @param writerId
	 */
	abstract public void insertContact(String email, String firstName,
			String lastName, long countryId, long skinId,
			HttpServletRequest request, HttpServletResponse response,
			String ip, String combId, String dynamicParam, String affSub1,
			String affSub2, String affSub3, long writerId);
    
    /**
     * Save data from request 
     * @param request
     * @param response
     * @throws IOException
     */
    protected void saveData(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("UTF-8");
        String queryString = request.getQueryString();	    
	    log.debug("Requested: " + request.getRequestURL() + "?" + queryString);	    
	    HttpSession session = request.getSession();
		String currentDomain = request.getServerName().replaceAll(".*\\.(?=.*\\.)", "");
		long countryId = 0;
		int urlId = 0;
		long skinId = 0;
		String ip = ConstantsBase.EMPTY_STRING;
		String countryCode = ConstantsBase.EMPTY_STRING;
		String redirectURL =ConstantsBase.EMPTY_STRING;
		String userAgent = CommonUtil.getUserAgent(request);
		// contact params
		String params = ConstantsBase.EMPTY_STRING;
		String firstName = request.getParameter(FIRST_NAME);
		String lastName = request.getParameter(LAST_NAME);
		String email = request.getParameter(EMAIL);
		// marketing tracking params
		String combId = (String) session.getAttribute(COMBINATION_ID);
		String dynamicParam = (String) session.getAttribute(DYNAMIC_PARAM);
		String affSub1 = (String) session.getAttribute(AFF_SUB1);
		String affSub2 = (String) session.getAttribute(AFF_SUB2);
		String affSub3 = (String) session.getAttribute(AFF_SUB3);
		// handle visitor details
		ip = CommonUtil.getIPAddress(request);
		countryCode = CommonUtil.getCountryCodeByIp(ip);
		if (CommonUtil.isParameterEmptyOrNull(countryCode)) {
			countryCode = "GB"; // default country
		}
		try {
			urlId = UrlsManagerBase.getIdByUrl(currentDomain);
		} catch (SQLException e) {
			log.error("Cannot get urlId", e);	
		}
		if (urlId == 0) {
			urlId = COPYOP_URL; // default URL
		}
		try {
			countryId = CountryManagerBase.getIdByCode(countryCode);
		} catch (SQLException e) {
			log.error("Cannot get countryId", e);		
		}
		try {
			skinId = SkinsManagerBase.getSkinIdByCountryAndUrl(countryId, urlId);
			if (skinId == 0) {
				skinId = Skin.SKIN_REG_EN;		
			}
		} catch (SQLException e) {
			log.error("Cannot get skinId", e);		
		}
	
		boolean isMobile = CommonUtil.isMobile(request);
						
		params = 	"?firstName=" 			+ 	firstName 	+
				 	"&last_Name=" 			+	lastName	+
				 	"&email="				+	email		+
				 	"&isMobile="			+	isMobile	+
				 	"&funnel_countryId=" 	+	countryId;
		
		redirectURL = Utils.getPropertyByHost(request, "homepage.url");
		
		params +=	"&" 					+	queryString;
		
		if (urlId == ANYOPTION_URL) {
		params +=	"&s=" 					+ 	skinId 		+ 
				 	"&isFromFB=true";
			redirectURL += "/jsp/paramAfterLandingInternal.html";
		}
		if (urlId == COPYOP_URL) {
			long writerId = Writer.WRITER_ID_COPYOP_WEB;
			if (isMobile) {
				writerId = Writer.WRITER_ID_COPYOP_MOBILE;
			}
			Skin skin = SkinsManagerBase.getSkin(skinId);
			redirectURL += "/" + skin.getLocale() + "/signup" ;
			
			// handle dynamic parameter
	   		if ( null == dynamicParam ) {  // not in session
				dynamicParam = (String) request.getParameter(DYNAMIC_PARAM);
				if ( null != dynamicParam ) {
					session.setAttribute(DYNAMIC_PARAM, dynamicParam);
				}
			}
	   		
	   		if ( null == affSub1 ) {  // not in session
	   			affSub1 = (String) request.getParameter(AFF_SUB1);
				if ( null != affSub1 ) {
					session.setAttribute(AFF_SUB1, affSub1);
				}
			}
	   		if ( null == affSub2 ) {  // not in session
	   			affSub2 = (String) request.getParameter(AFF_SUB2);
				if ( null != affSub2 ) {
					session.setAttribute(AFF_SUB2, affSub2);
				}
			}
	   		if ( null == affSub3 ) {  // not in session
	   			affSub3 = (String) request.getParameter(GCLID);
				if ( null != affSub3 ) {
					session.setAttribute(AFF_SUB3, GCLID + "_" + affSub3);
				}
			}
	   		
			if (combId != null) {
				// combination id exist in session! dont do nothing...
				log.info("Combination id exist in session! going to redirect to: " + redirectURL + params);
				response.sendRedirect(redirectURL + params);
				return;
			}
	
			combId = request.getParameter(COMBINATION_ID);
			
			// check combination
			if (null == combId) {
				// default combination in case combination was not found 
				combId = String.valueOf(skin.getDefaultCombinationId());
			}
			
			session.setAttribute(COMBINATION_ID, combId);	
			
			insertContact(email, firstName, lastName, countryId, skinId,
					request, response, ip, combId, dynamicParam, affSub1,
					affSub2, affSub3, writerId);
		}
		
		String encodedUrl = response.encodeRedirectURL(redirectURL);
		log.info("Going to redirect to: " + encodedUrl + params);
		response.sendRedirect(encodedUrl + params);
    }
    
    
}
