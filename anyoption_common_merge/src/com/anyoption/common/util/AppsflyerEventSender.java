package com.anyoption.common.util;

import java.text.SimpleDateFormat;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.AppsflyerEvent;
import com.anyoption.common.beans.ServerPixel;
import com.anyoption.common.managers.ServerPixelsManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AppsflyerEventSender extends Thread {
	
    public static final Logger log = Logger.getLogger(AppsflyerEventSender.class);
    
    public String apiUrl = "http://api2.appsflyer.com/inappevent/";
    public final static int RESPONSE_STATUS_SUCCESS = 200;
    
    private long transactionId; 
    private int pixelType;    

    public AppsflyerEventSender(long transactionId, int pixelType) {
		super();
		this.transactionId = transactionId;
		this.pixelType = pixelType;
	}
    
	public void run() {
        try {
			AppsflyerEvent appsflyerEvent = TransactionsManagerBase.checkAppsflyerFirstDeposit(transactionId);
			if (null != appsflyerEvent) {
				if (sendEvent(appsflyerEvent)) {
					try {
						ServerPixelsManagerBase.insert(new ServerPixel(pixelType, transactionId, ServerPixel.SERVER_PIXELS_PUBLISHER_APPSFLYER));
					} catch (Exception e) {
						log.error("cant insert to pixels server", e);
					}
				}
			}
		} catch (Exception e) {
			log.error("cant send appsflyer event", e);
		}
    }
	
	
	private boolean sendEvent(AppsflyerEvent appsflyerEvent) {
		HttpClient httpclient = null;
        try {
        	httpclient = new DefaultHttpClient();
        	if (appsflyerEvent.getOsTypeId() == 2) { // iPhone
        		apiUrl += "id";
        	}
            HttpPost request = new HttpPost(apiUrl + appsflyerEvent.getAppId());
            request.addHeader("content-type", "application/json");
            request.addHeader("authentication", appsflyerEvent.getDevKey());
            Gson gson = new GsonBuilder().create();
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            
            String json = gson.toJson(new Event(appsflyerEvent.getAppsflyerId(), appsflyerEvent.getIp(), "deposit",
            		String.valueOf(appsflyerEvent.getAmount()), "USD", sdf.format(appsflyerEvent.getTimeCreated())));
            StringEntity params = new StringEntity(json);
            request.setEntity(params);
            
            log.debug("executing request " + request.getURI() + " json " + json + " dev key " +  appsflyerEvent.getDevKey());

            HttpResponse response = httpclient.execute(request);
            
            log.debug("--------------status--------------------------");
            log.debug(response.getStatusLine().toString());
            log.debug("----------------------------------------");
            
            if (response.getStatusLine().getStatusCode() == RESPONSE_STATUS_SUCCESS) {
            	return true;
            }
            log.debug("cant send appsflyer event response code: " + response.getStatusLine().getStatusCode());
            
		} catch (Exception e) {
			log.error("cant send appsflyer event", e);
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }
        return false;
    }
	
	private class Event {
		private String appsflyer_id;
		private String ip;
		private String eventName;
		private String eventValue;
		private String eventCurrency;
		private String eventTime;
		
		public Event(String appsflyer_id, String ip, String eventName,
				String eventValue, String eventCurrency, String eventTime) {
			super();
			this.appsflyer_id = appsflyer_id;
			this.ip = ip;
			this.eventName = eventName;
			this.eventValue = eventValue;
			this.eventCurrency = eventCurrency;
			this.eventTime = eventTime;
		}

		public String getAppsflyer_id() {
			return appsflyer_id;
		}

		public void setAppsflyer_id(String appsflyer_id) {
			this.appsflyer_id = appsflyer_id;
		}

		public String getIp() {
			return ip;
		}

		public void setIp(String ip) {
			this.ip = ip;
		}

		public String getEventName() {
			return eventName;
		}

		public void setEventName(String eventName) {
			this.eventName = eventName;
		}

		public String getEventValue() {
			return eventValue;
		}

		public void setEventValue(String eventValue) {
			this.eventValue = eventValue;
		}

		public String getEventCurrency() {
			return eventCurrency;
		}

		public void setEventCurrency(String eventCurrency) {
			this.eventCurrency = eventCurrency;
		}

		public String getEventTime() {
			return eventTime;
		}

		public void setEventTime(String eventTime) {
			this.eventTime = eventTime;
		}		
	}
    
}