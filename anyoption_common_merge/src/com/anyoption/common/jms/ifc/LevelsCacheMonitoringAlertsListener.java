package com.anyoption.common.jms.ifc;

/**
 * Interface to be implemented by classes interested in monitorin alerts.
 * 
 * @author Tony
 */
public interface LevelsCacheMonitoringAlertsListener {
    /**
     * Notify about monitoring alert (opportunity disable/enable).
     * 
     * @param marketId the id of the market which was disabled/enabled
     * @param disabled the new state of the opportunity <code>true</code> for
     *      disabled and <code>false</code> for enabled
     * @param description text description of the reason for the disable/enable
     */
    public void alert(long marketId, boolean disabled, String description);
}