package com.anyoption.common.jms;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.enums.SkinGroup;

/**
 * Helper bean for WEB apps level lookup.
 *
 * @author Tony
 */
public class WebLevelLookupBean {
    protected long oppId;
    protected String feedName;
    protected double devCheckLevel;
    protected double realLevel;
    protected boolean isAutoDisable;
    protected double bid;
    protected double offer;
    protected String investmentsRejectInfo;
    protected SkinGroup skinGroup;
    protected int groupClose;

    public WebLevelLookupBean() {
    	bid = -1;
    	offer = -1;
    }

    public double getDevCheckLevel() {
        return devCheckLevel;
    }

    public void setDevCheckLevel(double devCheckLevel) {
        this.devCheckLevel = devCheckLevel;
    }

    public String getFeedName() {
        return feedName;
    }

    public void setFeedName(String feedName) {
        this.feedName = feedName;
    }

    public long getOppId() {
        return oppId;
    }

    public void setOppId(long oppId) {
        this.oppId = oppId;
    }

    public double getRealLevel() {
        return realLevel;
    }

    public void setRealLevel(double realLevel) {
        this.realLevel = realLevel;
    }

	public boolean isAutoDisable() {
		return isAutoDisable;
	}

	public void setAutoDisable(boolean isAutoDisable) {
		this.isAutoDisable = isAutoDisable;
	}

	public double getBid() {
		return bid;
	}

	public void setBid(double bid) {
		this.bid = bid;
	}

	public double getOffer() {
		return offer;
	}

	public void setOffer(double offer) {
		this.offer = offer;
	}

	public String getInvestmentsRejectInfo() {
		return investmentsRejectInfo;
	}

	public void setInvestmentsRejectInfo(String investmentsRejectInfo) {
		this.investmentsRejectInfo = investmentsRejectInfo;
	}

	/**
	 * @return the skinGroup
	 */
	public SkinGroup getSkinGroup() {
		return skinGroup;
	}

	/**
	 * @param skinGroup the skinGroup to set
	 */
	public void setSkinGroup(SkinGroup skinGroup) {
		this.skinGroup = skinGroup;
	}

	/**
	 * @return the groupClose
	 */
	public int getGroupClose() {
		return groupClose;
	}

	/**
	 * @param groupClose the groupClose to set
	 */
	public void setGroupClose(int groupClose) {
		this.groupClose = groupClose;
	}

	public boolean isClosingFlagClosest() {
		return (groupClose & Opportunity.CLOSING_FILTER_FLAG_CLOSEST) != 0;
	}

	public boolean isClosingFlagMonth() {
		return (groupClose & Opportunity.CLOSING_FILTER_FLAG_MONTH) != 0;
	}

	public boolean isClosingFlagToday() {
		return (groupClose & Opportunity.CLOSING_FILTER_FLAG_TODAY) != 0;
	}

	public boolean isClosingFlagWeek() {
		return (groupClose & Opportunity.CLOSING_FILTER_FLAG_WEEK) != 0;
	}

	@Override
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "WebLevelLookupBean:" + ls +
            "oppId: " + oppId + ls +
            "feedName: " + feedName + ls +
            "devCheckLevel: " + devCheckLevel + ls +
            "realLevel: " + realLevel + ls +
            "isAutoDisable: " + isAutoDisable + ls +
            "bid: " + bid + ls +
            "offer: " + offer + ls +
        	"investmentsRejectInfo: " + investmentsRejectInfo + ls +
        	"skinGroup: " + skinGroup + ls +
        	"groupClose: " + groupClose + ls;
    }
}