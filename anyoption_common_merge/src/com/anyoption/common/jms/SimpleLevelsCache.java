package com.anyoption.common.jms;

import java.util.Calendar;
import java.util.EnumMap;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.LiveJMSMessage;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.ifc.ExtendedMessageListener;
import com.anyoption.common.jms.ifc.SimpleLevelsCacheListener;
import com.anyoption.common.jms.msgs.SettledOpportunitiesMessage;

public class SimpleLevelsCache implements ExtendedMessageListener {
    private static final Logger log = Logger.getLogger(SimpleLevelsCache.class);

    // a read/write lock is used
    protected ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock(false);

    // this object handles comunications with JMS. Hides the use of Session, Connections, Publishers etc..
    protected JMSHandler jmsHandler;
    protected boolean subscribeToTopic;

    protected ConcurrentHashMap<String, SubscribedItemAttributes> opportunities = new ConcurrentHashMap<String, SubscribedItemAttributes>();
    protected Map<String, String> liveGlobeLastUpdate;
    protected Map<String, Map<String, Map<String, String>>> liveInvestmentsUpdates = new HashMap<String, Map<String, Map<String, String>>>();
    protected Map<String, Map<String, String>> liveTrendsLastUpdate = new HashMap<String, Map<String,String>>();
    protected int msgPoolSize;
    protected int recoveryPause;
    protected ConnectionLoop connector;
    protected HeartbeatThread heartbeat;
    protected SenderThread sender;

    protected long lastHeartbeatLifeId = -1;
    // a simple counter to know if any updates/heratbeats were received since the last check
    protected int heartbeatCount = 0;

    // we dont want more then 1 snapshot per min so we will save here the snapshot time
    protected Calendar lastSnapShotRequest;
    // if we got more then 1 snapshot request in a min we will send another snapshot after 1 min
    protected boolean needAnotherSnapshot = false;

    protected SimpleLevelsCacheListener simpleListener;

    /**
     * Construct a <code>LevelsCahce</code> that can be used to "control" the service as well as
     * listen for levels (current/real) updates.
     *
     * @param initialContextFactory
     * @param providerURL
     * @param connectionFactoryName
     * @param queueName
     * @param topicName
     * @param msgPoolSize
     * @param recoveryPause
     * @param listener
     */
    public SimpleLevelsCache(String initialContextFactory, String providerURL, String connectionFactoryName, String queueName, String topicName, SimpleLevelsCacheListener simpleListener) {
        this.simpleListener = simpleListener;
        this.msgPoolSize = 15;
        this.recoveryPause = 2000;

        sender = new SenderThread();
        sender.start();

        // create the JMS handler. The object will handle the instantiation of JMS-related objects
        jmsHandler = new JMSHandler("SimpleLevelsCache", initialContextFactory, providerURL, connectionFactoryName, queueName, connectionFactoryName, topicName);
        // the message listener that will receive JMS messages will be the StockQuotesJMSAdapter instance (this)
        jmsHandler.setListener(this);

        if (null != topicName) {
            subscribeToTopic = true;
            connector = new ConnectionLoopTSQS(jmsHandler, recoveryPause);
        } else {
            subscribeToTopic = false;
            connector = new ConnectionLoopQS(jmsHandler, recoveryPause);
        }
        connector.start();
        log.info("SimpleLevelsCache ready.");
    }

    /**
     * called whenever the connection to JMS is lost
     */
     @Override
	public void onException(JMSException je) {
        log.error("onException: JMSException -> " + je.getMessage());

        // get the write lock in order to call the onFeedDisconnection method
        rwLock.writeLock().lock();
        if (log.isDebugEnabled()) {
            log.debug("------------------>Write LOCK onException");
        }

        if (log.isInfoEnabled()) {
            log.info("JMS is now down");
        }
        // when the JMS connection is lost, obviously also the connection with the level service is
        onFeedDisconnection();

        if (log.isDebugEnabled()) {
            log.debug("------------------>Write UNLOCK onException");
        }
        // release the lock
        rwLock.writeLock().unlock();

        //start loop to try to reconnect
        jmsHandler.reset();
        if (subscribeToTopic) {
            connector = new ConnectionLoopTSQS(jmsHandler, recoveryPause);
        } else {
            connector = new ConnectionLoopQS(jmsHandler, recoveryPause);
        }
        connector.start();
    }

    /**
     * called in case the feed is lost (i.e. levels service is down or JMS connection is down)
     * As this method is always called by a method that already owns the write lock, we don't get any lock here,
     * (it would be better if there was a test here that gets a lock if the running thread doesn't own one).
     */
    public void onFeedDisconnection() {
        if (log.isInfoEnabled()) {
            log.info("Feed no more available");
        }
        // set lastHeartbeatRandom to -1, ie we are no more connected with the levels service
        lastHeartbeatLifeId = -1;
        clearInternalState();
    }

    /**
     * Clear internal state. Make sure you have write lock when calling this method.
     */
    protected void clearInternalState() {
        if (null != simpleListener) {
            try {
                Enumeration<SubscribedItemAttributes> subItems = opportunities.elements();
                while(subItems.hasMoreElements()) {
                    SubscribedItemAttributes sia = subItems.nextElement();
                    sia.crrState.put("command", "DELETE");

                    if (null != sia.home) {
                        Integer key = null;
                        for (Iterator<Integer> i = sia.home.keySet().iterator(); i.hasNext();) {
                            key = i.next();
                            if (sia.home.get(key)) {
                                sendHomeUpdate(new HashMap<String, String>(sia.crrState), sia.skinsPriorities, key);
                                sendAOCtrlUpdate(new HashMap<String, String>(sia.crrState), sia.skinsPriorities, key);
                            }
                        }
                    }
                    if (null != sia.skinsPriorities) {
                        sendGroupUpdate(new HashMap<String, String>(sia.crrState), sia.skinsPriorities, sia.groupId);
                    }
                    long oppType = sia.opportunityTypeId;
                    if (oppType == Opportunity.TYPE_REGULAR) {
                    	sendAOTPSUpdate(sia.marketId, new HashMap<String, String>(sia.crrState));
                    	sendInsurancesUpdate(Long.parseLong(sia.oppId.startsWith("opp") ? sia.oppId.substring(3) : sia.oppId), new HashMap<String, String>(sia.crrState), sia.level);
                        sendAOStateUpdate(sia.marketId, new HashMap<String, String>(sia.crrState));
                        simpleListener.ttUpdate(Long.parseLong(sia.oppId.startsWith("opp") ? sia.oppId.substring(3) : sia.oppId),
	                        						sia.autoShiftParameter,
	                        						sia.calls,
	                        						sia.puts,
	                        						sia.feedName,
	                        						sia.decimalPoint,
	                        						sia.isTraderDisable,
	                        						sia.isAutoDisable,
	                        						new HashMap<String, String>(sia.crrState),
	                        						sia.notClosed,
	                        						sia.skinGroupMapping);
                    } else if (oppType == Opportunity.TYPE_OPTION_PLUS) {
                        simpleListener.optionPlusUpdate(sia.marketId, new HashMap<String, String>(sia.crrState));
                        simpleListener.ttUpdate(Long.parseLong(sia.oppId.startsWith("opp") ? sia.oppId.substring(3) : sia.oppId),
					                        		sia.autoShiftParameter,
					                        		sia.calls,
					                        		sia.puts,
					                        		sia.feedName,
					                        		sia.decimalPoint,
					                        		sia.isTraderDisable,
					        						sia.isAutoDisable,
					        						new HashMap<String, String>(sia.crrState),
					        						sia.notClosed,
					        						sia.skinGroupMapping);
                    } else if (oppType == Opportunity.TYPE_BINARY_0_100_ABOVE || oppType == Opportunity.TYPE_BINARY_0_100_BELOW || oppType == Opportunity.TYPE_DYNAMICS) {
                    	simpleListener.binaryZeroOneHundredUpdate(sia.marketId, sia.opportunityTypeId, new HashMap<String, String>(sia.crrState));
                    	simpleListener.ttBinary0100Update(Long.parseLong(sia.oppId.startsWith("opp") ? sia.oppId.substring(3) : sia.oppId),
                    										sia.autoShiftParameter,
                    										sia.calls,
                    										sia.puts,
                    										sia.feedName,
                    										sia.decimalPoint,
                    										sia.isTraderDisable,
                    										sia.isAutoDisable,
                    										new HashMap<String, String>(sia.crrState),
                    										sia.contractsBought,
                    										sia.contractsSold,
                    										sia.parameters,
                    										sia.level,
                    										sia.notClosed,
                    										sia.skinGroupMapping);
                    } else {
                    	log.error("clearInternalState() encounrterd unknown type id [" + oppType + "] in opportunity :" + sia);
                    }
                }
            } catch (Exception e) {
                log.error("Error in clearInternalState.", e);
            }
            try {
//            	liveGlobeLastUpdate.put("command", "DELETE");
//            	simpleListener.liveGlobeUpdate(LiveJMSMessage.KEY_GLOBE, liveGlobeLastUpdate);
            	liveGlobeLastUpdate = null;

//            	liveTrendsLastUpdate.put("command", "DELETE");
//            	simpleListener.liveTrendsUpdate(LiveJMSMessage.KEY_TRENDS, liveTrendsLastUpdate);

            	Iterator<String> trendsTableKeys = liveTrendsLastUpdate.keySet().iterator();
            	while (trendsTableKeys.hasNext()) {
            		String trendsTableKey = trendsTableKeys.next();
            		Map<String, String> trends = liveTrendsLastUpdate.get(trendsTableKey);
            		trends.put("command", "DELETE");
            		simpleListener.liveTrendsUpdate(trendsTableKey, trends);
            	}
            	liveTrendsLastUpdate.clear();

            	Iterator<String> tablesKeys = liveInvestmentsUpdates.keySet().iterator();
            	while (tablesKeys.hasNext()) {
            		String tableKey = tablesKeys.next();
            		Map<String, Map<String, String>> table = liveInvestmentsUpdates.get(tableKey);
            		Iterator<Map<String, String>> invs = table.values().iterator();
            		while (invs.hasNext()) {
            			Map<String, String> inv = invs.next();
            			inv.put("command", "DELETE");
            			simpleListener.liveInvestmentsUpdate(tableKey, inv);
            		}
            	}
            	liveInvestmentsUpdates.clear();
            } catch (Exception e) {
            	log.error("Can't clear Live", e);
            }
        }
        opportunities.clear();
    }

    /**
     * @return <code>true</code> if the communication with the service working else <code>false</code>.
     *      This check depends on the heartbeat logic. The idea of the heartbeats is the clients (LevelsCache)
     *      to know that service is available and the channel from it to the client is ok (in case no updates
     *      are present at the moment).
     */
    public boolean isJMSConnectionOK() {
        return lastHeartbeatLifeId != -1;
    }

    /**
     * @return The size of the sender thread internal queue.
     */
    public int getSenderQueueSize() {
        return sender.getQueueSize();
    }

    /**
     * receive messages from JMS
     */
    @Override
	public void onMessage(Message message) {
        if (null == message) {
            log.warn("No message");
            return;
        }
        log.debug("Received message");
        //we have to extract data from the Message object
        ETraderFeedMessage feedMsg = null;
        try {
            ObjectMessage objectMessage = (ObjectMessage) message;
            Object obj = objectMessage.getObject();
            if (obj instanceof HeartbeatMessage) {
                HeartbeatMessage beat = (HeartbeatMessage) obj;
                handleHeartbeat(beat.lifeId);
                return;
            } else if (obj instanceof ETraderFeedMessage) {
                feedMsg = (ETraderFeedMessage) obj;
                if (!handleHeartbeat(feedMsg.lifeId)) {
                    return;
                }
            } else if (obj instanceof LiveJMSMessage) {
            	handleLiveMessage((LiveJMSMessage) obj);
            	return;
            } else if (obj instanceof SettledOpportunitiesMessage) {
            	handleSettledOpportunitiesMessage((SettledOpportunitiesMessage) obj);
            	return;
            } else if (obj instanceof String) {
            	String command = (String) obj;
            	if(command.startsWith("startGM_")){
            		simpleListener.notifyInsurancesStart(Long.parseLong(command.substring(8)));
            		return;
            	}
            } else {
                log.trace("Not processing message: " + obj);
                return;
            }
        } catch (ClassCastException cce) {
            log.warn("Invalid message.", cce);
            return;
        } catch (JMSException jmse) {
            log.error("Invalid message.", jmse);
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("Feed message.");
        }
        try {
            if (log.isTraceEnabled()) {
                log.trace("Message: " + feedMsg.toString());
            }

            if (null == feedMsg.currentValues) { // real level update
                return;
            }
        } catch (Exception e) {
            log.error("Error processing message.", e);
        }
        handleFeedMsg(feedMsg);
    }

	protected void handleSettledOpportunitiesMessage(SettledOpportunitiesMessage msg) {
    	if (simpleListener != null) {
    		try {
    			simpleListener.settledOpportunitiesUpdate(msg);
    		} catch(Exception e) {
    			log.error("Unable to handle settled opportunities message", e);
    		}
    	} else {
    		log.trace("No listener to handle settled opportunities message [" + msg + "]");
    	}
    }

    protected void handleFeedMsg(ETraderFeedMessage feedMsg) {
        try {
            rwLock.writeLock().lock();
            log.debug("------------------>Write LOCK onMessage");

            String oppId = feedMsg.currentValues.get("key");
            String type = feedMsg.currentValues.get("AO_TYPE");
            SubscribedItemAttributes item = opportunities.get(oppId);
            if (null == item) { // this is the first update about this opp
                if (log.isDebugEnabled()) {
                    log.debug("Add new opp");
                }
                if (!feedMsg.currentValues.get("command").equals("DELETE")) { // UPDATE left - the service send only UPDATE and DELETE
                    item = new SubscribedItemAttributes(
                            oppId,
                            feedMsg.home,
                            feedMsg.currentValues,
                            feedMsg.level,
                            feedMsg.groupId,
                            feedMsg.realLevel,
                            feedMsg.devCheckLevel,
                            feedMsg.skinsPriorities,
                            feedMsg.autoShiftParameter,
                            feedMsg.calls,
                            feedMsg.puts,
                            feedMsg.feedName,
                            feedMsg.decimalPoint,
                            feedMsg.isTraderDisable,
                            feedMsg.isAutoDisable,
                            feedMsg.contractsBought,
                            feedMsg.contractsSold,
                            feedMsg.parameters,
                            feedMsg.notClosed,
                            feedMsg.skinGroupMapping);
                    opportunities.put(oppId, item);
                    if (null != simpleListener) {
                        HashMap<String, String> tmp = new HashMap<String, String>(item.crrState);
                        // in our case this should be threated as "ADD" command
                        tmp.put("command", "ADD");
                        if (null == type || type.equals(String.valueOf(Opportunity.TYPE_REGULAR))) {
                            //loop over all feedMsg.home[] if true send the update and take the property from the skinsPriorities
                            Integer key = null;
                            for (Iterator<Integer> i = feedMsg.home.keySet().iterator(); i.hasNext();) {
                                key = i.next();
                                if (feedMsg.home.get(key)) {
                                    sendHomeUpdate(tmp, feedMsg.skinsPriorities , key);
                                    sendAOCtrlUpdate(tmp, feedMsg.skinsPriorities, key);
                                }
                            }
                            sendGroupUpdate(tmp, feedMsg.skinsPriorities, item.groupId);

                            sendAOTPSUpdate(item.marketId, new HashMap<String, String>(tmp));
                            sendInsurancesUpdate(feedMsg.oppId, tmp, feedMsg.level);
                            sendAOStateUpdate(item.marketId, item.crrState);
                            simpleListener.ttUpdate(feedMsg.oppId,
                            						feedMsg.autoShiftParameter,
                            						feedMsg.calls,
                            						feedMsg.puts,
                            						feedMsg.feedName,
                            						feedMsg.decimalPoint,
                            						feedMsg.isTraderDisable,
                            						feedMsg.isAutoDisable,
                            						tmp,
                            						feedMsg.notClosed,
                            						feedMsg.skinGroupMapping);
                        } else if (type.equals(String.valueOf(Opportunity.TYPE_OPTION_PLUS))) {
                            simpleListener.optionPlusUpdate(item.marketId, tmp);
                            simpleListener.ttUpdate(feedMsg.oppId,
                            						feedMsg.autoShiftParameter,
                            						feedMsg.calls,
                            						feedMsg.puts,
                            						feedMsg.feedName,
                            						feedMsg.decimalPoint,
                            						feedMsg.isTraderDisable,
                            						feedMsg.isAutoDisable,
                            						tmp,
                            						feedMsg.notClosed,
                            						feedMsg.skinGroupMapping);
                        } else if (type.equals(String.valueOf(Opportunity.TYPE_BUBBLES))) {
                        	// Do nothing
                        } else { // Binary 0-100 and Dynamics should fall here
                        	simpleListener.binaryZeroOneHundredUpdate(item.marketId, item.opportunityTypeId, tmp);
                        	simpleListener.ttBinary0100Update(feedMsg.oppId,
                        										feedMsg.autoShiftParameter,
                        										feedMsg.calls,
                        										feedMsg.puts,
                        										feedMsg.feedName,
                        										feedMsg.decimalPoint,
                        										feedMsg.isTraderDisable,
                        										feedMsg.isAutoDisable,
                        										tmp,
                        										feedMsg.contractsBought,
                        										feedMsg.contractsSold,
                        										feedMsg.parameters,
                        										feedMsg.level,
                        										feedMsg.notClosed,
                        										feedMsg.skinGroupMapping);
                        }
                    }
                } else {
                    log.info("DELETE command for opp we don't have - do nothing. " + feedMsg);
                }
            } else {
                if (null != simpleListener) {
                    if (null == type || type.equals(String.valueOf(Opportunity.TYPE_REGULAR))) {
                        Integer key = null;
                        for (Iterator<Integer> i = feedMsg.home.keySet().iterator(); i.hasNext();) {
                            key = i.next();
                            if ((item.home.containsKey(key) ? item.home.get(key) : false) && (!feedMsg.home.get(key))) {
                                // was home but no longer. so just for that update make "DELETE" for "home"
                                HashMap<String, String> tmp = new HashMap<String, String>(item.crrState);
                                tmp.put("command", "DELETE");
                                sendHomeUpdate(tmp, feedMsg.skinsPriorities , key);
                                sendAOCtrlUpdate(tmp, feedMsg.skinsPriorities, key);
                            }
                            if (!(item.home.containsKey(key) ? item.home.get(key) : false) && (feedMsg.home.get(key))) {
                                // was NOT on home but now it is. so just for that update make "ADD" for the ctrl subscription
                                HashMap<String, String> tmp = new HashMap<String, String>(feedMsg.currentValues);
                                tmp.put("command", "ADD");
                                sendAOCtrlUpdate(tmp, feedMsg.skinsPriorities, key);
                            }
                            if (feedMsg.home.get(key)) {
                                sendHomeUpdate(feedMsg.currentValues, feedMsg.skinsPriorities, key);
                                if (feedMsg.currentValues.get("command").equals("DELETE")) {
                                    sendAOCtrlUpdate(feedMsg.currentValues, feedMsg.skinsPriorities, key);
                                }
                            }
                        }
                        sendGroupUpdate(feedMsg.currentValues, feedMsg.skinsPriorities, item.groupId);
                        sendInsurancesUpdate(feedMsg.oppId, feedMsg.currentValues, feedMsg.level);
                        sendAOTPSUpdate(item.marketId, new HashMap<String, String>(feedMsg.currentValues));
                        if (feedMsg.currentValues.get("command").equals("DELETE")) {
                            sendAOStateUpdate(item.marketId, feedMsg.currentValues);
                        }
                        checkForAOOpenedClosedTransition(item.marketId, item.crrState, feedMsg.currentValues);
                        simpleListener.ttUpdate(feedMsg.oppId,
                        						feedMsg.autoShiftParameter,
                        						feedMsg.calls,
                        						feedMsg.puts,
                        						feedMsg.feedName,
                        						feedMsg.decimalPoint,
                        						feedMsg.isTraderDisable,
                        						feedMsg.isAutoDisable,
                        						new HashMap<String, String>(feedMsg.currentValues),
                        						feedMsg.notClosed,
                        						feedMsg.skinGroupMapping);
                    } else if (type.equals(String.valueOf(Opportunity.TYPE_OPTION_PLUS))) {
                        simpleListener.optionPlusUpdate(item.marketId, new HashMap<String, String>(feedMsg.currentValues));
                        simpleListener.ttUpdate(feedMsg.oppId,
                    							feedMsg.autoShiftParameter,
                    							feedMsg.calls,
                    							feedMsg.puts,
                    							feedMsg.feedName,
                    							feedMsg.decimalPoint,
                    							feedMsg.isTraderDisable,
                    							feedMsg.isAutoDisable,
                    							new HashMap<String, String>(feedMsg.currentValues),
                    							feedMsg.notClosed,
                    							feedMsg.skinGroupMapping);
                    } else if (type.equals(String.valueOf(Opportunity.TYPE_BUBBLES))) {
                    	// Do nothing
                    } else {
                    	simpleListener.binaryZeroOneHundredUpdate(item.marketId, item.opportunityTypeId, new HashMap<String, String>(feedMsg.currentValues));
                    	simpleListener.ttBinary0100Update(feedMsg.oppId,
                    										feedMsg.autoShiftParameter,
                    										feedMsg.calls,
                    										feedMsg.puts,
                    										feedMsg.feedName,
                    										feedMsg.decimalPoint,
                    										feedMsg.isTraderDisable,
                    										feedMsg.isAutoDisable,
                    										new HashMap<String, String>(feedMsg.currentValues),
                    										feedMsg.contractsBought,
                    										feedMsg.contractsSold,
                    										feedMsg.parameters,
                    										feedMsg.level,
                    										feedMsg.notClosed,
                    										feedMsg.skinGroupMapping);
                    }
                }
                if (!feedMsg.currentValues.get("command").equals("DELETE")) { // UPDATE left - the service send only UPDATE and DELETE
                    item.crrState = feedMsg.currentValues;
                    item.home = feedMsg.home;
                    item.level = feedMsg.level;
                    item.realLevel = feedMsg.realLevel;
                    item.devCheckLevel = feedMsg.devCheckLevel;
                    item.skinsPriorities = feedMsg.skinsPriorities;
                    item.autoShiftParameter = feedMsg.autoShiftParameter;
                    item.calls = feedMsg.calls;
                    item.puts = feedMsg.puts;
                    item.feedName = feedMsg.feedName;
                    item.decimalPoint = feedMsg.decimalPoint;
                    item.contractsBought = feedMsg.contractsBought;
                    item.contractsSold = feedMsg.contractsSold;
                    item.parameters = feedMsg.parameters;
                    item.skinGroupMapping = feedMsg.skinGroupMapping;
                } else {
                    opportunities.remove(oppId);
                    if (log.isDebugEnabled()) {
                        log.debug("Removed opp");
                    }
                }
            }
        } catch (Throwable t) {
            log.error("Error processing message.", t);
        } finally {
            //release the lock
            log.debug("------------------>Write UNLOCK onMessage");
            rwLock.writeLock().unlock();
        }
    }

    /*
     * called on each message received from JMS
     */
    private boolean handleHeartbeat(long beat) {
        rwLock.writeLock().lock();
        log.debug("------------------>Write LOCK handleHeartbet");
        if (lastHeartbeatLifeId == beat) {
            //the heartbeat is correct, we increase a counter
            heartbeatCount = (heartbeatCount + 1) % 1000;
            log.debug("Received heartbeat: " + beat);
            //release the lock
            log.debug("------------------>Write UNLOCK handleHeartbet 1");
            rwLock.writeLock().unlock();
            return true;
        } else {
            //this is the first heartbeat received from this service life (or the first one
            //after a service connectivity problem)
            log.info("Received NEW heartbeat: " + beat + ", feed is now available" );
            //sets the new Heartbeat ID
            lastHeartbeatLifeId = beat;
            //reset the heartbeat counter
            heartbeatCount = 0;
            //release the lock
            log.debug("------------------>Write UNLOCK handleHeartbet 2");
            rwLock.writeLock().unlock();

            getSnapshot();

            // start the new HeartbeatThread that will control that at least an heartbeat
            // was received in the last 2 seconds.
            heartbeat = new HeartbeatThread(beat);
            heartbeat.start();
            return false;
        }
    }

    private void handleLiveMessage(LiveJMSMessage msg) {
    	if (null != simpleListener) {
            rwLock.writeLock().lock();
            log.debug("------------------>Write LOCK handleLiveMessage");
            try {
            	log.trace("Live message to handle [" + msg + "]");
	            String key = msg.fields.get("key");
		    	if (key.equals(LiveJMSMessage.KEY_GLOBE)) {
		    		liveGlobeLastUpdate = msg.fields;
		    		Map<String, String> tmp = new HashMap<String, String>();
		    		tmp.putAll(liveGlobeLastUpdate);
		    		simpleListener.liveGlobeUpdate(key, tmp);
		    	} else if (key.startsWith(LiveJMSMessage.KEY_TRENDS)) {
		    		Map<String, String> table = liveTrendsLastUpdate.get(key);
		    		if (null == table) {
		    			table = new HashMap<String, String>();
		    			liveTrendsLastUpdate.put(key, table);
		    		}
		    		table.putAll(msg.fields);
		    		Map<String, String> tmp = new HashMap<String, String>();
		    		tmp.putAll(msg.fields);
		    		simpleListener.liveTrendsUpdate(key, tmp);
		    	} else {
		    		Map<String, Map<String, String>> table = liveInvestmentsUpdates.get(key);
		    		if (null == table) {
		    			table = new HashMap<String, Map<String, String>>();
		    			liveInvestmentsUpdates.put(key, table);
		    		}
		    		String invId = msg.fields.get("LI_INV_KEY");
		    		msg.fields.put("key", invId);
		    		table.put(invId, msg.fields);
		    		Map<String, String> tmp = new HashMap<String, String>();
		    		tmp.putAll(msg.fields);
		    		simpleListener.liveInvestmentsUpdate(key, tmp);
		    	}
            } finally {
                log.debug("------------------>Write UNLOCK handleLiveMessage 2");
                rwLock.writeLock().unlock();
            }
    	}
    }

    public void getSnapshot() {
        Calendar now = Calendar.getInstance();
        if (lastSnapShotRequest != null) {
            Calendar temp = lastSnapShotRequest;
            temp.add(Calendar.MINUTE, +1);
            if (temp.after(now)) {
                if (!needAnotherSnapshot) {
                    needAnotherSnapshot = true;
                    long timeNow = now.getTimeInMillis();
                    long lastTime = lastSnapShotRequest.getTimeInMillis();
                    long gapTime = timeNow - lastTime;
                    new SendSnapshotAfter1MinThread((60 - gapTime)).start();
                }
                return;
            }
        }
        lastSnapShotRequest = now;
        // make sure the cache is clean as we will get a fresh snapshot
        rwLock.writeLock().lock();
        log.debug("------------------>Write LOCK getSnapshot");

        clearInternalState();

        log.debug("------------------>Write UNLOCK getSnapshot");
        rwLock.writeLock().unlock();

        sender.send("snapshot");
    }

    /**
     * Send snapshot to the registered listener. The snapshot is taken from the
     * opps cache.
     *
     * @param home <code>ture</code> if to send home page snapshot else <code>false</code>.
     *      If <code>false</code> then snapshot for group specified in the groupId is sent.
     * @param groupId the if of the group to send snapshot for (if home is <code>false</code>).
     */
    public void sendSnapshot(boolean home, long groupId, int skinId) {
        if (log.isDebugEnabled()) {
            log.debug("Sending snapshot - home: " + home + " groupId: " + groupId + " listener: " + simpleListener);
        }
        if (null != simpleListener) {
            new SnapshotThread(home, groupId, skinId).start();
        } else {
            log.warn("No listener to send snapshot to!");
        }
    }

    /**
     * Send snapshot to the reigstered listener. The snapshot is taken from the
     * opps cache.
     *
     * @param marketId the id of the market for which snapshot (update) to be sent
     * @param scheduled the schedule for which snapshot (update) to be sent
     */
    public void sendSnapshot(long marketId, int scheduled) {
        if (log.isDebugEnabled()) {
            log.debug("Sending snapshot - marketId: " + marketId + " scheduled: " + scheduled + " listener: " + simpleListener);
        }
        if (null != simpleListener) {
            new SnapshotThread(marketId, scheduled).start();
        } else {
            log.warn("No listener to send snapshot to!");
        }
    }

    /**
     * Send snapshot to the reigstered listener. The snapshot is taken from the
     * opps cache.
     *
     * @param skinId the id of the skin for which ctrl conn snapshot (update) to be sent
     */
    public void sendSnapshot(int skinId) {
        if (log.isDebugEnabled()) {
            log.debug("Sending snapshot - skinId: " + skinId + " listener: " + simpleListener);
        }
        if (null != simpleListener) {
            new SnapshotThread(skinId).start();
        } else {
            log.warn("No listener to send snapshot to!");
        }
    }

    public void sendAOStateSnapshot() {
        if (log.isDebugEnabled()) {
            log.debug("Sending aoctrl snapshot");
        }
        if (null != simpleListener) {
            new SnapshotThread().start();
        } else {
            log.warn("No listener to send snapshot to!");
        }
    }

    public void sendOptionPlusSnapshot() {
        log.debug("Sending Option+ snapshot");
        if (null != simpleListener) {
            new SnapshotThread(true).start();
        } else {
            log.warn("No listener to send snapshot to!");
        }
    }

    public void sendBinaryZeroOneHundredSnapshot() {
        log.debug("Sending binary zero one hundred snapshot");
        if (null != simpleListener) {
            new SnapshotThread(false).start();
        } else {
            log.warn("No listener to send snapshot to!");
        }
    }

    public void sendTTSnapshot(int type) {
        log.debug("Sending TT snapshot for type = " + type);
        if (null != simpleListener) {
            SnapshotThread st = new SnapshotThread();
            st.setType(type);
            st.start();
        } else {
            log.warn("No listener to send snapshot to!");
        }
    }

    public void sendLiveGlobeSnapshot() {
        log.debug("Sending Live Globe snapshot");
        new SnapshotThread(SnapshotThread.TYPE_LIVE_GLOBE, null).start();
    }

    public void sendLiveInvestmentsSnapshot(String table) {
        log.debug("Sending Live Investments snapshot for " + table);
        new SnapshotThread(SnapshotThread.TYPE_LIVE_INVESTMENTS, new Object[] {table}).start();
    }

    public void sendLiveTrendsSnapshot(String table) {
        log.debug("Sending Live Trends snapshot for " + table);
        new SnapshotThread(SnapshotThread.TYPE_LIVE_TRENDS, new Object[] {table}).start();
    }

    /**
     * Get the current level of an opp.
     *
     * @param oppId the opp id
     * @return The current level of the opp or 0 if the opp is not found.
     */
    public double getCurrentLevel(long oppId) {
        double level = 0;
        //get the read lock in order to take the current opps
        rwLock.readLock().lock();
        log.debug("------------------>Read LOCK getCurrentLevel");
        SubscribedItemAttributes sia = opportunities.get("opp" + oppId);
        if (null != sia) {
            level = sia.level;
        }
        log.debug("------------------>Read UNLOCK getCurrentLevel");
        //release the lock
        rwLock.readLock().unlock();
        return level;
    }

    /**
     * Get the current levels of an opp formatted.
     *
     * @param oppId the opp id
     * @return A map of current levels for all skin groups. The levels are formatted. If a level is not found its value will be set to 0.
     */
    public Map<SkinGroup, String> getCurrentLevels(long oppId) {
        Map<SkinGroup, String> currentLevels = null;
        //get the read lock in order to take the current opps
        rwLock.readLock().lock();
        log.debug("------------------>Read LOCK getCurrentLevelAO");
        SubscribedItemAttributes sia = opportunities.get("opp" + oppId);
        if (null != sia) {
        	currentLevels = new EnumMap<SkinGroup, String>(SkinGroup.class);
        	String currentLevel = null;
        	for (SkinGroup skinGroup : SkinGroup.NON_AUTO_SET) {
        		currentLevel = sia.crrState.get(skinGroup.getLevelUpdateKey());
				currentLevels.put(skinGroup, currentLevel != null ? currentLevel : "0");
			}
        }
        log.debug("------------------>Read UNLOCK getCurrentLevelAO");
        //release the lock
        rwLock.readLock().unlock();

        if (currentLevels == null) {
        	currentLevels = new EnumMap<SkinGroup, String>(SkinGroup.class);
        	for (SkinGroup skinGroup : SkinGroup.NON_AUTO_SET) {
        		currentLevels.put(skinGroup, "0");
        	}
        }
        return currentLevels;
    }

    /**
     * Get the real level of an opp.
     *
     * @param oppId the opp id
     * @return The real level of the opp or 0 if the opp is not found.
     */
    public double getRealLevel(long oppId) {
        double level = 0;
        //get the read lock in order to take the current opps
        rwLock.readLock().lock();
        log.debug("------------------>Read LOCK getRealLevel");
        SubscribedItemAttributes sia = opportunities.get("opp" + oppId);
        if (null != sia) {
            level = sia.realLevel;
        }
        log.debug("------------------>Read UNLOCK getRealLevel");
        //release the lock
        rwLock.readLock().unlock();
        return level;
    }

    /**
     * Get the dev check level of an opp.
     *
     * @param oppId the opp id
     * @return The level to check against the deviation of the opp or 0 if the opp is not found.
     */
    public double getDevCheckLevel(long oppId) {
        double level = 0;
        //get the read lock in order to take the current opps
        rwLock.readLock().lock();
        log.debug("------------------>Read LOCK getRealLevel");
        SubscribedItemAttributes sia = opportunities.get("opp" + oppId);
        if (null != sia) {
            level = sia.devCheckLevel;
        }
        log.debug("------------------>Read UNLOCK getRealLevel");
        //release the lock
        rwLock.readLock().unlock();
        return level;
    }

    /**
     * Check if specified opportunity is currently on the home page.
     *
     * @param oppId the opp id
     * @return <code>true</code> if the opp is on the home page else <code>false</code>.
     */
    public boolean isOnHomePage(long oppId, int skinId) {
        boolean h = false;
        //get the read lock in order to take the current opps
        rwLock.readLock().lock();
        log.debug("------------------>Read LOCK isOnHomePage");
        SubscribedItemAttributes sia = opportunities.get("opp" + oppId);
        if (null != sia) {
            h = sia.home.get(skinId)==null?false:sia.home.get(skinId);
        }
        log.debug("------------------>Read UNLOCK isOnHomePage");
        //release the lock
        rwLock.readLock().unlock();
        return h;
    }

    /**
     * Disconnects from the levels service and free all resources used by the cache.
     */
    public void close() {
        if (null != connector) {
            connector.abort();
        }
        if (null != heartbeat) {
            heartbeat.stopHeartbeat();
        }
        if (null != sender) {
            sender.stopSenderThread();
        }
        if (null != jmsHandler) {
//            jmsHandler.reset();
            jmsHandler.close();
        }
    }

    /**
     * send data to specific group page
     *
     * @param DataToSend the data to send from ls
     * @param skinsPriorities group priorities to send each group page diff priorities
     * @param groupId the group id
     */
    protected void sendGroupUpdate(HashMap<String, String> DataToSend , HashMap<Integer, HashMap<String, Integer>> skinsPriorities , long groupId) {
        // do nothing
    }

    /**
     * send data to specific home page
     *
     * @param DataToSend
     * @param skinsPriorities
     * @param key
     */
    protected void sendHomeUpdate(HashMap<String, String> DataToSend , HashMap<Integer, HashMap<String, Integer>> skinsPriorities , int key) {
        // do nothing
    }

    /**
     * send data to needed AO TPS subscriptions
     *
     * @param marketId
     * @param update
     */
    private void sendAOTPSUpdate(long marketId, HashMap<String, String> update) {
        try {
            int groupClose = Integer.parseInt((String) update.get("ET_GROUP_CLOSE"));
            boolean opened = isInOpenedState(marketId, update);
            for (int i = 0; i < 6; i++) {
            	if (i == 4) { // group 5 is one touch, we'll skip it
            		i++;
            	}
                if (!opened || (groupClose & (1 << i)) != 0) {
                    simpleListener.aoTPSUpdate(marketId, i + 1, update);
                }
            }
        } catch (Throwable t) {
            log.error("Can't send AO TPS update. market id = " + marketId + " ET_GROUP_CLOSE " + update.get("ET_GROUP_CLOSE"), t);
        }
    }

    /**
     * Send update to all skins control subscriptions.
     *
     * @param toSend
     * @param skinsPriorities
     * @param key skin id
     */
    private void sendAOCtrlUpdate(HashMap<String, String> toSend, HashMap<Integer, HashMap<String, Integer>> skinsPriorities, int key) {
//        Integer key = null;
//        for (Iterator<Integer> i = skinsPriorities.keySet().iterator(); i.hasNext();) {
//            key = i.next();
            HashMap<String, String> ctrl = new HashMap<String, String>(toSend);
            ctrl.put("ET_PRIORITY", String.valueOf((String) ctrl.get("ET_PRIORITY") + skinsPriorities.get(key).get("home_page_priority")));
            simpleListener.aoCtrlUpdate(key, ctrl);
//        }
    }

    /**
     * Check if the opp in this update is in opened state or not.
     *
     * @param data
     * @return
     */
    private boolean isInOpenedState(long marketId, HashMap<String, String> data) {
        boolean b = false;
        try {
            String s = (String) data.get("ET_STATE");
            int state = Integer.parseInt(s);
            if (state >= 2/*Opportunity.STATE_OPENED*/ && state <= 6/*Opportunity.STATE_CLOSED*/) {
                b = true;
            } else {
                log.debug("not opened - marketId: " + marketId + " data: " + data);
            }
            // if this one is not opened see if there are others on this market
            if (!b) {
                String crrOppKey = (String) data.get("key");
                String key = null;
                for (Iterator<String> i = opportunities.keySet().iterator(); i.hasNext();) {
                    key = i.next();
                    SubscribedItemAttributes sia = opportunities.get(key);
                    if (marketId == sia.marketId && !key.equals(crrOppKey)) {
                        int crrState = Integer.parseInt(sia.crrState.get("ET_STATE"));
                        b = b || (crrState >= 2/*Opportunity.STATE_OPENED*/ && crrState <= 6/*Opportunity.STATE_CLOSED*/);
                        log.debug("marketId: " + marketId + " crrState: " + crrState + " b: " + b);
                    }
                }
            }
            if (!b) {
                log.debug("no other opp opened on this market");
            }
        } catch (Throwable t) {
            log.error("Can't check state of update.", t);
        }
        return b;
    }

    /**
     * AO state subscriptions need to know about opening and closing of market.
     *
     * @param prevUpdate
     * @param crrUpdate
     */
    private void checkForAOOpenedClosedTransition(long marketId, HashMap<String, String> prevUpdate, HashMap<String, String> crrUpdate) {
        boolean po = isInOpenedState(marketId, prevUpdate);
        boolean co = isInOpenedState(marketId, crrUpdate);

        if ((!po && co) ||     // opened
                (po && !co)) { // closed // Tony - I'm not really sure when this case will happen. Probably pausing long term opp
            if (log.isDebugEnabled()) {
                log.debug("Opened/Closed transition - po: " + po + " co: " + co);
            }
            sendAOStateUpdate(marketId, crrUpdate);
        }
    }

    /**
     * AO state subscription sends 1 for opened market and 0 for closed.
     *
     * @param data
     */
    private void sendAOStateUpdate(long marketId, HashMap<String, String> data) {
        HashMap<String, String> tmp = new HashMap<String, String>();
        tmp.put("key", (String) data.get("ET_NAME"));
        tmp.put("command", "UPDATE");
        tmp.put("STATE", (String) data.get("FILTER_SCHEDULED"));
        simpleListener.aoStateUpdate(tmp, marketId);
    }

    protected void sendInsurancesUpdate(long oppId, HashMap<String, String> tmp, double level) {
        // do nothing
    }

    private class HeartbeatThread extends Thread {
        private long lifeId;
        private int count;
        private boolean running;

        public HeartbeatThread(long lifeId) {
            this.lifeId = lifeId;
        }

        @Override
		public void run() {
            running = true;

            // this thread will go on until the heartbeat ID is changed
            while (this.lifeId == lastHeartbeatLifeId) {
                if (!running) {
                    break;
                }
                // waits 2 seconds (i.e. we check the counter each 2 seconds)
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                }
                // get the lock
                rwLock.writeLock().lock();
                log.debug("------------------>Write LOCK 7");

                // check the level service heartbeat
                if (this.lifeId == lastHeartbeatLifeId && count == heartbeatCount) {
                    log.warn("2 Seconds without Heartbeats: " + this.lifeId);
                    // the heartbeat is the same that we have to check, but no heartbeat arrived
                    // in the last two second. We consider the Generator down.
                    onFeedDisconnection();
                    // release the lock and end the thread
                    log.debug("------------------>Write UNLOCK 7 1");
                    rwLock.writeLock().unlock();
                    // TODO: if we don't get anything from the JMS how do we know we are connected to id?
                    return;
                } else {
                    // at least 1 heartbeat arrived in the last 2 seconds
                    count = heartbeatCount;
                }

                // release the lock
                log.debug("------------------>Write UNLOCK 7 2");
                rwLock.writeLock().unlock();
            }
            if (log.isDebugEnabled()) {
                log.debug("Heartbeat thread ends.");
            }
        }

        public void stopHeartbeat() {
            running = false;
        }
    }

    /**
     * This thread keeps on trying to connect to JMS until succedes. When connected
     * calls the onConnection method
     */
    private class ConnectionLoopTSQS extends ConnectionLoop {
        public ConnectionLoopTSQS(JMSHandler jmsHandler, int recoveryPause) {
            super(jmsHandler, recoveryPause);
            this.threadName = "ConnectionLoopTSQS";
        }

        @Override
		protected void onConnectionCall() {
            // let the connector go. it did its job
            connector = null;

            getSnapshot();
            // the get snapshot will actually put a "snapshot" request
            // to the sender queue (which will wake up the sender).
            // if no snapshot was requested here is a good place to wakeup
            // the sender
        }

        @Override
		protected void connectionCall() throws JMSException, NamingException {
            //initialize TopicSubscriber and QueueSender
            jmsHandler.initTopicSubscriber();
            jmsHandler.initQueueSender(msgPoolSize);
        }
    }

    /**
     * This thread keeps on trying to connect to JMS until succedes. When connected
     * calls the onConnection method
     */
    private class ConnectionLoopQS extends ConnectionLoop {
        public ConnectionLoopQS(JMSHandler jmsHandler, int recoveryPause) {
            super(jmsHandler, recoveryPause);
            this.threadName = "ConnectionLoopQS";
        }

        @Override
		protected void onConnectionCall() {
            // let the connector go. it did its job
            connector = null;

            sender.wakeUp();
        }

        @Override
		protected void connectionCall() throws JMSException, NamingException {
            //initialize QueueSender
            jmsHandler.initQueueSender(msgPoolSize);
        }
    }

    public class SenderThread extends Thread {
        private boolean running;

        //this is the queue of pending requests for the LevelsService
        private ConcurrentLinkedQueue<String> toSendRequests = new ConcurrentLinkedQueue<String>();

        @Override
		public void run() {
            running = true;
            if (log.isDebugEnabled()) {
                log.debug("SenderThread started");
            }

            while (true) {
                if (!running) {
                    break;
                }

                if (log.isDebugEnabled()) {
                    log.debug("Sender starting work.");
                }

                //go on until there are requests in the queue
                String nextRequest = "";
                try {
                    // just get the message from the head but don't remove it
                    while ((nextRequest = toSendRequests.peek()) != null) {
                        //send message to the feed through JMS
                        jmsHandler.sendMessage(nextRequest);
                        toSendRequests.poll(); // now that the message was sent we can remove it
                        if (log.isDebugEnabled()) {
                            log.debug("Message dispatched to JMS: " + nextRequest);
                        }
                    }
                } catch (JMSException je) {
                    log.error("Can't actually dispatch request " + nextRequest + ": JMSException -> " + je.getMessage());
                    onException(je);
                }

                if (log.isDebugEnabled()) {
                    log.debug("Sender falling asleep.");
                }

                try {
                    synchronized (sender) {
                        sender.wait();
                    }
                } catch (InterruptedException ie) {
                    // do nothing
                }
            }

            if (log.isDebugEnabled()) {
                log.debug("SenderThread ends");
            }
        }

        /**
         * Queue a message to be sent to the LevelsService.
         *
         * @param message the message to send
         */
        public void send(String message) {
            toSendRequests.add(message);
            synchronized (sender) {
                try {
                    sender.notify();
                } catch (Throwable t) {
                    log.error("Can't wake up the sender.", t);
                }
            }
        }

        public boolean cancel(String message) {
            return toSendRequests.remove(message);
        }

        /**
         * Wake up the sender to try to send messages if available.
         */
        public void wakeUp() {
            synchronized (sender) {
                try {
                    sender.notify();
                } catch (Throwable t) {
                    log.error("Can't wake up the sender.", t);
                }
            }
        }

        public void stopSenderThread() {
            running = false;
            wakeUp();
        }

        public int getQueueSize() {
            return toSendRequests.size();
        }
    }

    private class SnapshotThread extends Thread {
        private static final int TYPE_ETRADER = 1;
        private static final int TYPE_AO = 2;
        private static final int TYPE_AO_CTRL = 3;
        private static final int TYPE_AO_STATE = 4;
        private static final int TYPE_OPTION_PLUS = 5;
        private static final int TYPE_BINARY_ZERO_ONE_HUNDRED = 6;
        private static final int TYPE_TT_UPDATE = 7;
        private static final int TYPE_TT_BINARY0100_UPDATE = 8;
        private static final int TYPE_LIVE_GLOBE = 9;
        private static final int TYPE_LIVE_INVESTMENTS = 10;
        private static final int TYPE_LIVE_TRENDS = 11;

        private int type;
        private boolean home;
        private long groupId;
        private int skinId;
        private long marketId;
//        private int scheduled;
        private Object[] params;

        public SnapshotThread(boolean home, long groupId, int skinId) {
            this.skinId = skinId;
            this.home = home;
            this.groupId = groupId;
            type = TYPE_ETRADER;
            marketId = 0;
//            scheduled = 0;
            log.debug("SnapshotThread - skinId: " + skinId + " home: " + home + " groupId: " + groupId + " type: TYPE_ETRADER");
        }

        public SnapshotThread(long marketId, int scheduled) {
            skinId = 0;
            home = false;
            groupId = 0;
            type = TYPE_AO;
            this.marketId = marketId;
//            this.scheduled = scheduled;
            log.debug("SnapshotThread - type: TYPE_AO marketId: " + marketId + " scheduled: " + scheduled);
        }

        public SnapshotThread(int skinId) {
            this.skinId = skinId;
            home = false;
            groupId = 0;
            type = TYPE_AO_CTRL;
            marketId = 0;
//            scheduled = 0;
            log.debug("SnapshotThread - skindId: " + skinId + " type: TYPE_AO_CTRL");
        }

        public SnapshotThread(boolean optionPlus) {
            skinId = 0;
            home = false;
            groupId = 0;
            type = optionPlus ? TYPE_OPTION_PLUS : TYPE_BINARY_ZERO_ONE_HUNDRED;
            marketId = 0;
//            scheduled = 0;
            log.debug("SnapshotThread - type: " + type);
        }

        public SnapshotThread() {
            type = TYPE_AO_STATE;
            log.debug("SnapshotThread - type: TYPE_AO_STATE");
        }

        public SnapshotThread(int snapshotType, Object[] params) {
        	this.type = snapshotType;
        	this.params = params;
        }

		/**
		 * @param type the type to set
		 */
		public void setType(int type) {
			this.type = type;
		}

		@Override
		public void run() {
            if (log.isDebugEnabled()) {
                log.debug("SnapshotThread start");
            }
            try {
                //get the read lock in order to take the current opps
                rwLock.readLock().lock();
                if (log.isDebugEnabled()) {
                    log.debug("------------------>Read LOCK SnapshotThread");
                }

                if (type == TYPE_LIVE_GLOBE) {
                	if (liveGlobeLastUpdate != null) {
	                	Map<String, String> tmp = new HashMap<String, String>();
	                	tmp.putAll(liveGlobeLastUpdate);
	                	simpleListener.liveGlobeUpdate(LiveJMSMessage.KEY_GLOBE, tmp);
                	} else {
                		log.warn("Snapshot for live globe is [null]. Nothing to send...");
                	}
                } else if (type == TYPE_LIVE_INVESTMENTS) {
                	String tableName = (String) params[0];
                	Map<String, Map<String, String>> table = liveInvestmentsUpdates.get(tableName);
                	for (Iterator<Map<String, String>> i = table.values().iterator(); i.hasNext();) {
                    	Map<String, String> tmp = new HashMap<String, String>();
                		tmp.putAll(i.next());
                		simpleListener.liveInvestmentsUpdate(LiveJMSMessage.KEY_INVESTMENTS_PREFIX + "_" + tableName, tmp);
                	}
                } else if (type == TYPE_LIVE_TRENDS) {
                	String tableName = (String) params[0];
                	Map<String, String> table = liveTrendsLastUpdate.get(tableName);
                	if (table != null) {
	                	Map<String, String> tmp = new HashMap<String, String>();
	                	tmp.putAll(table);
	                	simpleListener.liveTrendsUpdate(tableName, tmp);
                	} else {
                		log.warn("Snapshot for live trends is [null]. Nothing to send...");
                	}
                } else {
	                Enumeration<SubscribedItemAttributes> subItems = opportunities.elements();
	                while(subItems.hasMoreElements()) {
	                    SubscribedItemAttributes sia = subItems.nextElement();
	                    String oppType = sia.crrState.get("AO_TYPE");
	                    if (null != oppType && oppType.equals(String.valueOf(Opportunity.TYPE_OPTION_PLUS))) {
	                        if (type == TYPE_OPTION_PLUS) {
	                            simpleListener.optionPlusUpdate(sia.marketId, new HashMap<String, String>(sia.crrState));
	                        } else if (type == TYPE_TT_UPDATE) {
	                        	simpleListener.ttUpdate(Long.parseLong(sia.oppId.startsWith("opp") ? sia.oppId.substring(3) : sia.oppId),
	                        							sia.autoShiftParameter,
	                        							sia.calls,
	                        							sia.puts,
	                        							sia.feedName,
	                        							sia.decimalPoint,
	                        							sia.isTraderDisable,
	                        							sia.isAutoDisable,
	                        							new HashMap<String, String>(sia.crrState),
	                        							sia.notClosed,
	                        							sia.skinGroupMapping);
	                        }
	                        continue;
	                    }
	                    if (null != oppType && (oppType.equals(String.valueOf(Opportunity.TYPE_BINARY_0_100_ABOVE)) || oppType.equals(String.valueOf(Opportunity.TYPE_BINARY_0_100_BELOW)) || oppType.equals(String.valueOf(Opportunity.TYPE_DYNAMICS)))) {
	                        if (type == TYPE_BINARY_ZERO_ONE_HUNDRED) {
	                        	simpleListener.binaryZeroOneHundredUpdate(sia.marketId, sia.opportunityTypeId, new HashMap<String, String>(sia.crrState));
	                        } else if (type == TYPE_TT_BINARY0100_UPDATE) {
	                        	simpleListener.ttBinary0100Update(Long.parseLong(sia.oppId.startsWith("opp") ? sia.oppId.substring(3) : sia.oppId),
	                        										sia.autoShiftParameter,
	                        										sia.calls,
	                        										sia.puts,
	                        										sia.feedName,
	                        										sia.decimalPoint,
	                        										sia.isTraderDisable,
	                        										sia.isAutoDisable,
	                        										new HashMap<String, String>(sia.crrState),
	                        										sia.contractsBought,
	                        										sia.contractsSold,
	                        										sia.parameters,
	                        										sia.level,
	                        										sia.notClosed,
	                        										sia.skinGroupMapping);
	                        }
	                        continue;
	                    }
	                    try {
	                        if (type == TYPE_ETRADER) {
	                            if (home && null != sia.home.get(skinId) && sia.home.get(skinId)) {
	                                HashMap<String, String> tmp = new HashMap<String, String>(sia.crrState);
	                                tmp.put("command", "ADD");
	                                sendHomeUpdate(tmp, sia.skinsPriorities, skinId);
	                                //listener.update("home", tmp);
	                            }
	                            if (!home && sia.groupId == groupId) {
	                                HashMap<String, String> tmp = new HashMap<String, String>(sia.crrState);
	                                tmp.put("command", "ADD");
	            //                    listener.update("invest", tmp);
	                                sendGroupUpdate(tmp, sia.skinsPriorities, groupId);
	                                //listener.update("group" + sia.groupId, tmp);
	                            }
	                        } else if (type == TYPE_AO) {
	    //                        if (sia.marketId == marketId && sia.scheduled == scheduled) {
	    //                            HashMap tmp = (HashMap) sia.crrState.clone();
	    //                            tmp.put("command", "ADD");
	    //                            listener.aoTPSUpdate(marketId, scheduled, tmp);
	    //                            break;
	    //                        }
	                            if (sia.marketId == marketId || marketId == 0) {
	                                HashMap<String, String> tmp = new HashMap<String, String>(sia.crrState);
	                                tmp.put("command", "ADD");
	                                sendAOTPSUpdate(sia.marketId, tmp);
	                            }
	                        } else if (type == TYPE_AO_CTRL) {
	                            if (sia.home.get(skinId)) {
	                                HashMap<String, String> tmp = new HashMap<String, String>(sia.crrState);
	                                tmp.put("command", "ADD");
	                                tmp.put("ET_PRIORITY", String.valueOf((String) tmp.get("ET_PRIORITY") + sia.skinsPriorities.get(skinId).get("home_page_priority")));
	                                simpleListener.aoCtrlUpdate(skinId, tmp);
	                            }
	                        } else if (type == TYPE_TT_UPDATE) {
	                        	simpleListener.ttUpdate(Long.parseLong(sia.oppId.startsWith("opp") ? sia.oppId.substring(3) : sia.oppId),
	                        							sia.autoShiftParameter,
	                        							sia.calls,
	                        							sia.puts,
	                        							sia.feedName,
	                        							sia.decimalPoint,
	                        							sia.isTraderDisable,
	                        							sia.isAutoDisable,
	                        							new HashMap<String, String>(sia.crrState),
	                        							sia.notClosed,
	                        							sia.skinGroupMapping);
	                        } else {
	                            sendAOStateUpdate(sia.marketId, sia.crrState);
	                        }
	                    } catch (Exception e) {
	                        log.error("Failed to send snapshot. skinid "  + skinId + " home: " + home + " sia= " + sia.home.get(skinId) + " market id " + sia.marketId + " group id " + sia.groupId, e);
	                    }
	                }
                }
            } catch (Exception e) {
                log.error("Failed to send snapshot. skinid "  + skinId + " home: " + home , e);
            } finally {
                log.debug("------------------>Read UNLOCK SnapshotThread");
                //release the lock
                rwLock.readLock().unlock();
            }
            if (log.isDebugEnabled()) {
                log.debug("SnapshotThread end");
            }
        }
    }
    
    /**
     *  class that will wait for X sec untill 1 min is over and then send another snapshot request
     *  @param tempTimeToWait the time to wait before sending the request
     */
    public class SendSnapshotAfter1MinThread extends Thread {
        //waiting time for the sleep
        long timeToWait;

        public SendSnapshotAfter1MinThread(long tempTimeToWait) {
            // adding 1 so it will do the snapshot for sure (wait 61 sec from last request)
            this.timeToWait = (tempTimeToWait + 1);
        }

        @Override
		public void run() {
            //sleeping and then getting snapshot
            try {
                Thread.sleep(timeToWait);
            } catch (InterruptedException e) {
                // do nothing
            }
            getSnapshot();
            needAnotherSnapshot = false;
        }
    }
}