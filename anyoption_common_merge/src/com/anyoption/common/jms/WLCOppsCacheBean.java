package com.anyoption.common.jms;

import java.io.Serializable;
import java.util.Date;

/**
 * MBean for opportunity cache
 * 
 * @author EyalG
 */
public class WLCOppsCacheBean implements Serializable {

	private static final long serialVersionUID = -3991127411814305789L;
	
	public static final int COMMAND_FIELD = 1;
	public static final int MARKETID_FIELD = 2;
	public static final int OPPORTUNITYID_FIELD = 3;
	public static final int SCHEDULE_FIELD = 4;
	public static final int TIMEESTCLOSING_FIELD = 5;
	public static final int STATE_FIELD = 6;
    
	private long marketId;
	private long opportunityId;
	private int schedule;
	private Date timeEstClosing;
	private int state;
	
	public WLCOppsCacheBean(String msg) {
		String[] arr = msg.split("_");
		marketId = Long.valueOf(arr[MARKETID_FIELD]);
		opportunityId = Long.valueOf(arr[OPPORTUNITYID_FIELD]);
		schedule = Integer.valueOf(arr[SCHEDULE_FIELD]);
		timeEstClosing = new Date(Long.valueOf(arr[TIMEESTCLOSING_FIELD]));
		state = Integer.valueOf(arr[STATE_FIELD]);
	}
	
	public long getMarketId() {
		return marketId;
	}
	
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	
	public long getOpportunityId() {
		return opportunityId;
	}
	
	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}
	
	public int getSchedule() {
		return schedule;
	}
	
	public void setSchedule(int schedule) {
		this.schedule = schedule;
	}
	
	public Date getTimeEstClosing() {
		return timeEstClosing;
	}
	
	public void setTimeEstClosing(Date timeEstClosing) {
		this.timeEstClosing = timeEstClosing;
	}
	
	public int getState() {
		return state;
	}
	
	public void setState(int state) {
		this.state = state;
	}
}