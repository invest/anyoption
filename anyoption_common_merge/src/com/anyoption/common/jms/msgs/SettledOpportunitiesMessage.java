/**
 * 
 */
package com.anyoption.common.jms.msgs;

import java.io.Serializable;
import java.util.List;

import com.anyoption.common.beans.OpportunitySettle;

/**
 * @author pavelhe
 *
 */
public class SettledOpportunitiesMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7999693248040971927L;
	
	private List<OpportunitySettle> settledOpps;
	
	public SettledOpportunitiesMessage(List<OpportunitySettle> settledOpps) {
		this.settledOpps = settledOpps;
	}

	/**
	 * @return the settledOpps
	 */
	public List<OpportunitySettle> getSettledOpps() {
		return settledOpps;
	}

	@Override
	public String toString() {
		return "SettledOpportunitiesMessage [settledOpps=" + settledOpps + "]";
	}

}
