package com.anyoption.common.server.pixel;

import org.apache.log4j.Logger;

/**
 * 
 * @author eyal.ohana
 *
 */
public class ServerPixelHandlerFTD extends ServerPixelHandler {
	public static final Logger log = Logger.getLogger(ServerPixelHandlerFTD.class);
	
	public ServerPixelHandlerFTD() {
		
	}
	
	@Override
	public boolean checkCondition() {
		return true;
	}
	
	@Override
	public void prepareInsertPixelToDB(String pixel) {
		insertPixelToDB(serverSidePixelEvent.getTransactionId(), pixel);
	}
}
