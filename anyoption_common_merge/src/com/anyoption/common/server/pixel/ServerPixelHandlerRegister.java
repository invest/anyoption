package com.anyoption.common.server.pixel;

import org.apache.log4j.Logger;


public class ServerPixelHandlerRegister extends ServerPixelHandler {
	public static final Logger log = Logger.getLogger(ServerPixelHandlerRegister.class);
    
	private int numberOfDays;
	
    public ServerPixelHandlerRegister() {
	}

	@Override
	public boolean checkCondition() {
		return true;
	}


	@Override
	public void prepareInsertPixelToDB(String pixel) {
		insertPixelToDB(serverSidePixelEvent.getUserId(), pixel);
	}


	public int getNumberOfDays() {
		return numberOfDays;
	}


	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}    
}