package com.anyoption.common.server.pixel;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.DeviceUniqueIdSkin;
import com.anyoption.common.managers.DeviceUniqueIdsSkinManagerBase;


public class ServerPixelHandlerOpenApp extends ServerPixelHandler {
	public static final Logger log = Logger.getLogger(ServerPixelHandlerOpenApp.class);
    
	
    public ServerPixelHandlerOpenApp() {
	}


	@Override
	public boolean checkCondition() {
		return true;
	}


	@Override
	public void prepareInsertPixelToDB(String pixel) {
		try {
			DeviceUniqueIdSkin deviceUniqueIdSkin = DeviceUniqueIdsSkinManagerBase.getByDeviceId(serverSidePixelEvent.getDuid());
			insertPixelToDB(deviceUniqueIdSkin.getId(), pixel);
		} catch (Exception e) {
			log.error("cant insert to pixels server", e);
		}
	}

	
    
    
}