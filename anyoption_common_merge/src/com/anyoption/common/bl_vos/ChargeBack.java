package com.anyoption.common.bl_vos;

import java.util.Date;

import javax.faces.event.ValueChangeEvent;

public class ChargeBack implements java.io.Serializable{

	private static final long serialVersionUID = 7264292272444309605L;

	public static final int STATE_POS = 1;
	public static final int STATE_CHB = 2;
	
	public static final int CHARGEBACK_STATUS_STARTED = 1;
	public static final int CHARGEBACK_STATUS_POS = 3;
	public static final int CHARGEBACK_STATUS_CLOSED = 4;

	private long id;
	private long writerId;
	private Date timeCreated;
	private long statusId;
	private String description;
	private String statusName;
	private String utcOffsetCreated;
	private String arn;
	private Date timePos;
	private Date timeChb;
	private Date timeRep;
	private Date timeSettled;
	private long issueId;
	private int state;

	private String writer;
	private long amount;
	private long transactionId;
	private String navigation;

	private long userId;
	private String userName;
	private String userFirstName;
	private String userLastName;
	private long currencyId;
	private boolean isStateChanged;
	private Date timeStateChanged;
	private String clearingProviderName;
	private boolean threeD;
	
	public ChargeBack() {
		isStateChanged = false;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getStatusId() {
		return statusId;
	}
	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public long getWriterId() {
		return writerId;
	}
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getNavigation() {
		return navigation;
	}
	public void setNavigation(String navigation) {
		this.navigation = navigation;
	}
	public String doNavigation() {
		return navigation;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "ChargeBack ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "writerId = " + this.writerId + TAB
	        + "timeCreated = " + this.timeCreated + TAB
	        + "statusId = " + this.statusId + TAB
	        + "description = " + this.description + TAB
	        + "utcOffsetCreated = " + this.utcOffsetCreated + TAB
	        + "writer = " + this.writer + TAB
	        + "amount = " + this.amount + TAB
	        + "transactionId = " + this.transactionId + TAB
	        + "navigation = " + this.navigation + TAB
	        + "userId = " + this.userId + TAB
	        + "userName = " + this.userName + TAB
	        + "userFirstName = " + this.userFirstName + TAB
	        + "userLastName = " + this.userLastName + TAB
	        + " )";

	    return retValue;
	}

	/**
	 * @return the statusName
	 */
	public String getStatusName() {
		return statusName;
	}

	/**
	 * @param statusName the statusName to set
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	/**
	 * @return the utcOffsetCreated
	 */
	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	/**
	 * @param utcOffsetCreated the utcOffsetCreated to set
	 */
	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	/**
	 * @return the arn
	 */
	public String getArn() {
		return arn;
	}

	/**
	 * @param arn the arn to set
	 */
	public void setArn(String arn) {
		this.arn = arn;
	}

	/**
	 * @return the issueId
	 */
	public long getIssueId() {
		return issueId;
	}

	/**
	 * @param issueId the issueId to set
	 */
	public void setIssueId(long issueId) {
		this.issueId = issueId;
	}

	/**
	 * @return the timeChb
	 */
	public Date getTimeChb() {
		return timeChb;
	}

	/**
	 * @param timeChb the timeChb to set
	 */
	public void setTimeChb(Date timeChb) {
		this.timeChb = timeChb;
	}

	/**
	 * @return the timePos
	 */
	public Date getTimePos() {
		return timePos;
	}

	/**
	 * @param timePos the timePos to set
	 */
	public void setTimePos(Date timePos) {
		this.timePos = timePos;
	}

	/**
	 * @return the timeRep
	 */
	public Date getTimeRep() {
		return timeRep;
	}

	/**
	 * @param timeRep the timeRep to set
	 */
	public void setTimeRep(Date timeRep) {
		this.timeRep = timeRep;
	}

	/**
	 * @return the timeSettled
	 */
	public Date getTimeSettled() {
		return timeSettled;
	}

	/**
	 * @param timeSettled the timeSettled to set
	 */
	public void setTimeSettled(Date timeSettled) {
		this.timeSettled = timeSettled;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * @return the isStateChanged
	 */
	public boolean isStateChanged() {
		return isStateChanged;
	}

	/**
	 * @param isStateChanged the isStateChanged to set
	 */
	public void setStateChanged(boolean isStateChanged) {
		this.isStateChanged = isStateChanged;
	}

	public boolean isChbState(){
		return state == STATE_CHB;
	}

	/**
	 * @return the timeStateChanged
	 */
	public Date getTimeStateChanged() {
		return timeStateChanged;
	}

	/**
	 * @param timeStateChanged the timeStateChanged to set
	 */
	public void setTimeStateChanged(Date timeStateChanged) {
		this.timeStateChanged = timeStateChanged;
	}

	public boolean isStateChangedBefore(){
		return (null != timeStateChanged);
	}

	/**
	 * @return the clearingProviderName
	 */
	public String getClearingProviderName() {
		return clearingProviderName;
	}

	/**
	 * @param clearingProviderName the clearingProviderName to set
	 */
	public void setClearingProviderName(String clearingProviderName) {
		this.clearingProviderName = clearingProviderName;
	}
	
	public boolean isThreeD() {
		return threeD;
	}

	public void setThreeD(boolean threeD) {
		this.threeD = threeD;
	}

	public void updateState(ValueChangeEvent ev){
		if (getId()>0){
			setStateChanged(true);
			setTimeStateChanged(new Date());
		}
	}
}
