package com.anyoption.common.bl_vos;



/**
 * @author Eran
 *
 */
public class UkashDeposit implements java.io.Serializable {

	private static final long serialVersionUID = -470587687089891557L;
	private long id;
	private long transactionId;
	private String voucherNumber;
	private String voucherValue;
	private String voucherCurrency;
	private boolean differentCurrencies;
	private long convertedAmount;
	private String userCurrency;
	private int status;
	
	private long voucherCurrencyId;
	private long voucherValueLong;

	public UkashDeposit() {
		id = 0;
		transactionId = 0;
		voucherNumber = "";
		voucherValue = "";
		voucherCurrency = "";
		status = -1;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String ls = " \n ";

	    String retValue = "";

	    retValue = "UkashDeposit ( "
	        + super.toString() + ls
	        + "id = " + this.id + ls
	        + "transactionId = " + this.transactionId + ls
	        + "voucherNumber = " + this.voucherNumber + ls
	        + "voucherValue = " + this.voucherValue + ls
	        + "voucherCurrency = " + this.voucherCurrency + ls
	        + "userCurrency = " + this.userCurrency + ls
	        + "status = " + this.status + ls
	        + " )";

	    return retValue;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the voucherCurrency
	 */
	public String getVoucherCurrency() {
		return voucherCurrency;
	}

	/**
	 * @param voucherCurrency the voucherCurrency to set
	 */
	public void setVoucherCurrency(String voucherCurrency) {
		this.voucherCurrency = voucherCurrency;
	}

	/**
	 * @return the voucherNumber
	 */
	public String getVoucherNumber() {
		return voucherNumber;
	}

	/**
	 * @param voucherNumber the voucherNumber to set
	 */
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	/**
	 * @return the voucherValue
	 */
	public String getVoucherValue() {
		return voucherValue;
	}


	/**
	 * @param voucherValue the voucherValue to set
	 */
	public void setVoucherValue(String voucherValue) {
		this.voucherValue = voucherValue;
	}

	/**
	 * @return the differentCurrencies
	 */
	public boolean isDifferentCurrencies() {
		return differentCurrencies;
	}

	/**
	 * @param differentCurrencies the differentCurrencies to set
	 */
	public void setDifferentCurrencies(boolean differentCurrencies) {
		this.differentCurrencies = differentCurrencies;
	}

	/**
	 * @return the convertedAmount
	 */
	public long getConvertedAmount() {
		return convertedAmount;
	}

	/**
	 * @param convertedAmount the convertedAmount to set
	 */
	public void setConvertedAmount(long convertedAmount) {
		this.convertedAmount = convertedAmount;
	}

	/**
	 * @return the userCurrency
	 */
	public String getUserCurrency() {
		return userCurrency;
	}

	/**
	 * @param userCurrency the userCurrency to set
	 */
	public void setUserCurrency(String userCurrency) {
		this.userCurrency = userCurrency;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * Return status msg of the response: 1- accepted, 2- decline or 99- failed
	 * @return
	 */
	public String getStatusMsg(){
		String msg = "";
		switch(status){
		case 0:
			msg = "Accepted";
			break;
		case 1:
			msg = "Decline";
			break;
		case 99:
			msg = "Failed";
			break;
		}
		return msg;
	}
	
	public long getVoucherValueLong() {
		return voucherValueLong;
	}

	public void setVoucherValueLong(long voucherValueLong) {
		this.voucherValueLong = voucherValueLong;
	}

	public long getVoucherCurrencyId() {
		return voucherCurrencyId;
	}

	public void setVoucherCurrencyId(long voucherCurrencyId) {
		this.voucherCurrencyId = voucherCurrencyId;
	}
}
