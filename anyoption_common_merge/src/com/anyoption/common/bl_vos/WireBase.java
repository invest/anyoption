package com.anyoption.common.bl_vos;

public class WireBase implements java.io.Serializable {

	private static final long serialVersionUID = 7948449323992300356L;
	
	protected long id;
	protected String bankId;
	protected String branch;
	protected String accountNum;
	protected String accountName;
	protected String accountInfo;
	protected String deposit;    // for wire deposit
	protected String bankName;   // related to bankId

	protected String beneficiaryName;
	protected String swift;
	protected String iban;
	protected String bankNameTxt;   // just name without bankId related

	protected String branchAddress;
	protected String branchName;
	protected String amount;
	protected String bankCode;

	protected String accountType; // from envoy system
	protected String checkDigits; // from envoy system
	protected Long bankFeeAmount;
    protected Long bankWithdrawId;
    protected String bankWithdrawName;

	public WireBase() {
		id = 0;
		beneficiaryName = "";
		swift = "";
		iban = "";
		accountNum = "";
		bankNameTxt = "";
		amount = "";
		bankId = "";
		branch = "";
		accountName = "";

	}


	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public String getAccountInfo() {
		return accountInfo;
	}

	public void setAccountInfo(String accountInfo) {
		this.accountInfo = accountInfo;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDeposit() {
		return deposit;
	}

	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}

	/**
	 * @param bankName the bankName to set
	 */
	public void setBankNameTxt(String bankNameTxt) {
		this.bankNameTxt = bankNameTxt;
	}


	/**
	 * @return the bankName
	 */
	public String getBankNameTxt() {
		return bankNameTxt;
	}

	/**
	 * @return the beneficiaryName
	 */
	public String getBeneficiaryName() {
		return beneficiaryName;
	}


	/**
	 * @param beneficiaryName the beneficiaryName to set
	 */
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}


	/**
	 * @return the iban
	 */
	public String getIban() {
		return iban;
	}


	/**
	 * @param iban the iban to set
	 */
	public void setIban(String iban) {
		this.iban = iban;
	}


	/**
	 * @return the swift
	 */
	public String getSwift() {
		return swift;
	}


	/**
	 * @param swift the swift to set
	 */
	public void setSwift(String swift) {
		this.swift = swift;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "WireBase ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "bankId = " + this.bankId + TAB
	        + "branch = " + this.branch + TAB
	        + "accountNum = " + this.accountNum + TAB
	        + "accountName = " + this.accountName + TAB
	        + "accountInfo = " + this.accountInfo + TAB
	        + "deposit = " + this.deposit + TAB
	        + "beneficiaryName = " + this.beneficiaryName + TAB
	        + "swift = " + this.swift + TAB
	        + "iban = " + this.iban + TAB
	        + "accountNumber = " + this.accountNum + TAB
	        + "bankName = " + this.bankName + TAB
	        + "amount = " + this.amount + TAB
	        + " )";

	    return retValue;
	}


	public String getBankName() {
		return bankName;
	}


	public void setBankName(String bankName) {
		this.bankName = bankName;
	}


	public String getBranchAddress() {
		return branchAddress;
	}


	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}

	/**
	 * @return the bankCode
	 */
	public String getBankCode() {
		return bankCode;
	}

	/**
	 * @param bankCode the bankCode to set
	 */
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}


	/**
	 * @return the accountType
	 */
	public String getAccountType() {
		return accountType;
	}


	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}


	/**
	 * @return the checkDigits
	 */
	public String getCheckDigits() {
		return checkDigits;
	}


	/**
	 * @param checkDigits the checkDigits to set
	 */
	public void setCheckDigits(String checkDigits) {
		this.checkDigits = checkDigits;
	}


	/**
	 * @return the bankFeeAmount
	 */
	public Long getBankFeeAmount() {
		return bankFeeAmount;
	}


	/**
	 * @param bankFeeAmount the bankFeeAmount to set
	 */
	public void setBankFeeAmount(Long bankFeeAmount) {
		this.bankFeeAmount = bankFeeAmount;
	}


    public void setBankWithdrawId(Long bankWithdrawId) {
        this.bankWithdrawId = bankWithdrawId;
    }


    public Long getBankWithdrawId() {
        return bankWithdrawId;
    }


    /**
     * @return the bankWithdrawName
     */
    public String getBankWithdrawName() {
        return bankWithdrawName;
    }


    /**
     * @param bankWithdrawName the bankWithdrawName to set
     */
    public void setBankWithdrawName(String bankWithdrawName) {
        this.bankWithdrawName = bankWithdrawName;
    }


	public String getBranchName() {
		return branchName;
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

}
