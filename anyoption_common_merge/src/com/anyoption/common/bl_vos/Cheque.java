package com.anyoption.common.bl_vos;

public class Cheque implements java.io.Serializable {

	private static final long serialVersionUID = 956268236792666594L;
	private long id;
	private String name;
	private String street;
	private String streetNo;
	private long cityId;
	private String cityName;
	private String zipCode;
	private String chequeId;
	private boolean feeCancel;

	/**
	 * 
	 */
	public Cheque() {
		feeCancel = false;
	}

    /**
     * @param cityName
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    
	/**
	 * @return
	 */
	public String getCityName() {
	    return cityName;
    }

	/**
	 * @return
	 */
	public long getCityId() {
		return cityId;
	}
    
	/**
	 * @param cityId
	 */
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
    
	/**
	 * @return
	 */
	public long getId() {
		return id;
	}
    
	/**
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}
    
	/**
	 * @return
	 */
	public String getName() {
		return name;
	}
    
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
    
	/**
	 * @return
	 */
	public String getStreet() {
		return street;
	}
    
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
    
	/**
	 * @return
	 */
	public String getZipCode() {
		return zipCode;
	}
    
	/**
	 * @param zipCode
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
    
	/**
	 * @return
	 */
	public String getChequeId() {
		return chequeId;
	}
    
	/**
	 * @param chequeId
	 */
	public void setChequeId(String chequeId) {
		this.chequeId = chequeId;
	}
    
	/**
	 * @return
	 */
	public String getStreetNo() {
		return streetNo;
	}
    
	/**
	 * @param streetNo
	 */
	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	/**
	 * @return
	 */
	public boolean getFeeCancel() {
		return feeCancel;
	}

	/**
	 * @param feeCancel
	 */
	public void setFeeCancel(boolean feeCancel) {
		this.feeCancel = feeCancel;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
	    return ls + "Cheque" + ls
	        + super.toString() + ls
	        + "id: " + id + ls
	        + "name: " + name + ls
	        + "street: " + street + ls
	        + "streetNo: " + streetNo + ls
	        + "cityId: " + cityId + ls
	        + "cityName: " + cityName + ls
	        + "zipCode: " + zipCode + ls
	        + "chequeId: " + chequeId + ls
	        + "feeCancel: " + feeCancel + ls;
	}
}