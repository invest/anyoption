package com.anyoption.common.bl_vos;

import java.util.Date;

/**
 * @author Lior
 *
 */
public class CDPayDeposit implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private long transactionId;
	private long cdpayTransactionId;
	private long amount;
	private String currencySymbol;
	private String bankcode;
	private String bankTransactionId;
	private String status;
	private String statusMessage;
	private String billingdescriptor;
	private String control;
	
	private Date timeCreated;

	public CDPayDeposit() {
		id = 0;
		transactionId = 0;
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the cdpayTransactionId
	 */
	public long getCdpayTransactionId() {
		return cdpayTransactionId;
	}

	/**
	 * @param cdpayTransactionId the cdpayTransactionId to set
	 */
	public void setCdpayTransactionId(long cdpayTransactionId) {
		this.cdpayTransactionId = cdpayTransactionId;
	}

	/**
	 * @return the bankcode
	 */
	public String getBankcode() {
		return bankcode;
	}

	/**
	 * @param bankcode the bankcode to set
	 */
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

	/**
	 * @return the bankTransactionId
	 */
	public String getBankTransactionId() {
		return bankTransactionId;
	}

	/**
	 * @param bankTransactionId the bankTransactionId to set
	 */
	public void setBankTransactionId(String bankTransactionId) {
		this.bankTransactionId = bankTransactionId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * @param statusMessage the statusMessage to set
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	/**
	 * @return the billingdescriptor
	 */
	public String getBillingdescriptor() {
		return billingdescriptor;
	}

	/**
	 * @param billingdescriptor the billingdescriptor to set
	 */
	public void setBillingdescriptor(String billingdescriptor) {
		this.billingdescriptor = billingdescriptor;
	}

	/**
	 * @return the control
	 */
	public String getControl() {
		return control;
	}

	/**
	 * @param control the control to set
	 */
	public void setControl(String control) {
		this.control = control;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the currencySymbol
	 */
	public String getCurrencySymbol() {
		return currencySymbol;
	}

	/**
	 * @param currencySymbol the currencySymbol to set
	 */
	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}
	
	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	@Override
	public String toString()
	{
	    final String ls = " \n ";
	    String retValue = "";
	    retValue = "CDPayDeposit ( "
	        + super.toString() + ls
	        + "id = " + this.id + ls
	        + "transactionId = " + this.transactionId + ls 	        
	        + "cdpayTransactionId = " + this.cdpayTransactionId + ls
	        + "bankcode = " + this.bankcode + ls
	        + "bankTransactionId = " + this.bankTransactionId + ls
	        + "status = " + this.status + ls
	        + "statusMessage = " + this.statusMessage + ls
	        + "billingdescriptor = " + this.billingdescriptor + ls
	        + "control = " + this.control + ls
	        + "timeCreated = " + this.timeCreated + ls
	        + "currencySymbol = " + this.currencySymbol + ls 
	        + " )";
	    return retValue;
	}
}
