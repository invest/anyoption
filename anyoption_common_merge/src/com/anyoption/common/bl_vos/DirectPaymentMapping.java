/**
 *
 */
package com.anyoption.common.bl_vos;

import java.io.Serializable;

/**
 * Direct payment types mapping class
 *
 * @author Kobi
 *
 */
public class DirectPaymentMapping implements Serializable {

	private static final long serialVersionUID = 4288981519590658989L;

	private long id;
	private long countryId;
	private long currencyId;
	private long paymentId;



public DirectPaymentMapping() {

}

public DirectPaymentMapping(long id, long countryId, long currenctId, long paymentId) {
	this.id = id;
	this.countryId = countryId;
	this.currencyId = currenctId;
	this.paymentId = paymentId;
}

/**
 * @return the countryId
 */
public long getCountryId() {
	return countryId;
}

/**
 * @param countryId the countryId to set
 */
public void setCountryId(long countryId) {
	this.countryId = countryId;
}

/**
 * @return the currencyId
 */
public long getCurrencyId() {
	return currencyId;
}

/**
 * @param currencyId the currencyId to set
 */
public void setCurrencyId(long currencyId) {
	this.currencyId = currencyId;
}

/**
 * @return the id
 */
public long getId() {
	return id;
}

/**
 * @param id the id to set
 */
public void setId(long id) {
	this.id = id;
}

/**
 * @return the paymentId
 */
public long getPaymentId() {
	return paymentId;
}

/**
 * @param paymentId the paymentId to set
 */
public void setPaymentId(long paymentId) {
	this.paymentId = paymentId;
}


}


