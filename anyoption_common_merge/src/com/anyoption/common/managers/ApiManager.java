package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.ApiUser;
import com.anyoption.common.beans.ApiUserService;
import com.anyoption.common.beans.base.ApiErrorCode;
import com.anyoption.common.beans.base.ApiPage;
import com.anyoption.common.beans.base.ApiUserActivity;
import com.anyoption.common.beans.base.FTDUser;
import com.anyoption.common.daos.ApiDAOBase;

/**
 * @author EranL
 *
 */
public class ApiManager extends BaseBLManager {	
	private static final Logger log = Logger.getLogger(ApiManager.class);
	
    /**
     * Get API user by user name and password
     * @param userName
     * @param password
     * @throws SQLException
     */
    public static void getAPIUser(String userName, String password, ApiUser apiUser) throws SQLException {    	
    	Connection con = null;
        try {
        	con = getConnection();
        	ApiDAOBase.getAPIUser(con, userName, password, apiUser);
        } finally {
            closeConnection(con);
        }
    }
    
    /**
     * Get API error codes
     * @return
     * @throws SQLException
     */
    public static Hashtable<String, ApiErrorCode> getAPIErrorCodes() throws SQLException {    	
    	Connection con = null;
        try {
        	con = getConnection();
        	return ApiDAOBase.getAPIErrorCodes(con);
        } finally {
            closeConnection(con);
        }
    }
    
    /**
     * Insert into APIUserActivity table
     * @return
     * @throws SQLException
     */
    public static void insertAPIUserActivity(ApiUserActivity apiUserActivity) throws SQLException {    	
    	Connection con = null;
    	log.debug("Going to insert API USER ACTIVITY " + apiUserActivity);
        try {
        	con = getConnection();
        	ApiDAOBase.insertAPIUserActivity(con, apiUserActivity);
        } finally {
            closeConnection(con);
        }
    }

    /**
     * Insert into LOGIN_TOKEN table
     * @return
     * @throws SQLException
     */
    public static String insertLoginToken(long userId) throws SQLException {    	
    	Connection con = null;
    	log.debug("Going to insert LOGIN_TOKEN for userId:" + userId);
        try {
        	con = getConnection();
        	return ApiDAOBase.insertLoginToken(con, userId);
        } finally {
            closeConnection(con);
        }
    }
    
    /**
     * Get API pages - for login procedure 
     * @return
     * @throws SQLException
     */
    public static Hashtable<Long, ApiPage> getAPIPages() throws SQLException {    	
    	Connection con = null;
        try {
        	con = getConnection();
        	return ApiDAOBase.getApiPages(con);
        } finally {
            closeConnection(con);
        }
    }
    

    /**
     * Get API users group
     * @return
     * @throws SQLException
     */
    public static Hashtable<Long,  Hashtable<Long, Long>> getAPIUsersGroup() throws SQLException {    	
    	Connection con = null;
        try {
        	con = getConnection();
        	return ApiDAOBase.getAPIUsersGroup(con);
        } finally {
            closeConnection(con);
        }
    }

    /**

     * @return
     * @throws SQLException
     */
    public static void updateAPINotifications(long apiUserId ,String listOfId, String comment, long statusId) throws SQLException {    	
    	Connection con = null;
        try {
        	con = getConnection();
        	ApiDAOBase.updateAPINotifications(con, apiUserId , listOfId, comment, statusId);
        } finally {
            closeConnection(con);
        }
    }
    
    /**
     * Get first time deposit users by date and API user
     * @return
     * @throws SQLException
     */
    public static ArrayList<FTDUser> getFTDUsersByDate(long apiUserId, String dateRequest) throws SQLException {    	
    	Connection con = null;
        try {
        	con = getConnection();
        	return ApiDAOBase.getFTDUsersByDate(con, apiUserId, dateRequest);           
        } finally {
            closeConnection(con);
        }
    }
    
    /**
     * Check if affiliate key is valid for API
     * @param apiUserId
     * @param affKey
     * @return true if affiliate key is valid for API
     * @throws SQLException
     */
    public static boolean isAffKeyValidForApi(long apiUserId, long affKey) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return ApiDAOBase.isAffKeyValidForApi(con, apiUserId, affKey);
    	} finally {
    		closeConnection(con);
    	}
    }
    
    /**
     * Get service roles.
     * @return Hashtable<String, Hashtable<Long, String>>
     * @throws SQLException
     */
    public static Hashtable<String, Hashtable<Long, String>> getServiceRoles() throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return ApiDAOBase.getServiceRoles(con);
    	} finally {
    		closeConnection(con);
    	}
    }
    
    /**
     * Get API services
     * @return
     * @throws SQLException
     */
    public static Hashtable<String, ApiUserService> getUserServices() throws SQLException {
    	Connection con = null; 
    	try {
    		con = getConnection();
    		return ApiDAOBase.getUserServices(con);
    	} finally {
    		closeConnection(con);
    	}
    } 
    
}