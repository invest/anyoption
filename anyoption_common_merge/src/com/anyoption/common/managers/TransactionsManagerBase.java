/**
 * 
 */
package com.anyoption.common.managers;


import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.SocketTimeoutException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.AppsflyerEvent;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.EpgTransactionStatus;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.WithdrawFee;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.bl_vos.ACHDepositBase;
import com.anyoption.common.bl_vos.ChargeBack;
import com.anyoption.common.bl_vos.ClearingRoute;
import com.anyoption.common.bl_vos.TransactionBin;
import com.anyoption.common.bl_vos.UkashDeposit;
import com.anyoption.common.bl_vos.WebMoneyDeposit;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingManagerBase;
import com.anyoption.common.clearing.EPGClearingProvider;
import com.anyoption.common.daos.ChargeBacksDAO;
import com.anyoption.common.daos.CreditCardsDAOBase;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.daos.UkashDepositDAO;
import com.anyoption.common.daos.UsersDAOBase;
import com.anyoption.common.daos.WebMoneyDepositDAO;
import com.anyoption.common.daos.WiresDAOBase;
import com.anyoption.common.payments.CancelDeposit;
import com.anyoption.common.payments.CaptureWithdrawal;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawUtil;
import com.anyoption.common.payments.epg.EpgDepositRequestDetails;
import com.anyoption.common.payments.epg.EpgRequestDetails;
import com.anyoption.common.payments.epg.EpgWithdrawalRequestDetails;
import com.anyoption.common.service.gmwithdrawal.GmWithdrawalPermissionToDisplayRequest;
import com.anyoption.common.service.gmwithdrawal.GmWithdrawalPermissionToDisplayResult;
import com.anyoption.common.service.gmwithdrawal.GmWithdrawalRequest;
import com.anyoption.common.service.results.WithdrawBonusRegulationStateMethodResult.WithdrawBonusStateEnum;
import com.anyoption.common.service.userdocuments.UserDocumentsManager;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.copyop.common.dao.ACHDepositDAOBase;


/**
 * @author pavelhe
 *
 */
public class TransactionsManagerBase extends BaseBLManager {
	
	private static final Logger log = Logger.getLogger(TransactionsManagerBase.class);
	
	// transaction types
	public static final int TRANS_TYPE_ALL = 0;
	public static final int TRANS_TYPE_CC_DEPOSIT = 1;
	public static final int TRANS_TYPE_WIRE_DEPOSIT = 2;
	public static final int TRANS_TYPE_CHEQUE_WITHDRAW = 3;
	public static final int TRANS_TYPE_ADMIN_DEPOSIT = 4;
	public static final int TRANS_TYPE_ADMIN_WITHDRAW = 5;
	public static final int TRANS_TYPE_REVERSE_WITHDRAW = 6;
	public static final int TRANS_TYPE_INVESTMENT = 7;

	public static final int TRANS_TYPE_CASH_DEPOSIT = 9;
	public static final int TRANS_TYPE_CC_WITHDRAW = 10;
	
	public static final int TRANS_TYPE_BONUS_DEPOSIT = 12;
	public static final int TRANS_TYPE_BONUS_WITHDRAW = 13;
	public static final int TRANS_TYPE_BANK_WIRE_WITHDRAW = 14;

	public static final int TRANS_TYPE_INTERNAL_CREDIT = 16;
	public static final int TRANS_TYPE_DIRECT_BANK_DEPOSIT = 17;
	public static final int TRANS_TYPE_ENVOY_ONLINE_DEPOSIT = 18;
	public static final int TRANS_TYPE_ENVOY_WITHDRAW = 19;
	public static final int TRANS_TYPE_POINTS_TO_CASH = 20;
	public static final int TRANS_TYPE_ENVOY_BANKING_DEPOSIT = 21;
	public static final int TRANS_TYPE_FIX_BALANCE_DEPOSIT = 22;
	public static final int TRANS_TYPE_FIX_BALANCE_WITHDRAW = 23;
	public static final int TRANS_TYPE_DEPOSIT_BY_COMPANY = 24;
	public static final int TRANS_TYPE_PAYPAL_DEPOSIT = 25;
	public static final int TRANS_TYPE_PAYPAL_WITHDRAW = 26;

	public static final int TRANS_TYPE_ACH_DEPOSIT = 28;
	public static final int TRANS_TYPE_CASHU_DEPOSIT = 29;
	public static final int TRANS_TYPE_UKASH_DEPOSIT = 30;

	public static final int TRANS_TYPE_MONEYBOOKERS_DEPOSIT = 32;
	public static final int TRANS_TYPE_WEBMONEY_DEPOSIT = 33;
	public static final int TRANS_TYPE_EFT_DEPOSIT = 34;
	public static final int TRANS_TYPE_EFT_WITHDRAW = 35;
	public static final int TRANS_TYPE_MONEYBOOKERS_WITHDRAW = 36;
	public static final int TRANS_TYPE_WEBMONEY_WITHDRAW = 37;
	public static final int TRANS_TYPE_DELTAPAY_CHINA_DEPOSIT = 38;
	public static final int TRANS_TYPE_DELTAPAY_CHINA_WITHDRAW = 39;
	public static final int TRANS_TYPE_CUP_CHINA_DEPOSIT = 38;
	public static final int TRANS_TYPE_CUP_CHINA_WITHDRAW = 39;
	public static final int TRANS_TYPE_CC_DEPOSIT_REROUTE = 42;
	public static final int TRANS_TYPE_FOCAL_PAYMENTS_WITHDRAW = 41;
	public static final int TRANS_TYPE_BAROPAY_DEPOSIT = 44;
	public static final int TRANS_TYPE_BAROPAY_WITHDRAW = 45;
	public static final int TRANS_TYPE_CUP_INTERNAL_CREDIT = 46;
	public static final int TRANS_TYPE_MAINTENANCE_FEE = 47;
	public static final int TRANS_TYPE_MAINTENANCE_FEE_CANCEL = 52;
	// These constants conflicts with the maintenance fee deployment
//	public static final int TRANS_TYPE_NOVAPAY_DEPOSIT = 47;
//	public static final int TRANS_TYPE_NOVAPAY_WITHDRAW = 48;
	public static final int TRANS_TYPE_FIX_NEGATIVE_BALANCE = 49;
	public static final String NEGATIVE_BALANCE_FIX_COMMENT = "Automatic fix balance deposit due to negative balance";

	public static final int TRANS_TYPE_COPYOP_COINS_TO_CASH = 53;
	public static final int TRANS_TYPE_INATEC_IFRAME_DEPOSIT = 54;
	public static final int TRANS_TYPE_DIRECT24_DEPOSIT = -172;
	public static final int TRANS_TYPE_GIROPAY_DEPOSIT = -173;
	public static final int TRANS_TYPE_EPS_DEPOSIT = -174;

	public static final int TRANS_TYPE_EPG_CHECKOUT_DEPOSIT = 58;

	// new fee - homogenous
	public static final int TRANS_TYPE_HOMO_FEE = 62;
	

	public static final int TRANS_TYPE_NETELLER_DEPOSIT = 64;
	
	// not in db:
	public static final int TRANS_TYPE_ALL_DEPOSIT = 90;
	public static final int TRANS_TYPE_ALL_WITHDRAW = 91;

	// Fee types are Deprecated - kept for historical reference purposes
	//public static final int TRANS_TYPE_WITHDRAW_FEE = 8;	
	//public static final int TRANS_TYPE_CC_WITHDRAW_FEE = 11;
	//public static final int TRANS_TYPE_BANK_WIRE_FEE = 15;
	//public static final int TRANS_TYPE_PAYPAL_WITHDRAW_FEE = 27;
	//public static final int TRANS_TYPE_ENVOY_WITHDRAW_FEE = 31;
	//public static final int TRANS_TYPE_MONEYBOOKERS_WITHDRAW_FEE = 50;
	//public static final int TRANS_TYPE_WEBMONEY_WITHDRAW_FEE = 51;
	//public static final int TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE = 55;

	//public static final float TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE_COEFFICIENT = .25f;

	// transaction statuses
	public static final int TRANS_STATUS_STARTED = 1;
	public static final int TRANS_STATUS_SUCCEED = 2;
	public static final int TRANS_STATUS_FAILED = 3;
	public static final int TRANS_STATUS_REQUESTED = 4;
	public static final int TRANS_STATUS_CANCELED = 5; // reverse withdrawal by admin / accounting
	public static final int TRANS_STATUS_REVERSE_WITHDRAW = 6; // reverse withdrawal by user /support / Job
	public static final int TRANS_STATUS_PENDING = 7;
	public static final int TRANS_STATUS_CANCELED_ETRADER = 8; // canceling of deposit by Admin
	public static final int TRANS_STATUS_APPROVED = 9;
	public static final int TRANS_STATUS_AUTHENTICATE = 10;
	public static final int TRANS_STATUS_IN_PROGRESS = 11;
	public static final int TRANS_STATUS_CANCELED_FRAUDS = 12; // canceling of deposit by Accounting
	public static final int TRANS_STATUS_CANCEL_S_DEPOSIT = 13; // canceling of deposit after succeed
	public static final int TRANS_STATUS_CHARGE_BACK = 14;
	public static final int TRANS_STATUS_CANCEL_S_M = 15; // canceling of deposit due to support mistake
	public static final int TRANS_STATUS_CANCEL_M_N_R = 16; // canceling of deposit - money not reviced
	public static final int TRANS_STATUS_SECOND_APPROVAL = 17; // second approval status by accounting
	public static final int TRANS_STATUS_PANDING_AND_SUCCESS = 27;

	public static final String SUCCESS_DEPOSITS_TYPES = TransactionsManagerBase.TRANS_STATUS_SUCCEED
															+ ","
															+ TransactionsManagerBase.TRANS_STATUS_PENDING
															+ ","
															+ TransactionsManagerBase.TRANS_STATUS_CANCELED_ETRADER;

	// charge back statuses
	public static long CHARGE_BACK_STATUS_STARTED = 1;
	public static long CHARGE_BACK_STATUS_INPROCESS = 2;
	public static long CHARGE_BACK_STATUS_SETTLED = 3;
	public static long CHARGE_BACK_STATUS_CLOSED = 4;

	// transaction type class
	public static final int TRANS_CLASS_DEPOSIT = 1;
	public static final int TRANS_CLASS_WITHDRAW = 2;
	public static final int TRANS_CLASS_INTERNALS = 3;
	public static final int TRANS_CLASS_FEES = 4;
	public static final int TRANS_CLASS_ADMIN_DEPOSITS = 5;
	public static final int TRANS_CLASS_ADMIN_WITHDRAWALS = 6;
	public static final int TRANS_CLASS_BONUS_DEPOSITS = 7;
	public static final int TRANS_CLASS_BONUS_WITHDRAWALS = 8;
	public static final int TRANS_CLASS_REVERSE_WITHDRAWALS = 9;
	public static final int TRANS_CLASS_FIX_BALANCE = 10;
	public static final int TRANS_CLASS_DEPOSIT_BY_COMPANY = 11;

	// Credit Cards Types
	public static final int CC_TYPE_MASTERCARD = 1;
	public static final int CC_TYPE_VISA = 2;
	public static final int CC_TYPE_DINERS = 3;
	public static final int CC_TYPE_ISRACARD = 4;
	public static final int CC_TYPE_AMEX = 5;
	public static final int CC_TYPE_MAESTRO = 6;
	public static final int CC_TYPE_CUP = 7;

	// Payment method types
	public static final long PAYMENT_TYPE_CC = 1;
	public static final long PAYMENT_TYPE_DIRECT24 = 2;
	public static final long PAYMENT_TYPE_GIROPAY = 3;
	public static final long PAYMENT_TYPE_EPS = 4;
	public static final long PAYMENT_TYPE_ENV_SPAIN_BANK = 5;
	public static final long PAYMENT_TYPE_ENV_MEXICO_BANK = 6;
	public static final long PAYMENT_TYPE_ENVOY = 7;
	public static final long PAYMENT_TYPE_ACH = 8;
	public static final long PAYMENT_TYPE_CASHU = 9;
	public static final long PAYMENT_TYPE_WIRE = 10;
	public static final long PAYMENT_TYPE_PAYPAL = 11;
	public static final long PAYMENT_TYPE_TELEINGRESO = 12;
	public static final long PAYMENT_TYPE_POLI2 = 13;
	public static final long PAYMENT_TYPE_SANTANDER = 14;
	public static final long PAYMENT_TYPE_IDEAL = 15;
	public static final long PAYMENT_TYPE_MONETA = 16;
	public static final long PAYMENT_TYPE_UKASH = 17;
	public static final long PAYMENT_TYPE_WEB_MONEY = 26;
	public static final long PAYMENT_TYPE_DINERO_MAIL = 37;
	public static final long PAYMENT_TYPE_MONETA_WITHDRAWAL = 38;
	public static final long PAYMENT_TYPE_WEB_MONEY_WITHDRAWAL = 39;
	public static final long PAYMENT_TYPE_LOBANET = 47;
	public static final long PAYMENT_TYPE_MONEYBOOKERS = 53;
	public static final long PAYMENT_TYPE_DELTAPAY_CHINA = 62;
	public static final long PAYMENT_TYPE_BAROPAY = 71;
	public static final long PAYMENT_TYPE_INATEC_IDEAL = 86;
	public static final long PAYMENT_TYPE_NETELLER = 87;
	
	public static final long TRANS_TYPE_DIRECT24_WITHDRAW = 59;
	public static final long TRANS_TYPE_GIROPAY_WITHDRAW = 60;
	public static final long TRANS_TYPE_EPS_WITHDRAW = 61;
	public static final long TRANS_TYPE_IDEAL_WITHDRAW = 63;
	
	public static final int PAYMENT_GROUP_EPG = 40;

	public static final long TRANSACTION_CLASS_TYPE_REAL_DEPOSIT = 1;
	public static final long TRANSACTION_CLASS_TYPE_REAL_WITHDRAWALS = 2; 

	public static final String TRANS_TYPS_ALL_DEPOSITS_TXT = 
			String.valueOf(TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_ENVOY_BANKING_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_ENVOY_ONLINE_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_EFT_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_ADMIN_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_PAYPAL_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_ACH_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_CASHU_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_UKASH_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_BAROPAY_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_WEBMONEY_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_CASH_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_INATEC_IFRAME_DEPOSIT)
				+ ","
				+ String.valueOf(TransactionsManagerBase.TRANS_TYPE_EPG_CHECKOUT_DEPOSIT);

	public static final long CONFIG_ID_PARAMETER_X = 1;
	public static final long CONFIG_ID_PARAMETER_Y = 2;
	
	public static final long FIRST_REAL_SUCCESS_DEPOSIT = 1;

	public enum transactionUpdateStatus {
		TRANS_UPDATED, TRANS_BALANCE_UPDATED, TRANS_UPDATED_BALANCE_FAILED, TRANS_FAILED;
	}

	public enum TransactionSource {
		TRANS_FROM_WEB, TRANS_FROM_MOBILE, TRANS_FROM_BACKEND, TRANS_FROM_JOB;
	}

	/**
	 * This specific first deposit check is used for additional fields of funnel
	 * deposits. We have to make sure that the user should be able to change
	 * his/hers currency only once.
	 * 
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static boolean isFirstPossibleDeposit(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.isFirstPossibleDeposit(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * This specific first real deposit check is used for additional fields of funnel
	 * deposits. We have to make sure that the user should be able to change
	 * his/hers DOB and address only once.
	 * 
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static boolean isFirstPossibleDepositExcludeBonus(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.isFirstPossibleDepositExcludeBonus(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static long getTransactionCountByType(long userId, long transType) {
		Connection con = null;
		try {
			con = getConnection();
			return TransactionsDAOBase.getTransactionCountByType(con, userId, transType);
		} catch (SQLException e) {
			log.error(e);
			return 0;
		} finally {
			closeConnection(con);
		}
	}

	/**
     * Is first succeed deposit
     * @param userId user id
     * @return
     * @throws SQLException
     */
	public static boolean isFirstDeposit(long userId, long expectedValue) throws SQLException {
		Connection con = getConnection();
		try {
			return isFirstDeposit(con, userId, expectedValue);
		} finally {
			closeConnection(con);
		}
	}

	public static boolean isFirstDeposit(Connection con, long userId, long expectedValue) throws SQLException {
 		return TransactionsDAOBase.isFirstDeposit(con, userId, expectedValue);
 	}

	/**
	 * Method called after every deposit method, so all deposit methods have one common exit point
	 * 
	 * @param user
	 * @param transaction
	 * @param source The transaction origin
	 * @throws SQLException 
	 */
	public static void afterDepositAction(User user, Transaction transaction, TransactionSource source) throws SQLException {
		// refresh user to make sure we have latest user data
		UsersManagerBase.loadUserByName(user.getUserName(), user);
		if (user.getFirstDepositId() == transaction.getId()
				&& (source.equals(TransactionSource.TRANS_FROM_WEB) || source.equals(TransactionSource.TRANS_FROM_MOBILE))
				&& transaction.getId() != 0) {
			UserBalanceStepManager.loadUserBalanceStep(user, true, transaction.getWriterId());

		}		
		//REMOVE AFTER SUCC If need create POP empty Files 
		//popDocumentsAction(user, transaction);
		
		try {
			//AO Regulation Check Limits Due Documents after Succ. Deposit		
			if(SkinsManagerBase.getSkin(user.getSkinId()).isRegulated() && user.getSkinId() != Skin.SKIN_ETRADER 
					 && (transaction.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED 
					 		|| transaction.getStatusId() == TransactionsManagerBase.TRANS_STATUS_PENDING)){
				UserRegulationBase ur = new UserRegulationBase();
				ur.setUserId(user.getId());
				UserRegulationManager.getUserRegulation(ur);
				UserRegulationManager.checkUserDueDocumentsDepLimit(ur);
			}
		} catch (Exception e) {
			log.error("Regulation AO:Can't check limits due documents!", e);
		}
	}
	
	private static void popDocumentsAction(User user, Transaction transaction){
		try { 
			if(transaction.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED  
					 	|| transaction.getStatusId() == TransactionsManagerBase.TRANS_STATUS_PENDING){
				//1.If succeed first CC deposit create empty front files
				if(transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT
					&& isFirstDepositWithCC(user.getId(), transaction.getCreditCardId().longValue())){
					UserDocumentsManager.insertEmptyNoExistFile(user.getId(), FileType.CC_COPY_FRONT, transaction.getCreditCardId().longValue());
				//2. Other Payment Types
				} else {
					ArrayList<Long> needPOPDoc = UserDocumentsManager.getPopDocsNeededByTransaction(transaction);
					if(needPOPDoc != null && needPOPDoc.size() > 0){
						HashMap<Long, Long> userExistPOPDocs = UserDocumentsManager.getUserPOPDocuments(transaction.getUserId());
						for (Long newFileTypeId : needPOPDoc) {
							//Check if not exist create new empty POP file
							if(!userExistPOPDocs.containsKey(newFileTypeId)){
								UserDocumentsManager.insertEmptyFile(user.getId(), newFileTypeId);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("When popDocumentsAction ", e);
		}
	}
	
	protected static void popDocumentsActionCreate(long userId, Transaction tr){
		try {
			User user = UsersManagerBase.getUserById(userId);
			if(SkinsManagerBase.getSkin(user.getSkinId()).isRegulated()){
				popDocumentsAction(user, tr);
			}
		} catch (Exception e) {
			log.debug("Cannot popDocumentsActionCreate", e);
		}
	}
	
	public static boolean isPastDeposit(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.isPastDeposit(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Count number of real deposit
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static int countRealDeposit(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.countRealDeposit(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * check if this first deposit transaction is from user that have appsflyerId
	 * and he was registered in mobile/copyop mobile
	 * and this transaction is from BE or WEB
	 * @param transactionId
	 * @return AppsflyerEvent
	 * @throws SQLException
	 */
	public static AppsflyerEvent checkAppsflyerFirstDeposit(long transactionId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.checkAppsflyerFirstDeposit(con, transactionId);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static boolean isHasOpenWithdraw(long userId) throws SQLException {
		Connection connection = getConnection();
		try {
			return TransactionsDAOBase.isHasOpenWithdraw(connection, userId);
		} finally {
			closeConnection(connection);
		}
	}
	
	public static void updateVisibleByUserId(long userId, long cardId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			TransactionsDAOBase.updateVisibleByUserId(con, userId, cardId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Check if user have monthly withdrawals transactions
	 * 
	 * @param userId user id - for checking his withdrawals
	 * @return true - in case the user did monthly withdrawals
	 * @throws SQLException
	 */
	public static boolean hasMonthlyWithdrawals(long userId, Date timeCreated, String utcOffset) throws SQLException {
		return hasMonthlyWithdrawals(userId, timeCreated, utcOffset, 0l);
	}

	public static boolean hasMonthlyWithdrawals(long userId, Date timeCreated, String utcOffset, long countForCheck) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return hasMonthlyWithdrawals(con, userId, timeCreated, utcOffset, countForCheck);
		} finally {
			closeConnection(con);
		}
	}

	public static boolean hasMonthlyWithdrawals(Connection con, long userId, Date timeCreated, String utcOffset,
												long countForCheck) throws SQLException {
		return TransactionsDAOBase.getMonthlyWithdrawalsCount(con, userId, timeCreated, utcOffset) > countForCheck;
	}
	@Deprecated
	public static boolean shouldChargeForLowWithdraw(	long userId, long amountForLowWithdraw, long transactionAmount,
														Date transactionTimeCreated,
														String transactionUtcOffsetCreated) throws SQLException {
		return shouldChargeForLowWithdraw(	userId, amountForLowWithdraw, transactionAmount, transactionTimeCreated,
											transactionUtcOffsetCreated, 0l);
	}
	@Deprecated
	public static boolean shouldChargeForLowWithdraw(	long userId, long amountForLowWithdraw, long transactionAmount,
														Date transactionTimeCreated, String transactionUtcOffsetCreated,
														long countForCheck) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return shouldChargeForLowWithdraw(	con, userId, amountForLowWithdraw, transactionAmount, transactionTimeCreated,
												transactionUtcOffsetCreated, countForCheck);
		} finally {
			closeConnection(con);
		}
	}
	@Deprecated
	public static boolean shouldChargeForLowWithdraw(	Connection con, long userId, long amountForLowWithdraw, long transactionAmount,
														Date transactionTimeCreated, String transactionUtcOffsetCreated,
														long countForCheck) throws SQLException {
		if (!hasMonthlyWithdrawals(con, userId, transactionTimeCreated, transactionUtcOffsetCreated, countForCheck)) {
			if (transactionAmount < amountForLowWithdraw) {
				return TransactionsDAOBase.shouldChargeForLowWithdraw(con, userId);
			}
		}
		return false;
	}

	public static ArrayList<Transaction> getRelatedSplittedTran(Transaction vo) throws SQLException{
		Connection con = getConnection();
		try{
			return TransactionsDAOBase.getRelatedSplittedTran(con, vo);
		} finally {
			closeConnection(con);
		}
	}
	
	public static CreditCard getCreditCard(BigDecimal id) throws SQLException {
		Connection con = getConnection();
		try {
			return CreditCardsDAOBase.getById(con, id.longValue());
		} finally {
			closeConnection(con);
		}
	}
	
	public static ACHDepositBase getAch(long id) throws SQLException {
		Connection con = getConnection();
		try {
			return ACHDepositDAOBase.get(con, id);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ChargeBack getChargeBack(BigDecimal id) throws SQLException {
		Connection con = getConnection();
		try {
			return ChargeBacksDAO.get(con, id.longValue());
		} finally {
			closeConnection(con);
		}
	}
	
	public static WireBase getWire(BigDecimal id) throws SQLException {
		Connection con = getConnection();
		try {
			return WiresDAOBase.get(con, id.longValue());
		} finally {
			closeConnection(con);
		}
	}
	
	public static WebMoneyDeposit getWebMoneyDeposit(long tranId) throws SQLException {
		Connection con = getConnection();
		try {
			return WebMoneyDepositDAO.get(con, tranId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updatePixelRunTime(long id) throws SQLException {
		Connection con = getConnection();
		try {
			TransactionsDAOBase.updatePixelRunTime(con, id);
		} finally {
			closeConnection(con);
		}
	}
	
	public static boolean isMaintenanceFeeCanceled(long trnId) throws SQLException {
	    Connection con = getConnection();
	    try {
	    	return TransactionsDAOBase.maintenanceFeeCanceled(con, trnId);
	    } finally {
	    	closeConnection(con);	
		}
	    
	}
	
	public static UkashDeposit getUkash(long id) throws SQLException {
		Connection con = getConnection();
		try {
			return UkashDepositDAO.get(con, id);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<Long> getTransactionsReroutConnectedIDById(long transactionId) throws SQLException {
		Connection conn = getConnection();
        try {
            return TransactionsDAOBase.getTransactionsReroutConnectedIDById(conn, transactionId);
        } finally {
            closeConnection(conn);
        }
	}
	
	public static Transaction createBonusTransaction(long userId, long amount, long bonusUsersId, long writerId, String userUtcOffset, long loginId) {
        Transaction t = new Transaction();
        t.setUserId(userId);
        t.setAmount(amount);
        t.setTypeId(TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT);
        t.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
        t.setComments("bonus activated id: " + bonusUsersId);
        t.setDescription("log.balance.bonus.deposit");
        t.setIp("IP NOT FOUND!");
        t.setTimeSettled(new Date());
        t.setTimeCreated(new Date());
        t.setUtcOffsetCreated(userUtcOffset);
        t.setUtcOffsetSettled(userUtcOffset);
        t.setWriterId(writerId);
        t.setBonusUserId(bonusUsersId);
        t.setLoginId(loginId);
        return t;
    }
	
    /**
     * Insert bonus withdrawal to user
     * this function operated after cancel bonus to user
     * @param amount bonus deposit amount that should withdraw now
     * @param userId the user that want to withdraw with
     * @param utcOffset utcOffset of the user
     * @param writerId writer that operate this action
     * @param bonusUserId bonusUserId reference
     * @return
     * @throws SQLException
     */	public static boolean insertBonusWithdraw(Connection con, long amount, long userId, String utcOffset, long writerId, long bonusUserId, String ip, long loginId) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Insert Bonus Withdraw: " + ls + "userId: "
					+ userId + ls);
		}

		try {
			Transaction tran = new Transaction();
			tran.setAmount(amount);
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setDescription("log.balance.bonus.withdraw");
			tran.setIp(ip);
			tran.setProcessedWriterId(writerId);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(utcOffset);
			tran.setUtcOffsetSettled(utcOffset);
			tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW);
			tran.setUserId(userId);
			tran.setWriterId(writerId);
			tran.setStatusId(TRANS_STATUS_SUCCEED);
			tran.setChequeId(null);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);
			tran.setBonusUserId(bonusUserId);
			tran.setLoginId(loginId);

			long negFixTransactionId = UsersManagerBase.addToBalanceNonNeg(	con, tran.getUserId(), -tran.getAmount(), tran.getRate(),
																			utcOffset);
			TransactionsDAOBase.insert(con, tran);
			GeneralDAO.insertBalanceLog(con, writerId,
					tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran
							.getId(), ConstantsBase.LOG_BALANCE_BONUS_WITHDRAW, utcOffset);
			if (negFixTransactionId > 0) {
				GeneralDAO.insertBalanceLog(con, writerId, tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(),
											ConstantsBase.LOG_BALANCE_FIX_NEGATIVE_BALANCE_DEPOSIT, utcOffset);
			}
			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, "Bonus Withdraw insert successfully! " + ls
						+ tran.toString());
			}
		} catch (SQLException e) {
			log.log(Level.ERROR, "insertBonusWithdraw: ", e);
			throw e;
		}
		return true;
	}
 	public static boolean insertBonusDepositBase(User user, long bonusUserId,
			long writerId, long amount, String comments, String ip, Connection con, long loginId) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Insert Bonus deposit: " + ls + "user:"
				+ user.getUserName() + ls );
		}
		
		try {
			Transaction tran = new Transaction();
			tran.setAmount(amount);
			tran.setCurrency(new Currency(user.getCurrency()));
			tran.setComments(comments);
			tran.setCreditCardId(null);
			tran.setDescription("log.balance.bonus.deposit");
			tran.setIp(ip);
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setUtcOffsetSettled(user.getUtcOffset());
			tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_SUCCEED);
			tran.setReceiptNum(null);
			tran.setBonusUserId(bonusUserId);
			tran.setLoginId(loginId);
			
			try {
				con.setAutoCommit(false);
				UsersDAOBase.addToBalance(con, user.getId(), amount);
				TransactionsDAOBase.insert(con, tran);
				GeneralDAO.insertBalanceLog(con, writerId,
						tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran
								.getId(), ConstantsBase.LOG_BALANCE_BONUS_DEPOSIT, user.getUtcOffset());
				con.commit();
			} catch (SQLException e) {
				log.log(Level.ERROR, "insertBonusDeposit: ", e);
				try {
					con.rollback();
				} catch (SQLException ie) {
					log.log(Level.ERROR, "Can't rollback.", ie);
				}
				throw e;
			} finally {
				try {
					con.setAutoCommit(true);
				} catch (Exception e) {
					log.error("Can't set back to autocommit.", e);
				}
			}
			
			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO, "Bonus deposit inserted successfully!");
			}
		
		} catch (SQLException e) {
			log.log(Level.ERROR, "insertBonusDeposit: ", e);
			throw e;
		}
		return true;
	}

	// TODO change insert deposit logic (for all types).
	/**
	 * Insert deposit method for new transaction type: fix negative balance. Method precondition: connection with auto commit false (ensures
	 * atomic actions)
	 * 
	 * @param conn
	 * @param depositVal
	 * @param writerId
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	public static void insertDepositToFixNegativeBalance(	Connection conn, long depositVal, long writerId, User user,
															Long bonusUserId, long loginId, String ip) throws SQLException {
		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls	+ "Insert deposit to fix balance:" + ls + "amount (small coins): " + depositVal + ls + "writer: " + writerId + ls
						+ "userId: " + user.getId() + ls);
		}

		Transaction tran = null;

		try {
			tran = new Transaction();
			tran.setAmount(depositVal);
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(ip);
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_FIX_NEGATIVE_BALANCE);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_SUCCEED);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setUtcOffsetSettled(null);
			tran.setBonusUserId(bonusUserId);
			tran.setDescription("log.balance.fix.negative.balance.deposit");
			tran.setComments(null);// TODO which comment?
			tran.setReferenceId(refNegTransaction(conn, bonusUserId));
			tran.setLoginId(loginId);

			UsersDAOBase.addToBalance(conn, user.getId(), depositVal);
			TransactionsDAOBase.insert(conn, tran);
			GeneralDAO.insertBalanceLog(conn, writerId, user.getId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(),
										ConstantsBase.LOG_BALANCE_FIX_NEGATIVE_BALANCE_DEPOSIT, user.getUtcOffset());

			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO, "Success, fix negative balance");
			}

		} catch (SQLException e) {
			log.log(Level.ERROR, "insertDepositToFixNegativeBalance: ", e);
			throw e;
		}
	}

	/**
	 * @param conn
	 * @param bonusUserId
	 * @return
	 * @throws SQLException 
	 */
	private static BigDecimal refNegTransaction(Connection conn, Long bonusUserId) throws SQLException {			
		try {
			return TransactionsDAOBase.refNegTransaction(conn, bonusUserId);
		} catch (SQLException e) {
			log.log(Level.ERROR, "in refNegTransaction", e);
			throw e;
		}
	}
	
	public static Transaction getTransactionInfo(long transactionId) throws SQLException {
		Connection conn = getConnection();
        try {
            return TransactionsDAOBase.getFTDInfoById(conn, transactionId);
        } finally {
            closeConnection(conn);
        }
	}
	
	/**
	 * Assumption - this should be atomic operation.
	 * 
	 * @param conn
	 * @param cancelDeposit
	 * @throws SQLException
	 */
	public static void cancelDeposit(Connection conn, CancelDeposit cancelDeposit) throws SQLException {
		log.info("start cancelDeposit");
		log.info(cancelDeposit.toString());
		TransactionsDAOBase.updateStatusId(conn, cancelDeposit.getClearingInfo().getTransactionId(), cancelDeposit.getStatus(), true, cancelDeposit.getWriterId());
		UsersDAOBase.addToBalance(conn, cancelDeposit.getClearingInfo().getUserId(), -(cancelDeposit.getClearingInfo().getAmount()));
		GeneralDAO.insertBalanceLog(conn, cancelDeposit.getWriterId(), cancelDeposit.getClearingInfo().getUserId(), ConstantsBase.TABLE_TRANSACTIONS, 
				cancelDeposit.getClearingInfo().getTransactionId(), ConstantsBase.LOG_BALANCE_CANCEL_DEPOSIT, cancelDeposit.getUserUtcOffset());
		log.info("end cancelDeposit");
	}
	
	/**
	 * @param cancelDeposit
	 * @throws SQLException
	 */
	public static void cancelDeposit(CancelDeposit cancelDeposit) throws SQLException {
		Connection connection = getConnection();
        try {
        	cancelDeposit(connection, cancelDeposit);
        } finally {
            closeConnection(connection);
        }
	}

	/**
	 * @param clearingInfo
	 * @param userUtcOffset
	 * @throws SQLException
	 */
	public static void updateTransactionCapture(ClearingInfo clearingInfo, String userUtcOffset) throws SQLException {
		Connection connection = getConnection();
        try {
        	TransactionsDAOBase.updateTransactionCapture(connection, clearingInfo, userUtcOffset);
        } finally {
        	closeConnection(connection);
        }		
	}
	
	/**
	 * @param info
	 * @return
	 * @throws SQLException
	 */
	public static EpgRequestDetails getEpgRequestAdditionalDetails(ClearingInfo info) throws SQLException {
		Connection connection = getConnection();
        try {
        	return TransactionsDAOBase.getEpgRequestAdditionalDetails(connection, info);
        } finally {
        	closeConnection(connection);
        }		
	}
	
	/**
	 * @param info
	 * @return
	 * @throws SQLException
	 */
	public static EpgDepositRequestDetails getEpgDepositRequestAdditionalDetails(ClearingInfo info) throws SQLException {
		Connection connection = getConnection();
        try {
        	return TransactionsDAOBase.getEpgDepositRequestAdditionalDetails(connection, info);
        } finally {
        	closeConnection(connection);
        }		
	}
	
	/**
	 * @param info
	 * @return
	 * @throws SQLException
	 */
	public static EpgWithdrawalRequestDetails getEpgWithdrawalRequestAdditionalDetails(ClearingInfo info) throws SQLException {
		Connection connection = getConnection();
        try {
        	return TransactionsDAOBase.getEpgWithdrawalRequestAdditionalDetails(connection, info);
        } finally {
        	closeConnection(connection);
        }		
	}
	
 	public static Transaction getTransaction(long id) throws SQLException {
		Connection con = getConnection();
		try {

			return TransactionsDAOBase.getById(con, id);
		} finally {
			closeConnection(con);
		}
	}
 	
 	/**
 	 * Update transaction after capture withdrawal
 	 * @param captureWithdrawal
 	 * @throws SQLException
 	 */
 	public static void updateTransactionAfterCaptureWithdrawal(CaptureWithdrawal captureWithdrawal) throws SQLException {
 		Connection con = getConnection();
		try {
			updateTransactionAfterCaptureWithdrawal(con, captureWithdrawal);
		} finally {
			closeConnection(con);
		}
 	}
 	
 	/**
 	 * Update transaction after capture withdrawal
 	 * @param captureWithdrawal
 	 * @throws SQLException
 	 */
 	public static void updateTransactionAfterCaptureWithdrawal(Connection connection, CaptureWithdrawal captureWithdrawal) throws SQLException {
 		log.info("start updateTransactionAfterCaptureWithdrawal with:\n"
 				+ captureWithdrawal.toString());
		TransactionsDAOBase.updateTransactionAfterCaptureWithdrawal(connection, captureWithdrawal);
		WithdrawUtil.finalWithdrawEntry(new WithdrawEntryInfo(captureWithdrawal.getUser(), captureWithdrawal.getTransactionId()));
		log.info("end updateTransactionAfterCaptureWithdrawal");
	}
 	
	public static boolean isDisableCcDeposits(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.isDisableCcDeposits(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateDisableCcDeposits(boolean isDisableCcDeposits, long userId) throws SQLException {
		Connection conn = getConnection();
        try {
        	TransactionsDAOBase.updateDisableCcDeposits(conn, isDisableCcDeposits, userId);
        } finally {
        	closeConnection(conn);
        }		
	}

	/**
	 * Get first transaction id
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static long getFirstTrxId(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getFirstTrxId(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static void insertWithdrawCancel(long userId, long transactionTypeId, long ccId, long writerId, String utcOffset, WithdrawBonusStateEnum state, String ip) {
		Connection con = null;
		try {
			con = getConnection();
			Transaction t = new Transaction();
			Date now = new Date();
			t.setUserId(userId);
			if (transactionTypeId <= 0) {
				transactionTypeId = TRANS_TYPE_CC_WITHDRAW;
			}
			t.setTypeId(transactionTypeId);
			t.setTimeCreated(now);
			if (ccId > 0 && transactionTypeId == TRANS_TYPE_CC_WITHDRAW) {
				t.setCreditCardId(new BigDecimal(ccId));
			}
			if (state == WithdrawBonusStateEnum.REGULATED_OPEN_BONUSES) {
				t.setDescription("transaction.cancel.regulated.desc");
			} else if (state == WithdrawBonusStateEnum.NOT_REGULATED_OPEN_BONUSES) {
				t.setDescription("transaction.cancel.not_regulated.desc");
			} else {
				log.error("Illegal state, transaction is not inserted for user with id: " + userId + ", and state: " + state);
				return;
			}
			t.setStatusId(TRANS_STATUS_FAILED);
			t.setTimeSettled(now);
			t.setUtcOffsetCreated(utcOffset);
			t.setUtcOffsetSettled(utcOffset);
			t.setWriterId(writerId);
			t.setIp(ip);
			TransactionsDAOBase.insert(con, t);
		} catch (SQLException e) {
			log.error("Unable to insert withdraw cancel for user with id: " + userId, e);
		}
	}
	
	public static boolean isBlackListBin(String binId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.isBlackListBin(con, Long.parseLong(binId));
		} finally {
			closeConnection(con);
		}
	}

	public static HashMap<MultiKey, EpgTransactionStatus> getEpgTransactionsStatus() throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getEpgTransactionsStatus(con);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static String getEPGPaymentDetails(long id) throws SQLException {
		Connection connection = getConnection();
		try {
			return TransactionsDAOBase.getEPGPaymentDetails(connection, id);
		} finally {
			closeConnection(connection);
		}
	}
	
	public static long getCreditCardMinWithdraw(long currencyId) throws SQLException {
		Connection connection = getConnection();
		try {
			WithdrawFee fee = TransactionsDAOBase.getWithdrawFee(connection, currencyId, TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
			return fee.getMinAmount();
		} finally {
			closeConnection(connection);
		}
	}

	public static long getWireMinWithdraw(long currencyId) throws SQLException {
		Connection connection = getConnection();
		try {
			WithdrawFee fee = TransactionsDAOBase.getWithdrawFee(connection, currencyId, TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW);
			return fee.getMinAmount();
		} finally {
			closeConnection(connection);
		}
	}

	public static long getTransactionHomoFee(Connection connection, long amount, long currencyId, long transactionTypeId) throws SQLException {
			// load min sum and percentage from database
			WithdrawFee fee = TransactionsDAOBase.getWithdrawFee(connection, currencyId, transactionTypeId);
			// if the fee is empty load default homo fee percentage
			if( fee == null ) {
				 String defaultPercentageString = CommonUtil.getEnum("default_homo_fee_withdraw", "homo_fee_real_withdraw");
				 Float defaultPercentage = Float.parseFloat(defaultPercentageString);
				 
				 fee = new WithdrawFee();
				 fee.setMinAmount(0);
				 fee.setPercentage(defaultPercentage);
			}
			
			// calculate
			long homoFee = new BigDecimal(amount).multiply(new BigDecimal(fee.getPercentage())).setScale(2, RoundingMode.HALF_UP).divide(new BigDecimal(100)).longValue();
			return Math.max(fee.getMinAmount(), homoFee);
	}

	
	public static long getTransactionHomoFee(long amount, long currencyId, long transactionTypeId) throws SQLException {
		Connection connection = getConnection();
		try {
			return getTransactionHomoFee(connection, amount, currencyId, transactionTypeId);
		} finally {
			closeConnection(connection);
		}
	}

	public static GmWithdrawalPermissionToDisplayResult getGmDisplayPerm(GmWithdrawalPermissionToDisplayRequest request, GmWithdrawalPermissionToDisplayResult result) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TransactionsDAOBase.getGmDisplayPerm(request, result, con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static GmWithdrawalRequest getTotalDeposits(GmWithdrawalRequest request) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TransactionsDAOBase.getTotalDeposits(request, con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void insertGmWithdraw(GmWithdrawalRequest request) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			TransactionsDAOBase.insertGmWithdraw(request, con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static boolean isFirstDepositWithCC(long userId, long creditCardId) throws SQLException {
	    Connection con = null;
	    try {
	    	con = getConnection();
	    return	TransactionsDAOBase.isFirstDepositWithCC(con, userId, creditCardId);
	    	
	    }finally {
	    closeConnection(con);	
	    }
	}
	
	public static double getRateById(long trxId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getRateById(con, trxId);
		} finally {
			closeConnection(con);
		}
	}
	
	
	private static	ClearingInfo
			depositBase(Connection connection, Transaction tran, User user, CreditCard cc, long providerId, long writerId,
						String authorizationCode)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
													NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
													UnsupportedEncodingException, InvalidAlgorithmParameterException {
		TransactionBin tb = new TransactionBin();
		ClearingRoute cr = null;
		boolean timeout = false;

		 // check card clearing
		ClearingInfo ci = new ClearingInfo(cc, user, tran.getAmount(), tran.getTypeId(), tran.getId(), tran.getIp(), tran.getAcquirerResponseId());
		ci.setProviderId(providerId);
		ci.setPaymentSolution(EPGClearingProvider.CREDIT_CARD);
		ci.setHomePageUrl(CommonUtil.getConfig("homepage.url.https"));
		ci.setMD(String.valueOf(ci.getTransactionId())+";"+writerId+";"+user.getUtcOffset());
		
		log.info("Transaction Clearing started: Clearing Info: " + ci.toString() );

		try {
			if (!CommonUtil.isParameterEmptyOrNull(authorizationCode)) { // just for capture
				ci.setAuthNumber(authorizationCode);
				ClearingManagerBase.captureWithRouting(ci);
			} else {
				log.info("Going to deposit with userId=" + user.getId() + " skinId=" + ci.getSkinId() + " cc_num=" + ci.getCcn() + " currencyId=" + ci.getCurrencyId() + " countryId=" +  ci.getCcCountryId() + " typeId=" + ci.getCcTypeId());
				cr = ClearingManagerBase.deposit(ci, providerId!=ConstantsBase.CLEARING_PROVIDER_NOT_SELECTED);
				tran.setThreeD(ci.is3dSecure());
				tran.setRender(ci.getFormData()!=null);
				tb = TransactionsDAOBase.getTransactionBinByBin(connection, cc.getBin(), SkinsManagerBase.getSkin(user.getSkinId()).getBusinessCaseIdForTranBins());
				if(tb.getId() == 0 ){
					tb.setBin(cc.getBin());
					tb.setBusinessCaseId(SkinsManagerBase.getSkin(user.getSkinId()).getBusinessCaseIdForTranBins());
					tb.setPlatformId(user.getPlatformId());
					TransactionsDAOBase.insertTranBin(connection, tb);
				} else {
					tb = TransactionsDAOBase.getTransactionBinById(connection, tb.getId());
				}
				if(ci.isSuccessful()){
					ClearingManagerBase.updateTimeLastSuccess(cr.getId());
					TransactionsManagerBase.updateTimeLastSuccess(tb);
					log.debug("Updated Trading Bin: " + tb.getId() + " because was succesful");
					log.debug("Updated clearing route: " + cr.getId() + " because was succesful");
				}
			}
		} catch (ClearingException ce) {
			if (ce.getCause() instanceof SocketTimeoutException) {
				timeout = true;
			}

			log.error("Failed to authorize transaction: " + ci.toString(), ce);
			ci.setResult("999");
			ci.setSuccessful(false);
			ci.setMessage(ce.getMessage());
		}
		
        tran.setClearingProviderId(ci.getProviderId());
        String c = tran.getComments() == null ? "": tran.getComments() + "|";
		tran.setComments("["+c + ci.getResult() + "|" + ci.getMessage() + "|" + ci.getUserMessage() + "|" + ci.getProviderTransactionId()+"]");

		if (!CommonUtil.isParameterEmptyOrNull(authorizationCode)) { // just for capture
			tran.setXorIdCapture(ci.getProviderTransactionId());
		} else {
			tran.setXorIdAuthorize(ci.getProviderTransactionId());
		}

	     if (ci.isSuccessful()) {  //success clearin
		   if (log.isInfoEnabled()) {
				log.info("Transaction Successful! authNumber: " + ci.getAuthNumber());
			}
		   
		   if(tran.getTypeId() == TRANS_TYPE_CC_DEPOSIT_REROUTE){
			   //Trans is rerouted set to TRANS_TYPE_CC_DEPOSIT
			   tran.setTypeId(TRANS_TYPE_CC_DEPOSIT);
		   }
		   
			if (!CommonUtil.isParameterEmptyOrNull(authorizationCode)) { // just for capture
				  tran.setAuthNumber(authorizationCode);
				  tran.setStatusId(TRANS_STATUS_SUCCEED);
				  tran.setTimeSettled(new Date());
				  tran.setUtcOffsetSettled(user.getUtcOffset());
			} else { // for Authorization
				tran.setAuthNumber(ci.getAuthNumber());
				tran.setAcquirerResponseId(ci.getAcquirerResponseId());
                if (ci.is3dSecure()) {
                    tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
                } else {
                    tran.setStatusId(TRANS_STATUS_PENDING);
                }
                tran.setTimeSettled(null);
			}

			if(!CommonUtil.isParameterEmptyOrNull(ci.getRecurringTransaction())){
				cc.setRecurringTransaction(ci.getRecurringTransaction());
				CreditCardsDAOBase.update(connection, cc);
			}

            if (tran.getStatusId() != TRANS_STATUS_AUTHENTICATE) { // not capture
            	Connection con = getConnection();
            	try {
            		con.setAutoCommit(false);
					UsersDAOBase.addToBalance(con, user.getId(), tran.getAmount());
					GeneralDAO.insertBalanceLog(con, writerId, tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_CC_DEPOSIT, user.getUtcOffset());
					TransactionsDAOBase.update(con, tran);
					con.commit();	    						
            	} catch (Exception e) {
            		log.error("Exception in success deposit Transaction! ", e);
   					rollback(con);
            	} finally {
            		setAutoCommitBack(con);
            		closeConnection(con);
        		}
            }
            
			} else {
				if(ClearingInfo.NEEDS_POLLING.equals(ci.getResult())) {
					// this transaction must be cleared by a job
					log.info("This transaction must be cleared latter:"+tran.getId());
					
				} else {
					// failed clearing
					tran.setComments(tran.getComments() + " | " + cc.toStringForComments());
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
					
					log.info("Transaction failed clearing! " + ci.toString());
		
					if (timeout) {
						tran.setDescription(ConstantsBase.ERROR_CLEARING_TIMEOUT);
					} else {
						tran.setDescription(ConstantsBase.ERROR_FAILED_CLEARING);
					}
				}
			}
		
     TransactionsDAOBase.update(connection, tran);
     return ci;
	}
	
	public static void updateTimeLastSuccess(TransactionBin tb) throws SQLException{
		Connection con = getConnection();
		try {
			TransactionsDAOBase.updateTimeLastSuccess(con, tb);
		} finally {
			closeConnection(con);
		}
	}
	
	// TO DO REROUTING 
	public static	ClearingInfo
			depositWithReroutingBase(	Connection conn, Transaction tran, User user, CreditCard cc, long providerId, long writerId,
										String authorizationCode)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																	UnsupportedEncodingException, InvalidAlgorithmParameterException {
		ClearingInfo ci = depositBase(conn, tran, user, cc, providerId, writerId, authorizationCode);
		if(tran.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT
				&& tran.getDescription() == ConstantsBase.ERROR_FAILED_CLEARING
					&& !ci.is3dSecure()
						&& !ReroutingManager.isNotBeReroutedErrCode(tran.getClearingProviderId(), ci.getResult())
							&& !ReroutingManager.isNotBeReroutedErrComment(tran.getComments(), tran.getId())){			
			ArrayList<Long> reroutingClearingProviders = ReroutingManager.getReroutingProviders(tran.getId());
			if(reroutingClearingProviders.size() > 0){				
				log.debug("Begin rerouting for Transaction:" + tran);				
				tran.setIsRerouted(true);
				tran.setReroutingTranId(tran.getId());
				tran.setComments(tran.getComments() + "|Declined, attempted reroutes in different providers");
				TransactionsDAOBase.update(conn, tran);
				Transaction reroutingTran = null;
				int i = 0;
				boolean continueRerout = true;
				while(reroutingClearingProviders.size() > i && continueRerout){
					long reroutProviderId = reroutingClearingProviders.get(i);
					log.debug("Try rerout with providerId[" + reroutProviderId +"] for transaction " + tran);
					reroutingTran = getReroutingInitail(tran);
					reroutingTran.setClearingProviderId(reroutProviderId);
					TransactionsDAOBase.insert(conn, reroutingTran);
					ci = depositBase(conn, reroutingTran, user, cc, reroutProviderId, writerId, authorizationCode);
					
					if(!ci.isSuccessful()){
						//Check errMsg
						if(ReroutingManager.isNotBeReroutedErrCode(reroutingTran.getClearingProviderId(), ci.getResult())){
							continueRerout = false;
						}
					} else {
						log.debug("Successful rerouting for transaction " + reroutingTran);
						continueRerout = false;
						//Set the main trans to reroute type
						tran.setTypeId(TRANS_TYPE_CC_DEPOSIT_REROUTE);
						TransactionsDAOBase.update(conn, tran);											
					}
					reroutingTran.setComments(reroutingTran.getComments() + "|Rerouted after a declined deposit attempt.");
					TransactionsDAOBase.update(conn, reroutingTran);
					i++;
				}																
			}			
		}		
		return ci;
	}
	
	private static Transaction getReroutingInitail(Transaction t){
		Transaction tran = new Transaction();
		tran.setAmount(t.getAmount());
		tran.setCreditCardId(t.getCreditCardId());
		tran.setCc4digit(t.getCc4digit());
		tran.setIp(t.getIp());
		tran.setProcessedWriterId(t.getWriterId());
		tran.setTimeCreated(new Date());
		tran.setTypeId(TRANS_TYPE_CC_DEPOSIT_REROUTE);
		tran.setUserId(t.getUserId());
		tran.setWriterId(t.getWriterId());
		tran.setStatusId(TRANS_STATUS_STARTED);
		tran.setCurrency(t.getCurrency());
		tran.setUtcOffsetCreated(t.getUtcOffsetCreated());
		tran.setLoginId(t.getLoginId());
		tran.setIsRerouted(true);
		tran.setReroutingTranId(t.getReroutingTranId());		
		return tran;
	}

    public static void finishFibonatixTransaction(Transaction t, long writerId, String utcOffset, String providerTrxId, String message, boolean success) throws ClearingException, SQLException {
    	Connection conn = null;

        try {
       	 	conn = getConnection();
       	 	
				if (success) {
				    t.setStatusId(TRANS_STATUS_PENDING);
				    t.setDescription(null);
				    t.setXorIdCapture(providerTrxId);
				    t.setUtcOffsetSettled(utcOffset);
				    t.setProcessedWriterId(writerId);
				    try {
				    	conn.setAutoCommit(false);
				        UsersDAOBase.addToBalance(conn, t.getUserId(), t.getAmount());
				        GeneralDAO.insertBalanceLog(conn, writerId, t.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, t.getId(), ConstantsBase.LOG_BALANCE_CC_DEPOSIT, utcOffset);
				        t.setComments("| Approved [" + message+"]");
				        TransactionsDAOBase.updateForStatus(conn, t, TRANS_STATUS_AUTHENTICATE);
				        conn.commit();
				    } catch (Exception e) {
						log.error("Exception in finish3DTransaction! success case. ", e);
						rollback(conn);
						throw e;
					} finally {
						setAutoCommitBack(conn);
					}
				
				} else {
				    t.setStatusId(TRANS_STATUS_FAILED);
				    t.setDescription(null);
				    t.setComments("| Failed [" + message+"]");
			        TransactionsDAOBase.updateForStatus(conn, t, TRANS_STATUS_AUTHENTICATE);
				}
        } finally {
        	closeConnection(conn);
        }
   }
    
    
    public static String getTransactionCreditCardCVV(long transactionId) throws SQLException {
        String cvv = null;
        Connection conn = null;
        try {
            conn = getConnection();
            cvv = TransactionsDAOBase.getTransactionCreditCardCVV(conn, transactionId);
        } finally {
            closeConnection(conn);
        }
        return cvv;
    }

	public static Transaction updatePowerPayTransactionStatus(long loginId, long transactionId, boolean success, String providerTxtNo, String message) {
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			log.debug("powerpay begin transaction update");
	    	Transaction transaction = getTransaction(transactionId);
	    	//There is no such transactionId in db.
	    	if (transaction == null) {
	    		log.error("ERROR! wrong PowerPay transactionId : " + transactionId);
	    		return null;
	    	}
	    	// Not expected transaction 
	    	if (transaction.getClearingProviderId() != ClearingManagerBase.POWERPAY21_PROVIDER_ID_APS) {
	    		log.error("ERROR! not expected clearing provider : "+ transaction.getClearingProviderId());
	    		return null;
	    	}
	    	// Transaction already cleared
			if (transaction.getStatusId() != TRANS_STATUS_AUTHENTICATE) {
				log.error("ERROR! not expected  status : " + transaction.getStatusId() + " for transaction :" + transaction.getId()); 
				return null;
			}
			
			// store providerTxtNo for future reference
			if(!CommonUtil.isParameterEmptyOrNull(providerTxtNo)) {
				transaction.setXorIdAuthorize(providerTxtNo);
				transaction.setAuthNumber(providerTxtNo);
			}
			//success
			if (success) {
				transaction.setTimeSettled(new Date());
				transaction.setUtcOffsetSettled(transaction.getUtcOffsetCreated());
				transaction.setStatusId(TRANS_STATUS_SUCCEED);
				transaction.setComments("1000| " + "Permitted transaction |" );
				log.debug("succesful powerpay transaction: "+ transaction.getId());
			} else {
				transaction.setStatusId(TRANS_STATUS_FAILED);
				transaction.setComments("-1| " + "Failed |" + message );
				log.debug("Failed powerpay transaction.");
			}
			
			// Update user data
			if (transaction.getStatusId() == TRANS_STATUS_SUCCEED) {
				//Update transaction.
				TransactionsDAOBase.updateForStatus(con, transaction, TRANS_STATUS_AUTHENTICATE);
				UsersDAOBase.addToBalance(con, transaction.getUserId(), transaction.getAmount());
				GeneralDAO.insertBalanceLog(con, transaction.getWriterId(), transaction.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, transaction.getId(), ConstantsBase.LOG_BALANCE_DIRECT_DEPOSIT, transaction.getUtcOffsetCreated());
				con.commit();
			} else {
				//Update transaction.
				TransactionsDAOBase.updateForStatus(con, transaction, TRANS_STATUS_AUTHENTICATE);
				con.commit();
			}
			
			return transaction;
		} catch (Exception e) {
    		log.error("Exception powerpay : ", e);
			rollback(con);
		} finally {
			setAutoCommitBack(con);
	        closeConnection(con);
		}
		return null;
	}
	
	public static long getLastTransactionsId(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TransactionsDAOBase.getLastTransactionsId(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<Transaction> getPendingWithdraws(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			String statuses = String.valueOf(TransactionsManagerBase.TRANS_STATUS_REQUESTED) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_STATUS_APPROVED) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL);

			return TransactionsDAOBase.getByUserAndStatus(con, userId, 0,
					TransactionsManagerBase.TRANS_CLASS_WITHDRAW, statuses, TransactionsDAOBase.ORDER_ASC);
		} finally {
			closeConnection(con);
		}
	}

	 public static boolean reverseWithdraws( ArrayList<Transaction> transactions, User user, long writerId, String ip, long loginId) throws SQLException  {
		 Connection con = null;
	        try {
	        	con = getConnection();
	            con.setAutoCommit(false);

	            for (Transaction tran : transactions) {

	                if (log.isEnabledFor(Level.INFO)) {
	                        String ls = System.getProperty("line.separator");
	                        log.log(Level.INFO, "Insert ReverseWithdraw: " + ls + tran.toString() + ls);
	                    }

	                    UsersDAOBase.addToBalance(con, user.getId(), tran.getAmount());
	                    UsersDAOBase.insertBalanceLog(con, writerId, tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(),
	                                    ConstantsBase.LOG_BALANCE_REVERSE_WITHDRAW, user.getUtcOffset());

	                    // update currency transaction
	                    long newBalance = user.getBalance() + tran.getAmount();
	                    tran.setTimeSettled(new Date());
	                    tran.setProcessedWriterId(writerId);
	                    tran.setStatusId(TRANS_STATUS_REVERSE_WITHDRAW);
	                    tran.setWriterId(writerId);
	                    TransactionsDAOBase.updateReverseWithdraw(con, tran);

	                    // insert reverse withdrawal transaction
	                    tran.setReferenceId(new BigDecimal(tran.getId()));
	                    tran.setIp(ip);
	                    tran.setTypeId(TRANS_TYPE_REVERSE_WITHDRAW);
	                    tran.setStatusId(TRANS_STATUS_SUCCEED);
	                    tran.setTimeSettled(new Date());
	                    tran.setUtcOffsetSettled(user.getUtcOffset());
	                    tran.setLoginId(loginId);
	                    TransactionsDAOBase.insert(con, tran);

	                    if (log.isEnabledFor(Level.INFO)) {
	                        String ls = System.getProperty("line.separator");
	                        log.log(Level.INFO, "ReverseWithdraw insert successfully! " + ls + tran.toString());
	                    }

	                    user.setBalance(newBalance);
	            }
	            con.commit();

	            return true;
	        } catch (SQLException e) {
	            log.log(Level.ERROR, "Error on reverseWithdraw:", e);
	            try {
	                con.rollback();
	            } catch (SQLException ie) {
	                log.log(Level.ERROR, "Can't rollback.", ie);
	            }
	            throw e;
	        } finally {
	            con.setAutoCommit(true);
	        }
	    }

}