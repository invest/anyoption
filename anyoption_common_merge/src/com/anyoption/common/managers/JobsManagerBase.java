package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.daos.JobsDAOBase;
import com.anyoption.common.jobs.JobUtil;

public class JobsManagerBase extends BaseBLManager {
    private static final Logger log = Logger.getLogger(JobsManagerBase.class);

    public static final String CONTEXT_PARAM_RUN_JOBS = "com.anyoption.common.jobs.RUN_JOBS";
    public static final String CONTEXT_PARAM_RUN_APPLICATION = "com.anyoption.common.jobs.RUN_APPLICATION";

    public static Job startJob(long jobId, String serverId, String application) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return JobUtil.startJob(conn, jobId, serverId, application);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void stopJob(long jobId, String serverId, boolean updateRunTime) {
        Connection conn = null;
        try {
            conn = getConnection();
            JobUtil.stopJob(conn, jobId, serverId, updateRunTime);
        } catch (SQLException sqle) {
            log.error("Can't stop job.", sqle);
        } finally {
            closeConnection(conn);
        }
    }

    public static Job getByIdWithLock(long id, String serverId, String application) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return JobsDAOBase.getByIdWithLock(conn, id, serverId, application);
        } finally {
            closeConnection(conn);
        }
    }

    public static List<Long> getJobsIds() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return JobsDAOBase.getJobsIds(conn);
        } finally {
            closeConnection(conn);
        }
    }
}