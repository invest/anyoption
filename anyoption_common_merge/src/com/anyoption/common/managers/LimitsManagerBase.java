package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Limit;
import com.anyoption.common.daos.LimitsDAOBase;

/**
 * @author kiril.mutafchiev
 */
public class LimitsManagerBase extends BaseBLManager {

	private static final Logger log = Logger.getLogger(LimitsManagerBase.class);
	private static Map<Long, Long> bdaDefaultInvestmentAmounts = new HashMap<>();
	private static Map<Long, Long> bdaPredefinedDepositAmounts = new HashMap<>();

	public static long getBDAPredefinedDepositAmount(long currencyId) {
		if (bdaPredefinedDepositAmounts.get(currencyId) == null) {
			Connection con;
			try {
				con = getConnection();
				bdaPredefinedDepositAmounts.put(currencyId, LimitsDAOBase.getBDAPredefinedDepositAmount(con, currencyId));
			} catch (SQLException e) {
				log.debug("Unable to load bda predefined deposit amount", e);;
				return 0l;
			}
		}
		return bdaPredefinedDepositAmounts.get(currencyId);
	}

	public static long getBDADefaultInvestmentAmount(long currencyId) {
		if (bdaDefaultInvestmentAmounts.get(currencyId) == null) {
			Connection con;
			try {
				con = getConnection();
				bdaDefaultInvestmentAmounts.put(currencyId, LimitsDAOBase.getBDADefaultInvestmentAmount(con, currencyId));
			} catch (SQLException e) {
				log.debug("Unable to load bda balance step", e);;
				return 0l;
			}
		}
		return bdaDefaultInvestmentAmounts.get(currencyId);
	}
	
	public static Limit getLimitById(long id) throws SQLException {
		Limit l = new Limit();
		Connection con = getConnection();
		try {
			l = LimitsDAOBase.getLimitById(con, id);
		} finally {
			closeConnection(con);
		}
		return l;
	}
}