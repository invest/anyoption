package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardTaskType;
import com.anyoption.common.beans.RewardUserTask;
import com.anyoption.common.daos.RewardUserTasksDAO;
import com.anyoption.common.rewards.FirstTimeDepositAmountTask;
import com.anyoption.common.rewards.InvestmentPremiaTask;
import com.anyoption.common.rewards.InvestmentTask;
import com.anyoption.common.rewards.RegistrationTask;
import com.anyoption.common.rewards.RewardTasksHandler;
import com.anyoption.common.rewards.SendingDocsTask;
import com.anyoption.common.rewards.ShowOffTask;
import com.anyoption.common.rewards.Task;

/**
 * @author liors
 *
 */
public class RewardUserTasksManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(RewardUserTasksManager.class);
	
	public enum TaskGroupType {
		REGISTRAITION(RewardTaskType.GROUP_TYPE_USERS, RegistrationTask.getInstance())
		,FIRST_TIME_DEPOSIT_AMOUNT(RewardTaskType.GROUP_TYPE_TRANSACTIONS, FirstTimeDepositAmountTask.getInstance())
		,INVESTMENT(RewardTaskType.GROUP_TYPE_INVESTMENTS, InvestmentTask.getInstance())
		/**, FRIENDS_INVITE(4)*/
		 , SHOW_OFF(RewardTaskType.GROUP_TYPE_SHOW_OFF, ShowOffTask.getInstance())
		 ,DOCS(RewardTaskType.GROUP_TYPE_DOCS, SendingDocsTask.getInstance())
		 ,INVESTMENT_PREMIA(RewardTaskType.GROUP_TYPE_INVESTMENTS_PREMIA, InvestmentPremiaTask.getInstance());
		
		private int id;
		private RewardTasksHandler rewardTasksHandler;
		
		private TaskGroupType(int id, RewardTasksHandler rewardTasksHandler) {
			this.setId(id);
			this.setRewardTasksHandler(rewardTasksHandler);
		}
		
		public void setId(int id) {
			this.id = id;
		}
		
		public int getId() {
			return this.id;
		}

		/**
		 * @return the rewardTasksHandler
		 */
		public RewardTasksHandler getRewardTasksHandler() {
			return rewardTasksHandler;
		}

		/**
		 * @param rewardTasksHandler the rewardTasksHandler to set
		 */
		public void setRewardTasksHandler(RewardTasksHandler rewardTasksHandler) {
			this.rewardTasksHandler = rewardTasksHandler;
		}
	}
	
	public static boolean rewardTasksHandler(TaskGroupType groupType, long userId, long amount, long referenceId, Class<?> BonusManagerClass, int writerId) {
		return rewardTasksHandler(groupType, userId, amount, referenceId, BonusManagerClass, writerId, 0, 0);
	}

	/**
	 * @param groupType
	 * @param userId
	 * @param amount
	 * @param referenceId
	 * @param BonusManagerClass
	 * @param writerId
	 * @return
	 */
	public static boolean rewardTasksHandler(TaskGroupType groupType, long userId, long amount, long referenceId, Class<?> BonusManagerClass, int writerId,
			int productTypeId, long opportunityTypeId) {
		Connection connection = null;
		boolean result = false;
//		User user = null;
//        try {            
//        	connection = getConnection();
//        	connection.setAutoCommit(false);
//        	if (groupType.compareTo(TaskGroupType.REGISTRAITION) == 0) {
//        		logger.info(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "found registration type, userId: " + userId);
//        		user = UsersDAOBase.getUser(connection, userId);
//        	} else {
//        		user = UsersDAOBase.getUserWithReward(connection, userId);
//        	}
//        	
//        	if (user != null) {
//	        	TaskUser taskUser = new TaskUser(groupType, user.getId(), amount, user.getCurrencyId().intValue(), referenceId, writerId, productTypeId, opportunityTypeId);
//	        	logger.info(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "about to start taskHandler");
//	        	result =  groupType.getRewardTasksHandler().taskHandler(connection, taskUser, user, BonusManagerClass);
//	        	connection.commit();
//        	}
//        } catch (Exception e) {
//        	logger.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "userId: " + userId + "groupType class name: " + groupType.getRewardTasksHandler().getClass().getName(), e);
//        	try {
//				connection.rollback();
//			} catch (SQLException sqe) {
//				logger.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "ERROR rollback!!!", sqe);
//			}
//		} finally {
//			try {
//				connection.setAutoCommit(true);
//			} catch (SQLException sqe) {
//				logger.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "ERROR setAutoCommit true!!!", sqe);		
//			}
//        	closeConnection(connection);
//        }
        return result;
	}      

	public static void updateTask(Task task) throws SQLException {
		Connection connection = getConnection();
        try {               	
        	RewardUserTasksDAO.update(connection, task.getRewardUserTask());
        } finally {
        	closeConnection(connection);
        }
	}

	public static void updateOrInsert(Connection connection, RewardUserTask rewardUserTasks) throws SQLException {
		if (rewardUserTasks.isFirstUserTask()) {
			RewardUserTasksDAO.insertValueObject(connection, rewardUserTasks);
		} else {
			RewardUserTasksDAO.update(connection, rewardUserTasks);
		}
		
	}
}