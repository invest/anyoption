package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.ApiExternalUser;
import com.anyoption.common.daos.ApiExternalUserDAOBase;

/**
 * @author EranL
 *
 */
public class ApiExternalUserManagerBase extends BaseBLManager {
		private static final Logger log = Logger.getLogger(ApiExternalUserManagerBase.class);	

	    /**
	     * Get API external user
	     * @param apiUserName
	     * @param apiExternalUserReference
	     * @param userName
	     * @return
	     * @throws SQLException
	     */
	    public static ApiExternalUser getApiExternalUser(String apiExternalUserReference, long apiUserId) throws SQLException {    	
	    	Connection con = null;
	        try {
	        	con = getConnection();
	        	return ApiExternalUserDAOBase.getAPIExternalUser(con, apiExternalUserReference, apiUserId); 
	        } finally {
	            closeConnection(con);
	        }
	    }
	    
	    /**
	     * Insert API external user
	     * @param apiUserName
	     * @param apiExternalUserReference
	     * @param userName
	     * @return
	     * @throws SQLException
	     */
	    public static void InsertApiExternalUser(ApiExternalUser aeu) throws SQLException {    	
	    	Connection con = null;
	        try {
	        	con = getConnection();
	        	ApiExternalUserDAOBase.insertAPIExternalUser(con, aeu); 
	        } finally {
	            closeConnection(con);
	        }
	    }
		
}
