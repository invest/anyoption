package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.State;
import com.anyoption.common.daos.StatesDAOBase;

public class StatesManagerBase extends BaseBLManager {

    /**
     * Get all states
     *
     * @param con connection to Db
     * @return <code>Hashtable<Long, String></code>
     * @throws SQLException
     */
    public static ArrayList<State> getAllStates() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return StatesDAOBase.getAll(conn);
        } finally {
            closeConnection(conn);
        }
    }

}