package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.FireServerPixelInfo;
import com.anyoption.common.beans.ServerPixel;
import com.anyoption.common.beans.ServerPixelsPublisherConfig;
import com.anyoption.common.beans.ServerSidePixelEvent;
import com.anyoption.common.daos.ServerPixelsDAOBase;
import com.anyoption.common.server.pixel.ServerPixelHandler;
import com.anyoption.common.service.CommonJSONService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

public class ServerPixelsManagerBase extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(ServerPixelsManagerBase.class);
	public static final int TYPE_OPEN_APP = 3;
		
	public static void insert(ServerPixel serverPixel) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();	
			ServerPixelsDAOBase.insert(con, serverPixel);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * The function check if 'device unique id' has open app pixel in the last X days.
	 * @param publisherId
	 * @param duid
	 * @param numOfDays
	 * @return true if exists
	 * @throws SQLException
	 */
	public static boolean isHasOpenAppPixel(long publisherId, String duid, long numOfDays) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return ServerPixelsDAOBase.isHasOpenAppPixel(con, publisherId, duid, numOfDays);
		} finally {
			closeConnection(con);
		}
	}
	
    public static int fireServerPixel(FireServerPixelInfo fireServerPixelInfo) {
    	ServerPixelsPublisherConfig serverPixelsPublisherConfig = new ServerPixelsPublisherConfig();
    	serverPixelsPublisherConfig.setServerPixelsPublisherId(fireServerPixelInfo.getServerPixelsPublisherId());
    	serverPixelsPublisherConfig.setServerPixelsTypeId(fireServerPixelInfo.getServerPixelsTypeId());
    	serverPixelsPublisherConfig.setPlatformId(fireServerPixelInfo.getPlatformId());
    	try {
			ServerPixelsPublisherConfigManagerBase.getServerPixelsPublisherConfig(serverPixelsPublisherConfig);
		} catch (SQLException e) {
			logger.debug("cant load pixel for publisher id " + fireServerPixelInfo.getServerPixelsPublisherId() + " event id " + fireServerPixelInfo.getServerPixelsTypeId());
		}
    	if (serverPixelsPublisherConfig.getId() > 0) {
	    	Gson gson = new GsonBuilder().serializeNulls().create();
			ServerPixelHandler serverPixelHandler;
			try {
				serverPixelHandler = (ServerPixelHandler) gson.fromJson(serverPixelsPublisherConfig.getHandlerClassParams(), Class.forName(serverPixelsPublisherConfig.getHandlerClass()));
				ServerSidePixelEvent serverSidePixelEvent = new ServerSidePixelEvent(fireServerPixelInfo.getDeviceId(), fireServerPixelInfo.getPublisherIdentifier(), fireServerPixelInfo.getServerPixelsPublisherId(),
						fireServerPixelInfo.getServerPixelsTypeId(), fireServerPixelInfo.getTransactionId(), fireServerPixelInfo.getUserId(), serverPixelsPublisherConfig.getPixelUrl(), fireServerPixelInfo.getOrderId());
				serverPixelHandler.setServerSidePixelEvent(serverSidePixelEvent);
				serverPixelHandler.run();
				return CommonJSONService.ERROR_CODE_SUCCESS;
			} catch (JsonSyntaxException | ClassNotFoundException e) {
				logger.debug("cant load handler for class " + serverPixelsPublisherConfig.getHandlerClassParams() + " with params " + serverPixelsPublisherConfig.getHandlerClass());
			}
    	}
		return CommonJSONService.ERROR_CODE_UNKNOWN;
    }
}