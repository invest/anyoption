package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.Notification;
import com.anyoption.common.daos.NotificationDAOBase;

/**
 * @author EyalG
 *
 */
public class NotificationManagerBase extends BaseBLManager {

	/**
     * Update Notifications status
     * @param statusId the new status
     * @param notifications Ids to update
     * @throws SQLException
     */
    public static void insertNotifications(Notification notification) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		NotificationDAOBase.insertNotifications(con, notification);
    	} finally {
        	closeConnection(con);
        }
    }
}