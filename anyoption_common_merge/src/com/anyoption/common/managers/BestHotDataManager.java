package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.BestHotDataDAO;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.common.enums.base.UserStateEnum;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.copyop.common.managers.CopiedManager;
import com.copyop.common.managers.FeedManager;
import com.copyop.common.managers.ProfileManager;

public class BestHotDataManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(BestHotDataManager.class);

	//private static long SEARCH_FOR_8_HOUR_TIME = 8;
	//private static long SEARCH_FOR_24_HOUR_TIME = 24;
	//private static long SEARCH_FOR_72_HOUR_TIME = 72;

	//private static long SEARCH_FOR_20_RECORDS_COUNT = 20;
	//private static long SEARCH_FOR_5_RECORDS_COUNT = 5;
	//private static long SEARCH_FOR_3_RECORDS_COUNT = 3;
	//private static long SEARCH_FOR_1_RECORDS_COUNT = 1;
	
	private static long HOT_ASSET_SPECIALISTS_FIRST_HOURS_SEARCH =  getCfg("HOT_ASSET_SPECIALISTS_FIRST_HOURS_SEARCH");
	private static long HOT_ASSET_SPECIALISTS_SECOND_HOURS_SEARCH =  getCfg("HOT_ASSET_SPECIALISTS_SECOND_HOURS_SEARCH");
	private static long HOT_ASSET_SPECIALISTS_THIRD_HOURS_SEARCH =  getCfg("HOT_ASSET_SPECIALISTS_THIRD_HOURS_SEARCH");
	private static long HOT_ASSET_SPECIALISTS_FIRST_RECORDS_COUNT_SEARCH =  getCfg("HOT_ASSET_SPECIALISTS_FIRST_RECORDS_COUNT_SEARCH");
	private static long HOT_ASSET_SPECIALISTS_SECOND_RECORDS_COUNT_SEARCH =  getCfg("HOT_ASSET_SPECIALISTS_SECOND_RECORDS_COUNT_SEARCH");
	private static long HOT_ASSET_SPECIALISTS_THIRD_RECORDS_COUNT_SEARCH =  getCfg("HOT_ASSET_SPECIALISTS_THIRD_RECORDS_COUNT_SEARCH");

	private static long HOT_BEST_TRADERS_FIRST_HOURS_SEARCH =  getCfg("HOT_BEST_TRADERS_FIRST_HOURS_SEARCH");
	private static long HOT_BEST_TRADERS_SECOND_HOURS_SEARCH =  getCfg("HOT_BEST_TRADERS_SECOND_HOURS_SEARCH");
	private static long HOT_BEST_TRADERS_THIRD_HOURS_SEARCH =  getCfg("HOT_BEST_TRADERS_THIRD_HOURS_SEARCH");
	private static long HOT_BEST_TRADERS_FIRST_RECORDS_COUNT_SEARCH =  getCfg("HOT_BEST_TRADERS_FIRST_RECORDS_COUNT_SEARCH");
	private static long HOT_BEST_TRADERS_SECOND_RECORDS_COUNT_SEARCH =  getCfg("HOT_BEST_TRADERS_SECOND_RECORDS_COUNT_SEARCH");
	private static long HOT_BEST_TRADERS_THIRD_RECORDS_COUNT_SEARCH =  getCfg("HOT_BEST_TRADERS_THIRD_RECORDS_COUNT_SEARCH");

	private static long HOT_BEST_TRADES_FIRST_HOURS_SEARCH =  getCfg("HOT_BEST_TRADES_FIRST_HOURS_SEARCH");
	private static long HOT_BEST_TRADES_SECOND_HOURS_SEARCH =  getCfg("HOT_BEST_TRADES_SECOND_HOURS_SEARCH");
	private static long HOT_BEST_TRADES_THIRD_HOURS_SEARCH =  getCfg("HOT_BEST_TRADES_THIRD_HOURS_SEARCH");
	private static long HOT_BEST_TRADES_FIRST_RECORDS_COUNT_SEARCH =  getCfg("HOT_BEST_TRADES_FIRST_RECORDS_COUNT_SEARCH");
	private static long HOT_BEST_TRADES_SECOND_RECORDS_COUNT_SEARCH =  getCfg("HOT_BEST_TRADES_SECOND_RECORDS_COUNT_SEARCH");
	private static long HOT_BEST_TRADES_THIRD_RECORDS_COUNT_SEARCH =  getCfg("HOT_BEST_TRADES_THIRD_RECORDS_COUNT_SEARCH");

	private static long HOT_BEST_COPIERS_FIRST_HOURS_SEARCH =  getCfg("HOT_BEST_COPIERS_FIRST_HOURS_SEARCH");
	private static long HOT_BEST_COPIERS_SECOND_HOURS_SEARCH =  getCfg("HOT_BEST_COPIERS_SECOND_HOURS_SEARCH");
	private static long HOT_BEST_COPIERS_THIRD_HOURS_SEARCH =  getCfg("HOT_BEST_COPIERS_THIRD_HOURS_SEARCH");
	private static long HOT_BEST_COPIERS_FIRST_RECORDS_COUNT_SEARCH =  getCfg("HOT_BEST_COPIERS_FIRST_RECORDS_COUNT_SEARCH");
	private static long HOT_BEST_COPIERS_SECOND_RECORDS_COUNT_SEARCH =  getCfg("HOT_BEST_COPIERS_SECOND_RECORDS_COUNT_SEARCH");
	private static long HOT_BEST_COPIERS_THIRD_RECORDS_COUNT_SEARCH =  getCfg("HOT_BEST_COPIERS_THIRD_RECORDS_COUNT_SEARCH");

	private static long SYSTEM_HIGHEST_HIT_FIRST_HOURS_SEARCH =  getCfg("SYSTEM_HIGHEST_HIT_FIRST_HOURS_SEARCH");
	private static long SYSTEM_HIGHEST_HIT_SECOND_HOURS_SEARCH =  getCfg("SYSTEM_HIGHEST_HIT_SECOND_HOURS_SEARCH");
	private static long SYSTEM_HIGHEST_HIT_THIRD_HOURS_SEARCH =  getCfg("SYSTEM_HIGHEST_HIT_THIRD_HOURS_SEARCH");
	private static long SYSTEM_HIGHEST_HIT_FIRST_RECORDS_COUNT_SEARCH =  getCfg("SYSTEM_HIGHEST_HIT_FIRST_RECORDS_COUNT_SEARCH");
	private static long SYSTEM_HIGHEST_HIT_SECOND_RECORDS_COUNT_SEARCH =  getCfg("SYSTEM_HIGHEST_HIT_SECOND_RECORDS_COUNT_SEARCH");
	private static long SYSTEM_HIGHEST_HIT_THIRD_RECORDS_COUNT_SEARCH =  getCfg("SYSTEM_HIGHEST_HIT_THIRD_RECORDS_COUNT_SEARCH");

	private static long SYSTEM_MOST_COPIED_FIRST_HOURS_SEARCH =  getCfg("SYSTEM_MOST_COPIED_FIRST_HOURS_SEARCH");
	private static long SYSTEM_MOST_COPIED_SECOND_HOURS_SEARCH =  getCfg("SYSTEM_MOST_COPIED_SECOND_HOURS_SEARCH");
	private static long SYSTEM_MOST_COPIED_FIRST_RECORDS_COUNT_SEARCH =  getCfg("SYSTEM_MOST_COPIED_FIRST_RECORDS_COUNT_SEARCH");
	private static long SYSTEM_MOST_COPIED_SECOND_RECORDS_COUNT_SEARCH =  getCfg("SYSTEM_MOST_COPIED_SECOND_RECORDS_COUNT_SEARCH");

	private static long SYSTEM_TRENDIEST_TRENDS_RECORDS_COUNT_SEARCH =  getCfg("SYSTEM_TRENDIEST_TRENDS_RECORDS_COUNT_SEARCH");
	private static long SYSTEM_TRENDIEST_TRENDS_MIN_RECORDS_COUNT_SEARCH =  getCfg("SYSTEM_TRENDIEST_TRENDS_MIN_RECORDS_COUNT_SEARCH");

	private static long getCfg(String key){

		long res = 0;
		try {
			return new Long (ConfigurationCopyopManager.getCopyopPropertiesConfig().get(key));
		} catch (Exception e) {
			logger.error("Can't get CFG for: " + key,e);
		}
		return res;
	}
	
	public static  ArrayList<FeedMessage> getHotAssetSpecialists() {
		ArrayList<FeedMessage> hotAssetSpecialistsList = new ArrayList<FeedMessage>();
		Connection con = null;
		try {
			con = getConnection();

			logger.debug("Try to get LIST of Hot Asset Specialists for:" + HOT_ASSET_SPECIALISTS_FIRST_HOURS_SEARCH + "h");
			hotAssetSpecialistsList = BestHotDataDAO.getHotAssetSpecialistsBestTraders(con, HOT_ASSET_SPECIALISTS_FIRST_HOURS_SEARCH, HOT_ASSET_SPECIALISTS_FIRST_RECORDS_COUNT_SEARCH, UpdateTypeEnum.BEST_ASSET_SPECIALISTS);
			if (hotAssetSpecialistsList.size() != HOT_ASSET_SPECIALISTS_FIRST_RECORDS_COUNT_SEARCH) {
				logger.debug("The LIST size:" + hotAssetSpecialistsList.size() + " < " + HOT_ASSET_SPECIALISTS_FIRST_RECORDS_COUNT_SEARCH + " records. Try to get LIST of Hot Asset Specialists for: " + HOT_ASSET_SPECIALISTS_SECOND_HOURS_SEARCH + "h");
				hotAssetSpecialistsList = BestHotDataDAO.getHotAssetSpecialistsBestTraders(con, HOT_ASSET_SPECIALISTS_SECOND_HOURS_SEARCH, HOT_ASSET_SPECIALISTS_FIRST_RECORDS_COUNT_SEARCH, UpdateTypeEnum.BEST_ASSET_SPECIALISTS);
					if (hotAssetSpecialistsList.size() < HOT_ASSET_SPECIALISTS_SECOND_RECORDS_COUNT_SEARCH) {
					logger.debug("The LIST size:" + hotAssetSpecialistsList.size() + " < " + HOT_ASSET_SPECIALISTS_SECOND_RECORDS_COUNT_SEARCH + " records. Try to get LIST of Hot Asset Specialists for: " + HOT_ASSET_SPECIALISTS_THIRD_HOURS_SEARCH + "h");
					hotAssetSpecialistsList = BestHotDataDAO.getHotAssetSpecialistsBestTraders(con, HOT_ASSET_SPECIALISTS_THIRD_HOURS_SEARCH, HOT_ASSET_SPECIALISTS_FIRST_RECORDS_COUNT_SEARCH, UpdateTypeEnum.BEST_ASSET_SPECIALISTS);
					if (hotAssetSpecialistsList.size() < HOT_ASSET_SPECIALISTS_THIRD_RECORDS_COUNT_SEARCH) {
						logger.debug("The LIST of Hot Asset Specialists size:" + hotAssetSpecialistsList.size() + " < " + HOT_ASSET_SPECIALISTS_THIRD_RECORDS_COUNT_SEARCH + " records. Clear and do nothing!");
						hotAssetSpecialistsList.clear();
					}
				}

			}
		} catch (SQLException e) {
			logger.error("Error when trying get Asset Specialists: ", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("The Hot Asset Specialists LIST size:" + hotAssetSpecialistsList.size());
		return hotAssetSpecialistsList;
	}

	public static ArrayList<FeedMessage> getHotAssetSpecialists(long hours, UpdateTypeEnum msgEnumType) {
		ArrayList<FeedMessage> hotAssetSpecialistsList = new ArrayList<FeedMessage>();
		Connection con = null;
		try {
			con = getConnection();
			logger.debug("Try to get LIST of Hot Asset Specialists 1W/1M/3M for:" + hours + "h");
			hotAssetSpecialistsList = BestHotDataDAO.getHotAssetSpecialistsBestTraders(con, hours, HOT_ASSET_SPECIALISTS_FIRST_RECORDS_COUNT_SEARCH, msgEnumType);
			if(hotAssetSpecialistsList.size() < HOT_ASSET_SPECIALISTS_THIRD_RECORDS_COUNT_SEARCH){
				logger.debug("The LIST of Hot Asset Specialists size:" + hotAssetSpecialistsList.size() + " < " + HOT_ASSET_SPECIALISTS_THIRD_RECORDS_COUNT_SEARCH + " records. Clear and insert running time now()!");
				FeedManager.insertFeedLastInsertMsgs(QueueTypeEnum.HOT.getId(), msgEnumType.getId());
				hotAssetSpecialistsList.clear();
			}
		} catch (SQLException e) {
			logger.error("Error when trying get Asset Specialists: ", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("The Hot Asset Specialists 1W/1M/3M LIST size:" + hotAssetSpecialistsList.size());
		return hotAssetSpecialistsList;
	}	

	public static  ArrayList<FeedMessage> getHotBestTraders() {
		ArrayList<FeedMessage> hotBestTradersList = new ArrayList<FeedMessage>();
		Connection con = null;
		try {
			con = getConnection();

			logger.debug("Try to get LIST of Hot Best Traders for:" + HOT_BEST_TRADERS_FIRST_HOURS_SEARCH + "h");
			hotBestTradersList = BestHotDataDAO.getHotBestTraders(con, HOT_BEST_TRADERS_FIRST_HOURS_SEARCH, HOT_BEST_TRADERS_FIRST_RECORDS_COUNT_SEARCH, UpdateTypeEnum.BEST_TRADERS);
			if(hotBestTradersList.size() != HOT_BEST_TRADERS_FIRST_RECORDS_COUNT_SEARCH){
				logger.debug("The LIST size:" + hotBestTradersList.size() + " < " + HOT_BEST_TRADERS_FIRST_RECORDS_COUNT_SEARCH + " records. Try to get LIST of Hot Best Traders for: " + HOT_BEST_TRADERS_SECOND_HOURS_SEARCH + "h");
				hotBestTradersList = BestHotDataDAO.getHotBestTraders(con, HOT_BEST_TRADERS_SECOND_HOURS_SEARCH, HOT_BEST_TRADERS_FIRST_RECORDS_COUNT_SEARCH, UpdateTypeEnum.BEST_TRADERS);
				if(hotBestTradersList.size() < HOT_BEST_TRADERS_SECOND_RECORDS_COUNT_SEARCH){
					logger.debug("The LIST size:" + hotBestTradersList.size() + " < " + HOT_BEST_TRADERS_SECOND_RECORDS_COUNT_SEARCH + " records. Try to get LIST of Hot Best Traders for: " + HOT_BEST_TRADERS_THIRD_HOURS_SEARCH + "h");
					hotBestTradersList = BestHotDataDAO.getHotBestTraders(con, HOT_BEST_TRADERS_THIRD_HOURS_SEARCH, HOT_BEST_TRADERS_FIRST_RECORDS_COUNT_SEARCH, UpdateTypeEnum.BEST_TRADERS);
					if(hotBestTradersList.size() < HOT_BEST_TRADERS_THIRD_RECORDS_COUNT_SEARCH){
						logger.debug("The LIST of Hot Best Traders size:" + hotBestTradersList.size() + " < " + HOT_BEST_TRADERS_THIRD_RECORDS_COUNT_SEARCH + " records. Clear and do nothing!");
						hotBestTradersList.clear();
					}
				}

			}
		} catch (SQLException e) {
			logger.error("When get best traders", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("The Hot Best Traders LIST size:" + hotBestTradersList.size());
		return hotBestTradersList;
	}

	public static ArrayList<FeedMessage> getHotBestTraders(long hours, UpdateTypeEnum msgEnumType) {
		ArrayList<FeedMessage> hotBestTradersList = new ArrayList<FeedMessage>();
		Connection con = null;
		try {
			con = getConnection();
			logger.debug("Try to get LIST of Hot Best Traders 1W/1M/3M for:" + hours + "h");
			hotBestTradersList = BestHotDataDAO.getHotBestTraders(con, hours, HOT_BEST_TRADERS_FIRST_RECORDS_COUNT_SEARCH, msgEnumType);
			if(hotBestTradersList.size() < HOT_BEST_TRADERS_THIRD_RECORDS_COUNT_SEARCH){
				logger.debug("The LIST of Hot Best Traders size:" + hotBestTradersList.size() + " < " + HOT_BEST_TRADERS_THIRD_RECORDS_COUNT_SEARCH + " records. Clear and insert running time now()!");
				FeedManager.insertFeedLastInsertMsgs(QueueTypeEnum.HOT.getId(), msgEnumType.getId());
				hotBestTradersList.clear();
			}
		} catch (SQLException e) {
			logger.error("When get best traders", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("The Hot Best Traders 1W/1M/3M LIST size:" + hotBestTradersList.size());
		return hotBestTradersList;
	}	
	
	public static  ArrayList<FeedMessage> getHotBestTrades() {
		ArrayList<FeedMessage> hotBestTradesList = new ArrayList<FeedMessage>();
		Connection con = null;
		try {
			con = getConnection();
			logger.debug("Try to get LIST of Hot Best Trades for: " + HOT_BEST_TRADES_FIRST_HOURS_SEARCH + "h");
			hotBestTradesList = BestHotDataDAO.getHotBestTrades(con, HOT_BEST_TRADES_FIRST_HOURS_SEARCH, HOT_BEST_TRADES_FIRST_RECORDS_COUNT_SEARCH, UpdateTypeEnum.BEST_TRADES);
			if(hotBestTradesList.size() != HOT_BEST_TRADES_FIRST_RECORDS_COUNT_SEARCH){
				logger.debug("The LIST size:" + hotBestTradesList.size() + "< " + HOT_BEST_TRADES_FIRST_RECORDS_COUNT_SEARCH + " records. Try to get LIST of Hot Best Trades for: " + HOT_BEST_TRADES_SECOND_HOURS_SEARCH + "h");
				hotBestTradesList = BestHotDataDAO.getHotBestTrades(con, HOT_BEST_TRADES_SECOND_HOURS_SEARCH, HOT_BEST_TRADES_FIRST_RECORDS_COUNT_SEARCH, UpdateTypeEnum.BEST_TRADES);
				if(hotBestTradesList.size() < HOT_BEST_TRADES_SECOND_RECORDS_COUNT_SEARCH){
					logger.debug("The LIST size:" + hotBestTradesList.size() + " < " + HOT_BEST_TRADES_SECOND_RECORDS_COUNT_SEARCH + " records. Try to get LIST of Hot Best Trades for: " + HOT_BEST_TRADES_THIRD_HOURS_SEARCH + "h");
					hotBestTradesList = BestHotDataDAO.getHotBestTrades(con, HOT_BEST_TRADES_THIRD_HOURS_SEARCH, HOT_BEST_TRADES_FIRST_RECORDS_COUNT_SEARCH, UpdateTypeEnum.BEST_TRADES);
					if(hotBestTradesList.size() < HOT_BEST_TRADES_THIRD_RECORDS_COUNT_SEARCH){
						logger.debug("The LIST of Hot Best Trades size:" + hotBestTradesList.size() + " < " + HOT_BEST_TRADES_THIRD_RECORDS_COUNT_SEARCH + " records. Clear and do nothing!");
						hotBestTradesList.clear();
					}
				}
			}
		} catch (SQLException e) {
			logger.error("When get best trades", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("The Hot Best Trades LIST size:" + hotBestTradesList.size());
		return hotBestTradesList;
	}

	public static ArrayList<FeedMessage> getHotBestTrades(long hours, UpdateTypeEnum msgEnumType) {
		ArrayList<FeedMessage> hotBestTradesList = new ArrayList<FeedMessage>();
		Connection con = null;

		try {
			con = getConnection();
			logger.debug("Try to get LIST of Hot Best Trades 1W/1M/3M for:" + hours + "h");
			hotBestTradesList = BestHotDataDAO.getHotBestTrades(con, hours, HOT_BEST_TRADES_FIRST_RECORDS_COUNT_SEARCH, msgEnumType);
			if(hotBestTradesList.size() < HOT_BEST_TRADES_THIRD_RECORDS_COUNT_SEARCH){
				logger.debug("The LIST of Hot Best Trades 1W/1M/3M size:" + hotBestTradesList.size() + " < " + HOT_BEST_TRADES_THIRD_RECORDS_COUNT_SEARCH + " records. Clear and insert running time now()!");
				FeedManager.insertFeedLastInsertMsgs(QueueTypeEnum.HOT.getId(), msgEnumType.getId());
				hotBestTradesList.clear();
			}
		} catch (SQLException e) {
			logger.error("When get best trades 1W/1M/3M", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("The Hot Best Traders 1W/1M/3M LIST size:" + hotBestTradesList.size());
		return hotBestTradesList;
	}
	
	public static  ArrayList<FeedMessage> getHotBestCopiers() {
		ArrayList<FeedMessage> hotBestCopiersList = new ArrayList<FeedMessage>();
		Connection con = null;
		try {
			con = getConnection();

			logger.debug("Try to get LIST of Hot Best Copiers for:" + HOT_BEST_COPIERS_FIRST_HOURS_SEARCH + "h");
			hotBestCopiersList = BestHotDataDAO.getHotBestCopiers(con, HOT_BEST_COPIERS_FIRST_HOURS_SEARCH, HOT_BEST_COPIERS_FIRST_RECORDS_COUNT_SEARCH, UpdateTypeEnum.BEST_COPIERS);
			if(hotBestCopiersList.size() != HOT_BEST_COPIERS_FIRST_RECORDS_COUNT_SEARCH){
				logger.debug("The LIST size:" + hotBestCopiersList.size() + "< " + HOT_BEST_COPIERS_FIRST_RECORDS_COUNT_SEARCH + " records. Try to get LIST of Hot Best Copiers for: " + HOT_BEST_COPIERS_SECOND_HOURS_SEARCH + "h");
				hotBestCopiersList = BestHotDataDAO.getHotBestCopiers(con, HOT_BEST_COPIERS_SECOND_HOURS_SEARCH, HOT_BEST_COPIERS_FIRST_RECORDS_COUNT_SEARCH, UpdateTypeEnum.BEST_COPIERS);
				if(hotBestCopiersList.size() < HOT_BEST_COPIERS_SECOND_RECORDS_COUNT_SEARCH){
					logger.debug("The LIST size:" + hotBestCopiersList.size() + " < " + HOT_BEST_COPIERS_SECOND_RECORDS_COUNT_SEARCH + " records. Try to get LIST of Hot Best Copiers for: " + HOT_BEST_COPIERS_THIRD_HOURS_SEARCH + "h");
					hotBestCopiersList = BestHotDataDAO.getHotBestCopiers(con, HOT_BEST_COPIERS_THIRD_HOURS_SEARCH, HOT_BEST_COPIERS_FIRST_RECORDS_COUNT_SEARCH, UpdateTypeEnum.BEST_COPIERS);
					if(hotBestCopiersList.size() < HOT_BEST_COPIERS_THIRD_RECORDS_COUNT_SEARCH){
						logger.debug("The LIST of Hot Best Copiers size:" + hotBestCopiersList.size() + " < " + HOT_BEST_COPIERS_THIRD_RECORDS_COUNT_SEARCH + " records. Clear and do nothing!");
						hotBestCopiersList.clear();
					}
				}
			}
		} catch (SQLException e) {
			logger.error("When get best Copiers", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("The Hot Best Copiers LIST size:" + hotBestCopiersList.size());
		return hotBestCopiersList;
	}

	public static ArrayList<FeedMessage> getHotBestCopiers(long hours, UpdateTypeEnum msgEnumType) {
		ArrayList<FeedMessage> hotBestCopiersList = new ArrayList<FeedMessage>();
		Connection con = null;

		try {
			con = getConnection();
			logger.debug("Try to get LIST of Hot Best Copiers 1W/1M/3M for:" + hours + "h");
			hotBestCopiersList = BestHotDataDAO.getHotBestCopiers(con, hours, HOT_BEST_TRADES_FIRST_RECORDS_COUNT_SEARCH, msgEnumType);
			if(hotBestCopiersList.size() < HOT_BEST_COPIERS_THIRD_RECORDS_COUNT_SEARCH){
				logger.debug("The LIST of Hot Best Copiers 1W/1M/3M size:" + hotBestCopiersList.size() + " < " + HOT_BEST_COPIERS_THIRD_RECORDS_COUNT_SEARCH + " records. Clear and insert running time now()!");
				FeedManager.insertFeedLastInsertMsgs(QueueTypeEnum.HOT.getId(), msgEnumType.getId());
				hotBestCopiersList.clear();
			}
		} catch (SQLException e) {
			logger.error("When get best Copiers 1W/1M/3M", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("The Hot Best Copiers 1W/1M/3M LIST size:" + hotBestCopiersList.size());
		return hotBestCopiersList;
	}
	
	public static  ArrayList<FeedMessage> getSystemHighestHit() {
		ArrayList<FeedMessage> systemHighestHitList = new ArrayList<FeedMessage>();
		Connection con = null;
		try {
			con = getConnection();

			logger.debug("Try to get LIST of *System Highest Hit* for:" + SYSTEM_HIGHEST_HIT_FIRST_HOURS_SEARCH + "h");
			systemHighestHitList = BestHotDataDAO.getSystemHighestHit(con, SYSTEM_HIGHEST_HIT_FIRST_HOURS_SEARCH, SYSTEM_HIGHEST_HIT_FIRST_RECORDS_COUNT_SEARCH);
			if(systemHighestHitList.size() != SYSTEM_HIGHEST_HIT_FIRST_RECORDS_COUNT_SEARCH){
				logger.debug("The LIST size:" + systemHighestHitList.size() + "< " + SYSTEM_HIGHEST_HIT_FIRST_RECORDS_COUNT_SEARCH + " records. Try to get LIST of *System Highest Hit* for: " + SYSTEM_HIGHEST_HIT_SECOND_HOURS_SEARCH + "h");
				systemHighestHitList = BestHotDataDAO.getSystemHighestHit(con, SYSTEM_HIGHEST_HIT_SECOND_HOURS_SEARCH, SYSTEM_HIGHEST_HIT_SECOND_RECORDS_COUNT_SEARCH);
				if(systemHighestHitList.size() < SYSTEM_HIGHEST_HIT_SECOND_RECORDS_COUNT_SEARCH){
					logger.debug("The LIST size:" + systemHighestHitList.size() + " < " + SYSTEM_HIGHEST_HIT_THIRD_RECORDS_COUNT_SEARCH + " records. Try to get LIST of *System Highest Hit* for: " + SYSTEM_HIGHEST_HIT_THIRD_HOURS_SEARCH + "h");
					systemHighestHitList = BestHotDataDAO.getSystemHighestHit(con, SYSTEM_HIGHEST_HIT_THIRD_HOURS_SEARCH, SYSTEM_HIGHEST_HIT_THIRD_RECORDS_COUNT_SEARCH);
					if(systemHighestHitList.size() < SYSTEM_HIGHEST_HIT_THIRD_RECORDS_COUNT_SEARCH){
						logger.debug("The LIST of *System Highest Hit* size:" + systemHighestHitList.size() + " < " + SYSTEM_HIGHEST_HIT_THIRD_RECORDS_COUNT_SEARCH + " records. Clear and do nothing!");
						systemHighestHitList.clear();
					}
				}
			}
		} catch (SQLException e) {
			logger.error("When get *System Highest Hit*", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("The *System Highest Hit* LIST size:" + systemHighestHitList.size());
		return systemHighestHitList;
	}

	public static  ArrayList<FeedMessage> getSystemTrendiestTrends() {
		ArrayList<FeedMessage> systemTrendiestTrends = new ArrayList<FeedMessage>();
		Connection con = null;
		try {
			con = getConnection();
			logger.debug("Try to get LIST of *System Trendiest Trends* ");
			systemTrendiestTrends = BestHotDataDAO.getSystemTrendiestTrends(con, SYSTEM_TRENDIEST_TRENDS_RECORDS_COUNT_SEARCH);
					if(systemTrendiestTrends.size() < SYSTEM_TRENDIEST_TRENDS_MIN_RECORDS_COUNT_SEARCH){
						logger.debug("The LIST of *System Trendiest Trends* size:" + systemTrendiestTrends.size() + " < " + SYSTEM_TRENDIEST_TRENDS_MIN_RECORDS_COUNT_SEARCH + " records. Clear and do nothing!");
						systemTrendiestTrends.clear();
					}
		} catch (SQLException e) {
			logger.error("When get *System Highest Hit*", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("The *System Trendiest Trends* LIST size:" + systemTrendiestTrends.size());
		return systemTrendiestTrends;
	}

	public static  ArrayList<FeedMessage> getSystemHighestProfit(int recNum) {
		ArrayList<FeedMessage> systemHighestProfits = new ArrayList<FeedMessage>();
		try {
			logger.debug("Try to get LIST of *System Trendiest Trends* ");
			ArrayList<FeedMessage> systemHighestProfitsShuffle = (ArrayList<FeedMessage>) FeedManager.getLastHotTableData(UpdateTypeEnum.BEST_TRADERS);
			Collections.shuffle(systemHighestProfitsShuffle);
			int br = 0;
			Map<Long, Integer> insertedUserMsg = FeedManager.getFeedPostedUsersMsgNow();
			
			//Get CopyopStatus
			HashMap<Long, Integer> usersCopyopStatus = new HashMap<Long, Integer>();
			List<Long> userIds = new ArrayList<Long>();
			for(FeedMessage fmUserStatus : systemHighestProfitsShuffle){
				long userId =  new Long(fmUserStatus.getProperties().get(FeedMessage.PROPERTY_KEY_USER_ID));
				userIds.add(userId);
			}
			usersCopyopStatus = UsersManagerBase.getUsersCopyopStatus(userIds);
			
			for(FeedMessage fm : systemHighestProfitsShuffle){
				if(br < recNum){
					fm.setQueueType(QueueTypeEnum.SYSTEM.getId());
					fm.setMsgType(UpdateTypeEnum.SYS_8H_MOST_PROFITABLE.getId());

					if(isOverMessageTreshold(fm)) {
						// No trader could be on the same day in more than 1 msgType
						long userId =  new Long(fm.getProperties().get(FeedMessage.PROPERTY_KEY_USER_ID));
						if (BestHotDataDAO.canBeInsertUser(userId, UpdateTypeEnum.SYS_8H_MOST_PROFITABLE.getId(), insertedUserMsg)
								&& !ProfileManager.isFrozenProfile(userId)
								&& usersCopyopStatus.get(userId) == UserStateEnum.STATE_REGULAR.getId()) {
								systemHighestProfits.add(fm);
								br++;
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("When get *System Highest Profit*", e);
		}
		logger.debug("The *System Trendiest Trends* LIST size:" + systemHighestProfits.size());
		return systemHighestProfits;
	}

	static boolean isOverMessageTreshold(FeedMessage fm) {
		// check cpop_profit_2 > 200$
		boolean messageThreshold = true;
		try{
			Double amountInUSD =  Double.parseDouble(fm.getProperties().get(FeedMessage.PROPERTY_KEY_PROFIT+"_2"));
			Double thresholdHotBestTradersMessage = Double.parseDouble(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_BEST_TRADERS_MESSAGE_TRESHOLD", "200"));
			messageThreshold = amountInUSD > thresholdHotBestTradersMessage;
		} catch(NumberFormatException ex) {
			logger.error("Can't check message threshold", ex);
		}
		
		return messageThreshold;
	}
	public static  ArrayList<FeedMessage> getSystemMostCopied() {
		ArrayList<FeedMessage> systemMostCopiedList = new ArrayList<FeedMessage>();
		try {

			logger.debug("Try to get LIST of *System Most Copied* for:" + SYSTEM_MOST_COPIED_FIRST_HOURS_SEARCH + "h");
			systemMostCopiedList = CopiedManager.getCopiedCounters(SYSTEM_MOST_COPIED_FIRST_HOURS_SEARCH, SYSTEM_MOST_COPIED_FIRST_RECORDS_COUNT_SEARCH);
			if(systemMostCopiedList.size() != SYSTEM_MOST_COPIED_FIRST_RECORDS_COUNT_SEARCH){
				logger.debug("The LIST size:" + systemMostCopiedList.size() + "< " + SYSTEM_MOST_COPIED_FIRST_RECORDS_COUNT_SEARCH + " records. Try to get LIST of *System Most Copied* for: " + SYSTEM_MOST_COPIED_SECOND_HOURS_SEARCH + "h");
				systemMostCopiedList =  CopiedManager.getCopiedCounters(SYSTEM_MOST_COPIED_SECOND_HOURS_SEARCH, SYSTEM_MOST_COPIED_SECOND_RECORDS_COUNT_SEARCH);
					if(systemMostCopiedList.size() < SYSTEM_MOST_COPIED_SECOND_RECORDS_COUNT_SEARCH){
						logger.debug("The LIST of *System Most Copied* size:" + systemMostCopiedList.size() + " < " + SYSTEM_MOST_COPIED_SECOND_RECORDS_COUNT_SEARCH + " records. Clear and do nothing!");
						systemMostCopiedList.clear();
					}
			}
		} catch (Exception e) {
			logger.error("When get *System Most Copied*", e);
		}
		logger.debug("The *System Most Copied* LIST size:" + systemMostCopiedList.size());
		return systemMostCopiedList;
	}

	public static  ArrayList<FeedMessage> getTopProfitable() {
		ArrayList<FeedMessage> topProfitable = new ArrayList<FeedMessage>();
		Connection con = null;
		try {
			con = getConnection();
			topProfitable = BestHotDataDAO.getTopProfitable(con);
		} catch (SQLException e) {
			logger.error("When get *Top Profitable*", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("Top Profitable LIST size:" + topProfitable.size());
		return topProfitable;
	}
}