/**
 * 
 */
package com.anyoption.common.managers;


import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.UsersAwardBonus;
import com.anyoption.common.daos.UsersAwardBonusDAO;

/**
 * The base manager for the OptimovUsers
 * 
 * @author EyalG
 */
public class UsersAwardBonusManagerBase extends BaseBLManager {

	/**
	 * insert Users Award Bonus into db
	 * @param vo UsersAwardBonus
	 * @throws SQLException
	 */
	
	public static void insert(UsersAwardBonus vo) throws SQLException {
		Connection con = getConnection();
		try {
			UsersAwardBonusDAO.insert(con, vo);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * mark that the user receive the bonus
	 * @param id the UsersAwardBonus id 
	 * @throws SQLException
	 */
	public static void updateBonusGranted(long id) throws SQLException {
		Connection con = getConnection();
		try {
			UsersAwardBonusDAO.updateBonusGranted(con, id);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * check if the user have bonus that need to be granted
	 * @param userId the user id
	 * @return UsersAwardBonus the bonus that will be granted
	 * @throws SQLException
	 */
	public static UsersAwardBonus getByUserId(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersAwardBonusDAO.getByUserId(con, userId);
		} finally {
			closeConnection(con);
		}
	}
}
