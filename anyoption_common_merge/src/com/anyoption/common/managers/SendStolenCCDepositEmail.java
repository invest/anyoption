package com.anyoption.common.managers;

import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.anyoption.common.util.CommonUtil;

public class SendStolenCCDepositEmail extends Thread  {
	public static final String PARAM_TRANSACTION_ID = "Transaction ID";
	public static final String PARAM_DATE = "Date";
	public static final String PARAM_CC_NUM_LAST4 = "CC last 4 digits";
	public static final String PARAM_CC_TYPE = "Brand";
	public static final String PARAM_USER_ID = "User ID";
	public static final String PARAM_USER_COUNTRY = "User country";
	public static final String PARAM_WRITER = "Attempted from";
	public static final String PARAM_DECLINE_DESCRIPTION = "Description";
	public static final String PARAM_DECLINE_COMMENTS = "Comments";
	
	public static final String PARAM_EMAIL = "Email";
	public static final String MAIL_FROM ="from";
	public static final String MAIL_TO = "to";
	public static final String MAIL_SUBJECT = "subject";

	private final String  END_OF_LINE = "<br/>";
	private final char DELIMITER = ':' ;

	private final String [] MAIL_FIELDS_LIST = {PARAM_TRANSACTION_ID, PARAM_DATE, PARAM_CC_NUM_LAST4, PARAM_CC_TYPE, PARAM_USER_ID
												,PARAM_USER_COUNTRY, PARAM_WRITER, PARAM_DECLINE_DESCRIPTION , PARAM_DECLINE_COMMENTS};


	public static final Logger logger = Logger.getLogger(SendStolenCCDepositEmail.class);

	private static Hashtable<String, String> serverProperties;
	private static Hashtable<String, String> emailProperties;

	public SendStolenCCDepositEmail(HashMap<String, String> params) throws Exception {
		logger.debug("Trying to send email with the following params: " + params);
		init(params);
	}

	private void init(HashMap<String, String> params) throws Exception {
		// set the server properties
		serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getConfig("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getConfig("email.uname"));
		serverProperties.put("pass", CommonUtil.getConfig("email.pass"));

		// Set the email properties
		emailProperties = new Hashtable<String, String>();
		//Put params from outside params
		emailProperties.put(MAIL_FROM, params.get(PARAM_EMAIL));
		emailProperties.put(MAIL_TO, params.get(MAIL_TO));
		emailProperties.put(MAIL_SUBJECT, params.get(MAIL_SUBJECT));
		emailProperties.put("body", getEmailBody(params));
	}

	public void run() {
		logger.debug("Sending Email ...");
		// send the email
		CommonUtil.sendEmail(serverProperties, emailProperties, null);
		logger.debug("Sending Email completed ");

	}

	public String getEmailBody(HashMap<String, String> params) throws Exception {
		String paramValue;
		String paramKey;
        StringBuffer mailBody = new StringBuffer();
		//If we have params and not empty then we will populate the mail body
		if (null != params && !params.isEmpty()) {
			for ( int i= 0 ; i < MAIL_FIELDS_LIST.length; i++) {
				paramKey = MAIL_FIELDS_LIST[i];
				paramValue = (String) params.get(paramKey);
				if (null != paramValue && !paramValue.equals("")) {
					mailBody.append(paramKey).
							append(DELIMITER).
							append(paramValue).
							append(END_OF_LINE);
				}
			}
		}
		return mailBody.toString();
	}
}