package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.base.MarketGroup;
import com.anyoption.common.daos.MarketsDAOBase;
import com.anyoption.common.daos.SkinsDAOBase;
import com.anyoption.common.enums.CountryStatus;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

public class SkinsManagerBase extends BaseBLManager {
	
	private static final Logger logger = Logger.getLogger(SkinsManagerBase.class);
	protected static Hashtable<Long, Skin> skins;
	protected static Hashtable<String, Skin> skinIdByLocale;
	protected static HashMap<Long, Long> skinsCombinationId;
	private static   HashMap<Long, ArrayList<Long>> visibleSkins;

	private static LocalDate dateSkinGroupsMarkets;
	
	private static HashMap<Long, Long> getSkinCombinations() throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return com.anyoption.common.daos.SkinsDAOBase
					.getSkinCombination(conn);
		} finally {
			closeConnection(conn);
		}
	}

	public static long getCombinationIdtBySkins(long skinId) {
		long combinationId;
		if (null == skinsCombinationId) {
			try {
				skinsCombinationId = SkinsManagerBase.getSkinCombinations();
			} catch (SQLException sqle) {
				logger.error("Can't load skinsCombinationId.", sqle);
			}
		}

		combinationId = skinsCombinationId.get(skinId);
		return combinationId;
	}
	
	public static ArrayList<Long> getSkinsId() throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return com.anyoption.common.daos.SkinsDAOBase.getSkinsId(conn);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static List<Long> getRegulatedAndNonSkinIdsBySkinId(long skinId) {
		ArrayList<Long>  list = new ArrayList<Long>();
		if (skinId == Skin.SKIN_REG_EN || skinId == Skin.SKIN_ENGLISH) {
			list.add(Skin.SKIN_REG_EN);
			list.add(Skin.SKIN_ENGLISH);
			return list;
		} else if (skinId == Skin.SKIN_REG_ES || skinId == Skin.SKIN_SPAIN) {
			list.add(Skin.SKIN_REG_ES);
			list.add(Skin.SKIN_SPAIN);
			return list;
		} else if (skinId == Skin.SKIN_REG_DE || skinId == Skin.SKIN_GERMAN){
			list.add(Skin.SKIN_REG_DE);
			list.add(Skin.SKIN_GERMAN);
			return list;
		} else if (skinId == Skin.SKIN_REG_IT || skinId == Skin.SKIN_ITALIAN){
			list.add(Skin.SKIN_REG_IT);
			list.add(Skin.SKIN_ITALIAN);
			return list;
		} else if (skinId == Skin.SKIN_REG_FR || skinId == Skin.SKIN_FRANCE){
			list.add(Skin.SKIN_REG_FR);
			list.add(Skin.SKIN_FRANCE);
			return list;
		} else {
			list.add(skinId);
			return list;
		}
	}
	
    /**
     * Get skin_id by country_id and url_id
     * 
     * @param con connection to Db
     * @param countryId country id
     * @param urlId url id
     * @return skin id
     * @throws SQLException
     */
    public static int getSkinIdByCountryAndUrl(long countryId, int urlId) throws SQLException {
        Connection conn = getConnection();
        try {
            return SkinsDAOBase.getSkinIdByCountryAndUrl(conn, countryId, urlId);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static Skin getSkin(long id) {
    	getSkins();
        return skins.get(id);
    }
    
    public static Hashtable<Long, Skin> getSkins() {
    	if (null == skins) {
             try {
                skins = SkinsManagerBase.getAll();
                skinIdByLocale = new Hashtable<String, Skin>();
				Skin skin = null;
				for (Iterator<Long> i = skins.keySet().iterator(); i.hasNext();) {
					Long skinId = i.next();
					skin = skins.get(skinId); 
					if (skin.getLocale() != null && 
							(skin.isRegulated() || skin.getId() == Skin.SKIN_CHINESE || skin.getId() == Skin.SKIN_RUSSIAN || skin.getId() == Skin.SKIN_TURKISH || skin.getId() == Skin.SKIN_KOREAN)) {
						skinIdByLocale.put(skin.getLocale(), skin);	
					}				    
				}                 
             } catch (SQLException sqle) {
                 logger.error("Can't load skins.", sqle);
             }
         }
    	 return skins;
    }
    
    /**
     * Return all skins from DB
     *
     * @return <code>Hashtable<Long, Skin></code> of all skins
     * @throws SQLException
     */
    public static Hashtable<Long, Skin> getAll() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return SkinsDAOBase.getAll(conn);
        } finally {
            closeConnection(conn);
        }
    }
    
    /**
	 * Get skin id, first search in the session and after that in the cookies.
	 * @return skinId
	 */
	public static Long getSkinId(HttpSession session, HttpServletRequest request) {
		Long skinId = null;
		String strSkinId = (String)session.getAttribute(ConstantsBase.SKIN_ID);
		if (null != strSkinId) {
			skinId = new Long(strSkinId);
		} else {
			// getting skin ID from cookie
			Cookie[] cs = request.getCookies();
			Cookie cookie = null;
			if (cs != null) {
				for (int i = 0; i < cs.length; i++) {
					if (cs[i].getName().equals(ConstantsBase.SKIN_ID)) {
						logger.log(Level.TRACE, "found skinId cookie!");
						cookie = cs[i];
						break;
					}
				}
			}
			if (null != cookie && cookie.getValue() != null
					&& !cookie.getValue().equals("")
					&& cookie.getValue().indexOf("null") == -1) {
				try {
					skinId = Long.valueOf(cookie.getValue());
				} catch (Exception e) {
					if (logger.isEnabledFor(Level.WARN)) {
						logger.warn("SkinId cookie with strange value!", e);
					}
				}
			}
		}

		// return default skinId in a case skinId not in session and cookie
		if (skinId == null) {
			skinId = Long.valueOf(CommonUtil.getPropertyByHost(request, "skin.default", null));
			logger.debug("SkinId not in session and cookies, Take default skin id: " + skinId );
		}

		String strCountryId = request.getParameter("cid");
		if (strCountryId != null && strCountryId.trim().length() > 0) {
			logger.debug("Found country ID [" + strCountryId
						+ "] as request parameter, trying to change current skin id [" + skinId
						+ "] to a regulated one");
			try {
				long counrtyId = Long.parseLong(strCountryId);
				
				if (CountryManagerBase.getRegulatedCountriesList().contains(counrtyId)) {
					skinId = getRegulatedSkinIdByNonRegulatedSkinId(skinId);
					logger.debug("Country [" + counrtyId + "] is regulated, skin ID reset to [" + skinId + "]");
				} else {
					skinId = getNonRegulatedSkinIdByRegulatedSkinId(skinId);
					logger.debug("Country [" + counrtyId + "] is not regulated, skin ID reset to [" + skinId + "]");
				}
			} catch (Exception e) {
				logger.error("Exception determining skin ID by countryID [" + strCountryId + "]", e);
			}
		}
		
		return skinId;
	}
	
	public static long getRegulatedSkinIdByNonRegulatedSkinId(long skinId) {
		if (skinId == Skin.SKIN_ENGLISH || skinId == Skin.SKIN_ENGLISH_NON_REG) {
			return Skin.SKIN_REG_EN;
		} else if (skinId == Skin.SKIN_SPAIN || skinId == Skin.SKIN_SPAIN_NON_REG) {
			return Skin.SKIN_REG_ES;
		} else if (skinId == Skin.SKIN_GERMAN){
			return Skin.SKIN_REG_DE;
		} else if (skinId == Skin.SKIN_ITALIAN){
			return Skin.SKIN_REG_IT;
		} else if (skinId == Skin.SKIN_FRANCE){
			return Skin.SKIN_REG_FR;
		} else {
			return skinId;
		}
	}
	
	public static long getNonRegulatedSkinIdByRegulatedSkinId(long skinId) {
		if (skinId == Skin.SKIN_REG_EN) {
			return Skin.SKIN_ENGLISH_NON_REG;
		} else if (skinId == Skin.SKIN_REG_ES) {
			return Skin.SKIN_SPAIN_NON_REG;		
		} else {
			return skinId;
		}
	}
	
    /**
     * Get market groups with markets by skinId
     */
    public static Hashtable<Long, MarketGroup> getSkinGroupsMarkets(long skinId) {
    	Hashtable<Long, MarketGroup> result = new Hashtable<Long, MarketGroup>();
			try {
				result = MarketsManagerBase.getSkinGroupsMarkets(skinId);
    		} catch (SQLException sqle) {
    			logger.error("Can't load getSkinGroupsMarkets.", sqle);
    	}
    	return result;
    }
    
    /**
	 * Get HashTable of all binary markets for skin
	 * @param skinId
	 * @return
	 */
	public static Hashtable<Long, Market> getAllBinaryMarketsForSkinHM(long skinId) {
    	getSkins();
    	if (null == skins.get(skinId).getAllBinaryMarketsHM()) {
    		try {
    			((Skin)skins.get(skinId)).setAllBinaryMarketsHM(SkinsManagerBase.getMarketsForSkinByOppType(skinId, Opportunity.TYPE_REGULAR, false)) ;
    		} catch (SQLException sqle) {
    			logger.error("Can't load getAllBinaryMarketsForSkinHM.", sqle);
    		}
    	}
    	return skins.get(skinId).getAllBinaryMarketsHM();  
    }
	
    /**
     * 
     * Get markets for skin by opportunity type
     * @param skinId
     * @param opportunityTypeId
     * @return
     * @throws SQLException
     */
    public static Hashtable<Long, Market> getMarketsForSkinByOppType(long skinId, long opportunityTypeId, boolean onlyHorly) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return MarketsDAOBase.getMarketsForSkinByOppType(conn, skinId, opportunityTypeId, onlyHorly);  
        } finally {
            closeConnection(conn);
        }
    }
    
    public static Skin defineSkinNoUser(String remoteUser, Long sParam, Long subdomainOrNewSelectedSkinId, Locale locale,
    		Long cookieSkinId, Long countryIdDetectByIp, Long sessionSkinId){
    	String ls = System.getProperty("line.separator");
    	logger.debug("DefineSkinNoUser with params "+ ls +
    			" remoteUser[" + remoteUser + "], " + ls + 
    			" sParam[" + sParam + "], " + ls +
    			" subdomainOrNewSelectedSkinId[" + subdomainOrNewSelectedSkinId + "], " + ls +
    			" locale[" + locale + "], " + ls +
    			" cookieSkinId[" + cookieSkinId + "], " + ls +
    			" countryIdDetectByIp[" + countryIdDetectByIp + "], " + ls +
    			" sessionSkinId[" + sessionSkinId + "] ");
		Long skinId = null;
		if (!CommonUtil.isParameterEmptyOrNull(remoteUser)) {
			return null;
		}		
		skinId = getSkinIdForDefine(sParam, subdomainOrNewSelectedSkinId, cookieSkinId, countryIdDetectByIp, sessionSkinId);   
		if(skinId == null){
			skinId = Skin.SKIN_REG_EN;
			logger.debug("Skinid is NULL, set Default ID:" + Skin.SKIN_REG_EN);
			return null;
		}
		
		logger.debug("DefineSkinNoUser return skinId[" + skinId + "]");
    	return getSkin(skinId);
    }

	private static Long getSkinIdForDefine(Long sParam, Long subdomainSkinId, Long cookieSkinId,
			Long countryIdDetectByIp, Long sessionSkinId) {
		Long skinId = null;
		try {
			if (sParam != null) {
				skinId  = sParam;
				logger.debug("Find sParam:[" + sParam + "] and set the SkinId:[" + sParam + "] ");				
				return skinId;
			} else if (sessionSkinId == null) {
				Long skinIdByIp = null;
				if(countryIdDetectByIp != null){
					skinIdByIp = CountriesManagerBase.getSkinIdByCountryId(countryIdDetectByIp);
					logger.debug("SessionSkinId is null, try to define. SkinId:[" + skinIdByIp + "] by IP, country Detect:[" + countryIdDetectByIp + "]");
				}				
				if (cookieSkinId == null) {
					if(subdomainSkinId != null && subdomainSkinId != Skin.SKIN_REG_EN){
						if(skinIdByIp != null){
							Skin skinByIp = SkinsManagerBase.getSkin(skinIdByIp);
							if(!skinByIp.isRegulated()){
								subdomainSkinId = SkinsManagerBase.getNonRegulatedSkinIdByRegulatedSkinId(subdomainSkinId);
							}
						}
						skinId = subdomainSkinId;
						logger.debug("cookieSkinId is null, defined skinId by subdomainSkinId:[" + subdomainSkinId + "]");
						return skinId;
					}
					skinId = skinIdByIp;
					logger.debug("cookieSkinId is null, defined skinId by IP:[" + skinIdByIp + "] from country Detect:[" + countryIdDetectByIp + "]");					
					return skinId;
				} else if(cookieSkinId != null){					
					//Check if have not equal sub domain with current cookie
					Skin cookieSkin = SkinsManagerBase.getSkin(cookieSkinId);
					if(!cookieSkin.isRegulated() && subdomainSkinId != null){
						subdomainSkinId = SkinsManagerBase.getNonRegulatedSkinIdByRegulatedSkinId(subdomainSkinId);
					}
					if(subdomainSkinId != null && subdomainSkinId != Skin.SKIN_REG_EN && subdomainSkinId != Skin.SKIN_ENGLISH_NON_REG){
						skinId = subdomainSkinId;
					} else {
						skinId = cookieSkinId;
					}
					
					logger.debug("cookieSkinId is:[" + cookieSkinId + "], skinId by IP:[" + skinIdByIp + "], sessionSkinId is[null] and retrun cookieSkinId:[" + cookieSkinId + "]");						
					return skinId;
				}
			} else if(sessionSkinId != null){
				skinId = sessionSkinId;
			}
		} catch (SQLException e) {
			logger.error("When defineSkinNoUser", e);
		}
		return skinId;
	}
	
    public static HashMap<Long, ArrayList<Long>> getVisibleSkins() throws SQLException {        
    	if(null == visibleSkins){
        	Connection conn = null;
            try {
                conn = getConnection();
                visibleSkins = SkinsDAOBase.getVisibleSkins(conn);
            } finally {
                closeConnection(conn);
            }
    	}        	
    	return visibleSkins;
    }
    
	public static long getSkinIdInsertUser(String ip, Long countryIdRegFormId, long currentSkinId) {
		long skinId = Skin.SKIN_REG_EN;
		try {
			Long ipDetectCountryId = CommonUtil.getCountryId(ip);
			Country ipDetectCountry = null;
			String ipDetectCountryName = "N/A";
			
			if(ipDetectCountryId != null){
				ipDetectCountry = CountryManagerBase.getById(ipDetectCountryId);
				ipDetectCountryName = ipDetectCountry.getName();
			}
			
			Country countryIdRegForm = null;
			String countryIdRegFormName = "N/A";
			if(countryIdRegFormId != null){
				countryIdRegForm = CountryManagerBase.getById(countryIdRegFormId);
				countryIdRegFormName = countryIdRegForm.getName();
			}									
	    	String ls = System.getProperty("line.separator");
	    	logger.debug("DefineSkinInsertUser with params "+ ls +
	    			" ip[" + ip + "], " + ls + 
	    			" ipDetectCountryName[" + ipDetectCountryName + "], " + ls +
	    			" countryIdRegFormName[" + countryIdRegFormName + "], " + ls +
	    			" currentSkinId[" + currentSkinId + "] ");
	    	
	    	//X:Country from the OB list; Y:Country from the AO non REG list; Z:Country Blocked
	    	// X|X = OB;  X|Y = OB;
	    	if(null != ipDetectCountry && ipDetectCountry.isRegulated()){
	    		return getRegulatedSkinIdByNonRegulatedSkinId(currentSkinId);
	    	}
	    	
	    	// Y|X = OB; NULL|X = OB; 
			if(null != countryIdRegForm && countryIdRegForm.isRegulated()){
				return getRegulatedSkinIdByNonRegulatedSkinId(currentSkinId);
			}
	    	// Y|Y = AO; Y|Z = AO; Z|Y = AO; NULL|Y = AO;
			if(null != countryIdRegForm && !countryIdRegForm.isRegulated()){
				return getNonRegulatedSkinIdByRegulatedSkinId(currentSkinId);
			}
	    	
		} catch (Exception e) {
			logger.error("When getSkinIdInsertUser error return skinId:" + skinId, e);
			
		}
		logger.debug("DefineSkinInsertUser skinID "+ skinId);
		return skinId;
	}
	
	public static Long getSkinListType(String ip, HttpServletRequest httpRequest, Skin s) {
		Long skinListTypeDetect = Skin.SKIN_LIST_OB; 
		//Check for AOPS and Block 
		Long detecCountryIdbyIp = CommonJSONService.getCountryFromIp(ip, httpRequest);
		if(detecCountryIdbyIp != null && !s.isRegulated()){
			Country c = CountryManagerBase.getCountry(detecCountryIdbyIp);
			if(c.getCountryStatus() == CountryStatus.NON_REG_NOTBLOCKED){
				skinListTypeDetect = Skin.SKIN_LIST_BLOCK_COUNTRY;
			} else if (c.getCountryStatus() == CountryStatus.NOTBLOCKED) {
				skinListTypeDetect = Skin.SKIN_LIST_GOLDBEAM;
			}
		}
		return skinListTypeDetect;
	}
	
	/**
	 * @return the skinIdByLocale
	 */
	public static Hashtable<String, Skin> getSkinIdByLocale() {
		getSkins();
		return skinIdByLocale;
	}
}