package com.anyoption.common.managers;



import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.base.CountryMiniBean;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.CountryDAOBase;
import com.anyoption.common.enums.CountryStatus;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

public class CountryManagerBase extends BaseBLManager {
	
	private static final Logger log = Logger.getLogger(CountryManagerBase.class);
	private static Hashtable<Long, Country> countries;
	private static Hashtable<Long, Country> allowedCountries;
	private static Hashtable<String, Country> countryByA3Code;
	private static HashMap<Long, String> countriesFilter;
	private static HashMap<Long, LinkedHashMap<Long, CountryMiniBean>> skinSortedCountriesMap = new HashMap<Long, LinkedHashMap<Long,CountryMiniBean>>();
	
	public static final String FILTER_COUNTRIES = "countries";
	
	/**
	 * @param id
	 * @return
	 */
	public static Country getCountry(long id) {
		getCountries();
	    return countries.get(id);
	}
	
	/**
	 * @return
	 */
	public static Hashtable<Long, Country> getCountries() {
		if (null == countries) {
	        try {
	            countries = CountriesManagerBase.getAll();
	            countryByA3Code = new Hashtable<String, Country>();
	            Country c = null;
				for (Iterator<Long> i = countries.keySet().iterator(); i.hasNext();) {
					Long countryId = i.next();
					c = countries.get(countryId); 
					countryByA3Code.put(c.getA3(), c);
				}
	        } catch (SQLException sqle) {
	            log.error("Can't load countries.", sqle);
	        }
	    }
		return countries;
	}
	
	public static Hashtable<Long, Country> getAllowedCountries() {
		if (null == allowedCountries) {
	        try {
	        	allowedCountries = CountriesManagerBase.getAllAllowedCountries();
	            countryByA3Code = new Hashtable<String, Country>();
	            Country c = null;
				for (Iterator<Long> i = allowedCountries.keySet().iterator(); i.hasNext();) {
					Long countryId = i.next();
					c = allowedCountries.get(countryId); 
					countryByA3Code.put(c.getA3(), c);
				}
	        } catch (SQLException sqle) {
	            log.error("Can't load countries.", sqle);
	        }
	    }
		return allowedCountries;
	}
	
	/**
	 * @param a2
	 * @return
	 */
	public static Country getCountry(String a2) {
	    getCountries();
        // TODO: if this is used a lot we should do a Map for faster lookup
	    Country c = null;
	    for (Iterator<Country> i = countries.values().iterator(); i.hasNext();) {
	        c = i.next();
	        if (c.getA2().equals(a2)) {
	            return c;
	        }
	    }
	    return null;
	}
	
    /**
     * Get phone code by country id
     * @param code
     *      country code
     * @return
     * @throws SQLException
     */
    public static String getPhoneCodeById(long id) throws SQLException {
        Connection con = getConnection();
        try {
            return CountryDAOBase.getPhoneCodeById(con, id);
        } finally {
            closeConnection(con);
        }
    }
    
	/**
	 * @return the countryByCode
	 */
	public static Hashtable<String, Country> getCountryByA3Code() {
		getCountries();
		return countryByA3Code;
	}

	/**
	 * @param countryByCode the countryByCode to set
	 */
	public static void setCountryByA3Code(Hashtable<String, Country> countryByA3Code) {
		CountryManagerBase.countryByA3Code = countryByA3Code;
	}
	

	/**
	 * Get code by country id
	 * @param countryId
	 * @return
	 * @throws SQLException
	 */
	public static String getCodeById(long countryId) throws SQLException {
		Connection con = getConnection();
		try {
			return CountryDAOBase.getCodeById(con, countryId);
		} finally {
			closeConnection(con);
		}
	}


	/**
	 * Get country id by code
	 * @param code
	 * 		country code
	 * @return
	 * @throws SQLException
	 */
	public static long getIdByCode(String code) throws SQLException {
		Connection con = getConnection();
		try {
			return CountryDAOBase.getIdByCode(con, code);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Get country name by id
	 * @param id
	 * 		country id
	 * @return
	 * @throws SQLException
	 */
	public static String getNameById(long id) throws SQLException {
		Connection con = getConnection();
		try {
			return CountryDAOBase.getNameById(con, (int) id);
		} finally {
			closeConnection(con);
		}
	}

    public static Country getById(long countryId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return CountryDAOBase.getById(conn, countryId);
        } finally {
            closeConnection(conn);
        }
    }

    public static Country getById(Connection conn,  long countryId) throws SQLException {
        try {
            return CountryDAOBase.getById(conn, countryId);
        } finally {

        }
    }
    
	public static HashSet<Long> getRegulatedCountriesList() {
		HashSet<Long> set = new HashSet<Long>();
		for (int i = 0; i < ConstantsBase.COUNTRY_ID_REGULATED.length; i++) {
			set.add(new Long(ConstantsBase.COUNTRY_ID_REGULATED[i]));
		}
		return set;
	}
	
	public static HashMap<Long, String> getCountriesFilter() {		
		HashMap<Long, String> countriesFilterHM = (HashMap<Long, String>) getCountries().entrySet().stream()
			     .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getName()));
		return countriesFilterHM;
	}
	
	public static HashMap<Long, String> getAllCountriesFilter() {
		if (countriesFilter == null) {
			countriesFilter = new HashMap<>();
			for (Long key : getCountries().keySet()) {
				countriesFilter.put((long) countries.get(key).getId(), countries.get(key).getDisplayName());
			}
		}
		return countriesFilter;
	}
	
	/**
	 * Get countries group tiers SI
	 * @return ArrayList<SelectItem>
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getCountriesGroupTiersSI() throws SQLException {
		Connection con = getConnection();
		try {
			return CountryDAOBase.getCountriesGroupTiersSI(con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static LinkedHashMap<Long, CountryMiniBean> getSortedCountriesMap(long skinId) {		
		LinkedHashMap<Long, CountryMiniBean> sortedCountriesMap = skinSortedCountriesMap.get(skinId);
		if (sortedCountriesMap == null || sortedCountriesMap.isEmpty()) {			
				LinkedList<CountryMiniBean> unsortedCountryMiniList = new LinkedList<CountryMiniBean>();				
				Locale locale;
				Skin skin = SkinsManagerBase.getSkin(skinId);				
				locale = new Locale(skin.getLocale());
				Collator collator = Collator.getInstance(locale);
				for (Long cId : getCountries().keySet()) {
					Country c = getCountries().get(cId);
					if ((c.getCountryStatus() == CountryStatus.NOTBLOCKED && c.getId() != ConstantsBase.COUNTRY_ID_IL)
							||(!skin.isRegulated() && c.getCountryStatus() == CountryStatus.NON_REG_NOTBLOCKED)) {
						String countryName =com.anyoption.common.util.CommonUtil.getMessage(locale, c.getDisplayName(), null);
						String phoneCode = c.getPhoneCode();
						if (phoneCode.length() > CountryMiniBean.COUNTRY_PHONE_CODE_PREFIX_LENGTH && c.getId()!=CountryMiniBean.COUNTRY_ID_BARBADOS) {
							phoneCode = phoneCode.substring(0, 1);
						}
						CountryMiniBean cmb = new CountryMiniBean(c.getId(), countryName, phoneCode, c.getA2(), 
								collator.getCollationKey(countryName), c.getSupportPhone(), c.getSupportFax(), c.getCountryStatus());
						unsortedCountryMiniList.add(cmb);
					}
				}
				Collections.sort(unsortedCountryMiniList, new CountryMiniBeanComparator<CountryMiniBean>());
				sortedCountriesMap = new LinkedHashMap<Long, CountryMiniBean>(unsortedCountryMiniList.size());
				for (CountryMiniBean cmb : unsortedCountryMiniList) {
					sortedCountriesMap.put(cmb.getId(), cmb);
				}
			skinSortedCountriesMap.put(skinId, sortedCountriesMap);
		}
		return sortedCountriesMap;
	}
	
	private static class CountryMiniBeanComparator<T> implements Comparator<CountryMiniBean> {
		@Override
		public int compare(CountryMiniBean o1, CountryMiniBean o2) {
			return o1.getDisplayNameCollationKey().compareTo(o2.getDisplayNameCollationKey());
		}
	}
	
	public static boolean isCountryForbidenAndIp(String ip, Long skinId, Long countryIdRegForm, Long cookieCountryId) {
		boolean isCountryForbiden = false;
		try {			
			Long countryId = CommonUtil.getCountryId(ip);
			Skin s = SkinsManagerBase.getSkin(skinId);
			log.debug("Check CountryForbidenAndIp with params ip[" + ip + "], detectCountryId[" + countryId + "], skinId[" + skinId + "], "
					+ " countryIdRegForm[" + countryIdRegForm + "], cookieCountryId["+ cookieCountryId +"] " );
			//Set cookie or default country when can't detect by IP
			if(countryId == null && cookieCountryId!= null){
				countryId = cookieCountryId;
			}			
			if(countryId == null){
				countryId = s.getDefaultCountryId();
			}
			
			if(countryId != null && !GeneralManager.getAllowIPList().contains(ip)){
				Country country = CountryManagerBase.getCountry(countryId);
				if(country.getCountryStatus() == CountryStatus.BLOCKED || country.getCountryStatus() == CountryStatus.REJECTED){
					isCountryForbiden = true;
					log.debug("For IP [" + ip + "] detected Blocekd Country [" + country.getName() + "]");
				} else if(country.getCountryStatus() == CountryStatus.NON_REG_NOTBLOCKED && s.isRegulated()) {
					isCountryForbiden = true;
					log.debug("For IP [" + ip + "] detected Blocekd NON_REG Country [" + country.getName() + "] and skinId [" + skinId + "]");
				} else if(countryId == ConstantsBase.COUNTRY_ID_IL){
					isCountryForbiden = true;
					log.debug("For IP [" + ip + "] detected Blocekd Country [Israel] and skinId [" + skinId + "]");
				} else if(null != countryIdRegForm && country.isRegulated() && CountryManagerBase.getCountry(countryIdRegForm).getCountryStatus() == CountryStatus.NON_REG_NOTBLOCKED ){
					isCountryForbiden = true;
					log.debug("For IP [" + ip + "] detected  Country [" + country.getName() + "] and Blocekd NON_REG from reg. form CountryId [" + countryIdRegForm + "]");
				} else if(null != countryIdRegForm && country.getCountryStatus() == CountryStatus.NON_REG_NOTBLOCKED  && CountryManagerBase.getCountry(countryIdRegForm).isRegulated()){
					isCountryForbiden = true;
					log.debug("For IP [" + ip + "] detected Blocekd NON_REG Country [" + country.getName() + "] and Regulted from reg. form CountryId [" + countryIdRegForm + "]");
				}
			}
		} catch (Exception e) {
			log.error("When check isCountryForbidenAndIp", e);
		}
		return isCountryForbiden;
	}
	
	public static boolean isCanLoginRegUser(long userSkinId, String ip, long userId){
		boolean res = true;
		//If try to login he can, on Monday 14.11.2016 must be remove
		if(userId > 0){
			return res;
		}		
		Skin userSkin = SkinsManagerBase.getSkin(userSkinId);
		String ls = System.getProperty("line.separator");
		if(userSkin.getId() == Skin.SKIN_TURKISH 
				|| userSkin.getId() == Skin.SKIN_RUSSIAN 
					|| userSkin.getId() == Skin.SKIN_CHINESE 
						|| userSkin.getId() == Skin.SKIN_KOREAN){
			res = false;
			log.debug("isCanLoginRegUser for FORBIDDEN SKIN with  params "+ ls +
					" userId[" + userId + "], " + ls +
	    			" userSkinId[" + userSkinId + "], " + ls + 
	    			" ip[" + ip + "], " + ls +
	    			" isCanLoginRegUser[" + res + "] ");
			return res;
		}
		
		Long ipDetectCountryId = CommonUtil.getCountryId(ip);				
		if(ipDetectCountryId != null){
			try {
				Country c = CountryManagerBase.getById(ipDetectCountryId);
				//Check for Rejected(1) countries				
				if(c.getCountryStatus() == CountryStatus.REJECTED){
					res = false;
					log.debug("isCanLoginRegUser REJECTED COUNRTY with params "+ ls +
							" userId[" + userId + "], " + ls +
			    			" userSkinId[" + userSkinId + "], " + ls + 
			    			" ip[" + ip + "], " + ls +
			    			" detectedCountry[" + c.getName()  + "], " + ls +
			    			" isCanLoginRegUser[" + res + "] ");
					return res;
				}
				//Check for REG users from  blocked(3) countries
				if(userSkin.isRegulated() && c.getCountryStatus() == CountryStatus.NON_REG_NOTBLOCKED){
					res = false;
					log.debug("isCanLoginRegUser with params "+ ls +
							" userId[" + userId + "], " + ls +
			    			" userSkinId[" + userSkinId + "], " + ls + 
			    			" ip[" + ip + "], " + ls +
			    			" detectedCountry[" + c.getName()  + "], " + ls +
			    			" isCanLoginRegUser[" + res + "] ");
					return res;
				}
			} catch (SQLException e) {
				log.error("Can't get country when check ipDetectCountryId", e);
			}
		}
		return res;
	}    
}