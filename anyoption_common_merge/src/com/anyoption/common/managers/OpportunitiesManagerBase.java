/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityMiniBean;
import com.anyoption.common.beans.OpportunityOddsType;
import com.anyoption.common.beans.base.OpportunityOddsPair;
import com.anyoption.common.daos.OpportunitiesDAOBase;
import com.anyoption.common.daos.OpportunityOddsTypesDAOBase;
import com.anyoption.common.util.OpportunityCacheBean;

/**
 * @author pavelhe
 *
 */
public class OpportunitiesManagerBase extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(OpportunitiesManagerBase.class);
	private static ArrayList<OpportunityOddsPair> opportunityOddsPairSelector;
	private static Calendar lastLoadOpportunityOddsPairSelector;
	private static Date lastLoadOpportunityOddsPairSelectorDate;

	private static Map<Long, OpportunityOddsType> opportunityOddsTypes;
	private static Date lastLoadOpportunityOddsTypesDate;

	private static void loadOpportunityOddsPairSelector() {
		Connection con = null;
		try {
			con = getConnection();
			opportunityOddsPairSelector = OpportunitiesDAOBase.getOpportunityOddsPair(con);
			lastLoadOpportunityOddsPairSelector = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			lastLoadOpportunityOddsPairSelectorDate = lastLoadOpportunityOddsPairSelector.getTime();
		} catch (SQLException e) {
			logger.error("Error Loading Opportunity OddsPairSelector", e);;
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<OpportunityOddsPair> getOpportunityOddsPairSelector() {
		if (opportunityOddsPairSelector == null || lastLoadOpportunityOddsPairSelector == null
				|| lastLoadOpportunityOddsPairSelectorDate == null) {
			loadOpportunityOddsPairSelector();
		}
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		Date dateNow = new Date();
		dateNow = cal.getTime();
		int days = (int) ((dateNow.getTime() - lastLoadOpportunityOddsPairSelectorDate.getTime()) / (1000 * 60 * 60 * 24));
		if (days > 0) {
			loadOpportunityOddsPairSelector();
		}
		return opportunityOddsPairSelector;
	}

	public static OpportunityOddsType getOpportunityOddsType(long id) {
		if (opportunityOddsTypes == null || lastLoadOpportunityOddsTypesDate == null) {
			loadOpportunityOddsType();
		}
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		Date dateNow = new Date();
		dateNow = cal.getTime();
		int days = (int) ((dateNow.getTime() - lastLoadOpportunityOddsTypesDate.getTime()) / (1000 * 60 * 60 * 24));
		if (days > 0) {
			loadOpportunityOddsType();
		}
		return opportunityOddsTypes.get(id);
	}
	
	public static Map<Long, OpportunityOddsType> getOpportunityOddsTypes() {
		if (opportunityOddsTypes == null || lastLoadOpportunityOddsTypesDate == null) {
			loadOpportunityOddsType();
		}
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		Date dateNow = new Date();
		dateNow = cal.getTime();
		int days = (int) ((dateNow.getTime() - lastLoadOpportunityOddsTypesDate.getTime()) / (1000 * 60 * 60 * 24));
		if (days > 0) {
			loadOpportunityOddsType();
		}
		return opportunityOddsTypes;
	}

	private static void loadOpportunityOddsType() {
		Connection con = null;
		try {
			con = getConnection();
			opportunityOddsTypes = OpportunityOddsTypesDAOBase.getTypes(con);
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			lastLoadOpportunityOddsTypesDate = cal.getTime();
		} catch (SQLException e) {
			logger.error("Failed loading opportunity odds types", e);
		} finally {
			closeConnection(con);
		}
	}

	public static OpportunityMiniBean getOpportunityMiniBean(long id) {
		Connection con = null;
		try {
			con = getConnection();
			return OpportunitiesDAOBase.getOpportunityMiniBean(con, id);
		} catch (SQLException e) {
			logger.error("Failed getting opportunity with id = " + id, e);
		} finally {
			closeConnection(con);
		}
		return null;
	}

	public static List<Opportunity> getPastExpiredOpportunities(long after) {
		Connection con = null;
		try {
			con = getConnection();
			return OpportunitiesDAOBase.getPastExpiredOpportunities(con, after);
		} catch (SQLException | ParseException e) {
			logger.debug("Unable to load past expiries", e);
			return new ArrayList<Opportunity>();
		} finally {
			closeConnection(con);
		}
	}

	public static List<Opportunity> getPastExpiredOppsForCacheInit(int count) {
		Connection con = null;
		try {
			con = getConnection();
			return OpportunitiesDAOBase.getPastExpiredOppsForCacheInit(con, count);
		} catch (SQLException | ParseException e) {
			logger.debug("Unable to load past expiries", e);
			return new ArrayList<Opportunity>();
		} finally {
			closeConnection(con);
		}
	}
	
    public static OpportunityCacheBean getOpportunityCacheBean(long oppId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAOBase.getOpportunityCacheBean(conn, oppId);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static long getMarketCurrentOpportunityBinary0100(long marketId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAOBase.getMarketCurrentOpportunityBinary0100(conn, marketId);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static long getBinary0100CurrentMarket(long skinId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAOBase.getBinary0100CurrentMarket(conn, skinId);
        } finally {
            closeConnection(conn);
        }
	}
    
	public static void createOpportunitiesForMarket(long marketId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			OpportunitiesDAOBase.createOpportunitiesForMarket(conn, marketId);
		} finally {
			closeConnection(conn);
		}
	}

	public static List<Opportunity> initPastExpiredOppsForProlongedGraph(List<Integer> scheduledTypes, Date after, boolean order) {
		Connection con = null;
		try {
			con = getConnection();
			return OpportunitiesDAOBase.initPastExpiriesForProlongedGraph(con, scheduledTypes, after, order);
		} catch (SQLException | ParseException e) {
			logger.debug("Unable to load past expiries for prolonged graph", e);
			return new ArrayList<Opportunity>();
		} finally {
			closeConnection(con);
		}
	}
	
	public static OpportunityOddsPair getDefaultOpportunityOddsPair(String oddsGroup) {
		OpportunityOddsPair defaultValues = null;
		long id = 0;
		try {
			String[] array = oddsGroup.split(" ");
			
			for (int i = 0; i < array.length; i++) {
			    if(array[i].contains("d")){
			    	id =  new Long(array[i].replace("d", ""));
			    }
			}			
			if(id > 0){
				for (OpportunityOddsPair opportunityOddsPair : getOpportunityOddsPairSelector()) {
					if(opportunityOddsPair.getSelectorID() == id){
						defaultValues = opportunityOddsPair;
					}
				}
			}			
		} catch (Exception e) {
			logger.error("When try to get default OpportunityOddsPair", e);
		}
		return defaultValues;
	}
}