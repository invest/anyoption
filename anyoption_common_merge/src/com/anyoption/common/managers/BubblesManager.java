package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.BubblesDAO;


/**
 * @author ivannp
 *
 */
public class BubblesManager extends BaseBLManager {
	protected static Logger log = Logger.getLogger(BubblesManager.class);
	
	protected static ArrayList<SelectItem> bubbleMarkets;
	protected static Map<Long, String> bubbleLevels;

	public static Map<Long, String> getBubbleLevels() {
		if (null == bubbleLevels) {
			Connection con = null;
			try {
				try {
					con = getConnection();
					bubbleLevels = BubblesDAO.getBubblesLevels(con);
				} catch (SQLException e) {
					log.error("Cannot load Bubbles pricing levels.", e);
				}
			} finally {
	            closeConnection(con);
	        }
		}
		return bubbleLevels;
	}
	
	public static ArrayList<SelectItem> getBubbleMarkets(long skinId) {
		if (null == bubbleMarkets) {
			Connection con = null;
			try {
				try {
					con = getConnection();
					bubbleMarkets = BubblesDAO.getBubbleMarkets(con, skinId);
				} catch (SQLException e) {
					log.error("Cannot load bubble markets: ", e);
				}
			} finally {
	            closeConnection(con);
	        }
		}
		return bubbleMarkets;
	}

}
