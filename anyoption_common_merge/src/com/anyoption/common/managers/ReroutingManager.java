/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.ReroutingDao;

public class ReroutingManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(ReroutingManager.class);	
	private static HashMap<Long, Long> providersClasses;
	private static HashMap<Long, Set<String>> errCodes;
	private static Set<String> errComments;
	
	public static ArrayList<Long> getReroutingProviders(long transactionId) throws SQLException {
	    Connection con = getConnection();
		try {
			ArrayList<Long> res = ReroutingDao.getReroutingProviders(con, transactionId);
			logger.debug("Rerouting clearingProviders for transactionId [" + transactionId + "] is : " + Arrays.toString(res.toArray()));
			return res;
		} finally {
			closeConnection(con);
		}
	}
	
	public static Boolean isNotBeReroutedErrCode(long clearingProviderId, String errCodse) throws SQLException{
		boolean res = false;
		logger.debug("Check Rerouting ErrCode for clearingProviderId[" + clearingProviderId + "] exist errCode[" + errCodse + "]");
		Long classesId = getProvidersClasses().get(clearingProviderId);	
		if(classesId != null){
			if(getExceptErrCodes().get(classesId) != null && getExceptErrCodes().get(classesId).contains(errCodse)){
				res = true;
				logger.debug("For clearingProviderId [" + clearingProviderId + "] exist errCode [" + errCodse + "] and can't make Rerouting");
			}
		}
		return res;
	}
	
	public static Boolean isNotBeReroutedErrComment(String transactionComment, long transactionId) throws SQLException{
		boolean res = false;
		logger.debug("Check Rerouting ErrComment for transactionComment[" + transactionComment + "] and transactionId[" + transactionId + "]");	
		for (String stringErr : getExceptErrComments()) {
			if(transactionComment.indexOf(stringErr) != -1){
				res = true;
				logger.debug("For transactionComment [" + transactionComment + "] and transactionId [" + transactionId + "] exist errCode [" + stringErr + "] and can't make Rerouting");
				break;
			}
		}
		return res;
	}
	
	private static HashMap<Long, Long> getProvidersClasses() throws SQLException {
	    Connection con = getConnection();
		try {
			if(providersClasses == null){
				providersClasses = ReroutingDao.getProvidersClasses(con); 
			}			
			return providersClasses;
		} finally {
			closeConnection(con);
		}
	}
	
	private static HashMap<Long, Set<String>> getExceptErrCodes() throws SQLException {
	    Connection con = getConnection();
		try {
			if(errCodes == null){
				errCodes = ReroutingDao.getExceptErrCodes(con); 
			}			
			return errCodes;
		} finally {
			closeConnection(con);
		}
	}
	
	private static Set<String> getExceptErrComments() throws SQLException {
	    Connection con = getConnection();
		try {
			if(errComments == null){
				errComments = ReroutingDao.getExceptErrComments(con); 
			}			
			return errComments;
		} finally {
			closeConnection(con);
		}
	}
}
