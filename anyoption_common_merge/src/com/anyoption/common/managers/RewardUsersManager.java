package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardTier.TierChanges;
import com.anyoption.common.beans.RewardUser;
import com.anyoption.common.beans.RewardUserHistory;
import com.anyoption.common.daos.RewardDAO;
import com.anyoption.common.daos.RewardUserBenefitsDAO;
import com.anyoption.common.daos.RewardUsersDAO;
import com.anyoption.common.daos.RewardUsersHistoryDAO;
import com.anyoption.common.rewards.RewardUtil;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author liors
 *
 */
public class RewardUsersManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(RewardUsersManager.class);
	public static boolean insertValueObject(RewardUser rewardUser) throws SQLException{
		Connection connection = getConnection();
		try {
			return RewardUsersDAO.insertValueObject(connection, rewardUser);
		} finally {
			closeConnection(connection);
		} 
	}
	/**
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static RewardUser getRewardUser(long userId) throws SQLException{
		Connection connection = getConnection();
		try {
			return RewardUsersDAO.getRewardUser(connection, userId);
		} finally {
			closeConnection(connection);
		} 
	}
	
	/**
	 * @param userId
	 * @param tierId
	 * @param experiencePoints
	 * @param isMoveTier
	 * @return
	 * @throws SQLException
	 */
	public static boolean update(long userId, int tierId, double experiencePoints, boolean isMoveTier) throws SQLException{
		Connection connection = getConnection();
		try {
			return RewardUsersDAO.update(connection, userId, tierId, experiencePoints, isMoveTier);
		} finally {
			closeConnection(connection);
		} 
	}
	
	/**
	 * assumption - The given experiencePoints not equal to the user experience points
	 * db: 
	 * 		atomic operation.
	 * 		change user Experience Points
	 * 		grant reward if user move tier up
	 * 		demotion - if user move tier down
	 * 		insert to history when needed.
	 * 
	 * @param userId
	 * @param tierId
	 * @param experiencePoints
	 * @param isMoveTier
	 * @return
	 * @throws Exception  
	 */
	public static boolean updateUserExperiencePoints(RewardUser rewardUser, double experiencePoints, int writerId, Class<?> bonusManagerClass, String historyComment, int historyTypeId) throws Exception {
		Connection connection = getConnection();
		
		try {
			connection.setAutoCommit(false);
			updateUserExperiencePoints(connection, rewardUser, experiencePoints, writerId, bonusManagerClass, historyComment, historyTypeId);
			connection.commit();			
		} catch (Exception e) {	
			try {
				logger.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "Problem to change user Exp Pnt", e);
				connection.rollback();
			} catch (SQLException sqe) {
				logger.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "in updateUserExperiencePoints, Can't rollback.", sqe);
			}
			throw new Exception();			
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException sqe) {
				logger.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "in bonusCleanup, Can't set AutoCommit true", sqe);
				throw new Exception();
			}
			closeConnection(connection);
		} 	
		return true;
	}
	
	public static boolean updateUserExperiencePoints(Connection connection, RewardUser rewardUser, double experiencePoints, int writerId, Class<?> bonusManagerClass, String historyComment, int historyTypeId) throws Exception {
		
		boolean isSkipped = false;
		int tierId = 0;
		TierChanges tierChanges = new TierChanges(rewardUser.getTierId(), false);		
		tierId = RewardDAO.getTierIdByExperiencePoints(connection, experiencePoints);
		if (rewardUser.getTierId() != tierId) {
			tierChanges.setTierChanges(tierId, true);
			if (tierId - rewardUser.getTierId() > 1) {
				isSkipped = true;
				logger.info(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "jump tier more than 1, isSkipped: " + isSkipped);
			}
		}
			
		RewardUsersDAO.update(connection, rewardUser.getUserId(), tierId, experiencePoints, tierChanges.isMoveTier());
		//insert to history
		RewardUserHistory rewardUserHistory = new RewardUserHistory(rewardUser.getUserId(), historyTypeId, writerId, experiencePoints, rewardUser.getTierId());
		rewardUserHistory.setComment(historyComment);
		rewardUserHistory.setAmountExperiencePoints(experiencePoints - rewardUser.getExperiencePoints());
		RewardUsersHistoryDAO.insertValueObject(connection, rewardUserHistory);
		rewardUser.setExperiencePoints(experiencePoints);
		logger.info(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "update userId: " + rewardUser.getUserId() + ", experience points: " + experiencePoints);

		if (tierChanges.isMoveTier()) {
			rewardUserHistory.setRewardTierId(tierId);
			rewardUserHistory.setAmountExperiencePoints(0);			
			if (tierChanges.getTierId() > rewardUser.getTierId()) {//up
				rewardUserHistory.setRewardHistoryTypeId(RewardUsersHistoryManager.PROMOTE_TIER);
				RewardUtil.grantRewards(connection, rewardUser.getUserId(), writerId, tierId, isSkipped, bonusManagerClass, rewardUser.getExperiencePoints());
			} else if (tierChanges.getTierId() < rewardUser.getTierId()) {//down - demotion
				rewardUserHistory.setRewardHistoryTypeId(RewardUsersHistoryManager.DEMOTE_TIER);
				RewardUsersHistoryDAO.updateHistoryRemoveRewards(connection, rewardUser.getUserId(), tierChanges.getTierId(), writerId);
				RewardUserBenefitsDAO.updateRemoveRewards(connection, rewardUser.getUserId(), tierChanges.getTierId());
			}
			RewardUsersHistoryDAO.insertValueObject(connection, rewardUserHistory);
			rewardUser.setTierId(tierId);
			logger.info(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "update userId: " + rewardUser.getUserId() + " tierId: " + tierId);
		}
		return isSkipped;
	}
}