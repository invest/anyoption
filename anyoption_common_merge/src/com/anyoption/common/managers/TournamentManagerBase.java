/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Tournament;
import com.anyoption.common.beans.TournamentLanguageTabs;
import com.anyoption.common.beans.TournamentsBanners;
import com.anyoption.common.daos.TournamentDAOBase;

/**
 * @author pavelt
 *
 */
public class TournamentManagerBase extends BaseBLManager {
	
	private static final Logger log = Logger.getLogger(TournamentManagerBase.class);
	
	private static ArrayList<Tournament> tournaments;
	private static Calendar lastUpdate;
	private static ArrayList<TournamentLanguageTabs> tournamentLangTabs;

	public static ArrayList<Tournament> getAllTournaments() throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return TournamentDAOBase.getAllTournaments(conn, false);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static ArrayList<TournamentsBanners> getTournamentsBanners() throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return TournamentDAOBase.getTournamentsBanners(conn);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static ArrayList<Tournament> getAllTournamentSkinLang(ArrayList<Tournament> tournamentList) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return TournamentDAOBase.getAllTournamentSkinLang(conn, tournamentList);
		} finally {
			closeConnection(conn);
		}
	}
	
	/**
	 * get all active tournaments from cache Tournaments will update every 24 hours
	 * @return list of active tournaments
	 * @throws SQLException
	 */
	public static ArrayList<Tournament> getAllTournamentsCache() throws SQLException {
		log.debug("getAllTournamentsCache tournaments= " + tournaments);
		Calendar lastHour = Calendar.getInstance();
		lastHour.set(Calendar.MINUTE, 0);
		lastHour.set(Calendar.MILLISECOND, 0);
		lastHour.add(Calendar.HOUR_OF_DAY, -1);
		if (null == tournaments || lastUpdate.before(lastHour)) {		
			Connection conn = null;
			try {
				conn = getConnection();
				tournaments = TournamentDAOBase.getAllTournaments(conn, true);
				tournaments = TournamentDAOBase.getAllTournamentSkinLang(conn, tournaments);
				Calendar now = Calendar.getInstance();
				now.set(Calendar.MINUTE, 0);
				now.set(Calendar.MILLISECOND, 0);
				lastUpdate = now;
			} finally {
				closeConnection(conn);
			}
		}
		log.debug("getAllTournamentsCache lastUpdate= " + lastUpdate.toString());
		return tournaments;
	}
	
	public static ArrayList<TournamentLanguageTabs> getLangTabs() {
		Connection conn = null;
		try {
			conn = getConnection();
			tournamentLangTabs = TournamentDAOBase.getLangTabs(conn);
		} catch (SQLException ex) {
			log.error("Can't lazy load tournament language tabs", ex);
		} finally {
			closeConnection(conn);
		}
		return tournamentLangTabs;
	}
}
