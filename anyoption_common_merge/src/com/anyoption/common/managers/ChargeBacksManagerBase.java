package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import com.anyoption.common.bl_vos.ChargeBack;
import com.anyoption.common.daos.ChargeBacksDAO;

public class ChargeBacksManagerBase extends BaseBLManager {
	
	private static HashMap<Long,ChargeBack> chargebackStatuses;
	
    public static ChargeBack getById(long id) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return ChargeBacksDAO.getById(conn, id);
        } finally {
            closeConnection(conn);
        }
    }
    
	public static HashMap<Long,ChargeBack> getAllStatuses() throws SQLException {
		if (null == chargebackStatuses) {
			Connection con = null;
			try {
				con = getConnection();
				chargebackStatuses = ChargeBacksDAO.getAllStatuses(con);
			} finally {
				closeConnection(con);
			}
		}
		return chargebackStatuses;
	}
}