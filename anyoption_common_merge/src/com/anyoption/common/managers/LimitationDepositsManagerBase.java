package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.LimitationDeposits;
import com.anyoption.common.daos.LimitationDepositsDAOBase;

public class LimitationDepositsManagerBase extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(LimitationDepositsManagerBase.class);

    public static LimitationDeposits getLimitByUserId(long userId) throws SQLException {
        Connection con = getConnection();
        try {
            return getLimitByUserId(con, userId);
        } finally {
            closeConnection(con);
        }
    }

    public static LimitationDeposits getLimitByUserId(Connection con, long userId) throws SQLException {
        long remarktingId = LimitationDepositsDAOBase.isUserInRemarketingByUserId(con, userId);
        return LimitationDepositsDAOBase.getLimitByUserId(con, userId, remarktingId);
    }
    
    public static LimitationDeposits getLimit(long skinId, long currencyId) {
        Connection con = null;
        try { 
        		con = getConnection();
        		return LimitationDepositsDAOBase.getLimit(con, skinId, currencyId);
        } catch (SQLException ex) {
        	return null;
        } finally {
            closeConnection(con);
        }
    }
}