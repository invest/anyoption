package com.anyoption.common.managers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.SilverPopContacts;
import com.anyoption.common.beans.SilverPopUsers;
import com.anyoption.common.daos.SilverPopDAOBase;

public class SilverPopManager  extends BaseBLManager{
    
    private static final Logger logger = Logger.getLogger(SilverPopManager.class);
    private static final String DELEMITER = ",";
    private static final int FETCH_ROWS = 100;
    
    public static String exportUsers(String filePath, HashMap<String, String> currSymbols, String testEmail) throws SQLException {
        Connection con = getConnection();
        try {
            return SilverPopDAOBase.exportUsers(con, filePath, currSymbols, testEmail);
        } finally {
            closeConnection(con);
        }
    }
    
    public static String exportContacts(String filePath, String testEmail) throws SQLException {
        Connection con = getConnection();
        try {
            return SilverPopDAOBase.exportContacts(con, filePath, testEmail);
        } finally {
            closeConnection(con);
        }
    }
    
    private static void updatetUserContactByEmail(ArrayList<SilverPopUsers> usersList) throws SQLException{
        Connection con = getConnection();
        con.setAutoCommit(false);
        try {
            SilverPopDAOBase.updatetUserContactByEmail(con, usersList);
            con.commit();
        } finally {
            try {
                con.setAutoCommit(true);
            } catch (Throwable t) {
                logger.error("Can't set back to autocommit.", t);
            }
            closeConnection(con);
        }
    }
    
    private static void updatetContactByEmail(ArrayList<SilverPopContacts> contactsList) throws SQLException{
        Connection con = getConnection();
        con.setAutoCommit(false);
        try {
            SilverPopDAOBase.updatetContactByEmail(con, contactsList);
            con.commit();
        } finally {
            try {
                con.setAutoCommit(true);
            } catch (Throwable t) {
                logger.error("Can't set back to autocommit.", t);
            }
            closeConnection(con);
        }
    }
    
    public static void importUsers(String filePathName) throws SQLException {
        BufferedReader fr = null;
        ArrayList<SilverPopUsers> usersList = new ArrayList<SilverPopUsers>();
        int br = 0;
        try {
            logger.info("Silver Pop Begin import users file: " + filePathName);
            fr = new BufferedReader(new FileReader(filePathName));
            String users = new String();
            String[] usrArr;
            fr.readLine(); // skip headers
            while ((users = fr.readLine()) != null) {
                br++;
                usrArr = users.split(DELEMITER);
                SilverPopUsers silverPopUsers = new SilverPopUsers();
                if(usrArr[0]!=null && usrArr[0].startsWith("\"")&&usrArr[0].endsWith("\"")) {
                	usrArr[0]=usrArr[0].substring(1, usrArr[0].length()-1);
                }
                silverPopUsers.setUserId(Long.parseLong(usrArr[0]));
                silverPopUsers.setIsContactByEmail(0);
                usersList.add(silverPopUsers);
                if (br == FETCH_ROWS) {
                    br = 0;
                    updatetUserContactByEmail(usersList);
                    usersList.clear();
                }
            }
            if (usersList.size() > 0) {
                updatetUserContactByEmail(usersList);
            }
            logger.info("Silver Pop End import users file: " + filePathName);
        } catch (Exception e) {
            logger.error("Silver Pop import Users:", e);
        } finally {
            try {
                fr.close();
            } catch (IOException e) {
                logger.error("Silver Pop import Contacts when close file:", e);
            }
        }
    }
    
    public static void importContacts(String filePathName) throws SQLException {
        BufferedReader fr = null;
        ArrayList<SilverPopContacts> contactsList = new ArrayList<SilverPopContacts>();
        int br = 0;
        try {
            logger.info("Silver Pop Begin import contacts file: " + filePathName);
            fr = new BufferedReader(new FileReader(filePathName));
            String contact = new String();
            String[] cntArr;
            fr.readLine(); // skip headers
            while ((contact = fr.readLine()) != null) {
                br++;
                cntArr = contact.split(DELEMITER);
                SilverPopContacts silverPopContacts =  new SilverPopContacts();
                if(cntArr[0]!=null && cntArr[0].startsWith("\"")&&cntArr[0].endsWith("\"")) {
                	cntArr[0]=cntArr[0].substring(1, cntArr[0].length()-1);
                }
                silverPopContacts.setContactId(Long.parseLong(cntArr[0]));
                silverPopContacts.setIsContactByEmail(0);
                contactsList.add(silverPopContacts);
                if (br == FETCH_ROWS) {
                    br = 0;
                    updatetContactByEmail(contactsList);
                    contactsList.clear();
                }
            }
            if (contactsList.size() > 0) {
                updatetContactByEmail(contactsList);
            }
            logger.info("Silver Pop End import contacts file: " + filePathName);
        } catch (Exception e) {
            logger.error("Silver Pop import Contacts:", e);
        } finally {
            try {
                fr.close();
            } catch (IOException e) {
                logger.error("Silver Pop import Contacts when close file:", e);
            }
        }
    }
}
