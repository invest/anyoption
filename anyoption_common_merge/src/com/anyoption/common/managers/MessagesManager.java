package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Message;
import com.anyoption.common.daos.MessagesDAO;
import com.anyoption.common.util.CommonUtil;

/**
 * Messages web manager.
 *
 * @author Tony
 */
public class MessagesManager extends BaseBLManager {
	
	private static final Logger logger = Logger.getLogger(MessagesManager.class);
	
	protected static ArrayList<Message> messages;
    protected static long messagesLoadTime;
    protected static long messagesRefreshPeriod;
	
    /**
     * Load all current messages from the db.
     *
     * @return
     * @throws SQLException
     */
    public static ArrayList<Message> getMessages() throws SQLException {
        ArrayList<Message> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = MessagesDAO.getAllMessages(conn);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

	public static ArrayList<Message> getAllMessages() throws SQLException {
		if (messages == null) {
			try {
				messagesRefreshPeriod = CommonUtil.getPropertyLong("tradingpages.messages.reload.period");
				messages = MessagesManager.getMessages();
				messagesLoadTime = System.currentTimeMillis();
			} catch (SQLException sqle) {
				logger.error("Can't load trading pages messages.", sqle);
			}
		} else if (System.currentTimeMillis() - messagesLoadTime > messagesRefreshPeriod) {
			if (logger.isDebugEnabled()) {
				logger.debug("Realod page messages. Refresh period: " + messagesRefreshPeriod);
			}
			try {
				messagesRefreshPeriod = CommonUtil.getPropertyLong("tradingpages.messages.reload.period");
				messages = MessagesManager.getMessages();
				messagesLoadTime = System.currentTimeMillis();
			} catch (SQLException sqle) {
				logger.error("Can't load trading pages messages.", sqle);
			}
		}
		return messages;
	}
	
    public static Message getMessage(long webScreen, long skinId) throws SQLException {
        Message msg = null;
        ArrayList<Message> messagesList = getAllMessages();
        try {
            if (null != messagesList) {
                Date now = new Date();
                Message m = null;
                for (int i = 0; i < messagesList.size(); i++) {
                    m = messagesList.get(i);
                    if (m.getWebScreen() == webScreen && m.getStartEffDate().before(now) &&
                        m.getEndEffDate().after(now) && m.getSkinId() == skinId) {
                        msg = m;
                        break;
                    }
                }
            }
        } catch (Exception e) {
        	 logger.error("Can't load msg for user.", e);
        }
        return msg;
    }
}