/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.UsersAutoMailDaoBase;

public class UsersAutoMailDaoManagerBase extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(UsersAutoMailDaoManagerBase.class);
	
	public static void notifeReachedTotalDeposit(long userId) {
		Connection conn = null;
		try {
			conn = getConnection();
			UsersAutoMailDaoBase.notifeReachedTotalDeposit(conn, userId);
		} catch (SQLException ex) {
			logger.info("Fail to notifeReachedTotalDeposit: ", ex);
		} finally {
			closeConnection(conn);
		}
	}
}
