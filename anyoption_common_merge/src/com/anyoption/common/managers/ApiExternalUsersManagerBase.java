package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.ApiExternalUsersDAOBase;


public class ApiExternalUsersManagerBase extends BaseBLManager {

    private static final Logger logger = Logger.getLogger(ApiExternalUsersManagerBase.class);


	public static String getApiExternalUserReferenceById(long id) throws SQLException {
		Connection con = getConnection();
		try {
			return ApiExternalUsersDAOBase.getApiExternalUserReferenceById(con, id);
		} finally {
			closeConnection(con);
		}
	}

}
