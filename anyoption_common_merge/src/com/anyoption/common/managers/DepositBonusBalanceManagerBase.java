package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.base.DepositBonusBalanceBase;
import com.anyoption.common.daos.DepositBonusBalanceDAOBase;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;

public class DepositBonusBalanceManagerBase extends BaseBLManager {
   
	
	public static DepositBonusBalanceBase getDepositBonusBalance(DepositBonusBalanceMethodRequest request) throws SQLException {
		DepositBonusBalanceBase dbb = new DepositBonusBalanceBase();
        Connection conn = null;
        try {
            conn = getConnection();
            if(request.getBalanceCallType() == DepositBonusBalanceMethodRequest.CALL_OPEN_INVESTMENTS_AMOUNT_BALANCE 
            		|| request.getBalanceCallType() == DepositBonusBalanceMethodRequest.CALL_SETTLED_INVESTMENTS_AMOUNT_BALANCE){
            	dbb = DepositBonusBalanceDAOBase.getDepositBonusBalanceByInvestment(conn, request);
            } else if(request.getBalanceCallType() == DepositBonusBalanceMethodRequest.CALL_SETTLED_INVESTMENTS_RETURN_BALANCE){
            	dbb = DepositBonusBalanceDAOBase.getDepositBonusBalanceByReturn(conn, request);
            } else if(request.getBalanceCallType() == DepositBonusBalanceMethodRequest.CALL_TOTAL_AMOUNT_BALANCE){
            	dbb = DepositBonusBalanceDAOBase.getDepositBonusBalanceByTotalBalance(conn, request);
            }
        } finally {
            closeConnection(conn);
        }
        
        return dbb;
    }
}
