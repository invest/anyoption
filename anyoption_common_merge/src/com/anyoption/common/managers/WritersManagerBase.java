/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.Writer;
import com.anyoption.common.daos.WritersDAOBase;

/**
 * @author kirilim
 *
 */
public class WritersManagerBase extends BaseBLManager {
	
	/**
     * This method loads writer info specifically for the login logic
     * @param name
     * @param user
     * @throws SQLException
     */
    public static void loadWriterByName(String name, Writer writer) throws SQLException {
    	if (writer == null) {
    		throw new NullPointerException("The writer parameter cannot be null");
    	}
        Connection conn = getSecondConnection();
        try {
            WritersDAOBase.loadWriterByName(conn, name, writer);            
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void getSalesTypeDepartmentId(long writerId, Writer writer) throws SQLException{
		Connection con = getConnection();
		try {
			WritersDAOBase.getSalesTypeDepartmentId(con, writerId, writer);
		} finally {
			closeConnection(con);
		}
	}

	public static String getWriterPassword(long writerId) throws SQLException {
		Connection con = getConnection();
		try {
			return WritersDAOBase.getWriterPassword(con, writerId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static String getWriterName(long id) throws SQLException{
		Connection con = getConnection();
		try {
			return WritersDAOBase.getWriterName(con, id);
		} finally {
			closeConnection(con);
		}
	}
	
	public static Writer getAssignedWriter(long userId) throws SQLException{
		Connection con = getConnection();
		try {
			return WritersDAOBase.getAssignedWriter(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static boolean writerHasRole( long writerId, String role) throws SQLException {
		Connection con = getConnection();
		try {
			return WritersDAOBase.writerHasRole(con, writerId, role);
		} finally {
			closeConnection(con);
		}
	}
}