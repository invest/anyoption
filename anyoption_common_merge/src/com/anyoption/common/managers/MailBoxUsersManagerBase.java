package com.anyoption.common.managers;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.MailBoxUsersDAOBase;

public class MailBoxUsersManagerBase extends BaseBLManager {

    private static final Logger logger = Logger.getLogger(MailBoxUsersManagerBase.class);

	public static long insertAttachmentAsBLOB(File attachment) throws SQLException {
		Connection con = getConnection();
		try {
			return MailBoxUsersDAOBase.insertAttachmentAsBLOB(con, attachment);
		} catch (IOException e) {
			logger.error("Can't insert Attachment" , e);
			return -1;
		} finally {
			closeConnection(con);
		}
	}
	
	public static long insertAttachmentAsBLOBWithUserIdinName(File attachment, long userId) throws SQLException {
		Connection con = getConnection();
		long attachmentId = -1;
		try {
			attachmentId =MailBoxUsersDAOBase.insertAttachmentAsBLOB(con, attachment);
			MailBoxUsersDAOBase.updateMailBoxAttName(con, attachmentId, userId);
		} catch (IOException e) {
			logger.error("Can't insert Attachment" , e);
			attachmentId = -1;
		} finally {
			closeConnection(con);
		}		
		return attachmentId;
	}

}