package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.base.CurrenciesRules;
import com.anyoption.common.daos.CurrenciesRulesDAOBase;

/**
 * @author EyalG
 *
 */
public class CurrenciesRulesManagerBase extends BaseBLManager {	
	
	/**
	 * Get default currency id
	 * @param currenciesRules
	 * @return currencyId as long
	 * @throws SQLException
	 */
    public static long getDefaultCurrencyId(CurrenciesRules currenciesRules) throws SQLException {    	
    	Connection con = null;
        try {
        	con = getConnection();
        	return CurrenciesRulesDAOBase.getDefaultCurrencyId(con, currenciesRules);
        } finally {
            closeConnection(con);
        }
    }
      
}