package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.DeviceUniqueIdSkin;
import com.anyoption.common.daos.DeviceUniqueIdsSkinDAOBase;
import com.anyoption.common.managers.BaseBLManager;

public class DeviceUniqueIdsSkinManagerBase extends BaseBLManager {
	public static DeviceUniqueIdSkin getByDeviceId(String duid) throws SQLException {
		Connection conn = getConnection();
		try {
			return DeviceUniqueIdsSkinDAOBase.getByDeviceId(conn, duid);
		} finally {
			closeConnection(conn);
		}
	}
}