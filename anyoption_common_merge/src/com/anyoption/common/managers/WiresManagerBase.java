package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.daos.WiresDAOBase;

public class WiresManagerBase extends BaseBLManager {
    public static WireBase getById(long id) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return WiresDAOBase.getById(conn, id);
        } finally {
            closeConnection(conn);
        }
    }
}
