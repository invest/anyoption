package com.anyoption.common.managers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.daos.FilesDAOBase;
import com.anyoption.common.service.userdocuments.UserDocumentsDAO;
import com.anyoption.common.service.withdrawuserdetails.WithdrawUserDetailsDAO;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author kirilim
 */
public class FilesManagerBase extends BaseBLManager {

	private static final Logger log = Logger.getLogger(FilesManagerBase.class);
	private static int NUM_SYSTEM_UNDERSCORES = 5;
	private static final long UPLOAD_LIMIT = 700000;
	private static final long UPLOAD_LIMIT_BE = 30000000;
	
	public static void deleteFileByName(String fileName) {
		if (fileName == null || fileName.isEmpty()) {
			return;
		}
		java.io.File targetFile = new java.io.File(CommonUtil.getProperty(ConstantsBase.FILES_PATH) + fileName);
		boolean result = targetFile.delete();
		if (!result) {
			log.warn("Couldn't delete file!");
		}
	}
	
	
	public static boolean updateFile(File file) {
		Connection con = null;
		try {
			con = getConnection();
			FilesDAOBase.update(con, file);
			return true;
		} catch (SQLException e) {
			log.debug("Cannot update file", e);
			return false;
		} finally {
			closeConnection(con);
		}
	}
		
	public static boolean updateWhenUpload(File file) {
		Connection con = null;
		try {
			con = getConnection();
			FilesDAOBase.updateWhenUpload(con, file);
			return true;
		} catch (SQLException e) {
			log.debug("Cannot updateWhenUpload file", e);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	public static String getFileName(String fileName) {
		if (fileName == null) {
			return null;
		}
		String name = fileName.substring(getNthOccurrenceIndex(fileName, '_', NUM_SYSTEM_UNDERSCORES) + 1, fileName.length());
		if (name.length() > 15) {
			String fileType = name.substring(name.lastIndexOf('.'), name.length());
			name = name.substring(0, 13 - fileType.length()) + ".." + fileType;
		}
		return name;
	}

	/**
	 * 
	 * @param str the {@link String} to search in
	 * @param c the char to search for
	 * @param n the count of occurrences
	 * @return the N-th index if found, otherwise -1
	 */
	private static int getNthOccurrenceIndex(String str, char c, int n) {
	    int pos = str.indexOf(c, 0);
	    while (n-- > 0 && pos != -1) {
	        pos = str.indexOf(c, pos + 1);
	    }
	    return pos;
	}

	public static String saveFile(String fileName, BufferedInputStream bis, long userId, long fileType, boolean isFromBE, boolean isComplaint) throws IOException {
		if (!isComplaint) {
			fileName = modifyFileName(fileName);
			SimpleDateFormat sd = new SimpleDateFormat("ddMMyyyy_HHmmss");
			fileName = "user_" + userId + "_doc_" + fileType + "_" + sd.format(new Date()) + "_" + fileName;
		}
		fileName = getSubDirName() + fileName;
		
		java.io.File targetFile = new java.io.File(CommonUtil.getProperty(ConstantsBase.FILES_PATH) + fileName); // destination
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(targetFile), 4096);
		int bt;
		boolean fileTooLarge = false;
		long uploadLimit = UPLOAD_LIMIT;
		if(isFromBE){
			uploadLimit = UPLOAD_LIMIT_BE;
		}
		while ((bt = bis.read()) != -1) {
			bos.write(bt);
			if (targetFile.length() > uploadLimit) {
				fileTooLarge = true;
				break;
			}
		}
		if (fileTooLarge) {
			log.debug("File too large. Size of file: " + targetFile.length());
			targetFile.delete();
			fileName = null;
		} else {
			InputStream in = new FileInputStream(targetFile);
			in.read();
			in.close();
		}
		bos.close();
		bis.close();
		return fileName;
	}

	public static void changeFileStatuses(long userId, List<Long> fileTypesToChangeStatus, long status, Locale locale) {
		for (long type : fileTypesToChangeStatus) {
			File file = UsersManagerBase.getUserFileByType(userId, type, locale);
			if (file != null && status != file.getFileStatusId()) {
				file.setFileStatusId(status);
				updateFile(file);
			}
		}
	}
		
	private static final List<Long> jointFileTypes = Arrays.asList(FileType.USER_ID_COPY, FileType.PASSPORT, FileType.DRIVER_LICENSE);
	public static void changeFileStatusesNotNeeded(long userId, Long fileType, Locale locale) {		
		if (jointFileTypes.contains(fileType)) {
			List<Long> fileTypesToChangeStatus = new ArrayList<Long>();
			for (Long type : jointFileTypes) {
				if (type != fileType) {
					fileTypesToChangeStatus.add(type);
				}
			}
			log.debug("Try to set not needed the files:" + fileTypesToChangeStatus);
			changeFileStatuses(userId, fileTypesToChangeStatus, File.STATUS_NOT_NEEDED, locale);
		}
	}

	public static String modifyFileName(String name) {
		int lastChar = name.lastIndexOf('\\');
		if (lastChar != -1) {
			name = name.substring(lastChar + 1);
		}
		lastChar = name.lastIndexOf('/');
		if (lastChar != -1) {
			name = name.substring(lastChar + 1);
		}
		name = name.replaceAll("\\?", "");
		name = name.replaceAll(":", "");
		name = name.replaceAll("\\*", "");
		name = name.replaceAll("<", "");
		name = name.replaceAll(">", "");
		name = name.replaceAll("\"", "");
		name = name.replaceAll("|", "");
		name = name.replaceAll("\\+", "");
		name = name.replaceAll("\\s", "");
		name = name.replaceAll("[^a-zA-Z0-9.-]", "");
		return name;
	}

	private static String getSubDirName() {
		SimpleDateFormat sdSubDir = new SimpleDateFormat("yyMM");
		String subDir = sdSubDir.format(new Date());
		java.io.File subDirFile = new java.io.File(CommonUtil.getProperty(ConstantsBase.FILES_PATH) + subDir);
		Boolean result = null;
		if (!subDirFile.exists()) {
			result = subDirFile.mkdir();
		}
		if (result != null && !result) {
			log.warn("Cannot create subdir");
			subDir = "";
		}
		return subDir + "/";
	}

	public static void setExpDateParams(File file) {
		Calendar c = Calendar.getInstance();
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(file.getExpDate());
		c.setTime(file.getExpDate());
		file.setExpDay(String.valueOf(c.get(Calendar.DAY_OF_MONTH)));
		file.setExpMonth(String.valueOf(c.get(Calendar.MONTH) + 1));
		file.setExpYear(String.valueOf(c.get(Calendar.YEAR)));
		// add "0" String for drop down
		if (file.getExpDay().length() == 1) {
			file.setExpDay("0" + file.getExpDay());
		}
		if (file.getExpMonth().length() == 1) {
			file.setExpMonth("0" + file.getExpMonth());
		}
	}

	public static void updateExpDate(File f) {
		if (!CommonUtil.isParameterEmptyOrNull(f.getExpYear()) && !CommonUtil.isParameterEmptyOrNull(f.getExpMonth())
				&& !CommonUtil.isParameterEmptyOrNull(f.getExpDay())) {
			Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 12);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.YEAR, Integer.valueOf(f.getExpYear()));
			c.set(Calendar.MONTH, Integer.valueOf(f.getExpMonth()) - 1);
			c.set(Calendar.DAY_OF_MONTH, Integer.valueOf(f.getExpDay()));
			f.setExpDate(c.getTime());
		}
	}
		
	public static int initStatus(String fileName, boolean regulated, boolean approved, boolean controlApproved) {
		if (fileName != null) {
			if (regulated) {
				if (controlApproved) {
					return  File.STATUS_DONE;
				} else {					
					return File.STATUS_IN_PROGRESS;
				}
			} else {
				if (approved) {
					return  File.STATUS_DONE;
				} else {
					return File.STATUS_IN_PROGRESS;
				}
			}
		} else {
			return File.STATUS_REQUESTED;
		}
	}
	
	public static boolean initLock(boolean approved,boolean controlReject) {
		if(approved && controlReject) {
			return false;
		}else {
		    return approved;
		}
	}
	
	/**
	 * This method returns the key of the reject reason (if any) and the key of the file name for a file 
	 * @param fileId
	 * @return HashMap<String, String> with file name and file reject reason
	 */
	public static HashMap<String, String> getReasonAndTypeByFileId(long fileId) {
		Connection con = null;
		HashMap<String, String> map = new HashMap<String, String>();
		try {
			con = getConnection();
			map = FilesDAOBase.getReasonAndTypeByFileId(con, fileId);
		} catch (SQLException e) {
			log.debug("Cannot get reject reason by fileId", e);
		} finally {
			closeConnection(con);
		}			
		return map;
	}
	
	public static void setFileDefaultValues(File file, String name) {
		log.debug("We have Uploading file, the file status is inprogress: 2 and set Deafult values fileId:" + file.getId());
		file.setFileName(name);
		file.setFileStatusId(File.STATUS_IN_PROGRESS);
		file.setApproved(false);
		file.setControlApproved(false);
		file.setControlReject(false);
		file.setSupportReject(false);				
		file.setRejectReason(File.REJECT_REASON_NOT_REJECTED);
		file.setComment(null);
		file.setRejectionWriterId(Writer.WRITER_ID_AUTO);
		file.setRejectionDate(null);
		file.setSupportApprovedWriterId(Writer.WRITER_ID_AUTO);
		file.setTimeSupportApproved(null);
		file.setControlApprovedWriterId(Writer.WRITER_ID_AUTO);
		file.setTimeControlApproved(null);
		file.setControlRejectWriterId(Writer.WRITER_ID_AUTO);
		file.setTimeControlRejected(null);
		file.setControlRejectReason(File.REJECT_REASON_NOT_REJECTED);
		file.setControlRejectComment(null);
	}
	
	public static void setFileDefaultValuesBE(File file, String name) {
		log.debug("We have Uploading file, the file status is inprogress: 2 and set Deafult values fileId:" + file.getId());
		file.setFileName(name);
		file.setFileStatusId(File.STATUS_IN_PROGRESS);
	}
	
	public static void clearKSFileData(long fileId) {		
		Connection con = null;
		try {
			con = getConnection();
			FilesDAOBase.clearKSFileData(con, fileId);
			log.debug("Cleared keesing data for fileId:" + fileId);
		} catch (SQLException e) {
			log.debug("Cannot gclearKSFileData", e);
		} finally {
			closeConnection(con);
		}			
	}
	
	public static void insertUploadFile(long userId, long typeId, long writerId, String fileName) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			long fileId = FilesDAOBase.insertUploadFile(con, userId, typeId, writerId, fileName, File.STATUS_DONE);			
			log.debug("insertUploadFile for FileId:" + fileId + " was created and upload with params "
					+ "userId[" + userId + "] " 
					+ "typeId[" + typeId + "] "
					+ "writerId[" + writerId + "] "
					+ "fileName[" + fileName + "] ");
			if(typeId == FileType.BANK_WIRE_PROTOCOL_FILE){
				WithdrawUserDetailsDAO.updateWithdrawFileIdUserDetils(con, userId, fileId, writerId);
			}
			UserDocumentsDAO.setCurrentFlagDocs(con, userId, typeId, null);
		} finally {
			closeConnection(con);
		}
	}
}