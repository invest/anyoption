/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.User;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.LoginDAO;
import com.anyoption.common.daos.WritersDAOBase;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author kirilim
 *
 */
public class LoginManager extends BaseBLManager {

	private static final Logger log = Logger.getLogger(LoginManager.class);	

	/**
	 * Login
	 * Logic taken from the login module (AO) operation
	 * @param user
	 * @return
	 */
	public static ArrayList<String> loginActionFromWeb(User user, String password) throws SQLException {
		ArrayList<String> res = new ArrayList<String>();
		Connection con = null;
		long userId = user.getId();
		String emailSubject = "Login failure ";
		String emailTo;
		
		if (user.isActive()) {
			if (!user.getPassword().equals(password)) {  // wrong password
				if (user.getFailedCount() == ConstantsBase.WEB_FAILED_LOGIN_COUNT - 1 /*4*/) { // this is the third (fourth) try
					//	handle user locking
					try {
						con = getConnection();
						con.setAutoCommit(false);
						// insert a fail login close issue
						IssuesManagerBase.insertLoginFailureLockIssue(con, user.getUserName(), userId);
						String assignedWriterEmail = WritersManagerBase.getAssignedWriter(userId).getEmail();
				        if (!CommonUtil.isParameterEmptyOrNull(assignedWriterEmail)) {
				        	handleLoginFailureLock(con, user, emailSubject, assignedWriterEmail);
				        	emailSubject = emailSubject.concat("- User assigned to writer - " + WritersManagerBase.getAssignedWriter(userId).getUserName());
				        	handleLoginFailureLock(con, user, emailSubject, CommonUtil.getConfig("support.email"));
				        } else {
				        	emailTo = CommonUtil.getConfig("support.email").concat(";").concat(CommonUtil.getConfig("account.lock.email.group"));
				        	handleLoginFailureLock(con, user, emailSubject, emailTo);
				        }
						LoginDAO.updateLoginFailed(con, user.getUserName(), true);
						con.commit();
						if (log.isInfoEnabled()) {
							log.log(Level.INFO, "User '" + user.getUserName() + "' failed to login and changed to not active");
						}
					} catch (SQLException e) {
						con.rollback();
						log.debug("SQLException in update login failure", e);
						throw e;
					} finally {
						if (con != null) {
							con.setAutoCommit(true);
						}
						closeConnection(con);
					}
					if (user.isDocsRequired() && user.getSkinId() != Skin.SKIN_ETRADER) {
						res.add("header.login.documents.needed.lockerror");
					} else {
						res.add("header.login.lockerror");
					}					
					return res;
				} else {
					try {
						con = getConnection();
						LoginDAO.updateLoginFailed(con, user.getUserName(), false);
						if (log.isInfoEnabled()) {
							log.log(Level.INFO, "User '" + user.getUserName() + "' failed to login, 'failed count' updated");
						}
					} catch (SQLException e) {
						log.debug("SQLException in update login failure", e);
						throw e;
					} finally {
						closeConnection(con);
					}
					if (user.getFailedCount() == ConstantsBase.WEB_FAILED_LOGIN_COUNT - 2) {
						res.add("header.login.lasterror");
					} else {					
						res.add("error.personal.wrongPass");
					}
					return res;
				}
			} else {  // update succeed login
				try {
					con = getConnection();
					LoginDAO.updateLoginSucceed(con, user.getUserName());
					if (log.isInfoEnabled()) {
						log.log(Level.INFO, "User '" + user.getUserName() + "' succeeded login");
					}
				} catch(SQLException e) {
					log.debug("SQLException in update login succeed", e);
					throw e;
				} finally {
					closeConnection(con);
				}
			}
		} else {  // not active
			try {
				con = getConnection();
				LoginDAO.updateLoginFailed(con, user.getUserName(), false);
				if (log.isInfoEnabled()) {
					log.log(Level.INFO, "User '" + user.getUserName() + "' is not active, 'failed count' updated");
				}
			} finally {
				closeConnection(con);
			}
			if (user.isDocsRequired() && user.getSkinId() != Skin.SKIN_ETRADER) {
				res.add("header.login.documents.needed.lockerror");
			} else {
				res.add("header.login.lockerror");
			}
			return res;
		}
		return res;
	}
	
	public static ArrayList<String> loginActionFromBackend(Writer writer, String password) throws SQLException {
		ArrayList<String> res = new ArrayList<String>();
		Connection con = null;
		
		if (writer.isActive()) {
			if (!writer.getPassword().equals(password)) { // wrong password
				try {
					con = getConnection();
					LoginDAO.updateBackendLoginFailed(con, writer.getUserName());
					if (log.isInfoEnabled()) {
						log.log(Level.INFO, "Writer '" + writer.getUserName() + "' failed to login, 'failed count' updated");
					}
				} finally {
					closeConnection(con);
				}
				res.add("error.personal.wrongPass");
				return res;
			} else { // correct password, checking fail count
				if (writer.getFailedCount() > ConstantsBase.BACKEND_FAILED_LOGIN_COUNT - 1) { // checking last fail time
					Date lastFailed = writer.getLastFailedTime();
					int offset = TimeZone.getDefault().getOffset(lastFailed.getTime());					
					long lastFailedInMillis = lastFailed.getTime() + offset;
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MINUTE, -30);
					long time = cal.getTimeInMillis();
					log.info("Comparing last failed time " + lastFailed + " with offset " + offset + " with " + cal.getTime());
					if (time > lastFailedInMillis) { // locked time is up, succeed login, set failed count to 0
						try {
							con = getConnection();
							LoginDAO.updateBackendLoginSucceed(con, writer.getUserName());
							if (log.isInfoEnabled()) {
								log.log(Level.INFO, "Writer '" + writer.getUserName() + "' succeeded login");
							}
						} finally {
							closeConnection(con);
						}
					} else { // writer is locked for 30 minutes						
						res.add("header.login.lockerror");
						return res;
					}
				} else { // update succeed login
					try {
						con = getConnection();
						LoginDAO.updateBackendLoginSucceed(con, writer.getUserName());
						if (log.isInfoEnabled()) {
							log.log(Level.INFO, "Writer '" + writer.getUserName() + "' succeeded login");
						}
					} finally {
						closeConnection(con);
					}
				}
			}
		} else { // not active
			try {
				con = getConnection();
				LoginDAO.updateBackendLoginFailed(con, writer.getUserName());
				if (log.isInfoEnabled()) {
					log.log(Level.INFO, "Writer '" + writer.getUserName() + "' is not active, 'failed count' updated");
				}
			} finally {
				closeConnection(con);
			}
			res.add("header.login.lockerror");
			return res;
		}
		
		return res;
	}
	
	public static List<String> getWriterRoles(String writer) throws SQLException {
		Connection con = null;
		try {
			con = getSecondConnection();
			return WritersDAOBase.getWriterRoles(con, writer);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Handle login failed lock
	 * @param conn
	 * @param user
	 * @throws SQLException 
	 */
	private static void handleLoginFailureLock(Connection conn, User user, String emailSubject, String emailTo) throws SQLException {
		long userId = user.getId();
		if (userId > 0) {
			// send email
	        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
			serverProperties.put("url", CommonUtil.getConfig("email.server"));
			serverProperties.put("auth", "true");
			serverProperties.put("user", CommonUtil.getConfig("email.uname"));
			serverProperties.put("pass", CommonUtil.getConfig("email.pass"));
			serverProperties.put("contenttype", "text/html; charset=UTF-8");

	        Hashtable<String, String> email = new Hashtable<String, String>();
	        email.put("subject", emailSubject);
	        email.put("to", emailTo);
	        email.put("from", CommonUtil.getConfig("email.from"));
	        email.put("body", "User ID: " + user.getId() + "<BR/>" +
	        				  "User Name: " + user.getUserName().toUpperCase() + "<BR/>" +
	        				  // TODO The skin name will be available when the skin bean is merged
//	        				  "Skin: " + SkinsManagerBase.getSkin(user.getSkinId()).getName());
							  "Skin ID: " + user.getSkinId());

			CommonUtil.sendEmail(serverProperties, email, null);
		} else {
			log.error("couldn't find user, username: " + user.getUserName());
		}
	}
}