/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Login;
import com.anyoption.common.daos.LoginsDAO;

/**
 * @author kirilim
 *
 */
public class LoginsManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(LoginsManager.class);
	
	
	/**
	 * 
	 * @param userName
	 * @param login
	 * @return the login id of the user, or -1 if there is no user or problem occurs
	 */
	public static long insertLogin(String userName, Login login) {
		Connection con = null;
		try {
			con = getConnection();
			return LoginsDAO.insert(con, userName, login);
		} catch (SQLException e) {
			logger.debug("Unable to insert login", e);
			return -1;
		} finally {
			closeConnection(con);
		}
	}
}