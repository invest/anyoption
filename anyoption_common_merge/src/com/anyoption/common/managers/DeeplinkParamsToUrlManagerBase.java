/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;

import com.anyoption.common.beans.base.DeeplinkParamsToUrl;
import com.anyoption.common.daos.DeeplinkParamsToUrlDAO;

/**
 * The base manager for the DeeplinkParamsToUrl
 * 
 * @author EyalG
 */
public class DeeplinkParamsToUrlManagerBase extends BaseBLManager {

	private static HashMap<String, DeeplinkParamsToUrl> deeplinkParamsToUrl;
	private static Calendar deeplinkParamsToUrlLastLoad;
		
	public static HashMap<String, DeeplinkParamsToUrl> getAllCache() throws SQLException {
		Calendar now = Calendar.getInstance();
		if (deeplinkParamsToUrl == null || (deeplinkParamsToUrlLastLoad != null && now.after(deeplinkParamsToUrlLastLoad))) {
			deeplinkParamsToUrl = getAll();
			deeplinkParamsToUrlLastLoad = Calendar.getInstance();
			deeplinkParamsToUrlLastLoad.add(Calendar.DATE, 1);//add 1 day
			deeplinkParamsToUrlLastLoad.set(Calendar.HOUR_OF_DAY, 0);
			deeplinkParamsToUrlLastLoad.set(Calendar.MINUTE, 0);
			deeplinkParamsToUrlLastLoad.set(Calendar.SECOND, 0);
			deeplinkParamsToUrlLastLoad.set(Calendar.MILLISECOND, 0);
		}
		return deeplinkParamsToUrl;
	}
	
	public static HashMap<String, DeeplinkParamsToUrl> getAll() throws SQLException {
		Connection con = getConnection();
		HashMap<String, DeeplinkParamsToUrl> deeplinkParamsToUrlMap = new HashMap<String, DeeplinkParamsToUrl>();
		try {
			deeplinkParamsToUrlMap = DeeplinkParamsToUrlDAO.getAll(con);
		} finally {
			closeConnection(con);
		}
		return deeplinkParamsToUrlMap;
	}
}
