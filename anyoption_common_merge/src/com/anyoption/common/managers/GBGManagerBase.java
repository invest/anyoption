/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.GBGDAOBase;


public class GBGManagerBase extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(GBGManagerBase.class);
	
	private static Map<Long, String> gbgCountries;
	
	public static Map<Long, String> getGbgCountries() {
		if(gbgCountries == null){
			Connection conn = null;
			try {
				conn = getConnection();
				gbgCountries = GBGDAOBase.loadGBGCountries(conn);
			} catch (SQLException ex) {
				logger.info("Fail to getGbgCountries: ", ex);				
			} finally {
				closeConnection(conn);
			}
			
		}
		return gbgCountries;
	}	
}
