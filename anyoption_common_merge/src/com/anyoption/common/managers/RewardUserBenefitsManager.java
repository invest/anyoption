package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.RewardUserBenefitsDAO;

/**
 * @author liors
 *
 */
public class RewardUserBenefitsManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(RewardUserBenefitsManager.class);
	
	public static long getUserMaxAcademyBenefit(long userId) throws SQLException {
		Connection connection = getConnection();
        try {
        	return RewardUserBenefitsDAO.getUserMaxAcademyBenefit(connection, userId);
        } finally {
            closeConnection(connection);
        }
	}
	
	/**
	 * @param userId
	 * @param tierId
	 * @throws SQLException
	 */
	public static void updateRemoveRewards(long userId, int tierId) throws SQLException {
		Connection connection = getConnection();
        try {
        	RewardUserBenefitsDAO.updateRemoveRewards(connection, userId, tierId);
        } finally {
            closeConnection(connection);
        }
	}
}