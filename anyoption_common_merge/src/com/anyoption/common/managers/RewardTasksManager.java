package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.RewardTasksDAO;
import com.anyoption.common.rewards.TaskUser;

/**
 * @author liors
 *
 */
public class RewardTasksManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(RewardTasksManager.class);

	/**
	 * @param taskUser
	 * @return
	 * @throws SQLException
	 */
	public static TaskUser getUserTasks(TaskUser taskUser) throws SQLException {
		Connection connection = getConnection();
        try {            
        	return RewardTasksDAO.getUserTasks(connection, taskUser, 0);
        } finally {
        	closeConnection(connection);
        }
	}	
}
