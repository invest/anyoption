package com.anyoption.common.managers;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.BonusUsersStep;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.bl_vos.BonusPopulationLimit;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlerFactory;
import com.anyoption.common.bonus.BonusHandlersException;
import com.anyoption.common.daos.BonusDAOBase;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.daos.UsersDAOBase;
import com.anyoption.common.service.results.WithdrawBonusRegulationStateMethodResult;
import com.anyoption.common.service.results.WithdrawBonusRegulationStateMethodResult.WithdrawBonusStateEnum;
import com.anyoption.common.util.BonusFormulaDetails;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

public class BonusManagerBase extends BaseBLManager {
    private static final Logger logger = Logger.getLogger(BonusManagerBase.class);	
	
    public static final int BONUS_P_L_FIXED = 1;
    public static final int BONUS_P_L_DEPOSIT_ATTEMPT = 2;
    public static final int BONUS_P_L_AVG_DEPOSITS = 3;
    public static final int BONUS_P_L_BALANCE= 4;
    
    public static final long BONUS_ROUND_TOP = 50000;

	public static final int BONUS_LIMIT_UPDATE_TYPE_SUM_DEPOSITS = 1;
	public static final int BONUS_LIMIT_UPDATE_TYPE_SUM_INVESTS = 2;
	public static final int BONUS_LIMIT_UPDATE_TYPE_ACTION_NUM = 3;
	public static final int BONUS_LIMIT_UPDATE_TYPE_BONUS_STEPS = 4;

	public static final String ERROR_DUMMY = "ERROR-DUMMY";

	
	 /**
     * getMinDepositAmount by bonus popualtion limitations
     * @param BonusUsers bu
     * @param Bonus b
     * @param BonusCurrency bc
     * @param popTypeId
     * @param bonusPopLimitTypeId
     * @param userId
     * @param currencyId
     * @param isHasSteps
     * @return minDepositAmount
     */
    public static boolean getLimitsForBonus(BonusUsers bu, Bonus b, BonusCurrency bc, long popEntryId, int bonusPopLimitTypeId, long userId, long currencyId, boolean isHasSteps) throws SQLException{
    	Connection con = getConnection();

    	return getLimitsForBonus(con, bu, b, bc, popEntryId, bonusPopLimitTypeId, userId, currencyId, isHasSteps);
    }

    /**
     * getMinDepositAmount by bonus popualtion limitations
     * @param Connection con
     * @param BonusUsers bu
     * @param Bonus b
     * @param BonusCurrency bc
     * @param popTypeId
     * @param bonusPopLimitTypeId
     * @param userId
     * @param currencyId
     * @param isHasSteps
     * @return minDepositAmount
     */
    public static boolean getLimitsForBonus(Connection con, BonusUsers bu, Bonus b, BonusCurrency bc, long popEntryId, int bonusPopLimitTypeId, long userId, long currencyId, boolean isHasSteps) throws SQLException{
    		if (popEntryId != 0 && bonusPopLimitTypeId != 0) {
        		long amountForLimit = getAmountForLimitCreation(con, userId, bonusPopLimitTypeId);

    			if (amountForLimit != -1) {
    				return getMinDepositByPopLimitations(bu, b, bc, popEntryId, amountForLimit, currencyId, isHasSteps, con);
    			} else {
    				logger.info("Problem with bonus population limits!");
    			}
        	}
		return false;
    }
    
    /**
     * Get amount for bonus limitation by population type id
     * @param con Connection
     * @param user
     * @param popTypeId population type id
     * @return
     * @throws SQLException
     */
    public static long getAmountForLimitCreation(Connection con, long userId, int bonusPopLimitTypeId) throws SQLException {
    	long amountForLimit = -1;

    	Transaction t = null;

    	switch (bonusPopLimitTypeId) {
    	case BonusManagerBase.BONUS_P_L_FIXED:
    		amountForLimit = 0;
    		break;
   		case BonusManagerBase.BONUS_P_L_DEPOSIT_ATTEMPT:
   			t = TransactionsDAOBase.getLastFailedDeposit(con, userId);
   			if (null != t) {
   				logger.debug("Got lastFailedDeposit. trxId: " + t.getId() + ", amount: " + t.getAmount());
   				amountForLimit = t.getAmount();
   			} else {
   				logger.warn("Problem getting lastFailedDeposit!");
   				amountForLimit = -1;
   			}
   			break;
   		case BonusManagerBase.BONUS_P_L_AVG_DEPOSITS:
   			long avgDeposits = TransactionsDAOBase.getAVGSucceedDeposit(con, userId);
   			if (avgDeposits == 0) {
   				logger.warn("Problem getting firstSucceedDeposit!");
   				amountForLimit = -1;
   			} else {
   				amountForLimit = avgDeposits;
   			}
   			break;
   		case BonusManagerBase.BONUS_P_L_BALANCE:
   			long balance = UsersDAOBase.getUserBalance(con, userId);
   			if (balance > 0) {
   				amountForLimit = balance;
   			} else {
   				amountForLimit = -1;
   				logger.warn("Problem getting users balance!");
   			}
   			break;
   		default:
   			logger.warn("no case for bonusPopLimitTypeId: " + bonusPopLimitTypeId);
			amountForLimit = -1;
			break;
    	}


    	return amountForLimit;
    }

    /**
     * Get min deposit amount defined by population limits
     * @param amountForLimit amount for limit calculation
     * @param currencyId user currecy
     * @param isHasSteps
     * @param con TODO
     * @param BonusUsers bu
     * @param Bonus b
     * @param BonusCurrency bc
     * @param popTypeId  population type id of the user
     * @return
     * @throws SQLException
     */
    public static boolean getMinDepositByPopLimitations(BonusUsers bu, Bonus b, BonusCurrency bc, long popEntryId, long amountForLimit, long currencyId, boolean isHasSteps, Connection con) throws SQLException {

    	long bonusId = bu.getBonusId();
    	long limitValue = 0;
    	ArrayList<BonusPopulationLimit> bpl = BonusDAOBase.getBonusPopulationLimits(con, popEntryId, bonusId, currencyId);
 
		for (int i = 0; i < bpl.size(); i++) {
			BonusPopulationLimit l = bpl.get(i);

			if (amountForLimit >= l.getMinDepositParam() && amountForLimit <= l.getMaxDepositParam()) {

				long baseCalculation = l.getMultiplication() * amountForLimit;
				long baseCalculationRounded = roundAmountToNearestX(baseCalculation, BONUS_ROUND_TOP);

		   		if (!isHasSteps){
   					limitValue = Math.max(baseCalculationRounded, l.getMinResult());

   					if (limitValue > 0){
	   					switch (l.getLimitUpdateType()) {
							case BONUS_LIMIT_UPDATE_TYPE_SUM_DEPOSITS:
								// If sum deposits hasn'e been changed by admin, set new sum deposits
								if (bu.getSumDeposits() == 0){
									bu.setSumDeposits(limitValue);
								}
								break;

							case BONUS_LIMIT_UPDATE_TYPE_SUM_INVESTS:
								bc.setSumInvestQualify(limitValue);
								break;

							case BONUS_LIMIT_UPDATE_TYPE_ACTION_NUM:
								b.setNumberOfActions(limitValue);
								break;

							default:
								logger.error("LimitUpdateType " + l.getLimitUpdateType() + " not recognized");
								return false;
						}
	   					// Check if needs to change bonus amount
	   					long bonusAmount = l.getBonusAmount();

	   					if (bonusAmount > 0){
	   						bc.setBonusAmount(bonusAmount);
	   					}

	   					// Check if needs to change min and/or investment amount
	   					long minInvAmount = l.getMinInvestmentAmount();
	   					long maxInvAmount = l.getMaxInvestmentAmount();

	   					if (minInvAmount > 0){
	   						bc.setMinInvestAmount(minInvAmount);
	   					}
	   					if (maxInvAmount > 0){
	   						bc.setMaxInvestAmount(maxInvAmount);
	   					}

	   					return true;
   					}
   					return false;

	   			}else{
	   	   			ArrayList<BonusUsersStep> bonusSteps = BonusDAOBase.getBonusSteps(con, bonusId, currencyId);
	   	   			long stepMin = 0;
    				long stepMax = 0;
    				String bonusPercentStr= null;
    				DecimalFormat df = new DecimalFormat("###.###");

	    			for (int j = 0; j < bonusSteps.size(); j++){

	    				// if first step, take the min between base calculation and min result
	    				if (stepMin == 0){
	    					stepMin = Math.max(baseCalculationRounded, l.getMinResult());
	    				}else{
	    					stepMin = stepMax + 100;
	    				}
	    				stepMax = roundAmountToNearestX(baseCalculation * l.getStepRangeMul() * (long)Math.pow(l.getStepLevelMul(), j),
	    								BONUS_ROUND_TOP);

	    				bonusSteps.get(j).setMinDepositAmount(stepMin);
	    				bonusSteps.get(j).setMaxDepositAmount(stepMax);

	    				bonusPercentStr = df.format(l.getMinBonusPercent() + l.getStepBonusAddition() * j);
	    				bonusSteps.get(j).setBonusPercent(Double.valueOf(bonusPercentStr));
	    			}
	    			bu.setBonusSteps(bonusSteps);
	    			return true;
	   			}
			}
		}
    	return false;
    }

    private static long roundAmountToNearestX(long amount, long xValue){
    	if (amount > xValue / 2){
            BigDecimal bd = new BigDecimal(String.valueOf(amount));
            bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP);
            bd = bd.divide(new BigDecimal(String.valueOf(xValue)), BigDecimal.ROUND_HALF_UP);
	    	return bd.longValue() * xValue;
    	}else{
    		return amount;
    	}
    }
    
	/**
	 * Update bonus users state
	 * @param id
	 * @param stateId
	 * @throws SQLException
	 */
    public static void updateBonusUser(long id, long stateId, long writerIdCancel) throws SQLException {
        Connection con = getConnection();
        try {
        	BonusDAOBase.updateState(con, id, stateId, writerIdCancel);
        } finally {
            closeConnection(con);
        }
    }
    
    /**
     * @param conn
     * @param id
     * @param stateId
     * @param writerIdCancel
     * @throws SQLException
     */
    public static void updateBonusUser(Connection conn, long id, long stateId, long writerIdCancel) throws SQLException {
        BonusDAOBase.updateState(conn, id, stateId, writerIdCancel); //Quick fix for deploy.
    }
    
    /**
     * get bonus users steps
     * @param bonusUserId
     * @return list of bonusUsersSteps
     * @throws SQLException
     */
    public static ArrayList<BonusUsersStep> getBonusUsersSteps(Connection con, long bonusUserId) throws SQLException {
    	return BonusDAOBase.getBonusUsersSteps(con, bonusUserId);
	}

    public static ArrayList<BonusUsersStep> getBonusUsersSteps(long bonusUserId) throws SQLException {
		Connection con = getConnection();
		try {
			return getBonusUsersSteps(con, bonusUserId);
		} finally {
		    closeConnection(con);
		}
	}
    
	/**
	 * Accept bonus user
	 * @param bu
	 * @param user
	 * @throws SQLException
	 */
	public static void acceptBonusUser(BonusUsers bu, User user, String ip, long loginId) throws SQLException {
		Connection con = getConnection();
		BonusHandlerBase bh = BonusHandlerFactory.getInstance(bu.getTypeId());
		if (null != bh) {
			try {
				con.setAutoCommit(false);
				bh.acceptBonus(con, bu, user, ip, loginId);
				con.commit();
			} catch (SQLException e){
				logger.error("Accept bonus user, userId: " + user.getId() + " BonusUserId: " + bu.getId(), e);
	    		try {
	    			con.rollback();
	    		} catch (SQLException ie) {
	    			logger.error("In accept bonus user, Can't rollback.", ie);
	    		}
	    		throw e;
			} finally {
				con.setAutoCommit(true);
				closeConnection(con);
			}
		}
	}

	/**
	 * Get bonusUsers by filters
	 * 
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<BonusUsers> getBounsUsers(	long userId, Date from, Date to, String userName, String skins, long classId,
														long bonusTypeId, long bonusStateId, long userActionId, long writerGroupId,
														long populationTypeId, long writerId, String utcOffset) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return BonusDAOBase.getBonusUsers(	con, userId, from, to, userName, skins, classId, bonusTypeId, bonusStateId, userActionId,
											writerGroupId, populationTypeId, writerId, utcOffset);
		} finally {
			closeConnection(con);
		}
	}

    /**
     * This method create a connection and call cancelBonus method to cancel bonus 
     * Using atomic action with autoCommit false.
     * @param bonusUser
     * @param utcOffset
     * @param writerId
     * @param skinId
     * @param user
     * @return String message with the return value from cancelBonus method. 
     * @throws SQLException
     */
	// TODO decide how to extract if error with canceling occurs in clients
    public static String cancelBonus(BonusUsers bonusUser, String utcOffset, long writerId, long skinId, User user, long loginId, String ip) throws SQLException {
    	String msg = "";
    	Connection conn = getConnection();
    	conn.setAutoCommit(false);
    	
    	try {
    		String[] msgParts = cancelBonus(conn, bonusUser, utcOffset, writerId, skinId, user, loginId, ip).split("_");
			if (writerId == Writer.WRITER_ID_WEB|| writerId == Writer.WRITER_ID_MOBILE || writerId == Writer.WRITER_ID_COPYOP_WEB
					|| writerId == Writer.WRITER_ID_COPYOP_MOBILE) {
				msg = msgParts[msgParts.length - 1]; // the last part, else old logic
			} else if (!CommonUtil.isParameterEmptyOrNull(msgParts[1])) {
    			msg = msgParts[1];
    		}
    		conn.commit();

    	} catch (SQLException e) {
    		logger.log(Level.ERROR, "in cancelBonus", e);
    		try {
    			conn.rollback();
    		} catch (SQLException ie) {
    			logger.log(Level.ERROR, "in cancelBonus, Can't rollback.", ie);
    		}
    		throw e;
		} finally {
			conn.setAutoCommit(true);
			closeConnection(conn);
		}
   	 	return msg;
    }

    /**
     * This method handle cancel bonus logic and functionality by  
     * given connection that must be auto commit false to create an atomic action. 
     *  
     * @param conn 
     * @param bonusUser 
     * @param utcOffset 
     * @param writerId 
     * @param skinId 
     * @param user 
     * @return message String that present an indication of the method result. 
     * Message structure: [indicator]_[key] 
     * 		[indicator]: 
     * 				A	: cancel success. 
     * 				F	: cancel did not occur. 
     * 		[key]:	key which present verbal explanation to the [indicator]  
     *  
     * @throws SQLException 
     */ 
    public static String cancelBonus(Connection conn, BonusUsers bonusUser, String utcOffset, long writerId, long skinId, User user, long loginId, String ip) throws SQLException {
    	String msg = "";
    	try {
    		/*boolean needToWaive = (bonusUser.getBonusStateId() == ConstantsBase.BONUS_STATE_USED) &&
    				(InvestmentsManagerBase.isCompanyProfitHigher(conn, bonusUser));*/
    		boolean needToWaive = false; //Quick fix - Stop bonuses from reaching state Waive.
    		boolean userHasOpenInvestments = InvestmentsManagerBase.hasInvestments(conn, bonusUser.getUserId(), false);
    		long bonusState = getStateInPlay(conn, bonusUser);
    		
    		if(bonusUser.getBonusStateId() != ConstantsBase.BONUS_STATE_USED || !userHasOpenInvestments) {
//   			if(!(bonusUser.getBonusStateId() == ConstantsBase.BONUS_STATE_USED && userHasOpenInvestments)) {
    			if (!needToWaive) {
    				//TODO there is a problem in insertBonusWithdraw, this method commit the given connection. if there is exception after this line, no roll back can occur. => fix insertBonusWithdraw method.
    				BonusManagerBase.cancelBonusToUser(conn, bonusUser, bonusState, utcOffset, writerId, skinId, loginId, ip);
    				long userBalance = UsersDAOBase.getUserBalance(conn, bonusUser.getUserId());
    				//Assumption: user don't have negative balance before cancel bonus. 
    				//Meaning, user had negative balance before cancel bonus => fix his balance.
    				//Implication: if user had negative balance before cancel bonus there will be a problem to track it, because this code fix the user negative balance.
    				msg = "A" + "_BMS.info.cancel.success";
    				if (userBalance < 0) {
    					logger.log(Level.INFO, "user negative balance! " + userBalance + ", about to fix negative balance");
    					TransactionsManagerBase.insertDepositToFixNegativeBalance(conn, -userBalance, writerId, user, bonusUser.getId(), loginId, ip);
    					msg = "A" + "_BMS.info.transactions.fix.negative.balance";
    				}
    			} else {//need to waive	
        			//BonusManagerBase.updateBonusUser(conn, bonusUser.getId(), ConstantsBase.BONUS_STATE_WAGERING_WAIVED, bonusUser.getWriterIdCancel());
        			msg = "F" + "_BMS.info.bonus.wagering.waived";
        		}
    		} else {
				msg = "F" + "_BMS.info.user.has.open.investment_" + ERROR_DUMMY;
			}		
    	} catch (SQLException e) {
    		logger.error("BMS Error!!! in cancelBonus", e);
    		throw e;
    	}
    	return msg;
    }

	private static long getStateInPlay(Connection conn, BonusUsers bonusUser) {
		long state = ConstantsBase.BONUS_STATE_WITHDRAWN;
		try {
			if (bonusUser != null) {
				if (CommonUtil.getProperty(ConstantsBase.APPLICATION_SOURCE).equalsIgnoreCase(ConstantsBase.APPLICATION_SOURCE_BACKEND) && 
						(bonusUser.getBonusStateId() == ConstantsBase.BONUS_STATE_GRANTED || bonusUser.getBonusStateId() == ConstantsBase.BONUS_STATE_PENDING || 
						(bonusUser.getBonusStateId() == ConstantsBase.BONUS_STATE_ACTIVE && BonusDAOBase.isCanCancelOnActive(conn, bonusUser.getId())))) {				
					 state = ConstantsBase.BONUS_STATE_CANCELED;
				}
			}
		} catch (Exception e) {
			logger.warn("BMS, in getState. state: " + state);
		}
		return state;
	}

    /**
     * cancel bonus to user
     * @param conn
     * @param bonusUser
     * @param stateToUpdate
     * @param utcOffset
     * @param writerId
     * @throws SQLException
     */
    public static void cancelBonusToUser(Connection conn, BonusUsers bonusUser, long stateToUpdate,
    		String utcOffset, long writerId, long skinId, long loginId, String ip) throws SQLException {

    	BonusHandlerBase bh = BonusHandlerFactory.getInstance(bonusUser.getTypeId());

    	if (null != bh){
    		try {
				bh.cancelBonusToUser(conn, bonusUser, stateToUpdate, utcOffset, writerId, skinId, ip, loginId);
			} catch (BonusHandlersException e) {
				throw new SQLException();
			}
    	}
    }

    /**
     * get all user bonus by user id
     * @param userId
     * @return list of bonus
     * @throws SQLException
     */
    public static List<BonusUsers> getAllUserBonus(long userId, Date from, Date to, long state, String utcOffset, ArrayList<Currency> currencyList) throws SQLException {
    	List<BonusUsers> list = new ArrayList<BonusUsers>();
		Connection con = getConnection();
		try {
			list = BonusDAOBase.getAllUserBonus(con, userId, from, to, state, utcOffset, currencyList);
		} finally {
		    closeConnection(con);
		}
		return list;
	}
    
    /**
     * gets if there are any bonuses user has not seen 0 param means not seen
     * @param userId
     * @return
     * @throws SQLException
     */
    public static boolean isBonusNotSeen(long userId) throws SQLException {
	boolean isThereBonusNotSeen;	
        	Connection con = getConnection();
        	try {
        	    isThereBonusNotSeen = BonusDAOBase.hasNotSeenBonuses(con, userId, 0);
        	} finally {
        	    closeConnection(con);
        	}
        	return isThereBonusNotSeen;
    }

	public static List<BonusUsers> getTurnoverParamsForBonus(List<BonusUsers> bonusUserList) throws SQLException {
		List<BonusUsers> list = new ArrayList<BonusUsers>();
		Connection con = getConnection();
		try {
			list = BonusDAOBase.getTurnoverParamsForBonus(con, bonusUserList);
		} finally {
			closeConnection(con);
		}
		return list;
	}

    /**
     * Bonuses in states 1, 2, 3, 10 are considered opened(not final)
     * @param userId
     * @return true if there are bonuses in opened state
     */
	public static boolean hasOpenedBonuses(long userId) {
		Connection con = null;
		try {
			con = getConnection();
			return BonusDAOBase.hasOpenedBonuses(con, userId);
		} catch (SQLException e) {
			logger.debug("Unable to check for opened bonuses", e);
			return false;
		} finally {
			closeConnection(con);
		}
	}
	
	public static WithdrawBonusRegulationStateMethodResult getWithdrawBonusRegulationState(long userId, long skinId) {
		WithdrawBonusRegulationStateMethodResult wbrsmr = new WithdrawBonusRegulationStateMethodResult();
		if (BonusManagerBase.hasOpenedBonuses(userId)) {
			if (SkinsManagerBase.getSkin(skinId).getBusinessCaseId() != Skin.SKIN_BUSINESS_OUROBOROS) {
				wbrsmr.setState(WithdrawBonusStateEnum.NOT_REGULATED_OPEN_BONUSES);
			} else {
				wbrsmr.setState(WithdrawBonusStateEnum.REGULATED_OPEN_BONUSES);
			}
		} else {
			wbrsmr.setState(WithdrawBonusStateEnum.NO_OPEN_BONUSES);
		}

		return wbrsmr;
	}

	public static void updateAdjustedAmount(Connection con, long id, long amount) throws SQLException {
		BonusDAOBase.updateAdjustedAmount(con, id, amount);
	}

	public static ArrayList<BonusFormulaDetails> getBonusAmounts(Connection con, long investmentId, boolean isSettled) throws SQLException {
		return BonusDAOBase.getBonusAmounts(con, investmentId, isSettled);
	}

	public static void insertBonusInvestments(Connection con, long investmentId, long bonusUsersId, long bonusAmount, long adjustedAmountHistory, int typeId) throws SQLException {
		BonusDAOBase.insertBonusInvestments(con, investmentId, bonusUsersId, bonusAmount, adjustedAmountHistory, typeId);
	}
	
	public static long getAutoBonusByLoginId(long loginId) {
		Connection con = null;
		long bonusId = 0;
		try {
			con = getConnection();
			return BonusDAOBase.getAutoBonusByLoginId(con, loginId);
		} catch (SQLException e) {
			logger.error("get Auto Bonus By Login Id failed for login id " + loginId, e);
		} finally {
			closeConnection(con);
		}
		return bonusId;
	}

	public static boolean reverseNIOUBonus(Connection con, long investmentId) throws SQLException {
		logger.info("reverseNIOUBonus: investmentId = " + investmentId);
		long buId = BonusDAOBase.getNIOUBonusForReverse(con, investmentId);
		if (buId > 0) {
			return BonusDAOBase.reverseNIOUBonus(con, buId);
		} else {
			logger.info("No bonus for reverse");
			return false;
		}
	}
	
    /**
     * check if user can withdraw.
     * @param userId
     * @return true only if user dont have any bonus in state used else false
     * @throws SQLException
     */
    public static boolean userCanWithdraw(long userId, long balance, long amount) throws SQLException {
    	boolean flag = false;
		Connection con = getConnection();
		try {
			flag = BonusDAOBase.userCanWithdraw(con, userId, balance, amount);
		} finally {
		    closeConnection(con);
		}
		return flag;
	}
}