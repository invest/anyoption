/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.Tournament;
import com.anyoption.common.beans.TournamentUsersRank;
import com.anyoption.common.daos.TournamentUsersRankDAOBase;



/**
 * @author EyalG
 *
 */
public class TournamentUsersRankManagerBase extends BaseBLManager {
	//private static final Logger logger = Logger.getLogger(TournamentUsersRankManagerBase.class);
	
	public static ArrayList<TournamentUsersRank> getTournamentUsersWithUser(Tournament tournament, Long userId) throws SQLException {
		Connection conn = null;
		ArrayList<TournamentUsersRank> list = new ArrayList<TournamentUsersRank>();
		boolean isUserInList = false;
		try {
			conn = getConnection();
			list = TournamentUsersRankDAOBase.getTournamentUsers(conn, tournament.getId(), null);
			
			// check user existence in list
			if (!list.isEmpty() && userId != null) {
				for (TournamentUsersRank tUrank : list) {
					if (tUrank.getUserId() == userId.longValue()) {
						isUserInList = true;
					}
				}
			}
			
			if (!isUserInList && !list.isEmpty() && userId != null) {
				ArrayList<TournamentUsersRank> currentUser = TournamentUsersRankDAOBase.getTournamentUsers(conn, tournament.getId(), userId);
				if(!currentUser.isEmpty()){
					list.addAll(currentUser);
				}
			}
	
		} finally {
			closeConnection(conn);
		}
		return list;
	}

	
}
