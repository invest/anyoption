package com.anyoption.common.jobs;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.ServerConfiguration;
import com.anyoption.common.managers.JobsManagerBase;

public class JobsScheduler extends Thread {
    private static final Logger log = Logger.getLogger(JobsScheduler.class);
    
    private static final long QUERY_FOR_JOB_TO_RUN_INTERVAL = 1 * 60 * 1000; // 1 min

    private boolean running;
    private ScheduledJob currentJob;
    private String application;
    
    public JobsScheduler(String application) {
    	this.application = application;
    }
    
    public void run() {
        Thread.currentThread().setName("JobsScheduler");
        log.info("JobsScheduler Starting for application [" + application + "]...");
        running = true;
        while (running) {
            log.info("star run");
            long startTime = System.currentTimeMillis();

            try {
                List<Long> jobsIds = JobsManagerBase.getJobsIds();
                for (Long jId : jobsIds) {
                    Job job = null;
                    try {
                       job = JobsManagerBase.startJob(jId, ServerConfiguration.getInstance().getServerName(), application);
                    } catch (SQLException sqle) {
                        log.error("Error in starting job.", sqle);
                    }
                    if (null != job) {
                        boolean runCompleated = false;
                        try {
                            Class<?> cl = Class.forName(job.getJobClass());
                            currentJob = (ScheduledJob) cl.newInstance();
                            currentJob.setJobInfo(job);
                            log.info("start job id: " + job.getId());
                            long jobStartTime = System.currentTimeMillis();
                            runCompleated = currentJob.run();
                            currentJob = null;
                            log.info("end job id: " + job.getId() + " running time: " + (System.currentTimeMillis() - jobStartTime));
                        } catch (Throwable t) {
                            log.error("Error in job.", t);
                        } finally {
                            JobsManagerBase.stopJob(jId, ServerConfiguration.getInstance().getServerName(), runCompleated);
                        }
                    }
                }
            } catch (Exception e) {
                log.error("Error running jobs.", e);
            }

            long runTime = System.currentTimeMillis() - startTime;
            log.info("end run. time: " + runTime);
            if (QUERY_FOR_JOB_TO_RUN_INTERVAL > runTime && running) {
                synchronized (this) {
                    long waitTime = QUERY_FOR_JOB_TO_RUN_INTERVAL - runTime;
                    log.info("Wait for " + waitTime);
                    try {
                        this.wait(waitTime);
                    } catch (InterruptedException ie) {
                        // ignore
                    }
                }
            }
        }
        log.info("JobsScheduler exit");
    }

    public void wakeUp() {
        try {
            if (null != currentJob) {
                currentJob.stop();
            }
        } catch (Exception e) {
            log.error("Can't stop currentJob.", e);
        }
        synchronized (this) {
            try {
                this.notify();
            } catch (Exception e) {
                log.error("Can't wake up.", e);
            }
        }
    }
    
    public void stopJobsScheduler() {
        log.info("Stopping...");
        running = false;
        wakeUp();
    }
}