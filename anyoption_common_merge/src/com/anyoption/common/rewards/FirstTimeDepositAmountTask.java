package com.anyoption.common.rewards;

import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;

/**
 * @author liors
 *
 */
public class FirstTimeDepositAmountTask extends RewardTasksHandler {
	private static volatile FirstTimeDepositAmountTask firstTimeDepositAmountTask;
	private static final TaskGroupType groupType = TaskGroupType.FIRST_TIME_DEPOSIT_AMOUNT;
	private FirstTimeDepositAmountTask() {} 
	

	public static FirstTimeDepositAmountTask getInstance() {
		if (firstTimeDepositAmountTask == null) {
			firstTimeDepositAmountTask = new FirstTimeDepositAmountTask();
		}
		return firstTimeDepositAmountTask;
	}
		
	protected boolean getIsSkipped() {	
		return true;
	}


}
