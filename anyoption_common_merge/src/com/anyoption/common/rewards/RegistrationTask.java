package com.anyoption.common.rewards;

import java.sql.Connection;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardUser;
import com.anyoption.common.beans.User;
import com.anyoption.common.daos.RewardUsersDAO;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;

/**
 * @author liors
 *
 */
public class RegistrationTask extends RewardTasksHandler {
	private final static Logger logger = Logger.getLogger(RewardTasksHandler.class);
	private static volatile RegistrationTask registrationTask;
	private static final TaskGroupType groupType = TaskGroupType.REGISTRAITION;
	private RegistrationTask() {} 

	/**
	 * @return
	 */
	public static RegistrationTask getInstance() {
		if (registrationTask == null) {
			registrationTask = new RegistrationTask();
		}
		return registrationTask;
	}
	
	/**
	 * @return the groupType
	 */
	public static TaskGroupType getGroupType() {
		return groupType;
	}
	
	public boolean taskHandler(Connection connection, TaskUser taskUser, User user, Class<?> bonusManagerClass) throws Exception {
		RewardUsersDAO.insertValueObject(connection, new RewardUser(user.getId()));
		user.setRewardUser(new RewardUser(user.getId()));
		return super.taskHandler(connection, taskUser, user, bonusManagerClass);
	}
}
