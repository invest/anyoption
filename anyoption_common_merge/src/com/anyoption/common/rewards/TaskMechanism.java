package com.anyoption.common.rewards;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.RewardTier.TierChanges;
import com.anyoption.common.beans.User;

/**
 * @author liors
 *
 */
public interface TaskMechanism {
	
	/**
	 * Get all user's tasks for specific group type 
	 *
	 * @param connection
	 * @param taskUser
	 * @return TaskUser
	 * @throws TaskHandlerException
	 * @throws SQLException
	 */
	public TaskUser getUserTasks(Connection connection, TaskUser taskUser) throws Exception;
	
	/**
	 * For each user's task, evaluate and calculate (java layer):
	 * - Task action and task recurring.
	 * - Task done.
	 * - Experience Points for all the tasks.
	 * 
	 * @param connection
	 * @param taskUser
	 * @return
	 * @throws TaskHandlerException
	 * @throws SQLException
	 */
	public TaskUser calculateExperiencePoint(Connection connection, TaskUser taskUser) throws Exception;
	
	/**
	 * DB process for each task:
	 * - insert or update task to user.
	 * - insert to history table.
	 * @param connection
	 * @param taskUser
	 * @throws TaskHandlerException
	 * @throws SQLException
	 */
	public void updateTask(Connection connection, User user, TaskUser taskUser) throws Exception;
	
	//public TaskUser calculateCoins(Connection connection, TaskUser taskUser);
	
	/**
	 * Set user Experience Points
	 * Check if user tier need to be change according to the "constant" minimum experience points of the next tier.
	 * 
	 * @param connection
	 * @param taskUser
	 * @param user
	 * @return TierChanges - represent pair: <next user tier id, true if user move to the next tier>
	 * @throws TaskHandlerException
	 * @throws SQLException
	 */
	public TierChanges calculateTier(Connection connection, User user, TaskUser taskUser) throws Exception;
	
	/**
	 * Set user experience points
	 * DB process: Update reward user.
	 * 
	 * @param connection
	 * @param user
	 * @param taskUser
	 * @param tierId
	 * @throws SQLException
	 */
	public void updateRewardUser(Connection connection, User user, TaskUser taskUser, TierChanges tierChanges) throws Exception; 
	
	/**
	 * grant/update reward user.
	 * grant bonus to user if needed.
	 * insert to history table: grant & update.
	 * 
	 * @param connection
	 * @param taskUser
	 * @param tierId
	 * @param user
	 * @param bonusManagerClass
	 * @return
	 * @throws TaskHandlerException
	 * @throws SQLException
	 */
	public boolean calculateReward(Connection connection, User user, TaskUser taskUser, int tierId, Class<?> bonusManagerClass) throws Exception;
}
