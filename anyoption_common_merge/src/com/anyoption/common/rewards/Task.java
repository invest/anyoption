package com.anyoption.common.rewards;

import com.anyoption.common.beans.RewardTask;
import com.anyoption.common.beans.RewardUser;
import com.anyoption.common.beans.RewardUserPlatform;
import com.anyoption.common.beans.RewardUserTask;

/**
 * @author liors
 *
 */
public class Task {
	private RewardTask rewardTask;
	private RewardUser rewardUser;
	private RewardUserTask rewardUserTask;
	private RewardUserPlatform rewardUserPlatform;
	
	public Task() {
		rewardTask = new RewardTask();
		rewardUser = new RewardUser();
		rewardUserTask = new RewardUserTask();
		rewardUserPlatform = new RewardUserPlatform();
	}

	/**
	 * @return the rewardTask
	 */
	public RewardTask getRewardTask() {
		return rewardTask;
	}

	/**
	 * @param rewardTask the rewardTask to set
	 */
	public void setRewardTask(RewardTask rewardTask) {
		this.rewardTask = rewardTask;
	}

	/**
	 * @return the rewardUsers
	 */
	public RewardUser getRewardUser() {
		return rewardUser;
	}

	/**
	 * @param rewardUsers the rewardUsers to set
	 */
	public void setRewardUser(RewardUser rewardUser) {
		this.rewardUser = rewardUser;
	}

	/**
	 * @return the rewardUserTasks
	 */
	public RewardUserTask getRewardUserTask() {
		return rewardUserTask;
	}

	/**
	 * @param rewardUserTask the rewardUserTasks to set
	 */
	public void setRewardUserTask(RewardUserTask rewardUserTask) {
		this.rewardUserTask = rewardUserTask;
	}

	/**
	 * @return the rewardUserPlatform
	 */
	public RewardUserPlatform getRewardUserPlatform() {
		return rewardUserPlatform;
	}

	/**
	 * @param rewardUserPlatform the rewardUserPlatform to set
	 */
	public void setRewardUserPlatform(RewardUserPlatform rewardUserPlatform) {
		this.rewardUserPlatform = rewardUserPlatform;
	}
}
