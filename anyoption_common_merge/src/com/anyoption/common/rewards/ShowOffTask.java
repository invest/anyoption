package com.anyoption.common.rewards;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;

/**
 * @author liors
 *
 */
public class ShowOffTask extends RewardTasksHandler {
	private final static Logger logger = Logger.getLogger(RewardTasksHandler.class);
	private static volatile ShowOffTask showOffTask;
	private static final TaskGroupType groupType = TaskGroupType.SHOW_OFF;
	private ShowOffTask() {} 

	/**
	 * @return
	 */
	public static ShowOffTask getInstance() {
		if (showOffTask == null) {
			showOffTask = new ShowOffTask();
		}
		return showOffTask;
	}
}
