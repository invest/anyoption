package com.anyoption.common.rewards;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardBenefitManagement;
import com.anyoption.common.beans.RewardUserBenefit;
import com.anyoption.common.beans.RewardUserHistory;
import com.anyoption.common.daos.RewardUserBenefitsDAO;
import com.anyoption.common.daos.RewardUsersHistoryDAO;
import com.anyoption.common.managers.RewardManager;
import com.anyoption.common.managers.RewardUsersHistoryManager;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author liors
 *
 */
public class RewardUtil {
	public static final Logger logger = Logger.getLogger(RewardTasksHandler.class);
	
	public static int safeLongToInt(long l) {
	    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
	    	logger.warn("WARNING,  not safe to convert long to int");
	    	return 0;
	    }
	    return (int) l;
	}
	
	public static boolean grantRewards(Connection connection, long userId, int writerId, int tierId, boolean isSkipped,  Class<?> bonusManagerClass, double experiencePoints) throws Exception {
		logger.info(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "grantRewards if found, userId: " + userId);
		boolean res = true; //TODO
		ArrayList<RewardBenefitManagement> list = RewardUserBenefitsDAO.getUserBenefitToGrant(connection, userId, tierId, isSkipped);
		for (RewardBenefitManagement rewardBenefitManagement : list) {
			int rewardActionId = rewardBenefitManagement.getUserBenefitId();
			if (rewardBenefitManagement.getUserBenefitId() == 0) { //first time need insert
				rewardActionId = RewardUserBenefitsDAO.insertValueObject(connection, 
						new RewardUserBenefit(userId, rewardBenefitManagement.getBenefitId(), true));
			} else { //update
				RewardUserBenefitsDAO.update(connection, rewardBenefitManagement.getUserBenefitId(), true);
			}
			if (rewardBenefitManagement.getBonusId() > 0) {
				try {
					Method insertBonusMethod = bonusManagerClass.getMethod("generateInsertBonus", Connection.class, Long.class, Long.class); 
					insertBonusMethod.invoke(null, connection, userId, rewardBenefitManagement.getBonusId());
				} catch (Exception e) {
					logger.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "ERROR insert bonusId: " + rewardBenefitManagement.getBonusId() + "!!! to userId: " + userId, e);
				}
			}
			RewardUsersHistoryDAO.insertValueObject(connection, new RewardUserHistory(userId, rewardActionId, 0, 
					RewardUsersHistoryManager.REWARD_GRANTED, writerId, tierId, experiencePoints, RewardManager.BENEFIT, null, 0));
		}		
		return res;
	}
}
