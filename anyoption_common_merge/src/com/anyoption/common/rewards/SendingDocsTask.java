package com.anyoption.common.rewards;

import org.apache.log4j.Logger;

/**
 * @author liors
 *
 */
public class SendingDocsTask extends RewardTasksHandler {
	private final static Logger logger = Logger.getLogger(RewardTasksHandler.class);
	private static volatile SendingDocsTask sendingDocsTask;
	private SendingDocsTask() {}
	
	/**
	 * 
	 * @return sendingDocsTask as SendingDocsTask
	 */
	public static SendingDocsTask getInstance() {
		if (sendingDocsTask == null) {
			sendingDocsTask = new SendingDocsTask();
		}
		return sendingDocsTask;
	}
}
