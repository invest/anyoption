package com.anyoption.common.rewards;

import org.apache.log4j.Logger;

/**
 * @author liors
 *
 */
public class FriendsInviteTask extends RewardTasksHandler {
	private static final Logger logger = Logger.getLogger(FriendsInviteTask.class);
	private static volatile FriendsInviteTask friendsInviteTask;

	private FriendsInviteTask() {} 

	public synchronized FriendsInviteTask getInstance() {
		if (friendsInviteTask == null) {
			friendsInviteTask = new FriendsInviteTask();
		}
		return friendsInviteTask;
	}
}
