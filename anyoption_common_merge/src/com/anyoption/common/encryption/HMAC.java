package com.anyoption.common.encryption;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

public class HMAC {
	private static final Logger logger = Logger.getLogger(HMAC.class);
 
	  public static boolean isValid(Map<String, String> parameters, String hashKey) {
		  String secureHash = parameters.get("secure_hash");
		  if(secureHash == null) {
			  logger.error("Invalid input no secure_hash:"+parameters);
			  return false;
		  }
		  SortedMap<String, String> keySortedParams = new TreeMap<String, String>(parameters);
		  String input = "";
		  for (Map.Entry<String, String> entry : keySortedParams.entrySet()) {
			  if(!entry.getKey().equals("secure_hash")) {
				  input += entry.getValue();
			  }
	      }
		  String secureHashGenerated = hmacDigest(input, hashKey, "HmacSha1");
		  boolean isValid = secureHash.equals(secureHashGenerated);
		  if(!isValid){
			  Log.error("Invalid request :"+ parameters);
		  }
		  return isValid;
	  }

	  public static String hmacDigest(String msg, String keyString, String algo) {
	    String digest = null;
	    try {
	      SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), algo);
	      Mac mac = Mac.getInstance(algo);
	      mac.init(key);

	      byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

	      StringBuffer hash = new StringBuffer();
	      for (int i = 0; i < bytes.length; i++) {
	        String hex = Integer.toHexString(0xFF & bytes[i]);
	        if (hex.length() == 1) {
	          hash.append('0');
	        }
	        hash.append(hex);
	      }
	      digest = hash.toString();
	      
	    } catch (UnsupportedEncodingException |InvalidKeyException| NoSuchAlgorithmException e) {
	    	e.printStackTrace();
	    }
	    return digest;
	  }
	}