package com.anyoption.common.sms;

public class SMSException extends Exception {

	private static final long serialVersionUID = 1045299414732560207L;

	public SMSException() {
        super();
    }

    public SMSException(String message) {
        super(message);
    }

    public SMSException(String message, Throwable t) {
        super(message, t);
    }
}