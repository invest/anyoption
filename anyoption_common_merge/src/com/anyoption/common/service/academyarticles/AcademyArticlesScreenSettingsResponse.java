package com.anyoption.common.service.academyarticles;

import java.util.Hashtable;

import com.anyoption.common.beans.base.Language;
import com.anyoption.common.service.results.PermissionsGetResponseBase;

public class AcademyArticlesScreenSettingsResponse extends PermissionsGetResponseBase {

	private Hashtable<Long, Language> languages;

	public Hashtable<Long, Language> getLanguages() {
		return languages;
	}

	public void setLanguages(Hashtable<Long, Language> languages) {
		this.languages = languages;
	}
}
