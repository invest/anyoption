package com.anyoption.common.service.academyarticles;

import java.util.ArrayList;

import com.anyoption.common.beans.base.AcademyArticle;
import com.anyoption.common.service.results.MethodResult;

public class AcademyArticlesGetAllMethodResult extends MethodResult {

	private ArrayList<AcademyArticle> articles;
	private long totalCount;
	
	public ArrayList<AcademyArticle> getArticles() {
		return articles;
	}
	
	public void setArticles(ArrayList<AcademyArticle> articles) {
		this.articles = articles;
	}
	
	public long getTotalCount() {
		return totalCount;
	}
	
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}	
}
