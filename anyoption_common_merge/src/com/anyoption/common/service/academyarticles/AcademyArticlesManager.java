package com.anyoption.common.service.academyarticles;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.anyoption.common.beans.base.AcademyArticle;
import com.anyoption.common.beans.base.AcademyArticleTopic;
import com.anyoption.common.managers.BaseBLManager;

public class AcademyArticlesManager extends BaseBLManager {

	public static ArrayList<AcademyArticle> getAll(AcademyArticlesGetAllMethodResult response,
			AcademyArticlesMethodRequest request) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return AcademyArticlesDAOBase.getAll(con, response, request);
		} finally {
			closeConnection(con);
		}
	}
	
	public static long addArticles(AcademyArticlesMethodRequest request) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return AcademyArticlesDAOBase.addArticles(con, request);
			
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<Long> editArticles(AcademyArticlesMethodRequest request) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return AcademyArticlesDAOBase.editArticles(con, request);
		} finally {
			closeConnection(con);
		}
	}

	public static void deleteArticle(long articleId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			AcademyArticlesDAOBase.deleteArticle(con, articleId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<AcademyArticleTopic> getTopicSuggestions(String topicString, String locale) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return AcademyArticlesDAOBase.getTopicSuggestions(con, topicString, locale);
		} finally {
			closeConnection(con);
		}
	}
	
	public static AcademyArticle getArticle(long articleId, String locale) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return AcademyArticlesDAOBase.getArticle(con, articleId, locale);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<AcademyArticleTopic> getTopics(String locale) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return AcademyArticlesDAOBase.getTopics(con, locale);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<AcademyArticle> getHomePageArticles(String locale) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return AcademyArticlesDAOBase.getHomePageArticles(con, locale);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<AcademyArticle> getSearchedArticles(ArrayList<Long> topics, String locale) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return AcademyArticlesDAOBase.getSearchedArticles(con, topics, locale);
		} finally {
			closeConnection(con);
		}
	}
}
