package com.anyoption.common.service.academyarticles;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.AcademyArticle;
import com.anyoption.common.beans.base.AcademyArticleTopic;
import com.anyoption.common.beans.base.AcademyArticleTranslation;
import com.anyoption.common.daos.DAOBase;

import oracle.jdbc.OracleTypes;

public class AcademyArticlesDAOBase extends DAOBase {

	private static final Logger log = Logger.getLogger(AcademyArticlesDAOBase.class);
	
	public static ArrayList<AcademyArticle> getAll(Connection con, AcademyArticlesGetAllMethodResult response,
			AcademyArticlesMethodRequest request) throws SQLException {
		ArrayList<AcademyArticle> articles = new ArrayList<AcademyArticle>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		long totalCount = 0;
		int index = 1;
		try {			   
			cstmt = con.prepareCall("{call pkg_acadarticles.get_all(o_articles => ? " +
																	",i_locale=> ? " +
																	",i_page => ? " +
																    ",i_rows_per_page => ?)}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setString(index++, request.getArticleLocale());
			cstmt.setLong(index++, request.getPage());
			cstmt.setLong(index++, request.getRowsPerPage());

			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				AcademyArticle article = new AcademyArticle();
				AcademyArticleTranslation translation = new AcademyArticleTranslation();
				article.setId(rs.getLong("id"));
				article.setActive(rs.getBoolean("is_active"));
				article.setDeleted(rs.getBoolean("is_deleted"));
				article.setTimeCreated(rs.getTimestamp("time_created").getTime());
				article.setTimeUpdated(rs.getTimestamp("time_updated").getTime());
				article.setHomePagePosition(rs.getLong("home_page_position"));
				
				translation.setTitle(rs.getString("title"));
				translation.setSubtitle(rs.getString("subtitle"));
				translation.setAuthor(rs.getString("author"));
	
				article.getTranslations().add(translation);
				articles.add(article);
				
				totalCount = rs.getLong("total_count");
			}
			response.setTotalCount(totalCount);
			response.setArticles(articles);
		} finally {
			closeResultSet(rs);
			closeStatement(cstmt);
		}
		return articles;
	}

	public static long addArticles(Connection con, AcademyArticlesMethodRequest request) throws SQLException {
		AcademyArticle article = request.getArticle();
		long articleId = 0;
		con.setAutoCommit(false);

		articleId = addArticle(con, request);
		for (int i = 0; i < article.getTranslations().size(); i++) {
			try {
				if(articleId != 0) {
					addArticleInfo(con, articleId, i, request);
				}
			} catch (SQLException e1) {
				try {
					con.rollback();
				} catch (SQLException er) {
					log.log(Level.ERROR, "Can't rollBack! ", er);
					throw er;
				} 
			} 
		}
		con.commit();
		try {
			con.setAutoCommit(true);
		} catch (SQLException e) {
			log.error("Can't set back to autocommit.", e);
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException e) {
				log.error("Can't set back to autocommit.", e);
				throw e;
			}
		}

		return articleId;
	}
	
	public static long addArticle(Connection con, AcademyArticlesMethodRequest request) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_acadarticles.add_article(I_IMAGE => ? " + 
																	",I_ACTIVE => ? " + 
					   												",I_DELETED => ? " + 
																	",I_HOME_PAGE_POSITION =>?)}");
			
			setStatementValue(request.getArticle().getImageUrl(), index++, cstmt);
			cstmt.setBoolean(index++, request.getArticle().isActive());
			cstmt.setBoolean(index++, request.getArticle().isDeleted());
			setStatementValue(request.getArticle().getHomePagePosition(), index++, cstmt);
			 
			cstmt.execute();
			
		} finally {
			closeStatement(cstmt);
		}
		return getLastArticle(con);
	}
	
	public static ArrayList<Long> editArticles(Connection con, AcademyArticlesMethodRequest request) throws SQLException {
		ArrayList<Long> editedArticles = new ArrayList<Long>();
		try {
			con.setAutoCommit(false);
		} catch (SQLException e1) {
			log.error("Can't set autocommit to false.", e1);
		}
		AcademyArticle article = request.getArticle();
		for (int i = 0; i < article.getTranslations().size(); i++) {
			try {
				editedArticles.add(new Long(editArticle(con, request, i)));
			} catch (SQLException e1) {
				try {
					con.rollback();
				} catch (SQLException er) {
					log.log(Level.ERROR, "Can't rollBack! ", er);
					throw er;
				} 
			}
		}

		con.commit();
		try {
			con.setAutoCommit(true);
		} catch (SQLException e) {
			log.error("Can't set back to autocommit.", e);
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException e) {
				log.error("Can't set back to autocommit.", e);
				throw e;
			}
		}
		return editedArticles;
	}

	public static long editArticle(Connection con, AcademyArticlesMethodRequest request, int position) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		long res = 0;
		try {
			cstmt = con.prepareCall("{call pkg_acadarticles.edit_article(I_ID => ? " +
					 												",I_TITLE => ? " +
					   												",I_SUBTITLE => ? " +
																	",I_AUTHOR => ? " + 
					   												",I_IMAGE => ? " +
																	",I_ACTIVE => ? " + 
					   												",I_DELETED => ? " + 
																	",I_HOME_PAGE_POSITION => ? " + 
																	",I_LOCALE => ? " + 
																	",I_WRITER_ID => ? " + 
					   												",I_TOPICS => ?)}");

			ArrayList<String> topics = request.getArticle().getTranslations().get(position).getTopics();
			if (topics != null) {
				for(int i = 0; i <topics.size(); i++) {
					String s = topics.get(i).toUpperCase();
					topics.remove(i);
					topics.add(i, s);
				}
			} 
			request.setTopics(topics);

			cstmt.setLong(index++, request.getArticle().getId());
			setStatementValue( request.getArticle().getTranslations().get(position).getTitle(), index++, cstmt);
			setStatementValue(request.getArticle().getTranslations().get(position).getSubtitle(), index++, cstmt);
			setStatementValue( request.getArticle().getTranslations().get(position).getAuthor(), index++, cstmt);
			setStatementValue(request.getArticle().getImageUrl(), index++, cstmt);
			cstmt.setBoolean(index++, request.getArticle().isActive());
			cstmt.setBoolean(index++, request.getArticle().isDeleted());
			setStatementValue(request.getArticle().getHomePagePosition(), index++, cstmt);
			cstmt.setString(index++, request.getArticle().getTranslations().get(position).getLocale());
			cstmt.setLong(index++, request.getWriterId());
			if (request.getArticle().getTranslations().get(position).getTopics() == null ||
				request.getArticle().getTranslations().get(position).getTopics().size() == 0) {
				cstmt.setNull(index++, Types.ARRAY, "STRING_TABLE");
			} else {
				cstmt.setArray(index++, getPreparedSqlArray(con, request.getArticle().getTranslations().get(position).getTopics()));
			}
			
			res = cstmt.executeUpdate();
			
		} finally {
			closeStatement(cstmt);
		}
		return res;
	}
	
	public static void deleteArticle(Connection con, long articleId) throws SQLException {
		CallableStatement cstmt = null;
		try {
			cstmt = con.prepareCall("{call pkg_acadarticles.delete_article(I_ID => ?)}");

			cstmt.setLong(1, articleId);
			cstmt.execute();
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static ArrayList<AcademyArticleTopic> getTopicSuggestions(Connection con, String topicString, String locale)
			throws SQLException {
		ArrayList<AcademyArticleTopic> suggestions = new ArrayList<AcademyArticleTopic>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_acadarticles.get_topic_suggestions(O_SUGGESTIONS => ?" +
																				",I_PREFIX => ?" +
																				",I_LOCALE =>?)}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setString(index++, topicString);
			cstmt.setString(index++, locale);
			
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			
			while (rs.next()) {
				AcademyArticleTopic topic = new AcademyArticleTopic();
				topic.setId(rs.getLong("id"));
				topic.setTitle( rs.getString("title"));
				suggestions.add(topic);
			}
			
		} finally {
			closeResultSet(rs);
			closeStatement(cstmt);
		}		
		return suggestions;
	}

	public static AcademyArticle getArticle(Connection con, long articleId, String locale) throws SQLException {
		AcademyArticle article = new AcademyArticle();
		AcademyArticleTranslation translation = new AcademyArticleTranslation();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		int counter = 0;
		int size = 0;
		try {
			cstmt = con.prepareCall("{call pkg_acadarticles.get_article(O_ARTICLE => ? " + 
																		",I_ID => ? " +
																	    ",I_LOCALE => ?)}");

			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, articleId);
			setStatementValue(locale, index++, cstmt);
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				String topicLocale = rs.getString("locale");
				article.setId(rs.getLong("id"));
				
				if (translation.getLocale() != null && !translation.getLocale().equals(topicLocale)) {
					article.getTranslations().add(translation);
					translation = new AcademyArticleTranslation();
					translation.setLocale(topicLocale);
					size++;
				} else {
					translation.setLocale(topicLocale);
				}
				translation.setTitle(rs.getString("title"));
				translation.setSubtitle(rs.getString("subtitle"));
				translation.setAuthor(rs.getString("author"));

				String topicTitle = rs.getString("topic_title");
				if (topicTitle != null) {
					translation.getTopics().add(topicTitle);
				}
				
				if (counter == 0) {
					article.setImageUrl(rs.getString("image"));
					article.setActive(rs.getBoolean("is_active"));
					article.setDeleted(rs.getBoolean("is_deleted"));
					article.setTimeCreated(rs.getTimestamp("time_created").getTime());
					article.setTimeUpdated(rs.getTimestamp("time_updated").getTime());
					article.setHomePagePosition(rs.getLong("home_page_position"));
					article.getTopicIds().add(rs.getLong("topic_translation_id"));
					counter++;
				}
			}	
			if(article.getTranslations().size() == 0 && translation.getTitle() != null) {
				article.getTranslations().add(translation);
			} else if(article.getTranslations().size() == size ){
				article.getTranslations().add(translation);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(cstmt);
		}
		return article;
	}

	public static ArrayList<AcademyArticleTopic> getTopics(Connection con, String locale) throws SQLException {
		ArrayList<AcademyArticleTopic> topics = new ArrayList<AcademyArticleTopic>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			cstmt = con.prepareCall("{call pkg_acadarticles.get_topics(O_TOPICS => ? " +
																	  ",I_LOCALE => ?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, locale);
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				AcademyArticleTopic topic = new AcademyArticleTopic();
				topic.setId(rs.getLong("topic_id"));
				topic.setTitle( rs.getString("title"));
				topics.add(topic);
			}	
		} finally {
			closeResultSet(rs);
			closeStatement(cstmt);
		}
		return topics;
	}

	public static ArrayList<AcademyArticle> getHomePageArticles(Connection con, String locale) throws SQLException {
		ArrayList<AcademyArticle> homePageArticles = new ArrayList<AcademyArticle>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			cstmt = con.prepareCall("{call pkg_acadarticles.get_home_page_articles(O_HOME_PAGE_ARTICLES => ? " + 
																				   ",I_LOCALE => ?)}");

			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, locale);
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				AcademyArticle article = new AcademyArticle();
				AcademyArticleTranslation translation = new AcademyArticleTranslation();
				article.setId(rs.getLong("id"));
				
				translation.setTitle(rs.getString("title"));
				translation.setSubtitle(rs.getString("subtitle"));
				translation.setAuthor(rs.getString("author"));
	
				article.setImageUrl(rs.getString("image"));
				article.setActive(rs.getBoolean("is_active"));
				article.setDeleted(rs.getBoolean("is_deleted"));
				article.setHomePagePosition(rs.getLong("home_page_position"));
				article.setTimeCreated(convertToTimestamp(rs.getDate("time_created")).getTime());
				article.setTimeUpdated(convertToTimestamp(rs.getDate("time_updated")).getTime());	

				article.getTranslations().add(translation);
				
				homePageArticles.add(article);
			}	
		} finally {
			closeResultSet(rs);
			closeStatement(cstmt);
		}
		return homePageArticles;
	}

	public static ArrayList<AcademyArticle> getSearchedArticles(Connection con, ArrayList<Long> topics, String locale)
			throws SQLException {
		ArrayList<AcademyArticle> articles = new ArrayList<AcademyArticle>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_acadarticles.search_articles(O_ARTICLES => ? " + 
																		   ",I_TOPICS => ? " +
																		   ",I_LOCALE => ?)}");

			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			setArrayVal(topics, index++, cstmt, con);
			cstmt.setString(index++, locale);
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				AcademyArticle article = new AcademyArticle();
				AcademyArticleTranslation translation = new AcademyArticleTranslation();
				article.setId(rs.getLong("id"));
				
				translation.setTitle(rs.getString("title"));
				translation.setSubtitle(rs.getString("subtitle"));
				translation.setAuthor(rs.getString("author"));
	
				article.setImageUrl(rs.getString("image"));
				article.setActive(rs.getBoolean("is_active"));
				article.setDeleted(rs.getBoolean("is_deleted"));
				article.setHomePagePosition(rs.getLong("home_page_position"));
				article.setTimeCreated(convertToTimestamp(rs.getDate("time_created")).getTime());
				article.setTimeUpdated(convertToTimestamp(rs.getDate("time_updated")).getTime());	

				article.getTranslations().add(translation);
				
				articles.add(article);
			}	
		} finally {
			closeResultSet(rs);
			closeStatement(cstmt);
		}
		return articles;
	}
	
	private static long getLastArticle(Connection con) throws SQLException {
		long articleId = 0;
		CallableStatement cstmt = null;
		try {
			cstmt = con.prepareCall("{call pkg_acadarticles.get_last_article(O_ARTICLE => ?)}");

			cstmt.registerOutParameter(1, OracleTypes.NUMBER);
			cstmt.execute();
			
			articleId = cstmt.getBigDecimal(1).longValue();		
		} finally {
			closeStatement(cstmt);
		}
		return articleId;
	}
	
	private static void addArticleInfo(Connection con, long articleId, int position,
			AcademyArticlesMethodRequest request) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_acadarticles.add_article_translations(I_ID => ? " + 
																					",I_TITLE => ? " + 
									   												",I_SUBTITLE => ? " + 
									   												",I_AUTHOR => ? " + 
									   												",I_LOCALE => ? " + 
																					",I_TOPICS =>?)}");
			
			ArrayList<String> topics = request.getArticle().getTranslations().get(position).getTopics();
			if (topics != null && topics.size() > 0) {
				for(int i = 0; i <topics.size(); i++) {
					String s = topics.get(i).toUpperCase();
					topics.remove(i);
					topics.add(i, s);
				}
			}
			request.setTopics(topics);

			cstmt.setLong(index++, articleId);
			setStatementValue(request.getArticle().getTranslations().get(position).getTitle(), index++, cstmt);
			setStatementValue(request.getArticle().getTranslations().get(position).getSubtitle(), index++, cstmt);
			setStatementValue(request.getArticle().getTranslations().get(position).getAuthor(), index++, cstmt);

			cstmt.setString(index++, request.getArticle().getTranslations().get(position).getLocale());
			if (request.getArticle().getTranslations().get(position).getTopics() == null ||
				request.getArticle().getTranslations().get(position).getTopics().size() == 0) {
				cstmt.setNull(index++, Types.ARRAY, "STRING_TABLE");
			} else {
				cstmt.setArray(index++, getPreparedSqlArray(con, request.getArticle().getTranslations().get(position).getTopics()));
			}
			 
			cstmt.execute();
			
		} finally {
			closeStatement(cstmt);
		}
	}
}
