package com.anyoption.common.service.academyarticles;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.base.AcademyArticle;
import com.anyoption.common.beans.base.AcademyArticleTopic;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.WriterPermisionsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

public class AcademyArticlesServices {
	private static final Logger log = Logger.getLogger(AcademyArticlesServices.class);
	private static final String SCREEN_NAME = "academyFiles";

	 @BackendPermission(id = "academyFiles_view")
	public static AcademyArticlesScreenSettingsResponse getScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getScreenSettings:" + request);
		AcademyArticlesScreenSettingsResponse response = new AcademyArticlesScreenSettingsResponse();
		try {
			response.setPermissionsList(
					WriterPermisionsManagerBase.getWriterScreenPermissions(SCREEN_NAME, request.getWriterId()));
			response.setLanguages(LanguagesManagerBase.getLanguages());
		} catch (SQLException e) {
			log.error("Can't getScreenSettings for academy articles", e);
		}
		return response;
	}

	 @BackendPermission(id = "academyFiles_view")
	public static AcademyArticlesGetAllMethodResult getAll(AcademyArticlesMethodRequest request) {
		log.debug("getAll:" + request);
		AcademyArticlesGetAllMethodResult response = new AcademyArticlesGetAllMethodResult();
		try {
			ArrayList<AcademyArticle> articles = AcademyArticlesManager.getAll(response, request);
			response.setArticles(articles);
		} catch (SQLException e) {
			log.error("Can't getAll academy articles", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}

	 @BackendPermission(id = "academyFiles_view")
	public static MethodResult addArticle(AcademyArticlesMethodRequest request) {
		log.debug("addArticle:" + request);
		MethodResult response = new MethodResult();
		if (request != null) {
			try {
				long articleId = AcademyArticlesManager.addArticles(request);
				if (articleId != 0) {
					request.getArticle().setId(articleId);
					saveArticle(request);
				}
			} catch (SQLException e) {
				log.error("Can't add new article", e);
				response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
		} else {
			log.error("The request is null in addArticle");
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}

	 @BackendPermission(id = "academyFiles_view")
	public static MethodResult editArticle(AcademyArticlesMethodRequest request) {
		log.debug("editArticle:" + request);
		MethodResult response = new MethodResult();
		if (request != null) {
			try {
				AcademyArticlesManager.editArticles(request);
				saveArticle(request);
			} catch (SQLException e) {
				log.error("Error in editArticle", e);
				response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
		} else {
			log.error("The request is null in editArticle");
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}

	 @BackendPermission(id = "academyFiles_view")
	public static MethodResult saveArticle(AcademyArticlesMethodRequest request) {
		log.debug("saveArticle:" + request);
		MethodResult response = new MethodResult();
		try {
			AcademyArticle article = AcademyArticlesManager.getArticle(request.getArticle().getId(), null);
			if (article != null) {
				Writer out = null;
				for (int i = 0; i < article.getTranslations().size(); i++) {
					String fileName = CommonUtil.getProperty(ConstantsBase.ACADEMY_FILES_PATH) + "article_"
							+ request.getArticle().getId() + "_"
							+ request.getArticle().getTranslations().get(i).getLocale() + ".html";

					out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"));
					out.write(request.getArticle().getTranslations().get(i).getHtml());
					out.flush();
				}

				if (out != null) {
					out.close();
				}
			} else {
				log.error("Can't find fileName :" + request);
				response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
		} catch (Exception e) {
			log.error("Unable to get saveArticle", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	 
	 @BackendPermission(id = "academyFiles_view")
	public static AcademyArticlesMethodResult getArticleById(AcademyArticlesMethodRequest request) {
			log.debug("getArticle:" + request);
			AcademyArticlesMethodResult response = new AcademyArticlesMethodResult();
			try {
				AcademyArticle article = AcademyArticlesManager.getArticle(request.getArticle().getId(), null);
				if (article != null) {
					for (int i = 0; i < article.getTranslations().size(); i++) {
						article.getTranslations().get(i).setHtml(getArticleFileBase("article_" + article.getId() + "_"
								+ article.getTranslations().get(i).getLocale() + ".html"));
						response.setArticle(article);
					}
				} else {
					log.debug("Can't find fileName :" + request);
					response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
				}
			} catch (Exception e) {
				log.error("Unable to get getArticle", e);
				response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
			return response;
	}
	 
	 @BackendPermission(id = "academyFiles_view")
	 public static MethodResult deleteArticle(AcademyArticlesMethodRequest request) {
		 log.debug("deleteArticle:" + request);
		 MethodResult response = new MethodResult();
		 try {
			 AcademyArticlesManager.deleteArticle(request.getArticle().getId());
		 } catch (Exception e) {
				log.error("Unable to delete article", e);
				response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
		 return response;
	 }
	 
	 @BackendPermission(id = "academyFiles_view")
	 public static AcademyArticlesMethodResult getTopicSuggestions(AcademyArticlesMethodRequest request) {
		 AcademyArticlesMethodResult response = new AcademyArticlesMethodResult();
		 try {
				response.setTopics(AcademyArticlesManager.getTopicSuggestions(request.getTopicString(), request.getArticleLocale()));
			} catch (Exception e) {
				log.error("Unable to get getArticle", e);
				response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}		 
		 return response;
	 }

	public static AcademyArticlesMethodResult getArticle(AcademyArticlesMethodRequest request) {
		log.debug("getArticle:" + request);
		AcademyArticlesMethodResult response = new AcademyArticlesMethodResult();
		try {
			AcademyArticle article = AcademyArticlesManager.getArticle(request.getArticle().getId(), request.getArticleLocale());
			if (article != null) {
				for (int i = 0; i < article.getTranslations().size(); i++) {
					article.getTranslations().get(i).setHtml(getArticleFileBase("article_" + article.getId() + "_"
							+ article.getTranslations().get(i).getLocale() + ".html"));
				}
				response.setArticle(article);
			} else {
				log.debug("Can't find fileName :" + request);
				response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
		} catch (Exception e) {
			log.error("Unable to get getArticle", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}

	private static String getArticleFileBase(String fileName) {
		String result = null;
		String dirFileName = CommonUtil.getProperty(ConstantsBase.ACADEMY_FILES_PATH) + fileName;
		BufferedReader br = null;
		try {
			FileInputStream inFile = new FileInputStream(dirFileName);
			
			br = new BufferedReader(new InputStreamReader(inFile, "UTF8"));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			
			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			result = sb.toString();
		} catch (Exception e) {
			log.error("Can't get academy article in getArticleFileBase");
		} finally {
			try {
				if (br == null) {
					return "";
				}
				br.close();
			} catch (IOException e) {
				log.error("Can't close buffer ", e);
			}
		}
		return result;
	}

	public static AcademyArticlesMethodResult getTopics(AcademyArticlesMethodRequest request) {
		AcademyArticlesMethodResult response = new AcademyArticlesMethodResult();
		ArrayList<AcademyArticleTopic> topics = new ArrayList<AcademyArticleTopic>();
		try {
			topics = AcademyArticlesManager.getTopics(request.getArticleLocale());
			response.setTopics(topics);
		} catch (SQLException e) {
			log.error("Can't get articles' topics in getTopics ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}

	public static AcademyArticlesMethodResult getHomePageArticles(AcademyArticlesMethodRequest request) {
		AcademyArticlesMethodResult response = new AcademyArticlesMethodResult();
		ArrayList<AcademyArticle> homePageArticles = new ArrayList<AcademyArticle>();
		try {
			homePageArticles = AcademyArticlesManager.getHomePageArticles(request.getArticleLocale());
			response.setHomePageArticles(homePageArticles);
		} catch (SQLException e) {
			log.error("Can't get home page articles ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}

	public static AcademyArticlesMethodResult searchArticle(AcademyArticlesMethodRequest request) {
		AcademyArticlesMethodResult response = new AcademyArticlesMethodResult();
		ArrayList<AcademyArticle> searched = new ArrayList<AcademyArticle>();
		try {
			searched = AcademyArticlesManager.getSearchedArticles(request.getTopicIds(), request.getArticleLocale());
			response.setSearchedArticles(searched);
		} catch (SQLException e) {
			log.error("Couldn't search articles ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
}
