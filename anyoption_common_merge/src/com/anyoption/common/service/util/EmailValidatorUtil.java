package com.anyoption.common.service.util;

import java.sql.SQLException;

import org.apache.commons.validator.EmailValidator;
import org.apache.log4j.Logger;

import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.util.CommonUtil;

public class EmailValidatorUtil {
	
	public final static int EMAIL_VALID = 0;
	public final static int EMAIL_FAILED_REGEX_VALIDATION = 1;
	public final static int EMAIL_FAILED_MX_VALIDATION = 2;
	public final static int EMAIL_FAILED_EXIST_VALIDATION = 3;

	public static final Logger log = Logger.getLogger(EmailValidatorUtil.class);
	
	public static int validate(String email) {
		if(checkRegex(email)) {
			if(checkMX(email)) {
				if(!checkExists(email)) {
					return EMAIL_VALID;
				} else {
					return EMAIL_FAILED_EXIST_VALIDATION;
				}
			} else {
				return EMAIL_FAILED_MX_VALIDATION;
			}
		} else {
			return EMAIL_FAILED_REGEX_VALIDATION;
		}
	}
	
	private static boolean checkRegex(String email) {
		return EmailValidator.getInstance().isValid(email);
	}
	
	private static boolean checkExists(String email) {
		try {
			return UsersManagerBase.isEmailInUseIgnoreCase(email);
		} catch (SQLException e) {
			log.debug("Can't check mail exists: "+e.getMessage());
			return false;
		}
	}
	
	private static boolean checkMX(String email) {
		return CommonUtil.isEmailValidUnix(email);
	}
}
