/**
 * 
 */
package com.anyoption.common.service.gmwithdrawal;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class GmWithdrawalPermissionToDisplayRequest extends MethodRequest {
	private long userId;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "GmWithdrawalPermissionToDisplayRequest: "
            + super.toString()
            + "userId: " + userId + ls;
    }
}
