/**
 * 
 */
package com.anyoption.common.service.gmwithdrawal;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author pavel.tabakov
 *
 */
public class GmWithdrawalValidateResult extends MethodResult {

	private long fee;

	public long getFee() {
		return fee;
	}

	public void setFee(long fee) {
		this.fee = fee;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "GmWithdrawalValidateResult: " + super.toString() + "fee: " + fee + ls;
	}

}
