/**
 * 
 */
package com.anyoption.common.service.gmwithdrawal;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.Limit;
import com.anyoption.common.beans.User;
import com.anyoption.common.managers.BonusManagerBase;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.LimitsManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.managers.WriterPermisionsManagerBase;
import com.anyoption.common.managers.WritersManagerBase;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawEntryResult;
import com.anyoption.common.payments.WithdrawUtil;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.CommonServiceServlet;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.ErrorMessage;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.PermissionsGetResponseBase;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.MessageToFormat;
import com.anyoption.common.util.Utils;

/**
 * @author pavel.tabakov
 *
 */
public class GmWithdrawalServices {
	
	private static final String SCREEN_NAME = "directBanking";
	private static final String FEE_EXMPT_PERMISION = "directBanking_feeExempt";
	
	private static final Logger logger = Logger.getLogger(GmWithdrawalServices.class);
	
	@BackendPermission(id = "directBanking_view")
	public static PermissionsGetResponseBase getScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		logger.debug("getScreenSettings:" + request);		
		PermissionsGetResponseBase response = new PermissionsGetResponseBase();		
		try {
			response.setPermissionsList(WriterPermisionsManagerBase.getWriterScreenPermissions(SCREEN_NAME, request.getWriterId()));
		} catch (SQLException e) {
			logger.error("Can't getScreenSettings for directBanking" , e);
		}
		return response;
	}
	
	//TODO : refactor - rename the Result class to apply not only for German methods
	@BackendPermission(id = "directBanking_view")
	public static GmWithdrawalPermissionToDisplayResult getPermissionsToDisplay(GmWithdrawalPermissionToDisplayRequest request, HttpServletRequest httpRequest) {//tested working !
		GmWithdrawalPermissionToDisplayResult result = new GmWithdrawalPermissionToDisplayResult();
		try {
			result = TransactionsManagerBase.getGmDisplayPerm(request, result);
			result.setErrorCode(CommonJSONService.ERROR_CODE_SUCCESS);
		} catch (SQLException ex) {
			logger.error("Fail to get german payment methods permissions due to: ", ex);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	//TODO : refactor - rename the method and Result to apply not only for German methods
	@BackendPermission(id = "directBanking_save")
	public static GmWithdrawalValidateResult insertGmWithdraw(GmWithdrawalRequest request, HttpServletRequest httpRequest) {
		GmWithdrawalValidateResult validateResult = validateGMWithdraw(request, httpRequest);
		if (validateResult.getErrorCode() == CommonJSONService.ERROR_CODE_SUCCESS) {
			try {

				User user = UsersManagerBase.getUserById(request.getUserId());
				request.setRate(CurrencyRatesManagerBase.getInvestmentsRate(user.getCurrencyId()));
				request.setTrnStatusId(TransactionsManagerBase.TRANS_STATUS_REQUESTED);
				if (request.isExemptFee()) {					
					//Check Permission for Exempt Fee
					if(WriterPermisionsManagerBase.getWriterScreenPermissions(SCREEN_NAME, request.getWriterId()).contains(FEE_EXMPT_PERMISION)){
						logger.error("Writer:" + request.getWriterId() + " exempt the fee GermanWithd for userId " + request.getUserId());
					} else {
						logger.error("Writer:" + request.getWriterId() + " don't have permission: " + FEE_EXMPT_PERMISION);
						validateResult.setErrorCode(CommonServiceServlet.ERROR_CODE_USER_NOT_HAVE_PERMISSION);
						return validateResult;
					}
				}
				TransactionsManagerBase.insertGmWithdraw(request);
			} catch (SQLException ex) {
				logger.debug("Can not validate withdraw due to: ", ex);
				validateResult.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
		}
		return validateResult;
	}
	
	//TODO : refactor - rename the method and Result to apply not only for German methods
	public static GmWithdrawalValidateResult validateGMWithdraw(GmWithdrawalRequest request, HttpServletRequest httpRequest) {

		GmWithdrawalValidateResult result = new GmWithdrawalValidateResult();
		boolean preWithdrawError = false;
		Map<String, MessageToFormat> mtf = new HashMap<String, MessageToFormat>();
		if (validateRequest(request, result)) {
			try {
				User user = UsersManagerBase.getUserById(request.getUserId());
				Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(user.getSkinId()).getDefaultLanguageId()).getCode());
				user.setLocale(locale);
				
				int regErrorCode = UserRegulationManager.canWithdraw(request.getUserId());
				if (regErrorCode != CommonJSONService.ERROR_CODE_SUCCESS) {
					logger.debug("User with ID: " + request.getUserId() + "is not allowed to withdraw!");
					result.setErrorCode(regErrorCode);
					return result;
				}

				TransactionsManagerBase.getTotalDeposits(request);
				// check total deposit by current GM method
				if (request.getAllDepositsAmount() > 0) {
					request.setConvertedRequestAmount(CommonUtil.calcAmount(request.getAmount()));
				} else {
					result.setErrorCode(CommonJSONService.ERROR_CODE_WITHDRAWAL_AMOUNT_HIGHER_THAN_ALLOWED);
					return result;
				}
				
				boolean checkLimits = WritersManagerBase.writerHasRole(request.getWriterId(), "accounting");
				String error = validateWithdraw(request, user, mtf, checkLimits);

				if (error == null) {
					WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
					if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
						error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
						result.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
						result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), error, null));
						preWithdrawError = true;
					}
				}

				if (!preWithdrawError) {
					// in case of error insert transaction with failed status
					if (!mtf.isEmpty()) {
						request.setTrnStatusId(TransactionsManagerBase.TRANS_STATUS_FAILED);
						request.setTrnFailDesc(error);
						TransactionsManagerBase.insertGmWithdraw(request);
						result.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
						for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
							String key = i.next();
							MessageToFormat m = mtf.get(key);
							if (m.getErrorMsgType() == MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD) {
								result.setErrorCode(CommonJSONService.ERROR_CODE_VALIDATION_WITHOUT_FIELD);
							}
							result.addUserMessage(key, CommonUtil.getMessage(user.getLocale(), m.getKey(), m.getParams()));
						}
					} else {
						// get fee
						long feeVal = TransactionsManagerBase.getTransactionHomoFee(CommonUtil.calcAmount(request.getAmount()), user.getCurrencyId(), request.getGmType());
						result.setFee(feeVal);
					}
				}

			} catch (Exception ex) {
				logger.debug("Can not validate withdraw due to: ", ex);
				result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
				return result;
			}
			if (!preWithdrawError && mtf.isEmpty()) {
				result.setErrorCode(CommonJSONService.ERROR_CODE_SUCCESS);
			}
		} else {
			result.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
		}

		return result;
	}
	

	private static String validateWithdraw(GmWithdrawalRequest request, User user, Map<String, MessageToFormat> res, boolean checkLimits) throws SQLException {
			
			if (request.getConvertedRequestAmount() > user.getBalance()) {
				logger.debug("User with ID: " + user.getId() + " request to withdraw amount higher than available balance!");
				res.put("amount", new MessageToFormat("error.withdraw.highAmount", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));				
				return "error.withdraw.highAmount";
			}	
			
			if ((request.getConvertedRequestAmount() + request.getAllWithdrawalAmount()) > request.getAllDepositsAmount()) {
				logger.debug("User with ID: " + user.getId() + " request to withdraw amount higher than allowed one!");
				res.put("amount", new MessageToFormat("error.transaction.GM.maxamount", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
				return "error.transaction.GM.maxamount";
			}
			
			if (!BonusManagerBase.userCanWithdraw(user.getId(), user.getBalance(), request.getConvertedRequestAmount())) { // check for bonuses in state used
				logger.debug("User with ID: " + user.getId() + "is not allowed to withdraw, because has bonuses in state used!");
                res.put("amount", new MessageToFormat("bonus.cannot.withdraw.error", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                return "bonus.cannot.withdraw.error";
			}
			
			Limit limit = LimitsManagerBase.getLimitById(user.getLimitId());
            if (request.getConvertedRequestAmount() > limit.getMaxBankWire()) {// Why maxBankWire? -->  because flow for GM is quite the same -> check BAC-2470
				logger.debug("User with ID: " + user.getId() + "is trying to withdraw amount higher than maximum allowed!");
                res.put("amount", new MessageToFormat("error.maxbankwire", new String[] {CommonUtil.formatCurrencyAmountAO(limit.getMaxBankWire(), user.getCurrencyId())}, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                return "error.transaction.bankWire.maxamount";
            }
            
            if (request.getConvertedRequestAmount() < limit.getMinWithdraw() && !checkLimits) {
				logger.debug("User with ID: " + user.getId() + "is trying to withdraw amount higher than maximum allowed!");
                res.put("amount", new MessageToFormat("error.minbankwire", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                return "error.transaction.GM.minamount";
            }
		return null;
	}
	
	private static boolean validateRequest(GmWithdrawalRequest request, MethodResult result) {
		ArrayList<ErrorMessage> errorList = new ArrayList<>();
		if (request.getGmType() == TransactionsManagerBase.TRANS_TYPE_EPS_WITHDRAW
				|| request.getGmType() == TransactionsManagerBase.TRANS_TYPE_DIRECT24_WITHDRAW
				|| request.getGmType() == TransactionsManagerBase.TRANS_TYPE_GIROPAY_WITHDRAW
				|| request.getGmType() == TransactionsManagerBase.TRANS_TYPE_IDEAL_WITHDRAW
				|| request.getUserId() > 0) {

			Locale locale = null;
			try {
				locale = new Locale(LanguagesManagerBase.getLanguage(
						SkinsManagerBase.getSkin(UsersManagerBase.getUserById(request.getUserId()).getSkinId())
						.getDefaultLanguageId()).getCode());
			} catch (SQLException e) {
				logger.debug("Failed to get user locale", e);
			}
			if (locale == null) {
				locale = new Locale("en");
			}
			if (request.getGmType() == TransactionsManagerBase.TRANS_TYPE_EPS_WITHDRAW) {
				if (Utils.isParameterEmptyOrNull(request.getBeneficiaryName())
						|| request.getAccountNumber() <= 0
						|| Utils.isParameterEmptyOrNull(request.getAmount())
						|| Utils.isParameterEmptyOrNull(request.getBankName())
						|| Utils.isParameterEmptyOrNull(request.getBranchAddress())
						|| Utils.isParameterEmptyOrNull(request.getIban())
						|| Utils.isParameterEmptyOrNull(request.getSwiftBicCode())
						// TODO request.getAccountInfo
						|| request.getBranchNumber() <= 0) {
					
					ErrorMessage errMsg = null;
					
					if (Utils.isParameterEmptyOrNull(request.getBeneficiaryName())) {
						errMsg = new ErrorMessage("beneficiaryName", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}
					if (request.getAccountNumber() <= 0) {
						errMsg = new ErrorMessage("accountNumber", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}
					if (Utils.isParameterEmptyOrNull(request.getAmount())) {
						errMsg = new ErrorMessage("amount", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}
					if (Utils.isParameterEmptyOrNull(request.getBankName())) {
						errMsg = new ErrorMessage("bankName", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}
					if (Utils.isParameterEmptyOrNull(request.getBranchAddress())) {
						errMsg = new ErrorMessage("branchAddress", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}
					if (Utils.isParameterEmptyOrNull(request.getIban())) {
						errMsg = new ErrorMessage("iban", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}
					if (Utils.isParameterEmptyOrNull(request.getSwiftBicCode())) {
						errMsg = new ErrorMessage("swiftBicCode", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}

					if (request.getBranchNumber() <= 0) {
						errMsg = new ErrorMessage("branchNumber", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}
					
					result.addUserMessages(errorList);
					
					return false;
				}
			} else if (request.getGmType() == TransactionsManagerBase.TRANS_TYPE_GIROPAY_WITHDRAW
					|| request.getGmType() == TransactionsManagerBase.TRANS_TYPE_DIRECT24_WITHDRAW) {
				if (Utils.isParameterEmptyOrNull(request.getBeneficiaryName())
						|| request.getAccountNumber() <= 0
						|| Utils.isParameterEmptyOrNull(request.getAmount())
						|| Utils.isParameterEmptyOrNull(request.getBankName())
						|| Utils.isParameterEmptyOrNull(request.getBranchAddress())
						|| request.getBranchNumber() <= 0) {
					
					ErrorMessage errMsg = null;
					
					if (Utils.isParameterEmptyOrNull(request.getBeneficiaryName())) {
						errMsg = new ErrorMessage("beneficiaryName", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}
					if (request.getAccountNumber() <= 0) {
						errMsg = new ErrorMessage("accountNumber", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}
					if (Utils.isParameterEmptyOrNull(request.getAmount())) {
						errMsg = new ErrorMessage("amount", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}
					if (Utils.isParameterEmptyOrNull(request.getBankName())) {
						errMsg = new ErrorMessage("bankName", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}
					if (Utils.isParameterEmptyOrNull(request.getBranchAddress())) {
						errMsg = new ErrorMessage("branchAddress", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}
					if (request.getBranchNumber() <= 0) {
						errMsg = new ErrorMessage("branchNumber", CommonUtil.getMessage(locale, "error.mandatory", null));
						errorList.add(errMsg);
					}
					
					result.addUserMessages(errorList);
					return false;
				}
			} else if (request.getGmType() == TransactionsManagerBase.TRANS_TYPE_IDEAL_WITHDRAW) {
				if (Utils.isParameterEmptyOrNull(request.getBeneficiaryName())
						|| Utils.isParameterEmptyOrNull(request.getAmount())
						|| Utils.isParameterEmptyOrNull(request.getIban())
						|| Utils.isParameterEmptyOrNull(request.getSwiftBicCode()) ) {
					
					ErrorMessage errMsg = null;
					
					if (Utils.isParameterEmptyOrNull(request.getBeneficiaryName())) {
							errMsg = new ErrorMessage("beneficiaryName", CommonUtil.getMessage(locale, "error.mandatory", null));
							errorList.add(errMsg);
						}
						if (Utils.isParameterEmptyOrNull(request.getAmount())) {
							errMsg = new ErrorMessage("amount", CommonUtil.getMessage(locale, "error.mandatory", null));
							errorList.add(errMsg);
						}
						if (Utils.isParameterEmptyOrNull(request.getIban())) {
							errMsg = new ErrorMessage("iban", CommonUtil.getMessage(locale, "error.mandatory", null));
							errorList.add(errMsg);
						}
						if (Utils.isParameterEmptyOrNull(request.getSwiftBicCode())) {
							errMsg = new ErrorMessage("swiftBicCode", CommonUtil.getMessage(locale, "error.mandatory", null));
							errorList.add(errMsg);
						}
						
						result.addUserMessages(errorList);
						return false;
				}
			}
		} else {
			if (request.getUserId() <= 0) {
				logger.debug("Wrong userId: " + request.getUserId());
			} else {
				logger.debug("Wrong type: withdraw type is not Giropay, Eps, Direct24 or iDeal");
			}
			return false;
		}
		return true;
	}
}
