/**
 * 
 */
package com.anyoption.common.service.gmwithdrawal;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author pavel.tabakov
 *
 */
public class GmWithdrawalPermissionToDisplayResult extends MethodResult {

	private boolean displayDirect24;
	private boolean displayGiropay;
	private boolean displayEPS;
	private boolean displayIdeal;

	public boolean isDisplayDirect24() {
		return displayDirect24;
	}

	public void setDisplayDirect24(boolean displayDirect24) {
		this.displayDirect24 = displayDirect24;
	}

	public boolean isDisplayGiropay() {
		return displayGiropay;
	}

	public void setDisplayGiropay(boolean displayGiropay) {
		this.displayGiropay = displayGiropay;
	}

	public boolean isDisplayEPS() {
		return displayEPS;
	}

	public void setDisplayEPS(boolean displayEPS) {
		this.displayEPS = displayEPS;
	}
	
    public boolean isDisplayIdeal() {
		return displayIdeal;
	}

	public void setDisplayIdeal(boolean displayIdeal) {
		this.displayIdeal = displayIdeal;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "GmWithdrawalPermissionToDisplayResult: "
            + super.toString()
            + "displayDirect24: " + displayDirect24 + ls
            + "displayGiropay: " + displayGiropay + ls
            + "displayEPS: " + displayEPS + ls
            + "displayIdeal: " + displayIdeal + ls;
    }
}
