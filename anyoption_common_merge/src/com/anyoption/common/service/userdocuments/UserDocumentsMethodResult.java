package com.anyoption.common.service.userdocuments;

import java.util.ArrayList;
import java.util.Map;

import com.anyoption.common.beans.base.File;
import com.anyoption.common.service.results.UserMethodResult;

public class UserDocumentsMethodResult extends UserMethodResult {

	private ArrayList<File> userDocuments;


	public ArrayList<File> getUserDocuments() {
		return userDocuments;
	}

	public void setUserDocuments(ArrayList<File> userDocuments) {
		this.userDocuments = userDocuments;
	}
	
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "UserDocumentsMethodResult: "
            + super.toString()
           + "userDocuments: " + userDocuments + ls;
    }
}