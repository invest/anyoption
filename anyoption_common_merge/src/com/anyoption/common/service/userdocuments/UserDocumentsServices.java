package com.anyoption.common.service.userdocuments;

import org.apache.log4j.Logger;

import com.anyoption.common.service.CommonJSONService;

public class UserDocumentsServices {
	private static final Logger log = Logger.getLogger(UserDocumentsServices.class);	
		
	public static UserDocumentsMethodResult getUserDocuments (UserDocumentsMethodRequest request) {		
		log.debug("getUserDocuments:" + request);
		UserDocumentsMethodResult result = new UserDocumentsMethodResult();		
		try {
			result.setUserDocuments(UserDocumentsManager.getUserDocuments(request.getUserId()));
		} catch (Exception e) {
			log.error("When getUserDocument", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}		
		return result;
	}
}