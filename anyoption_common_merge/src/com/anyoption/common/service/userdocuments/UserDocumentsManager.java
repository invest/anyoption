package com.anyoption.common.service.userdocuments;


import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;
import org.jfree.util.Log;

import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.File;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.daos.FilesDAOBase;
import com.anyoption.common.daos.UsersDAOBase;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.FilesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

public class UserDocumentsManager extends BaseBLManager {

	private static final Logger log = Logger.getLogger(UserDocumentsManager.class);	
	private static ArrayList<Long> fileTypeLits;
	private static ArrayList<Long> fileTypeLitsGB;
	private static HashMap<Integer, ArrayList<Long>> popDocsNeededByPayment;
	
	public static ArrayList<File> getUserDocuments(long userId) throws SQLException {
		Connection conn = null;
		ArrayList<File> fileList = new ArrayList<File>();		
		ArrayList<Long> fileListType = new ArrayList<Long>();		 
		try {
			conn = getConnection();
			User user = UsersDAOBase.getUser(conn, userId);		
			fileList = UserDocumentsDAO.getUserDocuments(conn, userId);
			for (File file : fileList) {
				file.setFileNameForClient(FilesManagerBase.getFileName(file.getFileName()));			
				file.setCcName(getCCName(file, user.getSkinId()));				
				fileListType.add(file.getFileTypeId());
				log.debug("UserDocuments for userId:" + userId + " file:" + file);				
			}
			
			//Check if missing Reg file
			ArrayList<Long> userFileList =  new ArrayList<>();
			if(isUserRegulated(user)){
				userFileList = getRegulationFileTypes();
			}
			if(SkinsManagerBase.getSkin(user.getSkinId()).getBusinessCaseId() == Skin.SKIN_BUSINESS_GOLDBEAM){
				userFileList = getRegulationFileTypesGB();
			}
			for (Long regFile : userFileList) {
				if(!fileListType.contains(regFile)){
					File emptyF = getEmptyRegFile(regFile);
					fileList.add(emptyF);
					log.debug("UserDocuments for userId:" + userId + " added missing file:" + emptyF);
				}
			}			
		} finally {
			closeConnection(conn);
		}		
		return fileList;
	}
	
	private static String getCCName(File f, long skinId){
		String ccNumber = f.getCcName();
		String newCCName = f.getCcName();
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId()).getCode());
		if (!CommonUtil.isParameterEmptyOrNull(ccNumber)) {
			String ccName = CommonUtil.getMessage(locale, f.getCcType(), null);
			try {
				ccNumber = AESUtil.decrypt(f.getCcName());
			} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
						| NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
				Log.error("When get getCCName for file:" + f, ce);
			}
			int l = ccNumber.length();
			if (l > 4) {
				newCCName = ccNumber.substring(l - 4, l) + "  " + ccName;
			}			
		}
		return newCCName;
	}
	
	private static ArrayList<Long> getRegulationFileTypes(){
		if(fileTypeLits == null){
			fileTypeLits = new ArrayList<Long>();
			fileTypeLits.add(FileType.USER_ID_COPY);
			fileTypeLits.add(FileType.PASSPORT);
			fileTypeLits.add(FileType.UTILITY_BILL);
		}		
		return fileTypeLits;
	}
	
	private static ArrayList<Long> getRegulationFileTypesGB(){
		if(fileTypeLitsGB == null){
			fileTypeLitsGB = new ArrayList<Long>();
			fileTypeLitsGB.add(FileType.USER_ID_COPY);
			fileTypeLitsGB.add(FileType.PASSPORT);
		}		
		return fileTypeLitsGB;
	}
	
	private static File getEmptyRegFile(long fileTypeId){
		File f = new File();
		f.setFileTypeId(fileTypeId);
		f.setStatusMsgId(File.STATUS_REQUESTED);
		return f;
	}
	
	private static boolean isUserRegulated(User user){
		 Skin userSkin = SkinsManagerBase.getSkin(user.getSkinId());
		 return userSkin.isRegulated();
	}
	
	public static void insertEmptyFile(long userId, long type) {
		insertEmptyFile(userId, type, 0);		
	}
	
	public static void insertEmptyNoExistFile(long userId, long type, long ccId) throws SQLException {
		if(!isExistFileType(userId, type, ccId)){
			insertEmptyFile(userId, type, ccId);
		}		
	}
	
	public static void insertEmptyFile(long userId, long type, long ccId) {
		Connection con = null;
		try {
			con = getConnection();
			FilesDAOBase.insertFile(con, null, ccId, type, userId, File.STATUS_REQUESTED, ConstantsBase.EMPTY_STRING, Writer.WRITER_ID_AUTO);
			UserDocumentsDAO.setCurrentFlagDocs(con, userId, type, ccId);
		} catch (SQLException e) {
			log.debug("Cannot insert file into file table", e);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void uploadUserDocs(long userId, long typeId, long writerId, String fileName, Long ccId) {
		Connection con = null;
		try {
			con = getConnection();
			UserDocumentsDAO.uploadUserDocs(con, userId, typeId, writerId, fileName, ccId);
		} catch (SQLException e) {
			log.debug("Cannot uploadUserDocs", e);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void setCurrentFlagDocsManual(long fileId) {
		Connection con = null;
		try {
			con = getConnection();
			UserDocumentsDAO.setCurrentFlagDocsManual(con, fileId);
		} catch (SQLException e) {
			log.debug("Cannot setCurrentFlagDocs", e);
		} finally {
			closeConnection(con);
		}
	}
	
	public static HashMap<Long, Long> getUserPOPDocuments(long userId) throws SQLException {
		HashMap<Long, Long> res = new HashMap<>();
		Connection conn = null;		 
		try {
			conn = getConnection();
			res = UserDocumentsDAO.getUserPOPDocuments(conn, userId);			
		} finally {
			closeConnection(conn);
		}		
		return res;
	}

	public static HashMap<Integer, ArrayList<Long>> getPopDocsNeededByPayment() {
		if(popDocsNeededByPayment == null){
			popDocsNeededByPayment = new HashMap<>();
			//Skrill
			ArrayList<Long> skrillDocs = new ArrayList<>();
			skrillDocs.add(FileType.BANKWIRE_CONFIRMATION);
			popDocsNeededByPayment.put(TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_DEPOSIT, skrillDocs);
			//Bank Wire
			ArrayList<Long> bankWire = new ArrayList<>();
			bankWire.add(FileType.PROTOCOL);
			popDocsNeededByPayment.put(TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT, bankWire);
			//Direct24  -172
			ArrayList<Long> direct24 = new ArrayList<>();
			direct24.add(FileType.PROTOCOL);
			popDocsNeededByPayment.put(-172, direct24);
			//GiroPay  -173
			ArrayList<Long> giroPay = new ArrayList<>();
			giroPay.add(FileType.PROTOCOL);
			popDocsNeededByPayment.put(-173, giroPay);
			//EPS  -174
			ArrayList<Long> eps = new ArrayList<>();
			eps.add(FileType.PROTOCOL);
			eps.add(FileType.BANKWIRE_CONFIRMATION);
			popDocsNeededByPayment.put(-174, eps);
			//idealInatec
			ArrayList<Long> idealInatec = new ArrayList<>();
			idealInatec.add(FileType.PROTOCOL);
			idealInatec.add(FileType.BANKWIRE_CONFIRMATION);
			popDocsNeededByPayment.put(TransactionsManagerBase.TRANS_TYPE_INATEC_IFRAME_DEPOSIT, idealInatec);
			//cash
			ArrayList<Long> cash = new ArrayList<>();
			cash.add(FileType.PROTOCOL);
			cash.add(FileType.BANKWIRE_CONFIRMATION);
			popDocsNeededByPayment.put(TransactionsManagerBase.TRANS_TYPE_CASH_DEPOSIT, cash);
		}		
		return popDocsNeededByPayment;
	}
	
	public static ArrayList<Long> getPopDocsNeededByTransaction(Transaction t) {
		int paymentType = 0;
		if(t.isDirect24Deposit()){
			paymentType = -172;
		} else if (t.isGiropayDeposit()) {
			paymentType = -173;
		} else if (t.isEpsDeposit()){
			paymentType = -174;
		} else {
			paymentType = (int) t.getTypeId();
		}
		return getPopDocsNeededByPayment().get(paymentType);
	}
	
	public static boolean isExistFileType(long userId, long fileType, long ccId) throws SQLException {
		boolean res;
		Connection conn = null;		 
		try {
			conn = getConnection();
			res = FilesDAOBase.isExistFileType(conn, userId, fileType, ccId);			
		} finally {
			closeConnection(conn);
		}		
		return res;
	}
}

