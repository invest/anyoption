package com.anyoption.common.service.userdocuments;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.File;
import com.anyoption.common.daos.DAOBase;

import oracle.jdbc.OracleTypes;

public class UserDocumentsDAO extends DAOBase {
	
	private static final Logger log = Logger.getLogger(UserDocumentsDAO.class);	
	
	public static ArrayList<File> getUserDocuments(Connection conn, long userId) throws SQLException {
		ArrayList<File> res = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call PKG_USER_DOCUMENTS.GET_USER_DOCS_BY_ID(o_data => ? " +
																			   " , i_user_id => ? )}");
		
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, userId);			
			cstmt.execute();			
			rs = (ResultSet) cstmt.getObject(1);
			
			while (rs.next()) {
				File f = new File();
				f.setFileName(rs.getString("file_name"));
				f.setFileTypeId(rs.getLong("file_type_id"));
				f.setFileNameForClient(rs.getString("file_name"));
				f.setHideField(rs.getBoolean("hide_file"));
				f.setApproved(rs.getBoolean("is_approved"));
				f.setCcId(rs.getLong("cc_id"));
				f.setCcName(rs.getString("cc_number"));
				f.setCcType(rs.getString("cc_type_name"));
				f.setStatusMsgId(rs.getInt("status"));
				f.setLock(rs.getBoolean("is_approved"));
				res.add(f);				
			}
			rs.close();
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return res;
	}	
	
	public static long uploadUserDocs(Connection conn, long userId, long typeId, long writerId, String fileName, Long ccId) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		long fileId = 0;
		try {
			String sql = "{call PKG_USER_DOCUMENTS.UPLOAD_DOCS( o_file_id => ? " +
															  ",i_user_id => ? " +
															  ",i_file_type_id => ? " +
															  ",i_writer_id => ? " +
															  ",i_file_name => ? " +
															  ",i_cc_id => ?)}";
			cstmt = conn.prepareCall(sql);
	
			cstmt.registerOutParameter(index++, Types.INTEGER);
			cstmt.setLong(index++, userId);
			cstmt.setLong(index++, typeId);
			cstmt.setLong(index++, writerId);
			cstmt.setString(index++, fileName);		
			setStatementValue(ccId, index++, cstmt);
			cstmt.executeQuery();	
			fileId = cstmt.getInt(1);
		} finally {
			closeStatement(cstmt);
		}			
		log.debug("Upload User Docs with fileId[" + fileId + "]");
		return fileId;
	}
	
	public static void setCurrentFlagDocs(Connection conn, long userId, long typeId, Long ccId) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			Long creditCardId = ( null != ccId && ccId == 0) ? null : ccId;
			String sql = "{call PKG_USER_DOCUMENTS.SET_IS_CURRENT_DOCS( i_user_id => ? " +
															  ",i_file_type_id => ? " +
															  ",i_cc_id => ?)}";
			cstmt = conn.prepareCall(sql);
	
			cstmt.setLong(index++, userId);
			cstmt.setLong(index++, typeId);	
			setStatementValue(creditCardId, index++, cstmt);
			cstmt.executeQuery();	
			log.debug("Set current flag for "
					+ " userId[" + userId + "]"
					+ " typeId[" + typeId + "]"
					+ " ccId[" + creditCardId + "]");
		} finally {
			closeStatement(cstmt);
		}			
	}
	
	public static void setCurrentFlagDocsManual(Connection conn, long fileId) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			String sql = "{call PKG_USER_DOCUMENTS.SET_IS_CURRENT_DOCS_MANUAL( i_file_id => ? )}";
			cstmt = conn.prepareCall(sql);	
			cstmt.setLong(index++, fileId);
			cstmt.executeQuery();	
			log.debug("Set current flag for Manual"
					+ " fileId[" + fileId + "]");
		} finally {
			closeStatement(cstmt);
		}			
	}
	
	public static HashMap<Long, Long> getUserPOPDocuments(Connection conn, long userId) throws SQLException {
		HashMap<Long, Long> res = new HashMap<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call PKG_USER_DOCUMENTS.GET_USER_POP_DOCUMENTS(o_data => ? " +
																			      " , i_user_id => ? )}");
		
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, userId);			
			cstmt.execute();			
			rs = (ResultSet) cstmt.getObject(1);
			
			while (rs.next()) {
				res.put(rs.getLong("file_type_id"), rs.getLong("id"));
			}
			rs.close();
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return res;
	}
}