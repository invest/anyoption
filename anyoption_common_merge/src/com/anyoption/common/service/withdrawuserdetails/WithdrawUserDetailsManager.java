package com.anyoption.common.service.withdrawuserdetails;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.WithdrawUserDetails;
import com.anyoption.common.managers.FilesManagerBase;

public class WithdrawUserDetailsManager extends FilesManagerBase {
	private static final Logger log = Logger.getLogger(WithdrawUserDetailsManager.class);	

	public static void insertWithdrawUserDetailas(WithdrawUserDetails wud) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			WithdrawUserDetailsDAO.insertWithdrawUserDetailas(con, wud);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateWithdrawWireUserDetils(WithdrawUserDetails wud) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			WithdrawUserDetailsDAO.updateWithdrawWireUserDetils(con, wud);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateWithdrawSkrillUserDetils(WithdrawUserDetails wud) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			WithdrawUserDetailsDAO.updateWithdrawSkrillUserDetils(con, wud);
		} finally {
			closeConnection(con);
		}
	}

	public static WithdrawUserDetails getWithdrawUserDetails(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return WithdrawUserDetailsDAO.getWithdrawUserDetailas(con, userId);				
		} finally {
			closeConnection(con);
		}
	}
	
	public static void skipedWithdrawBankUserDetils(long userId, long issueActionId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			WithdrawUserDetailsDAO.skipedWithdrawBankUserDetils(con, userId, issueActionId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static boolean showWithdrawUserDetailas(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return WithdrawUserDetailsDAO.showWithdrawUserDetailas(con, userId);
		} finally {
			closeConnection(con);
		}
	}
}