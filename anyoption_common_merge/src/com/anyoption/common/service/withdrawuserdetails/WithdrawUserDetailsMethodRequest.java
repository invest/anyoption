package com.anyoption.common.service.withdrawuserdetails;

import com.anyoption.common.beans.base.WithdrawUserDetails;
import com.anyoption.common.service.requests.MethodRequest;

public class WithdrawUserDetailsMethodRequest extends MethodRequest {
	
	public static int WITHDRAW_USER_DETAILS_WIRE_TYPE = 1;
	public static int WITHDRAW_USER_DETAILS_SKRILL_TYPE = 2;
	
	private WithdrawUserDetails withdrawUserDetails;
	private int withdrawUserDetailsType;

	public WithdrawUserDetails getWithdrawUserDetails() {
		return withdrawUserDetails;
	}

	public void setWithdrawUserDetails(WithdrawUserDetails withdrawUserDetails) {
		this.withdrawUserDetails = withdrawUserDetails;
	}
	
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "WithdrawUserDetails: "
            + super.toString() + ls
            + "withdrawUserDetails: " + withdrawUserDetails + ls;
	}

	public int getWithdrawUserDetailsType() {
		return withdrawUserDetailsType;
	}

	public void setWithdrawUserDetailsType(int withdrawUserDetailsType) {
		this.withdrawUserDetailsType = withdrawUserDetailsType;
	}
}
