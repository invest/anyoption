package com.anyoption.common.service.withdrawuserdetails;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.EmailValidator;
import org.apache.log4j.Logger;

import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.base.WithdrawUserDetails;
import com.anyoption.common.managers.IssuesManagerBase;
import com.anyoption.common.managers.WriterPermisionsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.PermissionsGetResponseBase;
import com.anyoption.common.util.CommonUtil;

public class WithdrawUserDetailsServices {
	private static final String SCREEN_NAME = "withdrawUserDetails";
	private static final Logger log = Logger.getLogger(WithdrawUserDetailsServices.class);	
	
	@BackendPermission(id = "withdrawUserDetails_view")
	public static PermissionsGetResponseBase getScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getScreenSettings:" + request);		
		PermissionsGetResponseBase response = new PermissionsGetResponseBase();		
		try {
			response.setPermissionsList(WriterPermisionsManagerBase.getWriterScreenPermissions(SCREEN_NAME, request.getWriterId()));
		} catch (SQLException e) {
			log.error("Can't getScreenSettings for withdrawUserDetails" , e);
		}
		return response;
	}	
	
	public static WithdrawUserDetailsMethodResult showWithdrawUserDetails(WithdrawUserDetailsMethodRequest request) {		
		log.debug("showWithdrawUserDetails:" + request.getWithdrawUserDetails());		
		WithdrawUserDetailsMethodResult result = new WithdrawUserDetailsMethodResult();
		try {			
			if(WithdrawUserDetailsManager.showWithdrawUserDetailas(request.getWithdrawUserDetails().getUserId())){	
				result.setShowPopup(true);
			} else {
				result.setShowPopup(false);
			}
		} catch (Exception e) {
			log.error("Unable to check for showWithdrawUserDetails", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	@BackendPermission(id = "withdrawUserDetails_view")
	public static WithdrawUserDetailsMethodResult getWithdrawUserDetails(WithdrawUserDetailsMethodRequest request) {

		log.debug("getWithdrawUserDetails:" + request.getWithdrawUserDetails());		
		WithdrawUserDetailsMethodResult result = new WithdrawUserDetailsMethodResult();
		try {
			WithdrawUserDetails wud = WithdrawUserDetailsManager.getWithdrawUserDetails(request.getWithdrawUserDetails().getUserId());
			if(wud != null){	
				result.setWithdrawUserDetails(wud);
			} else {
				log.debug("Can't find WithdrawUserDetails :" + request.getWithdrawUserDetails());
			}
		} catch (Exception e) {
			log.error("Unable to getWithdrawUserDetails", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}

	@BackendPermission(id = "withdrawUserDetails_save")
	public static MethodResult insertWithdrawUserDetailas(WithdrawUserDetailsMethodRequest request) {
		log.debug("insertWithdrawUserDetailas:" + request.getWithdrawUserDetails());		
		
		MethodResult result = new MethodResult();
		try {
			log.debug("Validate WithdrawUserDetailas");
			if(!isValideData(request)){
				result.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
				return result;
			}						
			String skrillEmail = request.getWithdrawUserDetails().getSkrillEmail();
			if(request.getWithdrawUserDetailsType() == WithdrawUserDetailsMethodRequest.WITHDRAW_USER_DETAILS_SKRILL_TYPE
					&& !CommonUtil.isParameterEmptyOrNull(skrillEmail)  && !isValidEmail(skrillEmail)){
				log.debug("Unable to insert  WithdrawUserDetails SkirlEmail is not valide: " + request.getWithdrawUserDetails().getSkrillEmail());
				result.setErrorCode(CommonJSONService.ERROR_CODE_GENERAL_VALIDATION);
				return result;
			}
			
			request.getWithdrawUserDetails().setCreatedWriterId(request.getWriterId());			
			WithdrawUserDetailsManager.insertWithdrawUserDetailas(request.getWithdrawUserDetails());
			log.debug("Inserted WithdrawUserDetails :" + request);
		} catch (Exception e) {
			log.error("Unable to insert WithdrawUserDetails", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}
		
	@BackendPermission(id = "withdrawUserDetails_save")
	public static MethodResult updateWithdrawUserDetils(WithdrawUserDetailsMethodRequest request) {
		log.debug("updateWithdrawUserDetils:" + request);		
		
		MethodResult result = new MethodResult();
		try {
			log.debug("Validate WithdrawUserDetailas");
			if(!isValideData(request)){
				result.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
				return result;
			}
			
			request.getWithdrawUserDetails().setUpdatedWriterId(request.getWriterId());			
			if(request.getWithdrawUserDetailsType() == WithdrawUserDetailsMethodRequest.WITHDRAW_USER_DETAILS_WIRE_TYPE){
				WithdrawUserDetailsManager.updateWithdrawWireUserDetils(request.getWithdrawUserDetails());
			} else if(request.getWithdrawUserDetailsType() == WithdrawUserDetailsMethodRequest.WITHDRAW_USER_DETAILS_SKRILL_TYPE){	
				if(!isValidEmail(request.getWithdrawUserDetails().getSkrillEmail())){
					log.debug("Unable to updateWithdraw  SkirlEmail is not valide: " + request.getWithdrawUserDetails().getSkrillEmail());
					result.setErrorCode(CommonJSONService.ERROR_CODE_GENERAL_VALIDATION);
					return result;
				}
				WithdrawUserDetailsManager.updateWithdrawSkrillUserDetils(request.getWithdrawUserDetails());
			}
			log.debug("updated WithdrawUserDetails :" + request);
		} catch (Exception e) {
			log.error("Unable to updateWithdrawWireUserDetils", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	public static MethodResult skipedWithdrawBankUserDetils(WithdrawUserDetailsMethodRequest request) {
		log.debug("skipedWithdrawBankUserDetils for userId:" + request.getWithdrawUserDetails().getUserId());		
		MethodResult result = new MethodResult();
		try {
			Long issueActionId = IssuesManagerBase.insertSkipedWhtdBankUserDetailsIssueAction(request.getWithdrawUserDetails().getUserId(), request.getWriterId());
			if(issueActionId != null){
				WithdrawUserDetailsManager.skipedWithdrawBankUserDetils(request.getWithdrawUserDetails().getUserId(), issueActionId);
				log.debug("skipedWithdrawBankUserDetils :" + request.getWithdrawUserDetails().getUserId() +  " with issueActionId" + issueActionId);
			} else {
				result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
				log.debug("Can't skipedWithdrawBankUserDetils for userId:" + request.getWithdrawUserDetails().getUserId());
			}
		} catch (Exception e) {
			log.error("Unable to skipedWithdrawBankUserDetils for userId:" + request.getWithdrawUserDetails().getUserId(), e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}	
	
	private static boolean isValideData(WithdrawUserDetailsMethodRequest request) {
		boolean res = true;
		WithdrawUserDetails wth = request.getWithdrawUserDetails();
		if (wth == null) {
			log.debug("WithdrawUserDetails obj. is null in request:" + request);
			res = false;
		}
		if (request.getWithdrawUserDetailsType() == WithdrawUserDetailsMethodRequest.WITHDRAW_USER_DETAILS_WIRE_TYPE) {
			if(wth.getAccountNum() == null || CommonUtil.isParameterEmptyOrNull(wth.getBankName())
					|| CommonUtil.isParameterEmptyOrNull(wth.getBeneficiaryName())
					|| CommonUtil.isParameterEmptyOrNull(wth.getBranchAddress()) || wth.getBranchNumber() == null
					|| wth.getBranchNumber() == null || CommonUtil.isParameterEmptyOrNull(wth.getIban())
					|| CommonUtil.isParameterEmptyOrNull(wth.getSwift()) || wth.getUserId() == null) {
				res = false;
				log.debug("WIRE WithdrawUserDetails is not valide:" + wth);
			}
		} else if (request.getWithdrawUserDetailsType() == WithdrawUserDetailsMethodRequest.WITHDRAW_USER_DETAILS_SKRILL_TYPE) {
			if (CommonUtil.isParameterEmptyOrNull(wth.getSkrillEmail()) || wth.getUserId() == null) {
				res = false;
				log.debug("SKRIL WithdrawUserDetails is not valide:" + wth);
			} 
		}else {
			res = false;
			log.debug("WithdrawUserDetails TYPE is not valide:" + wth);
		}			
		return res;
	}
	
	private static boolean isValidEmail(String email){		
		return EmailValidator.getInstance().isValid(email);		
	}
}