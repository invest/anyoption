package com.anyoption.common.service;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Enumeration;
import java.util.HashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.annotations.PrintLogAnnotations;
import com.anyoption.common.charts.ChartHistoryCache;
import com.anyoption.common.charts.ChartsUpdater;
import com.anyoption.common.charts.LevelHistoryCache;
import com.anyoption.common.jms.BackendLevelsCache;
import com.anyoption.common.jms.LevelsCache;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.OpportunityCache;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Jamal
 */
public class CommonServiceServlet extends HttpServlet {

	private static final long serialVersionUID = -3536866919755220527L;
	private static final Logger log = Logger.getLogger(CommonServiceServlet.class);
	
	public static final int ERROR_CODE_WRITER_IS_NOT_IN_SESSION				= 6999;
	public static final int ERROR_CODE_USER_NOT_HAVE_PERMISSION				= 6998;
	private static final String REQUEST_POINT_TO_COMMON_PKG_FROM_BE = "/BackendService/CommonService/";
	
	private static ChartsUpdater chartsUpdater;
	private static LevelHistoryCache levelHistoryCache;
	private static ChartHistoryCache chartHistoryCache;
	private static OpportunityCache opportunityCache;
	// expiriesCache is removed by duplicated logic in CommonJSONServiceServlet
	// private static PastExpiriesCache expiriesCache;
//	private static String webLevelsCacheJMXName;
	protected static LevelsCache levelsCache;

	@Override
	public void init() {
		log.info("CommonServiceServlet starting...");
		HashMap<String, Object> objectShare = null;
		try {
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			objectShare = (HashMap<String, Object>) envCtx.lookup("anyoption/share");
			log.info("Lookedup object share: " + objectShare);
		} catch (Exception e) {
			log.fatal("Cannot lookup object share", e);
		}

		if (null != objectShare) {
			chartsUpdater = (ChartsUpdater) objectShare.get(ConstantsBase.OBJECT_SHARE_CHARTS_UPDATER);
			log.info("Got from share chartsUpdater: " + chartsUpdater);
		}
		if (null != objectShare && null == chartsUpdater) {
			log.info("Creating new chartsUpdater.");
			chartsUpdater = new ChartsUpdater();
			objectShare.put(ConstantsBase.OBJECT_SHARE_CHARTS_UPDATER, chartsUpdater);
		}

		if (null != objectShare) {
			levelHistoryCache = (LevelHistoryCache) objectShare.get(ConstantsBase.OBJECT_SHARE_LEVELS_HISTORY_CACHE);
			log.info("Got from share levelHistoryCache: " + levelHistoryCache);
		}
		if (null != objectShare && null == levelHistoryCache) {
			log.info("Creating new levelHistoryCache");
			levelHistoryCache = new LevelHistoryCache();
			chartsUpdater.addListener(levelHistoryCache);
			objectShare.put(ConstantsBase.OBJECT_SHARE_LEVELS_HISTORY_CACHE, levelHistoryCache);
		}

		if (null != objectShare) {
			chartHistoryCache = (ChartHistoryCache) objectShare.get(ConstantsBase.OBJECT_SHARE_CHARTS_HISTORY_CACHE);
			log.info("Got from share chartHistoryCache: " + chartHistoryCache);
		}
		if (null != objectShare && null == chartHistoryCache) {
			log.info("Creating new chartHistoryCache");
			chartHistoryCache = new ChartHistoryCache();
			chartsUpdater.addListener(chartHistoryCache);
			objectShare.put(ConstantsBase.OBJECT_SHARE_CHARTS_HISTORY_CACHE, chartHistoryCache);
		}

		opportunityCache = new OpportunityCache();
		// expiriesCache is removed by duplicated logic in CommonJSONServiceServlet
		// expiriesCache = new PastExpiriesCache();

		ServletContext servletContext = getServletContext();
// TODO: Uncomment after we remove the CommonJSONServiceServlet and its subclasses
// Tony: When we do that add another parameter that will specify the type of LevelsCache to instantiate - WebLevelsCache or BackendLevelsCache
// ... and maybe change the names of the params not to be etrader but anyoption
//		try {
//			String initialContextFactory = servletContext.getInitParameter("il.co.etrader.service.level.INITIAL_CONTEXT_FACTORY");
//			String providerURL = servletContext.getInitParameter("il.co.etrader.service.level.PROVIDER_URL");
//			String connectionFactory = servletContext.getInitParameter("il.co.etrader.service.level.CONNECTION_FACTORY_NAME");
//			String topic = servletContext.getInitParameter("il.co.etrader.service.level.TOPIC_NAME");
//			String queue = servletContext.getInitParameter("il.co.etrader.service.level.QUEUE_NAME");
//			String msgPoolSizeStr = servletContext.getInitParameter("il.co.etrader.service.level.MSG_POOL_SIZE");
//			String recoveryPauseStr = servletContext.getInitParameter("il.co.etrader.service.level.RECOVERY_PAUSE");
//			int msgPoolSize = 15;
//			try {
//				msgPoolSize = Integer.parseInt(msgPoolSizeStr);
//			} catch (Throwable t) {
//				log.warn("Can't parse msgPoolSize. Set to 15.", t);
//			}
//			int recoveryPause = 2000;
//			try {
//				recoveryPause = Integer.parseInt(recoveryPauseStr);
//			} catch (Throwable t) {
//				log.warn("Can't parse recoveryPause. Set to 2000.", t);
//			}
//			webLevelsCacheJMXName = servletContext.getInitParameter("il.co.etrader.service.level.JMX_NAME");
//			if (log.isDebugEnabled()) {
//				String ls = System.getProperty("line.separator");
//				log.debug(ls+ "WebLevelsCache Configuration:" + ls + "initialContextFactory: " + initialContextFactory + ls
//							+ "providerURL: " + providerURL + ls + "connectionFactory: " + connectionFactory + ls + "topic: " + topic + ls
//							+ "queue: " + queue + ls + "msgPoolSize: " + msgPoolSize + ls + "recoveryPause: " + recoveryPause + ls
//							+ "jmxName: " + webLevelsCacheJMXName + ls);
//			}
//
			levelsCache = (LevelsCache) servletContext.getAttribute("levelsCache");
//			if (levelsCache == null) {
//				levelsCache = new WebLevelsCache(initialContextFactory, providerURL, connectionFactory, queue, topic, msgPoolSize, recoveryPause, null);
//				levelsCache.registerJMX(webLevelsCacheJMXName);
//				servletContext.setAttribute("levelsCache", levelsCache);
//				log.info("WebLevelsCache bound to the context");
//			}
//		} catch (Exception e) {
//			log.error("Can't start WebLevelsCache.", e);
//		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		String uri = request.getRequestURI();
		String uriWithoutMethod = uri.substring(0, uri.lastIndexOf("/"));
		String methodReq = uri.substring(uri.lastIndexOf("/") + 1);
		
		String classReq = uriWithoutMethod.substring(uriWithoutMethod.lastIndexOf("/") + 1);
		
		if (uriWithoutMethod.contains(REQUEST_POINT_TO_COMMON_PKG_FROM_BE)) {
			classReq = getCommonPackage(classReq) + classReq;
		} else {
			classReq = getPackage(classReq) + classReq;
		}
		
		log.debug("URI requested: " + uri + " Class: " + classReq + " Method: " + methodReq + " sessionId: " + request.getSession().getId());

		if (log.isTraceEnabled()) {
			Enumeration<String> headerNames = request.getHeaderNames();
			String headerName = null;
			String ls = System.getProperty("line.separator");
			StringBuffer sb = new StringBuffer(ls);
			while (headerNames.hasMoreElements()) {
				headerName = headerNames.nextElement();
				sb.append(headerName).append(": ").append(request.getHeader(headerName)).append(ls);
			}
			log.trace(sb.toString());
		}
		
		Object result = null;
		try {
			Gson gson = new GsonBuilder().serializeNulls().create();
			Class<?> serviceClass = Class.forName(classReq);			
			Method[] allMethods = serviceClass.getMethods();
			boolean isFoundMethod = false;
		    for (Method m : allMethods) {
		    	if(m.getName().equals(methodReq)){
		    		isFoundMethod = true;
		    		Parameter[] methodParams = m.getParameters();
					Object[] requestParams = new Object[methodParams.length];
					requestParams[0] = gson.fromJson(new InputStreamReader(request.getInputStream(), "UTF-8"), methodParams[0].getType());
					for (int i = 1; i < methodParams.length; i++) {
						Class<?> paramClass = methodParams[i].getType();
	                    if (paramClass == HttpServletRequest.class) {
	                        requestParams[i] = request;
	                    }else if (paramClass == OpportunityCache.class) {
							requestParams[i] = opportunityCache;
						} else if (paramClass == ChartHistoryCache.class) {
							requestParams[i] = chartHistoryCache;
						} else if (paramClass == LevelHistoryCache.class) {
							requestParams[i] = levelHistoryCache;
						} else if (paramClass == WebLevelsCache.class || paramClass == BackendLevelsCache.class) {
							requestParams[i] = levelsCache;
						} else if (paramClass == HttpServletRequest.class) {
							requestParams[i] = request;
						}/* else if (paramClass == PastExpiriesCache.class) {
							// expiriesCache is removed by duplicated logic in CommonJSONServiceServlet
							requestParams[i] = expiriesCache;
						}*/
					}
					if (requestParams[0] instanceof MethodRequest) {
						((MethodRequest) requestParams[0]).setIp(CommonUtil.getIPAddress(request));
					}
					
					String jsonResponse = "";					
					if(!isWriterInSession(request) && isMethodNeedWriterSessionToBeBind(m)){
						log.error("Writer not in session!");
						jsonResponse = "{\"errorCode\" : \"" + ERROR_CODE_WRITER_IS_NOT_IN_SESSION + "\"}";					
					}else if(!isHavePermission(m, request) && isMethodNeedWriterSessionToBeBind(m)){
						log.error("No permission for method " + m.getName());
						jsonResponse = "{\"errorCode\" : \"" + ERROR_CODE_USER_NOT_HAVE_PERMISSION + "\"}";
					} else {
						result = m.invoke(null, requestParams);
						jsonResponse = gson.toJson(result);
					}				
					
					if (null != jsonResponse && jsonResponse.length() > 0) {
						if(!stopPrintDebugLog(m)){
							log.debug(jsonResponse);
						}
						byte[] data = jsonResponse.getBytes("UTF-8");
						OutputStream os = response.getOutputStream();
						response.setHeader("Content-Type", "application/json");
						response.setHeader("Content-Length", String.valueOf(data.length));
						os.write(data, 0, data.length);
						os.flush();
						os.close();
					}
		    	} 		    	
		    }
		    
	    	if(!isFoundMethod) {
	    		log.error("Can't find method:" + methodReq + " in Class:" + classReq);
	    	}
		    
		} catch (Exception e) {
			log.error("Problem executing " + methodReq + " Method! ", e);
		}
	}
	
	private boolean isMethodNeedWriterSessionToBeBind(Method m) {
		boolean flag = true;
		if (m.getName().equals("resetPassword")) {
			flag = false;
		}
		return flag;
	}

	@Override
	public void destroy() {
		levelHistoryCache = null;
		chartHistoryCache = null;
// TODO: Uncomment after we remove the CommonJSONServiceServlet and its subclasses
//		webLevelsCacheJMXName = null;
//		try {
//			levelsCache.close();
//			levelsCache.unregisterJMX();
//			if (log.isInfoEnabled()) {
//				log.info(levelsCache.getClass().getName() + " unbound from JNDI");
//			}
			levelsCache = null;
//		} catch (Exception e) {
//			log.error("Failed to unbind WebLevelsCache from JNDI", e);
//		}
		opportunityCache.stopOpportunityCacheCleaner();
		// expiriesCache is removed by duplicated logic in CommonJSONServiceServlet
		// expiriesCache.stopPastExpiriesCache();
	}
	
	protected String getPackage(String className){
		String pkg = "com.anyoption.common.service." + className.substring(0, className.indexOf("Service")) + ".";
		return pkg.toLowerCase();
	}
	
	private String getCommonPackage(String className){
		String pkg = "com.anyoption.common.service." + className.substring(0, className.indexOf("Service")) + ".";
		return pkg.toLowerCase();
	}
	
	protected boolean isHavePermission(Method method, HttpServletRequest request) {
		return true;		
	}
	
	protected boolean isWriterInSession(HttpServletRequest request) {
		return true;		
	}
	
	private boolean stopPrintDebugLog(Method method) {
		boolean res = false;
		PrintLogAnnotations annotation = method.getAnnotation(PrintLogAnnotations.class);
		if (annotation != null) {			
			res = annotation.stopPrintDebugLog();
		}
		return res;
	}
}