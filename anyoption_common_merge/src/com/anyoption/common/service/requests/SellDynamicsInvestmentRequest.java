package com.anyoption.common.service.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * contains box number, marketId, opportunityId and opportunityTypeId.
 *
 */
public class SellDynamicsInvestmentRequest extends UserMethodRequest {

	protected long investmentId;
	protected long investmentTypeId; // up/down
	protected long opportunityId;
	protected long opportunityTypeId;
	protected double fullAmount;
	protected double sellAmount;
	protected double offer;
	protected double bid;
	
	public double getFullAmount() {
		return fullAmount;
	}

	public void setFullAmount(double fullAmount) {
		this.fullAmount = fullAmount;
	}

	public double getSellAmount() {
		return sellAmount;
	}

	public void setSellAmount(double sellAmount) {
		this.sellAmount = sellAmount;
	}

	public long getInvestmentId() {
		return investmentId;
	}

	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}

	/**
	 * @return opportunityId.
	 */
	public long getOpportunityId() {
		return opportunityId;
	}

	/**
	 * @param opportunityId
	 */
	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}

	/**
	 * @return opportunityTypeId
	 */
	public long getOpportunityTypeId() {
		return opportunityTypeId;
	}

	/**
	 * @param opportunityTypeId
	 */
	public void setOpportunityTypeId(long opportunityTypeId) {
		this.opportunityTypeId = opportunityTypeId;
	}

	public long getInvestmentTypeId() {
		return investmentTypeId;
	}

	public void setInvestmentTypeId(long investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}

	public double getOffer() {
		return offer;
	}

	public void setOffer(double offer) {
		this.offer = offer;
	}

	public double getBid() {
		return bid;
	}

	public void setBid(double bid) {
		this.bid = bid;
	}

	@Override
	public String toString() {
		return "SellDynamicsInvestmentRequest [investmentId=" + investmentId + ", investmentTypeId=" + investmentTypeId
				+ ", opportunityId=" + opportunityId + ", opportunityTypeId=" + opportunityTypeId + ", fullAmount="
				+ fullAmount + ", sellAmount=" + sellAmount + ", offer=" + offer + ", bid=" + bid + "]";
	}

}