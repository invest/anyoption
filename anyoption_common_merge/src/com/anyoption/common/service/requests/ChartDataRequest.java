package com.anyoption.common.service.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * contains box number, marketId, opportunityId and opportunityTypeId.
 *
 */
public class ChartDataRequest extends UserMethodRequest {
	protected int box;
	protected long marketId;
	protected long opportunityId;
	protected long opportunityTypeId;

	/**
	 * @return box number.
	 */
	public int getBox() {
		return box;
	}

	/**
	 * @param box
	 */
	public void setBox(int box) {
		this.box = box;
	}

	/**
	 * @return marketId.
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	/**
	 * @return opportunityId.
	 */
	public long getOpportunityId() {
		return opportunityId;
	}

	/**
	 * @param opportunityId
	 */
	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}

	/**
	 * @return opportunityTypeId
	 */
	public long getOpportunityTypeId() {
		return opportunityTypeId;
	}

	/**
	 * @param opportunityTypeId
	 */
	public void setOpportunityTypeId(long opportunityTypeId) {
		this.opportunityTypeId = opportunityTypeId;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "ChartDataMethodRequest: " + super.toString() + "box: "
				+ box + ls + "marketId: " + marketId + ls + "opportunityId: "
				+ opportunityId + ls + "opportunityTypeId: "
				+ opportunityTypeId + ls;
	}
}