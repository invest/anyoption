package com.anyoption.common.service.requests;

/**
 * @author kiril.mutafchiev
 */
public class CancelInvestmentCheckboxStateMethodRequest extends UserMethodRequest {

	private boolean showCancelInvestment;

	public boolean isShowCancelInvestment() {
		return showCancelInvestment;
	}

	public void setShowCancelInvestment(boolean showCancelInvestment) {
		this.showCancelInvestment = showCancelInvestment;
	}

	@Override
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "CancelInvestmentCheckboxStateMethodRequest: "
            + super.toString()
            + "showCancelInvestment: " + showCancelInvestment + ls;
	}
}