package com.anyoption.common.service.requests;


public class TermsFileMethodRequest extends UserMethodRequest {
	
	public static final int AGREEMENT_TERMS = 1;
	public static final int GENERAL_TERMS = 2;
	public static final int RISK_DISCLOSURS_TERMS = 3;

	private long platformIdFilter;
	private long skinIdFilter;
	private long partIdFilter;
	private String html;
	private int termsType;
	private String title;
	private long currencyId;
	private boolean isActive;
	
	public long getPlatformIdFilter() {
		return platformIdFilter;
	}
	public void setPlatformIdFilter(long platformIdFilter) {
		this.platformIdFilter = platformIdFilter;
	}
	public long getSkinIdFilter() {
		return skinIdFilter;
	}
	public void setSkinIdFilter(long skinIdFilter) {
		this.skinIdFilter = skinIdFilter;
	}
	public long getPartIdFilter() {
		return partIdFilter;
	}
	public void setPartIdFilter(long partIdFilter) {
		this.partIdFilter = partIdFilter;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public int getTermsType() {
		return termsType;
	}
	public void setTermsType(int termsType) {
		this.termsType = termsType;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
}