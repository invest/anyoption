package com.anyoption.common.service.requests;

import com.anyoption.common.beans.base.Register;

/**
 *
 * @author liors
 *
 */
public class InsertUserMethodRequest extends MethodRequest {

	private Register register;
	private String clientId;
	private boolean isBaidu;

	//Marketing Tracking
	private String marketingStaticPart;
	private String mId;
	private String etsMId;
	private String utmSource;
	
	private boolean sendMail;
	private boolean sendSms;
	private long fingerPrint;

	public boolean isBaidu() {
		return isBaidu;
	}

	public void setBaidu(boolean isBaidu) {
		this.isBaidu = isBaidu;
	}

	/**
	 * @return the clientId
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	/**
	 * @return the register
	 */
	public Register getRegister() {
		return register;
	}

	/**
	 * @param register the register to set
	 */
	public void setRegister(Register register) {
		this.register = register;
	}

    /**
     * @return the mId
     */
    public String getmId() {
        return mId;
    }

    /**
     * @param mId the mId to set
     */
    public void setmId(String mId) {
        this.mId = mId;
    }

    /**
     * @return the marketingStaticPart
     */
    public String getMarketingStaticPart() {
        return marketingStaticPart;
    }

    /**
     * @param marketingStaticPart the marketingStaticPart to set
     */
    public void setMarketingStaticPart(String marketingStaticPart) {
        this.marketingStaticPart = marketingStaticPart;
    }

    /**
     * @return the sendMail
     */
    public boolean isSendMail() {
        return sendMail;
    }

    /**
     * @param sendMail the sendMail to set
     */
    public void setSendMail(boolean sendMail) {
        this.sendMail = sendMail;
    }

    /**
     * @return the sendSms
     */
    public boolean isSendSms() {
        return sendSms;
    }

    /**
     * @param sendSms the sendSms to set
     */
    public void setSendSms(boolean sendSms) {
        this.sendSms = sendSms;
    }

    /**
     * @return the etsMId
     */
    public String getEtsMId() {
        return etsMId;
    }

    /**
     * @param etsMId the etsMId to set
     */
    public void setEtsMId(String etsMId) {
        this.etsMId = etsMId;
    }

	public String getUtmSource() {
		return utmSource;
	}

	public void setUtmSource(String utmSource) {
		this.utmSource = utmSource;
	}

	public long getFingerPrint() {
		return fingerPrint;
	}

	public void setFingerPrint(long fingerPrint) {
		this.fingerPrint = fingerPrint;
	}

	public String toString() {
		 String ls = System.getProperty("line.separator");
		 return ls + "com.anyoption.json.requests.InsertUserMethodRequest: "
				 + super.toString()
				 + "register: " + register + ls
				 + "clientId: " + clientId + ls
				 + "isBaidu: " + isBaidu + ls
				 + "marketingStaticPart: " + marketingStaticPart + ls
				 + "mId: " + mId + ls
				 + "etsMId: " + etsMId + ls
				 + "utmSource: " + utmSource + ls
				 + "sendMail: " + sendMail + ls
				 + "sendSms: " + sendSms + ls;
	}
}