package com.anyoption.common.service.requests;

import com.anyoption.common.enums.CopyOpInvTypeEnum;


/**
 *
 * @author KobiM
 *
 */
public class InsertInvestmentMethodRequest extends UserMethodRequest {

	private long opportunityId;
	private double requestAmount;
	private double pageLevel;
	private float pageOddsWin;
	private float pageOddsLose;
	private int choice;
	private boolean isFromGraph;
	private String ipAddress;
	private String apiExternalUserReference;
	private long apiExternalUserId;
	private long defaultAmountValue;
	private CopyOpInvTypeEnum copyopTypeId;
	private long copyopInvId;
	private double price; // offer or bid for dynamics
	private boolean dev2Second;	

	/**
	 * @return the choice
	 */
	public int getChoice() {
		return choice;
	}

	/**
	 * @param choice the choice to set
	 */
	public void setChoice(int choice) {
		this.choice = choice;
	}

	/**
	 * @return the opportunityId
	 */
	public long getOpportunityId() {
		return opportunityId;
	}

	/**
	 * @param opportunityId the opportunityId to set
	 */
	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}

	/**
	 * @return the pageLevel
	 */
	public double getPageLevel() {
		return pageLevel;
	}

	/**
	 * @param pageLevel the pageLevel to set
	 */
	public void setPageLevel(double pageLevel) {
		this.pageLevel = pageLevel;
	}

	/**
	 * @return the pageOddsLose
	 */
	public float getPageOddsLose() {
		return pageOddsLose;
	}

	/**
	 * @param pageOddsLose the pageOddsLose to set
	 */
	public void setPageOddsLose(float pageOddsLose) {
		this.pageOddsLose = pageOddsLose;
	}

	/**
	 * @return the pageOddsWin
	 */
	public float getPageOddsWin() {
		return pageOddsWin;
	}

	/**
	 * @param pageOddsWin the pageOddsWin to set
	 */
	public void setPageOddsWin(float pageOddsWin) {
		this.pageOddsWin = pageOddsWin;
	}

	/**
	 * @return the requestAmount
	 */
	public double getRequestAmount() {
		return requestAmount;
	}

	/**
	 * @param requestAmount the requestAmount to set
	 */
	public void setRequestAmount(double requestAmount) {
		this.requestAmount = requestAmount;
	}

	/**
	 * @return the isFromGraph
	 */
	public boolean isFromGraph() {
		return isFromGraph;
	}

	/**
	 * @param isFromGraph the isFromGraph to set
	 */
	public void setFromGraph(boolean isFromGraph) {
		this.isFromGraph = isFromGraph;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the apiExternalUserReference
	 */
	public String getApiExternalUserReference() {
		return apiExternalUserReference;
	}

	/**
	 * @param apiExternalUserReference the apiExternalUserReference to set
	 */
	public void setApiExternalUserReference(String apiExternalUserReference) {
		this.apiExternalUserReference = apiExternalUserReference;
	}

	/**
	 * @return the apiExternalUserId
	 */
	public long getApiExternalUserId() {
		return apiExternalUserId;
	}

	/**
	 * @param apiExternalUserId the apiExternalUserId to set
	 */
	public void setApiExternalUserId(long apiExternalUserId) {
		this.apiExternalUserId = apiExternalUserId;
	}

	/**
	 * @return the defaultAmountValue
	 */
	public long getDefaultAmountValue() {
		return defaultAmountValue;
	}

	/**
	 * @param defaultAmountValue the defaultAmountValue to set
	 */
	public void setDefaultAmountValue(long defaultAmountValue) {
		this.defaultAmountValue = defaultAmountValue;
	}

	public CopyOpInvTypeEnum getCopyopTypeId() {
		return copyopTypeId;
	}

	public void setCopyopTypeId(CopyOpInvTypeEnum copyopTypeId) {
		this.copyopTypeId = copyopTypeId;
	}

	/**
	 * @return the copyopInvId
	 */
	public long getCopyopInvId() {
		return copyopInvId;
	}

	/**
	 * @param copyopInvId the copyopInvId to set
	 */
	public void setCopyopInvId(long copyopInvId) {
		this.copyopInvId = copyopInvId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean isDev2Second() {
		return dev2Second;
	}

	public void setDev2Second(boolean dev2Second) {
		this.dev2Second = dev2Second;
	}

	@Override
	public String toString() {
		return "InsertInvestmentMethodRequest [opportunityId=" + opportunityId + ", requestAmount=" + requestAmount
				+ ", pageLevel=" + pageLevel + ", pageOddsWin=" + pageOddsWin + ", pageOddsLose=" + pageOddsLose
				+ ", choice=" + choice + ", isFromGraph=" + isFromGraph + ", ipAddress=" + ipAddress
				+ ", apiExternalUserReference=" + apiExternalUserReference + ", apiExternalUserId=" + apiExternalUserId
				+ ", defaultAmountValue=" + defaultAmountValue + ", copyopTypeId=" + copyopTypeId + ", copyopInvId="
				+ copyopInvId + ", price=" + price + ", dev2Second=" + dev2Second + "]";
	}


}