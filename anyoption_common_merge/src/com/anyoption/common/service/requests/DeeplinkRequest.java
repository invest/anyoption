/**
 * 
 */
package com.anyoption.common.service.requests;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class DeeplinkRequest extends MethodRequest {
	
	private String dpn;

	public String getDpn() {
		return dpn;
	}

	public void setDpn(String dpn) {
		this.dpn = dpn;
	}
	
	
	public String toString() {
		return "DeeplinkRequest [dpn=" + dpn + "]";
	}
}
