package com.anyoption.common.service.requests;

/**
 * @author kirilim
 */
public class WithdrawBonusRegulationStateMethodRequest extends UserMethodRequest {

	private int transactionTypeId;
	private long ccId;

	public int getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(int transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	public long getCcId() {
		return ccId;
	}

	public void setCcId(long ccId) {
		this.ccId = ccId;
	}

    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "WithdrawBonusRegulationStateMethodRequest: "
            + super.toString()
            + "transactionTypeId: " + transactionTypeId + ls
            + "ccId: " + ccId + ls;
    }
}