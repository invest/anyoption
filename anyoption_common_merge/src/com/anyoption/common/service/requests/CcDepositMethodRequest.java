package com.anyoption.common.service.requests;

/**
 * @author EranL
 */
public class CcDepositMethodRequest extends UserMethodRequest {
	private String amount;
	private long cardId;
	private String error;
	private boolean firstNewCardChanged;
	private String ccPass;
	private Long userAnswerId;
	private String textAnswer;
	
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	/**
	 * @return the cardId
	 */
	public long getCardId() {
		return cardId;
	}
	/**
	 * @param cardId the cardId to set
	 */
	public void setCardId(long cardId) {
		this.cardId = cardId;
	}
	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}
	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}
	/**
	 * @return the firstNewCardChanged
	 */
	public boolean isFirstNewCardChanged() {
		return firstNewCardChanged;
	}
	/**
	 * @param firstNewCardChanged the firstNewCardChanged to set
	 */
	public void setFirstNewCardChanged(boolean firstNewCardChanged) {
		this.firstNewCardChanged = firstNewCardChanged;
	}
	
    @Override
	public String toString() {
		return "CcDepositMethodRequest [amount=" + amount + ", cardId="
				+ cardId + ", error=" + error + ", firstNewCardChanged="
				+ firstNewCardChanged + ", ccPass= xxxxx"
				+ ", userAnswerId=" + userAnswerId + ", textAnswer=" + textAnswer + "]";
	}
    
	public String getCcPass() {
		return ccPass;
	}
	
	public void setCcPass(String ccPass) {
		this.ccPass = ccPass;
	}
	
	public Long getUserAnswerId() {
		return userAnswerId;
	}
	
	public void setUserAnswerId(Long userAnswerId) {
		this.userAnswerId = userAnswerId;
	}
	
	public String getTextAnswer() {
		return textAnswer;
	}
	
	public void setTextAnswer(String textAnswer) {
		this.textAnswer = textAnswer;
	}
	
}