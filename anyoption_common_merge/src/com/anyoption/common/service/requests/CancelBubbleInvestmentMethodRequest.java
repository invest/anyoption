package com.anyoption.common.service.requests;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author kirilim
 */
public class CancelBubbleInvestmentMethodRequest extends MethodRequest {

	private long userId;
	private long investmentId;
	private String s;
	private String token;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getInvestmentId() {
		return investmentId;
	}

	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}
	

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		 return ls + "CancelBubbleInvestmentMethodRequest: " + ls
				 + super.toString() + ls
				 + "userId: " + userId + ls
				 + "investmentId: " + investmentId + ls
				 + "s: " + s + ls
				 + "token: " + token;
	}
}