package com.anyoption.common.service.requests;


public class InstantSpreadChangeMethodRequest extends MethodRequest {
    
    private long oppID;
    private long marketID;
    private long writerID;
    private double currentSpreadAO;
    private boolean sign;		//negative or positive sign
    
    public long getOppID() {
        return oppID;
    }
    public void setOppID(long oppID) {
        this.oppID = oppID;
    }
    public long getMarketID() {
        return marketID;
    }
    public void setMarketID(long marketID) {
        this.marketID = marketID;
    }
    public long getWriterID() {
        return writerID;
    }
    public void setWriterID(long writerID) {
        this.writerID = writerID;
    }
    public double getCurrentSpreadAO() {
        return currentSpreadAO;
    }
    public void setCurrentSpreadAO(double currentSpreadAO) {
        this.currentSpreadAO = currentSpreadAO;
    }
    public boolean isSign() {
        return sign;
    }
    public void setSign(boolean sign) {
        this.sign = sign;
    }
}