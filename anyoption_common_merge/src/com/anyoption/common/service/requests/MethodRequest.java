package com.anyoption.common.service.requests;

import com.anyoption.common.beans.base.ApiUser;

/**
 * contains skin id, ip, utcOffset, and device unique id of the request.
 *
 * @author liors
 *
 */
public class MethodRequest {
    protected long skinId;
    protected String ip;
    protected int utcOffset;
    protected String deviceId;
    protected long writerId;
    protected ApiUser apiUser; 
    protected String locale;
    protected String osVersion;
    protected String deviceType;
    protected String appVersion;
    protected boolean isTablet;

    /**
     *
     * @return ip
     */
    public String getIp() {
		return ip;
	}

    /**
     * set ip.
     * @param ip
     */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 *
	 */
	public MethodRequest() {
    }

    public MethodRequest(long skinId) {
    	this.skinId = skinId;
    }

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the utcOffset
	 */
	public int getUtcOffset() {
		return utcOffset;
	}

	/**
	 * @param utcOffset the utcOffset to set
	 */
	public void setUtcOffset(int utcOffset) {
		this.utcOffset = utcOffset;
	}

    public String getDeviceId() {
        return deviceId;
    }
    /**
     *
     * @param deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	
	/**
	 * @return the apiUser
	 */
	public ApiUser getApiUser() {
		return apiUser;
	}

	/**
	 * @param apiUser the apiUser to set
	 */
	public void setApiUser(ApiUser apiUser) {
		this.apiUser = apiUser;
	}
	

	/**
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * @return the osVersion
	 */
	public String getOsVersion() {
		return osVersion;
	}

	/**
	 * @param osVersion the osVersion to set
	 */
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}

	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	/**
	 * @return the appVersion
	 */
	public String getAppVersion() {
		return appVersion;
	}

	/**
	 * @param appVersion the appVersion to set
	 */
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	/**
	 * @return the isTablet
	 */
	public boolean isTablet() {
		return isTablet;
	}

	/**
	 * @param isTablet the isTablet to set
	 */
	public void setTablet(boolean isTablet) {
		this.isTablet = isTablet;
	}

	public String toString() {
	    String ls = System.getProperty("line.separator");
	    return ls + "MethodRequest: " + ls
	    	+ super.toString() + ls
	        + "skinId: " + skinId + ls
	        + "ip: " + ip + ls
	        + "utcOffset: " + utcOffset + ls
            + "deviceId: " + deviceId + ls
            + "writerId: " + writerId + ls
            + "apiUser: " + apiUser + ls 
		    + "locale: " + locale + ls
		    + "osVersion: " + osVersion + ls
		    + "deviceType: " + deviceType + ls
		    + "appVersion: " + appVersion + ls
		    + "isTablet: " + isTablet + ls;
	}
}