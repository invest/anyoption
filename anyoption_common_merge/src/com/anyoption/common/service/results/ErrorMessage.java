package com.anyoption.common.service.results;

import com.google.gson.annotations.Expose;

/**
 * 
 * @author liors
 *
 */
public class ErrorMessage {
	@Expose
    protected String field;
	@Expose
    protected String message;
	@Expose
    protected String apiErrorCode;

	public ErrorMessage() {

	}

    public ErrorMessage(String field, String message) {
        this.field = field;
        this.message = message;
    }
    
    public ErrorMessage(String field, String message, String apiErrorCode) {
        this.field = field;
        this.message = message;
        this.apiErrorCode = apiErrorCode;
    }

    /**
     * @return
     */
    public String getField() {
        return field;
    }

    /**
     * @param field
     */
    public void setField(String field) {
        this.field = field;
    }

    /**
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
    
    /**
	 * @return the apiErrorCode
	 */
	public String getApiErrorCode() {
		return apiErrorCode;
	}

	/**
	 * @param apiErrorCode the apiErrorCode to set
	 */
	public void setApiErrorCode(String apiErrorCode) {
		this.apiErrorCode = apiErrorCode;
	}

	/* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "ErrorMessage" + ls
            + "field: " + field + ls 
            + "message: " + message + ls 
        	+ "apiErrorCode: " + apiErrorCode + ls;
    }
}