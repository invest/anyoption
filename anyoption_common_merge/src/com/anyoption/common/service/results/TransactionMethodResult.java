package com.anyoption.common.service.results;

import com.anyoption.common.beans.base.ThreeDParams;


/**
 *
 *	This class contains the transaction information that need to be processed and deliver this information to the necessary services. 
 * 
 * @author liors
 *
 */
public class TransactionMethodResult extends MethodResult {
    protected long transactionId;
    protected double balance;
    protected String formattedAmount;
    protected String fee;
    protected double dollarAmount;
    protected boolean isFirstDeposit;
    private boolean success;
    private double amount;
//    private double lowAmountWithdraw;
    private ThreeDParams threeDParams;
    private String clearingProvider;

	/**
	 * @return fee
	 */
	public String getFee() {
		return fee;
	}

	/**
	 * @param fee
	 */
	public void setFee(String fee) {
		this.fee = fee;
	}

	/**
	 * @return the formattedAmount
	 */
	public String getFormattedAmount() {
		return formattedAmount;
	}

	/**
	 * @param formattedAmount the formattedAmount to set
	 */
	public void setFormattedAmount(String formattedAmount) {
		this.formattedAmount = formattedAmount;
	}

	/**
	 * @return transactionId
	 */
	public long getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId
     */
    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * @return balance
     */
    public double getBalance() {
        return balance;
    }

    /**
     * @param balance
     */
    public void setBalance(double balance) {
        this.balance = balance;
    }

	/**
	 * @return the dollarAmount
	 */
	public double getDollarAmount() {
		return dollarAmount;
	}

	/**
	 * @param dollarAmount the dollarAmount to set
	 */
	public void setDollarAmount(double dollarAmount) {
		this.dollarAmount = dollarAmount;
	}

	/**
	 * @return the isFirstDeposit
	 */
	public boolean isFirstDeposit() {
		return isFirstDeposit;
	}

	/**
	 * @param isFirstDeposit the isFirstDeposit to set
	 */
	public void setFirstDeposit(boolean isFirstDeposit) {
		this.isFirstDeposit = isFirstDeposit;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

//	public double getLowAmountWithdraw() {
//		return lowAmountWithdraw;
//	}
//
//	public void setLowAmountWithdraw(double lowAmountWithdraw) {
//		this.lowAmountWithdraw = lowAmountWithdraw;
//	}

	public ThreeDParams getThreeDParams() {
		return threeDParams;
	}

	public void setThreeDParams(ThreeDParams threeDParams) {
		this.threeDParams = threeDParams;
	}

	public String getClearingProvider() {
		return clearingProvider;
	}

	public void setClearingProvider(String clearingProvider) {
		this.clearingProvider = clearingProvider;
	}

	@Override
	public String toString() {
		return "TransactionMethodResult [transactionId=" + transactionId + ", balance=" + balance + ", formattedAmount="
				+ formattedAmount + ", fee=" + fee + ", dollarAmount=" + dollarAmount + ", isFirstDeposit="
				+ isFirstDeposit + ", success=" + success + ", amount=" + amount + ", threeDParams=" + threeDParams
				+ ", clearingProvider=" + clearingProvider + "]";
	}
	
	
}