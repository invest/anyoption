/**
 * 
 */
package com.anyoption.common.service.results;

import com.anyoption.common.beans.base.PendingUserWithdrawalsDetailsBase;

public class PendingUserWithdrawalsDetailsMethodResult extends MethodResult {
	
	private PendingUserWithdrawalsDetailsBase pendingUserWithdrawalsDetailsBase;

	public PendingUserWithdrawalsDetailsBase getPendingUserWithdrawalsDetailsBase() {
		return pendingUserWithdrawalsDetailsBase;
	}

	public void setPendingUserWithdrawalsDetailsBase(PendingUserWithdrawalsDetailsBase pendingUserWithdrawalsDetailsBase) {
		this.pendingUserWithdrawalsDetailsBase = pendingUserWithdrawalsDetailsBase;
	}
	
}
