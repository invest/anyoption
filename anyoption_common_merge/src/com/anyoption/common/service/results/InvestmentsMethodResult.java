package com.anyoption.common.service.results;

import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.service.results.UserMethodResult;
import com.google.gson.annotations.Expose;

/**
 * 
 * @author liors
 *
 */
public class InvestmentsMethodResult extends UserMethodResult {
	@Expose
	protected Investment[] investments;
	protected String sumAllInvAmount;

	/**
	 * @return list of investments
	 */
	public Investment[] getInvestments() {
		return investments;
	}

	/**
	 * @param investments
	 */
	public void setInvestments(Investment[] investments) {
		this.investments = investments;
	}

	/**
	 * @return the sumAllInvAmount
	 */
	public String getSumAllInvAmount() {
		return sumAllInvAmount;
	}

	/**
	 * @param sumAllInvAmount the sumAllInvAmount to set
	 */
	public void setSumAllInvAmount(String sumAllInvAmount) {
		this.sumAllInvAmount = sumAllInvAmount;
	}

}
