package com.anyoption.common.service.results;

import java.util.ArrayList;
import java.util.HashMap;

public class BubblesMarketsMethodResult extends MethodResult {
	private ArrayList<HashMap<String, String>> markets = new ArrayList<HashMap<String, String>>();

	public ArrayList<HashMap<String, String>> getMarkets() {
		return markets;
	}

	public void setMarkets(ArrayList<HashMap<String, String>> markets) {
		this.markets = markets;
	}
}
