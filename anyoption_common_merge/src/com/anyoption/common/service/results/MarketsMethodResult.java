package com.anyoption.common.service.results;

import com.anyoption.common.beans.base.Market;
import com.anyoption.common.service.results.MethodResult;
import com.google.gson.annotations.Expose;

/**
 * @author Eyal
 *
 */
public class MarketsMethodResult extends MethodResult {
	@Expose
	protected Market[] markets;

	/**
	 * @return the markets
	 */
	public Market[] getMarkets() {
		return markets;
	}

	/**
	 * @param markets the markets to set
	 */
	public void setMarkets(Market[] markets) {
		this.markets = markets;
	}
}
