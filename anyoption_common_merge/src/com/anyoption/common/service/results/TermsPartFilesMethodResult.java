package com.anyoption.common.service.results;

import java.util.ArrayList;

import com.anyoption.common.beans.base.TermsPartFile;

public class TermsPartFilesMethodResult extends MethodResult {
	
	private ArrayList<TermsPartFile> filesList;

	public ArrayList<TermsPartFile> getFilesList() {
		return filesList;
	}

	public void setFilesList(ArrayList<TermsPartFile> filesList) {
		this.filesList = filesList;
	}
}
