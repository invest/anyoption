package com.anyoption.common.service.results;

import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.google.gson.annotations.Expose;

/**
 * 
 * @author liors
 *
 */
public class UserMethodResult extends MethodResult {
	@Expose
    protected User user;
    protected UserRegulationBase userRegulation;
    
    //Marketing Tracking
    protected String mId;
    protected String marketingStaticPart;
    protected String etsMid;
    protected long skinGroupId;
    
    protected long minIvestmentAmount;
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
    
    /**
	 * @return the userRegulation
	 */
	public UserRegulationBase getUserRegulation() {
		return userRegulation;
	}

	/**
	 * @param userRegulation the userRegulation to set
	 */
	public void setUserRegulation(UserRegulationBase userRegulation) {
		this.userRegulation = userRegulation;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "com.anyoption.json.results.UserMethodResult: "
            + super.toString()
            + "user: " + user + ls
            + "userRegulation: " + userRegulation + ls;
    }

    /**
     * @return the marketingStaticPart
     */
    public String getMarketingStaticPart() {
        return marketingStaticPart;
    }

    /**
     * @param marketingStaticPart the marketingStaticPart to set
     */
    public void setMarketingStaticPart(String marketingStaticPart) {
        this.marketingStaticPart = marketingStaticPart;
    }

    /**
     * @return the mId
     */
    public String getmId() {
        return mId;
    }

    /**
     * @param mId the mId to set
     */
    public void setmId(String mId) {
        this.mId = mId;
    }

    /**
     * @return the etsMid
     */
    public String getEtsMid() {
        return etsMid;
    }

    /**
     * @param etsMid the etsMid to set
     */
    public void setEtsMid(String etsMid) {
        this.etsMid = etsMid;
    }
    
    /**
     * @return the businessCaseId
     */
    public long getSkinGroupId() {
        return skinGroupId;
    }

    /**
     * @param the businessCaseId to set
     */
    public void setSkinGroupId(long skinGroupId) {
        this.skinGroupId = skinGroupId;
    }

	public long getMinIvestmentAmount() {
		return minIvestmentAmount;
	}

	public void setMinIvestmentAmount(long minIvestmentAmount) {
		this.minIvestmentAmount = minIvestmentAmount;
	}
}