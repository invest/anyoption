package com.anyoption.common.service.results;

import com.anyoption.common.beans.base.CreditCard;

/**
 * 
 * @author liors
 *
 */
public class CardMethodResult extends MethodResult {
    protected CreditCard card;
    protected String error; // used for transaction error
    
	/**
	 * @return the card
	 */
	public CreditCard getCard() {
		return card;
	}
	
	/**
	 * @param card the card to set
	 */
	public void setCard(CreditCard card) {
		this.card = card;
	}
	
	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}
	
	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}


}