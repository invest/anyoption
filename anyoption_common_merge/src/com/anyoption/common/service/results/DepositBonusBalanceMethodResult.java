/**
 * 
 */
package com.anyoption.common.service.results;

import com.anyoption.common.beans.base.DepositBonusBalanceBase;

public class DepositBonusBalanceMethodResult extends MethodResult {
	
	private DepositBonusBalanceBase depositBonusBalanceBase;

	public DepositBonusBalanceBase getDepositBonusBalanceBase() {
		return depositBonusBalanceBase;
	}

	public void setDepositBonusBalanceBase(DepositBonusBalanceBase depositBonusBalanceBase) {
		this.depositBonusBalanceBase = depositBonusBalanceBase;
	} 
	
}
