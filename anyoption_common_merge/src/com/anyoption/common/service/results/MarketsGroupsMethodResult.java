package com.anyoption.common.service.results;

import com.anyoption.common.beans.base.MarketGroup;
import com.anyoption.common.service.results.MethodResult;

/**
 * 
 * @author liors
 *
 */
public class MarketsGroupsMethodResult extends MethodResult {
    protected MarketGroup[] groups;

	/**
	 * @return the groups
	 */
	public MarketGroup[] getGroups() {
		return groups;
	}

	/**
	 * @param groups the groups to set
	 */
	public void setGroups(MarketGroup[] groups) {
		this.groups = groups;
	}
}