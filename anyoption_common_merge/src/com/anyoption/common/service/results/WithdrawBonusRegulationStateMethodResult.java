package com.anyoption.common.service.results;

/**
 * @author kirilim
 */
public class WithdrawBonusRegulationStateMethodResult extends MethodResult {

	private WithdrawBonusStateEnum state;

	public enum WithdrawBonusStateEnum {
		NO_OPEN_BONUSES, REGULATED_OPEN_BONUSES, NOT_REGULATED_OPEN_BONUSES;
	}

	public WithdrawBonusStateEnum getState() {
		return state;
	}

	public void setState(WithdrawBonusStateEnum state) {
		this.state = state;
	}

    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "WithdrawBonusRegulationStateMethodResult: " + ls
        	+ super.toString() + ls
            + "state: " + state + ls;
    }
}