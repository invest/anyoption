package com.anyoption.common.service.results;

import java.util.ArrayList;



public class VisibleSkinListMethodResult extends MethodResult {
	
	private ArrayList<VisibleSkinPair> visibleSkins;
	private boolean isRegulated;

	public ArrayList<VisibleSkinPair> getVisibleSkins() {
		return visibleSkins;
	}

	public void setVisibleSkins(ArrayList<VisibleSkinPair> visibleSkins) {
		this.visibleSkins = visibleSkins;
	}

	public boolean isRegulated() {
		return isRegulated;
	}

	public void setRegulated(boolean isRegulated) {
		this.isRegulated = isRegulated;
	}
	
	public class VisibleSkinPair {
		  private Long skinId;
		  private String prettyURL;
		  private String prefix;
		  private String skinSupportPhone;
		  private String skinSupportEmail;
		
		  public VisibleSkinPair(Long skinId, String prettyURL, String prefix) {
			super();
			this.skinId = skinId;
			this.prettyURL = prettyURL;
			this.prefix = prefix;
		}

		  public VisibleSkinPair(Long skinId, String prettyURL, String prefix, String skinSupportPhone, String skinSupportEmail) {
			super();
			this.skinId = skinId;
			this.prettyURL = prettyURL;
			this.prefix = prefix;
			this.skinSupportPhone = skinSupportPhone;
			this.skinSupportEmail = skinSupportEmail;
	    }		  
		  
		public Long getSkinId() {
			return skinId;
		}
		public void setSkinId(Long skinId) {
			this.skinId = skinId;
		}
		public String getPrettyURL() {
			return prettyURL;
		}
		public void setPrettyURL(String prettyURL) {
			this.prettyURL = prettyURL;
		}

		public String getPrefix() {
			return prefix;
		}

		public void setPrefix(String prefix) {
			this.prefix = prefix;
		}

		public String getSkinSupportPhone() {
			return skinSupportPhone;
		}

		public void setSkinSupportPhone(String skinSupportPhone) {
			this.skinSupportPhone = skinSupportPhone;
		}

		public String getSkinSupportEmail() {
			return skinSupportEmail;
		}

		public void setSkinSupportEmail(String skinSupportEmail) {
			this.skinSupportEmail = skinSupportEmail;
		}		 
	}
	
}
