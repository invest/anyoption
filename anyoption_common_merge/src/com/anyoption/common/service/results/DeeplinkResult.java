/**
 * 
 */
package com.anyoption.common.service.results;

import com.anyoption.common.beans.base.DeeplinkParamsToUrl;

/**
 * @author pavel.tabakov
 *
 */
public class DeeplinkResult extends MethodResult {

	private DeeplinkParamsToUrl deeplink;

	public DeeplinkParamsToUrl getDeeplink() {
		return deeplink;
	}

	public void setDeeplink(DeeplinkParamsToUrl deeplink) {
		this.deeplink = deeplink;
	}
}
