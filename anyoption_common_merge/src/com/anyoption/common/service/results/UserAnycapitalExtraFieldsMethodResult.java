package com.anyoption.common.service.results;

import com.anyoption.common.beans.base.UserAnycapitalExtraFields;
import com.google.gson.annotations.Expose;

/**
 * @author LioR SoLoMoN
 *
 */
public class UserAnycapitalExtraFieldsMethodResult extends MethodResult {
	@Expose
	private UserAnycapitalExtraFields data;

	/**
	 * @return the user
	 */
	public UserAnycapitalExtraFields getData() {
		return data;
	}

	/**
	 * @param user the user to set
	 */
	public void setData(UserAnycapitalExtraFields data) {
		this.data = data;
	}
}