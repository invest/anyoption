package com.anyoption.common.service.results;

import java.util.LinkedHashMap;

import com.anyoption.common.beans.base.CountryMiniBean;



public class CountriesListMethodResult extends MethodResult {
	
	private LinkedHashMap<Long, CountryMiniBean> countriesMap;
	private Long ipDetectedCountryId;
	private Long skinDefautlCointryId;
	
	public LinkedHashMap<Long, CountryMiniBean> getCountriesMap() {
		return countriesMap;
	}
	public void setCountriesMap(LinkedHashMap<Long, CountryMiniBean> countriesMap) {
		this.countriesMap = countriesMap;
	}
	public Long getIpDetectedCountryId() {
		return ipDetectedCountryId;
	}
	public void setIpDetectedCountryId(Long ipDetectedCountryId) {
		this.ipDetectedCountryId = ipDetectedCountryId;
	}
	public Long getSkinDefautlCointryId() {
		return skinDefautlCointryId;
	}
	public void setSkinDefautlCointryId(Long skinDefautlCointryId) {
		this.skinDefautlCointryId = skinDefautlCointryId;
	}
}
