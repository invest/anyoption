package com.anyoption.common.service.results;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.anyoption.common.beans.base.TermsParts;

public class TermsFiltersMethodResult extends MethodResult {
	
	private Map<Long, Map<Long, ArrayList<TermsParts>>> filters = new HashMap<Long, Map<Long,ArrayList<TermsParts>>>();

	public Map<Long, Map<Long, ArrayList<TermsParts>>> getFilters() {
		return filters;
	}

	public void setFilters(Map<Long, Map<Long, ArrayList<TermsParts>>> filters) {
		this.filters = filters;
	}
}
