package com.anyoption.common.service.assetindex;

import java.util.ArrayList;

import com.anyoption.common.beans.base.AssetIndexFormula;
import com.anyoption.common.beans.base.MarketAssetIndex;
import com.anyoption.common.service.results.MethodResult;

public class AssetIndexMarketsResult extends MethodResult {
	
	protected ArrayList<MarketAssetIndex> marketList;
	protected ArrayList<AssetIndexFormula> assetIndexFormulasBySkin;
	protected String tooltipData;
	protected String tooltipDataUnderTable;
	
	public ArrayList<MarketAssetIndex> getMarketList() {
		return marketList;
	}

	public void setMarketList(ArrayList<MarketAssetIndex> marketList) {
		this.marketList = marketList;
	}

	public ArrayList<AssetIndexFormula> getAssetIndexFormulasBySkin() {
		return assetIndexFormulasBySkin;
	}
	
	public void setAssetIndexFormulasBySkin(ArrayList<AssetIndexFormula> assetIndexFormulasBySkin) {
		this.assetIndexFormulasBySkin = assetIndexFormulasBySkin;
	}
	
	public String getTooltipData() {
		return tooltipData;
	}
	
	public void setTooltipData(String tooltipData) {
		this.tooltipData = tooltipData;
	}
	
	public String getTooltipDataUnderTable() {
		return tooltipDataUnderTable;
	}
	
	public void setTooltipDataUnderTable(String tooltipDataUnderTable) {
		this.tooltipDataUnderTable = tooltipDataUnderTable;
	}
}
