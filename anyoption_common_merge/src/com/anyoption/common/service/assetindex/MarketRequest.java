package com.anyoption.common.service.assetindex;

import com.anyoption.common.service.requests.MethodRequest;

public class MarketRequest extends MethodRequest {
	
	private Long marketId;

	public Long getMarketId() {
		return marketId;
	}

	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}
}
