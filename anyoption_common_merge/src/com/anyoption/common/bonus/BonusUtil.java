package com.anyoption.common.bonus; 
 
import java.sql.Connection; 
import java.sql.SQLException; 
import com.anyoption.common.beans.Skin; 
import com.anyoption.common.beans.base.BonusUsers; 
import com.anyoption.common.managers.BonusManagerBase; 
import com.anyoption.common.managers.TransactionsManagerBase; 
import com.anyoption.common.util.ConstantsBase; 
 
 
/** 
 * @author liors
 *  
 */ 
public class BonusUtil { 
 
   /** 
     * This method cancel bonus to user with the withdraw mechanism if the bonus is in active or in used state. 
     *  
     * @param conn 
     * @param bonusUser 
     * @param stateToUpdate 
     * @param utcOffset 
     * @param writerId 
     * @param skinId 
     * @throws BonusHandlersException 
     */ 
    public static void cancelBonusToUserWithdraw(Connection conn, BonusUsers bonusUser, long stateToUpdate, String utcOffset, long writerId, long skinId, String ip, long loginId) throws BonusHandlersException { 
		try { 
	    	BonusManagerBase.updateBonusUser(conn, bonusUser.getId(), stateToUpdate, bonusUser.getWriterIdCancel()); 
	    	if (bonusUser.getBonusStateId() == ConstantsBase.BONUS_STATE_ACTIVE || bonusUser.getBonusStateId() == ConstantsBase.BONUS_STATE_USED) { 
	    		long amountToDeduct = bonusUser.getBonusAmount(); 
	    		if (skinId != Skin.SKIN_ETRADER) { 
	    			amountToDeduct = bonusUser.getAdjustedAmount(); 
	    		} 
	    		TransactionsManagerBase.insertBonusWithdraw(conn, amountToDeduct, bonusUser.getUserId(), utcOffset, writerId, bonusUser.getId(), ip, loginId); 
	    	} 
		} catch (SQLException e) { 
			throw new BonusHandlersException("Error cancelBonusToUserWithdraw ",e); 
		} 
    }    
} 
