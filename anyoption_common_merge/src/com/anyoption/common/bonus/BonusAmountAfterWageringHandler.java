package com.anyoption.common.bonus;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.InvestmentBonusData;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlersException;
import com.anyoption.common.bonus.BonusUtil;
import com.anyoption.common.daos.BonusDAOBase;
import com.anyoption.common.daos.InvestmentsDAOBase;
import com.anyoption.common.managers.BonusManagerBase;
import com.anyoption.common.util.ConstantsBase;


public class BonusAmountAfterWageringHandler extends BonusHandlerBase {

	/**
	 *  bonusInsert event handler implementation
	 */
	@Override
	public boolean bonusInsert(Connection conn,BonusUsers bu, Bonus b, BonusCurrency bc, long userId, long currencyId, long writerId, long popEntryId, int bonusPopLimitTypeId, boolean isNeedToSendInternalMail) throws BonusHandlersException{
		boolean res = false;
		bu.setBonusStateId(ConstantsBase.BONUS_STATE_PENDING);
		long bonusAmount = 0;

		try {
			if (b.getId() == ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID) {
				bonusAmount = bu.getBonusAmount();
			}
			
			BonusManagerBase.getLimitsForBonus(conn, bu, b, bc, popEntryId,bonusPopLimitTypeId, userId, currencyId, false);

			bu.setSumInvWithdrawal(bc.getBonusAmount() * b.getWageringParameter());

			res = BonusDAOBase.insertBonusUser(conn, bu, b, bc, popEntryId, isNeedToSendInternalMail);
			
			if (b.getId() == ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID){
				bu.setBonusAmount(bonusAmount);
				BonusDAOBase.updateBonusUsers(conn, bu);
			}

		} catch (SQLException e) {
			throw new BonusHandlersException("Error in inserting bonus" , e);
		}

		return res;

	}


	/**
	 *  isActivateBonus event handler implementation
	 */
	@Override
	public boolean isActivateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long amount, long transactionId) throws BonusHandlersException{

    	long depositsSum = bu.getSumDeposits();

    	// Check if requiered deposits sum > 0
    	if (depositsSum > 0){

    		if (bu.getSumDepositsReached() == 0 && amount >= depositsSum){
    			// Add the amount of current deposit to sum deposits reached
        		try{
        			bu.setSumDepositsReached(amount);
        			BonusDAOBase.addBonusUsersSumDeposits(conn, bu.getId(), amount);
//        			TransactionsDAOBase.setTransactionBonuUsersId(conn, transactionId, bu.getId());
        		}catch (SQLException e) {
    				throw new BonusHandlersException("can't addBonusUsersSumDeposits ", e);
    			}
    		}

        	if (bu.getSumDepositsReached() >= depositsSum &&
        			bu.getSumInvQualifyReached() >= bu.getSumInvQualify()){
        		return true;
        	}else{
        		return false;
        	}
    	}

    	return false;
	}

	/**
	 *  activateBonusAfterTransactionSuccess event handler implementation
	 */
	@Override
	public void activateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long transactionId, long userId, long amount, long writerId, long loginId) throws BonusHandlersException{
		activateBonusAndAddToBalance(conn, bu, userId, writerId, transactionId, 0, loginId);
	}

	/**
	 *  touchBonusesAfterInvestmentSuccess event handler implementation
	 */
	@Override
	public long touchBonusesAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft, long opportunityTypeId) throws BonusHandlersException{
		if (opportunityTypeId == Opportunity.TYPE_BUBBLES) { // no touch for bubbles
			return amountLeft;
		}
		try {
			BonusDAOBase.useBonusUsers(conn, bu.getId());
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in touchBonusesAfterInvestmentSuccess ",e);
		}
        return (amountLeft - bu.getBonusAmount());
	}

	/**
	 *  cancelBonusToUser event handler implementation
	 */
	@Override
    public void cancelBonusToUser(Connection conn, BonusUsers bonusUser, long stateToUpdate, String utcOffset, long writerId, long skinId, String ip, long loginId) throws BonusHandlersException {
		BonusUtil.cancelBonusToUserWithdraw(conn, bonusUser, stateToUpdate, utcOffset, writerId, skinId, ip, loginId);
    }

	/**
	 *  setBonusStateDescription event handler implementation
	 */
	@Override
    public void setBonusStateDescription(BonusUsers bonusUser) throws BonusHandlersException{
		// Do Nothing
	}

	/**
	 *  activateBonusAfterInvestmentSuccessByInvAmount event handler implementation
	 */
	@Override
	public boolean activateBonusAfterInvestmentSuccessByInvAmount(Connection conn, BonusUsers bu, long amountLeft, long userId, long investmentId, long writerId, boolean isFree, long loginId, long opportunityTypeId) throws BonusHandlersException{
		if (opportunityTypeId == Opportunity.TYPE_BUBBLES) { // no activate for bubbles
			return false;
		}
		try{
			boolean isAmountLeftUsed = true;

			if (bu.getSumInvQualify() - bu.getSumInvQualifyReached() > amountLeft) {
		        BonusDAOBase.addBonusUserQualifyWagering(conn, bu.getId(), amountLeft);
		        // for this bonus, change state to Active when first invest.
		        if (bu.getBonusId() == ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID) {
		        	if (bu.getBonusStateId() == ConstantsBase.BONUS_STATE_GRANTED) {
		        		activateBonus(conn, bu, userId, writerId, 0, investmentId);
		        	}
		        }
		    } else {
				if (bu.getSumInvQualify() - bu.getSumInvQualifyReached() > 0) {
			    	BonusDAOBase.addBonusUserQualifyWagering(conn, bu.getId(), bu.getSumInvQualify());
		            if (!isFree) { //check if the invest wasnt next invest on us
		            	InvestmentsDAOBase.setInvestmentBonuUsersId(conn, investmentId, bu.getId());
		            }
		            // for this bonus, Add bonus and change state to Done (without wagering).
		            if (bu.getBonusId() == ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID) {
			            addBonusToBalance(conn, bu, bu.getUserId(), Writer.WRITER_ID_WEB, 0, investmentId, loginId);
			            BonusDAOBase.doneBonusUsers(conn, bu.getId(),true, investmentId);
	    			    BonusDAOBase.updateBonusUsersQualify(conn, bu);
		            }
				}else{
					isAmountLeftUsed = false;
				}

				//	check if sum deposit was reached (in case there is one)
	        	if (bu.getSumDeposits() == 0 || bu.getSumDepositsReached() >= bu.getSumDeposits()){
	        		if (bu.getBonusId() == ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID) {
	        			addBonusToBalance(conn, bu, userId, writerId, 0, investmentId, loginId);
	        			BonusDAOBase.doneBonusUsers(conn, bu.getId(),true, investmentId);
	        			BonusDAOBase.updateBonusUsersQualify(conn, bu);
	        		} else {
	        			activateBonusAndAddToBalance(conn, bu, userId, writerId, 0, investmentId, loginId);
	        		}
	        		
	        	}
		    }

			return isAmountLeftUsed;
		}catch (SQLException e) {
			throw new BonusHandlersException("Error in activateBonusAfterInvestmentSuccessByInvAmount ", e);
		}
	}

	/**
	 *  activateBonusAfterInvestmentSuccessByInvCount event handler implementation
	 */
	@Override
    public boolean activateBonusAfterInvestmentSuccessByInvCount(Connection conn, BonusUsers bu,  boolean isInvWasCountForActivation, long investmentId, long opportunityTypeId) throws BonusHandlersException{
		if (opportunityTypeId == Opportunity.TYPE_BUBBLES) { // no activate for bubbles
			return false;
		}
		return isInvWasCountForActivation;
	}

	/**
	 *  handleBonusOnSettleInvestment event handler implementation
	 */
	@Override
	public long handleBonusOnSettleInvestment(Connection conn, InvestmentBonusData investment, boolean isWin) throws BonusHandlersException{
        return 0;
	}

	/**
	 *  getAmountThatUserCantWithdraw event handler implementation
	 */
	@Override
	public long getAmountThatUserCantWithdraw(long win, long lose, long invAmount, BonusUsers bu){
		return 0;
	}
	
	/**
     * Process update wagering for bonuses after inv success through this handler.
     *
     * @param conn - Connection
     * @param bu - bonus user
     * @param amountLeft - the amount that left before current bonus was used
     * @param investmentId
	 * @return amountLeft  - the amount that left after current bonus was used
     */
    public long wageringAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft, long investmentId) throws BonusHandlersException{
    	if (bu.getBonusId() == ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID) {
    		return 0; // we don't have wagering.
    	}
    	return super.wageringAfterInvestmentSuccess(conn, bu, amountLeft, investmentId);
    }
}