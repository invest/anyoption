package com.anyoption.common.bonus;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.InvestmentBonusData;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlersException;
import com.anyoption.common.bonus.BonusUtil;
import com.anyoption.common.daos.BonusDAOBase;
import com.anyoption.common.daos.CurrenciesDAOBase;
import com.anyoption.common.managers.BonusManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.util.ConstantsBase;

public class BonusInstantAmountHandler extends BonusHandlerBase {

	/**
	 *  bonusInsert event handler implementation
	 */
	@Override
	public boolean bonusInsert(Connection conn,BonusUsers bu, Bonus b, BonusCurrency bc, long userId, long currencyId, long writerId, long popEntryId, int bonusPopLimitTypeId, boolean isNeedToSendInternalMail) throws BonusHandlersException{
		boolean res = false;
		bu.setBonusStateId(ConstantsBase.BONUS_STATE_PENDING);

		try {
			BonusManagerBase.getLimitsForBonus(conn, bu, b, bc, popEntryId,bonusPopLimitTypeId, userId, currencyId, false);

			if (b.getId() == Bonus.COPYOP_COINS_CONVERT) {
				bc.setBonusAmount(bu.getBonusAmount());
			}
			bu.setSumInvWithdrawal(bc.getBonusAmount() * b.getWageringParameter());

			res = BonusDAOBase.insertBonusUser(conn, bu, b, bc, popEntryId, isNeedToSendInternalMail);
			
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in inserting bonus" , e);
		}
		return res;
	}
	
	@Override
	public boolean acceptBonus(Connection conn, BonusUsers bu, User user, String ip, long loginId) throws SQLException {
		boolean res = false;

		long stateId = ConstantsBase.BONUS_STATE_DONE;
		//	for bonus deposit amount that have no wagering
		if (bu.getWageringParam() != 0) {
			stateId = ConstantsBase.BONUS_STATE_ACTIVE;
		} 
		
		res = BonusDAOBase.acceptBonusUser(conn, bu.getId(), stateId);
		
		if (res) {
			if (user.getCurrency() == null) {
				user.setCurrency(CurrenciesDAOBase.getById(conn, user.getCurrencyId().intValue()));
			}
			res = TransactionsManagerBase.insertBonusDepositBase(user, bu.getId(), bu.getWriterId(), bu.getBonusAmount(), bu.getComments(), ip, conn, loginId);
		}
		
		return res;
	}

	/**
	 *  isActivateBonus event handler implementation
	 */
	@Override
	public boolean isActivateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long amount, long transactionId) throws BonusHandlersException{

		return false;
	}

	/**
	 *  activateBonusAfterTransactionSuccess event handler implementation
	 */
	@Override
	public void activateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long transactionId, long userId, long amount, long writerId, long loginId) throws BonusHandlersException{
		// Do Nothing
	}

	/**
	 *  touchBonusesAfterInvestmentSuccess event handler implementation
	 */
	@Override
	public long touchBonusesAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft, long opportunityTypeId) throws BonusHandlersException{
		try {
			BonusDAOBase.useBonusUsers(conn, bu.getId());
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in touchBonusesAfterInvestmentSuccess ",e);
		}
        return (amountLeft - bu.getBonusAmount());
	}

	/**
	 *  cancelBonusToUser event handler implementation
	 */
	@Override
    public void cancelBonusToUser(Connection conn, BonusUsers bonusUser, long stateToUpdate, String utcOffset, long writerId, long skinId, String ip, long loginId) throws BonusHandlersException {
		BonusUtil.cancelBonusToUserWithdraw(conn, bonusUser, stateToUpdate, utcOffset, writerId, skinId, ip, loginId);
    }

	/**
	 *  setBonusStateDescription event handler implementation
	 */
	@Override
    public void setBonusStateDescription(BonusUsers bonusUser) throws BonusHandlersException{
		// Do Nothing
	}

	/**
	 *  activateBonusAfterInvestmentSuccessByInvAmount event handler implementation
	 */
	@Override
	public boolean activateBonusAfterInvestmentSuccessByInvAmount(Connection conn, BonusUsers bu, long amountLeft, long userId, long investmentId, long writerId, boolean isFree, long loginId, long opportunityTypeId) throws BonusHandlersException{
		return false;
	}

	/**
	 *  activateBonusAfterInvestmentSuccessByInvCount event handler implementation
	 */
	@Override
    public boolean activateBonusAfterInvestmentSuccessByInvCount(Connection conn, BonusUsers bu,  boolean isInvWasCountForActivation, long investmentId, long opportunityTypeId) throws BonusHandlersException{
		return isInvWasCountForActivation;
	}

	/**
	 *  handleBonusOnSettleInvestment event handler implementation
	 */
	@Override
	public long handleBonusOnSettleInvestment(Connection conn, InvestmentBonusData investment, boolean isWin) throws BonusHandlersException{
        return 0;
	}

	/**
	 *  getAmountThatUserCantWithdraw event handler implementation
	 */
	@Override
	public long getAmountThatUserCantWithdraw(long win, long lose, long invAmount, BonusUsers bu){
		return 0;
	}
}