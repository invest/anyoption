package com.anyoption.common.charts;

import java.util.List;
import java.util.Map;

import com.anyoption.common.beans.ChartsUpdaterMarket;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.enums.SkinGroup;

public interface ChartsUpdaterListener {
    public void marketRates(ChartsUpdaterMarket m, Map<SkinGroup, List<MarketRate>> mrs);
    
    public void marketClosed(ChartsUpdaterMarket m);
}