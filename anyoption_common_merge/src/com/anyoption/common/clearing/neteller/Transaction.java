package com.anyoption.common.clearing.neteller;

import java.util.Arrays;

public class Transaction {
	
	public final static String STATUS_ACCEPTED	= "accepted";
	public final static String STATUS_PENDING	= "pending";
	public final static String STATUS_DECLINED	= "declined";
	public final static String STATUS_CANCELLED	= "cancelled";
	public final static String STATUS_FAILED	= "failed";
	
	long amount;
	// ISO 8601 format (UTC) /	YYYY-MM-DDThh:mm:ssZ
	public String createDate;
	// EUR, USD, GBP etc.
	public String currency;
	public String errorCode;
	public String errorMessage;
	Fee[] fees;
	long id; 	
	public String merchantRefId;
	// pending / declined / cancelled / failed
	public String status;
	public String transactionType;
	// ISO 8601 format (UTC) /	YYYY-MM-DDThh:mm:ssZ
	public String updateDate;
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public Fee[] getFees() {
		return fees;
	}
	public void setFees(Fee[] fees) {
		this.fees = fees;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getMerchantRefId() {
		return merchantRefId;
	}
	public void setMerchantRefId(String merchantRefId) {
		this.merchantRefId = merchantRefId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	@Override
	public String toString() {
		return "Transaction [amount=" + amount + ", createDate=" + createDate + ", currency=" + currency
				+ ", errorCode=" + errorCode + ", errorMessage=" + errorMessage + ", fees=" + Arrays.toString(fees)
				+ ", id=" + id + ", merchantRefId=" + merchantRefId + ", status=" + status + ", transactionType="
				+ transactionType + ", updateDate=" + updateDate + "]";
	}


}
