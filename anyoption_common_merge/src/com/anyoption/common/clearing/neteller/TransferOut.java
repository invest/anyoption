package com.anyoption.common.clearing.neteller;

public class TransferOut {
	
	AccountProfile payeeProfile;
	Transaction	transaction;
	String message;
	
	public AccountProfile getPayeeProfile() {
		return payeeProfile;
	}
	public void setPayeeProfile(AccountProfile payeeProfile) {
		this.payeeProfile = payeeProfile;
	}
	public Transaction getTransaction() {
		return transaction;
	}
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return "TransferOut [payeeProfile=" + payeeProfile + ", transaction=" + transaction + ", message=" + message
				+ "]";
	}
	
	
}
