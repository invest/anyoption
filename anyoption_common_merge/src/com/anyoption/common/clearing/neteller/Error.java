package com.anyoption.common.clearing.neteller;

public class Error {
	String code;
	String message;
	String fieldErrors;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getFieldErrors() {
		return fieldErrors;
	}
	public void setFieldErrors(String fieldErrors) {
		this.fieldErrors = fieldErrors;
	}
	@Override
	public String toString() {
		return "Error [code=" + code + ", message=" + message + ", fieldErrors=" + fieldErrors + "]";
	}
	
	
}
