/**
 * BasicOperationsServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.anyoption.common.clearing.payvision.gatewayV2;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.UUID;

import javax.xml.rpc.ServiceException;

import org.apache.axis.types.UnsignedByte;

import com.anyoption.common.clearing.payvision.gatewayV2.entities.TransactionResult;

public class BasicOperationsServiceLocator extends org.apache.axis.client.Service implements BasicOperationsService {

    public BasicOperationsServiceLocator() {
    }


    public BasicOperationsServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BasicOperationsServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BasicHttpBinding_BasicOperations
    private java.lang.String BasicHttpBinding_BasicOperations_address = "https://testprocessor.payvisionservices.com/GatewayV2/BasicOperationsService.svc";

    public java.lang.String getBasicHttpBinding_BasicOperationsAddress() {
        return BasicHttpBinding_BasicOperations_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BasicHttpBinding_BasicOperationsWSDDServiceName = "BasicHttpBinding_BasicOperations";

    public java.lang.String getBasicHttpBinding_BasicOperationsWSDDServiceName() {
        return BasicHttpBinding_BasicOperationsWSDDServiceName;
    }

    public void setBasicHttpBinding_BasicOperationsWSDDServiceName(java.lang.String name) {
        BasicHttpBinding_BasicOperationsWSDDServiceName = name;
    }

    public BasicOperations getBasicHttpBinding_BasicOperations() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BasicHttpBinding_BasicOperations_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBasicHttpBinding_BasicOperations(endpoint);
    }

    public BasicOperations getBasicHttpBinding_BasicOperations(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            BasicHttpBinding_BasicOperationsStub _stub = new BasicHttpBinding_BasicOperationsStub(portAddress, this);
            _stub.setPortName(getBasicHttpBinding_BasicOperationsWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBasicHttpBinding_BasicOperationsEndpointAddress(java.lang.String address) {
        BasicHttpBinding_BasicOperations_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (BasicOperations.class.isAssignableFrom(serviceEndpointInterface)) {
                BasicHttpBinding_BasicOperationsStub _stub = new BasicHttpBinding_BasicOperationsStub(new java.net.URL(BasicHttpBinding_BasicOperations_address), this);
                _stub.setPortName(getBasicHttpBinding_BasicOperationsWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BasicHttpBinding_BasicOperations".equals(inputPortName)) {
            return getBasicHttpBinding_BasicOperations();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://payvision.com/gatewayV2", "BasicOperationsService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://payvision.com/gatewayV2", "BasicHttpBinding_BasicOperations"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BasicHttpBinding_BasicOperations".equals(portName)) {
            setBasicHttpBinding_BasicOperationsEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }
}
