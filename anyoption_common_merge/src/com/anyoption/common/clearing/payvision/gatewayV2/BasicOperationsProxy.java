package com.anyoption.common.clearing.payvision.gatewayV2;

import com.anyoption.common.clearing.payvision.gatewayV2.entities.TransactionResult;

public class BasicOperationsProxy implements BasicOperations {
  private String _endpoint = null;
  private BasicOperations basicOperations = null;
  
  public BasicOperationsProxy() {
    _initBasicOperationsProxy();
  }
  
  public BasicOperationsProxy(String endpoint) {
    _endpoint = endpoint;
    _initBasicOperationsProxy();
  }
  
  private void _initBasicOperationsProxy() {
    try {
      basicOperations = (new BasicOperationsServiceLocator()).getBasicHttpBinding_BasicOperations();
      if (basicOperations != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)basicOperations)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)basicOperations)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (basicOperations != null)
      ((javax.xml.rpc.Stub)basicOperations)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public BasicOperations getBasicOperations() {
    if (basicOperations == null)
      _initBasicOperationsProxy();
    return basicOperations;
  }
  
  public TransactionResult authorize(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (basicOperations == null)
      _initBasicOperationsProxy();
    return basicOperations.authorize(memberId, memberGuid, countryId, amount, currencyId, trackingMemberCode, cardNumber, cardholder, cardExpiryMonth, cardExpiryYear, cardCvv, merchantAccountType, dbaName, dbaCity, avsAddress, avsZip, additionalInfo);
  }
  
  public TransactionResult payment(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (basicOperations == null)
      _initBasicOperationsProxy();
    return basicOperations.payment(memberId, memberGuid, countryId, amount, currencyId, trackingMemberCode, cardNumber, cardholder, cardExpiryMonth, cardExpiryYear, cardCvv, merchantAccountType, dbaName, dbaCity, avsAddress, avsZip, additionalInfo);
  }
  
  public TransactionResult credit(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (basicOperations == null)
      _initBasicOperationsProxy();
    return basicOperations.credit(memberId, memberGuid, countryId, amount, currencyId, trackingMemberCode, cardNumber, cardholder, cardExpiryMonth, cardExpiryYear, cardCvv, merchantAccountType, dbaName, dbaCity, avsAddress, avsZip, additionalInfo);
  }
  
  public TransactionResult refund(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Long transactionId, java.lang.String transactionGuid, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (basicOperations == null)
      _initBasicOperationsProxy();
    return basicOperations.refund(memberId, memberGuid, transactionId, transactionGuid, amount, currencyId, trackingMemberCode, additionalInfo);
  }
  
  public TransactionResult capture(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Long transactionId, java.lang.String transactionGuid, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (basicOperations == null)
      _initBasicOperationsProxy();
    return basicOperations.capture(memberId, memberGuid, transactionId, transactionGuid, amount, currencyId, trackingMemberCode, additionalInfo);
  }
  
  public TransactionResult _void(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Long transactionId, java.lang.String transactionGuid, java.lang.String trackingMemberCode, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (basicOperations == null)
      _initBasicOperationsProxy();
    return basicOperations._void(memberId, memberGuid, transactionId, transactionGuid, trackingMemberCode, additionalInfo);
  }
  
  public TransactionResult referralApproval(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Long transactionId, java.lang.String transactionGuid, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String approvalCode, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (basicOperations == null)
      _initBasicOperationsProxy();
    return basicOperations.referralApproval(memberId, memberGuid, transactionId, transactionGuid, amount, currencyId, trackingMemberCode, approvalCode, additionalInfo);
  }
  
  public TransactionResult cardFundTransfer(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (basicOperations == null)
      _initBasicOperationsProxy();
    return basicOperations.cardFundTransfer(memberId, memberGuid, countryId, amount, currencyId, trackingMemberCode, cardNumber, cardholder, cardExpiryMonth, cardExpiryYear, cardCvv, merchantAccountType, dbaName, dbaCity, avsAddress, avsZip, additionalInfo);
  }
  
  public TransactionResult retrieveTransactionResult(java.lang.Long memberId, java.lang.String memberGuid, java.lang.String trackingMemberCode, java.util.Calendar transactionDate, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (basicOperations == null)
      _initBasicOperationsProxy();
    return basicOperations.retrieveTransactionResult(memberId, memberGuid, trackingMemberCode, transactionDate, additionalInfo);
  }
  
  
}