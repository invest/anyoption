package com.anyoption.common.clearing.payvision.gatewayV2;

import com.anyoption.common.clearing.payvision.gatewayV2.entities.EnrollmentResult;
import com.anyoption.common.clearing.payvision.gatewayV2.entities.TransactionResult;

public class ThreeDSecureProxy implements ThreeDSecure {
  private String _endpoint = null;
  private ThreeDSecure threeDSecure = null;
  
  public ThreeDSecureProxy() {
    _initThreeDSecureProxy();
  }
  
  public ThreeDSecureProxy(String endpoint) {
    _endpoint = endpoint;
    _initThreeDSecureProxy();
  }
  
  private void _initThreeDSecureProxy() {
    try {
      threeDSecure = (new ThreeDSecureServiceLocator()).getBasicHttpBinding_ThreeDSecure();
      if (threeDSecure != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)threeDSecure)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)threeDSecure)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (threeDSecure != null)
      ((javax.xml.rpc.Stub)threeDSecure)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ThreeDSecure getThreeDSecure() {
    if (threeDSecure == null)
      _initThreeDSecureProxy();
    return threeDSecure;
  }
  
  public TransactionResult authorize(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.String xid, java.lang.String authenticationValue, java.lang.String authenticationIndicator, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (threeDSecure == null)
      _initThreeDSecureProxy();
    return threeDSecure.authorize(memberId, memberGuid, countryId, amount, currencyId, trackingMemberCode, cardNumber, cardholder, cardExpiryMonth, cardExpiryYear, cardCvv, merchantAccountType, dbaName, dbaCity, avsAddress, avsZip, xid, authenticationValue, authenticationIndicator, additionalInfo);
  }
  
  public TransactionResult payment(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.String xid, java.lang.String authenticationValue, java.lang.String authenticationIndicator, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (threeDSecure == null)
      _initThreeDSecureProxy();
    return threeDSecure.payment(memberId, memberGuid, countryId, amount, currencyId, trackingMemberCode, cardNumber, cardholder, cardExpiryMonth, cardExpiryYear, cardCvv, merchantAccountType, dbaName, dbaCity, avsAddress, avsZip, xid, authenticationValue, authenticationIndicator, additionalInfo);
  }
  
  public EnrollmentResult checkEnrollment(java.lang.Long memberId, java.lang.String memberGuid, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (threeDSecure == null)
      _initThreeDSecureProxy();
    return threeDSecure.checkEnrollment(memberId, memberGuid, amount, currencyId, trackingMemberCode, cardNumber, cardholder, cardExpiryMonth, cardExpiryYear, additionalInfo);
  }
  
  public TransactionResult authorizeUsingIntegratedMPI(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.lang.String trackingMemberCode, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.Long enrollmentId, java.lang.String enrollmentTrackingMemberCode, java.lang.String payerAuthenticationResponse, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (threeDSecure == null)
      _initThreeDSecureProxy();
    return threeDSecure.authorizeUsingIntegratedMPI(memberId, memberGuid, countryId, trackingMemberCode, cardCvv, merchantAccountType, dbaName, dbaCity, avsAddress, avsZip, enrollmentId, enrollmentTrackingMemberCode, payerAuthenticationResponse, additionalInfo);
  }
  
  public TransactionResult paymentUsingIntegratedMPI(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.lang.String trackingMemberCode, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.Long enrollmentId, java.lang.String enrollmentTrackingMemberCode, java.lang.String payerAuthenticationResponse, java.lang.String additionalInfo) throws java.rmi.RemoteException{
    if (threeDSecure == null)
      _initThreeDSecureProxy();
    return threeDSecure.paymentUsingIntegratedMPI(memberId, memberGuid, countryId, trackingMemberCode, cardCvv, merchantAccountType, dbaName, dbaCity, avsAddress, avsZip, enrollmentId, enrollmentTrackingMemberCode, payerAuthenticationResponse, additionalInfo);
  }
  
  
}