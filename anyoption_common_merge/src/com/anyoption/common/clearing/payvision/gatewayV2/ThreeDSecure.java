/**
 * ThreeDSecure.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.anyoption.common.clearing.payvision.gatewayV2;

import com.anyoption.common.clearing.payvision.gatewayV2.entities.EnrollmentResult;
import com.anyoption.common.clearing.payvision.gatewayV2.entities.TransactionResult;

public interface ThreeDSecure extends java.rmi.Remote {
    public TransactionResult authorize(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.String xid, java.lang.String authenticationValue, java.lang.String authenticationIndicator, java.lang.String additionalInfo) throws java.rmi.RemoteException;
    public TransactionResult payment(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.String xid, java.lang.String authenticationValue, java.lang.String authenticationIndicator, java.lang.String additionalInfo) throws java.rmi.RemoteException;
    public EnrollmentResult checkEnrollment(java.lang.Long memberId, java.lang.String memberGuid, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String additionalInfo) throws java.rmi.RemoteException;
    public TransactionResult authorizeUsingIntegratedMPI(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.lang.String trackingMemberCode, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.Long enrollmentId, java.lang.String enrollmentTrackingMemberCode, java.lang.String payerAuthenticationResponse, java.lang.String additionalInfo) throws java.rmi.RemoteException;
    public TransactionResult paymentUsingIntegratedMPI(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.lang.String trackingMemberCode, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.Long enrollmentId, java.lang.String enrollmentTrackingMemberCode, java.lang.String payerAuthenticationResponse, java.lang.String additionalInfo) throws java.rmi.RemoteException;
}
