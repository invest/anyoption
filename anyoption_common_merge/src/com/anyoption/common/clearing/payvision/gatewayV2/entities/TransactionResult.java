/**
 * TransactionResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.anyoption.common.clearing.payvision.gatewayV2.entities;

public class TransactionResult  extends BaseResult  implements java.io.Serializable {
	private java.lang.String trackingMemberCode;

    private java.util.Calendar transactionDateTime;

    private java.lang.String transactionGuid;

    private java.lang.Long transactionId;

    public TransactionResult() {
    }

    public TransactionResult(
           CdcEntry[] cdc,
           java.lang.String message,
           java.lang.Integer result,
           java.lang.String trackingMemberCode,
           java.util.Calendar transactionDateTime,
           java.lang.String transactionGuid,
           java.lang.Long transactionId) {
        super(
            cdc,
            message,
            result);
        this.trackingMemberCode = trackingMemberCode;
        this.transactionDateTime = transactionDateTime;
        this.transactionGuid = transactionGuid;
        this.transactionId = transactionId;
    }


    /**
     * Gets the trackingMemberCode value for this TransactionResult.
     * 
     * @return trackingMemberCode
     */
    public java.lang.String getTrackingMemberCode() {
        return trackingMemberCode;
    }


    /**
     * Sets the trackingMemberCode value for this TransactionResult.
     * 
     * @param trackingMemberCode
     */
    public void setTrackingMemberCode(java.lang.String trackingMemberCode) {
        this.trackingMemberCode = trackingMemberCode;
    }


    /**
     * Gets the transactionDateTime value for this TransactionResult.
     * 
     * @return transactionDateTime
     */
    public java.util.Calendar getTransactionDateTime() {
        return transactionDateTime;
    }


    /**
     * Sets the transactionDateTime value for this TransactionResult.
     * 
     * @param transactionDateTime
     */
    public void setTransactionDateTime(java.util.Calendar transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }


    /**
     * Gets the transactionGuid value for this TransactionResult.
     * 
     * @return transactionGuid
     */
    public java.lang.String getTransactionGuid() {
        return transactionGuid;
    }


    /**
     * Sets the transactionGuid value for this TransactionResult.
     * 
     * @param transactionGuid
     */
    public void setTransactionGuid(java.lang.String transactionGuid) {
        this.transactionGuid = transactionGuid;
    }


    /**
     * Gets the transactionId value for this TransactionResult.
     * 
     * @return transactionId
     */
    public java.lang.Long getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this TransactionResult.
     * 
     * @param transactionId
     */
    public void setTransactionId(java.lang.Long transactionId) {
        this.transactionId = transactionId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransactionResult)) return false;
        TransactionResult other = (TransactionResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.trackingMemberCode==null && other.getTrackingMemberCode()==null) || 
             (this.trackingMemberCode!=null &&
              this.trackingMemberCode.equals(other.getTrackingMemberCode()))) &&
            ((this.transactionDateTime==null && other.getTransactionDateTime()==null) || 
             (this.transactionDateTime!=null &&
              this.transactionDateTime.equals(other.getTransactionDateTime()))) &&
            ((this.transactionGuid==null && other.getTransactionGuid()==null) || 
             (this.transactionGuid!=null &&
              this.transactionGuid.equals(other.getTransactionGuid()))) &&
            ((this.transactionId==null && other.getTransactionId()==null) || 
             (this.transactionId!=null &&
              this.transactionId.equals(other.getTransactionId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTrackingMemberCode() != null) {
            _hashCode += getTrackingMemberCode().hashCode();
        }
        if (getTransactionDateTime() != null) {
            _hashCode += getTransactionDateTime().hashCode();
        }
        if (getTransactionGuid() != null) {
            _hashCode += getTransactionGuid().hashCode();
        }
        if (getTransactionId() != null) {
            _hashCode += getTransactionId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransactionResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "TransactionResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trackingMemberCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "TrackingMemberCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "TransactionDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionGuid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "TransactionGuid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "TransactionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
