/**
 * BasicOperations.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.anyoption.common.clearing.payvision.gatewayV2;

import com.anyoption.common.clearing.payvision.gatewayV2.entities.TransactionResult;

public interface BasicOperations extends java.rmi.Remote {
    public TransactionResult authorize(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.String additionalInfo) throws java.rmi.RemoteException;
    public TransactionResult payment(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.String additionalInfo) throws java.rmi.RemoteException;
    public TransactionResult credit(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.String additionalInfo) throws java.rmi.RemoteException;
    public TransactionResult refund(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Long transactionId, java.lang.String transactionGuid, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String additionalInfo) throws java.rmi.RemoteException;
    public TransactionResult capture(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Long transactionId, java.lang.String transactionGuid, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String additionalInfo) throws java.rmi.RemoteException;
    public TransactionResult _void(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Long transactionId, java.lang.String transactionGuid, java.lang.String trackingMemberCode, java.lang.String additionalInfo) throws java.rmi.RemoteException;
    public TransactionResult referralApproval(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Long transactionId, java.lang.String transactionGuid, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String approvalCode, java.lang.String additionalInfo) throws java.rmi.RemoteException;
    public TransactionResult cardFundTransfer(java.lang.Long memberId, java.lang.String memberGuid, java.lang.Integer countryId, java.math.BigDecimal amount, java.lang.Integer currencyId, java.lang.String trackingMemberCode, java.lang.String cardNumber, java.lang.String cardholder, org.apache.axis.types.UnsignedByte cardExpiryMonth, java.lang.Short cardExpiryYear, java.lang.String cardCvv, java.lang.Integer merchantAccountType, java.lang.String dbaName, java.lang.String dbaCity, java.lang.String avsAddress, java.lang.String avsZip, java.lang.String additionalInfo) throws java.rmi.RemoteException;
    public TransactionResult retrieveTransactionResult(java.lang.Long memberId, java.lang.String memberGuid, java.lang.String trackingMemberCode, java.util.Calendar transactionDate, java.lang.String additionalInfo) throws java.rmi.RemoteException;
}
