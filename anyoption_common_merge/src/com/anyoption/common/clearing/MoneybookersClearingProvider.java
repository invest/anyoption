package com.anyoption.common.clearing;


/**
 * Implement the communication with MoneyBookers
 *
 * @author Eran
 */
public class MoneybookersClearingProvider extends ClearingProvider {

    public static final String STATUS_PROCESSED_CODE = "2";
    public static final String STATUS_PENDING_CODE = "0";
    public static final String STATUS_CANCELLED_CODE = "-1";
    public static final String STATUS_FAILED_CODE = "-2";
    public static final String STATUS_CHARGEBACK_CODE = "-3";
    public static final String STATUS_PROCESSED_STRING = "Processed";
    public static final String STATUS_PENDING_STRING = "Pending";
    public static final String STATUS_CANCELLED_STRING = "Cancelled";
    public static final String STATUS_FAILED_STRING = "Failed";
    public static final String STATUS_CHARGEBACK_STRING = "Chargeback";

    public void authorize(ClearingInfo info) throws ClearingException {
    	throw new ClearingException("Unsupported operation.");
    }

    public void capture(ClearingInfo info) throws ClearingException {
    	throw new ClearingException("Unsupported operation.");
    }

    public void enroll(ClearingInfo info) throws ClearingException {
    	throw new ClearingException("Unsupported operation.");
    }

    public void purchase(ClearingInfo info) throws ClearingException {
    	throw new ClearingException("Unsupported operation.");
    }

    public void withdraw(ClearingInfo info) throws ClearingException {
    	throw new ClearingException("Unsupported operation.");
    }

    public void bookback(ClearingInfo info) throws ClearingException {
    	throw new ClearingException("Unsupported operation.");
    }

    public void setProviderDetails(MoneybookersInfo info) {
    	info.setRedirectURL(url);
    	info.setMerchantAccount(username);
    	info.setMerchant_id(password); // password in our case equal to merchantId
    }

	@Override
	public void cancel(ClearingInfo info) throws ClearingException {
		// TODO Auto-generated method stub
	}

}