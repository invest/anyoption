package com.anyoption.common.clearing;


import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingProvider;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.util.ClearingUtil;
import com.anyoption.common.util.CommonUtil;

/**
 * Implements the TBI clearing interface communication.
 *
 * @author oshikl
 */
public class TBIClearingProvider extends ClearingProvider {
    private static Logger log = Logger.getLogger(TBIClearingProvider.class);
    private static final String CHARGE_DESCRIPTOR_ANYOPTION = "anyoption"; //TODO refactor, should be changeable.
    private static final String CHARGE_DESCRIPTOR_COPYOP = "copyop binary options"; //TODO refactor, should be changeable.
	private static SimpleDateFormat dateOfBirth = new SimpleDateFormat("yyyyMMdd");

	/**
     * Process an "authorize" call through this provider.
     *
     * @param info thransaction info
     * @throws ClearingException
     */
    public void authorize(ClearingInfo info) throws ClearingException {
        request(info, true, false);
    }

    /**
     * Process an "capture" call through this provider.
     *
     * @param info thransaction info
     * @throws ClearingException
     */
    public void capture(ClearingInfo info) throws ClearingException {
        request(info, false, false);
    }

    /**
     * Process a "enroll" request through this provider. Enroll is the
     * request that checks through hosted MPI if certain card has 3D
     * security enrolled. A Merchant Plug In (MPI) is a software module
     * which provides a communication interface between the merchant and
     * the Visa or MasterCard directory servers. Hosted MPI is when the
     * clearing provider also provide MPI to be used through the communication
     * interface with it (and no need to be installed on merchant side).
     *
     * @param info transaction info
     * @throws ClearingException
     */
    public void enroll(ClearingInfo info) throws ClearingException {
        throw new ClearingException("Unsupported operation.");
    }

    /**
     * Process a "purchase" call through this provider. Purchase is a direct
     * one step debit of the card (compared to authorize and capture 2 steps).
     *
     * @param info transaction info
     * @throws ClearingException
     */
    public void purchase(ClearingInfo info) throws ClearingException {
        throw new ClearingException("Unsupported operation.");
    }

    /**
     * Process a "withdraw" call through this provider.
     *
     * @param info
     * @throws ClearingException
     */
    public void withdraw(ClearingInfo info) throws ClearingException {
    	request(info, false, true);
    }

    /**
     * Process a "bookback" call through this provider. This is refund money
     * for previous successful transaction over the same provider.
     *
     * @param info
     * @throws ClearingException
     */
    public void bookback(ClearingInfo info) throws ClearingException {
    	requestWithdrawal(info, false);
    }

    /**
     * Process a request through this provider.
     *
     * @param info thransaction info
     * @param authorize authorize or capture request to make
     * @throws ClearingException
     */
    private void request(ClearingInfo info, boolean authorize, boolean isCFT) throws ClearingException {
        if (log.isTraceEnabled()) {
            log.trace(info.toString());
        }
        try {
            String request = createRequest(info, authorize, isCFT);
            if (log.isTraceEnabled()) {
                log.trace(request);
            }
            String body =
                "user=" + URLEncoder.encode(username,"UTF-8") +
                "&password=" + URLEncoder.encode(password,"UTF-8") +
                "&int_in=" + URLEncoder.encode(request,"UTF-8");
            String response = ClearingUtil.executePOSTRequest(url, body);
            parseResponse(response, info);
        } catch (Throwable t) {
            throw new ClearingException("Transaction failed.", t);
        }
    }
    
    private void requestWithdrawal(ClearingInfo info, boolean authorize) throws ClearingException {
        if (log.isTraceEnabled()) {
            log.trace(info.toString());
        }
        try {
            String request = createRequestWithdrawal(info, authorize);
            if (log.isTraceEnabled()) {
                log.trace(request);
            }
            String body =
                "user=" + URLEncoder.encode(username,"UTF-8") +
                "&password=" + URLEncoder.encode(password,"UTF-8") +
                "&int_in=" + URLEncoder.encode(request,"UTF-8");
            String response = ClearingUtil.executePOSTRequest(url, body);
            parseResponse(response, info);
        } catch (Throwable t) {
            throw new ClearingException("Transaction failed.", t);
        }
    }

    /**
     * Create the XML string for "doDeal" request.
     *
     * @param info transaction info
     * @return New <code>String</code> with the authorize XML.
     */
    private  String createRequest(ClearingInfo info, boolean authorize, boolean isCFT) {
        boolean deposit = info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT || info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT_REROUTE;
        boolean commit =
            ((info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT && !authorize) ||
           ( info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW ||
        		   info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT ));
        boolean bookBack =
        				((deposit == false) && (info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW
        					|| info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT)
        					&& info.getOriginalDepositId() > 0);
        String birthDate = "";
        if (info.getUserDOB() != null) {
        	birthDate = dateOfBirth.format(info.getUserDOB());
        }
        
        String cvv = "";
        if (authorize) {
        	cvv = (null != info.getCvv() && !info.getCvv().equals("") ? info.getCvv() : "2");
        }
        
		//TODO refactor, create request with parser.
        String xmlRequest = 
        		 	"<ashrait>" +
    	                "<request>" +
    	                    "<command>doDeal</command>" +
    	                    "<requestId>" + (authorize ? "a" : "c") + info.getTransactionId() + "</requestId>" +
    	                    "<version>1000</version>" +
    	                    "<language>ENG</language>" +
    	                    "<doDeal>" +
    	                    	"<terminalNumber>" + ((deposit || bookBack) ? depositTerminalId : withdrawTerminalId) + "</terminalNumber>" +
    	                        "<cardNo>" + info.getCcn() + "</cardNo>" +
    	                        "<cardExpiration>" + info.getExpireMonth() + info.getExpireYear() + "</cardExpiration>" +
    	                        "<total>" + info.getAmount() + "</total>" +
    	                        "<transactionType>" + (deposit ? "Debit" : "Credit") + "</transactionType>" +
    	                        "<creditType>RegularCredit</creditType>" +
    	                        "<currency>" + info.getCurrencySymbol() + "</currency>" +
    	                        (authorize ? "" : "<authNumber>" + (null == info.getAuthNumber()? "" : info.getAuthNumber()) + "</authNumber>") +
    	                        "<transactionCode>Phone</transactionCode>" +
    	                        "<validation>" + (commit ? "autoComm" : "Verify") + "</validation>" +
    	                        "<firstPayment></firstPayment>" +
    	                        "<periodicalPayment></periodicalPayment>" +
    	                        "<numberOfPayments></numberOfPayments>" +
    	                        "<cvv>" + cvv + "</cvv>" +
                        		"<user>" + info.getTransactionId() + "</user>" +
                				"<cavv></cavv>" +
                				"<xid></xid>" +
    	                        "<eci>7</eci>";
//        	if (info.getIp() != null) {
//        		xmlRequest += 	"<clientIp>" + info.getIp() + "</clientIp>";
//        	}
        	xmlRequest +=		"<email>" + info.getEmail() + "</email>" +
    	                        "<authType></authType>" + 
    							"<customerData>" + 
    								"<extendedTransactionType>ECOMMERCE</extendedTransactionType>" +
    								"<extendedPaymentType>" + (isCFT ? "CFT" : "SALE") + "</extendedPaymentType>" +
									"<transactionApplicationId>" + (isCFT ? "FD" : "") + "</transactionApplicationId>" +
									"<senderReferenceNumber></senderReferenceNumber>" +
									"<senderAccountNumber></senderAccountNumber>" +        								
    								"<chargeDescriptor>" + (info.getPlatformId() == Platform.COPYOP ? CHARGE_DESCRIPTOR_COPYOP : CHARGE_DESCRIPTOR_ANYOPTION) + "</chargeDescriptor> " +	
    								"<merchantCity>Nicosia</merchantCity>" +        								
    								"<merchantState>CY</merchantState>" +       								
    								"<merchantZip></merchantZip>" +
    								"<merchantCountryCode>357</merchantCountryCode>" +
    								"<merchantBin></merchantBin>" +
    								(authorize ? "" : "<acquirerResponseId>" + (CommonUtil.isParameterEmptyOrNull(info.getAcquirerResponseId()) ? "" : info.getAcquirerResponseId() ) + "</acquirerResponseId>") +
    								(info.isFirstTimeTBI()? "<recurringTransaction>INITIAL</recurringTransaction>" + "<recurringTransactionSecurity></recurringTransactionSecurity>" : 
    								"<recurringTransaction>PAYMENT</recurringTransaction>" +
    								"<recurringTransactionSecurity>" + info.getRecurringTransaction() + "</recurringTransactionSecurity>" ) +	
    								"<avsAddress></avsAddress>" +
    								"<avsZip></avsZip>" +
    								"<firstName>" + info.getFirstName() + "</firstName>" +
    								"<lastName>" + info.getLastName() + "</lastName>" +
    								"<birthDate>" + birthDate + "</birthDate>" +
    							"</customerData>" +
    							"<purchaseData>" +
    								"<billingAddressCountry></billingAddressCountry>" +
        							"<billingAddressState></billingAddressState>" +
        							"<billingAddressPostalCode></billingAddressPostalCode>" +
        							"<billingAddressCity></billingAddressCity>" +
        							"<billingAddressHouseNumber></billingAddressHouseNumber>" +
        							"<billingAddressStreet></billingAddressStreet>" +
        							"<shippingAddressCountry></shippingAddressCountry>" +
        							"<shippingAddressState></shippingAddressState>" +
        							"<shippingAddressPostalCode></shippingAddressPostalCode>" +
        							"<shippingAddressCity></shippingAddressCity>" +
        							"<shippingAddressHouseNumber></shippingAddressHouseNumber>" +
        							"<shippingAddressStreet></shippingAddressStreet>" +
    							"</purchaseData>" +
    							"<sectorData/>" +
    						"</doDeal>" +
    	                "</request>" +
    	            "</ashrait>";       
        return xmlRequest;
           
    }
    
    private  String createRequestWithdrawal(ClearingInfo info, boolean authorize) {
//        boolean deposit = info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT;
//        boolean commit =
//            ((info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT && !authorize) ||
//           ( info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW ||
//        		   info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT ));
//        boolean bookBack =
//        				((deposit == false) && (info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW
//        					|| info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT)
//        					&& info.getOriginalDepositId() > 0);
        return
            
        		"<ashrait>" +
        			"<request>" +
        				"<version>1000</version>" +
        				"<language>ENG</language>" +
        				"<dateTime></dateTime>" +
        				"<command>refundDeal</command>" +
        				"<requestId>" + (authorize ? "a" : "c") + info.getTransactionId() + "</requestId>" +
        				"<refundDeal>" +
        					"<terminalNumber>" +  depositTerminalId  + "</terminalNumber>" +
        					"<tranId>" + (null == info.getProviderDepositId() ? "" : info.getProviderDepositId()) + "</tranId>" +
        					"<total>" + info.getAmount() + "</total>" +
        					"<user></user>" +
        					"<authNumber></authNumber>" +
        					"<cardNo>" + info.getCcn() + "</cardNo>" +
        				"</refundDeal>" +
        			"</request>" +
        		"</ashrait>";
    }

    
    /**
     * Parse TBI "doDeal" result.
     *
     * @param result the result XML
     * @return <code>Hashtable</code> with result values.
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    private  void parseResponse(String response, ClearingInfo info) throws ParserConfigurationException, SAXException, IOException {
        Document respDoc = ClearingUtil.parseXMLToDocument(response);
        Element root = (Element) ClearingUtil.getNode(respDoc.getDocumentElement(), "");
        info.setResult(ClearingUtil.getElementValue(root, "result/*"));
        info.setMessage(ClearingUtil.getElementValue(root, "message/*"));
        info.setUserMessage(ClearingUtil.getElementValue(root, "userMessage/*"));
        info.setProviderTransactionId(ClearingUtil.getElementValue(root, "tranId/*"));
        info.setSuccessful(info.getResult().equals("000"));

        //verify result from TBI is numeric
        try {
            Integer.parseInt(info.getResult());
        } catch (NumberFormatException nfe) {
            log.log(Level.WARN, "TBI result not a number.", nfe);
        }

        if (info.isSuccessful()) {
            info.setAuthNumber(ClearingUtil.getElementValue(root, "doDeal/authNumber/*"));
            info.setAcquirerResponseId(ClearingUtil.getElementValue(root, "doDeal/customerData/acquirerResponseId/*"));
        }

        if(info.isFirstTimeTBI()){
        	info.setRecurringTransaction(ClearingUtil.getElementValue(root, "doDeal/customerData/recurringTransactionSecurity/*"));
        }
        	

        if (log.isDebugEnabled()) {
            log.debug(info.toString());
        }
    }

	@Override
	public void cancel(ClearingInfo info) throws ClearingException {
		// TODO Auto-generated method stub
		log.info("NOT IMPLEMENTED !!!");	
	}
}