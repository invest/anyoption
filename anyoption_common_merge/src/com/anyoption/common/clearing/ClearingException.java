package com.anyoption.common.clearing;

public class ClearingException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 8071286121025861386L;

	public ClearingException() {
        super();
    }

    public ClearingException(String message) {
        super(message);
    }

    public ClearingException(String message, Throwable t) {
        super(message, t);
    }
}