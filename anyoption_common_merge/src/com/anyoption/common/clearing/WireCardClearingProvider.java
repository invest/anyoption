package com.anyoption.common.clearing;

import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingProvider;
import com.anyoption.common.clearing.ClearingProviderConfig;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.util.ClearingUtil;
import com.anyoption.common.util.CommonUtil;

/**
 * Implement the communication with Wire Card.
 *
 * @author Tony
 */
public class WireCardClearingProvider extends ClearingProvider {
    private static final Logger log = Logger.getLogger(WireCardClearingProvider.class);

    private static final int FNC_AUTHORIZE = 1;
    private static final int FNC_CAPTURE_AUTHORIZE = 2;
    private static final int FNC_ENROLL = 3;
    private static final int FNC_PURCHASE = 4;
    private static final int FNC_OCT = 5;
    private static final int FNC_BOOKBACK = 6;
    private static final int FNC_REFUND = 7;

    private Hashtable<String, String> headers;
    private boolean demo;

    /**
     * Process an "authorize" call through this provider.
     *
     * @param info thransaction info
     * @throws ClearingException
     */
    public void authorize(ClearingInfo info) throws ClearingException {
        request(info, FNC_AUTHORIZE);
    }

    /**
     * Process an "capture" call through this provider.
     *
     * @param info thransaction info
     * @throws ClearingException
     */
    public void capture(ClearingInfo info) throws ClearingException {
        request(info, FNC_CAPTURE_AUTHORIZE);
    }

    /**
     * Process a "enroll" request through this provider. Enroll is the
     * request that checks through hosted MPI if certain card has 3D
     * security enrolled. A Merchant Plug In (MPI) is a software module
     * which provides a communication interface between the merchant and
     * the Visa or MasterCard directory servers. Hosted MPI is when the
     * clearing provider also provide MPI to be used through the communication
     * interface with it (and no need to be installed on merchant side).
     *
     * @param info transaction info
     * @throws ClearingException
     */
    public void enroll(ClearingInfo info) throws ClearingException {
        request(info, FNC_ENROLL);
    }

    /**
     * Process a "purchase" call through this provider. Purchase is a direct
     * one step debit of the card (compared to authorize and capture 2 steps).
     *
     * @param info transaction info
     * @throws ClearingException
     */
    public void purchase(ClearingInfo info) throws ClearingException {
        request(info, FNC_PURCHASE);
    }

    /**
     * Process a "withdraw" call through this provider.
     *
     * @param info
     * @throws ClearingException
     */
    public void withdraw(ClearingInfo info) throws ClearingException {
        //request(info, FNC_OCT);
    	request(info, FNC_REFUND);
    }

    /**
     * Process a "bookback" call through this provider. This is refund money
     * for previous successful transaction over the same provider.
     *
     * @param info
     * @throws ClearingException
     */
    public void bookback(ClearingInfo info) throws ClearingException {
        request(info, FNC_BOOKBACK);
    }

    /**
     * Process a request through this provider.
     *
     * @param info thransaction info
     * @param authorize authorize or capture request to make
     * @throws ClearingException
     */
	private void request(ClearingInfo info, int type) throws ClearingException {
        if (log.isTraceEnabled()) {
            log.trace(info.toString());
        }
        try {
            boolean deposit = info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT || info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT_REROUTE;
            boolean bookBack =
				((deposit == false) && (info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW
					|| info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT)
					&& (!CommonUtil.isParameterEmptyOrNull(info.getProviderDepositId())));

            Document request = createRequest((deposit || bookBack) ? depositTerminalId : withdrawTerminalId);            
            String fnc = null;
            switch (type) {
            case FNC_AUTHORIZE:
                fnc = "FNC_CC_AUTHORIZATION";
                addFunction(request, info, fnc);
                break;
            case FNC_CAPTURE_AUTHORIZE:
                fnc = addCaptureAuthorizationFunction(request, info);
                break;
            case FNC_ENROLL:
                fnc = "FNC_CC_ENROLLMENT_CHECK";
                addFunction(request, info, fnc);
                break;
            case FNC_PURCHASE:
                fnc = addPurchaseFunction(request, info);
                break;
            case FNC_OCT:
                fnc = addOCTFunction(request, info);
                break;
            case FNC_BOOKBACK:
                fnc = addBookbackFunction(request, info);
                break;
            case FNC_REFUND:
                fnc = addRefundFunction(request, info);
                break;
            default:
                throw new ClearingException("Unkown request type");
            }
            String requestXML = ClearingUtil.transformDocumentToString(request);
            if (log.isTraceEnabled()) {
                log.trace(requestXML);
            }
            String responseXML = ClearingUtil.executePOSTRequest(url, requestXML, false, true, headers);
            if (log.isTraceEnabled()) {
                log.trace(responseXML);
            }
            parseResponse(responseXML, info, fnc);
        } catch (Throwable t) {
            throw new ClearingException("Transaction failed.", t);
        }
    }

    /**
     * Set clearing provider configuration.
     *
     * @param key
     * @param props
     * @throws ClearingException
     */
    public void setConfig(ClearingProviderConfig config) throws ClearingException {
        super.setConfig(config);

        try {
            headers = new Hashtable<String, String>();
            headers.put("Authorization", "Basic " + new String(Base64.encodeBase64((config.getUserName() + ":" + config.getPassword()).getBytes()), "UTF-8"));
            headers.put("Content-Type", "text/xml");
    
            demo = false;
            if (null != config.getProps() && !config.getProps().trim().equals("")) {
                String[] ps = config.getProps().split(",");
                for (int i = 0; i < ps.length; i++) {
                    String[] p = ps[i].split("=");
                    if (p[0].equals("demo")) {
                        demo = p[1].equalsIgnoreCase("true");
                    } else {
                        log.warn("Unknown property " + ps[i]);
                    }
                }
            }
            if (log.isInfoEnabled()) {
                log.info("demo: " + demo);
            }
        } catch (Exception e) {
            throw new ClearingException("Can't init WireCardClearingProvider.", e);
        }
    }

    /**
     * Create WireCard request XML document.
     *
     * @param businessCaseSignature the business case signature
     * @return New Document with the WireCard XML structure.
     */
    private Document createRequest(String businessCaseSignature) {
        Document request = null;
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            request = docBuilder.newDocument();

            Element root = request.createElement("WIRECARD_BXML");
            request.appendChild(root);
            ClearingUtil.setAttribute(request, "", "xmlns:xsi", "http://www.w3.org/1999/XMLSchema-instance");
            ClearingUtil.setAttribute(request, "", "xsi:noNamespaceSchemaLocation", "wirecard.xsd");

            ClearingUtil.setNodeValue(root, "W_REQUEST/W_JOB/BusinessCaseSignature/*", businessCaseSignature);
        } catch (Exception e) {
            log.log(Level.ERROR, "Failed to create request document.", e);
        }

        return request;
    }

    /**
     * Add "authorize" and "enroll" functions details.
     *
     * @param request
     * @param info
     * @param fnc
     */
	private void addFunction(Document request, ClearingInfo info, String fnc) {
        Element root = request.getDocumentElement();
        String bp = "W_REQUEST/W_JOB/" + fnc + "/";

        // transaction data
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/TransactionID/*", String.valueOf(info.getTransactionId()));
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/Amount/*", String.valueOf(info.getAmount()));
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/Currency/*", info.getCurrencySymbol());
        //ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CountryCode/*", info.getCountryA2());
          ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CountryCode/*", "CY");

        if (null != info.getAuthNumber()) {
            ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/GuWID/*", info.getAuthNumber());
        }

        // CC data
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CREDIT_CARD_DATA/CreditCardNumber/*", info.getCcn());
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CREDIT_CARD_DATA/CVC2/*", info.getCvv());
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CREDIT_CARD_DATA/ExpirationYear/*", "20" + info.getExpireYear());
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CREDIT_CARD_DATA/ExpirationMonth/*", info.getExpireMonth());
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CREDIT_CARD_DATA/CardHolderName/*", ClearingUtil.a(info.getOwner(), 256));

        // contact data
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CONTACT_DATA/IPAddress/*", info.getIp());

        // trust center data
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CORPTRUSTCENTER_DATA/ADDRESS/FirstName/*", ClearingUtil.an(info.getFirstName(), 128));
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CORPTRUSTCENTER_DATA/ADDRESS/LastName/*", ClearingUtil.an(info.getLastName(), 128));
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CORPTRUSTCENTER_DATA/ADDRESS/Address1/*", ClearingUtil.an(info.getAddress(), 256));
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CORPTRUSTCENTER_DATA/ADDRESS/Country/*", info.getCountryA2());

        // set demo mode if needed
        if (demo) {
            ClearingUtil.setAttribute(request, bp + "CC_TRANSACTION", "mode", "demo");
        }
    }

    /**
     * Add "capture authorization" function to the general wire card request (the FNC_CC_CAPTURE_AUTHORIZATION node).
     *
     * @param request the request to which to add the function
     * @param info the transaction information
     * @return Wire Card function name.
     */
    private String addCaptureAuthorizationFunction(Document request, ClearingInfo info) {
        Element root = request.getDocumentElement();
        String bp = "W_REQUEST/W_JOB/FNC_CC_CAPTURE_AUTHORIZATION/";

        // transaction data
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/TransactionID/*", String.valueOf(info.getTransactionId()));
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/GuWID/*", info.getAuthNumber());

        // set demo mode if needed
        if (demo) {
            ClearingUtil.setAttribute(request, bp + "CC_TRANSACTION", "mode", "demo");
        }

        return "FNC_CC_CAPTURE_AUTHORIZATION";
    }

    /**
     * Add "purchase" function to the request (the FNC_CC_PURCHASE node).
     *
     * @param request
     * @param info
     * @return Wire Card function name.
     */
    private String addPurchaseFunction(Document request, ClearingInfo info) {
        Element root = request.getDocumentElement();
        String bp = "W_REQUEST/W_JOB/FNC_CC_PURCHASE/";

        // transaction data
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/TransactionID/*", String.valueOf(info.getTransactionId()));
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/GuWID/*", info.getAuthNumber());

        // CC data
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CREDIT_CARD_DATA/CVC2/*", info.getCvv());

        // 3D Secure
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/THREE-D_SECURE/PARes/*", info.getPaRes());

        // set demo mode if needed
        if (demo) {
            ClearingUtil.setAttribute(request, bp + "CC_TRANSACTION", "mode", "demo");
        }

        return "FNC_CC_PURCHASE";
    }

    /**
     * Add OCT function to the general wire card request (the FNC_CC_OCT node).
     *
     * @param request the request to which to add the function
     * @param info the transaction information
     * @return Wire Card function name.
     */
    private String addOCTFunction(Document request, ClearingInfo info) {
        Element root = request.getDocumentElement();
        String bp = "W_REQUEST/W_JOB/FNC_CC_OCT/CC_TRANSACTION/";

        // transaction data
        ClearingUtil.setNodeValue(root, bp + "TransactionID/*", String.valueOf(info.getTransactionId()));
        ClearingUtil.setNodeValue(root, bp + "Amount/*", String.valueOf(info.getAmount()));
        ClearingUtil.setNodeValue(root, bp + "Currency/*", info.getCurrencySymbol());
        //ClearingUtil.setNodeValue(root, bp + "CountryCode/*", info.getCountryA2());
        ClearingUtil.setNodeValue(root, bp + "CountryCode/*", "CY");


        if (null != info.getAuthNumber()) {
            ClearingUtil.setNodeValue(root, bp + "GuWID/*", info.getAuthNumber());
        } else {
            ClearingUtil.setNodeValue(root, bp + "CREDIT_CARD_DATA/CreditCardNumber/*", info.getCcn());
            ClearingUtil.setNodeValue(root, bp + "CREDIT_CARD_DATA/ExpirationYear/*", "20" + info.getExpireYear());
            ClearingUtil.setNodeValue(root, bp + "CREDIT_CARD_DATA/ExpirationMonth/*", info.getExpireMonth());
            ClearingUtil.setNodeValue(root, bp + "CREDIT_CARD_DATA/CardHolderName/*", ClearingUtil.a(info.getOwner(), 256));
        }

        // set demo mode if needed
        if (demo) {
            ClearingUtil.setAttribute(request, "W_REQUEST/W_JOB/FNC_CC_OCT/CC_TRANSACTION", "mode", "demo");
        }

        return "FNC_CC_OCT";
    }

    /**
     * Add Refund function to the general wire card request (the FNC_CC_REFUND node).
     *
     * @param request the request to which to add the function
     * @param info the transaction information
     * @return Wire Card function name.
     */
    private String addRefundFunction(Document request, ClearingInfo info) {
        Element root = request.getDocumentElement();
        String bp = "W_REQUEST/W_JOB/FNC_CC_REFUND/CC_TRANSACTION/";

        // transaction data
        ClearingUtil.setNodeValue(root, bp + "TransactionID/*", String.valueOf(info.getTransactionId()));
        ClearingUtil.setNodeValue(root, bp + "Amount/*", String.valueOf(info.getAmount()));
        ClearingUtil.setNodeValue(root, bp + "Currency/*", info.getCurrencySymbol());
        //ClearingUtil.setNodeValue(root, bp + "CountryCode/*", info.getCountryA2());
        ClearingUtil.setNodeValue(root, bp + "CountryCode/*", "CY");


        if (null != info.getAuthNumber()) {
            ClearingUtil.setNodeValue(root, bp + "GuWID/*", info.getAuthNumber());
        } else {
            ClearingUtil.setNodeValue(root, bp + "CREDIT_CARD_DATA/CreditCardNumber/*", info.getCcn());
            ClearingUtil.setNodeValue(root, bp + "CREDIT_CARD_DATA/ExpirationYear/*", "20" + info.getExpireYear());
            ClearingUtil.setNodeValue(root, bp + "CREDIT_CARD_DATA/ExpirationMonth/*", info.getExpireMonth());
            ClearingUtil.setNodeValue(root, bp + "CREDIT_CARD_DATA/CardHolderName/*", ClearingUtil.a(info.getOwner(), 256));
        }

        // set demo mode if needed
        if (demo) {
            ClearingUtil.setAttribute(request, "W_REQUEST/W_JOB/FNC_CC_REFUND/CC_TRANSACTION", "mode", "demo");
        }

        return "FNC_CC_REFUND";
    }

    /**
     * Add "bookback" function to the general wire card request (the FNC_CC_BOOKBACK node).
     *
     * @param request the request to which to add the function
     * @param info the transaction information
     * @return Wire Card function name.
     */
    private String addBookbackFunction(Document request, ClearingInfo info) {
        Element root = request.getDocumentElement();
        String bp = "W_REQUEST/W_JOB/FNC_CC_BOOKBACK/";

        // transaction data
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/TransactionID/*", String.valueOf(info.getTransactionId()));
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/GuWID/*", info.getProviderDepositId());  // filled in the auth_number
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/Amount/*", String.valueOf(info.getAmount()));

        // credit card data
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CREDIT_CARD_DATA/ExpirationYear/*", "20" + info.getExpireYear());
        ClearingUtil.setNodeValue(root, bp + "CC_TRANSACTION/CREDIT_CARD_DATA/ExpirationMonth/*", info.getExpireMonth());

        // set demo mode if needed
        if (demo) {
            ClearingUtil.setAttribute(request, bp + "CC_TRANSACTION", "mode", "demo");
        }

        return "FNC_CC_BOOKBACK";
    }

    /**
     * Parse the response.
     *
     * @param xml the XML to parse
     * @param info the transaction info to fill the result values
     * @param fnc the function that is called (FNC_CC_AUTHORIZATION/FNC_CC_CAPTURE_AUTHORIZATION)
     * @throws Exception
     */
    private static void parseResponse(String xml, ClearingInfo info, String fnc) throws Exception {
        Document resp = ClearingUtil.parseXMLToDocument(xml);

        Element status = (Element) ClearingUtil.getNode(resp.getDocumentElement(), "W_RESPONSE/W_JOB/" + fnc + "/CC_TRANSACTION/PROCESSING_STATUS");
        String guWID    = ClearingUtil.getElementValue(status, "GuWID/*");
        String funRes   = ClearingUtil.getElementValue(status, "FunctionResult/*");
        String statType = ClearingUtil.getElementValue(status, "StatusType/*");

        Element trans = (Element) ClearingUtil.getNode(resp.getDocumentElement(), "W_RESPONSE/W_JOB/" + fnc + "/CC_TRANSACTION");
        info.setPaReq(ClearingUtil.getElementValue(trans, "THREE-D_SECURE/PAReq/*"));
        info.setAcsUrl(ClearingUtil.getElementValue(trans, "THREE-D_SECURE/AcsUrl/*"));

        info.setAuthNumber(guWID);
        info.setProviderTransactionId(guWID);

        if (null != funRes) {
            // the three valid values are ACK, NOK and PENDING
            if (funRes.equals("ACK")) {
                if (!fnc.equals(FNC_ENROLL) || ((null != statType) && statType.equals("Y"))) {
                    info.setSuccessful(true);
                    info.setResult("1000");
                    info.setMessage("Permitted transaction.");
                    info.setUserMessage("Permitted transaction.");
                } else {
                    info.setSuccessful(false);
                    info.setResult("999");
                    info.setMessage("3D StatusType not Y");
                    info.setUserMessage("3D StatusType not Y");
                }
            } else if (funRes.equals("NOK")) {
                info.setSuccessful(false);
                handleError(info, status);
            } else if (funRes.equals("PENDING")) {
                info.setSuccessful(true);
                info.setResult("1000");
                info.setMessage("Permitted transaction.");
                info.setUserMessage("Permitted transaction.");
            }
        } else {
            // try if the resonse is not with the strange error structure that we've seen
            Element error = (Element) ClearingUtil.getNode(resp.getDocumentElement(), "W_RESPONSE");
            handleError(info, error);
        }
    }

    /**
     * Handle the "ERROR" tag and its "Type", "Number", "Message" and "Advice"
     * sub tags.
     *
     * @param info the transaction info (to set error code and description)
     * @param e the element under which to look for "ERROR" tag
     */
    private static void handleError(ClearingInfo info, Element e) {
        // fill the error part
        String type     = ClearingUtil.getElementValue(e, "ERROR/Type/*");
        String number   = ClearingUtil.getElementValue(e, "ERROR/Number/*");
        String message  = ClearingUtil.getElementValue(e, "ERROR/Message/*");
        String advice   = ClearingUtil.getElementValue(e, "ERROR/Advice/*");
        if (null != number) {
            info.setResult(number);
        } else {
            info.setResult("-1");
            info.setMessage((null != type ? type : "") + "Unknown response.");
            info.setUserMessage("Transaction failed.");
        }
        if (null != message) {
            info.setUserMessage(message);
        }
        if (null != advice) {
            info.setMessage((null != type ? type : "") + advice);
        }
    }

	@Override
	public void cancel(ClearingInfo info) throws ClearingException {
		// TODO Auto-generated method stub
		log.info("NOT IMPLEMENTED !!!");
	}
}