package com.anyoption.common.clearing;

import java.io.IOException;
import java.net.URLEncoder;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingProvider;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.util.ClearingUtil;
import com.anyoption.common.util.ConstantsBase;

/**
 * Implements the XOR clearing interface communication.
 *
 * @author Tony
 */
public class XORClearingProvider extends ClearingProvider {
    private static Logger log = Logger.getLogger(XORClearingProvider.class);

	/**
     * Process an "authorize" call through this provider.
     *
     * @param info thransaction info
     * @throws ClearingException
     */
    public void authorize(ClearingInfo info) throws ClearingException {
        request(info, true);
    }

    /**
     * Process an "capture" call through this provider.
     *
     * @param info thransaction info
     * @throws ClearingException
     */
    public void capture(ClearingInfo info) throws ClearingException {
        request(info, false);
    }

    /**
     * Process a "enroll" request through this provider. Enroll is the
     * request that checks through hosted MPI if certain card has 3D
     * security enrolled. A Merchant Plug In (MPI) is a software module
     * which provides a communication interface between the merchant and
     * the Visa or MasterCard directory servers. Hosted MPI is when the
     * clearing provider also provide MPI to be used through the communication
     * interface with it (and no need to be installed on merchant side).
     *
     * @param info transaction info
     * @throws ClearingException
     */
    public void enroll(ClearingInfo info) throws ClearingException {
        throw new ClearingException("Unsupported operation.");
    }

    /**
     * Process a "purchase" call through this provider. Purchase is a direct
     * one step debit of the card (compared to authorize and capture 2 steps).
     *
     * @param info transaction info
     * @throws ClearingException
     */
    public void purchase(ClearingInfo info) throws ClearingException {
        throw new ClearingException("Unsupported operation.");
    }

    /**
     * Process a "withdraw" call through this provider.
     *
     * @param info
     * @throws ClearingException
     */
    public void withdraw(ClearingInfo info) throws ClearingException {
        request(info, false);
    }

    /**
     * Process a "bookback" call through this provider. This is refund money
     * for previous successful transaction over the same provider.
     *
     * @param info
     * @throws ClearingException
     */
    public void bookback(ClearingInfo info) throws ClearingException {
    	request(info, false);
    }

    /**
     * Process a request through this provider.
     *
     * @param info thransaction info
     * @param authorize authorize or capture request to make
     * @throws ClearingException
     */
    private void request(ClearingInfo info, boolean authorize) throws ClearingException {
        if (log.isTraceEnabled()) {
            log.trace(info.toString());
        }
        try {
            String request = createRequest(info, authorize);
            if (log.isTraceEnabled()) {
                log.trace(request);
            }
            String body =
                "user=" + URLEncoder.encode(username,"UTF-8") +
                "&password=" + URLEncoder.encode(password,"UTF-8") +
                "&int_in=" + URLEncoder.encode(request,"UTF-8");
            String response = ClearingUtil.executePOSTRequest(url, body);
           // String response = "<?xml version='1.0'?><ashrait><response><command>doDeal</command><dateTime>2006-10-12 14:31</dateTime><requestId>a47</requestId><tranId>1181011</tranId><result>000</result><message>Invalid value in an XML field</message><userMessage>Please contact System Administration</userMessage><additionalInfo>transactionType value is incorrect</additionalInfo><version>1001</version><language>Eng</language><doDeal><status>331</status><statusText>Invalid value in an XML field</statusText><cardNo></cardNo><cardName></cardName><cardType code=\"\"></cardType><creditCompany code=\"\"></creditCompany><cardBrand code=\"\"></cardBrand><cardAcquirer code=\"\"></cardAcquirer><cardExpiration></cardExpiration><serviceCode></serviceCode><transactionType code=\"\"></transactionType><creditType code=\"\"></creditType><currency code=\"\"></currency><transactionCode code=\"\"></transactionCode><total></total><balance></balance><starTotal></starTotal><firstPayment></firstPayment><periodicalPayment></periodicalPayment><numberOfPayments></numberOfPayments><clubId></clubId><clubCode></clubCode><validation code=\"\"></validation><commReason code=\"\"></commReason><idStatus code=\"\"></idStatus><cvvStatus code=\"\"></cvvStatus><authSource code=\"\"></authSource><authNumber></authNumber><fileNumber></fileNumber><slaveTerminalNumber></slaveTerminalNumber><slaveTerminalSequence></slaveTerminalSequence><creditGroup></creditGroup><pinKeyIn></pinKeyIn><pfsc></pfsc><ptc></ptc><eci></eci><cavv code=\"\"></cavv><user></user><addonData></addonData><supplierNumber></supplierNumber><intIn>B4111111111111111C100D1150J5T0909X47</intIn><intOt></intOt><systemSpecific></systemSpecific></doDeal></response></ashrait>";
            parseResponse(response, info);
        } catch (Throwable t) {
            throw new ClearingException("Transaction failed.", t);
        }
    }

    /**
     * Create the XML string for "doDeal" request.
     *
     * @param info transaction info
     * @return New <code>String</code> with the authorize XML.
     */
    private  String createRequest(ClearingInfo info, boolean authorize) {
        boolean deposit = info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT || info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT_REROUTE;
        boolean commit =
            ((info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT && !authorize) ||
           ( info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW ||
        		   info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT ));
        boolean bookBack =
        				((deposit == false) && (info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW
        					|| info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT)
        					&& info.getOriginalDepositId() > 0);
        String cvv = "";
        if (authorize) {
        	cvv = (null != info.getCvv() && !info.getCvv().equals("") ? info.getCvv() : "2");
        }
       
        return
            "<ashrait>" +
                "<request>" +
                    "<command>doDeal</command>" +
                    "<requestId>" + (authorize ? "a" : "c") + info.getTransactionId() + "</requestId>" +
                    "<version>1001</version>" +
                    "<language>Eng</language>" +
                    "<doDeal>" +
                        "<terminalNumber>" + ((deposit || bookBack) ? depositTerminalId : withdrawTerminalId) + "</terminalNumber>" +
                        "<cardNo>" + info.getCcn() + "</cardNo>" +
                        "<cardExpiration>" + info.getExpireMonth() + info.getExpireYear() + "</cardExpiration>" +
                        "<cvv>" + cvv + "</cvv>" +
                        (!(info.getCardUserId()).equals(ConstantsBase.NO_ID_NUM)? "<id>" + info.getCardUserId() + "</id>" : ConstantsBase.EMPTY_STRING) +
                        "<transId>" + (bookBack ? info.getOriginalDepositId() : info.getTransactionId()) + "</transId>" +
                        "<transactionType>" + (deposit ? "Debit" : "Credit") + "</transactionType>" +
                        "<creditType>RegularCredit</creditType>" +
                        "<currency>" + info.getCurrencySymbol() + "</currency>" +
                        "<transactionCode>Phone</transactionCode>" +
                        (!authorize && null != info.getAuthNumber() ? "<authNumber>" + info.getAuthNumber() + "</authNumber>" : "") +
                        "<total>" + info.getAmount() + "</total>" +
                        "<validation>" + (commit ? "AutoComm" : "Verify") + "</validation>" +
                        "<user>" + info.getTransactionId() + "</user>" +
                        "<addOnData>" + info.getTransactionId() + "</addOnData>"+
                        "<mayBeDuplicate>0</mayBeDuplicate>" +
                    "</doDeal>" +
                "</request>" +
            "</ashrait>";
    }

    /**
     * Parse XOR "doDeal" result.
     *
     * @param result the result XML
     * @return <code>Hashtable</code> with result values.
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    private  void parseResponse(String response, ClearingInfo info) throws ParserConfigurationException, SAXException, IOException {
        Document respDoc = ClearingUtil.parseXMLToDocument(response);
        Element root = (Element) ClearingUtil.getNode(respDoc.getDocumentElement(), "");
        info.setResult(ClearingUtil.getElementValue(root, "result/*"));
        info.setMessage(ClearingUtil.getElementValue(root, "message/*"));
        info.setUserMessage(ClearingUtil.getElementValue(root, "userMessage/*"));
        info.setProviderTransactionId(ClearingUtil.getElementValue(root, "tranId/*"));
        info.setSuccessful(info.getResult().equals("000"));

        //verify result from xor is numeric
        try {
            Integer.parseInt(info.getResult());
        } catch (NumberFormatException nfe) {
            log.log(Level.WARN, "XOR result not a number.", nfe);
        }

        if (info.isSuccessful()) {
            info.setAuthNumber(ClearingUtil.getElementValue(root, "doDeal/authNumber/*"));
        }

        if (log.isDebugEnabled()) {
            log.debug(info.toString());
        }
    }

	@Override
	public void cancel(ClearingInfo info) throws ClearingException {
		// TODO Auto-generated method stub
		log.info("NOT IMPLEMENTED !!!");
	}
}