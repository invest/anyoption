package com.anyoption.common.clearing;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.anyoption.common.bl_vos.ClearingRoute;
import com.anyoption.common.daos.ClearingDAOBase;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.TransactionRequestResponseManager;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.payments.epg.EpgDepositRequestDetails;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;


public class ClearingManagerBase extends BaseBLManager implements ClearingManagerConstants {

    private static final Logger log = Logger.getLogger(ClearingManagerBase.class);

    private static Hashtable<Long, ClearingProvider> clearingProviders;
    private static HashMap<Long, String> clearingProvidersCCNames;
    private static HashMap<Long, String> clearingProviderNames;
    private static HashMap<Long, HashMap<Long, Long>> providersIdByPaymentGroupAndBusinessCase;
    private static Hashtable<Long, Hashtable<Long, ClearingProvider>> clearingProvidersByPaymentGroupAndBusinessCase;
    private static HashMap<Long, String> clearingProvidersDescriptors;

    public static void loadClearingProviders(Connection conn) throws ClearingException {
        if (null == clearingProviders) {
        	clearingProviders = new Hashtable<Long, ClearingProvider>();
            try {

                ArrayList<ClearingProviderConfig> l = ClearingDAOBase.loadClearingProvidersConfigs(conn);
                ClearingProviderConfig c = null;
                for (int i = 0; i < l.size(); i++) {
                    c = l.get(i);
                    try {
                        Class<?> cl = Class.forName(c.getProviderClass());
                        ClearingProvider p = (ClearingProvider) cl.newInstance();
                        p.setConfig(c);
                        clearingProviders.put(c.getId(), p);
                    } catch (Throwable t) {
                        log.error("Failed to load provider for config: " + c, t);
                    }
                }

            	providersIdByPaymentGroupAndBusinessCase = ClearingDAOBase.loadProvidersIdByPaymentGroupAndBusinessCase(conn);                
                clearingProvidersByPaymentGroupAndBusinessCase = loadClearingProvidersByPaymentGroupAndBusinessCase();
            } catch (Throwable t) {
                throw new ClearingException("Error loading clearing providers.", t);
            }
        }
    }
    
    public static ClearingRoute deposit(ClearingInfo info, boolean isManualRouted) throws ClearingException {
    	ClearingRoute cr = null;
    	if(!isManualRouted){
        	cr = findRoute(info);
        } else {
        	cr = new ClearingRoute();
        	cr.setDepositClearingProviderId(info.getProviderId());
        }
        ClearingProvider p = getClearingProviders().get(cr.getDepositClearingProviderId());
        if (null != p) {
            info.setProviderId(cr.getDepositClearingProviderId());
            try {
                if (p.isSupport3DEnrollement()) {
                    p.enroll(info);
                    if (info.isSuccessful()) {
                    	info.setIs3dSecure(true);
                    } else {
                        p.authorize(info);
                    }
                } else {
                	if (p instanceof EPGClearingProvider) {
                		EpgDepositRequestDetails depositReq = TransactionsManagerBase.getEpgDepositRequestAdditionalDetails(info);
                		info.setAdditionalRequestParams(EPGClearingProvider.getAdditionalDetails(depositReq));
                		info.setProductId(depositReq.getProductId());
                	}
                    p.authorize(info);
                    if (info.getRequestResponse() != null) {
                    	TransactionRequestResponseManager.insert(info.getRequestResponse());
                    }
                }
                return cr;
            } catch (Throwable t) {
                throw new ClearingException("Problem processing deposit.", t);
            }
        } else {
            throw new ClearingException("No route");
        }
    }

    public static void loadClearingProviders() throws ClearingException {
        if (null == clearingProviders) {
            Connection conn = null;
            try {
                conn = getConnection();
                loadClearingProviders(conn);
            } catch (ClearingException ce) {
                throw ce;
            } catch (Throwable t) {
                throw new ClearingException("Error loading clearing providers.", t);
            } finally {
                closeConnection(conn);
            }
        } else {
            log.warn("Clearing providers already loaded.");
        }
    }

    /**
     * load clearing providers by payment group and business case
     * @return cpByPaymentGroupAndBusinessCase as Hashtable<Long, Hashtable<Long, ClearingProvider>>
     */
    public static Hashtable<Long, Hashtable<Long, ClearingProvider>> loadClearingProvidersByPaymentGroupAndBusinessCase() {
    	Hashtable<Long, Hashtable<Long, ClearingProvider>> cpByPaymentGroupAndBusinessCase = new Hashtable<Long, Hashtable<Long,ClearingProvider>>();
    	Iterator<Long> iterator = getProvidersIdByPaymentGroupAndBusinessCase().keySet().iterator();
    	while (iterator.hasNext()) {
    		long businessCaseId = iterator.next();
    		if (getProvidersIdByPaymentGroupAndBusinessCase().containsKey(businessCaseId)) {
    			Hashtable<Long, ClearingProvider> ClearingProviderHT = new Hashtable<Long, ClearingProvider>();
    	    	HashMap<Long, Long> ProvidersIdhm = getProvidersIdByPaymentGroupAndBusinessCase().get(businessCaseId);
    	    	Iterator<Long> it = ProvidersIdhm.values().iterator();
    	    	while (it.hasNext()) {
    	    		long providerId = it.next();
    	    		try {
    	    			ClearingProviderHT.put(providerId, getClearingProviders().get(providerId));
    				} catch (Throwable t) {
    					log.error("Failed to load providerId : " + providerId, t);
    				}
    	    	}
    	    	cpByPaymentGroupAndBusinessCase.put(businessCaseId, ClearingProviderHT);
    		}
    	}
    	return cpByPaymentGroupAndBusinessCase;
    }
    
	/**
	 * BusinessCase as a key for outside hash map
	 * PaymentGroup as a key for inside hash map
	 * @return the providersIdByPaymentGroupAndBusinessCase
	 */
	public static HashMap<Long, HashMap<Long, Long>> getProvidersIdByPaymentGroupAndBusinessCase() {
		return providersIdByPaymentGroupAndBusinessCase;
	}

	/**
	 * BusinessCase as a key for outside hash map
	 * PaymentGroup as a key for inside hash map
	 * @param providersIdByPaymentGroupAndBusinessCase the providersIdByPaymentGroupAndBusinessCase to set
	 */
	public static void setProvidersIdByPaymentGroupAndBusinessCase(
			HashMap<Long, HashMap<Long, Long>> providersIdByPaymentGroupAndBusinessCase1) {
		providersIdByPaymentGroupAndBusinessCase = providersIdByPaymentGroupAndBusinessCase1;
	}

	/**
	 * BusinessCase as a key for outside hash map
	 * Provider id as a key for inside hash map
	 * @return the clearingProvidersByPaymentGroupAndBusinessCase
	 */
	public static Hashtable<Long, Hashtable<Long, ClearingProvider>> getClearingProvidersByPaymentGroupAndBusinessCase() {
		return clearingProvidersByPaymentGroupAndBusinessCase;
	}
	
	public static HashMap<Long, String> getClearingProviderNames() {
		if (clearingProviderNames == null) {
			Connection conn = null;
			try {
				conn = getConnection();
				clearingProviderNames = new HashMap<Long, String>();
				Map<Integer, String> providers = ClearingDAOBase.getProvidersNameMaping(conn);
				Set<Integer> keys = providers.keySet();
				for(Integer key : keys) {
					clearingProviderNames.put(key.longValue(), providers.get(key));
				}
			} catch (SQLException sqle) {
				log.error("Failed to load clearing providers names : " + sqle);
			} finally {
				closeConnection(conn);
			}
		}
		return clearingProviderNames;
	}
	
	public static HashMap<Long, String> getClearingProviderCCNames() {
		if (clearingProvidersCCNames == null) {
			Connection conn = null;
			try {
				conn = getConnection();
				clearingProvidersCCNames = new HashMap<Long, String>();
				Map<Integer, String> ccProviders = ClearingDAOBase.getCCProvidersNameMaping(conn);
				Set<Integer> keys = ccProviders.keySet();
				for(Integer key : keys) {
					clearingProvidersCCNames.put(key.longValue(), ccProviders.get(key));
				}
			} catch (SQLException sqle) {
				log.error("Failed to load CC clearing providers names : " + sqle);
			} finally {
				closeConnection(conn);
			}
		}
		return clearingProvidersCCNames;
	}

    public static Hashtable<Long, ClearingProvider> getClearingProviders() {
    	try {
    		  if (null == clearingProviders) {
    			  loadClearingProviders();
    		  }
		} catch (ClearingException e) {
			log.error("unable to load Clearing Providers", e);
		}
		return clearingProviders;
	}
    
    /**
     * Load clearing provider by id
     * @param conn
     * @param providerId  provider to load
     * @throws ClearingException
     */
    public static void loadClearingProviderById(Connection conn, long providerId) throws ClearingException {
        if (null == getClearingProviders()) {
            clearingProviders = new Hashtable<Long, ClearingProvider>();
            try {
                ClearingProviderConfig c = ClearingDAOBase.loadClearingProviderById(conn, providerId);
                    try {
                        Class<?> cl = Class.forName(c.getProviderClass());
                        ClearingProvider p = (ClearingProvider) cl.newInstance();
                        p.setConfig(c);
                        getClearingProviders().put(c.getId(), p);
                    } catch (Throwable t) {
                        log.error("Failed to load provider for config: " + c, t);
                    }
            } catch (Throwable t) {
                throw new ClearingException("Error loading clearing providers.", t);
            }
        }
    }
    
    private static boolean isWeb() {
    	return CommonUtil.getProperty(ConstantsBase.APPLICATION_SOURCE , "false").equalsIgnoreCase("web");
    }
    
    /**
     * Find clearing route to process the request through.
     *
     * @param info the request info
     * @param deposit <code>true</code> to look for deposit route, <code>false</code> for withdraw
     * @return The id of the provider to process the request through or 0 if no route.
     * @throws ClearingException
     */
    public static ClearingRoute findRoute(ClearingInfo info) throws ClearingException {
        ClearingRoute cr = new ClearingRoute();
        Connection conn = null;
        try {
            conn = getConnection();
            String bin = info.getCcn().substring(0, 6);
            cr = ClearingDAOBase.findRouteList(conn, info.getSkinId(), bin, info.getCurrencyId(), info.getCcCountryId(), info.getCcTypeId(), info.getPlatformId()).get(0);
        } catch (Throwable t) {
            throw new ClearingException("Error finding route.", t);
        } finally {
            closeConnection(conn);
        }
        return cr;
    }
    
    /**
     * Find clearing route to process the request through.
     *
     * @param info the request info
     * @param deposit <code>true</code> to look for deposit route, <code>false</code> for withdraw
     * @return The id of the provider to process the request through or 0 if no route.
     * @throws ClearingException
     */
    public static ArrayList<ClearingRoute> findAllRoutes(String bin, long skinId, long ccCountryId, long currencyId, long ccTypeId, int platformId) throws ClearingException {
        
        Connection conn = null;
        try {
            conn = getConnection();
            return ClearingDAOBase.findRouteList(conn, skinId, bin,  currencyId, ccCountryId, ccTypeId, platformId);
        } catch (Throwable t) {
            throw new ClearingException("Error finding route.", t);
        } finally {
            closeConnection(conn);
        }

    }
    
    /**
     * Process purchase request.
     *
     * @param info the request info
     * @throws ClearingException
     */
    public static void purchase(ClearingInfo info) throws ClearingException {
        ClearingProvider p = clearingProviders.get(info.getProviderId());
        if (null != p) {
            p.purchase(info);
        } else {
            throw new ClearingException("No route");
        }
    }

    public static String getProviderName(long clearingProviderId) {
        ClearingProvider p = clearingProviders.get(clearingProviderId);
        if (null != p) {
            return p.getName();
        }
        return null;
    }

    public static long getProviderNonCftAvailable(long clearingProviderId) {
        ClearingProvider p = clearingProviders.get(clearingProviderId);
        if (null != p) {
            return p.getNonCftAvailable();
        }
        return 0;
    }

    /**
     * Refund money for previous successful debit transaction.
     *
     * @param info the request info
     * @throws ClearingException
     */
    public static void bookback(ClearingInfo info) throws ClearingException {
        ClearingProvider p = clearingProviders.get(info.getProviderId());
        if (null != p) {
            p.bookback(info);
        } else {
            throw new ClearingException("No route");
        }
    }
    
    /**
     * Process withdraw request.
     *
     * @param info the request info
     * @throws ClearingException
     */
    public static void withdraw(ClearingInfo info, Connection conn) throws ClearingException {
        long providerId = findRoute(info, false, conn).getWithdrawClearingProviderId();
        ClearingProvider p = clearingProviders.get(providerId);
        if (null != p) {
            info.setProviderId(providerId);
            p.withdraw(info);
        } else {
            throw new ClearingException("No route");
        }
    }
    

    /**
     * Find clearing route to process the request through.
     *
     * @param info the request info
     * @param deposit <code>true</code> to look for deposit route, <code>false</code> for withdraw
     * @return The id of the provider to process the request through or 0 if no route.
     * @throws ClearingException
     */
    public static ClearingRoute findRoute(ClearingInfo info, boolean deposit) throws ClearingException {
    	ClearingRoute cr = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String bin = info.getCcn().substring(0, 6);
            cr = ClearingDAOBase.findRoute(conn, info.getSkinId(), bin, info.getCurrencyId(), deposit, info.getCcCountryId(), info.getCcTypeId(), info.getPlatformId());
        } catch (Throwable t) {
            throw new ClearingException("Error finding route.", t);
        } finally {
            closeConnection(conn);
        }
        return cr;
    }

    /**
     * Capture previously authorized request.
     *
     * @param info the request info
     * @throws ClearingException
     */
    public static void capture(ClearingInfo info) throws ClearingException {
        ClearingProvider p = clearingProviders.get(info.getProviderId());
        if (null != p) {
            p.capture(info);
        } else {
            throw new ClearingException("No route");
        }
    }
    
    /**
     * Find clearing route to process the request through.
     *
     * @param info the request info
     * @param deposit <code>true</code> to look for deposit route, <code>false</code> for withdraw
     * @return The id of the provider to process the request through or 0 if no route.
     * @throws ClearingException
     */
    public static ClearingRoute findRoute(ClearingInfo info, boolean deposit, Connection conn) throws ClearingException {
        ClearingRoute cr = null;
        try {
            conn = getConnection();
            String bin = info.getCcn().substring(0, 6);
            cr = ClearingDAOBase.findRoute(conn, info.getSkinId(), bin, info.getCurrencyId(), deposit, info.getCcCountryId(), info.getCcTypeId(), info.getPlatformId());
        } catch (Throwable t) {
            throw new ClearingException("Error finding route.", t);
        } finally {
            closeConnection(conn);
        }
        return cr;
    }
    
    /**
     * Process capture request with routing.
     *
     * @param routingNeeded  if true, find provider id
     * @param info the request info
     * @throws ClearingException
     */
    public static void captureWithRouting(ClearingInfo info) throws ClearingException {
        info.setProviderId(findRoute(info).getDepositClearingProviderId());
        capture(info);
    }

	public static void updateTimeLastSuccess(long id) throws SQLException {
		Connection conn = getConnection();
		try{
			ClearingDAOBase.updateTimeLastSuccess(conn, id);
		} finally {
			closeConnection(conn);
		}
	}
    
    public static String getClearingProviderDescriptor (Long providerId) {
    	if (providerId == null) {
    		return null;
    	} 
		switch ((int) providerId.longValue()) {
			case 1:
				return "ODL*ANYOPTION.COM";
			case 2:
				return "ODL*ANYOPTION.COM";
			case 3:
				return "Copyop binary options";
			case 4:
				return "ODTL*ANYOPTION.COM";
		}
		return null;
    }
    
    /**
     * Neteller deposit
     * @param info
     * @throws ClearingException
     */
    public static void netellerDeposit(ClearingInfo info, String currency, String email, String verificationCode) throws ClearingException {
    	ClearingProvider p = getClearingProviders().get(65l);
    	if (null != p) {
    		info.setProviderId(65l);
    		try { 
    			((NetellerClearingProvider)p).deposit(info, currency, email, verificationCode); 
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing netellerDeposit.", t);
            }
    	} else {
            throw new ClearingException("Can't load neteller provider");
        }
    }
}
