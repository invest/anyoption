package com.anyoption.common.clearing;

import java.math.BigDecimal;
import java.net.URL;
import java.util.Arrays;

import org.apache.axis.types.UnsignedByte;
import org.apache.log4j.Logger;

import com.anyoption.common.clearing.payvision.PayVisionConvertor;
import com.anyoption.common.clearing.payvision.gatewayV2.BasicOperations;
import com.anyoption.common.clearing.payvision.gatewayV2.BasicOperationsServiceLocator;
import com.anyoption.common.clearing.payvision.gatewayV2.ThreeDSecure;
import com.anyoption.common.clearing.payvision.gatewayV2.ThreeDSecureServiceLocator;
import com.anyoption.common.clearing.payvision.gatewayV2.entities.CdcEntry;
import com.anyoption.common.clearing.payvision.gatewayV2.entities.CdcEntryItem;
import com.anyoption.common.clearing.payvision.gatewayV2.entities.EnrollmentResult;
import com.anyoption.common.clearing.payvision.gatewayV2.entities.TransactionResult;

public class PayVisionClearingProvider extends ClearingProvider {
	private static final Logger log = Logger.getLogger(PayVisionClearingProvider.class);
	
	final static String BASIC_SERVICE_SUFFIX = "BasicOperationsService.svc";
	final static String THREED_SERVICE_SUFFIX = "ThreeDSecureService.svc";
	
	
	@Override
	public void authorize(ClearingInfo info) throws ClearingException {
		try {
			BasicOperationsServiceLocator bosl = new BasicOperationsServiceLocator();
			BasicOperations bo = bosl.getBasicHttpBinding_BasicOperations(new URL(url + BASIC_SERVICE_SUFFIX));
			TransactionResult result = bo.authorize(
					Long.parseLong(password),										// member Id
					username,														// member Guid 
					PayVisionConvertor.aoCountryToIso(info.getCountryA2()),			// country Id 
					new BigDecimal(info.getAmount()).divide(new BigDecimal(100)),	// amount 
					PayVisionConvertor.aoCurrencyToIso((int) info.getCurrencyId()),	// currency Id 
					""+info.getTransactionId(),								// tracking member code, 
					info.getCcn(),  												// card number
					info.getFirstName() + " " + info.getLastName(),					// card holder
					new UnsignedByte(info.getExpireMonth()),						// expire month
					new Short("20"+info.getExpireYear()),							// expire year
					info.getCvv(),  												// cvv
					Integer.parseInt(depositTerminalId),							// merchantAccountType
					null, null, null, null, null // Not used : dbaName, dbaCity, avsAddress, avsZip, additionalInfo
					);
			
			log.debug("authorize result:" + result.getResult() +" msg:" + result.getMessage()+" tid:"+result.getTrackingMemberCode());
			
			if(result.getResult() == 0 ) {
				info.setSuccessful(true);
				info.setAcquirerResponseId(""+result.getTransactionId());
				info.setAuthNumber(result.getTransactionGuid());
				info.setMessage("Authorized");
				info.setResult("1000");
				info.setUserMessage("");
			} else {
				log.error(Arrays.toString(result.getCdc())); 
				info.setSuccessful(false);
				info.setProviderTransactionId(""+result.getTransactionId());
				info.setResult(String.valueOf(result.getResult()));
				info.setMessage(result.getMessage());
				info.setUserMessage("");
			}
			
		} catch (Exception e) {
			log.error("Unable to authorize", e);
			info.setSuccessful(false);
			throw new ClearingException();
		} 
	}

	@Override
	public void capture(ClearingInfo info) throws ClearingException {
		try {
			BasicOperationsServiceLocator bosl = new BasicOperationsServiceLocator();
			BasicOperations bo = bosl.getBasicHttpBinding_BasicOperations(new URL(url + BASIC_SERVICE_SUFFIX));
			TransactionResult result = bo.capture(
					Long.parseLong(password),										// member Id
					username,														// member Guid 
					Long.parseLong(info.getAcquirerResponseId()),					// transactionId
					info.getAuthNumber(),											// transaction guid
					new BigDecimal(info.getAmount()).divide(new BigDecimal(100)),	// amount
					PayVisionConvertor.aoCurrencyToIso((int) info.getCurrencyId()),	// currency Id
					""+info.getTransactionId()+"-2",								// trackingMemberCode
					null // Not used : additionalInfo
					);
			
			log.debug("capture result:" + result.getResult() +" msg:" + result.getMessage()+" tid:"+result.getTrackingMemberCode());
			
			if(result.getResult() == 0 ) {
				info.setSuccessful(true);
				info.setProviderTransactionId(""+result.getTransactionId());
				info.setMessage("Captured");
				info.setAuthNumber(result.getTransactionGuid());
				info.setResult("1000");
				info.setUserMessage("");
			} else {
				log.error(Arrays.toString(result.getCdc()));
				info.setSuccessful(false);
				info.setProviderTransactionId(""+result.getTransactionId());
				info.setMessage(result.getMessage());
				info.setResult(String.valueOf(result.getResult()));
				info.setUserMessage("");
			}
			
		} catch (Exception e) {
			log.error("Unable to capture", e);
			info.setSuccessful(false);
			throw new ClearingException();
		} 
	}

	@Override
	public void cancel(ClearingInfo info) throws ClearingException {
		try {
			BasicOperationsServiceLocator bosl = new BasicOperationsServiceLocator();
			BasicOperations bo = bosl.getBasicHttpBinding_BasicOperations(new URL(url + BASIC_SERVICE_SUFFIX));
			TransactionResult result = bo._void(
					Long.parseLong(password),										// member Id
					username,														// member Guid 
					Long.parseLong(info.getAcquirerResponseId()),					// transactionId
					info.getAuthNumber(),											// transaction guid
					""+info.getTransactionId(),										// trackingMemberCode
					null // Not used : additionalInfo
					);
			
			log.debug("capture result:" + result.getResult() +" msg:" + result.getMessage()+" tid:"+result.getTrackingMemberCode());
			
			if(result.getResult() == 0 ) {
				info.setSuccessful(true);
				info.setProviderTransactionId(""+result.getTransactionId());
				info.setMessage("Canceled");
				info.setResult("1000");
				info.setUserMessage("");
			} else {
				log.error(Arrays.toString(result.getCdc()));
				info.setSuccessful(false);
				info.setProviderTransactionId(""+result.getTransactionId());
				info.setMessage(result.getMessage());
				info.setResult(String.valueOf(result.getResult()));
				info.setUserMessage("");
			}
			
		} catch (Exception e) {
			log.error("Unable to cancel", e);
			info.setSuccessful(false);
			throw new ClearingException();
		} 	
	}

	@Override
	public void enroll(ClearingInfo info) throws ClearingException {
		try {
			ThreeDSecureServiceLocator threeDsl = new ThreeDSecureServiceLocator();
			ThreeDSecure threeDsecure = threeDsl.getBasicHttpBinding_ThreeDSecure(new URL(url + THREED_SERVICE_SUFFIX));
			EnrollmentResult result = threeDsecure.checkEnrollment(
					Long.parseLong(password),										// member Id
					username,														// member Guid 
					new BigDecimal(info.getAmount()).divide(new BigDecimal(100)),	// amount
					PayVisionConvertor.aoCurrencyToIso((int) info.getCurrencyId()),	// currency Id
					""+info.getTransactionId(),										// TrackingMemberCode
					info.getCcn(),  												// card number
					info.getFirstName() + " " + info.getLastName(),					// card holder
					new UnsignedByte(info.getExpireMonth()),						// expire month
					new Short("20"+info.getExpireYear()),  							// expire year
					null															// additionalInfo
					);
			
			CdcEntry[] cdcs = result.getCdc();
			boolean enrolled = false;
			String mpr = "";
			for(CdcEntry cdcEntry: cdcs){
				if(cdcEntry.getName().equals("MerchantPluginInformation")) {
					CdcEntryItem[] items = cdcEntry.getItems();
					for(CdcEntryItem item :items) {
						if(item.getKey().equals("MerchantPluginResult")) {
							mpr = item.getValue();
							if(item.getValue().equals("Y")) {
								enrolled = true;
								log.debug("enrolled successful");
							}
						}
					}
				}
			}
			
			log.debug("enroll result:" + result.getResult() +" msg:" + result.getMessage()+" tid:"+result.getTrackingMemberCode());
			
			if(result.getResult() == 0 && enrolled) {
				info.setSuccessful(true);
				info.setPaReq(result.getPaymentAuthenticationRequest());
				info.setAcsUrl(result.getIssuerUrl());
				info.setTransactionId(Long.parseLong(result.getTrackingMemberCode()));
				info.setAcquirerResponseId(String.valueOf(result.getEnrollmentId()));
				info.setProviderTransactionId("");
				info.setMessage("Enrolled");
				info.setUserMessage("");
				info.setResult("1000");
			} else {
				log.error(Arrays.toString(result.getCdc()));
				info.setSuccessful(false);
				info.setTransactionId(Long.parseLong(result.getTrackingMemberCode()));
				info.setMessage(result.getMessage()+" mpr:"+mpr);
				info.setResult(String.valueOf(result.getResult()));
				info.setUserMessage(""); 
				info.setProviderTransactionId("");
			}
		} catch (Exception e) {
			log.error("Unable to enroll", e);
			info.setSuccessful(false);
			throw new ClearingException();
		}
	}

	@Override
	public void purchase(ClearingInfo info) throws ClearingException {
		try {
			ThreeDSecureServiceLocator threeDsl = new ThreeDSecureServiceLocator();
			ThreeDSecure threeDsecure = threeDsl.getBasicHttpBinding_ThreeDSecure(new URL(url + THREED_SERVICE_SUFFIX));
			TransactionResult result = threeDsecure.paymentUsingIntegratedMPI(
					Long.parseLong(password),										// member Id
					username,														// member Guid 
					PayVisionConvertor.aoCountryToIso(info.getCountryA2()),			// country Id 
					""+info.getTransactionId(),										// tracking member code   
					null, 
					Integer.parseInt(depositTerminalId),							// merchantAccountType
					null, 															// dbaName 
					null, 															// dbaCity 
					null, 															// avsAddress 
					null, 															// avsZip 
					Long.valueOf(info.getAcquirerResponseId()),						// enrollmentId, 
					String.valueOf(info.getTransactionId()),						// enrollmentTrackingMemberCode, 
					String.valueOf(info.getPaRes()), 								// payerAuthenticationResponse, 
					null															// additionalInfo
					);

			CdcEntry[] cdcs = result.getCdc();
			boolean authenticated = false;
			String bankMessage = "";
			for(CdcEntry cdcEntry: cdcs){
				if(cdcEntry.getName().equals("MerchantPluginInformation")) {
					CdcEntryItem[] items = cdcEntry.getItems();
					for(CdcEntryItem item :items) {
						if(item.getKey().equals("OperationResult")) {
							 if(item.getValue().equals("0") ) {
								 authenticated = true;
								 log.debug("purchase authenticated");
							 }
						}
						if(item.getKey().equals("BankMessage")) {
							bankMessage = bankMessage + " "+item.getValue();
						}
						if(item.getKey().equals("MerchantPluginMessage")) {
							bankMessage = bankMessage + " " + item.getValue(); 
						}
					}
				}
			}

			log.debug("purchase result:" + result.getResult() +" msg:" + result.getMessage()+" tid:"+result.getTrackingMemberCode());
			
			if(result.getResult() == 0 && authenticated) {
				info.setSuccessful(true);
				info.setProviderTransactionId(""+result.getTransactionId());
				info.setCaptureNumber(result.getTransactionGuid());
				info.setMessage("Purchased");
				info.setUserMessage("");
				info.setResult("1000");
			} else {
				log.error(Arrays.toString(result.getCdc()));
				info.setSuccessful(false);
				info.setProviderTransactionId(""+result.getTransactionId());
				info.setMessage(result.getMessage()+"|" + bankMessage);
				info.setUserMessage("");
				info.setResult(String.valueOf(result.getResult()));
			}
		} catch (Exception e) {
			log.error("Unable to purchase", e);
			info.setSuccessful(false);
			throw new ClearingException();
		}
	}

	@Override
	public void withdraw(ClearingInfo info) throws ClearingException {
		try {
			BasicOperationsServiceLocator bosl = new BasicOperationsServiceLocator();
			BasicOperations bo = bosl.getBasicHttpBinding_BasicOperations(new URL(url + BASIC_SERVICE_SUFFIX));
			TransactionResult result = bo.cardFundTransfer(
					Long.parseLong(password),										// member Id
					username,														// member Guid 
					PayVisionConvertor.aoCountryToIso(info.getCountryA2()),			// country Id 
					new BigDecimal(info.getAmount()).divide(new BigDecimal(100)),	// amount
					PayVisionConvertor.aoCurrencyToIso((int) info.getCurrencyId()),	// currency Id
					""+info.getTransactionId(),										// TrackingMemberCode
					info.getCcn(),  												// card number
					info.getFirstName() + " " + info.getLastName(),					// card holder
					new UnsignedByte(info.getExpireMonth()),						// expire month
					new Short("20"+info.getExpireYear()),  								// expire year
					info.getCvv(),  												// cvv
					Integer.parseInt(depositTerminalId),							// merchantAccountType
					null, null, null, null, null // Not used : dbaName, dbaCity, avsAddress, avsZip, additionalInfo
					);

			log.debug("withdraw result:" + result.getResult() +" msg:" + result.getMessage()+" tid:"+result.getTrackingMemberCode());
			
			if(result.getResult() == 0 ) {
				info.setSuccessful(true);
				info.setProviderTransactionId(""+result.getTransactionId());
				info.setMessage("Withdrawn");
				info.setResult("1000");
				info.setUserMessage("");
			} else {
				log.error(Arrays.toString(result.getCdc()));
				info.setSuccessful(false);
				info.setProviderTransactionId(""+result.getTransactionId());
				info.setMessage(result.getMessage());
				info.setResult(String.valueOf(result.getResult()));
				info.setUserMessage("");
			}
			
		} catch (Exception e) {
			log.error("Unable to withdraw", e);
			info.setSuccessful(false);
			throw new ClearingException();
		} 
		
	}

	@Override
	public void bookback(ClearingInfo info) throws ClearingException {
		try {
			BasicOperationsServiceLocator bosl = new BasicOperationsServiceLocator();
			BasicOperations bo = bosl.getBasicHttpBinding_BasicOperations(new URL(url + BASIC_SERVICE_SUFFIX));
			TransactionResult result = bo.refund(
					Long.parseLong(password),										// member Id
					username,														// member Guid 
					new Long(info.getProviderDepositId()),							// transactionId 
					info.getCaptureNumber(),										// transactionGuid,
					new BigDecimal(info.getAmount()).divide(new BigDecimal(100)),	// amount
					PayVisionConvertor.aoCurrencyToIso((int) info.getCurrencyId()),	// currency Id
					""+info.getTransactionId(),										// trackingMemberCode
					null // Not used : additionalInfo
					);

			log.debug("bookback result:" + result.getResult() +" msg:" + result.getMessage()+" tid:"+result.getTrackingMemberCode());
			
			if(result.getResult() == 0 ) {
				info.setSuccessful(true);
				info.setProviderTransactionId(""+result.getTransactionId());
				info.setMessage("Bookbacked");
				info.setResult("1000");
				info.setUserMessage("");
			} else {
				log.error(Arrays.toString(result.getCdc()));
				info.setSuccessful(false);
				info.setProviderTransactionId(""+result.getTransactionId());
				info.setMessage(result.getMessage());
				info.setResult(String.valueOf(result.getResult()));
				info.setUserMessage("");
			}
			
		} catch (Exception e) {
			log.error("Unable to bookback", e);
			info.setSuccessful(false);
			throw new ClearingException();
		} 
		
	}

}
