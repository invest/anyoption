package com.anyoption.common.clearing;


public interface ClearingManagerConstants {
	
    public static final long LIMITATION_TYPE_CC_COUNTRY = 1;
    public static final long LIMITATION_TYPE_CURRNECY = 2;
    public static final long LIMITATION_TYPE_CC_TYPE = 3;
    public static final long LIMITATION_TYPE_BIN = 4 ;
    

    
	public static final long XOR_ET_PROVIDER_ID = 1;
    public static final long XOR_AO_PROVIDER_ID = 2;
    public static final long WC_PROVIDER_ID = 3;
    public static final long WC_3D_PROVIDER_ID = 4;
    public static final long INATEC_ONLINE_PROVIDER_ID = 6;
    public static final long ENVOY_PROVIDER_ID = 7;
    public static final long PAYPAL_PROVIDER_ID = 8;
    public static final long INATEC_PROVIDER_ID_CFT = 9;  // Inatec provider for CFT
    public static final long ACH_PROVIDER_ID = 10;
    public static final long CASHU_PROVIDER_ID = 11;
    public static final long INATEC_PROVIDER_ID_USD = 12;  // Inatec provider for deposits & bookback USD
    public static final long INATEC_PROVIDER_ID_EUR = 13;  // Inatec provider for deposits & bookback EUR
    public static final long INATEC_PROVIDER_ID_GBP = 14;  // Inatec provider for deposits & bookback GBP
    public static final long INATEC_PROVIDER_ID_TRY = 15;  // Inatec provider for deposits & bookback TRY
    public static final long UKASH_PROVIDER_ID = 16;
    public static final long MONEYBOOKERS_PROVIDER_ID = 17;
    public static final long WEBMONEY_PROVIDER_ID_USD = 18; // WebMoney provider for deposits USD
    public static final long WEBMONEY_PROVIDER_ID_EUR = 19; // WebMoney provider for deposits EUR
    public static final long WEBMONEY_PROVIDER_ID_RUB = 20; // WebMoney provider for deposits RUB
    public static final long DELTAPAY_CHINA_PROVIDER_ID = 22; //delta pay china
    public static final long TESTING_PROVIDER_ID = 25;
    public static final long DELTAPAY_CHINA_TEST_PROVIDER_ID = 1022; // for testing purposes
    public static final long TBI_AO_PROVIDER_ID = 23; //TBI clearing provider
    public static final long TBI_AO_PROVIDER_TRY_ID = 29;
    public static final long CDPAY_PROVIDER_ID = 30;
    public static final long BAROPAY_PROVIDER_ID = 31;
    public static final long INATEC_IFRAME_PROVIDER_ID = 48;
    public static final long EPG_AO_PROVIDER_ID = 54;
    public static final long EPG_ET_PROVIDER_ID = 55;
    public static final long EPG_OUROBOROS_PROVIDER_ID = 56;
    public static final int EPG_CHECKOUT_PROVIDER_ID = 57;
    public static final long EZEEBILL_PROVIDER_ID = 58;
    
    public static final long CLEARING_GROUP_INATEC = 1;
    public static final long CLEARING_GROUP_TBI = 3;
    public static final long CLEARING_GROUP_WIRE_CARD = 2;
    public static final long CLEARING_GROUP_WIRE_CARD_3D = 4;

    public static final long CLEARING_REFERENCE_ANYOPTION_AND_OUROBOROS = 0;
    public static final long CLEARING_PAYMENT_GROUP_INATEC_ONLINE = 5;
    public static final long CLEARING_PAYMENT_GROUP_MONEYBOOKERS = 16;
    
    public static final long INATEC_PC21 = 37;
    public static final long INATEC_OUROBOROS_CC = 41;
    
    public static final long POWERPAY21_PROVIDER_ID_APS = 59;
    public static final long FIBONATIX_3D_PROVIDER_ID = 64; 
}
