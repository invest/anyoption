package com.anyoption.common.clearing;

import java.lang.reflect.Field;

import org.apache.log4j.Logger;

public class EzeebillClearingProvider extends ClearingProvider {
    private static final Logger log = Logger.getLogger(EzeebillClearingProvider.class);

    public static String returnUrl;
    public static String version;
    public static String accessId;
    public static String payType;
    public static String apiAccess;
    public static String merchId;
    public static String accessUrl;
    public static String terminalId;
    public static String action;
    public static String hashKey;
    
	@Override
	public void authorize(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Not Supported");
	}

	@Override
	public void capture(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Not Supported");
	}

	@Override
	public void cancel(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Not Supported");	}

	@Override
	public void enroll(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Not Supported");	}

	@Override
	public void purchase(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Not Supported");	}

	@Override
	public void withdraw(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Not Supported");	}

	@Override
	public void bookback(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Not Supported");	}
	
    public void setConfig(ClearingProviderConfig config) throws ClearingException {
        id = config.getId();
        accessUrl = config.getUrl();
        terminalId= config.getTerminalId();
        username = config.getUserName();
        password = config.getPassword();
        name = config.getName();
        nonCftAvailable = config.getNonCftAvailable();
        isActive = config.isActive();
        hashKey = config.getPrivateKey();
        
        String propsString = config.getProps();
        String[] props = propsString.split(";");
        for(int i =0 ;i<props.length;i++){
        	String[] p = props[i].split("=");
        	try {
				Field f = this.getClass().getField(p[0]);
				f.set(this, p[1]);
			} catch (Exception e) {
				log.error("Unrecognized field:"+p[0]);
			} 
        }
        log.info(toString());
    }

    public String toString() {
    	String ls = System.getProperty("line.separator");
    	return "id: " + id + ls +
        "accessUrl: " + accessUrl + ls +
        "action: " + action + ls +
        "terminalId: " + terminalId + ls +
        "withdrawTerminalId: " + withdrawTerminalId + ls +
        "returnURL: " + returnUrl + ls +
        "version: " + version + ls +
        "access_id: " + accessId + ls +
        "pay_type: " + payType + ls +
        "api_access: " + apiAccess + ls +
        "merch_id: " + merchId + ls ;
    }
}
