package com.anyoption.common.clearing;

import org.apache.log4j.Logger;

import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingProvider;

/**
 * @author liors
 *
 */
public class InatecIframeClearingProvider extends ClearingProvider {
	private static final Logger log = Logger.getLogger(InatecIframeClearingProvider.class);

	@Override
	public void authorize(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");
		
	}

	@Override
	public void capture(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");
		
	}

	@Override
	public void cancel(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");
		
	}

	@Override
	public void enroll(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");
		
	}

	@Override
	public void purchase(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");
		
	}

	@Override
	public void withdraw(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");
		
	}

	@Override
	public void bookback(ClearingInfo info) throws ClearingException {
		
	}

	public void setProviderDetails(InatecIframeInfo info) {
    	info.setRedirectURL(url);
    	info.setMerchantAccount(username); //merchantId
    	//info.setMerchant_id(password); // no password
    	info.setPrivateKey(privateKey); //secret
    }
}
