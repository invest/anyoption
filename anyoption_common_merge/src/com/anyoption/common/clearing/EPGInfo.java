package com.anyoption.common.clearing;

import com.anyoption.common.beans.User;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.jboss.logging.Logger;
import com.anyoption.common.beans.Transaction;

/**
 * @author LioR SoLoMoN
 *
 */
public class EPGInfo extends ClearingInfo implements Serializable {
	
	private static final long serialVersionUID = -3537132026479492275L;
	private static final Logger logger = Logger.getLogger(EPGInfo.class);
	private String expressCheckoutURL; //for payment solutions
	private String tokenizeURL;
	private String merchantId;
	private String operationType;
	private String format;
	private long transactionStatusId;
	
	public EPGInfo() {
		
	}
	public EPGInfo(User user, Transaction t, String amount, String prefixURL) {
		super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId());
		if (userDOB == null) {
			try {
				userDOB = new SimpleDateFormat("dd-MM-yyyy").parse("14-12-1984");
				user.setTimeBirthDate(userDOB);
			} catch (ParseException e) {
				logger.error("EPGInfo, ", e);
			}	
		}
		this.homePageUrl = prefixURL;
	}

	/**
	 * @return the expressCheckoutURL
	 */
	public String getExpressCheckoutURL() {
		return expressCheckoutURL;
	}

	/**
	 * @param expressCheckoutURL the expressCheckoutURL to set
	 */
	public void setExpressCheckoutURL(String expressCheckoutURL) {
		this.expressCheckoutURL = expressCheckoutURL;
	}

	/**
	 * @return the tokenizeURL
	 */
	public String getTokenizeURL() {
		return tokenizeURL;
	}

	/**
	 * @param tokenizeURL the tokenizeURL to set
	 */
	public void setTokenizeURL(String tokenizeURL) {
		this.tokenizeURL = tokenizeURL;
	}

	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return the transactionStatusId
	 */
	public long getTransactionStatusId() {
		return transactionStatusId;
	}

	/**
	 * @param transactionStatusId the transactionStatusId to set
	 */
	public void setTransactionStatusId(long transactionStatusId) {
		this.transactionStatusId = transactionStatusId;
	}
}
