package com.copyop.migration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.SortedSet;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.util.CommonUtil;
import com.copyop.common.Constants;
import com.copyop.common.dao.InvestmentDAO;
import com.copyop.common.dao.NicknameDAO;
import com.copyop.common.dao.ProfileStatisticsDAO;
import com.copyop.common.dao.RiskAppetiteDAO;
import com.copyop.common.dto.CopiedInvestment;
import com.copyop.common.dto.Profile;
import com.copyop.common.managers.HitManager;
import com.copyop.common.managers.ManagerBase;
import com.copyop.common.managers.ProfileManager;
import com.copyop.common.managers.ProfileStatisticsManager;
import com.copyop.migration.data.InvestmentData;
import com.copyop.migration.data.UserData;
import com.datastax.driver.core.Session;


public class ProfilesMigrationManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(ProfilesMigrationManager.class);

	static String avatarPrefix = "https://www.copyop.com/jsonService/avatar/";
	static String avatarSuffix = ".png";
    static Random rand = new Random();



private final static String tradesHistorySQL = "SELECT " +
															"trunc(time_created) t, " +
															"count(*) c " +
												"FROM " +
															"investments " +
												"WHERE " +
															"user_id = ? " +
															"AND copyop_type_id != 1 " +
															"AND trunc(time_created) >=  trunc(SYSDATE -28) " +
															"GROUP BY trunc(time_created) " +
															"ORDER BY trunc(time_created) desc";

private final static String assetsResults = "SELECT " +
														"o.market_id market, " +
														"sum(i.win -i.lose) suma " +
											"FROM " +
														"investments i " +
														",opportunities o " +
														",users u " +
											"WHERE " +
														"u.id = ? " +
														"AND u.id =	i.user_id " +
														"AND i.is_canceled = 0 " +
														"AND i.copyop_type_id != 1 " +
														"AND (o.scheduled = 1 OR i.is_like_hourly  = 1 ) " +
														"AND o.opportunity_type_id = 1 " +
														"AND i.opportunity_id = o.ID " +
														"GROUP BY market_id " +
														"ORDER BY suma desc " ;

private final static String loadInvestmentDataListSql =
														"SELECT * FROM ( " +
																			"SELECT " +
																						"i.id " +
																						",i.amount " +
																						",i.win " +
																						",i.lose " +
																						",i.time_created " +
																						",o.market_id " +
																			"FROM " +
																						"investments i " +
																						",opportunities o " +
																						",users u " +
																			"WHERE " +
																						"i.user_id = ?  " +
																						"AND u.id =	i.user_id " +
																						"AND i.is_canceled	= 0 " +
																						"AND i.copyop_type_id != 1 " +
																						"AND (o.scheduled = 1 OR i.is_like_hourly  = 1 ) " +
																						"AND o.opportunity_type_id = 1 " +
																						"AND i.opportunity_id = o.id " +
																						"AND i.time_created	>= u.time_created " +
																			"ORDER BY i.id DESC " + // latest first in the list
																		") WHERE ROWNUM < 121";

	static void importProfile(UserData user) {
			Connection connection = null;
			try{
				connection = getConnection();
				long userId = user.getId();
				String nickName = NicknameDAO.getUnunsedNicknameMarkAsUsed(connection, userId);
				ArrayList<InvestmentData> investmentDataList = loadInvestmentDataList(connection, userId);
				HashMap<Date, Integer> tradesHistory = loadTradesHistory(connection, userId);
				HashMap<Long, Long> assetsResult = loadAssetsResults(connection, user, userId);

				Profile profile = new Profile();
				profile.setFrozen(user.isFrozen());
				profile.setUserId(user.getId());
				profile.setNickname(nickName);
				profile.setTest(user.isTest());
				profile.setAvatar(generateAvatar(user.getGender()));
				profile.setRiskAppetite(new ArrayList<Float>(0));
				profile.setTimeUserCreated(user.getTimeCreated());
				profile.setBestStreak(user.getBestStreak());
				profile.setBestStreakAmount(user.getBestStreakAmount());
				profile.setAssetsResults(assetsResult);
				profile.setBestAsset(user.getBestAsset());

				if(investmentDataList != null && investmentDataList.size() >0) {
					int currentStreak = 0;
					int currentStreakAmount = 0;
					if(investmentDataList.get(0).getResult()) {
						for(int k =0;k<investmentDataList.size();k++) {
							if(!investmentDataList.get(k).getResult()) {
								break;
							}
							currentStreak ++;
							currentStreakAmount += investmentDataList.get(k).getResultAmount();
						}
					}

					ArrayList<Boolean> invResultSelf = new ArrayList<Boolean>();
					for(InvestmentData investmentData: investmentDataList) {
						invResultSelf.add(investmentData.getResult());
						ProfileStatisticsManager.insertProfileInvResultSelf(investmentData.getResult(), investmentData.getMarketId(), user.getId());
					}
					// calculate the hit rate
					int frequency = Collections.frequency(invResultSelf, Boolean.TRUE);
					float hitRate = ((float)frequency/invResultSelf.size());

					// list of the last three investments
					ArrayList<Long> lastInvestments = new ArrayList<Long>();
					if(investmentDataList.size()>2) {
						lastInvestments.add(investmentDataList.get(0).getId());
						lastInvestments.add(investmentDataList.get(1).getId());
						lastInvestments.add(investmentDataList.get(2).getId());
					} else {
						for(int i =0;i<investmentDataList.size();i++) {
							lastInvestments.add(investmentDataList.get(i).getId());
						}
					}
					profile.setCurrentStreak(currentStreak);
					profile.setCurrentStreakAmount(currentStreakAmount);
					profile.setHitRate(hitRate);
					profile.setLastInvestments(lastInvestments);
				}

				ProfileManager.insertProfile(profile);
				updateTradesHistory(tradesHistory, userId);

				try {
					HitManager.insertHitRate(user.getId(), profile.getHitRatePercentage(), 0 );
					RiskAppetiteDAO.insertRiskAppetite(connection, user.getId());
				} catch (SQLException e) {
					logger.debug("Can't update hit rates to oracle for user: "+user.getId(), e);
				}
			} catch (SQLException e1) {
				logger.error("Error while importing "+ user.getId());
			} finally {
				closeConnection(connection);
			}

	}

	static void updateTradesHistory(Map<Date, Integer> tradesHistory, long userId) {
		// save trades history
		long today = new Date().getTime();
		SimpleDateFormat formatter = new SimpleDateFormat(Constants.TRADES_HISTORY_DATE_FORMAT);
		for (Map.Entry<Date, Integer> entry : tradesHistory.entrySet()) {
			int ttl = (int) (TimeUnit.DAYS.toSeconds(29) - (TimeUnit.MILLISECONDS.toSeconds(today) -  TimeUnit.MILLISECONDS.toSeconds(entry.getKey().getTime())));
			if(ttl < 0 ) {
				logger.debug("TTL problem :"+userId + " "+formatter.format(entry.getKey())+" "+ entry.getValue());
				ttl = (int) TimeUnit.DAYS.toSeconds(1);
			}
			if( TimeUnit.SECONDS.toDays(ttl)>30) {
				logger.debug("TTL problem :"+userId + " "+  formatter.format(entry.getKey())+" "+ entry.getValue());
				ttl = (int) TimeUnit.DAYS.toSeconds(28);
			}
			ProfileManager.updateTradesHistory(ttl, userId, formatter.format(entry.getKey()), entry.getValue());
		}
	}

	static void updateProfileStatistics(Connection connection, long userId, int bestStreak, int bestStreakAmount) {
		boolean close = false;
		try{
			if(connection == null) {
				connection = getConnection();
				close = true;
			}

			ArrayList<InvestmentData> investmentDataList = loadInvestmentDataList(connection, userId);
			HashMap<Date, Integer> tradesHistory = loadTradesHistory(connection, userId);
			HashMap<Long, Long> assetsResult = loadAssetsResults(connection, null, userId);

			SortedSet<Map.Entry<Long, Long>> sortedAssetsResults =  CommonUtil.entriesSortedByValues(assetsResult);
			Long bestAsset = 0l ;
			if(sortedAssetsResults.size() > 0) {
			 bestAsset = sortedAssetsResults.last().getKey();
			}

			int currentStreak = 0;
			int currentStreakAmount = 0;
			float hitRate = 0;
			// list of the last three investments
			ArrayList<Long> lastInvestments = new ArrayList<Long>();
			if(investmentDataList != null && investmentDataList.size() >0) {

				if(investmentDataList.get(0).getResult()) {
					for(int k =0;k<investmentDataList.size();k++) {
						if(!investmentDataList.get(k).getResult()) {
							break;
						}
						currentStreak ++;
						currentStreakAmount += investmentDataList.get(k).getResultAmount();
					}
				}

				ArrayList<Boolean> invResultSelf = new ArrayList<Boolean>();
				ProfileStatisticsManager.deleteProfileInvResultSelf(userId);
				for(InvestmentData investmentData: investmentDataList) {
					invResultSelf.add(investmentData.getResult());
					ProfileStatisticsManager.insertProfileInvResultSelf(investmentData.getResult(), investmentData.getMarketId(), userId);
				}
				// calculate the hit rate
				int frequency = Collections.frequency(invResultSelf, Boolean.TRUE);
				hitRate = ((float)frequency/invResultSelf.size());

				if(investmentDataList.size()>2) {
					lastInvestments.add(investmentDataList.get(0).getId());
					lastInvestments.add(investmentDataList.get(1).getId());
					lastInvestments.add(investmentDataList.get(2).getId());
				} else {
					for(int i =0;i<investmentDataList.size();i++) {
						lastInvestments.add(investmentDataList.get(i).getId());
					}
				}
			}

			ProfileManager.updateProfileLastInvestments(lastInvestments, userId);
			ProfileManager.updateProfile(userId, currentStreak, currentStreakAmount, bestStreak, bestStreakAmount, assetsResult, bestAsset, hitRate);
			updateTradesHistory(tradesHistory, userId);
		} catch (SQLException e1) {
			logger.error("Error while importing "+ userId);
		} finally {
			if(close) {
				closeConnection(connection);
			}
		}

}
	
	public static void updateCopiedInvestmentStats(CopiedInvestment ci)  {
		// add copied investment result to profile statistics
		Session session = ManagerBase.getSessionOld();
		ProfileStatisticsDAO.insertProfileInvResultCopy(session, (ci.getProfit()>0), ci.getUserId(), ci.getTimeCreated());
		
		CopiedInvestment cp = InvestmentDAO.getCopiedInvestment(session, ci.getUserId(), ci.getMarketId(), ci.getCopyFromUserId());
		if( cp == null) {
			cp = new CopiedInvestment(ci.getUserId(), ci.getCopyFromUserId(), ci.getMarketId());
		}

		cp.setProfit(cp.getProfit()+ci.getProfit());
		cp.setCopiedCount(cp.getCopiedCount()+1);
		if(ci.getProfit() >0 ) {
			cp.setCopiedCountSuccess(cp.getCopiedCountSuccess()+1);
		}
		float successRate =((float)cp.getCopiedCountSuccess()/cp.getCopiedCount());
		cp.setSuccessRate(successRate);
		// update copied investment statistics
		InvestmentDAO.updateCopiedInvestment(session, cp, cp.getCopiedCount());
	}

	private static HashMap<Long, Long> loadAssetsResults(Connection connection, UserData user, long userId) throws SQLException {
		if(connection == null){
			connection = getConnection();
		}
		HashMap<Long, Long> result = new HashMap<Long, Long>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(assetsResults);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			boolean first = true;
			while (rs.next()) {
				long marketId = rs.getLong("market");
				long sum = rs.getLong("suma");
				if(first && user != null){
					first= false;
					user.setBestAsset(marketId);
				}
				result.put(marketId, sum);
			}
		}  catch(Exception e){
			logger.info("Error" +userId, e);
		} finally {
			rs.close();
			ps.close();
		}
		return result;
	}

	private static HashMap<Date, Integer> loadTradesHistory(Connection connection, long userId) throws SQLException {
		if(connection == null){
			connection = getConnection();
		}
		HashMap<Date, Integer> result = new HashMap<Date, Integer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(tradesHistorySQL);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Date date = rs.getDate(1);
				int count = rs.getInt(2);
				result.put(date, count);
			}
		} catch(Exception e){
			logger.info("Error" +userId, e);
		} finally {
			rs.close();
			ps.close();
		}
		return result;
	}

	private static ArrayList<InvestmentData> loadInvestmentDataList(Connection connection, long userId) throws SQLException {
		if(connection == null){
			connection = getConnection();
		}
		ArrayList<InvestmentData> result = new ArrayList<InvestmentData>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(loadInvestmentDataListSql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				InvestmentData invData = new InvestmentData();
				invData.setId(rs.getLong("id"));
				invData.setAmount(rs.getLong("amount"));
				invData.setResultAmount(rs.getLong("win")-rs.getLong("lose"));
				invData.setMarketId(rs.getLong("market_id"));
				invData.setResult(rs.getLong("win")>rs.getLong("lose"));
				invData.setDateCreated(rs.getDate("time_created"));
				//invData.setBalance(rs.getLong("balance"));
				result.add(invData);
			}
		} catch(Exception e){
			logger.info("Error" +userId, e);
		} finally {
			rs.close();
			ps.close();
		}
		return result;
	}

	public static String generateAvatar(String gender){
		if(gender == null || gender.length() == 0) {
			if(randInt(0, 100) > 50){
				gender = "m";
			} else {
				gender = "f";
			}
		}

		int random = 0;
		if(gender.equalsIgnoreCase("M")){
			random = randInt(1, 100);
		}
		if(gender.equalsIgnoreCase("F")){
			random = randInt(1, 50);
		}
		return (avatarPrefix+gender.toLowerCase()+random+avatarSuffix);
	}

	public static int randInt(int min, int max) {

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
}