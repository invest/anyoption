package com.copyop.migration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class UserStatisticsManager extends UsersMigrationManager {

	private static final Logger logger = Logger.getLogger(UserStatisticsManager.class);
	
	public static void updateUserStatistics(Long userId) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
				connection = getConnection();
				long startTime = System.nanoTime();
				String exportUsersSql = getSqlForUsersExport(userId, null);
				ps = connection.prepareStatement(exportUsersSql);
				// prepare parameters
				if(userId != null ) { 
					ps.setLong(1, userId);
					ps.setLong(2, userId);
				}
				//logger.info("Start updating user : "+userId);

				rs = ps.executeQuery();
				while (rs.next()) {
					int bestStreak = rs.getInt("streak");
					int bestStreakAmount = rs.getInt("streakAmount");
					ProfilesMigrationManager.updateProfileStatistics(connection, userId, bestStreak, bestStreakAmount);
				}
				long endTime = System.nanoTime();
				long timeNanos = endTime - startTime;
				//logger.info("Statistics fix done. Took :"+ TimeUnit.NANOSECONDS.toMinutes(timeNanos)+" minutes" );
		} finally {
			closeConnection(connection);
		}
	}
}
