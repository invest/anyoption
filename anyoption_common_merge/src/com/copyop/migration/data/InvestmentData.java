package com.copyop.migration.data;

import java.util.Date;

public class InvestmentData{
	long id;
	long marketId;
	long amount;
	long balance;
	long resultAmount;
	boolean result;
	Date dateCreated;
	
	public long getMarketId() {
		return marketId;
	}
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public boolean getResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public long getBalance() {
		return balance;
	}
	public void setBalance(long balance) {
		this.balance = balance;
	}
	public long getResultAmount() {
		return resultAmount;
	}
	public void setResultAmount(long resultAmount) {
		this.resultAmount = resultAmount;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "InvestmentData [id=" + id + ", marketId=" + marketId
				+ ", amount=" + amount + ", balance=" + balance
				+ ", resultAmount=" + resultAmount + ", result=" + result
				+ ", dateCreated=" + dateCreated + "]";
	}
	
}
