/**
 *
 */
package com.copyop.common.beans;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * @author pavelhe
 *
 */
public class AppoxeeRule implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@SerializedName("field")
	protected String fieldName;
	@SerializedName("operator")
	protected String operator;
	@SerializedName("operand")
	protected List<String> operandList;

	public AppoxeeRule(String fieldName, String operator, List<String> operandList) {
		this.fieldName = fieldName;
		this.operator = operator;
		this.operandList = operandList;
	}

	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * @param fieldName the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * @param operator the operator to set
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}

	/**
	 * @return the operandList
	 */
	public List<? extends Object> getOperandList() {
		return operandList;
	}

	/**
	 * @param operandList the operandList to set
	 */
	public void setOperandList(List<String> operandList) {
		this.operandList = operandList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppoxeeRule [fieldName=").append(fieldName)
				.append(", operator=").append(operator)
				.append(", operandList=").append(operandList).append("]");
		return builder.toString();
	}

}
