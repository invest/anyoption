/**
 *
 */
package com.copyop.common.beans;

import java.io.Serializable;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

/**
 * @author Simeon
 *
 */
public class AppoxeeMessagesData implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String alias;
	
	@SerializedName("push_body")
	private String pushBody;
	private Map<String, String> payload;

	public AppoxeeMessagesData(String alias, String pushBody, Map<String, String> payload) {
		this.alias = alias;
		this.pushBody = pushBody;
		this.payload = payload;
	}

	/**
	 * @return the pushBody
	 */
	public String getPushBody() {
		return pushBody;
	}
	/**
	 * @param pushBody the pushBody to set
	 */
	public void setPushBody(String pushBody) {
		this.pushBody = pushBody;
	}
	/**
	 * @return the applicationId
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * @param applicationId the applicationId to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * @return the payload
	 */
	public Map<String, String>  getPayload() {
		return payload;
	}
	/**
	 * @param payload the rules to payload
	 */
	public void setPayload(Map<String, String> payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppoxeeMessagesData [alias=").append(alias)
				.append(", pushBody=").append(pushBody)
				.append(", getPayload=").append(payload).append("]");
		return builder.toString();
	}

}
