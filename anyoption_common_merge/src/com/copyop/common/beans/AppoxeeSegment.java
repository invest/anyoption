/**
 *
 */
package com.copyop.common.beans;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * @author pavelhe
 *
 */
public class AppoxeeSegment implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	private String description;

	@SerializedName("application_id")
	private Long applicationId;

	private List<? extends AppoxeeRule> rules;

	public AppoxeeSegment(String name, String description, Long applicationId, List<? extends AppoxeeRule> rules) {
		this.name = name;
		this.description = description;
		this.applicationId = applicationId;
		this.rules = rules;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the applicationId
	 */
	public Long getApplicationId() {
		return applicationId;
	}

	/**
	 * @param applicationId the applicationId to set
	 */
	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	/**
	 * @return the rules
	 */
	public List<? extends AppoxeeRule> getRules() {
		return rules;
	}
	/**
	 * @param rules the rules to set
	 */
	public void setRules(List<? extends AppoxeeRule> rules) {
		this.rules = rules;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppoxeeSegment [name=").append(name)
				.append(", description=").append(description)
				.append(", applicationId=").append(applicationId)
				.append(", rules=").append(rules).append("]");
		return builder.toString();
	}

}
