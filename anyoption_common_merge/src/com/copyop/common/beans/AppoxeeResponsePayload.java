/**
 *
 */
package com.copyop.common.beans;

import java.io.Serializable;

/**
 * @author pavelhe
 *
 */
public class AppoxeeResponsePayload implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	protected long id;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppoxeeResponsePayload [id=").append(id).append("]");
		return builder.toString();
	}

}
