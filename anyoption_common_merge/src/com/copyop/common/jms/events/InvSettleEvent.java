package com.copyop.common.jms.events;

import java.util.Date;

import com.anyoption.common.enums.CopyOpInvTypeEnum;

public class InvSettleEvent implements CopyOpEvent{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8985000206051730959L;
	long userId;
	long currencyId;
	long investmentId;
	long srcInvestmentId;
	long marketId;
	long result;
	long win;
	long lose;
	double closingLevel;
	double rateToUSD;
	CopyOpInvTypeEnum type;
	Date settlementTime;
	
	public InvSettleEvent(long userId, long investmentId, long result, CopyOpInvTypeEnum type, long marketId,
			long win, long lose, double closingLevel, Date settlementTime, long srcInvestmentId, long currencyId, double rateToUSD) {
		super();
		this.userId = userId;
		this.investmentId = investmentId;
		this.result = result;
		this.win = win;
		this.lose = lose;
		this.closingLevel = closingLevel;
		this.settlementTime = settlementTime;
		this.type = type;
		this.marketId = marketId;
		this.srcInvestmentId = srcInvestmentId;
		this.currencyId = currencyId;
		this.rateToUSD = rateToUSD;
	}
	
	public long getUserId() {
		return userId;
	}
	public long getInvestmentId() {
		return investmentId;
	}
	public long getResult() {
		return result;
	}
	public long getWin() {
		return win;
	}
	public long getLose() {
		return lose;
	}
	public double getClosingLevel() {
		return closingLevel;
	}
	public Date getSettlementTime() {
		return settlementTime;
	}
	public CopyOpInvTypeEnum getType() {
		return type;
	}
	public long getMarketId() {
		return marketId;
	}
	public long getSrcInvestmentId() {
		return srcInvestmentId;
	}
	public double getRateToUSD() {
		return rateToUSD;
	}
	public long getCurrencyId() {
		return currencyId;
	}

	@Override
	public String toString() {
		return "InvSettleEvent [userId=" + userId + ", rateToUSD=" + rateToUSD
				+ ", currencyId=" + currencyId + ", investmentId="
				+ investmentId + ", srcInvestmentId=" + srcInvestmentId
				+ ", marketId=" + marketId + ", result=" + result + ", win="
				+ win + ", lose=" + lose + ", closingLevel=" + closingLevel
				+ ", type=" + type + ", settlementTime=" + settlementTime + "]";
	}
	
}
