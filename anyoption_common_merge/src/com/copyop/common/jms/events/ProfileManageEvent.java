package com.copyop.common.jms.events;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

import com.copyop.common.enums.base.ProfileLinkCommandEnum;

public class ProfileManageEvent implements CopyOpEvent{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5422754399645950957L;
	long userId;
	long destUserId;
	long marketId;
	long currencyId;
	double rate;
	long amount;
	int count;
	Set<Long> assets;
	ProfileLinkCommandEnum command;
	UUID timeReset;
	Date timeSettled;
	String oldNickName = null;
	String newNickName = null;
	
	ProfileManageEvent(long userId) {
		
	}
	
	public ProfileManageEvent(long userId, String oldNickName, String newNickName, ProfileLinkCommandEnum linkCommand) {
		this.userId = userId;
		this.oldNickName = oldNickName;
		this.newNickName = newNickName;
		this.command = linkCommand;
	}
	
	public ProfileManageEvent(UUID timeReset, long userId) {
		this.userId = userId;
		this.timeReset = timeReset;
		this.command = ProfileLinkCommandEnum.COINSRESET;
	}
	
	public ProfileManageEvent(long userId, long destUserId, long amount, 
			int count, Set<Long> assets, ProfileLinkCommandEnum linkCommand) {
		super();
		this.userId = userId;
		this.destUserId = destUserId;
		this.amount = amount;
		this.count = count;
		this.assets = assets;
		this.command = linkCommand;
	}
	
	public ProfileManageEvent(long userId, long destUserId,
			ProfileLinkCommandEnum command) {
		super();
		this.userId = userId;
		this.destUserId = destUserId;
		this.command = command;
	}

	public ProfileManageEvent(ProfileLinkCommandEnum linkCommand, long userId, int count) {
		this.command = linkCommand;
		this.userId = userId;
		this.count = count;
	}
	
	public ProfileManageEvent(ProfileLinkCommandEnum linkCommand, long userId) {
		this.command = linkCommand;
		this.userId = userId;
	}
	
	public ProfileManageEvent(ProfileLinkCommandEnum linkCommand) {
		this.command = linkCommand;
	}
	
	public ProfileManageEvent(long userId, long amount,long marketId,long currencyId, double rate, Date timeSettled, ProfileLinkCommandEnum linkCommand){
		this.userId =  userId;
		this.amount = amount;
		this.marketId = marketId;
		this.currencyId = currencyId;
		this.rate = rate;
		this.timeSettled = timeSettled;
		this.command = linkCommand;
	}
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getDestUserId() {
		return destUserId;
	}
	public void setDestUserId(long destUserId) {
		this.destUserId = destUserId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Set<Long> getAssets() {
		return assets;
	}
	public void setAssets(Set<Long> assets) {
		this.assets = assets;
	}
	public ProfileLinkCommandEnum getCommand() {
		return command;
	}
	public void setCommand(ProfileLinkCommandEnum linkCommand) {
		this.command = linkCommand;
	}
	public UUID getTimeReset() {
		return timeReset;
	}
	public void setTimeReset(UUID timeReset) {
		this.timeReset = timeReset;
	}
	public Date getTimeSettled() {
		return timeSettled;
	}
	public void setTimeSettled(Date timeSettled) {
		this.timeSettled = timeSettled;
	}
	public long getMarketId() {
		return marketId;
	}
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	public long getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}

	public String getOldNickName() {
		return oldNickName;
	}
	public void setOldNickName(String oldNickName) {
		this.oldNickName = oldNickName;
	}
	public String getNewNickName() {
		return newNickName;
	}
	public void setNewNickName(String newNickName) {
		this.newNickName = newNickName;
	}

	@Override
	public String toString() {
		return "ProfileManageEvent [userId=" + userId + ", destUserId="
				+ destUserId + ", marketId=" + marketId + ", currencyId="
				+ currencyId + ", rate=" + rate + ", amount=" + amount
				+ ", count=" + count + ", assets=" + assets + ", command="
				+ command + ", timeReset=" + timeReset + ", timeSettled="
				+ timeSettled + ", oldNickName=" + oldNickName
				+ ", newNickName=" + newNickName + "]";
	}

}
