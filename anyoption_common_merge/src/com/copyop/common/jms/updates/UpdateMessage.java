/**
 *
 */
package com.copyop.common.jms.updates;

import java.io.Serializable;
import java.util.Date;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.datastax.driver.core.utils.UUIDs;

/**
 * @author pavelhe
 *
 */
public class UpdateMessage extends FeedMessage implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private UpdateTypeEnum updateType;
	private Set<QueueTypeEnum> wherePosted;

	public UpdateMessage(long userId, UpdateTypeEnum updateType, Map<String, String> properties) {
		this(userId, updateType, properties, UUIDs.timeBased());
	}

	public UpdateMessage(long userId, UpdateTypeEnum updateType, Map<String, String> properties, UUID timeCreated) {
		this.userId = userId;
		this.updateType = updateType;
		this.properties = properties;
		this.setTimeCreatedUUID(timeCreated);
		this.wherePosted = EnumSet.copyOf(updateType.getWherePosted());
		this.msgType = updateType.getId();
	}


	public UpdateMessage(FeedMessage fm) {
		this.userId = fm.getUserId();
		this.queueType = fm.getQueueType();
		this.setTimeCreatedUUID(fm.getTimeCreatedUUID());
		this.msgType = fm.getMsgType();
		this.properties = fm.getProperties();
		this.updateType = UpdateTypeEnum.getById(fm.getMsgType());
		this.wherePosted = EnumSet.copyOf(this.updateType.getWherePosted());
	}

	public UpdateMessage(UpdateMessage um, Set<QueueTypeEnum> wherePosted) {
		this.userId = um.getUserId();
		this.queueType = um.getQueueType();
		this.setTimeCreatedUUID(um.getTimeCreatedUUID());
		this.msgType = um.getMsgType();
		this.properties = um.getProperties();
		this.updateType = um.getUpdateType();
		this.wherePosted = wherePosted;
	}

	/**
	 * @return the updateType
	 */
	public UpdateTypeEnum getUpdateType() {
		return updateType;
	}
	/**
	 * @param updateType the updateType to set
	 */
	public void setUpdateType(UpdateTypeEnum updateType) {
		this.updateType = updateType;
	}

	/**
	 * @return the wherePosted
	 */
	public Set<QueueTypeEnum> getWherePosted() {
		return wherePosted;
	}

	/**
	 * @param timeCreatedUUID the timeCreatedUUID to set
	 */
	@Override
	public void setTimeCreatedUUID(UUID timeCreatedUUID) {
		this.timeCreatedUUID = timeCreatedUUID;
		timeCreated = new Date(UUIDs.unixTimestamp(timeCreatedUUID));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateMessage [updateType=").append(updateType)
				.append(", wherePosted=").append(wherePosted)
				.append(", userId=").append(userId)
				.append(", queueType=").append(queueType)
				.append(", timeCreated=").append(timeCreated)
				.append(", timeCreatedUUID=").append(timeCreatedUUID)
				.append(", msgType=").append(msgType)
				.append(", properties=").append(properties)
				.append("]");
		return builder.toString();
	}

}
