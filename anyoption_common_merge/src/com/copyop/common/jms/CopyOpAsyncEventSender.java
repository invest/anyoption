package com.copyop.common.jms;

import java.util.Vector;

import org.apache.log4j.Logger;

import com.copyop.common.jms.events.CopyOpEvent;

public class CopyOpAsyncEventSender implements CopyOpAsyncEventSenderMBean {
	
	private static Logger log = Logger.getLogger(CopyOpAsyncEventSender.class);
	
	private static Vector<CopyOpEvent> cache = null;
	private static boolean started = false;
	private static Thread sender = null; 
	
	public static void sendEvent(CopyOpEvent msg) {
		if(cache == null ) {
			cache = new Vector<CopyOpEvent>();
		}
		cache.add(msg);
		
		if(!started) {
			started = true;
			getSender().start();
		}
	}

	private static Thread getSender() {
		if(sender == null) {
			sender = new Thread() {
				long sleep = 200;
				@Override
				public void run() {
					while(started) {
						if(cache.size() > 0 ) {
							CopyOpEvent msg = cache.get(0);

							if(CopyOpEventSender.sendMessage(msg)) {
								cache.remove(0);
							} else {
								log.error("Waiting for ActiveMQ with"+cache.size()+ " msgs");
								sleep = 2000;
							}
						}
						try {
							sleep(sleep);
							sleep = 200;
						} catch (InterruptedException e) {
						}
					}
				}
			};
			sender.setName("Copyop event sender");
		}
		return sender;
	}

	@Override
	public int getPendingMsgSize() {
		return cache.size();
	}

	public static void stop() {
		started = false;
		sender = null;
	}
	

}
