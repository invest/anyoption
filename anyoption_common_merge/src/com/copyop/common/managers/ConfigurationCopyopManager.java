package com.copyop.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.copyop.common.dao.ConfigurationCopyopDao;

public class ConfigurationCopyopManager extends com.anyoption.common.managers.BaseBLManager {

	private static final Logger logger = Logger.getLogger(ConfigurationCopyopManager.class);

	private static HashMap<String, String> cfgProperties;
	private static HashMap<Long, TreeMap<Long, Long>> copyopCoinsConversionRates;
	private static HashMap<Long, TreeMap<Long, Long>> copyopCoinStepsPerCurrency;
	private static HashMap<Long, HashMap<Long, ArrayList<Long>>> copyoAmountsCfg;
	private static HashMap<Long, Long> increaseAmountBtnShowCfg;

	public static void initPriorityEnum(Connection con) throws SQLException {
		logger.debug("Begin init Copyop PriorityEnum");
		ConfigurationCopyopDao.initPriorityEnum(con);
		logger.debug("End init Copyop PriorityEnum");
	}

	public static void initUpdateTypeEnum(Connection con) throws SQLException {
		logger.debug("Begin init Copyop UpdateTypeEnum");
		ConfigurationCopyopDao.initUpdateTypeEnum(con);
		logger.debug("End init Copyop UpdateTypeEnum");
	}

	public static String getCopyopPropertiesValue(String key, String defaultValue)  {
		String res = defaultValue;
		try {
			res = ConfigurationCopyopManager.getCopyopPropertiesConfig().get(key);
			if (res == null) {
				res = defaultValue;
				logger.warn("No value found for property [" + key + "], returning default value: " + defaultValue);
			}
		} catch (Exception e) {
			logger.error("Can't get CFG Properties! Return default value:" + defaultValue + " for: " + key, e);
		}
		return res;
	}

	public static HashMap<String, String> getCopyopPropertiesConfig() throws SQLException {
		 if (null == cfgProperties) {
		        Connection con = null;
		        try {
		            con = getConnection();
		            cfgProperties = ConfigurationCopyopDao.getCopyopPropertiesConfig(con);
		        } finally {
		            closeConnection(con);
		        }
	       }
	       return cfgProperties;
	}

	public static void loadCopyopCfg() {
        Connection con = null;
        try {
            con = getConnection();
            loadCopyopCfg(con);
        } catch (Exception e) {
        	logger.error("Can't load copyop cfg", e);
			e.printStackTrace();
		} finally {
            closeConnection(con);
        }
	}

	public static HashMap<Long, TreeMap<Long, Long>> getCopyopCoinsConversionRates() throws SQLException {
		 if (null == copyopCoinsConversionRates) {
		        Connection con = null;
		        try {
		            con = getConnection();
		            copyopCoinsConversionRates = ConfigurationCopyopDao.getCopyopCoinsConversionRatesCfg(con);
		        } finally {
		            closeConnection(con);
		        }
	       }
		return copyopCoinsConversionRates;
	}

	/**
	 * @return Map with currency id and TreeMap inside with coins per given amount top threshold
	 * @throws SQLException
	 */
	public static HashMap<Long, TreeMap<Long, Long>> getCopyopCoinStepsPerCurrency() throws SQLException {
	    if (copyopCoinStepsPerCurrency == null || copyopCoinStepsPerCurrency.isEmpty()) {
		Connection con = null;
		try {
		    con = getConnection();
		    copyopCoinStepsPerCurrency = ConfigurationCopyopDao.getCopyopCoinStepsPerCurrencyCfg(con);
		} finally {
		    closeConnection(con);
		}
	    }
	    return copyopCoinStepsPerCurrency;
	}
	
	/**
	 * @param currnecyId
	 * @return Map of <Amount> to be reached in non-decimal, multiplied by 100 format, and <Coins> to be awarded if so
	 * @throws SQLException
	 */
	public static TreeMap<Long, Long> getCopyopStepsAndCoinsPerCurrency(Long currnecyId) throws SQLException {
	    return getCopyopCoinStepsPerCurrency().get(currnecyId);
	}

	public static TreeMap<Long, Long> getCopyopCoinsConversionRates(Long currnecyId) throws SQLException {
	    return getCopyopCoinsConversionRates().get(currnecyId);
	}

	public static Long getMinCopyopCoinsConversionRate(Long currnecyId) throws SQLException {
		return  getCopyopCoinsConversionRates().get(currnecyId).firstEntry().getValue();
	}

	public static HashMap<Long, HashMap<Long, ArrayList<Long>>> getCopyoAmountsCfg() throws SQLException {
		 if (null == copyoAmountsCfg) {
		        Connection con = null;
		        try {
		            con = getConnection();
		            copyoAmountsCfg = ConfigurationCopyopDao.getCopyoAmountsCfg(con);
		        } finally {
		            closeConnection(con);
		        }
	       }
		return copyoAmountsCfg;
	}
	
	public static HashMap<Long, Long> getIncreaseAmountBtnShowCfg() throws SQLException {
		 if (null == increaseAmountBtnShowCfg) {
		        Connection con = null;
		        try {
		            con = getConnection();
		            increaseAmountBtnShowCfg = ConfigurationCopyopDao.getCopyopIncreaseAmountBtnShowCfg(con);
		        } finally {
		            closeConnection(con);
		        }
	       }
		return increaseAmountBtnShowCfg;
	}

	public static ArrayList<Long> getCopyoAmountsCfg(Long currnecyId, Long userRegistrationType) throws SQLException {
		return  getCopyoAmountsCfg().get(userRegistrationType).get(currnecyId);
	}
	
	public static long getUserRegistrationType(Date date) throws SQLException {
		long type = 1L; //default 1 if the Copyop user registration date are messed up
		Connection con = null;
		try {
			con = getConnection();
			HashMap<Long, ArrayList<Date>> map = ConfigurationCopyopDao.getCopyopRegistrationDateType(con);
			for (HashMap.Entry<Long, ArrayList<Date>> entry : map.entrySet())
			{
				ArrayList<Date> list = entry.getValue();
				if (date.after(list.get(0)) && date.before(list.get(1))) {
					type = entry.getKey();
					break;
				}
			}
		} finally {
            closeConnection(con);
        }
		return type;
	}

	/**
	 * Load/Refresh Copyop Config
	 *
	 * @param Oracle Connection
	 */
	public static void loadCopyopCfg(Connection con) throws SQLException {
		logger.debug("Begin load Copyop cfg");
		initPriorityEnum(con);
		initUpdateTypeEnum(con);
		cfgProperties = null;
		copyopCoinsConversionRates = null;
		copyopCoinStepsPerCurrency = null;
		copyoAmountsCfg = null;
		increaseAmountBtnShowCfg = null;
		logger.debug("End load Copyop cfg");
	}
	
	public static double[] getCopyopInvOdds(Date linkDate) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return ConfigurationCopyopDao.getCopyopInvOdds(con, linkDate);
		} finally {
            closeConnection(con);
		}
	}
	
	public static double[] getCopyopLastInvOdds() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return ConfigurationCopyopDao.getCopyopLastInvOdds(con);
		} finally {
			closeConnection(con);
		}
	}
}