package com.copyop.common.managers;

import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.copyop.common.dao.ProfilesOnlineDAO;
import com.datastax.driver.core.Session;


public class ProfilesOnlineManager extends ManagerBase {

	private static final Logger log = Logger.getLogger(ProfilesOnlineManager.class);

	private static final long CACHE_REFRESH_DELAY = 1000L;

	private static Map<Long, Set<String>> sessionsPerUser;

	private static ScheduledExecutorService cacheUpdateThreadPool;
	private static UUID latestDate;

	public static void initOnlineUsersCache() {
		if (sessionsPerUser != null) {
			log.error("Online profiles cache already initialized");
			/* !!! WARNING !!! Method exit point ! */
			return;
		}

		log.info("Initializing online profiles cache");

		// Only one thread will modify the map
		sessionsPerUser = new ConcurrentHashMap<Long, Set<String>>(16, 0.75f, 1);
		Session session = getSession();

		log.info("Loading online profiles from DB");
		latestDate = ProfilesOnlineDAO.loadUserActiveSessions(session, sessionsPerUser);
		log.info("Loading online profiles from DB done");

		cacheUpdateThreadPool = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {

			@Override
			public Thread newThread(Runnable r) {
				Thread thread = new Thread(r);
				thread.setName("ONLINE-PROFILES-CACHE");
				return thread;
			}
		});

		cacheUpdateThreadPool.scheduleWithFixedDelay(new Runnable() {

			@Override
			public void run() {
				try {
					log.trace("Refreshing online profiles cache");
					Session session = getSession();
					latestDate = ProfilesOnlineDAO.loadUserActiveSessions(session, sessionsPerUser, latestDate);
					log.trace("Refreshing online profiles cache done");
				} catch(Exception e) {
					log.error("Exception while refreshing cache", e);
				}
			}
		}, CACHE_REFRESH_DELAY, CACHE_REFRESH_DELAY, TimeUnit.MILLISECONDS);

		log.info("Initializing online profiles cache done");
	}

	public static void clearOnlineUsersCache() {
		log.info("Clearing online profiles cache");
		try {
			if (cacheUpdateThreadPool != null) {
				cacheUpdateThreadPool.shutdownNow();
			}

			if (sessionsPerUser != null) {
				sessionsPerUser.clear();
				sessionsPerUser = null;
			}
		} catch (Exception e) {
			log.error("Unable to slear online users cache", e);
		}
		log.info("Clearing online profiles cache done");
	}

	public static Set<Long> getUserIdsOnline() {
		if (sessionsPerUser != null) {
			return Collections.unmodifiableSet(sessionsPerUser.keySet());
		} else {
			log.error("Online profiles cache not initialized while requesting online user IDs set");
			return null;
		}
	}

	/**
	 * Checks whether a user has open Lightstreamer sessions.
	 * This should indicate whether the user is online (logged in).
	 *
	 * @param userId
	 * @return <code>true</code> if the user has open Lighstreamer sessions, otherwise <code>false</code>
	 */
	public static boolean isUserOnline(long userId) {
		if (sessionsPerUser != null) { // if cache is initialized
			/* !!! WARNING !!! Method exit point! */
			return sessionsPerUser.containsKey(userId);
		}

		Session session = getSession();
		long beginTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		boolean result = ProfilesOnlineDAO.isUserOnline(session, userId);
		if (log.isTraceEnabled()) {
			log.trace("TIME: get last hot table data " + (System.currentTimeMillis() - beginTime) + " ms");
		}
		return result;
	}
	
	public static Date getLastLogoutDate(long userId) {
		Session session = getSession();
		return ProfilesOnlineDAO.getLastLogoutDate(session, userId);
	}
}
