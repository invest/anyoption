package com.copyop.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.copyop.common.dao.FrozenCopyopDao;
import com.copyop.common.dto.Frozen;
import com.copyop.common.dto.ProfileLink;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;


public class FrozenCopyopManager extends com.anyoption.common.managers.BaseBLManager {

	private static final Logger logger = Logger.getLogger(FrozenCopyopManager.class);	
    
	public static void insert(Frozen fr) throws SQLException {
        Connection conn = getConnection();
        try {
        	FrozenCopyopDao.insert(conn, fr);
        } finally {
            closeConnection(conn);
        }
    }
	
	public static void updateNonFrozen(Frozen fr) throws SQLException {
        Connection conn = getConnection();
        try {
        	FrozenCopyopDao.updateNonFrozen(conn, fr);
        } finally {
            closeConnection(conn);
        }
    }
	
	public static void update(ArrayList<Frozen> frozenList) throws SQLException {
        Connection conn = getConnection();
        try {
        	FrozenCopyopDao.update(conn, frozenList);
        } finally {
            closeConnection(conn);
        }
    }
	
	
	public static void unlinkFrozenUsers() {
		Connection con = null;
		int hours  = Integer.parseInt(ConfigurationCopyopManager.getCopyopPropertiesValue("FROZEN_USERS_UNLINK_HOURS", "6"));
		try {
			con = getConnection();
			ArrayList<Long> frozenList =  FrozenCopyopDao.getFrozenUserIds(con, hours);
			logger.debug("Start unlinking :" + frozenList.size() +" users.");
			for(Long userId : frozenList) {
				removeCopyLinks(userId);
				FrozenCopyopDao.updateUnlinked(con, userId, 1);
			}
			logger.debug("Done unlinking.");
			
		} catch (Exception e) {
			logger.error("When unlink FrozenUsers", e);
		} finally {
			closeConnection(con);
		}
	}

	
	private static void removeCopyLinks(long userId) {
		try {
			ArrayList<ProfileLink> copyLinks = (ArrayList<ProfileLink>) ProfileLinksManager.getProfileLinksByType(userId, ProfileLinkTypeEnum.COPY);
			boolean userIsTest = ProfileManager.isTestProfile(userId);
			if(copyLinks.size() > 0) {
				//the traders I copy will be moved to traders I watch
				for(ProfileLink copyLink:copyLinks) {
					boolean destUserIsTest = ProfileManager.isTestProfile(copyLink.getDestUserId());
					ProfileLinksManager.deleteCopyProfileLinkBatch(userId, copyLink.getDestUserId(), userIsTest, destUserIsTest);
					ProfileLinksManager.insertWatchProfileLink(userId, copyLink.getDestUserId(), userIsTest, destUserIsTest);
				}
			}
			ArrayList<ProfileLink> copiedLinks = (ArrayList<ProfileLink>) ProfileLinksManager.getProfileLinksByType(userId, ProfileLinkTypeEnum.COPIED_BY);
			if(copiedLinks.size() > 0) {
				//the traders that copy me will be moved to watchers
				for(ProfileLink copyLink:copiedLinks) {
					boolean destUserIsTest = ProfileManager.isTestProfile(copyLink.getUserId());
					ProfileLinksManager.deleteCopyProfileLinkBatch(copyLink.getDestUserId(), userId, destUserIsTest, userIsTest);
					ProfileLinksManager.insertWatchProfileLink(copyLink.getDestUserId(), userId, destUserIsTest, userIsTest);
				}
			}
		}catch(Throwable t) {
			logger.error("While removing copy links:", t);
		}
	}
		
	public static   ArrayList<Frozen> getBrokeFrozen() {
		ArrayList<Frozen> frozenList = new ArrayList<Frozen>();
		Connection con = null;
		long hours  = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("FROZEN_BROKE_BACK_TRADING_HOURS", "72"));
		try {
			con = getConnection();			
			frozenList = FrozenCopyopDao.getBrokeFrozen(con, hours);
		} catch (SQLException e) {
			logger.error("When get *Broke Frozen*", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("The *Frozen Broke* LIST size:" + frozenList.size());
		return frozenList;
	}
	
	public static   ArrayList<Frozen> getDormantFrozen() {
		ArrayList<Frozen> frozenList = new ArrayList<Frozen>();
		Connection con = null;
		int days  = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("FROZEN_DORMANT_BACK_DAYS", "7"));
		try {
			con = getConnection();			
			frozenList = FrozenCopyopDao.getDormantFrozen(con, days);
		} catch (SQLException e) {
			logger.error("When get *Dormant Frozen*", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("The *Dormant Broke* LIST size:" + frozenList.size());
		return frozenList;
	}
	
	public static   ArrayList<Frozen> getFreshOffUsers() {
		ArrayList<Frozen> unFrozenList = new ArrayList<Frozen>();
		Connection con = null;
		int hours  = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("FROZEN_FRESH_USER_HOURS", "48"));
		int invCount  = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("FROZEN_OFF_MIN_INV", "3"));
		try {
			con = getConnection();			
			unFrozenList = FrozenCopyopDao.getFreshOffUsers(con, hours, invCount);
		} catch (SQLException e) {
			logger.error("When get *UnFrozen New Users*", e);
		} finally {
			closeConnection(con);
		}
		logger.debug("The *getFreshOffUsers* LIST size:" + unFrozenList.size());
		return unFrozenList;
	}
	
}