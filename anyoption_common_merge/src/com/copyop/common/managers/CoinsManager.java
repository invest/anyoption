package com.copyop.common.managers;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.UsersManagerBase;
import com.copyop.common.dao.CoinsBalanceHistoryDAO;
import com.copyop.common.dto.CoinsBalanceHistory;
import com.copyop.common.dto.CoinsConvert;
import com.copyop.common.enums.CoinsActionTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.updates.UpdateMessage;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;

public class CoinsManager extends ManagerBase {

	private static final Logger log = Logger.getLogger(CoinsManager.class);

	public static CoinsConvert getCopyopCoinsConvertValue(long userId) {
		long currencyId = 0;
		long copyopCoins = 0;
		CoinsConvert ccc =  new CoinsConvert();
		try {
			currencyId = UsersManagerBase.getUserCurrencyId(userId);
			copyopCoins = ProfileCountersManager.getProfileCoinsBalance(userId);
			ccc = getCopyopCoinsConvertValue(copyopCoins, currencyId);
			ccc.setUserId(userId);
		} catch (Exception e) {
			log.error("Can't convert copyop coins",e);
		}
		return ccc;
	}

	public static CoinsConvert getCopyopCoinsConvertValue(long copyopCoins, long currencyId) {
		CoinsConvert ccc = new CoinsConvert();
		long amount = 0;
		long currentCoinsBalance = copyopCoins;
		try {
			TreeMap<Long, Long> coinsConversion = ConfigurationCopyopManager.getCopyopCoinsConversionRates(currencyId);
			log.debug( "Get Amount for COINS " + copyopCoins);
			log.debug( "Configuration for currencyId " + currencyId + " IS:");
			for(Entry<Long, Long> entry: coinsConversion.entrySet()){
				  Long key = entry.getKey();
				  Long value = entry.getValue();
				  log.debug( "For " + value + " coins =>" + key);
			}

			boolean flag = true;
			while (flag) {
				long convertedAmnt = getValues(currentCoinsBalance, coinsConversion);
				amount = amount + convertedAmnt;
				if(convertedAmnt > 0){
					currentCoinsBalance = currentCoinsBalance - coinsConversion.get(convertedAmnt);
				}
				if(currentCoinsBalance < ConfigurationCopyopManager.getMinCopyopCoinsConversionRate(currencyId)){
					flag = false;
				}

			}
			ccc.setAmount(amount);
			ccc.setConvrtedCoins(copyopCoins - currentCoinsBalance);
			ccc.setCurrentCoinsBalance(currentCoinsBalance);

		} catch (Exception e) {
			log.error("Can't convert copyop coins", e);
		}

		return ccc;
	}

	private static long getValues(long coins, TreeMap<Long, Long> coinsConversion){
		NavigableMap<Long, Long> nmp =  coinsConversion.descendingMap();
		long amountConverted = 0;
		for(Map.Entry<Long,Long> entry : nmp.entrySet()) {
			  Long key = entry.getKey();
			  Long value = entry.getValue();
			  if(coins >= value){
				  amountConverted = key;
				  coins = coins - value;
				  return amountConverted;
			  }
			}
		return amountConverted;
	}

	public static void insertCoinsBalanceHistory(CoinsBalanceHistory coinsBalance) {
		Session session = getSession();
		CoinsBalanceHistoryDAO.insertCoinsBalanceHistory(session, coinsBalance);
	}

	public static void updateCoins(long userId, long amount, CoinsActionTypeEnum actionType, String skinId, long writerId, String balanseHistoryDescription) {
		log.info("Not updating coins because of INV-228. userId: " + userId + " amount: " + amount + " actionType: " + actionType + " skinId: " + skinId + " balanseHistoryDescription: " + balanseHistoryDescription);
/*
		long coins = ProfileCountersManager.getProfileCoinsBalance(userId);
		int treshold_1 = Integer.parseInt(ConfigurationCopyopManager.getCopyopPropertiesValue("COINS_RESET_THRESHOLD_1", "0"));
		int treshold_2 = Integer.parseInt(ConfigurationCopyopManager.getCopyopPropertiesValue("COINS_RESET_THRESHOLD_2", "50"));
		if(coins == treshold_1 || (coins + amount) > treshold_2 && coins < treshold_2) {
			log.debug("start reset coins timer for " + userId);
			// set time_reset_coins
			ProfileManager.updateTimeResetCoinsNow(userId);
		}

		int multipleOf = Integer.parseInt(ConfigurationCopyopManager.getCopyopPropertiesValue("COINS_REACHED_MSG_ON_MULTIPLE_OF", "10"));
		double before = Math.floor(coins/multipleOf);
		double after = Math.floor((coins + amount)/multipleOf);
		if( after > before) {
			UUID eventId = UUIDs.timeBased();
			log.info("updateCoins: " + eventId);
			// send message for reaching coins level
			HashMap<String, String> props = new HashMap<String, String>();
			props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(userId));
			props.put(UpdateMessage.PROPERTY_KEY_COINS_BALANCE, String.valueOf((coins + amount)));
			props.put(UpdateMessage.PROPERTY_KEY_PUSH_SKIN_ID, skinId);
			CopyOpEventSender.sendUpdate(new UpdateMessage(userId, UpdateTypeEnum.AB_REACHED_COINS, props, eventId),
											CopyOpEventSender.FEED_DESTINATION_NAME);
		}

		log.debug("increasing coins for " + userId + " with amount " + amount + " reason " + actionType);
		CoinsBalanceHistory coinsBalanceHistory = new CoinsBalanceHistory(userId, actionType.getId(), false, amount, balanseHistoryDescription, writerId);
		CoinsManager.insertCoinsBalanceHistory(coinsBalanceHistory);
		ProfileCountersManager.increaseProfileCoinsBalance(userId, amount);
*/
	}
}