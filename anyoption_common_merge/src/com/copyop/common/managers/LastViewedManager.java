package com.copyop.common.managers;

import java.util.ArrayList;
import java.util.List;

import com.copyop.common.dao.FbDAO;
import com.copyop.common.dao.LastViewedDAO;
import com.datastax.driver.core.Session;

public class LastViewedManager extends ManagerBase {
	
	public static ArrayList<Long> getFbFriendsUserIds(long userId) {
		Session session = getSession();
		List<String> friends = FbDAO.getFbFriends(session, userId);
		return FbDAO.getUserIdsForFbIds(session, friends);
	}
	
	public static void insertLastViewed(long userId, long viewedUserId) {
		Session session = getSession();				
		LastViewedDAO.insertLastViewed(session, userId, viewedUserId);
	}
	
	public static List<Long> getLastViewedUserIds(long userId) {
		long limit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("LAST_VIEWED_USERS_LIST_SIZE", "3"));
		Session session = getSession();
		return LastViewedDAO.getLastViewedUserIds(session, userId, limit);
	}	
}
