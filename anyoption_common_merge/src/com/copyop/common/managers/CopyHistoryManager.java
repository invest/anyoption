package com.copyop.common.managers;

import java.util.List;

import com.copyop.common.dao.CopyHistoryDAO;
import com.datastax.driver.core.Session;

public class CopyHistoryManager extends ManagerBase {

	public static void insertCopyHistory(long userId,long destUserId) {
		Session session = getSession();
		CopyHistoryDAO.insertCopyHistory(session, userId, destUserId);
	}

	public static List<Long> getCopyHistoryOf(long userId) {
		Session session = getSession();
		return CopyHistoryDAO.getCopyHistoryOf(session, userId, false);
	}

}
