package com.copyop.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.managers.BaseBLManager;
import com.copyop.common.dao.HitDAO;

public class HitManager extends BaseBLManager{
	
	public static void insertHitRate(long userId, float hitRateSelf, float hitRateCopied) throws SQLException {
		Connection conn = null;
		try{
			conn = getConnection();
			HitDAO.insertHitRate(conn, userId, hitRateSelf, hitRateCopied );
		} finally {
			conn.close();
		}
	}
}
