package com.copyop.common.managers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.copyop.common.dao.InvestmentDAO;
import com.copyop.common.dto.CopiedInvestment;
import com.datastax.driver.core.Session;

public class InvestmentManager extends ManagerBase {


	public static CopiedInvestment getCopiedInvestment(long userId, long marketId, long copiedFromUserId) {
		Session session = getSession();
		return InvestmentDAO.getCopiedInvestment(session, userId, marketId, copiedFromUserId);
	}

	public static boolean updateCopiedInvestment(CopiedInvestment cp, int copied_count_lock) {
		Session session = getSession();
		return InvestmentDAO.updateCopiedInvestment(session, cp, copied_count_lock);
	}

	public static void updateCopiedInvestmentProfit(long profit, long userId, long marketId, long copiedFromUserId){
		Session session = getSession();
		InvestmentDAO.updateCopiedInvestmentProfit(session, profit, userId, marketId, copiedFromUserId);

	}

	public static TreeMap<Long, List<CopiedInvestment>> getMarketsByProfit(long userId, long copiedFromUserId) {
		Session session = getSession();
		return InvestmentDAO.getMarketsByProfit(session, userId, copiedFromUserId);
	}

	public static Map<Long, List<CopiedInvestment>> getCopiedInvestments(long userId, List<Long> copyFromUserIds) {
		Session session = getSession();
		return InvestmentDAO.getCopiedInvestments(session, userId, copyFromUserIds);
	}
}