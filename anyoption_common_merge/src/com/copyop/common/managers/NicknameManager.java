package com.copyop.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.managers.BaseBLManager;
import com.copyop.common.dao.NicknameDAO;

	public class NicknameManager extends BaseBLManager {
		public static String getUnunsedNicknameFor(long userId) throws SQLException {
			Connection connection = getConnection();
			String nickName = NicknameDAO.getUnunsedNickname(connection);
			NicknameDAO.markAsUsed(connection, nickName, userId);
			return nickName;
		}
	}
