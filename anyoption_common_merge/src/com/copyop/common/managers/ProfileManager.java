package com.copyop.common.managers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.CopyopUserCopy;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.util.ConstantsBase;
import com.copyop.common.beans.UserUniqueDetails;
import com.copyop.common.dao.CopiedInvestmentDAO;
import com.copyop.common.dao.FeedDAO;
import com.copyop.common.dao.ProfileDAO;
import com.copyop.common.dao.ProfileLinksDAO;
import com.copyop.common.dto.CopiedInvestment;
import com.copyop.common.dto.Frozen;
import com.copyop.common.dto.Profile;
import com.copyop.common.dto.ProfileLink;
import com.copyop.common.dto.ProfileLinkHistory;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.common.enums.base.UserStateEnum;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.events.ProfileManageEvent;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;

public class ProfileManager extends ManagerBase {

	private static final Logger log = Logger.getLogger(ProfileManager.class);

	public static Profile getProfile(long userId, boolean loadBlockedProfileIfBlocked) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		Profile p = ProfileDAO.getProfile(session, userId);

		if(p != null) {
			if (p.getUserState() == UserStateEnum.STATE_BLOCKED && loadBlockedProfileIfBlocked) {
				ProfileDAO.getBlockedProfile(session, p);
			}
			ProfileCountersManager.loadAllProfileRelations(p);
		}
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get profile " + (endTime - beginTime) + "ms");
		}
		return p;
	}

	public static Profile getProfile(long userId) {
		return getProfile(userId, false);
	}

	public static boolean isTestProfile(long userId) {
		Session session = getSession();
		return ProfileDAO.isTestProfile(session, userId);
	}

	public static void updateProfileToTest(long userId, boolean isTest) {
		Session session = getSession();
		ProfileDAO.updateProfileToTest(session, userId, isTest);
	}

	public static void insertProfile(Profile profile) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		ProfileDAO.insertProfile(session, profile);
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: insert profile " + (endTime - beginTime) + "ms");
		}
	}

	public static void updateProfile(boolean frozen, long userId) {
		Session session = getSession();
		ProfileDAO.updateProfile(session, frozen, userId);
	}

	public static void updateProfile(List<Long> lastInvestments, List<Float> riskAppetite, String today, int value, long userId) {
		Session session = getSession();
		ProfileDAO.updateProfile(session, lastInvestments, riskAppetite, today, value, userId);
	}

	public static void updateProfileLastInvestments(List<Long> lastInvestments, long userId) {
		Session session = getSession();
		ProfileDAO.updateProfileLastInvestments(session, lastInvestments, userId);
	}

	public static void updateTradesHistory(int ttl, long userId, String formatedDate, int value) {
		Session session = getSession();
		ProfileDAO.updateTradesHistory(session, ttl, userId, formatedDate, value);
	}

	public static boolean updateProfile(long userId, int newCurrentStreak, long newCurrentStreakAmount,
			int newBestStreak, long newBestStreakAmount, long newAssetResult, long newMarketId, long newBestAsset, float newHitRate, UUID lock) {
		Session session = getSession();
		return ProfileDAO.updateProfile(session, userId, newCurrentStreak, newCurrentStreakAmount,
				newBestStreak, newBestStreakAmount, newAssetResult, newMarketId, newBestAsset, newHitRate, lock);
	}

	public static void updateProfile(long userId, int currentStreak, long currentStreakAmount,
			int bestStreak, long bestStreakAmount, Map<Long, Long> assetResult, long bestAsset, float hitRate) {
		Session session = getSession();
		ProfileDAO.updateProfile(session, userId, currentStreak, currentStreakAmount,
				bestStreak, bestStreakAmount, assetResult, bestAsset, hitRate);
	}

//	public static void updateAssetsResult(long assetId, long result, long userId) {
//		Session session = getSession();
//		ProfileDAO.updateAssetsResult(session, assetId, result, userId);
//	}

	public static List<Profile> getProfiles(List<Long> userIds, boolean loadBlockedProfileIfBlocked) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		List<Profile> pList = ProfileDAO.getProfiles(session, userIds);
		if (pList != null && !pList.isEmpty() && loadBlockedProfileIfBlocked) {
			for (Profile profile : pList) {
				if (profile.getUserState() == UserStateEnum.STATE_BLOCKED) {
					ProfileDAO.getBlockedProfile(session, profile);
				}
			}
		}
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get profiles " + (endTime - beginTime) + "ms");
		}
		return pList;
	}

	public static void loadDestProfileDetailsForMessage(Long destUserId, FeedMessage message) {
		loadDestProfileDetailsForMessage(destUserId, message.getProperties());
	}

	public static void loadDestProfileDetailsForMessage(Long destUserId, Map<String, String> props) {
		Profile p = loadProfileDetails(destUserId);
		props.put(FeedMessage.PROPERTY_KEY_DEST_NICKNAME, p.getNickname());
		props.put(FeedMessage.PROPERTY_KEY_DEST_USER_FROZEN, String.valueOf(p.isFrozen()));
	}

	public static void loadProfileDetailsForMessage(Long userId, FeedMessage message) {
		loadProfileDetailsForMessage(userId, message.getProperties());
	}

	public static void loadProfileDetailsForMessage(Long userId, Map<String, String> props) {
		Profile p = loadProfileDetails(userId);
		props.put(FeedMessage.PROPERTY_KEY_NICKNAME, p.getNickname());
		props.put(FeedMessage.PROPERTY_KEY_AVATAR, p.getAvatar());
		props.put(FeedMessage.PROPERTY_KEY_USER_FROZEN, String.valueOf(p.isFrozen()));
	}

	public static void loadSrcProfileDetailsForMessage(Long userId, Map<String, String> props) {
		Profile p = loadProfileDetails(userId);
		props.put(FeedMessage.PROPERTY_KEY_SRC_NICKNAME, p.getNickname());
		props.put(FeedMessage.PROPERTY_KEY_SRC_AVATAR, p.getAvatar());
	}

	public static void loadProfileDetailsForTrader(long userId, Profile profile) {
		Profile p = loadProfileDetails(userId);
		profile.setNickname(p.getNickname());
		profile.setAvatar(p.getAvatar());
		profile.setFrozen(p.isFrozen());
		profile.setUserState(p.getUserState());
	}

	private static Profile loadProfileDetails(long userId) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		Profile p = new Profile();
		ProfileDAO.loadProfileDetails(session, userId, p);
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get load profile details " + (endTime - beginTime) + "ms");
		}
		return p;
	}

	public static List<Long> getRandomHotTraderIds(int size) {
		List<Long> result = new ArrayList<Long>();
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		List<FeedMessage> fm = FeedDAO.getLastHotTableData(session, UpdateTypeEnum.BEST_TRADERS);
		List<Long> userIds = FeedManager.getProfileIdsFromFeedMessage(fm);
		Collections.shuffle(userIds, new Random());
		if (userIds.size() > size) {
			result = userIds.subList(0, size);
		} else {
			result = userIds;
		}
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get last hot table data " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static boolean isFrozenProfile(long userId) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		boolean result = ProfileDAO.isFrozenTrader(session, userId);
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get last hot table data " + (endTime - beginTime) + "ms");
		}
		return result;
	}

	public static void changeCopyopNickname(long userId, String nickName, String oldNickname) {
		Session session = getSession();
		ProfileDAO.changeCopyopNickname(session, userId, nickName);
		ProfileDAO.deleteOldNicknameFromNicknames(session, oldNickname);
	}

	public static void deleteOldNicknameFromNicknames(String nickname) {
		Session session = getSession();
		ProfileDAO.deleteOldNicknameFromNicknames(session, nickname);
	}

	public static boolean isCopyopNicknameChanged(long userId) {
		Session session = getSession();
		return ProfileDAO.isCopyopNicknameChanged(session, userId);
	}

	public static void updateFrozenTrader(List<Frozen> frozenList) {
		Session session = getSession();
		ProfileDAO.updateFrozenTrader(session, frozenList);
	}

	public static void insertFrozenTraderTemp(List<Frozen> frozenList) {
		Session session = getSession();
		ProfileDAO.insertFrozenTraderTemp(session, frozenList);
	}

	public static ArrayList<Long> getFrozenTraders(){
		Session session = getSession();
		return ProfileDAO.getFrozenTraders(session);
	}

	public static void deleteFrozenTraders(){
		Session session = getSession();
		ProfileDAO.deleteFrozenTraders(session);
	}

	public static void updateProfile(UUID timeConvertCoins, long userId) {
		Session session = getSession();
		ProfileDAO.updateProfile(session, timeConvertCoins, userId);
	}

	public static void updateTimeResetCoinsNow(long userId) {
		Session session = getSession();
		UUID timeConvertCoins = UUIDs.timeBased();
		ProfileDAO.updateProfile(session, timeConvertCoins, userId);
		int delayHours = Integer.parseInt(ConfigurationCopyopManager.getCopyopPropertiesValue("COINS_RESET_PERIOD_HOURS", "720"));
		long delayMilis = TimeUnit.HOURS.toMillis(delayHours);
		ProfileManageEvent scheduled = new ProfileManageEvent(timeConvertCoins, userId);
		// schedule reset coin balance (send COINSRESET msg)
		log.debug("3Scheduling "+scheduled +" for " +delayMilis);
		CopyOpEventSender.sendEvent(scheduled, CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME, delayMilis);
	}

	public static List<com.copyop.common.dto.base.Profile> getFbProfiles(List<Long> userIds) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		List<com.copyop.common.dto.base.Profile> list = ProfileDAO.getFbProfile(session, userIds);

		log.debug("Set Copiers and Watchers");
		ProfileCountersManager.setProfileCopiersWatchers(list, userIds);
		log.debug("Set First and Last names");
		setProfileFirstLastName(list, userIds);
		loadBlockedContent(session, list);

		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get profile " + (endTime - beginTime) + "ms");
		}
		return list;
	}

	public static List<com.copyop.common.dto.base.Profile> getFbProfilesSimple(List<Long> userIds) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		List<com.copyop.common.dto.base.Profile> list = ProfileDAO.getFbProfile(session, userIds);

		log.debug("Set First and Last names");
		setProfileFirstLastName(list, userIds);
		loadBlockedContent(session, list);

		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get fb profiles simple " + (endTime - beginTime) + "ms");
		}
		return list;
	}

	public static com.copyop.common.dto.base.Profile getFbFriendProfile(long userId) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		com.copyop.common.dto.base.Profile p = ProfileDAO.getFbFriendProfile(session, userId);
		if (p.getUserState() == UserStateEnum.STATE_BLOCKED) {
			loadBlockedContent(session, p);
		}
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get fb friend profile " + (endTime - beginTime) + "ms");
		}
		return p;
	}

	private static void loadBlockedContent(Session session, List<com.copyop.common.dto.base.Profile> list) {
		for (com.copyop.common.dto.base.Profile p : list) {
			if (p.getUserState() == UserStateEnum.STATE_BLOCKED) {
				loadBlockedContent(session, p);
			}
		}
	}

	private static void loadBlockedContent(Session session, com.copyop.common.dto.base.Profile p) {
		Profile profile = new Profile(); // loading it this way to ensure all of the blocked
		profile.setUserId(p.getUserId()); // content is taken from the db
		ProfileDAO.getBlockedProfile(session, profile);
		p.setBestAsset(profile.getBestAsset());
		p.setBestStreak(profile.getBestStreak());
		p.setBestStreakAmount(profile.getBestStreakAmount());
		p.setHitRate(profile.getHitRate());
		p.setLastInvestments(profile.getLastInvestments());
	}

	public static void setProfileFirstLastName(Collection<com.copyop.common.dto.base.Profile> profiles, List<Long> userIds){

		HashMap<Long, User> hm = new HashMap<Long, User>();
		try {
			hm = UsersManagerBase.getUserFirstLastName(userIds);
		} catch (SQLException e) {
			log.error("Can't get Users" , e);
		}

		for(com.copyop.common.dto.base.Profile pf : profiles){
			pf.setFirstName(hm.get(pf.getUserId()).getFirstName());
			pf.setLastName(hm.get(pf.getUserId()).getLastName());
		}
	}

	public static void updateAvatar(long userId, String avatar, String avatarFileName) {
		Session session = getSession();
		ProfileDAO.updateAvatar(session, userId, avatar, avatarFileName);
	}

	public static String getAvatarFileName(long userId) {
		Session session = getSession();
		return ProfileDAO.getAvatarFileName(session, userId);
	}

	public static void updateAcceptedTersmNicknameAvatar(long userId, boolean acceptedTermsAndConditions, String nickname, String avatar) {
		try {
			if (acceptedTermsAndConditions) {
				UsersManagerBase.updateAcceptTerms(userId);
			}
			Session session = getSession();
			ProfileDAO.updateAcceptedTersmNicknameAvatar(session, userId, acceptedTermsAndConditions, nickname, avatar);
		} catch (Exception e) {
			log.error("Can't AcceptTerms and conditions");
		}
	}

	public static long getCoinsRemainingConvertTime(long userId){
		long res = 0;
		Session session = getSession();
		Date coinsConvertTime = ProfileDAO.getCoinsTimeResetDate(session, userId);
		if(coinsConvertTime != null){
			Date dateNow = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
			final int MILLISEC_TO_HOUR = 1000 * 60 * 60;
			long delayHours = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("COINS_RESET_PERIOD_HOURS", "720"));
			res = delayHours - (long) (dateNow.getTime() - coinsConvertTime.getTime()) / MILLISEC_TO_HOUR;
		}
		return res;
	}

	//TODO Refactoring. Change location functionality. It's due to copyopservice wanna be
	public static ArrayList<CopyopUserCopy> getUsersCopy(ProfileLinkTypeEnum profileLinkType, long userId, Date from,
			long userIdFilter, int skinIdFilter, int isStillCopyFilter, int isFrozenFilter) {
		ArrayList<CopyopUserCopy> copyopUserCopyList = new ArrayList<CopyopUserCopy>();
		HashMap<Long, UserUniqueDetails> usersUniqueDetails = new HashMap<Long, UserUniqueDetails>();
		HashSet<Long> destinationUsersIds = new HashSet<Long>();

		List<ProfileLink> profilesLink = ProfileLinksManager.getProfileLinksByType(userId, profileLinkType);
		Session session = getSession();
		List<ProfileLinkHistory> profilesLinkHistory = ProfileLinksDAO.getProfileLinksHistory(session, userId, profileLinkType);

		//get usersUniqueDetails from ProfileLink & ProfileLinkHistory.
		for (ProfileLink profileLink : profilesLink) {
			destinationUsersIds.add(profileLink.getDestUserId());
		}

		for (ProfileLinkHistory profileLinkHistory : profilesLinkHistory) {
			destinationUsersIds.add(profileLinkHistory.getDestUserId());
		}

		//assumption: method return size can't return more than ConstantsBase.MAXIMUM_ORACLE_IN_CLAUSE elements.
		if (destinationUsersIds.size() > ConstantsBase.MAXIMUM_ORACLE_IN_CLAUSE) {
			return null;
		}

		usersUniqueDetails = getUserUniqueDetails(session, userId, destinationUsersIds);

		for (ProfileLink profileLink : profilesLink) {
			CopyopUserCopy copyopUserCopy = getCopyopUserCopy(profileLink, usersUniqueDetails);
			if (copyopUserCopy != null) {
				copyopUserCopyList.add(copyopUserCopy);
			}
		}

		for (ProfileLinkHistory profileLinkHistory : profilesLinkHistory) {
			CopyopUserCopy copyopUserCopy = getCopyopUserCopyHistory(profileLinkHistory, usersUniqueDetails);
			if (copyopUserCopy != null) {
				copyopUserCopyList.add(copyopUserCopy);
			}
		}

		return copyopUserCopyList;
	}

	//TODO Refactoring. Change location functionality. It's due to copyopservice wanna be
	private static CopyopUserCopy getCopyopUserCopy(ProfileLink profileLink, HashMap<Long, UserUniqueDetails> usersUniqueDetails) {
		CopyopUserCopy copyopUserCopy = null;
		UserUniqueDetails user = usersUniqueDetails.get(profileLink.getDestUserId());
		if (user != null) {
			copyopUserCopy = new CopyopUserCopy();
			copyopUserCopy.setNickName(usersUniqueDetails.get(profileLink.getDestUserId()).getNickname());
			copyopUserCopy.setCopyTradingProfit(usersUniqueDetails.get(profileLink.getDestUserId()).getCopyTradingProfit());
			copyopUserCopy.setTurnover(usersUniqueDetails.get(profileLink.getDestUserId()).getTurnOver());
			copyopUserCopy.setBalance(usersUniqueDetails.get(profileLink.getDestUserId()).getBalance());
			copyopUserCopy.setFrozen(usersUniqueDetails.get(profileLink.getDestUserId()).isFrozen());
			copyopUserCopy.setUserId(profileLink.getDestUserId());
			copyopUserCopy.setStartDate(profileLink.getTimeCreated());
			copyopUserCopy.setInvCount(profileLink.getCount());
			copyopUserCopy.setInvCountReached(profileLink.getCountReached());
			copyopUserCopy.setAmount(profileLink.getAmount());
			copyopUserCopy.setSpecific((profileLink.getAssets().isEmpty() || profileLink.getAssets() == null) ? false : true); //empty list means "all".
			copyopUserCopy.setCoinsGenerated(usersUniqueDetails.get(profileLink.getDestUserId()).getCoinsGenerated());
			copyopUserCopy.setCurrencyId(usersUniqueDetails.get(profileLink.getDestUserId()).getCurrencyId());
			copyopUserCopy.setAssets(profileLink.getAssets());
			copyopUserCopy.setSkinId(usersUniqueDetails.get(profileLink.getDestUserId()).getSkinId());
		}

		return copyopUserCopy;
	}
	//TODO Refactoring. Change location functionality. It's due to copyopservice wanna be
	private static CopyopUserCopy getCopyopUserCopyHistory(ProfileLinkHistory profileLinkHistory, HashMap<Long, UserUniqueDetails> usersUniqueDetails) {
		CopyopUserCopy copyopUserCopy = getCopyopUserCopy(profileLinkHistory, usersUniqueDetails);
		copyopUserCopy.setEndDate(profileLinkHistory.getTimeDone());
		copyopUserCopy.setEndReason(profileLinkHistory.getResonDone().getDisplayName());

		return copyopUserCopy;
	}

	//TODO Refactoring. Change location functionality. It's due to copyopservice wanna be
	private static HashMap<Long, UserUniqueDetails> getUserUniqueDetails(Session session, long userId, HashSet<Long> destinationUsersIds) {
		ArrayList<Long> arrDestUsersIds = new ArrayList<Long>();
		StringBuilder usersIdsParse = new StringBuilder();
		String delim = "";

		arrDestUsersIds.addAll(destinationUsersIds);
		HashMap<Long, UserUniqueDetails> userUniqueDetailsC = ProfileDAO.getNicknamesAndFrozen(session, arrDestUsersIds);
		for (Long destUserId : arrDestUsersIds) {
			usersIdsParse.append(delim).append(destUserId);
			delim = ",";
		}

		HashMap<Long, UserUniqueDetails> userUniqueDetailsO = UsersManagerBase.getUserUniqueDetails(usersIdsParse);
		//user & his sum.
		HashMap<Long, CopiedInvestment> usersCopiedInvestmentSum = CopiedInvestmentDAO.getCopiedInvestmentSum(session, userId, arrDestUsersIds);

		for (Map.Entry<Long, UserUniqueDetails> userDetailsEntry : userUniqueDetailsO.entrySet()) {
			log.info("Key : " + userDetailsEntry.getKey() + " Value : " + userDetailsEntry.getValue().toString());
			UserUniqueDetails userUniqueDetails = new UserUniqueDetails();

			if (userUniqueDetailsO.containsKey(userDetailsEntry.getKey())) {
				userUniqueDetails.setBalance(userUniqueDetailsO.get(userDetailsEntry.getKey()).getBalance());
				userUniqueDetails.setTurnOver((userUniqueDetailsO.get(userDetailsEntry.getKey()).getTurnOver()));
				userUniqueDetails.setCurrencyId(userUniqueDetailsO.get(userDetailsEntry.getKey()).getCurrencyId());
				userUniqueDetails.setSkinId(userUniqueDetailsO.get(userDetailsEntry.getKey()).getSkinId());
			}

			if (userUniqueDetailsC.containsKey(userDetailsEntry.getKey())) {
				userUniqueDetails.setFrozen(userUniqueDetailsC.get(userDetailsEntry.getKey()).isFrozen());
				userUniqueDetails.setNickname(userUniqueDetailsC.get(userDetailsEntry.getKey()).getNickname());
			}

			if (usersCopiedInvestmentSum.containsKey(userDetailsEntry.getKey())) {
				userUniqueDetails.setCopyTradingProfit(usersCopiedInvestmentSum.get(userDetailsEntry.getKey()).getProfit());
				userUniqueDetails.setCoinsGenerated(2 * usersCopiedInvestmentSum.get(userDetailsEntry.getKey()).getCopiedCountSuccess());
			}

			userUniqueDetailsC.put(userDetailsEntry.getKey(), userUniqueDetails);
		}
		return userUniqueDetailsC;
	}


	public static ArrayList<CopyopUserCopy> doUserCopyFilters(ArrayList<CopyopUserCopy> userCopyingList, Date from, long userId,int skinId, int isStillCopy, int isFrozen) {
		if (userCopyingList != null && userCopyingList.size() > 0) {
			//Filters.
				CopyopUserCopy copyopUserCopy = null;
				boolean remove = false;
				for (Iterator<CopyopUserCopy> copyopUserCopyIterator = userCopyingList.iterator(); copyopUserCopyIterator.hasNext();) {
	    		    copyopUserCopy = copyopUserCopyIterator.next();
	    		    remove = false;
	    			//start date
	    		    if (copyopUserCopy.getStartDate().before(from)) {
	    				remove = true;
	    		    }
	    		    //userId
	    		    if (userId != ConstantsBase.FILTER_ALL) {
	    		    	if (userId != copyopUserCopy.getUserId()) {
	        		    	remove = true;
	        			}
	    		    }

	    		    //skinId
	    		    if (skinId != Skin.SKINS_ALL_VALUE) {
	    				if (skinId != copyopUserCopy.getSkinId()) {
	    					remove = true;
	    				}
	    			}
	    		    //Still Copying
	    		    if (isStillCopy != ConstantsBase.FILTER_ALL) {
	    		    	if (isStillCopy == ConstantsBase.FILTER_YES) {
	    		    		if (copyopUserCopy.getEndDate() != null) {
	    		    			remove = true;
	    		    		}
	    		    	} else if (isStillCopy == ConstantsBase.FILTER_NO) {
	    		    		if (copyopUserCopy.getEndDate() == null) {
	    		    			remove = true;
	    		    		}
	    		    	}
	    		    }
	    		    //Include Frozen
	    		    if (isFrozen == ConstantsBase.FILTER_NO) {
	    		    	if (copyopUserCopy.isFrozen()) {
	    		    		remove = true;
			    		}
	    		    }

	    		    if (remove) {
	    		    	log.debug("about to remove: " + copyopUserCopy.toString());
		    			copyopUserCopyIterator.remove();
	    		    }
	    		}
		}
		return userCopyingList;
	}

	public static String getProfileNickname(long userId) {
		Session session = getSession();
		return ProfileDAO.getProfileNickname(session, userId);
	}
	public static boolean lockProfile(long userId) {
		Session session = getSession();
		return ProfileDAO.lockProfile(session, userId);
	}

	public static void unlockProfile(long userId) {
		Session session = getSession();
		ProfileDAO.unlockProfile(session, userId);
	}

	public static boolean isLockedProfile(long userId){
		Session session = getSession();
		return ProfileDAO.isLockedProfile(session, userId);
	}

	public static boolean insertNicknameIfNotExists(String nickname, long userId) {
		Session session = getSession();
		return ProfileDAO.insertNicknameIfNotExists(session, nickname, userId);
	}

	public static boolean isNicknameRegistered(String nickname) {
		Session session = getSession();
		return ProfileDAO.isNicknameRegistered(session, nickname);
	}

	public static String getFaceBookId(long userId) {
		Session session = getSession();
		String res = "";
		String fbId = ProfileDAO.getFaceBookId(session, userId);
		if( fbId != null){
			res = fbId;
		}
		return res;
	}

	public static void insertRiskAppetite(long userId, float riskAppetite) {
		Session session = getSession();
		ProfileDAO.insertRiskAppetite(session, userId, riskAppetite);
	}

	public static Date getLastTimeMostCopied(long userId) {
		Session session = getSession();
		return ProfileDAO.getLastTimeMostCopied(session, userId);
	}

	public static void updateLastTimeMostCopied(long userId) {
		Session session = getSession();
		ProfileDAO.updateLastTimeMostCopied(session, userId);
	}

	public static HashMap<Long, CopiedInvestment> getCopiedInvestmentSum(long userId, List<Profile> profiles) {
		Session session = getSession();
		List<Long> destinationUsersId = new ArrayList<Long>();
		for(Profile pf : profiles){
			destinationUsersId.add(pf.getUserId());
		}
		return CopiedInvestmentDAO.getCopiedInvestmentSum(session, userId, destinationUsersId);
	}

	// used in scripts, should not be called anywhere in code
	public static ArrayList<Profile> getAllProfiles(int limit) {
		Session session = getSession();
		return ProfileDAO.getAllProfiles(session, limit);
	}

	public static boolean profileExists(long userId) {
		Session session = getSession();
		return ProfileDAO.profileExists(session, userId);
	}

	public static UserStateEnum getUserState(long userId) {
		Session session = getSession();
		return ProfileDAO.getUserState(session, userId);
	}

	public static void updateUserStateBS(long userId, UserStateEnum userState){
		Session session = getSession();
		ProfileDAO.UpdateUserState(session, userId, userState);
	}

	public static void clearProfilesLocks() {
		Session session = getSession();
		ProfileDAO.clearProfilesLocks(session);
	}
}