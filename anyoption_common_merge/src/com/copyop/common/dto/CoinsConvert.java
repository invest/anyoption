package com.copyop.common.dto;

public class CoinsConvert {
	private long userId;
	private long currentCoinsBalance;
	private long convrtedCoins;
	private long amount;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getCurrentCoinsBalance() {
		return currentCoinsBalance;
	}
	public void setCurrentCoinsBalance(long currentCoinsBalance) {
		this.currentCoinsBalance = currentCoinsBalance;
	}
	public long getConvrtedCoins() {
		return convrtedCoins;
	}
	public void setConvrtedCoins(long convrtedCoins) {
		this.convrtedCoins = convrtedCoins;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	
	@Override
	public String toString() {
		return "CopyopCoinsConvert [userId=" + userId + ", currentCoinsBalance="
				+ currentCoinsBalance + ", convrtedCoins=" + convrtedCoins + ", amount="
				+ amount + "]";
	}

}
