package com.copyop.common.dto.base;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import com.copyop.common.enums.base.UserStateEnum;

/**
 * @author kirilim
 */
public class Profile implements Serializable {

	private static final long serialVersionUID = 3222634035678199119L;
	private long userId;
	private long bestAsset;
	private long bestStreakAmount;
	private String nickname;
	private String avatar;
	private int bestStreak;
	private boolean frozen;
	private boolean test;
	private float hitRate;
	private int hitRatePercentage;
	private List<Long> lastInvestments;
	private String fbId;
	private boolean nicknameChanged;
	private UUID timeResetCoins;
	private String firstName;
	private String lastName;
	private long copiers;
	private long copying;
	private long watchers;
	private long watching;
	private long share;
	private long rate;
	private long copiedInvestments;
	private boolean acceptedTermsAndConditions;	
	private boolean isCopying;
	private boolean isWatching;	
	private double costEarned;
	private UserStateEnum userState;

	public Profile() {

	}

	public Profile(long userId, String nickname, String avatar, String fbId, Boolean acceptedTermsAndConditions, boolean test) {
		this.userId = userId;
		this.nickname = nickname;
		this.avatar = avatar;
		this.fbId = fbId;
		this.acceptedTermsAndConditions = acceptedTermsAndConditions;
		this.test = test;
	}
	
	public Profile(long userId, String avatar, String firstName, String lastName, float hitRate, long copiers, long watchers) {
		this.userId = userId;
		this.avatar = avatar;
		this.firstName = firstName;
		this.lastName = lastName;
		setHitRate(hitRate);
		this.copiers = copiers;
		this.watchers = watchers;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the bestAsset
	 */
	public long getBestAsset() {
		return bestAsset;
	}

	/**
	 * @param bestAsset the bestAsset to set
	 */
	public void setBestAsset(long bestAsset) {
		this.bestAsset = bestAsset;
	}

	/**
	 * @return the bestStreakAmount
	 */
	public long getBestStreakAmount() {
		return bestStreakAmount;
	}

	/**
	 * @param bestStreakAmount the bestStreakAmount to set
	 */
	public void setBestStreakAmount(long bestStreakAmount) {
		this.bestStreakAmount = bestStreakAmount;
	}

	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * @return the avatar
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * @param avatar the avatar to set
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	/**
	 * @return the bestStreak
	 */
	public int getBestStreak() {
		return bestStreak;
	}

	/**
	 * @param bestStreak the bestStreak to set
	 */
	public void setBestStreak(int bestStreak) {
		this.bestStreak = bestStreak;
	}

	/**
	 * @return the frozen
	 */
	public boolean isFrozen() {
		return frozen;
	}

	/**
	 * @param frozen the frozen to set
	 */
	public void setFrozen(boolean frozen) {
		this.frozen = frozen;
	}

	/**
	 * @return the hitRate
	 */
	public float getHitRate() {
		return hitRate;
	}

	/**
	 * @param hitRate the hitRate to set
	 */
	public void setHitRate(float hitRate) {
		this.hitRate = hitRate;
		setHitRatePercentage(Math.round(this.hitRate*100));
	}

	/**
	 * @return the lastInvestments
	 */
	public List<Long> getLastInvestments() {
		return lastInvestments;
	}

	/**
	 * @param lastInvestments the lastInvestments to set
	 */
	public void setLastInvestments(List<Long> lastInvestments) {
		this.lastInvestments = lastInvestments;
	}

	/**
	 * @return the fbId
	 */
	public String getFbId() {
		return fbId;
	}

	/**
	 * @param fbId the fbId to set
	 */
	public void setFbId(String fbId) {
		this.fbId = fbId;
	}

	/**
	 * @return the nicknameChanged
	 */
	public boolean isNicknameChanged() {
		return nicknameChanged;
	}

	/**
	 * @param nicknameChanged the nicknameChanged to set
	 */
	public void setNicknameChanged(boolean nicknameChanged) {
		this.nicknameChanged = nicknameChanged;
	}

	public UUID getTimeResetCoins() {
		return timeResetCoins;
	}

	public void setTimeResetCoins(UUID timeResetCoins) {
		this.timeResetCoins = timeResetCoins;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getCopiers() {
		return copiers;
	}

	public void setCopiers(long copiers) {
		this.copiers = copiers;
	}

	public long getCopying() {
		return copying;
	}

	public void setCopying(long copying) {
		this.copying = copying;
	}

	public long getWatchers() {
		return watchers;
	}

	public void setWatchers(long watchers) {
		this.watchers = watchers;
	}

	public long getWatching() {
		return watching;
	}

	public void setWatching(long watching) {
		this.watching = watching;
	}
	
	public boolean isAcceptedTermsAndConditions() {
		return acceptedTermsAndConditions;
	}

	public void setAcceptedTermsAndConditions(boolean acceptedTermsAndConditions) {
		this.acceptedTermsAndConditions = acceptedTermsAndConditions;
	}
	
	public boolean isCopying() {
		return isCopying;
	}

	public void setCopying(boolean isCopying) {
		this.isCopying = isCopying;
	}

	public boolean isWatching() {
		return isWatching;
	}

	public void setWatching(boolean isWatching) {
		this.isWatching = isWatching;
	}
	public boolean isTest() {
		return test;
	}

	public void setTest(boolean test) {
		this.test = test;
	}
	
	public long getShare() {
		return share;
	}

	public void setShare(long share) {
		this.share = share;
	}

	public long getRate() {
		return rate;
	}

	public void setRate(long rate) {
		this.rate = rate;
	}

	public int getHitRatePercentage() {
		return hitRatePercentage;
	}

	private void setHitRatePercentage(int hitRatePercentage) {
		this.hitRatePercentage = hitRatePercentage;
	}
		
	public double getCostEarned() {
		return costEarned;
	}

	public void setCostEarned(double costEarned) {
		this.costEarned = costEarned;
	}

	public UserStateEnum getUserState() {
		return userState;
	}

	public void setUserState(UserStateEnum userState) {
		this.userState = userState;
	}
	
	public long getCopiedInvestments() {
		return copiedInvestments;
	}

	public void setCopiedInvestments(long copiedInvestments) {
		this.copiedInvestments = copiedInvestments;
	}

	@Override
	public String toString() {
		return "Profile [userId=" + userId + ", bestAsset=" + bestAsset
				+ ", bestStreakAmount=" + bestStreakAmount + ", nickname="
				+ nickname + ", avatar=" + avatar + ", bestStreak="
				+ bestStreak + ", frozen=" + frozen + ", test=" + test
				+ ", hitRate=" + hitRate + ", hitRatePercentage="
				+ hitRatePercentage + ", lastInvestments=" + lastInvestments
				+ ", fbId=" + fbId + ", nicknameChanged=" + nicknameChanged
				+ ", timeResetCoins=" + timeResetCoins + ", firstName="
				+ firstName + ", lastName=" + lastName + ", copiers=" + copiers
				+ ", copying=" + copying + ", watchers=" + watchers
				+ ", watching=" + watching + ", share=" + share + ", rate="
				+ rate + ", acceptedTermsAndConditions=" + copiedInvestments + ", acceptedTermsAndConditions="
				+ acceptedTermsAndConditions + ", isCopying=" + isCopying
				+ ", isWatching=" + isWatching + ", costEarned=" + costEarned
				+ ", userState=" + userState + "]";
	}
}