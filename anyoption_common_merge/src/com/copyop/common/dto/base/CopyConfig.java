package com.copyop.common.dto.base;

import java.util.Set;

import com.copyop.common.enums.base.ProfileLinkCommandEnum;

/**
 * @author kirilim
 */
public class CopyConfig {

	private long userId;
	private long destUserId;
	private int count;
	private int amount;
	private Set<Long> assets;
	private ProfileLinkCommandEnum command;
	
	public CopyConfig(long userId, long destUserId, int count, int amount, Set<Long> assets) {
		this.userId = userId;
		this.destUserId = destUserId;
		this.count = count;
		this.amount = amount;
		this.assets = assets;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getDestUserId() {
		return destUserId;
	}

	public void setDestUserId(long destUserId) {
		this.destUserId = destUserId;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Set<Long> getAssets() {
		return assets;
	}

	public void setAssets(Set<Long> assets) {
		this.assets = assets;
	}

	public ProfileLinkCommandEnum getCommand() {
		return command;
	}

	public void setCommand(ProfileLinkCommandEnum command) {
		this.command = command;
	}
	
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "CopyConfig: " + ls
				+ super.toString()
				+ "userId: " + userId + ls
				+ "destUserId: " + destUserId + ls
				+ "count: " + count + ls
				+ "amount: " + amount + ls
				+ "assets: " + assets + ls
				+ "command: " + command + ls;
	}
}