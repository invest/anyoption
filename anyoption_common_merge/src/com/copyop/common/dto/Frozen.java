package com.copyop.common.dto;

public class Frozen {

	public static long FROZEN_REASON_FRESH = 0;
	public static long FROZEN_REASON_OFF = 1;
	public static long FROZEN_REASON_BROKE = 2;
	public static long FROZEN_REASON_DORMANT = 3;
	
	private long id;
	private long userId;
	private boolean isFrozen;
	private long reason;
	private long investmentId;
	private long writerId;
	
	
	public Frozen(){
	}
	
	public Frozen(long userId, boolean isFrozen, long reason, long investmentId, Long writerId){
		this.userId = userId;
		this.isFrozen = isFrozen;
		this.reason = reason;
		this.investmentId = investmentId;
		this.writerId = writerId;
	}
	
	
	public Frozen(long userId, boolean isFrozen, long reason, long writerId){
		this.userId = userId;
		this.isFrozen = isFrozen;
		this.reason = reason;
		this.writerId = writerId;
	}
	
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public boolean isFrozen() {
		return isFrozen;
	}
	public void setFrozen(boolean isFrozen) {
		this.isFrozen = isFrozen;
	}
	public long getReason() {
		return reason;
	}
	public void setReason(long reason) {
		this.reason = reason;
	}
	public long getInvestmentId() {
		return investmentId;
	}
	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}
	public long getWriterId() {
		return writerId;
	}
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	
	@Override
	public String toString() {
		return "Frozen [Id=" + id + ",userId=" + userId + ", isFrozen="
				+ isFrozen + ", reason=" + reason + ", investmentId="
				+ investmentId + ", writerId=" + writerId + "]";
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	
}
