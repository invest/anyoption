package com.copyop.common.dto;

import java.util.Date;
import java.util.Set;

import com.copyop.common.enums.ProfileLinkChangeReasonEnum;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;

/**
 * @author liors
 *
 */
public class ProfileLinkHistory extends ProfileLink {
	private ProfileLinkChangeReasonEnum resonDone;
	private Date timeDone;
	
	public ProfileLinkHistory() {
				
	}

	public ProfileLinkHistory(long userId, ProfileLinkTypeEnum type, long destUserId, 
			int count, int amount, Set<Long> assets, ProfileLinkChangeReasonEnum resonDone, Date timeDone) {
		super(userId, type, destUserId, count, amount, assets);
		this.resonDone = resonDone;
		this.timeDone = timeDone;
	}

	/**
	 * @return the resonDone
	 */
	public ProfileLinkChangeReasonEnum getResonDone() {
		return resonDone;
	}

	/**
	 * @param resonDone the resonDone to set
	 */
	public void setResonDone(ProfileLinkChangeReasonEnum resonDone) {
		this.resonDone = resonDone;
	}

	/**
	 * @return the timeDone
	 */
	public Date getTimeDone() {
		return timeDone;
	}

	/**
	 * @param timeDone the timeDone to set
	 */
	public void setTimeDone(Date timeDone) {
		this.timeDone = timeDone;
	}

	@Override
	public String toString() {
		return "ProfileLinkHistory [resonDone=" + resonDone + ", timeDone="
				+ timeDone + ", userId=" + userId + ", destUserId="
				+ destUserId + ", count=" + count + ", amount=" + amount
				+ ", type=" + type + ", timeCreated=" + timeCreated
				+ ", assets=" + assets + "]";
	}
}
