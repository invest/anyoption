package com.copyop.common.dto;

import java.util.ArrayList;
import java.util.UUID;

public class ProfileInvResultCopy {
	Long userId;
	ArrayList<Boolean> results;
	ArrayList<UUID> timeCreated;

	public ProfileInvResultCopy(long userId, int limit){
		this.userId = userId;
		this.results = new ArrayList<Boolean>(limit);
		this.timeCreated = new ArrayList<UUID>(limit);
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public ArrayList<Boolean> getResults() {
		return results;
	}
	public void setResults(ArrayList<Boolean> results) {
		this.results = results;
	}
	/**
	 * @return the timeCreated
	 */
	public ArrayList<UUID> getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(ArrayList<UUID> timeCreated) {
		this.timeCreated = timeCreated;
	}
	@Override
	public String toString() {
		return "ProfileInvResultCopy [userId=" + userId + ", results="
				+ results + "]";
	}


}
