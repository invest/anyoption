package com.copyop.common.dto;

import java.util.Date;

public class CoinsBalanceHistory {
	private long userId;
	private Date timeCreated;
	private int actionType;
	private boolean actionTypeBE;
	private long coinsBalance;
	private String comment;
	private boolean isRegranted;
	private long writerId;
	private Long amount;

	public CoinsBalanceHistory(){
		
	}
	
	public CoinsBalanceHistory(long userId, int actionType, boolean actionTypeBE, long coinsBalances, String comment, long writerId){
		this.userId = userId;
		this.actionType = actionType;
		this.actionTypeBE = actionTypeBE;
		this.coinsBalance = coinsBalances;
		this.comment = comment;
		this.writerId = writerId;
	}
	
	public CoinsBalanceHistory(long userId, int actionType, boolean actionTypeBE, long coinsBalance, long amount, String comment, long writerId){
		this.userId = userId;
		this.actionType = actionType;
		this.actionTypeBE = actionTypeBE;
		this.coinsBalance = coinsBalance;
		this.amount = amount;
		this.comment = comment;
		this.writerId = writerId;
	}
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public int getActionType() {
		return actionType;
	}

	public void setActionType(int actionType) {
		this.actionType = actionType;
	}

	public boolean isActionTypeBE() {
		return actionTypeBE;
	}

	public void setActionTypeBE(boolean actionTypeBE) {
		this.actionTypeBE = actionTypeBE;
	}

	public long getCoinsBalance() {
		return coinsBalance;
	}

	public void setCoinsBalance(long coinsBalance) {
		this.coinsBalance = coinsBalance;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isRegranted() {
		return isRegranted;
	}

	public void setRegranted(boolean isRegranted) {
		this.isRegranted = isRegranted;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}


	@Override
	public String toString() {
		return "CoinsBalanceHistory [userId=" + userId + ", timeCreated="
				+ timeCreated + ", actionType=" + actionType
				+ ", actionTypeBE=" + actionTypeBE + ", coinsBalance="
				+ coinsBalance + ", comment=" + comment + ", isRegranted="
				+ isRegranted + ", writerId=" + writerId + ", amount=" + amount + "]";
	}

}
