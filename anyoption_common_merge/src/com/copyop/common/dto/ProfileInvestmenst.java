package com.copyop.common.dto;

import java.util.Date;

public class ProfileInvestmenst {
    private Long userId;
    private Long marketIds;
    private Boolean results ;
    private Date timeCreated;
    private Date investmentsTimeCreated;

	public ProfileInvestmenst() {
		
	}
	
    public ProfileInvestmenst(long userId, long marketIds, boolean results, Date timeCreated, Date investmentsTimeCreated) {
    	this.userId = userId;
		this.setMarketIds(marketIds);
		this.setResults(results);
		this.setTimeCreated(timeCreated);
		this.setInvestmentsTimeCreated(investmentsTimeCreated);
    }

	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getMarketIds() {
		return marketIds;
	}

	public void setMarketIds(Long marketIds) {
		this.marketIds = marketIds;
	}

	public Boolean getResults() {
		return results;
	}

	public void setResults(Boolean results) {
		this.results = results;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getInvestmentsTimeCreated() {
		return investmentsTimeCreated;
	}

	public void setInvestmentsTimeCreated(Date investmentsTimeCreated) {
		this.investmentsTimeCreated = investmentsTimeCreated;
	}
}
