package com.copyop.common.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class ProfileInvResultSelf {
    Long userId;
    ArrayList<Long> marketIds;
    ArrayList<Boolean> results ;
    ArrayList<Date> timeCreated;
    ArrayList<Date> investmentsTimeCreated;
    ArrayList<UUID> timeUUIDCreated;

	public ProfileInvResultSelf() {

	}

    public ProfileInvResultSelf(long userId, int limit) {
    	this.userId = userId;
		this.results = new ArrayList<Boolean>(limit);
		this.marketIds = new ArrayList<Long>(limit);
		this.timeCreated =  new ArrayList<Date>(limit);
		this.investmentsTimeCreated =  new ArrayList<Date>(limit);
		this.timeUUIDCreated = new ArrayList<UUID>(limit);
    }

	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public ArrayList<Long> getMarketIds() {
		return marketIds;
	}
	public void setMarketIds(ArrayList<Long> marketIds) {
		this.marketIds = marketIds;
	}
	public ArrayList<Boolean> getResults() {
		return results;
	}
	public void setResults(ArrayList<Boolean> results) {
		this.results = results;
	}

	public ArrayList<Date> getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(ArrayList<Date> timeCreated) {
		this.timeCreated = timeCreated;
	}

	public ArrayList<Date> getInvestmentsTimeCreated() {
		return investmentsTimeCreated;
	}

	public void setInvestmentsTimeCreated(ArrayList<Date> investmentsTimeCreated) {
		this.investmentsTimeCreated = investmentsTimeCreated;
	}

	/**
	 * @return the timeUUIDCreated
	 */
	public ArrayList<UUID> getTimeUUIDCreated() {
		return timeUUIDCreated;
	}

	/**
	 * @param timeUUIDCreated the timeUUIDCreated to set
	 */
	public void setTimeUUIDCreated(ArrayList<UUID> timeUUIDCreated) {
		this.timeUUIDCreated = timeUUIDCreated;
	}

	@Override
	public String toString() {
		return "ProfileInvResultSelf [userId=" + userId + ", marketIds="
				+ marketIds + ", results=" + results + ", timeCreated =" + timeCreated +
				", investmentsTimeCreated =" + investmentsTimeCreated + "]";
	}

}
