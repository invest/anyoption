package com.copyop.common.dto;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.anyoption.common.annotations.AnyoptionNoJSON;

public class Profile extends com.copyop.common.dto.base.Profile {

	private static final long serialVersionUID = -3570206812993242038L;
	public static final int RANDOM_HOT_TRADERS_SIZE = 5;
	public static final int RANDOM_SIMILAR_TRADERS_SIZE = 10;
	@AnyoptionNoJSON
	long currentStreakAmount;
	@AnyoptionNoJSON
	int currentStreak;
	@AnyoptionNoJSON
	Date timeCreated;
	@AnyoptionNoJSON
	Date timeUserCreated;
	@AnyoptionNoJSON
	UUID lastTimeChanged;
	@AnyoptionNoJSON
	List<Float> riskAppetite;
	@AnyoptionNoJSON
	Map<String, Integer> tradesHistory;
	@AnyoptionNoJSON
	Map<Long, Long> assetsResults;	
	private boolean online;
	
	private UUID lastMonthInvestment;

	public Profile() {

	}

	public Profile(long userId, String nickname, String avatar, String fbId, boolean acceptedTermsAndConditions, boolean isTestAccount) {
		super(userId, nickname, avatar, fbId, acceptedTermsAndConditions, isTestAccount);
	}
	
	public Profile(long userId, String avatar, String firstName, String lastName, float hitRate, long copiers, long watchers) {
		super(userId, avatar, firstName, lastName, hitRate, copiers, watchers);
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public List<Float> getRiskAppetite() {
		return riskAppetite;
	}

	public void setRiskAppetite(List<Float> riskAppetite) {
		this.riskAppetite = riskAppetite;
	}

	public int getCurrentStreak() {
		return currentStreak;
	}

	public void setCurrentStreak(int currentStreak) {
		this.currentStreak = currentStreak;
	}

	public Map<Long, Long> getAssetsResults() {
		return assetsResults;
	}

	public void setAssetsResults(Map<Long, Long> assetsResults) {
		this.assetsResults = assetsResults;
	}

	public long getCurrentStreakAmount() {
		return currentStreakAmount;
	}

	public void setCurrentStreakAmount(long currentStreakAmount) {
		this.currentStreakAmount = currentStreakAmount;
	}

	public UUID getLastTimeChanged() {
		return lastTimeChanged;
	}

	public void setLastTimeChanged(UUID lastTimeChanged) {
		this.lastTimeChanged = lastTimeChanged;
	}

	public Map<String, Integer> getTradesHistory() {
		return tradesHistory;
	}

	public void setTradesHistory(Map<String, Integer> tradesHistory) {
		this.tradesHistory = tradesHistory;
	}
	
	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	public Date getTimeUserCreated() {
		return timeUserCreated;
	}

	public void setTimeUserCreated(Date timeUserCreated) {
		this.timeUserCreated = timeUserCreated;
	}

	@Override
	public String toString() {
		return "Profile [currentStreakAmount=" + currentStreakAmount
				+ ", currentStreak=" + currentStreak + ", timeCreated="
				+ timeCreated + ", timeUserCreated=" + timeUserCreated
				+ ", lastTimeChanged=" + lastTimeChanged + ", riskAppetite="
				+ riskAppetite + ", tradesHistory=" + tradesHistory
				+ ", assetsResults=" + assetsResults + ", online=" + online
				+ ", lastMonthInvestment=" + lastMonthInvestment + "]";
	}

}