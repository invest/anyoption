package com.copyop.common.dto;

import java.util.Date;
import java.util.Set;

import com.copyop.common.enums.base.ProfileLinkTypeEnum;

public class ProfileLink {
	long userId;
	long destUserId;
	long writerId;
	int count;
	int countReached;
	int amount;
	ProfileLinkTypeEnum type; 
	Date timeCreated ;
	Set<Long> assets;
	boolean watchingPushNotification;		

	public ProfileLink() {
		
	}
	
	public ProfileLink(long userId, long destUserId, ProfileLinkTypeEnum type, boolean watchingPushNotification) {
		this.userId = userId;
		this.destUserId = destUserId;
		this.type = type;
		this.watchingPushNotification = watchingPushNotification;
	}
	
	public ProfileLink(long userId, ProfileLinkTypeEnum type, long destUserId,
			int count, int amount, Set<Long> assets) {
		super();
		this.userId = userId;
		this.type = type;
		this.destUserId = destUserId;
		this.count = count;
		this.amount = amount;
		this.assets = assets;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public ProfileLinkTypeEnum getType() {
		return type;
	}
	public void setType(ProfileLinkTypeEnum type) {
		this.type = type;
	}
	public long getDestUserId() {
		return destUserId;
	}
	public void setDestUserId(long destUserId) {
		this.destUserId = destUserId;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public Set<Long> getAssets() {
		return assets;
	}
	public void setAssets(Set<Long> assets) {
		this.assets = assets;
	}
	public int getCountReached() {
		return countReached;
	}
	public void setCountReached(int countReached) {
		this.countReached = countReached;
	}
	
	public boolean isWatchingPushNotification() {
		return watchingPushNotification;
	}

	public void setWatchingPushNotification(boolean watchingPushNotification) {
		this.watchingPushNotification = watchingPushNotification;
	}

	public int getCountDown() {
		return count - countReached;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	@Override
	public String toString() {
		return "ProfileLink [userId=" + userId + ", destUserId=" + destUserId
				+ ", writerId=" + writerId + ", count=" + count
				+ ", countReached=" + countReached + ", amount=" + amount
				+ ", type=" + type + ", timeCreated=" + timeCreated
				+ ", assets=" + assets + ", watchingPushNotification="
				+ watchingPushNotification + "]";
	}
}
