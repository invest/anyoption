package com.copyop.common.dto;

/**
 * @author kirilim
 */
public class ProfileRelations {

	private long userId;
	private long copiers;
	private long copying;
	private long watchers;
	private long watching;
	private long share;
	private long rate;
	private long copiedInvestments;

	public ProfileRelations(long userId) {
		this.userId = userId;
	}
	

	public ProfileRelations(long userId, long copiers, long watchers) {
		super();
		this.userId = userId;
		this.copiers = copiers;
		this.watchers = watchers;
	}


	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getCopiers() {
		return copiers;
	}

	public void setCopiers(long copiers) {
		this.copiers = copiers;
	}

	public long getCopying() {
		return copying;
	}

	public void setCopying(long copying) {
		this.copying = copying;
	}

	public long getWatchers() {
		return watchers;
	}

	public void setWatchers(long watchers) {
		this.watchers = watchers;
	}

	public long getWatching() {
		return watching;
	}

	public void setWatching(long watching) {
		this.watching = watching;
	}

	public long getShare() {
		return share;
	}

	public void setShare(long share) {
		this.share = share;
	}


	public long getRate() {
		return rate;
	}

	public void setRate(long rate) {
		this.rate = rate;
	}


	public long getCopiedInvestments() {
		return copiedInvestments;
	}


	public void setCopiedInvestments(long copiedInvestments) {
		this.copiedInvestments = copiedInvestments;
	}
}