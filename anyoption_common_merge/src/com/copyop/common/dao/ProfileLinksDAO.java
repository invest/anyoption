/**
 *
 */
package com.copyop.common.dao;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.copyop.common.dto.ProfileLink;
import com.copyop.common.dto.ProfileLinkHistory;
import com.copyop.common.enums.ProfileLinkChangeReasonEnum;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

/**
 * @author kirilim
 */
public class ProfileLinksDAO extends DAOBase {

	private static final Logger log = Logger.getLogger(ProfileLinksDAO.class);

	public static void updateReachedCound(Session session, ProfileLink link) {
		PreparedStatement statement = methodsPreparedStatement.get("updateReachedCound");
		if(statement == null) {
			String cql = " UPDATE profile_links " +
						 " SET count_reached = ? " +
						 " WHERE user_id = ? AND dest_user_id = ? and type = ?";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("updateReachedCound", statement);
		}
		BoundStatement boundStatement = new BoundStatement(statement);
		session.execute(boundStatement.bind(link.getCountReached(), link.getUserId(), link.getDestUserId(), link.getType().getCode()));
	}

	public static ProfileLink getProfileLink(Session session, long userId, long destUserId, ProfileLinkTypeEnum type) {
		PreparedStatement ps = methodsPreparedStatement.get("getProfileLink");
		if (ps == null) {
			String cql = "SELECT * FROM profile_links WHERE user_id = ? AND dest_user_id = ? AND type = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfileLink", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement.bind(userId, destUserId, type.getCode()));
		if (!rs.isExhausted()) {
			return loadProfileLink(rs.one());
		}
		return null;
	}

	public static List<ProfileLink> getProfileLinksByType(Session session, long userId, ProfileLinkTypeEnum type) {
		List<ProfileLink> result = new ArrayList<ProfileLink>();
		PreparedStatement ps = methodsPreparedStatement.get("getProfileLinksByType");
		if (ps == null) {
			String cql = "SELECT * FROM profile_links WHERE user_id = ? AND type = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfileLinksByType", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId, type.getCode()));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		while (!rs.isExhausted()) {
			result.add(loadProfileLink(rs.one()));
		}
		return result;
	}

	public static List<Long> getUserIdLinksByType(Session session, long userId, ProfileLinkTypeEnum type) {
		List<Long> result = new ArrayList<Long>();
		PreparedStatement ps = methodsPreparedStatement.get("getUserIdLinksByType");
		if (ps == null) {
			String cql = "SELECT * FROM profile_links WHERE user_id = ? AND type = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getUserIdLinksByType", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId, type.getCode()));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		while (!rs.isExhausted()) {
			result.add(rs.one().getLong("dest_user_id"));
		}
		return result;
	}

	public static void insertProfileLinkHistory(Session session, ProfileLink link, ProfileLinkChangeReasonEnum reason) {
		if(link == null) {
			log.error("empty link - skip");
		}
		PreparedStatement ps = methodsPreparedStatement.get("insertProfileLinkHistory");
		if (ps == null) {
			String cql =
					"INSERT INTO " +
							" profile_links_history( " +
														"user_id, " +
														"type, " +
														"time_created, " +
														"amount, " +
														"assets, " +
														"count, " +
														"count_reached, " +
														"dest_user_id, " +
														"reason_done, " +
														"time_done, " +
														"watching_push_notification" +
													")" +
					" VALUES(?, ?, dateof(now()), ?, ?, ?, ?, ?, ?, dateof(now()), ?);";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertProfileLinkHistory", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		session.execute(boundStatement.bind(link.getUserId(), link.getType().getCode(), link.getAmount(), link.getAssets(),
											link.getCount(), link.getCountReached(), link.getDestUserId(), reason.getCode(),
											link.isWatchingPushNotification()));
		link.isWatchingPushNotification();
	}

	public static List<Long> getProfileLinkIdsByTypes(Session session, long profileUserId, List<Integer> types) {
		List<Long> result = new ArrayList<Long>();
		PreparedStatement ps = methodsPreparedStatement.get("getProfileLinkIdsByTypes");
		if (ps == null) {
			String cql = "SELECT * FROM profile_links where user_id = ? AND type IN ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfileLinkIdsByTypes", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(profileUserId, types));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		while (!rs.isExhausted()) {
			result.add(rs.one().getLong("dest_user_id"));
		}
		return result;
	}

	public static ArrayList<ProfileLink> getProfileLinks(Session session, long userId) {
		ArrayList<ProfileLink> list = new ArrayList<ProfileLink>();
		PreparedStatement ps = methodsPreparedStatement.get("getProfileLinks");
		if (ps == null) {
			String cql = "SELECT * FROM profile_links " +
								" WHERE user_id = ? " +
							" AND " +
								" type IN ("+ProfileLinkTypeEnum.COPIED_BY.getCode()+","+ProfileLinkTypeEnum.WATCHED_BY.getCode()+")";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfileLinks", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		List<Row> rows = rs.all();
		for (Row one : rows) {
			list.add(loadProfileLink(one));
		}
		return list;
	}

	public static List<ProfileLink> getProfileLinks(Session session, long userId, long destUserId, List<ProfileLinkTypeEnum> types) {
		List<ProfileLink> result = new ArrayList<ProfileLink>();
		PreparedStatement statement = methodsPreparedStatement.get("getProfileLinksByTypes");
		if (statement == null) {
			String cql = "SELECT * FROM profile_links WHERE user_id = ? AND dest_user_id = ? AND type IN ?";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("getProfileLinksByTypes", statement);
		}
		BoundStatement boundStatement = new BoundStatement(statement);
		List<Integer> list = new ArrayList<Integer>();
		for (ProfileLinkTypeEnum type : types) {
			list.add(type.getCode());
		}
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId, destUserId, list));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		while(!rs.isExhausted()) {
			result.add(loadProfileLink(rs.one()));
		}
		return result;
	}

	public static void deleteProfileLink(Session session, long userId, long destUserId, ProfileLinkTypeEnum linkType) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteProfileLink");
		if (ps == null) {
			String cql = "DELETE from profile_links WHERE user_id = ? AND dest_user_id = ? AND type = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("deleteProfileLink", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId, destUserId, linkType.getCode()));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
	}

	public static Map<Long, Boolean> getWatchersIds(Session session, long userId) {
		Map<Long, Boolean> result = new LinkedHashMap<Long, Boolean>();
		PreparedStatement ps = methodsPreparedStatement.get("getWatchersIds");
		if (ps == null) {
			String cql = "SELECT dest_user_id, watching_push_notification FROM profile_links WHERE user_id = ? and type = " + ProfileLinkTypeEnum.WATCHED_BY.getCode();
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getWatchersIds", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement.bind(userId));
		List<Row> rows = rs.all();
		for(Row row : rows) {
			result.put(row.getLong("dest_user_id"), row.getBool("watching_push_notification"));
		}
		return result;	}

	public static List<Long> getCopiersIds(Session session, long userId) {
		List<Long> result = new ArrayList<Long>();
		PreparedStatement ps = methodsPreparedStatement.get("getCopiersIds");
		if (ps == null) {
			String cql = "SELECT * FROM profile_links WHERE user_id = ? and type = " + ProfileLinkTypeEnum.COPIED_BY.getCode();
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getCopiersIds", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement.bind(userId));
		List<Row> rows = rs.all();
		for(Row row:rows) {
			result.add(row.getLong("dest_user_id"));
		}
		return result;
	}


	public static List<Long> getCopyingIds(Session session, long userId) {
		List<Long> result = new ArrayList<Long>();
		PreparedStatement statement = methodsPreparedStatement.get("getCopyingIds");
		if (statement == null) {
			String cql = "SELECT dest_user_id FROM profile_links WHERE user_id = ? and type = " + ProfileLinkTypeEnum.COPY.getCode();
			statement = session.prepare(cql);
			methodsPreparedStatement.put("getCopyingIds", statement);
		}
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs = session.execute(boundStatement.bind(userId));
		while (!rs.isExhausted()) {
			Row row = rs.one();
			result.add(row.getLong(0));
		}
		return result;
	}

	public static void deleteCopyProfileLinkBatch(Session session, long userId,	long destUserId) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteCopyProfileLinkBatch");
		if (ps == null) {
			String cql = "DELETE from profile_links WHERE user_id = ? AND dest_user_id = ? AND type = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("deleteCopyProfileLinkBatch", ps);
		}

		BatchStatement batch = new BatchStatement();
		batch.add(ps.bind(userId, destUserId, ProfileLinkTypeEnum.COPY.getCode()));
		batch.add(ps.bind(destUserId, userId, ProfileLinkTypeEnum.COPIED_BY.getCode()));
		session.execute(batch);
	}

	public static void deleteWatchProfileLinkBatch(Session session, long userId, long destUserId) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteWatchProfileLinkBatch");
		if (ps == null) {
			String cql = "DELETE from profile_links WHERE user_id = ? AND dest_user_id = ? AND type = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("deleteWatchProfileLinkBatch", ps);
		}

		BatchStatement batch = new BatchStatement();
		batch.add(ps.bind(userId, destUserId, ProfileLinkTypeEnum.WATCH.getCode()));
		batch.add(ps.bind(destUserId, userId, ProfileLinkTypeEnum.WATCHED_BY.getCode()));
		session.execute(batch);
	}

	public static void insertWatchProfileLinkBatch(Session session, long userId, long destUserId, boolean isWatchingPushNotification) {
		PreparedStatement ps = methodsPreparedStatement.get("insertWatchProfileLinkBatch");
		if (ps == null) {
			String cql = "INSERT INTO profile_links(user_id, type, dest_user_id, amount, assets, count, count_reached, time_created, watching_push_notification, writer_id) "
							+ "VALUES(?, ?, ?, ?, ?, ?, ?, dateof(now()), ?, ?)";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertWatchProfileLinkBatch", ps);
		}

		BatchStatement batch = new BatchStatement();
		batch.add(ps.bind(userId, ProfileLinkTypeEnum.WATCH.getCode(), destUserId, 0, null, 0, 0, isWatchingPushNotification, null));
		batch.add(ps.bind(destUserId, ProfileLinkTypeEnum.WATCHED_BY.getCode(), userId, 0, null, 0, 0, isWatchingPushNotification, null));
		session.execute(batch);
	}

	public static void insertCopyProfileLinkBatch(Session session, long userId, long destUserId, int amount, int maxInv, Set<Long> assets,
													long writerId) {
		PreparedStatement ps = methodsPreparedStatement.get("insertCopyProfileLinkBatch");
		if (ps == null) {
			String cql = "INSERT INTO profile_links(user_id, type, dest_user_id, amount, assets, count, count_reached, time_created, watching_push_notification, writer_id) "
							+ "VALUES(?, ?, ?, ?, ?, ?, ?, dateof(now()), ?, ?)";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertCopyProfileLinkBatch", ps);
		}

		BatchStatement batch = new BatchStatement();
		batch.add(ps.bind(userId, ProfileLinkTypeEnum.COPY.getCode(), destUserId, amount, assets, maxInv, 0, false, writerId));
		batch.add(ps.bind(destUserId, ProfileLinkTypeEnum.COPIED_BY.getCode(), userId, amount, assets, maxInv, 0, false, writerId));
		session.execute(batch);
	}
	
	public static void updateCopyProfileLinkWithoutCountBatch(Session session, long userId, long destUserId, int amount, Set<Long> assets,
																long writerId) {
		PreparedStatement ps = methodsPreparedStatement.get("updateCopyProfileLinkWithoutCountBatch");
		if (ps == null) {
			String cql = " UPDATE profile_links " +
						 " SET " +
						 	 " amount = ?, " +
						 	 " assets = ?, " +
						 	 " time_created = dateof(now()), " +
						 	 " watching_push_notification = ?, " +
						 	 " writer_id = ? " +
						 " WHERE user_id = ? AND dest_user_id = ? and type = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateCopyProfileLinkWithoutCountBatch", ps);
		}

		BatchStatement batch = new BatchStatement();
		batch.add(ps.bind(amount, assets, false, writerId, userId, destUserId, ProfileLinkTypeEnum.COPY.getCode()));
		batch.add(ps.bind(amount, assets, false, writerId, destUserId, userId, ProfileLinkTypeEnum.COPIED_BY.getCode()));
		session.execute(batch);
	}

	public static ArrayList<ProfileLinkHistory> getProfileLinksHistory(Session session, long userId, ProfileLinkTypeEnum type) {
		PreparedStatement ps = methodsPreparedStatement.get("getProfileLinksHistoryByType");
		ArrayList<ProfileLinkHistory> profileLinkHistory = new ArrayList<ProfileLinkHistory>();
		if (ps == null) {
			String cql =
					"SELECT " +
					"	* " +
					"FROM " +
					"	profile_links_history " +
					"WHERE " +
					"	user_id = ? " +
					"	AND type = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfileLinksHistoryByType", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId, type.getCode());
		ResultSet resultSet = session.execute(boundStatement);

		while (!resultSet.isExhausted()) {
			profileLinkHistory.add(getProfileLinkHistoryVO(resultSet.one()));
		}
		return profileLinkHistory;
	}

	private static ProfileLinkHistory getProfileLinkHistoryVO(Row row) {
		ProfileLinkHistory profileLinkHistory = new ProfileLinkHistory();
		profileLinkHistory.setUserId(row.getLong("user_id"));
		profileLinkHistory.setType(ProfileLinkTypeEnum.of(row.getInt("type")));
		profileLinkHistory.setTimeCreated(row.getDate("time_created"));
		profileLinkHistory.setAmount(row.getInt("amount"));
		profileLinkHistory.setAssets(row.getSet("assets", Long.class));
		profileLinkHistory.setCount(row.getInt("count"));
		profileLinkHistory.setDestUserId(row.getLong("dest_user_id"));
		profileLinkHistory.setResonDone(ProfileLinkChangeReasonEnum.of(row.getInt("reason_done")));
		profileLinkHistory.setTimeDone(row.getDate("time_done"));

		return profileLinkHistory;
	}

	private static ProfileLink loadProfileLink(Row row) {
		ProfileLink pl = new ProfileLink();
		pl.setUserId(row.getLong("user_id"));
		pl.setType(ProfileLinkTypeEnum.of(row.getInt("type")));
		pl.setDestUserId(row.getLong("dest_user_id"));
		pl.setTimeCreated(row.getDate("time_created"));
		pl.setCount(row.getInt("count"));
		pl.setCountReached(row.getInt("count_reached"));
		pl.setAmount(row.getInt("amount"));
		pl.setAssets(row.getSet("assets", Long.class));
		pl.setWriterId(row.getLong("writer_id"));
		pl.setWatchingPushNotification(row.getBool("watching_push_notification"));
		return pl;
	}

	public static List<Long> getListWatchersIds(Session session, long userId) {
		ArrayList<Long> result = new ArrayList<Long>();
		PreparedStatement ps = methodsPreparedStatement.get("getListWatchersIds");
		if (ps == null) {
			String cql = "SELECT dest_user_id, watching_push_notification FROM profile_links WHERE user_id = ? and type = " + ProfileLinkTypeEnum.WATCHED_BY.getCode();
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getListWatchersIds", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement.bind(userId));
		List<Row> rows = rs.all();
		for(Row row : rows) {
			result.add(row.getLong("dest_user_id"));
		}
		return result;
	}

	public static void updateReachedCound(Session session, int countReached, long userId, long destUserId, ProfileLinkTypeEnum type) {
		PreparedStatement statement = methodsPreparedStatement.get("updateReachedCound");
		if(statement == null) {
			String cql = " UPDATE profile_links " +
						 " SET count_reached = ? " +
						 " WHERE user_id = ? AND dest_user_id = ? and type = ?";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("updateReachedCound", statement);
		}
		BoundStatement boundStatement = new BoundStatement(statement);
		session.execute(boundStatement.bind(countReached, userId, destUserId, type.getCode()));
	}

	public static void deleteAllProfileLinks(Session session, long userId) {
		PreparedStatement statement = methodsPreparedStatement.get("deleteAllProfileLinks");
		if(statement == null) {
			String cql = "DELETE FROM profile_links WHERE user_id = ? AND type IN (1,2,3,4)";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("deleteAllProfileLinks", statement);
		}
		BoundStatement boundStatement = new BoundStatement(statement);
		session.execute(boundStatement.bind(userId));
	}
	
	public static long countProfileLinks(Session session, long userId, int type) {
		PreparedStatement statement = methodsPreparedStatement.get("countProfileLinks");
		if(statement == null) {
			String cql = "SELECT COUNT(*) FROM profile_links WHERE user_id = ? AND type = ?";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("countProfileLinks", statement);
		}
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs = session.execute(boundStatement.bind(userId, type));
		if(!rs.isExhausted()) {
			Row row = rs.one();
			return row.getLong(0);
		} else {
			log.debug("No links for user:"+userId);
			return -1;
		}
	}
}