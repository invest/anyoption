/**
 *
 */
package com.copyop.common.dao;

import org.apache.log4j.Logger;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

/**
 * @author pavelhe
 *
 */
public class AppoxeeDAO extends DAOBase {

	private static final Logger log = Logger.getLogger(AppoxeeDAO.class);

	public static Long getUserSegment(Session session, long userId, long applicationId) {
		Long result = null;
		PreparedStatement ps = methodsPreparedStatement.get("getUserSegment");
		if (ps == null) {
			ps = session.prepare(" SELECT * FROM appoxee_user_segments WHERE user_id = ? AND application_id = ? ");
			methodsPreparedStatement.put("getUserSegment", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId, applicationId));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}

		if (!rs.isExhausted()) {
			result = rs.one().getLong("segment_id");
		}
		return result;
	}

	public static void insertUserSegment(Session session, long userId, long applicationId, long segmentId) {
		PreparedStatement ps = methodsPreparedStatement.get("insertUserSegment");
		if (ps == null) {
			String cql = " INSERT INTO appoxee_user_segments (user_id, application_id, segment_id) VALUES (?, ?, ?) IF NOT EXISTS ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertUserSegment", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}

		ResultSet rs = session.execute(boundStatement.bind(userId, applicationId, segmentId));
		Row row = rs.one();
		if(!row.getBool(APPLIED)) {
			log.warn("Insert not applied. Record already exists with segment_id [" + row.getLong("segment_id") + "]");
		}
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
	}

	public static void updateUserSegmentIf(Session session, long userId, long applicationId, long newSegmentId, long oldSegmentId) {
		PreparedStatement ps = methodsPreparedStatement.get("updateUserSegmentIf");
		if (ps == null) {
			String cql = " UPDATE appoxee_user_segments SET segment_id = ? WHERE user_id = ? AND application_id = ? IF segment_id = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateUserSegmentIf", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}

		ResultSet rs = session.execute(boundStatement.bind(newSegmentId, userId, applicationId, oldSegmentId));
		Row row = rs.one();
		if(!row.getBool(APPLIED)) {
			log.warn("Update not applied. Record already exists with segment_id [" + row.getLong("segment_id") + "]");
		}
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
	}

}
