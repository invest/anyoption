package com.copyop.common.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class LastViewedDAO extends DAOBase {

	public static List<Long> getLastViewedUserIds(Session session, long userId, long limit) {
		PreparedStatement statement = methodsPreparedStatement.get("getLastViewedUserIds");
		if(statement == null ){
			String cql = "SELECT viewed_user_id FROM last_viewed WHERE user_id = ? LIMIT 100";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("getLastViewedUserIds", statement);
		}
		
		ArrayList<Long> result = new ArrayList<Long>();
		Set<Long> hsUserId = new HashSet<Long>();
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs = session.execute(boundStatement.bind(userId));
		while(!rs.isExhausted() && hsUserId.size() < limit) {
			Row row = rs.one();
			long uId = row.getLong(0);
			if(!hsUserId.contains(uId)){
				result.add(uId);
			}
			hsUserId.add(uId);			
		}
		return result;
	}
	
	public static void insertLastViewed(Session session, long userId, long viewedUserId) {
		PreparedStatement ps = methodsPreparedStatement.get("insertLastViewed");
		if (ps == null) {
			String cql = " insert into last_viewed " + 
							" (user_id, viewed_user_id, time_created) " +
						 " values " +
							" (?, ?, now()) ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertLastViewed", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId, viewedUserId);
		session.execute(boundStatement);
	}
}
