package com.copyop.common.dao;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.copyop.common.dto.CopiedInvestment;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class InvestmentDAO extends DAOBase {

	public static CopiedInvestment getCopiedInvestment(Session session, long userId, long marketId, long copiedFromUserId) {
		PreparedStatement ps = methodsPreparedStatement.get("getCopiedInvestment");
		if (ps == null) {
			String cql =
					"SELECT " +
								" * " +
					"FROM " +
							" copied_investments " +
					"WHERE " +
								" user_id = ? " +
							"AND " +
								" market_id = ? " +
							"AND " +
								" copy_from_user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getCopiedInvestment", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement.bind(userId, marketId, copiedFromUserId));
		if (!rs.isExhausted()) {
			return loadCopiedInvestment(rs.one());
		} else {
			return null;
		}
	}

	public static boolean updateCopiedInvestment(Session session, CopiedInvestment cp, int copied_count_lock) {
		PreparedStatement ps = methodsPreparedStatement.get("insertCopiedInvestments");
		if (ps == null) {
			String cql =
					"UPDATE " +
							"copied_investments " +
					"SET " +
							" copied_count = ?, " +
							" profit = ?, " +
							" copied_count_success = ?, " +
							" success_rate = ? " +
					"WHERE " +
							" user_id = ? AND market_id = ? AND copy_from_user_id = ?";
					//"IF copied_count = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertCopiedInvestments", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		Row row = session.execute(boundStatement.bind(
												cp.getCopiedCount(),
												cp.getProfit(),
												cp.getCopiedCountSuccess(),
												cp.getSuccessRate(),
												cp.getUserId(),
												cp.getMarketId(),
												cp.getCopyFromUserId()//,
												//copied_count_lock
											)).one();

		//return(row.getBool(APPLIED));
		return true;
	}

	static CopiedInvestment loadCopiedInvestment(Row row){
		CopiedInvestment cp = new CopiedInvestment();
		cp.setUserId(row.getLong("user_id"));
		cp.setCopiedCount(row.getInt("copied_count"));
		cp.setCopiedCountSuccess(row.getInt("copied_count_success"));
		cp.setSuccessRate(row.getFloat("success_rate"));
		cp.setCopyFromUserId(row.getLong("copy_from_user_id"));
		cp.setMarketId(row.getLong("market_id"));
		cp.setProfit(row.getLong("profit"));
		return cp;
	}

	public static void updateCopiedInvestmentProfit(Session session, long profit, long userId, long marketId, long copiedFromUserId) {
		PreparedStatement ps = methodsPreparedStatement.get("updateCopiedInvestmentProfit");
		if (ps == null) {
			String cql = "UPDATE copied_investments " +
							" SET profit = ? " +
							" WHERE user_id = ? AND market_id = ? AND copy_from_user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateCopiedInvestmentProfit", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		session.execute(boundStatement.bind(profit, userId,	marketId, copiedFromUserId));
	}

	public static TreeMap<Long, List<CopiedInvestment>> getMarketsByProfit(Session session,
																			long userId,
																			long copiedFromUserId) {
		String statementName = "getMarketsByProfit";
		TreeMap<Long, List<CopiedInvestment>> map = new TreeMap<Long, List<CopiedInvestment>>();
		PreparedStatement ps = methodsPreparedStatement.get(statementName);
		if (ps == null) {
			String cql = "SELECT * FROM "
							+ "copied_investments WHERE user_id = ? AND copy_from_user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put(statementName, ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement.bind(userId, copiedFromUserId));
		while (!rs.isExhausted()) {
			Row row = rs.one();
			long profit = row.getLong("profit");
			List<CopiedInvestment> ciList = map.get(profit);
			if (ciList == null) {
				ciList = new ArrayList<CopiedInvestment>();
				map.put(profit, ciList);
			}
			ciList.add(loadCopiedInvestment(row));
		}
		return map;
	}

	public static Map<Long, List<CopiedInvestment>> getCopiedInvestments(Session session, long userId, List<Long> copyFromUserIds) {
		Map<Long, List<CopiedInvestment>> result = new LinkedHashMap<Long, List<CopiedInvestment>>();
		PreparedStatement statement = methodsPreparedStatement.get("getCopiedInvestments");
		if (statement == null) {
			String cql = "SELECT * FROM copied_investments WHERE user_id = ? AND copy_from_user_id IN ?";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("getCopiedInvestments", statement);
		}
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs = session.execute(boundStatement.bind(userId, copyFromUserIds));
		while (!rs.isExhausted()) {
			Row row = rs.one();
			long copyUserId = row.getLong("copy_from_user_id");
			if (result.containsKey(copyUserId)) {
				result.get(copyUserId).add(loadCopiedInvestment(row));
			} else {
				List<CopiedInvestment> list = new ArrayList<CopiedInvestment>();
				list.add(loadCopiedInvestment(row));
				result.put(copyUserId, list);
			}
		}
		return result;
	}

	public static BoundStatement getInsertBlockedCopiedInvestmentBS(Session session, CopiedInvestment copiedInvestment) {
		PreparedStatement ps = methodsPreparedStatement.get("insertBlockedCopiedInvestment");
		if (ps == null) {
			String cql = "INSERT INTO blocked_copied_investments (user_id"
																+ ",copy_from_user_id"
																+ ",market_id"
																+ ",copied_count"
																+ ",copied_count_success"
																+ ",profit"
																+ ",success_rate"
																+ ",time_created) "
					  + " VALUES (?, ?, ?, ?, ?, ?, ?, now())";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertBlockedCopiedInvestment", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		return boundStatement.bind(copiedInvestment.getUserId(),
									copiedInvestment.getCopyFromUserId(),
									copiedInvestment.getMarketId(),
									copiedInvestment.getCopiedCount(),
									copiedInvestment.getCopiedCountSuccess(),
									copiedInvestment.getProfit(),
									copiedInvestment.getSuccessRate());
	}

	public static BoundStatement getDeleteBlockedCopiedInvestmentBS(Session session, long userId, List<Long> copyFromUsers) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteBlockedCopiedInvestment");
		if (ps == null) {
			String cql = "DELETE FROM blocked_copied_investments WHERE user_id = ? AND copy_from_user_id IN ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("deleteBlockedCopiedInvestment", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		return boundStatement.bind(userId, copyFromUsers);
	}
}