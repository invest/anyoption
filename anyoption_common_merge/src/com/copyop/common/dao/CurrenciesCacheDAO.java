package com.copyop.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import com.anyoption.common.daos.DAOBase;

public class CurrenciesCacheDAO extends DAOBase  {
	
	public static HashMap<Long, Double> loadCurrenciesRatesToUSD(Connection connection) throws SQLException {
		HashMap<Long, Double> rates = new HashMap<Long, Double>();
	    PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    try {
			String sql = "SELECT cm.* , " + 
					  "CASE " +
					   " WHEN is_multiply_to_convert(market_id) = 1 " +
					   " THEN 1/get_last_closing_level(cm.market_id, sysdate) " +
					   " ELSE get_last_closing_level(cm.market_id, sysdate) " +
					  " END AS rate " +
					" FROM " +
					 " ( " +
							 " SELECT id, get_currency_market(id) AS market_id " +
							 " FROM currencies " +
							 " WHERE id != 2 " +
					 " )cm " ;
			pstmt = connection.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()){
				rates.put(rs.getLong("id"), rs.getDouble("rate"));
			}
		} finally {
	        closeResultSet(rs);
	        closeStatement(pstmt);
	    }
	    return rates;
	}
}
