package com.copyop.common.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.copyop.common.beans.UserUniqueDetails;
import com.copyop.common.dto.Frozen;
import com.copyop.common.dto.Profile;
import com.copyop.common.enums.base.UserStateEnum;
import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class ProfileDAO extends DAOBase {

	private static final Logger log = Logger.getLogger(ProfileDAO.class);


	public static Profile getProfile(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("getProfile");
		if (ps == null) {
			String cql = "SELECT * FROM profiles WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfile", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		if (!rs.isExhausted()) {
			return loadProfile(rs.one());
		} else {
			return null;
		}
	}

	public static Profile getBlockedProfile(Session session, Profile profile) {
		PreparedStatement ps = methodsPreparedStatement.get("getBlockedProfile");
		if (ps == null) {
			String cql = "SELECT * FROM blocked_profiles WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getBlockedProfile", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(profile.getUserId()));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		if (!rs.isExhausted()) {
			return loadBlockedProfile(rs.one(), profile);
		} else {
			return null;
		}
	}

	public static boolean isExistProfile(Session session, long userId) {
		boolean res = false;
		PreparedStatement ps = methodsPreparedStatement.get("isExistProfile");
		if (ps == null) {
			String cql =
					"SELECT " +
					"	user_id " +
					"FROM " +
					"	profiles " +
					"WHERE " +
					"	user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("isExistProfile", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		ResultSet resultSet = session.execute(boundStatement);
		if (!resultSet.isExhausted()) {
			res =  true;
		}
		return res;
	}

	public static List<Profile> getProfiles(Session session, List<Long> userIds) {
		List<Profile> result = new ArrayList<Profile>();
		PreparedStatement ps = methodsPreparedStatement.get("getProfiles");
		if (ps == null) {
			String cql = "SELECT * FROM profiles WHERE user_id IN ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfiles", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userIds));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		while (!rs.isExhausted()) {
			result.add(loadProfile(rs.one()));
		}
		return result;
	}

	public static void insertProfile(Session session, Profile profile) {
		PreparedStatement ps = methodsPreparedStatement.get("insertProfile");
		if (ps == null) {
			String cql = "INSERT INTO profiles(" +
													"user_id, assets_results, avatar, best_asset, best_streak, " +
													"current_streak, fb_id, hit_rate, is_accepted_terms_and_conditions, is_frozen, last_investments, " +
													"nickname, risk_appetite, best_streak_amount, current_streak_amount, is_test, " +
													"nickname_change, time_user_created, time_created, time_last_change) "
						+ "VALUES(" +
													" ?, ?, ?, ?, ?," +
													" ?, ?, ?, ?, ?, ?, " +
													" ?, ?, ?, ?, ?, " +
													" ?, ?, dateof(now()), now())";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertProfile", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(
													profile.getUserId(), profile.getAssetsResults(), profile.getAvatar(), profile.getBestAsset(), profile.getBestStreak(),
													profile.getCurrentStreak(), profile.getFbId(), profile.getHitRate(), profile.isAcceptedTermsAndConditions(), profile.isFrozen(), profile.getLastInvestments(),
													profile.getNickname(), profile.getRiskAppetite(), profile.getBestStreakAmount(),profile.getCurrentStreakAmount(), profile.isTest(),
													profile.isNicknameChanged(), profile.getTimeUserCreated()));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
	}

	public static BoundStatement getInsertBlockedProfileBS(Session session, Profile profile) {
		PreparedStatement ps = methodsPreparedStatement.get("insertBlockedProfile");
		if (ps == null) {
			String cql = "INSERT INTO blocked_profiles (user_id"
													+ ",time_created"
													+ ",hit_rate"
													+ ",current_streak"
													+ ",current_streak_amount"
													+ ",best_asset"
													+ ",best_streak"
													+ ",best_streak_amount"
													+ ",assets_results"
													+ ",last_investments"
													+ ",risk_appetite"
													+ ",trades_history)"
						+ " VALUES (?, now(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertBlockedProfile", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		return boundStatement.bind(profile.getUserId(),
									profile.getHitRate(),
									profile.getCurrentStreak(),
									profile.getCurrentStreakAmount(),
									profile.getBestAsset(),
									profile.getBestStreak(),
									profile.getBestStreakAmount(),
									profile.getAssetsResults(),
									profile.getLastInvestments(),
									profile.getRiskAppetite(),
									profile.getTradesHistory());
	}

	public static BoundStatement getDeleteBlockedProfileBS(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteBlockedProfile");
		if (ps == null) {
			String cql = "DELETE FROM blocked_profiles WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("deleteBlockedProfile", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		return boundStatement.bind(userId);
	}

	public static void loadProfileDetails(Session session, Long userId, Profile profile) {
		PreparedStatement ps = methodsPreparedStatement.get("loadProfileDetails");
		if (ps == null) {
			String cql = "SELECT nickname, avatar, is_frozen, user_state from profiles WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("loadProfileDetails", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		if (!rs.isExhausted()) {
			Row row = rs.one();
			profile.setNickname(row.getString("nickname"));
			/*
			 * Quick fix for null Avatars. If you let it null, Cassandra will throw exception because it does
			 * not support null in collections.
			 */
			profile.setAvatar("" + row.getString("avatar"));
			profile.setFrozen(row.getBool("is_frozen"));
			profile.setUserState(UserStateEnum.of(row.getInt("user_state")));
		}
	}

	public static boolean isFrozenTrader(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("isFrozenTrader");
		if (ps == null) {
			String cql = "SELECT is_frozen FROM profiles WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("isFrozenTrader", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		if (!rs.isExhausted()) {
			return rs.one().getBool("is_frozen");
		}
		return false;
	}

	static Profile loadProfile(Row row) {
		Profile p = new Profile();
		p.setUserId(row.getLong("user_id"));
		p.setNickname(row.getString("nickname"));
		p.setAvatar(row.getString("avatar"));
		p.setTimeCreated(row.getDate("time_created"));
		p.setAcceptedTermsAndConditions(row.getBool("is_accepted_terms_and_conditions"));
		p.setFrozen(row.getBool("is_frozen"));
		p.setTest(row.getBool("is_test"));
		p.setRiskAppetite(row.getList("risk_appetite", Float.class));
		p.setHitRate(row.getFloat("hit_rate"));
		p.setBestAsset(row.getLong("best_asset"));
		p.setBestStreakAmount(row.getLong("best_streak_amount"));
		p.setAssetsResults(row.getMap("assets_results", Long.class, Long.class));
		p.setBestStreak(row.getInt("best_streak"));
		p.setCurrentStreak(row.getInt("current_streak"));
		p.setCurrentStreakAmount(row.getLong("current_streak_amount"));
		p.setTradesHistory(row.getMap("trades_history", String.class, Integer.class));
		p.setLastInvestments(row.getList("last_investments", Long.class));
		p.setFbId(row.getString("fb_id"));
		p.setTimeUserCreated(row.getDate("time_user_created"));
		p.setLastTimeChanged(row.getUUID("time_last_change"));
		p.setNicknameChanged(row.getBool("nickname_change"));
		p.setTimeResetCoins(row.getUUID("time_reset_coins"));
		p.setUserState(UserStateEnum.of(row.getInt("user_state")));
		return p;
	}

	private static Profile loadBlockedProfile(Row row, Profile p) {
		p.setUserId(row.getLong("user_id"));
		p.setAssetsResults(row.getMap("assets_results", Long.class, Long.class));
		p.setBestAsset(row.getLong("best_asset"));
		p.setBestStreak(row.getInt("best_streak"));
		p.setBestStreakAmount(row.getLong("best_streak_amount"));
		p.setCurrentStreak(row.getInt("current_streak"));
		p.setCurrentStreakAmount(row.getLong("current_streak_amount"));
		p.setHitRate(row.getFloat("hit_rate"));
		p.setLastInvestments(row.getList("last_investments", Long.class));
		p.setRiskAppetite(row.getList("risk_appetite", Float.class));
		p.setTradesHistory(row.getMap("trades_history", String.class, Integer.class));
		return p;
	}

//	public static void updateAssetsResult(Session session, long assetId, long result, long userId) {
//		PreparedStatement ps = methodsPreparedStatement.get("updateAssetsResult");
//		if (ps == null) {
//			String cql = "UPDATE profiles SET assets_results[?] = ? WHERE user_id = ?";
//			ps = session.prepare(cql);
//			methodsPreparedStatement.put("updateAssetsResult", ps);
//		}
//		BoundStatement boundStatement = new BoundStatement(ps);
//		boundStatement.bind(assetId, result, userId);
//		session.execute(boundStatement);
//	}

	public static void changeCopyopNickname(Session session, long userId, String nickName) {
		PreparedStatement ps = methodsPreparedStatement.get("changeCopyopNickname");
		if (ps == null) {
			String cql =
					"UPDATE " +
					"	profiles " +
					"SET " +
					"	nickname = ?" +
					"	, nickname_change = true " +
					"WHERE " +
					"	user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("changeCopyopNickname", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(nickName, userId);
		session.execute(boundStatement);
	}

	public static boolean isCopyopNicknameChanged(Session session, long userId) {
		boolean res = false;
		PreparedStatement ps = methodsPreparedStatement.get("isCopyopNicknameChanged");
		if (ps == null) {
			String cql =
					"SELECT " +
					"	nickname_change " +
					"FROM " +
					"	profiles " +
					"WHERE " +
					"	user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("isCopyopNicknameChanged", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		ResultSet resultSet = session.execute(boundStatement);
		if (!resultSet.isExhausted()) {
			res =  resultSet.one().getBool("nickname_change");
		}
		return res;
	}

	public static boolean updateProfile(Session session, long userId, int currentStreak, long currentStreakAmount,
			int bestStreak, long bestStreakAmount, long assetResult, long marketId, long bestAsset, float hitRate, UUID time) {
		PreparedStatement ps = methodsPreparedStatement.get("updateProfile");
		if (ps == null) {
			String cql =
					"UPDATE profiles SET " +
									"assets_results[?] = ?, " +
									"best_asset = ?, " +
									"best_streak = ?, " +
									"best_streak_amount = ?, " +
									"current_streak = ?, " +
									"current_streak_amount = ?, " +
									"hit_rate = ?, " +
									"time_last_change = NOW() " +
					"WHERE user_id = ? " ;
					// LWT is replaced by profile lock
					//"IF time_last_change = ?"; // ensure consistency
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateProfile", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(marketId, assetResult, bestAsset, bestStreak, bestStreakAmount, currentStreak, currentStreakAmount, hitRate, userId/*, time*/);

		session.execute(boundStatement);
		return true;
	}

	public static void updateProfile(Session session, long userId, int currentStreak, long currentStreakAmount,
			int bestStreak, long bestStreakAmount, Map<Long, Long> assetResult, long bestAsset, float hitRate) {
		PreparedStatement ps = methodsPreparedStatement.get("updateProfile");
		if (ps == null) {
			String cql =
					"UPDATE profiles SET " +
									"assets_results = ?, " +
									"best_asset = ?, " +
									"best_streak = ?, " +
									"best_streak_amount = ?, " +
									"current_streak = ?, " +
									"current_streak_amount = ?, " +
									"hit_rate = ?, " +
									"time_last_change = NOW() " +
					"WHERE user_id = ? " ;
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateProfile", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(assetResult, bestAsset, bestStreak, bestStreakAmount, currentStreak, currentStreakAmount, hitRate, userId);

		session.execute(boundStatement);
	}

	/*
	 * when using batch :
	 * throws com.datastax.driver.core.exceptions.InvalidQueryException:
	 * Cannot include non-counter statement in a counter batch
	 */
	public static void updateProfile(Session session, List<Long> lastInvestments, List<Float> riskAppetite, String today, int value, long userId) {
		PreparedStatement statement1 = updateProfilePS(session);
		PreparedStatement statement2 = updateTradesHistoryPS(session);

		session.execute(statement1.bind(lastInvestments, riskAppetite, userId ));
		session.execute(statement2.bind((int)TimeUnit.DAYS.toSeconds(28), today, value, userId));

	}
	public static void updateProfileLastInvestments(Session session,List<Long> lastInvestments, long userId) {
		PreparedStatement statement = updateProfileLastInvestments(session);
		session.execute(statement.bind(lastInvestments, userId ));
	}
	public static void updateTradesHistory(Session session, int timeToLive, long userId, String formatedDate, int value) {
		PreparedStatement ps = updateTradesHistoryPS(session);
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(timeToLive, formatedDate, value, userId);
		session.execute(boundStatement);
	}

	public static void insertRiskAppetite(Session session, long userId, float riskAppetite) {
		PreparedStatement ps = insertRiskAppetite(session);
		BoundStatement boundStatement = new BoundStatement(ps);
		ArrayList<Float> ra = new ArrayList<Float>(selectRiskAppetite(session, userId));
		ra.add(riskAppetite);
		boundStatement.bind(ra, userId);
		session.execute(boundStatement);
	}

	public static List<Float> selectRiskAppetite(Session session, long userId) {
		PreparedStatement ps = selectRiskAppetite(session);
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		ResultSet rs = session.execute(boundStatement);
		if (!rs.isExhausted()) {
			return rs.one().getList("risk_appetite", Float.class);
		} else {
			return new ArrayList<Float>();
		}
	}

	static PreparedStatement insertRiskAppetite(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("insertRiskAppetite");
		if (ps == null) {
			String cql =
					"UPDATE profiles set risk_appetite = ? where user_id = ?";
							ps = session.prepare(cql);
			methodsPreparedStatement.put("insertRiskAppetite", ps);
		}
		return ps;
	}

	static PreparedStatement selectRiskAppetite(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("selectRiskAppetite");
		if (ps == null) {
			String cql =
					"SELECT risk_appetite FROM profiles where user_id = ?";
							ps = session.prepare(cql);
			methodsPreparedStatement.put("selectRiskAppetite", ps);
		}
		return ps;
	}


	static PreparedStatement updateTradesHistoryPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("updateTradesHistory");
		if (ps == null) {
			String cql =
					"UPDATE profiles USING TTL ? set trades_history[ ? ] = ? where user_id = ?";
							ps = session.prepare(cql);
			methodsPreparedStatement.put("updateTradesHistory", ps);
		}
		return ps;
	}

	static PreparedStatement updateProfilePS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("updateProfile2");
		if (ps == null) {
			String cql =
					"UPDATE profiles SET " +
									"last_investments = ?, " +
									"risk_appetite = ? " +
					"WHERE user_id = ? ";

			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateProfile2", ps);
		}
		return ps;
	}

	static PreparedStatement updateProfileLastInvestments(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("updateProfileLastInvestments");
		if (ps == null) {
			String cql =
					"UPDATE profiles SET " +
									"last_investments = ? " +
					"WHERE user_id = ? ";

			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateProfileLastInvestments", ps);
		}
		return ps;
	}

	public static void updateProfile(Session session, boolean frozen, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("updateProfileFrozen");
		if (ps == null) {
			String cql =  "UPDATE profiles SET is_frozen = ? WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateProfileFrozen", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(frozen, userId);
		session.execute(boundStatement);
	}

	public static void resetTimeConvertCoins(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("resetTimeConvertCoins");
		if (ps == null) {
			String cql =  "UPDATE profiles SET time_reset_coins = null WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("resetTimeConvertCoins", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		session.execute(boundStatement);

	}

	public static void updateFrozenTrader(Session session, List<Frozen> frozenList) {
		PreparedStatement ps = methodsPreparedStatement.get("updateIsFrozen");
		if (ps == null) {
			String cql =
					"UPDATE profiles SET " +
							"is_frozen = ? " +
					"WHERE user_id = ? ";

			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateIsFrozen", ps);
		}
		BatchStatement batch = new BatchStatement();
		for(Frozen fr : frozenList){
			if(isExistProfile(session, fr.getUserId())){
			batch.add(ps.bind(fr.isFrozen(), fr.getUserId()));
			} else {
				log.debug("Fozen not update in profiles for userId:" + fr.getUserId() + " is, beacuse Profile not created!");
			}
		}
		session.execute(batch);
	}

	public static void insertFrozenTraderTemp(Session session, List<Frozen> frozenList) {
		PreparedStatement ps = methodsPreparedStatement.get("insertFrozenProfileTemp");
		if (ps == null) {
			String cql =
					"INSERT into " +
					" frozen_profiles_temp (user_id, time_insert) " +
					" values(?, now());";

			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertFrozenProfileTemp", ps);
		}
		BatchStatement batch = new BatchStatement();
		for(Frozen fr : frozenList){
			batch.add(ps.bind(fr.getUserId()));
		}
		session.execute(batch);
	}

	public static ArrayList<Long> getFrozenTraders(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("getFrozenTraders");
		if (ps == null) {
			String cql = "SELECT user_id FROM frozen_profiles_temp ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getFrozenTraders", ps);
		}
		ResultSet rs = session.execute(ps.bind());
		List<Row> rows =  rs.all();
		ArrayList<Long> rez = new ArrayList<Long>();
		for(Row row:rows){
			rez.add(row.getLong(0));
		}
		return rez;
	}

	public static void updateProfile(Session session, UUID timeConvertCoins, long user_id) {
		PreparedStatement ps = methodsPreparedStatement.get("updateTimeConvertCoins");
		if (ps == null) {
			String cql = "UPDATE profiles SET time_reset_coins = ? WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateTimeConvertCoins", ps);
		}
		session.execute(ps.bind(timeConvertCoins, user_id));
	}

	public static void updateTimeResetCoinsNow(Session session, long user_id) {
		PreparedStatement ps = methodsPreparedStatement.get("updateTimeResetCoinsNow");
		if (ps == null) {
			String cql = "UPDATE profiles SET time_reset_coins = now() WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateTimeConvertCoins", ps);
		}
		session.execute(ps.bind(user_id));
	}

	public static List<com.copyop.common.dto.base.Profile> getFbProfile(Session session, List<Long> userId) {
		PreparedStatement statement = methodsPreparedStatement.get("getFbProfile");
		if (statement == null) {
			String cql = "SELECT user_id, hit_rate, avatar, is_test, user_state FROM profiles WHERE user_id IN :list";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("getFbProfile", statement);
		}

		ArrayList<com.copyop.common.dto.base.Profile> result = new ArrayList<com.copyop.common.dto.base.Profile>();
		BoundStatement boundStatement = new BoundStatement(statement);
		boundStatement.setList("list", userId);
		ResultSet rs = session.execute(boundStatement);
		while(!rs.isExhausted()) {
			Row row = rs.one();
			Profile p = new Profile(row.getLong("user_id"), row.getString("avatar"), null, null, row.getFloat("hit_rate"), 0l, 0l);
			p.setTest(row.getBool("is_test"));
			p.setUserState(UserStateEnum.of(row.getInt("user_state")));
			result.add(p);
		}
		return result;
	}

	public static com.copyop.common.dto.base.Profile getFbFriendProfile(Session session, long userId) {
		PreparedStatement statement = methodsPreparedStatement.get("getFbFriendProfile");
		if (statement == null) {
			String cql = "SELECT user_id, avatar, nickname, is_test, user_state FROM profiles WHERE user_id = ?";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("getFbFriendProfile", statement);
		}
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs = session.execute(boundStatement.bind(userId));
		if (!rs.isExhausted()) {
			Row row = rs.one();
			com.copyop.common.dto.base.Profile profile = new com.copyop.common.dto.base.Profile(row.getLong("user_id"),
																								row.getString("nickname"),
																								row.getString("avatar"),
																								null,
																								false,
																								row.getBool("is_test"));
			profile.setUserState(UserStateEnum.of(row.getInt("user_state")));
			return profile;
		}
		return null;
	}

	public static void updateAvatar(Session session, long userId, String avatar, String avatarFileName) {
		PreparedStatement ps = methodsPreparedStatement.get("updateAvatar");
		if (ps == null) {
			String cql = "UPDATE profiles SET avatar = ? " +
										   ", avatar_file_name = ? " +
					    " WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateAvatar", ps);
		}
		session.execute(ps.bind(avatar, avatarFileName, userId));
	}

	public static String getAvatarFileName(Session session, long userId) {
		PreparedStatement statement = methodsPreparedStatement.get("getAvatarFileName");
		if (statement == null) {
			String cql = "SELECT  avatar_file_name FROM profiles WHERE user_id = ?";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("getAvatarFileName", statement);
		}
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs = session.execute(boundStatement.bind(userId));
		if (!rs.isExhausted()) {
			Row row = rs.one();
			return row.getString("avatar_file_name");
		}
		return null;
	}

	public static void updateAcceptedTersmNicknameAvatar(Session session, long userId, boolean acceptedTermsAndConditions, String nickname, String avatar) {
		PreparedStatement ps = methodsPreparedStatement.get("updateAcceptedTersmNicknameAvatar");
		if (ps == null) {
			String cql = "UPDATE profiles SET " +
							"nickname = ?, " +
							"avatar = ?, " +
							"is_accepted_terms_and_conditions = ? " +
						 "WHERE user_id = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateAcceptedTersmNicknameAvatar", ps);
		}
		session.execute(ps.bind(nickname, avatar, acceptedTermsAndConditions, userId));
	}

	public static Date getCoinsTimeResetDate(Session session, long userId) {
		Date restTime = null;
		PreparedStatement ps = methodsPreparedStatement.get("getCoinsTimeResetDate");
		if (ps == null) {
			String cql = "SELECT dateOf( time_reset_coins) as time_reset FROM profiles WHERE  user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getCoinsTimeResetDate", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		if (!rs.isExhausted()) {
			restTime = rs.one().getDate("time_reset");
		}

		return restTime;
	}

	public static boolean isTestProfile(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("isTestProfile");
		if (ps == null) {
			String cql = "SELECT is_test FROM profiles WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("isTestProfile", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement.bind(userId));
		if (!rs.isExhausted()) {
			Row row = rs.one();
			return row.getBool("is_test");
		}
		return false;
	}

	public static void updateProfileToTest(Session session, long userId, boolean isTest) {
		PreparedStatement ps = methodsPreparedStatement.get("updateProfileToTest");
		if (ps == null) {
			String cql =" UPDATE " +
						"	profiles " +
						" SET " +
						"	is_test = ? " +
						" WHERE " +
						"	user_id = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateProfileToTest", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(isTest, userId);
		session.execute(boundStatement);
	}

	public static void updateProfilesToTest(Session session, List<Long> usersId) {
		PreparedStatement ps = methodsPreparedStatement.get("updateProfileToTest");
		if (ps == null) {
			String cql ="UPDATE " +
								"profiles " +
						"SET " +
								"is_test = true " +
						"WHERE " +
								"user_id in :list";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateProfileToTest", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.setList("list", usersId);
		session.execute(boundStatement);
	}
	
	/**
	 * @param session
	 * @param userId
	 * @return
	 */
	//Not the best name :)
	public static HashMap<Long, UserUniqueDetails> getNicknamesAndFrozen(Session session, List<Long> userId) {
		HashMap<Long, UserUniqueDetails> result = new HashMap<Long, UserUniqueDetails>();
		PreparedStatement ps = methodsPreparedStatement.get("getNicknamesAndFrozen");
		if (ps == null) {
			String cql =
					"SELECT " +
					"	user_id" +
					"	,nickname" +
					"	,is_frozen " +
					"FROM " +
					"	profiles " +
					"WHERE " +
					"	user_id IN :list";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getNicknamesAndFrozen", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.setList("list", userId);
		ResultSet rs = session.execute(boundStatement);
		while(!rs.isExhausted()) {
			Row row = rs.one();
			UserUniqueDetails userUniqueDetails = new UserUniqueDetails();
			userUniqueDetails.setUserId(row.getLong("user_id"));
			userUniqueDetails.setNickname(row.getString("nickname"));
			userUniqueDetails.setFrozen(row.getBool("is_frozen"));
			result.put(userUniqueDetails.getUserId(), userUniqueDetails);
		}
		return result;
	}

	public static String getProfileNickname(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("getProfileNickname");
		if (ps == null) {
			String cql = "SELECT nickname FROM profiles WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfileNickname", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement.bind(userId));
		if (!rs.isExhausted()) {
			Row row = rs.one();
			return row.getString("nickname");
		}
		log.debug("can't load nickname of"+userId);
		return null;
	}

	public static boolean lockProfile(Session session, long userId) {
		PreparedStatement insertPS = methodsPreparedStatement.get("insertLockProfile");
		PreparedStatement updatePS = methodsPreparedStatement.get("updateLockProfile");
		if (insertPS == null) {
			String cql = "INSERT INTO profiles_lock (user_id, lock) VALUES (?, now()) IF NOT EXISTS";
			insertPS = session.prepare(cql);
			methodsPreparedStatement.put("insertLockProfile", insertPS);
		}
		if (updatePS == null) {
			String cql = "UPDATE profiles_lock SET lock = now() WHERE user_id = ? IF lock = null";
			updatePS = session.prepare(cql);
			methodsPreparedStatement.put("updateLockProfile", updatePS);
		}

		BoundStatement boundStatementInsert = new BoundStatement(insertPS);
		boundStatementInsert.bind(userId);
		ResultSet rsInsert = session.execute(boundStatementInsert.bind(userId));
		if(!(rsInsert.one().getBool(APPLIED))){
			BoundStatement boundStatementUpdate = new BoundStatement(updatePS);
			boundStatementUpdate.bind(userId);
			ResultSet rsUpdate = session.execute(boundStatementUpdate.bind(userId));
			return rsUpdate.one().getBool(APPLIED);
		} else {
			return true;
		}
	}

	public static void unlockProfile(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("unlockProfile");
		if (ps == null) {
			String cql = "UPDATE profiles_lock SET lock = null WHERE user_id = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("unlockProfile", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		session.execute(boundStatement);
	}

	public static boolean isLockedProfile(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("isLockedProfile");
		if (ps == null) {
			String cql = "SELECT lock from profiles_lock WHERE user_id = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("isLockedProfile", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		ResultSet rs = session.execute(boundStatement);
		return rs.one().getUUID("lock")!= null;
	}

	public static void deleteFrozenTraders(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteFrozenTraders");
		if (ps == null) {
			String cql = "TRUNCATE frozen_profiles_temp";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("deleteFrozenTraders", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		session.execute(boundStatement);
	}

	public static boolean insertNicknameIfNotExists(Session session, String nickname, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("insertNicknameIfNotExists");
		if (ps == null) {
			String cql = "INSERT INTO nicknames (nickname, user_id) VALUES (?, ?) IF NOT EXISTS";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertNicknameIfNotExists", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement.bind(nickname.toUpperCase(), userId));
		if (!rs.isExhausted()) {
			return rs.one().getBool(APPLIED);
		}
		return false;
	}

	public static void deleteOldNicknameFromNicknames(Session session, String nickName) {
		PreparedStatement ps = methodsPreparedStatement.get("changeNicknameForUserId");
		if (ps == null) {
			String cql = "DELETE FROM nicknames WHERE nickname = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("changeNicknameForUserId", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		session.execute(boundStatement.bind(nickName.toUpperCase()));
	}

	public static boolean isNicknameRegistered(Session session, String nickname) {
		PreparedStatement ps = methodsPreparedStatement.get("isNicknameRegistered");
		if (ps == null) {
			String cql = "SELECT nickname FROM nicknames WHERE nickname = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("isNicknameRegistered", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement.bind(nickname.toUpperCase()));
		if (!rs.isExhausted()) {
			return true;
		}
		return false;
	}

	public static String getFaceBookId(Session session, long userId) {
		String res = "";
		PreparedStatement ps = methodsPreparedStatement.get("getFaceBookId");
		if (ps == null) {
			String cql =
					"SELECT " +
					"	fb_id " +
					"FROM " +
					"	profiles " +
					"WHERE " +
					"	user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getFaceBookId", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		ResultSet resultSet = session.execute(boundStatement);
		if (!resultSet.isExhausted()) {
			res =  resultSet.one().getString("fb_id");
		}
		return res;
	}

	public static Date getLastTimeMostCopied(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("getLastTimeMostCopied");
		if (ps == null) {
			String cql =
					"SELECT " +
							"time_fully_copied_msg " +
					"FROM " +
						"profiles " +
					"WHERE " +
						"user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getLastTimeMostCopied", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		ResultSet resultSet = session.execute(boundStatement);

		if (!resultSet.isExhausted()) {
			return resultSet.one().getDate("time_fully_copied_msg");
		} else{
			log.debug("No time_fully_copied_msg for user "+userId);
			return null;
		}
	}

	public static void updateLastTimeMostCopied(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("updateLastTimeMostCopied");
		if (ps == null) {
			String cql =
					"UPDATE " +
							"profiles " +
					"SET " +
							"time_fully_copied_msg = dateof(now()) " +
					"WHERE " +
							"user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateLastTimeMostCopied", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		session.execute(boundStatement);
	}

	public static ArrayList<Profile> getAllProfiles(Session session, int limit) {
		PreparedStatement ps = methodsPreparedStatement.get("getAllProfiles");
		if (ps == null) {
			String cql =
					"SELECT" +
								" * " +
					"FROM " +
							"profiles " +

					"LIMIT ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getAllProfiles", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(limit);
		ArrayList<Profile> profiles = new ArrayList<Profile>();
		ResultSet rs = session.execute(boundStatement);
		while(!rs.isExhausted()) {
			profiles.add(loadProfile(rs.one()));
		}
		return profiles;
	}

	public static boolean profileExists(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("profileExists");
		if (ps == null) {
			String cql = "SELECT user_id FROM profiles WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("profileExists", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		ResultSet resultSet = session.execute(boundStatement);
		if (!resultSet.isExhausted()) {
			return true;
		}

		return false;
	}

	public static UserStateEnum getUserState(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("getUserState");
		if (ps == null) {
			String cql = "SELECT user_state FROM profiles WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getUserState", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		ResultSet resultSet = session.execute(boundStatement);
		if (!resultSet.isExhausted()) {
			return UserStateEnum.of(resultSet.one().getInt("user_state"));
		} else {
			log.debug("UserState: profile not found"+userId);
		}
		return null;
	}

	public static BoundStatement getUpdateUserStateBS(Session session, long userId, UserStateEnum userState) {
		PreparedStatement ps = methodsPreparedStatement.get("updateUserState");
		if (ps == null) {
			String cql = " UPDATE profiles SET user_state = ? WHERE user_id = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateUserState", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		return boundStatement.bind(userState.getId(), userId);
	}

	public static void UpdateUserState(Session session, long userId, UserStateEnum userState){
		session.execute(getUpdateUserStateBS(session, userId, userState));
	}

	public static void clearProfilesLocks(Session session) {
		session.execute("TRUNCATE profiles_lock");		
	}

}