package com.copyop.common.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.copyop.common.dto.ProfileRelations;
import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BatchStatement.Type;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class ProfileCountersDAO extends DAOBase {

	private static final Logger log = Logger.getLogger(ProfileCountersDAO.class);

	public static long getProfileCopiers(Session session, long userId) {
		PreparedStatement statement = getProfileCopiersPS(session);
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs =  session.execute(boundStatement.bind(userId));
		if(!rs.isExhausted()) {
			return rs.one().getLong(0);
		}
		return 0;
	}
	
	public static long getProfileCopying(Session session, long userId) {
		PreparedStatement statement = getProfileCopyingPS(session);
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs =  session.execute(boundStatement.bind(userId));
		if(!rs.isExhausted()) {
			return rs.one().getLong(0);
		}
		return 0;
	}
	
	public static long getProfileCoinsBalance(Session session, long userId) {
		PreparedStatement statement = getProfileCoinsBalancePS(session);
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs =  session.execute(boundStatement.bind(userId));
		if(!rs.isExhausted()) {
			return rs.one().getLong(0);
		}
		return 0;
	}

	public static void loadAllProfileRelations(Session session, long userId, ProfileRelations pr) {
		PreparedStatement ps = methodsPreparedStatement.get("loadAllProfileRelations");
		if (ps == null) {
			String cql = "SELECT  copiers_count, " +
								" coping_count, " +
								" watchers_count, " +
								" watching_count, " +
								" share_count, " +
								" rate_count, " +
								" copied_investments_count " +
						" FROM profiles_counters " +
						" WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("loadAllProfileRelations", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		if (!rs.isExhausted()) {
			Row row = rs.one();
			pr.setCopiers(row.getLong("copiers_count"));
			pr.setCopying(row.getLong("coping_count"));
			pr.setWatchers(row.getLong("watchers_count"));
			pr.setWatching(row.getLong("watching_count"));
			pr.setShare(row.getLong("share_count"));
			pr.setRate(row.getLong("rate_count"));
			pr.setCopiedInvestments(row.getLong("copied_investments_count"));
		}
	}
	
	public static List<Long> loadUsersWithCounters(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("loadUsersWithCounters");
		if (ps == null) {
			String cql = "SELECT user_id FROM profiles_counters limit 10000";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("loadUsersWithCounters", ps);
		}
		ArrayList<Long> list = new ArrayList<Long>();
		BoundStatement boundStatement = new BoundStatement(ps);

		ResultSet rs = session.execute(boundStatement);
		while (!rs.isExhausted()) {
			Row row = rs.one();
			list.add(row.getLong(0));
		}
		return list;
	}
	
	public static List<Long> loadCounters(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("loadCounters");
		if (ps == null) {
			String cql = "SELECT coping_count, copiers_count, watching_count, watchers_count FROM profiles_counters WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("loadCounters", ps);
		}
		ArrayList<Long> list = new ArrayList<Long>();
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		ResultSet rs = session.execute(boundStatement);
		if (!rs.isExhausted()) {
			Row row = rs.one();
			list.add(row.getLong(0));
			list.add(row.getLong(1));
			list.add(row.getLong(2));
			list.add(row.getLong(3));
		}
		return list;
	}

	
	public static void decreaseCounters(Session session, long userId, long copy, long copiers, long watch, long watchers) {
		PreparedStatement ps = methodsPreparedStatement.get("updateCounters");
		if (ps == null) {
			String cql = "UPDATE profiles_counters set " +
					"coping_count = coping_count - ?, " +
					"copiers_count = copiers_count - ?, " +
					"watching_count = watching_count - ?, " +
					"watchers_count = watchers_count - ? " +
					"WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateCounters", ps);
		}
		
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(copy, copiers, watch, watchers, userId);
		session.execute(boundStatement);
	}

	public static void increaseCounters(Session session, long userId, long copy, long copiers, long watch, long watchers) {
		PreparedStatement ps = methodsPreparedStatement.get("increaseCounters");
		if (ps == null) {
			String cql = "UPDATE profiles_counters set " +
					"coping_count = coping_count + ?, " +
					"copiers_count = copiers_count + ?, " +
					"watching_count = watching_count + ?, " +
					"watchers_count = watchers_count + ? " +
					"WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("increaseCounters", ps);
		}
		
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(copy, copiers, watch, watchers, userId);
		session.execute(boundStatement);
	}
	
	
	public static void increaseCopiedInvestmntsCounters(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("increaseCopiedInvestmentsCounters");
		if (ps == null) {
			String cql = "UPDATE profiles_counters set " +
					" copied_investments_count = copied_investments_count + 1 " +
					" WHERE user_id = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("increaseCopiedInvestmentsCounters", ps);
		}
		
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		session.execute(boundStatement);
	}

	
	public static HashMap<Long, ProfileRelations> getCopiersWathcerProfileRelations(Session session, List<Long> userIds) {
		PreparedStatement statement = methodsPreparedStatement.get("getCopiersWathcerProfileRelations");
		if (statement == null) {
			String cql = "SELECT user_id, copiers_count, watchers_count FROM profiles_counters WHERE user_id in :list";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("getCopiersWathcerProfileRelations", statement);
		}
		
		HashMap<Long, ProfileRelations> hm = new HashMap<Long, ProfileRelations>();
		BoundStatement boundStatement = new BoundStatement(statement);
		boundStatement.setList("list", userIds);
		ResultSet rs = session.execute(boundStatement);
		while(!rs.isExhausted()) {
			Row row = rs.one();		
			hm.put(row.getLong("user_id"), new ProfileRelations(row.getLong("user_id"), row.getLong("copiers_count"), row.getLong("watchers_count")));
		}
		return hm;
	}

	public static void updateProfileCoinsBalance(Session session, long userId, long amount){
		PreparedStatement statement = updateProfileCoinsBalancePS(session);
		BoundStatement boundStatement = new BoundStatement(statement);
		session.execute(boundStatement.bind(amount, userId));
	}

	public static void increaseMutualCountsWatch(Session session, long userId, long destUserId) {
		PreparedStatement statement1 = increaseProfileWatchersPS(session);
		PreparedStatement statement2 = increaseProfileWatchingPS(session);
		BatchStatement batch = new BatchStatement(Type.COUNTER);
		batch.add(statement1.bind(destUserId));
		batch.add(statement2.bind(userId));
		session.execute(batch);
	}
	
	public static void increaseMutualCountsCopy(Session session, long userId, long destUserId) {
		PreparedStatement statement1 = increaseProfileCopiersPS(session);
		PreparedStatement statement2 = increaseProfileCopyingPS(session);
		BatchStatement batch = new BatchStatement(Type.COUNTER);
		batch.add(statement1.bind(destUserId));
		batch.add(statement2.bind(userId));
		session.execute(batch);
	}
	
	public static void decreaseMutualCountsCopy(Session session, long userId, long destUserId) {
		PreparedStatement statement1 = decreaseProfileCopiersPS(session);
		PreparedStatement statement2 = decreaseProfileCopyingPS(session);
		BatchStatement batch = new BatchStatement(Type.COUNTER);
		batch.add(statement1.bind(destUserId));
		batch.add(statement2.bind(userId));
		session.execute(batch);
	}
	
	public static void decreaseMutualWatchers(Session session, long userId,	long destUserId) {
		PreparedStatement statement1 = decreaseProfileWatchersPS(session);
		PreparedStatement statement2 = decreaseProfileWatchingPS(session);
		BatchStatement batch = new BatchStatement(Type.COUNTER);
		batch.add(statement1.bind(destUserId));
		batch.add(statement2.bind(userId));
		session.execute(batch);
	}
	
	public static void increaseProfileShare(Session session, long userId){
		PreparedStatement statement = increaseProfileShareCountPS(session);
		BoundStatement boundStatement = new BoundStatement(statement);
		session.execute(boundStatement.bind(userId));
	}
	
	public static void increaseProfileRate(Session session, long userId){
		PreparedStatement statement = increaseProfileRateCountPS(session);
		BoundStatement boundStatement = new BoundStatement(statement);
		session.execute(boundStatement.bind(userId));
	}
	
	public static long getProfileProfileShare(Session session, long userId) {
		PreparedStatement statement = getProfileShareCountPS(session);
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs =  session.execute(boundStatement.bind(userId));
		if(!rs.isExhausted()) {
			return rs.one().getLong(0);
		}
		return 0;
	}
	
	public static long getProfileProfileRate(Session session, long userId) {
		PreparedStatement statement = getProfileRateCountPS(session);
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs =  session.execute(boundStatement.bind(userId));
		if(!rs.isExhausted()) {
			return rs.one().getLong(0);
		}
		return 0;
	}
	
	static PreparedStatement getProfileCopiersPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("getProfileCopiersPS");
		if (ps == null) {
			String cql = "SELECT copiers_count FROM profiles_counters WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfileCopiersPS", ps);			
		}
		return ps;
	}
	
	static PreparedStatement getProfileCopyingPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("getProfileCopyingPS");
		if (ps == null) {
			String cql = "SELECT coping_count FROM profiles_counters WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfileCopyingPS", ps);			
		}
		return ps;
	}
	
	static PreparedStatement increaseProfileCopiersPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("increaseProfileCopiersPS");
		if (ps == null) {
			String cql = "UPDATE profiles_counters SET copiers_count = copiers_count + 1 WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("increaseProfileCopiersPS", ps);
		}
		return ps;
	}
	
	static PreparedStatement increaseProfileCopyingPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("increaseProfileCopyingPS");
		if (ps == null) {
			String cql = "UPDATE profiles_counters SET coping_count = coping_count + 1 WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("increaseProfileCopyingPS", ps);
		}
		return ps;
	}
	
	static PreparedStatement increaseProfileWatchersPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("increaseProfileWatchersPS");
		if (ps == null) {
			String cql = "UPDATE profiles_counters SET watchers_count = watchers_count + 1 WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("increaseProfileWatchersPS", ps);
		}
		return ps;
	}
	
	static PreparedStatement increaseProfileWatchingPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("increaseProfileWatchingPS");
		if (ps == null) {
			String cql = "UPDATE profiles_counters SET watching_count = watching_count + 1 WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("increaseProfileWatchingPS", ps);
		}
		return ps;
	}

	
	static PreparedStatement decreaseProfileCopiersPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("decreaseProfileCopiersPS");
		if (ps == null) {
			String cql = "UPDATE profiles_counters SET copiers_count = copiers_count - 1 WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("decreaseProfileCopiersPS", ps);
		}
		return ps;
	}
	
	static PreparedStatement decreaseProfileCopyingPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("decreaseProfileCopyingPS");
		if (ps == null) {
			String cql = "UPDATE profiles_counters SET coping_count = coping_count - 1 WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("decreaseProfileCopyingPS", ps);
		}
		return ps;
	}
	
	static PreparedStatement decreaseProfileWatchersPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("decreaseProfileWatchersPS");
		if (ps == null) {
			String cql = "UPDATE profiles_counters SET watchers_count = watchers_count - 1 WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("decreaseProfileWatchersPS", ps);			
		}
		return ps;
	}
	
	static PreparedStatement decreaseProfileWatchingPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("decreaseProfileWatchingPS");
		if (ps == null) {
			String cql = "UPDATE profiles_counters SET watching_count = watching_count - 1 WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("decreaseProfileWatchingPS", ps);			
		}
		return ps;
	}
	
	static PreparedStatement getProfileCoinsBalancePS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("getProfileCoinsBalancePS");
		if (ps == null) {
			String cql = "SELECT coins_balance FROM profiles_counters WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfileCoinsBalancePS", ps);			
		}
		return ps;
	}

	static PreparedStatement updateProfileCoinsBalancePS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("increaseProfileCoinsBalancePS");
		if (ps == null) {
			String cql = "UPDATE profiles_counters SET coins_balance = coins_balance + ? WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("increaseProfileCoinsBalancePS", ps);			
		}
		return ps;
	}
	
	static PreparedStatement increaseProfileShareCountPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("increaseProfileShareCountPS");
		if (ps == null) {
			String cql = "UPDATE profiles_counters SET share_count = share_count + 1 WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("increaseProfileShareCountPS", ps);			
		}
		return ps;
	}
	
	static PreparedStatement increaseProfileRateCountPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("increaseProfileRateCountPS");
		if (ps == null) {
			String cql = "UPDATE profiles_counters SET rate_count = rate_count + 1 WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("increaseProfileRateCountPS", ps);			
		}
		return ps;
	}
	
	static PreparedStatement getProfileShareCountPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("getProfileShareCountPS");
		if (ps == null) {
			String cql = "SELECT share_count FROM profiles_counters WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfileShareCountPS", ps);			
		}
		return ps;
	}
	
	static PreparedStatement getProfileRateCountPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("getProfileRateCountPS");
		if (ps == null) {
			String cql = "SELECT rate_count FROM profiles_counters WHERE user_id = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfileRateCountPS", ps);			
		}
		return ps;
	}
}
