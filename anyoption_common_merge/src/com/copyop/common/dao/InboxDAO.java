/**
 *
 */
package com.copyop.common.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;

/**
 * @author pavelhe
 *
 */
public class InboxDAO extends DAOBase {

	private static final Logger log = Logger.getLogger(InboxDAO.class);

	public static List<FeedMessage> getInboxFeed(Session session,
													long userId,
													String period,
													String timeUUID,
													int inboxFeedMessagePageSize) {
		List<FeedMessage> result = new ArrayList<FeedMessage>();
		PreparedStatement ps = methodsPreparedStatement.get("getInboxFeedWTimeUUID");
		if (ps == null) {
			String cql = "SELECT * FROM inbox WHERE user_id = ? AND period = ? AND time_created < ? LIMIT ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getInboxFeedWTimeUUID", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId, period, UUID.fromString(timeUUID), inboxFeedMessagePageSize));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		while (!rs.isExhausted()) {
			result.add(loadMessage(rs.one()));
		}
		return result;
	}

	public static List<FeedMessage> getInboxFeed(Session session, long userId, String period, int inboxFeedMessagePageSize) {
		List<FeedMessage> result = new ArrayList<FeedMessage>();
		PreparedStatement ps = methodsPreparedStatement.get("getInboxFeed");
		if (ps == null) {
			String cql = "SELECT * FROM inbox WHERE user_id = ? AND period = ? LIMIT ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getInboxFeed", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId, period, inboxFeedMessagePageSize));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		while (!rs.isExhausted()) {
			result.add(loadMessage(rs.one()));
		}
		return result;
	}

	private static FeedMessage loadMessage(Row row) {
		FeedMessage fm = new FeedMessage();
		fm.setUserId(row.getLong("user_id"));
		fm.setQueueType(QueueTypeEnum.INBOX.getId());
		fm.setMsgType(row.getInt("msg_type"));
		fm.setTimeCreatedUUID(row.getUUID("time_created"));
		fm.setTimeCreated(new Date(UUIDs.unixTimestamp(fm.getTimeCreatedUUID())));
		// cassandra returns unmodifiable collection
		fm.setProperties(new HashMap<String, String>(row.getMap("properties", String.class, String.class)));
		return fm;
	}
}