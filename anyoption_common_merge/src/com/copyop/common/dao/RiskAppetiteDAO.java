package com.copyop.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;
import com.copyop.common.managers.ProfileManager;

public class RiskAppetiteDAO extends DAOBase  {

	private static final Logger logger = Logger.getLogger(ProfileManager.class);

	/*
	 * calculate risk appetite for the given user id
	 */
	public static float calculateUserRiskAppetite(Connection connection, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT NVL(copyop.CALCULATE_RISK_APPETITE(?), 0) as RA FROM dual";
			ps = connection.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if(rs.next()) {
				return rs.getFloat("RA");
			} else {
				logger.debug("No Risk appetite for " +userId);
				return 0;
			}
		} finally {
		    closeResultSet(rs);
		    closeStatement(ps);
		}
	}
	
	public static ArrayList<String> getUsersForCalculate(Connection con, int returnRecords) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> list = new ArrayList<String>();		
		
		try {
            String sql = "select * from(" +
            		      " SELECT user_id " +
            			  " FROM " + 
            				" copyop_risk_appetite " + 
            			  " WHERE  " +
            			    " risk_appetite is null " +
            			  " ORDER by 1) " +
            			  " WHERE ROWNUM <= " + returnRecords;
            
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {									
				list.add(rs.getString("user_id"));
			}			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
	
    public static void updateRiskAppetite(Connection conn, float riskAppetite, long userId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =  " UPDATE " +
			        "copyop_risk_appetite " +
				    "SET " +
				        " risk_appetite = ?, " +
				        " calculate_time = sysdate " +
				    " WHERE " +
				        " user_id = ? ";
            pstmt = conn.prepareStatement(sql);
            
            pstmt.setFloat(1, riskAppetite);
            pstmt.setLong(2, userId);
           
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
        
        logger.debug("Updated copyop_risk_appetite: " + riskAppetite + " for userId:" + userId);
    }
    
    public static void insertRiskAppetite(Connection conn, long userId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =" INSERT into copyop_risk_appetite " +
            				" (id, user_id) " +
            			" 	VALUES " +
            				" (seq_copyop_risk_appetite.nextval, ?)";
            pstmt = conn.prepareStatement(sql);            
            pstmt.setLong(1, userId);           
            
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
        
    }
}
