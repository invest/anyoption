/**
 *
 */
package com.copyop.common.dao;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.copyop.common.dto.CopiedInvestment;
import com.copyop.common.dto.Profile;
import com.copyop.common.dto.ProfileInvResultCopy;
import com.copyop.common.dto.ProfileInvResultSelf;
import com.copyop.common.enums.base.UserStateEnum;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.copyop.common.managers.ProfileManager;
import com.copyop.common.managers.ProfileStatisticsManager;
import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;

/**
 * @author pavelhe
 *
 */
public class BlockUserDAO extends DAOBase {
	private static final Logger log = Logger.getLogger(BlockUserDAO.class);

	public static void blockUser(Session session, long userId) {
		BatchStatement batch = new BatchStatement();
		batch.add(ProfileDAO.getUpdateUserStateBS(session, userId, UserStateEnum.STATE_BLOCKED));

		List<Long> userCopyHistory = CopyHistoryDAO.getCopyHistoryOf(session, userId, false);
		for (Long destUserId : userCopyHistory) {
			batch.add(CopyHistoryDAO.getInsertBlockedCopyHistoryBS(session, userId, destUserId));
		}

		Map<Long, List<CopiedInvestment>> copiedInvestments = InvestmentDAO.getCopiedInvestments(session,
																									userId,
																									userCopyHistory);
		for (List<CopiedInvestment> copiedInvestmentsList : copiedInvestments.values()) {
			for (CopiedInvestment copiedInvestment : copiedInvestmentsList) {
				batch.add(InvestmentDAO.getInsertBlockedCopiedInvestmentBS(session, copiedInvestment));
			}
		}

		int limit = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("INV_SUCCESS_RATE_HISTORY_SIZE", "120"));
		UserStateEnum state = ProfileManager.getUserState(userId);
		ProfileInvResultSelf invResultSelf = ProfileStatisticsDAO.getProfileInvResultSelf(session, userId, limit, state == UserStateEnum.STATE_BLOCKED);
		for (int idx = 0; idx < invResultSelf.getResults().size(); idx++) {
			batch.add(ProfileStatisticsDAO.getInsertBlockedProfileInvResultSelfBS(
																			session,
																			userId,
																			invResultSelf.getTimeUUIDCreated().get(idx),
																			invResultSelf.getMarketIds().get(idx),
																			invResultSelf.getResults().get(idx)));
		}

		limit = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("COPIED_INV_SUCCESS_RATE_HISTORY_SIZE", "200"));
		ProfileInvResultCopy invResultCopy = ProfileStatisticsManager.getProfileInvResultCopy(userId, limit);
		for (int idx = 0; idx < invResultCopy.getResults().size(); idx++) {
			batch.add(ProfileStatisticsDAO.getInsertBlockedProfileInvResultCopyBS(
																				session,
																				userId,
																				invResultCopy.getTimeCreated().get(idx),
																				invResultCopy.getResults().get(idx)));
		}

		Profile profile = ProfileDAO.getProfile(session, userId);
		batch.add(ProfileDAO.getInsertBlockedProfileBS(session, profile));

		if (log.isTraceEnabled()) {
			batch.enableTracing();
		}
		ResultSet rs = session.execute(batch);
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}

	}

	public static void unBlockUser(Session session, long userId) {
		BatchStatement batch = new BatchStatement();
		batch.add(ProfileDAO.getUpdateUserStateBS(session, userId, UserStateEnum.STATE_REGULAR));

		List<Long> blockedUserCopyHistory = CopyHistoryDAO.getCopyHistoryOf(session, userId, true);

		batch.add(CopyHistoryDAO.getDeleteBlockedCopyHistoryBS(session, userId));
		batch.add(InvestmentDAO.getDeleteBlockedCopiedInvestmentBS(session, userId, blockedUserCopyHistory));
		batch.add(ProfileStatisticsDAO.getDeleteBlockedProfileInvResultSelfBS(session, userId));
		batch.add(ProfileStatisticsDAO.getDeleteBlockedProfileInvResultCopyBS(session, userId));
		batch.add(ProfileDAO.getDeleteBlockedProfileBS(session, userId));

		if (log.isTraceEnabled()) {
			batch.enableTracing();
		}
		ResultSet rs = session.execute(batch);
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
	}

}
