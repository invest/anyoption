package com.copyop.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Writer;
import com.anyoption.common.daos.BestHotDataDAO;
import com.anyoption.common.util.CommonUtil;
import com.copyop.common.dto.Frozen;

public class FrozenCopyopDao extends com.anyoption.common.daos.DAOBase {
	
	private static final Logger logger = Logger.getLogger(FrozenCopyopDao.class);
	
	  public static void insert(Connection con, Frozen fr) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {

 				String sql="insert into copyop_frozen" +
 						" (id, user_id, is_frozen, reason, investment_id, writer_id) " +
 							" values " +
 						" (seq_copyop_frozen.nextval, ?, ?, ?, ?, ?) ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, fr.getUserId());
				ps.setLong(2,fr.isFrozen() == true ? 1 : 0);
				ps.setLong(3, fr.getReason());
				ps.setLong(4, fr.getInvestmentId());
				ps.setLong(5, fr.getWriterId());

				ps.executeUpdate();
				fr.setId(getSeqCurValue(con,"seq_copyop_frozen"));
		    } finally {
 			    closeResultSet(rs);
				closeStatement(ps);
			}
		  logger.debug("Inserted " + fr);
	  }
	  
	    public static void updateNonFrozen(Connection conn, Frozen fr) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql =  " UPDATE " +
				        "copyop_frozen " +
					    "SET " +
					        " is_frozen = ?, " +
					        " investment_id = ?," +
					        " writer_id = ?, " +
					        " unlinked = ?, " +
					        " updated_time = sysdate " +
					    " WHERE " +
					        " user_id = ? ";
	            pstmt = conn.prepareStatement(sql);
	            
	            pstmt.setLong(1,fr.isFrozen() == true ? 1 : 0);	           
	            pstmt.setLong(2, fr.getInvestmentId());
	            pstmt.setLong(3, fr.getWriterId());
	            pstmt.setLong(4, fr.isFrozen() == true ? 0 : 1);
	            pstmt.setLong(5, fr.getUserId());
	           
	            pstmt.executeUpdate();
	        } finally {
	            closeStatement(pstmt);
	        }
	        
	        logger.debug("Updated " + fr);
	    }
	    
	    public static void update(Connection con,  ArrayList<Frozen> frozenList) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql =  " UPDATE " +
				        "copyop_frozen " +
					    "SET " +
					        " is_frozen = ?, " +
					        " reason = ?, " +
					        " investment_id = ?," +
					        " writer_id = ?," +
					        " unlinked = ?, " +
					        " updated_time = sysdate " +
					    " WHERE " +
					        " user_id = ? ";
	            pstmt = con.prepareStatement(sql);
	            for (Frozen fr : frozenList ){
		            pstmt.setLong(1,fr.isFrozen() == true ? 1 : 0);
		            pstmt.setLong(2, fr.getReason());
		            pstmt.setLong(3, fr.getInvestmentId());
		            pstmt.setLong(4, fr.getWriterId());
		            pstmt.setLong(5, fr.isFrozen() == true ? 0 : 1);		            
		            pstmt.setLong(6, fr.getUserId());
	                pstmt.addBatch();
	            }

	            pstmt.executeBatch();
	        } finally {
	            closeStatement(pstmt);
	        }
	    }
	    
	    
	    
		public static ArrayList<Frozen> getBrokeFrozen(Connection con, long hours) throws SQLException {
			
			PreparedStatement ps = null;
			ResultSet rs = null;
			ArrayList<Frozen> list = new ArrayList<Frozen>();
			Date tradingDateTimeBack = BestHotDataDAO.getTradingDateBack(con, hours);			
			
			try {
	            String sql = " select u.id as user_id " +
	            			 " from " +  
	            				" users u, " +
	            				" (SELECT " + 
	            						" MIN(ilgcmin.value) min_amount , " +
	            						" ilgcmin.CURRENCY_ID " +
	            				    " FROM  " +
	            				    	" investment_limits il, " +
	            				    	" investment_limit_types ilt, " +
	            				    	" INVESTMENT_LIMIT_GROUP_CURR ilgcmin " + 
	            				    " WHERE  " +
	            				    	" il.LIMIT_TYPE_ID = ilt.id " + 
	            				    	" AND il.MIN_LIMIT_GROUP_ID = ilgcmin.INVESTMENT_LIMIT_GROUP_ID " + 
	            				    	" AND il.IS_ACTIVE = 1  " +
	            				    	" AND ilt.IS_ACTIVE = 1  " +
	            				    	" AND il.LIMIT_TYPE_ID = 1  " +
	            				    " GROUP BY " +  
	            				    	" ilgcmin.CURRENCY_ID) min_inv," +
	            				    	"COPYOP_FROZEN fr " +          
							" where u.currency_id = min_inv.CURRENCY_ID " +
							      " and u.balance < min_inv.min_amount " +
							      " and u.id = fr.user_id and fr.is_frozen != 1 " +
							      " and u.time_created <= ? " +
							      " and u.id not in  (select user_id  from  balance_history where time_created >= ? )";
	            
				ps = con.prepareStatement(sql);
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(tradingDateTimeBack));
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(tradingDateTimeBack));
				rs = ps.executeQuery();

				while (rs.next()) {
					Frozen fr = new Frozen(rs.getLong("user_id"), true, Frozen.FROZEN_REASON_BROKE, Writer.WRITER_ID_AUTO);									
					list.add(fr);
				}			
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
		}
		
		public static ArrayList<Frozen> getDormantFrozen(Connection con, int days) throws SQLException {
			GregorianCalendar gc = new GregorianCalendar();		
			gc.add(Calendar.DAY_OF_MONTH, - days);
			Date dtBack = gc.getTime();
			
			PreparedStatement ps = null;
			ResultSet rs = null;
			ArrayList<Frozen> list = new ArrayList<Frozen>();
			
			try {
	            String sql = "SELECT " +
	            							"u.id as user_id " +
	            			 "FROM " +
	            			 				"users u, " +
            			 					"copyop_frozen cf " +
	            			 "WHERE " +
				            			 	"u.id = cf.user_id " +
				            			 	"AND cf.is_frozen != 1 " +
				            			 	"AND u.time_created <= ? " +
				            			 	"AND " +
				            			 	" ( u.id not in ( " +
				            			 							"SELECT " +
				            			 										"distinct user_id " +
	            			 										"FROM " + 
																				"investments i, " +
																				"opportunities op " +
																	"WHERE " +
																				"op.id = i.opportunity_id " +
																				"AND op.scheduled = 1 " +
																				"AND op.opportunity_type_id = 1 " +
																				"AND i.copyop_type_id in (0,1,2) " +
																				"AND ( i.time_created >= ? )" + 
															") " +
	            								" OR " +
	            								"u.id in ( " +
	            													"SELECT " +
	            																"uad.user_id " +
    																"FROM " +
    																			"users_active_data uad " +
																	"WHERE " +
																				"uad.copyop_user_status = 1 " +
																				"AND sysdate - uad.status_update_date > ?" +
	            										" ) " +
	            								") ";
	            
				ps = con.prepareStatement(sql);
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(dtBack));
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(dtBack));
				ps.setInt(3, days);
				rs = ps.executeQuery();

				while (rs.next()) {
					Frozen fr = new Frozen(rs.getLong("user_id"), true, Frozen.FROZEN_REASON_DORMANT, Writer.WRITER_ID_AUTO);									
					list.add(fr);
				}			
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
		}
		
		public static ArrayList<Frozen> getFreshOffUsers(Connection con, int hours, int invCount) throws SQLException {
			GregorianCalendar gc = new GregorianCalendar();		
			gc.add(Calendar.HOUR, - hours);
			Date dtBack = gc.getTime();
			
			PreparedStatement ps = null;
			ResultSet rs = null;
			ArrayList<Frozen> list = new ArrayList<Frozen>();
			
			try {
	                            
				String sql =    " select fr.user_id, (select count(*) " +
		 											   " from investments i, " + 
										               " opportunities op " + 
										           " where " + 
										            " i.opportunity_id = op.id " + 
										            " and i.copyop_type_id in (0,2) " +
										            " and op.scheduled = 1 " +
										            " and i.user_id = u.id " +
										            " and i.time_created > u.time_created) as count_inv " +
										" from " +
										" copyop_frozen fr, " +
										" users u " +
										" where fr.user_id = u.id " +
										" and fr.reason = " + Frozen.FROZEN_REASON_FRESH +
										" and fr.is_frozen = 1 " +
										" and u.time_created <= ? ";
	            
				ps = con.prepareStatement(sql);
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(dtBack));
				rs = ps.executeQuery();

				while (rs.next()) {
					boolean isFrozen = true;
					long reason = Frozen.FROZEN_REASON_OFF;					
					
					if (rs.getInt("count_inv") >= invCount){
						isFrozen = false;
						reason = Frozen.FROZEN_REASON_FRESH;
					}
					
					Frozen fr = new Frozen(rs.getLong("user_id"), isFrozen, reason, Writer.WRITER_ID_AUTO);									
					list.add(fr);
				}			
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
		}
		
		public static boolean isFrozen(Connection connection, long userId) throws SQLException {
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try {
	            String sql = "SELECT is_frozen FROM copyop_frozen WHERE user_id = ?";
	            ps = connection.prepareStatement(sql);
	            ps.setLong(1, userId);
	            rs = ps.executeQuery();
	            if(rs.next()) {
	            	return rs.getBoolean("is_frozen");
	            } else {
	            	logger.debug("Can't load frozen status for user:"+userId);
	            	return false;
	            }
	    	} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		}
		
		public static ArrayList<Long> getFrozenUserIds(Connection connection, int hours) throws SQLException {
			ArrayList<Long> list = new ArrayList<Long>();
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try {
	            String sql = 
	            		"SELECT * FROM " +
					            		  " ( " +
						            		  	" SELECT cf.user_id " +
						            		    " FROM " +
						            		     		" copyop_frozen cf, " +
						            		    		" users u " +
						            		    " WHERE " +
						            		     	" u.id = cf.user_id " +
						            		    	" AND u.class_id     <> 0 " +
						            		    	" AND cf.unlinked     = 0 " +
						            		    	" AND cf.is_frozen    = 1 " +
						            		    	" AND cf.updated_time < SYSDATE - ? / 24 " +
						            		    " ORDER BY cf.updated_time DESC " +
					            		  " ) " +
	            		" WHERE rownum < 10000 ";
	            ps = connection.prepareStatement(sql);
	            ps.setLong(1, hours);
	            rs = ps.executeQuery();
	            while(rs.next()) {
	            	list.add(rs.getLong("user_id"));
	            } 
	            return list;
	    	} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		}
		
	    public static void updateUnlinked(Connection conn, long userId, int unlinked) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql =  
	            		" UPDATE " +
	            					"copyop_frozen " +
					    " SET " +
					        		" unlinked = ? " +
					    " WHERE " +
					        		" user_id = ? ";
	            pstmt = conn.prepareStatement(sql);
	            
	            pstmt.setLong(1, unlinked);	           
	            pstmt.setLong(2, userId);
	           
	            pstmt.executeUpdate();
	        } finally {
	            closeStatement(pstmt);
	        }
	    }
}
