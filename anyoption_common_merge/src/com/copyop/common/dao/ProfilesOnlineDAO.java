package com.copyop.common.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;

public class ProfilesOnlineDAO extends DAOBase {
	private static final Logger log = Logger.getLogger(ProfilesOnlineDAO.class);

	private static final int ONLINE_CHECK_LIMIT = 10; // should be externalized to DB

	protected static final long ONLINE_STATUS_PERIOD_MILLIS = 2L * 24L * 60L * 60L * 1000L; // 2 days  // should be externalized to DB

	public static UUID loadUserActiveSessions(Session session, Map<Long, Set<String>> sessionsPerUser) {
		return loadUserActiveSessions(session, sessionsPerUser, getCurrentLimitDate());
	}
	public static UUID loadUserActiveSessions(Session session, Map<Long, Set<String>> sessionsPerUser, UUID dateLimit) {
		PreparedStatement ps = methodsPreparedStatement.get("loadUserActiveSessions");
		if (ps == null) {
			String cql = "SELECT user_id, session_id, time_created, start_session FROM "
							+ " profiles_online_per_server WHERE time_created > ? ALLOW FILTERING;";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("loadUserActiveSessions", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(dateLimit));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}

		Set<String> closedSessions = new HashSet<String>();
		List<Long> usersToRemove = new LinkedList<Long>();
		Row row = null;
		boolean first = true;
		UUID latestDate = null;
		while (!rs.isExhausted()) {
			row = rs.one();
			if (first) {
				latestDate = row.getUUID("time_created");
				first = false;
			}
			String sessionId = row.getString("session_id");
			long userId = row.getLong("user_id");
			if (row.getBool("start_session")) {
				if (!closedSessions.remove(sessionId)) {
					Set<String> sessions = sessionsPerUser.get(userId);
					if (sessions == null) {
						sessions = new HashSet<String>();
						sessionsPerUser.put(userId, sessions);
					}
					sessions.add(sessionId);
				}
			} else {
				Set<String> sessions = sessionsPerUser.get(userId);
				if (sessions != null) {
					sessions.remove(sessionId);
					if (sessions.size() == 0) {
						sessionsPerUser.remove(userId);
						usersToRemove.add(userId);
					}
				}
				closedSessions.add(sessionId);
			}
		}

		if (!usersToRemove.isEmpty()) {
			for (long userId : usersToRemove) {
				FeedDAO.deleteUserPostAlgorithmData(session, userId);
			}
		}

		return latestDate == null ? UUIDs.timeBased() : latestDate;
	}

	/**
	 * Checks whether a user has open Lightstreamer sessions.
	 * This should indicate whether the user is online (logged in).
	 *
	 * @param session
	 * @param userId
	 * @return <code>true</code> if the user has open Lighstreamer sessions, otherwise <code>false</code>
	 */
	public static boolean isUserOnline(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("isUserOnline");
		if (ps == null) {
			String cql = "SELECT session_id, start_session FROM profiles_online_per_user WHERE user_id = ? LIMIT ?;";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("isUserOnline", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId, ONLINE_CHECK_LIMIT));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}

		Set<String> closedSessions = new HashSet<String>();
		Row row = null;
		while (!rs.isExhausted()) {
			row = rs.one();
			String sessionId = row.getString("session_id");
			if (row.getBool("start_session")) {
				if (!closedSessions.remove(sessionId)) {
					log.debug("User [" + userId + "] session [" + sessionId + "] is open");
					return true;
				}
			} else {
				closedSessions.add(sessionId);
			}
		}

		return false;
	}
	
	
	public static Date getLastLogoutDate(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("getLastLogoutDate");
		if (ps == null) {
			String cql = "SELECT dateOf(time_created) as time_created , start_session  FROM profiles_online_per_user WHERE user_id = ? LIMIT ?;";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getLastLogoutDate", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId, ONLINE_CHECK_LIMIT));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		
		Row row = null;
		Date lastOffLineDate = null;
		while (!rs.isExhausted()) {
			row = rs.one();
			if (!row.getBool("start_session")) {
				lastOffLineDate = row.getDate("time_created");
					log.debug("User [" + userId + "] getLastLogoutDate [" + lastOffLineDate + "] ");
					return lastOffLineDate;
				}
			} 		
		return lastOffLineDate;
	}

	protected static PreparedStatement getServerSessionLogPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("getServerSessionLog");
		if (ps == null) {
			String cql = "SELECT user_id, session_id, start_session "
							+ " FROM profiles_online_per_server WHERE server_id = ? AND time_created > ?;";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getServerSessionLog", ps);
		}
		return ps;
	}

	protected static UUID getCurrentLimitDate() {
		return UUIDs.endOf(System.currentTimeMillis() - ONLINE_STATUS_PERIOD_MILLIS);
	}

}
