package com.copyop.common.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class CopyHistoryDAO extends DAOBase{


	public static void insertCopyHistory(Session session, long userId,long destUserId){
		PreparedStatement ps = methodsPreparedStatement.get("insertCopyHistory");
		if(ps == null) {
			String query = "INSERT INTO copy_history(user_id, dest_user_id) VALUES(?, ?)";
        	ps = session.prepare(query);
			methodsPreparedStatement.put("insertCopyHistory", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		session.execute(boundStatement.bind(userId, destUserId));
	}

	public static List<Long> getCopyHistoryOf(Session session, long userId, boolean blockedUserHistory) {
		String statementName = blockedUserHistory ? "getCopyHistoryOfBlocked" : "getCopyHistoryOf";
		PreparedStatement ps = methodsPreparedStatement.get(statementName);
		if(ps == null) {
			String query = "SELECT dest_user_id FROM "
							+ (blockedUserHistory ? "blocked_" : "") + "copy_history WHERE user_id = ?";
        	ps = session.prepare(query);
			methodsPreparedStatement.put(statementName, ps);
		}
		ArrayList<Long> result = new ArrayList<Long>();
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement.bind(userId));
		while(!rs.isExhausted()) {
			Row row = rs.one();
			result.add(row.getLong(0));
		}
		return result;
	}

	public static BoundStatement getInsertBlockedCopyHistoryBS(Session session, long userId, long destUserId) {
		PreparedStatement ps = methodsPreparedStatement.get("insertBlockedCopyHistory");
		if(ps == null) {
			String query = " INSERT INTO blocked_copy_history (user_id, dest_user_id, time_created) VALUES (?, ?, now()) ";
        	ps = session.prepare(query);
			methodsPreparedStatement.put("insertBlockedCopyHistory", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		return boundStatement.bind(userId, destUserId);
	}

	public static BoundStatement getDeleteBlockedCopyHistoryBS(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteBlockedCopyHistory");
		if(ps == null) {
			String query = " DELETE FROM blocked_copy_history WHERE user_id = ? ";
        	ps = session.prepare(query);
			methodsPreparedStatement.put("deleteBlockedCopyHistory", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		return boundStatement.bind(userId);
	}
}
