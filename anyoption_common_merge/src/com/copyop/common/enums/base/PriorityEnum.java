/**
 *
 */
package com.copyop.common.enums.base;

import java.util.HashMap;
import java.util.Map;

/**
 * @author pavelhe
 *
 */
public enum PriorityEnum {
	CATEGORY_1(1), CATEGORY_2(2), CATEGORY_3(3), ALL_OTHERS(4);

	private static final Map<Integer, PriorityEnum> ID_TO_ENUM_VALUE_MAP = new HashMap<Integer, PriorityEnum>(PriorityEnum.values().length);
	static {
		for (PriorityEnum enumVal : PriorityEnum.values()) {
			ID_TO_ENUM_VALUE_MAP.put(enumVal.getId(), enumVal);
		}
	}

	private final int id;
	/** KAHA DB Priorities*/
	private int priority;
	private long messageTimeLimit;
	private int messageCountLimit;
	private long messageWaitPeriod;

	PriorityEnum(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the KAHA DB priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * @param priority the KAHA DB priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	/**
	 * @return the messageTimeLimit in milliseconds
	 */
	public long getMessageTimeLimit() {
		return messageTimeLimit;
	}

	/**
	 * @param messageTimeLimit the messageTimeLimit in milliseconds to set
	 */
	public void setMessageTimeLimit(long messageTimeLimit) {
		this.messageTimeLimit = messageTimeLimit;
	}

	/**
	 * @return the messageCountLimit
	 */
	public int getMessageCountLimit() {
		return messageCountLimit;
	}

	/**
	 * @param messageCountLimit the messageCountLimit to set
	 */
	public void setMessageCountLimit(int messageCountLimit) {
		this.messageCountLimit = messageCountLimit;
	}

	/**
	 * @return the messageWaitPeriod in milliseconds
	 */
	public long getMessageWaitPeriod() {
		return messageWaitPeriod;
	}

	/**
	 * @param messageWaitPeriod the messageWaitPeriod in milliseconds to set
	 */
	public void setMessageWaitPeriod(long messageWaitPeriod) {
		this.messageWaitPeriod = messageWaitPeriod;
	}

	public static PriorityEnum getById(int id) {
		PriorityEnum enumVal = ID_TO_ENUM_VALUE_MAP.get(id);
		if (enumVal == null) {
			throw new IllegalArgumentException("No enum value with ID: " + id);
		} else {
			return enumVal;
		}
	}
}
