package com.copyop.common.enums.base;


public enum ProfileLinkCommandEnum {
	COPY(1), WATCH(2), UNCOPY(3), UNWATCH(4), FREEZE(5), SHOWOFF(6), COINSRESET(7), LOWBALANCE_COPYING(8), LOWBALANCE_COPIERS(9), FB_NEW_FRIEND(10), NEW_NICKNAME(11), PING(12);
	
	private int code;
	 
	private ProfileLinkCommandEnum(int c) {
		code = c;
	}

	public int getCode() {
		return code;
	}
	
	public static ProfileLinkCommandEnum of(int linkType) {

	    switch (linkType) {
	        case 1: 
        		return COPY;
	        case 2:
	        	return WATCH;
	        case 3:
	        	return UNCOPY;
	        case 4:
	        	return UNWATCH;
	        case 5:
	        	return FREEZE;
	        case 6:
	        	return SHOWOFF;
	        case 7:
	        	return COINSRESET;
	        case 8:
	        	return LOWBALANCE_COPYING;
	        case 9:
	        	return LOWBALANCE_COPIERS;
	        case 10:
	        	return FB_NEW_FRIEND;
	        case 11:
	        	return NEW_NICKNAME;
	        default: 
	        	return null;

	    }
	}
	
}
