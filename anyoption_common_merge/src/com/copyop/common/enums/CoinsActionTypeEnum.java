/**
 *
 */
package com.copyop.common.enums;

import java.util.HashMap;
import java.util.Map;

public enum CoinsActionTypeEnum {
	COPIED(1, "Copied", false), FOLLOWED(2, "Followed", false), RESET_COINS(3, "Missed", true), CONVERT(4, "Converted", true), RE_GRANT(5, "Re-Granted", true), SHARE(6, "Share", false), RATE(7, "Rate", false);

	private static final Map<Integer, CoinsActionTypeEnum> ID_TO_COINS_ACTION_TYPE = new HashMap<Integer, CoinsActionTypeEnum>(CoinsActionTypeEnum.values().length);
	static {
		for (CoinsActionTypeEnum queueType : CoinsActionTypeEnum.values()) {
			ID_TO_COINS_ACTION_TYPE.put(queueType.getId(), queueType);
		}
	}

	private final int id;
	private final String displayName;
	private final boolean backendAction;

	CoinsActionTypeEnum(int id, String displayName, boolean backendAction) {
		this.id = id;
		this.displayName = displayName;
		this.backendAction = backendAction;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	
	/**
	 * @return the backendAction
	 */
	public boolean isBackendAction() {
		return backendAction;
	}

	public static CoinsActionTypeEnum getById(int id) {
		CoinsActionTypeEnum actionType = ID_TO_COINS_ACTION_TYPE.get(id);
		if (actionType == null) {
			throw new IllegalArgumentException("No coins action type with ID: " + id);
		} else {
			return actionType;
		}
	}

}
