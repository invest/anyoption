package com.copyop.common.enums;

public enum ProfileLinkChangeReasonEnum {
	STOP_COPY(10, "UNCOPY")
	,STOP_WATCH(20, "UNWATCH")
	,BALANCE_LOW(3, "AUTO_STOP_LOW_BALANCE")
	,BACKEND_STOP_COPY(4, "STOPPED_BY_BE_USER")
	,START_COPY(11, "START_COPY")
	,START_WATCH(22, "START_WATCH");
	
	private int code;
	private String displayName;
	 
	private ProfileLinkChangeReasonEnum(int c, String displayName) {
		code = c;
		this.displayName = displayName;
	}

	public static ProfileLinkChangeReasonEnum of(int reason) {

	    switch (reason) {
	        case 10: 
        		return STOP_COPY;
	        case 20:
	        	return STOP_WATCH;
	        case 3:
	        	return BALANCE_LOW;
	        case 4:
	        	return BACKEND_STOP_COPY;
	        case 11:
	        	return START_COPY;
	        case 22:
	        	return START_WATCH;
	        default: 
	        	return null;
	    }
	}
	
	public int getCode() {
		return code;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
} 
