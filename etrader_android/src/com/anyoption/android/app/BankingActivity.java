package com.anyoption.android.app;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.anyoption.android.app.util.ChartSlideListener;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;


public class BankingActivity extends BaseMenuActivity {
	
    private ViewFlipper viewFlipper;
    private PageControl viewFlipperControl;
    private BankingSlideListener chartSlideListener;
	 
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        User user = ap.getUser();

		if (null == user) {
			setContentView(R.layout.banking_logout);
	        viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);
	        viewFlipperControl = (PageControl) findViewById(R.id.pageControl);
	        viewFlipperControl.setPageCount(viewFlipper.getChildCount());
	        chartSlideListener = new BankingSlideListener(this, null, viewFlipper, viewFlipperControl);
	        viewFlipper.setOnTouchListener(chartSlideListener);
	        viewFlipperControl.setListener(chartSlideListener);
	        manageOpenMenuStrip(findViewById(R.id.FAQViewBanking));
	        CreatePaymentsSlide();	        
		} else {
			setContentView(R.layout.banking_login);
			manageOpenMenuStrip(findViewById(R.id.depositView));
		}
		LinearLayout menu = (LinearLayout) findViewById(R.id.linearLayoutMenu);
        for (int i = 0 ; i < menu.getChildCount() ; i++) {
        	((MenuViewBase)menu.getChildAt(i)).setOnMenuStripClickListener(new OnMenuStripClickListener() {
				public void onClickCloseAll(MenuViewBase menuViewBase) {
					manageOpenMenuStrip(menuViewBase);
				}
			});
        }
		((TextView)findViewById(R.id.TextViewHeader)).setText(getString(R.string.menuStrip_banking).toUpperCase());
    }
      
    public void openAccountHandler(View target){
        Class<Activity> actvityRedirect = Utils.getActivityClass(Constants.ACTIVITY_REDIRECT_REGISTER, this);
        Intent intent = new Intent(this, actvityRedirect);
        startActivityForResult(intent, Constants.REQUEST_CODE_REGISTER);
    }
    
    private void CreatePaymentsSlide() {
    	AnyoptionApplication ap = (AnyoptionApplication) this.getApplication();
	    LayoutInflater inflater = getLayoutInflater();
	    ViewFlipper vf = (ViewFlipper) findViewById(R.id.viewFlipper);
	    LinearLayout paymentMethod;
	    ImageView allowWithdraw ;
	    ImageView logoImg;
	    TextView logoText; 
	    //Common payment method
	    //Credit card
	    paymentMethod = (LinearLayout) inflater.inflate(R.layout.payment_mothod_slide, vf, false);
	    logoImg = (ImageView) paymentMethod.findViewById(R.id.imageViewLogo);
	    if(Utils.isRegulated(this)){
	    	logoImg.setImageResource(R.drawable.logo_creditcards_big_regulated);
	    } else {
	    	logoImg.setImageResource(R.drawable.logo_creditcards_big);
	    }
	    allowWithdraw = (ImageView) paymentMethod.findViewById(R.id.imageViewAllowWithdraw);
	    allowWithdraw.setImageResource(R.drawable.icon_yes);
	    PaymentMethod paymentCC = new PaymentMethod();
	    paymentCC.setDisplayName("payment.type.cc");
	    paymentCC.setBigLogo("logo_creditcards_big");
	    setInfoClick(paymentCC, paymentMethod);
	    vf.addView(paymentMethod);    
	    //Bank wire	 
	    paymentMethod = null;
	    paymentMethod = (LinearLayout) inflater.inflate(R.layout.payment_mothod_slide, vf, false);
	    logoImg = (ImageView) paymentMethod.findViewById(R.id.imageViewLogo);
	    logoImg.setVisibility(View.GONE);
	    logoText = (TextView) paymentMethod.findViewById(R.id.textViewLogo);
	    logoText.setVisibility(View.VISIBLE); 
	    logoText.setText(Utils.getStringFromResource(this, "deposit.bank.wire", null).toUpperCase());
	    allowWithdraw = (ImageView) paymentMethod.findViewById(R.id.imageViewAllowWithdraw);
	    allowWithdraw.setImageResource(R.drawable.icon_yes);
	    PaymentMethod paymentBW = new PaymentMethod();
	    paymentBW.setId(PaymentMethod.PAYMENT_TYPE_WIRE);	    
	    setInfoClick(paymentBW, paymentMethod);
	    vf.addView(paymentMethod);		    
	    //Domestic payments
	    if (ap.getSkinId() != Skin.SKIN_GERMAN) {
		    paymentMethod = null;
		    paymentMethod = (LinearLayout) inflater.inflate(R.layout.payment_mothod_slide, vf, false);
		    logoImg = (ImageView) paymentMethod.findViewById(R.id.imageViewLogo);
		    logoImg.setVisibility(View.GONE);
		    logoText = (TextView) paymentMethod.findViewById(R.id.textViewLogo);
		    logoText.setVisibility(View.VISIBLE); 		       
		    logoText.setText(Utils.getStringFromResource(this, "header.envoy", null).toUpperCase());
		    PaymentMethod paymentDP = new PaymentMethod();
		    paymentDP.setId(PaymentMethod.PAYMENT_TYPE_ENVOY);	
		    paymentDP.setDisplayName("payment.type.domestic.payment");
		    setInfoClick(paymentDP, paymentMethod);
		    vf.addView(paymentMethod);	 	    
	    }
	    //Moneybookers	 
	    if (!Utils.isEtraderProject(this)) {
	    	paymentMethod = null;
		    paymentMethod = (LinearLayout) inflater.inflate(R.layout.payment_mothod_slide, vf, false);
		    logoImg = (ImageView) paymentMethod.findViewById(R.id.imageViewLogo);
		    allowWithdraw = (ImageView) paymentMethod.findViewById(R.id.imageViewAllowWithdraw);
		    logoImg.setImageResource(R.drawable.logo_moneybookers_big);
		    PaymentMethod paymentMB = new PaymentMethod();
		    paymentMB.setDisplayName("payment.type.moneybookers");
		    paymentMB.setBigLogo("logo_moneybookers_big");
		    setInfoClick(paymentMB, paymentMethod);
		    vf.addView(paymentMethod);
	    }
	    //CUP
	    if (!Utils.isEtraderProject(this) && false) {
	    	paymentMethod = null;
		    paymentMethod = (LinearLayout) inflater.inflate(R.layout.payment_mothod_slide, vf, false);
		    logoImg = (ImageView) paymentMethod.findViewById(R.id.imageViewLogo);
		    allowWithdraw = (ImageView) paymentMethod.findViewById(R.id.imageViewAllowWithdraw);
		    logoImg.setImageResource(R.drawable.logo_cup_big);
		    PaymentMethod paymentMB = new PaymentMethod();
		    paymentMB.setDisplayName("payment.type.cc");
		    paymentMB.setBigLogo("logo_cup_big");
		    setInfoClick(paymentMB, paymentMethod);
		    vf.addView(paymentMethod);
	    }
	    //dynamic payment method	    	   
	    long countryId = 0;
	    countryId = ap.getCountryId();
	    if (countryId == 0) {
	    	countryId = ap.getSkins().get(ap.getSkinId()).getDefaultCountryId();
	    }
		ArrayList<PaymentMethod> pm = ap.getCountries().get(countryId).getPayments();
		Log.d(getLocalClassName(), "geting payment method for countryId " + countryId);
		if (pm.size() > 0){			        	
			for (final PaymentMethod payment : pm){
				if (payment.getTypeId() == PaymentMethod.TYPE_ID_DEPOSIT && (ap.getSkinId() == 3 && payment.getTypeId() != PaymentMethod.PAYMENT_TYPE_UKASH)) { //add only deposit
					paymentMethod = null;
					paymentMethod = (LinearLayout) inflater.inflate(R.layout.payment_mothod_slide, vf, false);
					logoImg = (ImageView) paymentMethod.findViewById(R.id.imageViewLogo);
				    allowWithdraw = (ImageView) paymentMethod.findViewById(R.id.imageViewAllowWithdraw);			   
				    int image = Utils.getImageFromResource(this, payment.getBigLogo());
				    setInfoClick(payment, paymentMethod);
				    if (image > 0) {
				    	logoImg.setImageResource(image);
				    } else {
					    logoImg = (ImageView) paymentMethod.findViewById(R.id.imageViewLogo);
					    logoImg.setVisibility(View.GONE);
					    logoText = (TextView) paymentMethod.findViewById(R.id.textViewLogo);
					    logoText.setVisibility(View.VISIBLE); 		       
					    logoText.setText(Utils.getStringFromResource(this, payment.getDisplayName(), null).toUpperCase());
				    }
				    vf.addView(paymentMethod);
				} 
			}
		}
		viewFlipperControl.setPageCount(viewFlipper.getChildCount());
    }

    public void setInfoClick(PaymentMethod payment, LinearLayout paymentMethodLayout) {
	    final ImageView plus = (ImageView) paymentMethodLayout.findViewById(R.id.imageViewInfo);
	    plus.setTag(payment);
	    plus.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent;
				intent = new Intent(BankingActivity.this, PaymentMethodPopupActivity.class);
		    	intent.putExtra(Constants.PUT_EXTRA_BANKING_PAYMENT, (PaymentMethod) plus.getTag());
		    	startActivity(intent);
			}
		});
    }
    
     class BankingSlideListener extends ChartSlideListener {
        public BankingSlideListener(Activity activity, HomePageChartView chart, ViewFlipper viewFlipper, PageControl pageControl) {
            super(activity, chart, viewFlipper, pageControl);
        }
        
        public void getChartData() {
            // do nothing
        }
        
        public void openAssetPage(long marketId) {
            // do nothing
        }               
    }    
     
     @Override
     public int getInfoPageId() {     	
         return R.layout.security;
     }
     
     @Override
     public void onResume() {
    	 super.onResume();
    	 if (null != lastOpenGroup) {
    		 manageOpenMenuStrip(lastOpenGroup);
    	 }
         AnyoptionApplication ap = (AnyoptionApplication) getApplication();
         User user = ap.getUser();
         if(null != user) {
         	 if (user.isAcceptedTerms() == false) {
        		 Intent intent = new Intent(this, TermsActivity.class).putExtra(Constants.ACTIVITY_BEFORE_TERMS, "my_account");
        		 startActivity(intent);
        	 }
         }
     }
}