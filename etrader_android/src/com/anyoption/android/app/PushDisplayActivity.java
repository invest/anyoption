package com.anyoption.android.app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class PushDisplayActivity extends BaseActivity  {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// turn off the window's title bar
		setContentView(R.layout.push_display);
		Log.d(getLocalClassName(), "got push notficition");
		Bundle extras = getIntent().getExtras();
		if(extras != null) {
			TextView title = (TextView)findViewById(R.id.title); 
			title.setText(extras.getString("title"));
			
			TextView message = (TextView)findViewById(R.id.info); 
		    message.setText(extras.getString("message"));
		}
					
	    Button b = (Button) findViewById(R.id.button_ok);
		b.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				setResult(RESULT_OK);
				//need to check if program not runing
				/*if(FirstPopUpLangActivity.isApplicationRunning) {
					finish();
				} else {*/
					Intent intent = new Intent(PushDisplayActivity.this, FirstPopUpLangActivity.class);
					startActivity(intent);
					finish();
				//}				
			}
		});
	}
}
