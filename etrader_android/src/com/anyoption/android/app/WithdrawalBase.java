package com.anyoption.android.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.results.TransactionMethodResult;

/**
 * @author AviadH
 *
 */
public class WithdrawalBase extends BaseActivity {
	
	protected Handler responseH;
	protected LayoutInflater inflater;
	protected Activity activity;
	protected String withAmount;
	protected int successId;
	protected boolean clickedWithdrawal;
	protected String fee;
	
	/**
	 * Show important note popup (only after all validations were checked)
	 * @param serviceName
	 * @param successId
	 * @param request
	 */ 
	public void showImportantNotePopUp(final String serviceName, final int successId, final MethodRequest request, String fee) {
		this.successId = successId;
		this.fee = fee;
		inflater = getLayoutInflater();
		LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.reverse_withdrawal_pop_up, null);
		final Button cancelButton = (Button) ll.findViewById(R.id.buttonCancel);
		TextView content = (TextView) ll.findViewById(R.id.textViewReverseAmount);
		TextView headLine = (TextView) ll.findViewById(R.id.textViewReverseHeader);
		((TextView) ll.findViewById(R.id.textViewReverseReadMore)).setVisibility(View.GONE);
		 
		//insert text values
		headLine.setText(getString(R.string.banking_withdrawal_note_header));
		content.setText(getString(R.string.bank_wire_approve_message, fee));		
		Button confirm = (Button) ll.findViewById(R.id.buttonSubmit);
		confirm.setText(getResources().getString(R.string.button_continue));
		final AlertDialog ad = Utils.showCustomAlert(this, ll, null, null); // adding different dismiss event for each click
		// in case we press on cancel
		cancelButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				makePopUpForSuccessOrCancel(null, Constants.EMPTY_STRING);
				ad.dismiss();
			}
		});
		confirm.setOnClickListener(new OnClickListener() {		
			public void onClick(View v) {
				Utils.requestService(activity, responseH, "withdrawalCallBack", request, new TransactionMethodResult(), serviceName);
				ad.dismiss();// in this case of continue we only close the alert and the confirmation pop up is in the callback
			}
		});
		
		ad.setOnCancelListener(new OnCancelListener() {
			public void onCancel(DialogInterface dialog) {
				// Enable withdrawal button after the dialog is canceled with back.
				clickedWithdrawal = false;
			}
		});
	}
	
	public void withdrawalCallBack(Object result) {
		TransactionMethodResult tmr = (TransactionMethodResult) result;
		if (tmr.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {			
			Log.d(getPackageName(), "Insert withdrawal: " + tmr.getTransactionId());
			makePopUpForSuccessOrCancel(tmr.getFormattedAmount(), fee);
		} else { //in case of not insert cc withdrawal
			Utils.showAlert(this, tmr.getUserMessages()[0].getField(),
					tmr.getUserMessages()[0].getMessage(), this.getString(R.string.alert_title));
		}
	}
	
	public void makePopUpForSuccessOrCancel(String formattedAmount, String fee) {
		//if the formatted amount null is for cancel click
		LinearLayout l = (LinearLayout) inflater.inflate(R.layout.rotate_phone_popup, null);
		TextView contentText = (TextView) l.findViewById(R.id.textViewContent);
	    Button closeButton  = (Button) l.findViewById(R.id.buttonRotatePhonePopup);
	    if (null != formattedAmount) {//for success withdrawal
	    	contentText.setText(getString(successId, formattedAmount, fee));
	    } else { //for cancel
	    	contentText.setText(getString(R.string.withdraw_cancel));
	    }
	    final AlertDialog ad = Utils.showCustomAlert(this, l, closeButton);
	    ad.setCancelable(false);
	    closeButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				startActivityForResult(new Intent(activity, MarketsGroupActivity.class), Constants.REQUEST_CODE_MARKETS_GROUP_PAGE);
				ad.dismiss();
				clickedWithdrawal = false;
				finish();
			}
		});
	}

}
