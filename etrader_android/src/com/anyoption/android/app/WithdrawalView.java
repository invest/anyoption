package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.UserMethodRequest;
import com.anyoption.json.results.BaroPayMethodResult;
import com.anyoption.json.results.CreditCardsMethodResult;
import com.anyoption.json.results.CupWithdrawalMethodResult;

public class WithdrawalView extends BankingViewBase {
	
	private static final String WITHDRAW_CC = "cc_withdraw";
	private static final String WITHDRAW_WIRE = "bank_wire_withdrawal";
	private static final String WITHDRAW_DELTAPAY = "cup_withdraw";
	private static final String WITHDRAW_BAROPAY = "baropay_withdraw";
	private static final long PAYMENT_TYPE_DELTAPAY_CHINA = 63;
	private static final long PAYMENT_TYPE_BAROPAY = 64;
	
	private CreditCard[] ccListWithdraw;
	private TableRow trCredit;
	private TableRow trWire;
	private TableRow trCUP;
	private TableRow trBaropay;
	
	private boolean isShowBaroPayWithdraw;
	
	private int baropayVisibility = GONE;
	private int cupVisibility = GONE;
	private int creditVisibility = GONE;
	private int wireVisibility = GONE;
	
	public WithdrawalView(Context context) {
		this(context, null);
	}
	
	public WithdrawalView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initComponent();
	}
	
	public void initComponent() {
		tlMain.setColumnShrinkable(0, true);
		addWithdrawalRows();
	}
	
	public void addWithdrawalRows() {
		final PaymentMethod payment = new PaymentMethod();
		//make info row
		TableRow trInfo = (TableRow) inflater.inflate(R.layout.withdrawal_row, tlMain, false);
		tlMain.addView(trInfo);
		//domestic payments rows
		makePaymentMethodsRows(Constants.PAYMENT_TYPE_WITHDRAW);
		//credit card row
		trCredit = (TableRow) inflater.inflate(R.layout.banking_deposit_withdrawal_row, tlMain, false);
	    TextView textContentCredit = (TextView) trCredit.findViewById(R.id.textViewBankingRowText);
		ImageView imageContentCredit = (ImageView) trCredit.findViewById(R.id.textViewBankingRowImage);
		textContentCredit.setText(getResources().getString(R.string.header_creditcards_text));
		
		int headerCreditCards = R.drawable.header_creditcards;
		if(Utils.isRegulated(context)){
			headerCreditCards = R.drawable.header_creditcards_regulated;
		}
		imageContentCredit.setImageDrawable(getResources().getDrawable(headerCreditCards));
		trCredit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(context, CcWithdrawalActivity.class);    	
		    	intent.putExtra(Constants.PUT_EXTRA_CC_LIST, ccListWithdraw);
		    	payment.setId(PaymentMethod.PAYMENT_TYPE_CC);
		    	payment.setDisplayName("payment.type.cc");
			    payment.setBigLogo("logo_creditcards_big");
			    payment.setTypeId(Constants.PAYMENT_TYPE_WITHDRAW);
			    
				int headerCreditcards = R.drawable.header_creditcards;
				if(Utils.isRegulated(context)){
					headerCreditcards = R.drawable.header_creditcards_regulated;
				}
				bankingRowClick(WITHDRAW_CC, intent, headerCreditcards, R.string.withdrawal_cc_desc, payment);
			}
		});
		//bank wire rows
		trWire = (TableRow) inflater.inflate(R.layout.banking_deposit_withdrawal_row, tlMain, false);
	    TextView textContentWire = (TextView) trWire.findViewById(R.id.textViewBankingRowText);
		textContentWire.setText(getResources().getString(R.string.header_bank_wire_text));
		trWire.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {				
				payment.setId(PaymentMethod.PAYMENT_TYPE_WIRE);
				payment.setTypeId(Constants.PAYMENT_TYPE_WITHDRAW);
			    payment.setBigLogo("logo_creditcards_big");
				bankingRowClick(WITHDRAW_WIRE, null, 0, R.string.header_bank_wire_text, payment);
			}
		});
		//deltaPayRow
		trCUP = (TableRow) inflater.inflate(R.layout.banking_deposit_withdrawal_row, tlMain, false);
		TextView textContentDeltapay = (TextView) trCUP.findViewById(R.id.textViewBankingRowText);
		textContentDeltapay.setText(getResources().getString(R.string.header_cup_withdraw_text));
		ImageView imageCUP = (ImageView) trCUP.findViewById(R.id.textViewBankingRowImage);
		imageCUP.setImageDrawable(getResources().getDrawable(R.drawable.logo_cup_big));
		trCUP.setOnClickListener(new OnClickListener() {			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				payment.setId(PAYMENT_TYPE_DELTAPAY_CHINA);
				payment.setTypeId(Constants.PAYMENT_TYPE_WITHDRAW);
				payment.setBigLogo("logo_cup_big");
				bankingRowClick(WITHDRAW_DELTAPAY, null, R.drawable.logo_cup_small, R.string.transactions_deltapay_withdraw, payment);
			}
		});
		//baropay
		trBaropay = (TableRow) inflater.inflate(R.layout.banking_deposit_withdrawal_row, tlMain, false);
		TextView textContentBaropay = (TextView) trBaropay.findViewById(R.id.textViewBankingRowText);
		textContentBaropay.setText(getResources().getString(R.string.header_baropay_withdraw_text));
		ImageView imageBaropay = (ImageView) trBaropay.findViewById(R.id.textViewBankingRowImage);
		imageBaropay.setImageDrawable(getResources().getDrawable(R.drawable.logo_baro_big));
		trBaropay.setOnClickListener(new OnClickListener() {			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				payment.setId(PAYMENT_TYPE_BAROPAY);
				payment.setTypeId(Constants.PAYMENT_TYPE_WITHDRAW);
				payment.setDisplayName("payment.type.baropay");
				payment.setBigLogo("logo_baro_big");
				bankingRowClick(WITHDRAW_BAROPAY, null, R.drawable.logo_baro_small, R.string.withdrawal_baropay_desc, payment);
			}
		});
		tlMain.addView(trBaropay);
		tlMain.addView(trCUP);
		tlMain.addView(trCredit);
		tlMain.addView(trWire);

		//hide all withdraw options 
		setVisibilities();
		
	}
	// TODO improve requests calls and withdraw methods setting visibility algorithm
	
	public void withdrawCcCallBack(Object result) { 
		
		Log.d("", "insert into withdrawCcCallBack");
        CreditCardsMethodResult omr = (CreditCardsMethodResult) result;
		AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context).getApplication();
		User user = ap.getUser();
        ccListWithdraw = omr.getOptions();    
        Log.d("", "ccListWithdraw is " + ccListWithdraw + " isShowBaroPayWithdraw is " + isShowBaroPayWithdraw);        
        if (null != ccListWithdraw && ccListWithdraw.length > 0) {
        	creditVisibility = VISIBLE;
        	wireVisibility = GONE;
        } else {
	        //show wire option 
        	creditVisibility = GONE;
        	wireVisibility = VISIBLE;
        }        

        //show BaroPay option only when there is no CC list for withdraw
        if(null == ccListWithdraw && isShowBaroPayWithdraw && Utils.isShowBaroPay(user)){
        	baropayVisibility = VISIBLE;
        } else {
        	baropayVisibility = GONE;
        }
  	 
        if(user.getCurrencyId() == Constants.CURRENCY_YUAN){
        	// cup may be available - check the amount
            checkCup();		
        } else {
        	setVisibilities();
        }
    }

	public void getMaxCupWithdrawalCallback(Object resultObj){
	 Log.d(context.getPackageName(), "checkMaxAmountCallback");
		CupWithdrawalMethodResult result = (CupWithdrawalMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			long maxWithdrawAmount = result.getMaxCupWithdrawal();
			if(maxWithdrawAmount==0){
				// cup not available - disable it
				cupVisibility = GONE;
			} else {
				// cup available - disable wire
				cupVisibility = VISIBLE;
				wireVisibility = GONE;
			}
			setVisibilities();
		} else {
			String error = result.getUserMessages()[0].getMessage();
			 String field = result.getUserMessages()[0].getField();
			 if (result.getErrorCode() == Constants.ERROR_CODE_VALIDATION_WITHOUT_FIELD) {
				 field = Constants.EMPTY_STRING;
			 }			
			 Utils.showAlert(context, error, field, context.getString(R.string.alert_title));
		}
	}
	
	public String getMenuStripText() {
		return getResources().getString(R.string.header_withdraw);
	}
	
	public void openGroup() {
		super.openGroup();		
		AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context).getApplication();
		User user = ap.getUser();
		if (Utils.isShowBaroPay(user)) {
			checkBaroPay();
		} else {
			getWithdrawCCList();
		}		    		
	}
	
	public void getWithdrawCCList() {
		UserMethodRequest request = new UserMethodRequest();
		Utils.requestService(view, responseH, "withdrawCcCallBack", request, new CreditCardsMethodResult(), "getWithdrawCCList", (Activity) context);	
	}
	
	public void checkBaroPay() {
		UserMethodRequest request = new UserMethodRequest();
		Utils.requestService(view, responseH, "withdrawBaroPayAvailableCallBack", request, new BaroPayMethodResult(), "checkBaroPayWithdrawAvailable", (Activity) context);
	}
	
	private void checkCup() {
		Utils.requestService(view, responseH, "getMaxCupWithdrawalCallback", new UserMethodRequest(), new CupWithdrawalMethodResult(), "getMaxCupWithdrawal", (Activity) context);
	}
	
	
	public void withdrawBaroPayAvailableCallBack(Object result) {		
		BaroPayMethodResult bpmr = (BaroPayMethodResult) result;
		isShowBaroPayWithdraw = bpmr.isAllowBaroPayWithdraw();
		getWithdrawCCList();
	}


	protected void setVisibilities() {
		trBaropay.setVisibility(baropayVisibility);
		trCUP.setVisibility(cupVisibility);
		trCredit.setVisibility(creditVisibility);
		trWire.setVisibility(wireVisibility);
	}

}
