/**
 * 
 */
package com.anyoption.android.app;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.TextView;

public class PersonalDetailsInnerView extends PersonalDetailsView{

	public PersonalDetailsInnerView(Context context, AttributeSet attrs) {
		super(context, attrs);

		buttonMenuStrip.setBackgroundResource(R.drawable.bg_option_row_press);
		TextView groupName = (TextView) findViewById(R.id.textViewGroupName);
		groupName.setTextColor(Color.WHITE);
		groupName.setText(context.getString(R.string.personal_details));
	}

	public PersonalDetailsInnerView(Context mContext) {
		this(mContext, null);
	}
	
	@Override
	protected void manageStrip() {
		// do nothing
	}
}