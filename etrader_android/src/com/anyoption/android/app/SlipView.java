package com.anyoption.android.app;

import java.math.BigDecimal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Investment;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.json.requests.InsertInvestmentMethodRequest;
import com.anyoption.json.results.InvestmentMethodResult;

public class SlipView extends RelativeLayout implements View.OnClickListener {
    /** Called when the activity is first created. */
	private int slipType; 
	private float winOdds;
	private float loseOdds;
	private double pageLevel;
	private long oppId;
	private String currencySymbol;
	
	private boolean currencyLeftSymbol;
    private LinearLayout mainSlip;
    private Context contextd;
    private ChartDataListener chart;
    private long commission;

	private Handler responseH;
    private final String TAG = "SlipView";
    
    private long currencyId;
    
    public SlipView(Context context) {
        this(context, null);
        this.contextd = context;
    }

    public SlipView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.contextd = context;
        responseH = new Handler();
        //TODO get the symbol from the user or skin
        currencySymbol = context.getString(R.string.currencySymbolDefault);
        currencyLeftSymbol = true;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        mainSlip = (LinearLayout) inflater.inflate(R.layout.slip, this, false);
        EditText etAmount = (EditText) mainSlip.findViewById(R.id.editTextAmount);
        
        mainSlip.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// Do not do nothing				
			}
		});
        AnyoptionApplication ap = (AnyoptionApplication) contextd.getApplicationContext();
        if (ap.getSkinId() == Skin.SKIN_KOREAN) {
            InputFilter[] fArray = new InputFilter[1];
            fArray[0] = new InputFilter.LengthFilter(Constants.NUM_DIGITS_AMOUNT_KR);
            etAmount.setFilters(fArray);      	
        }
        
        etAmount.setInputType(InputType.TYPE_NULL);
        etAmount.addTextChangedListener(new TextWatcher() {
			private int lastLength = 0;
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				//System.out.println("onTextChanged s= " + s + " start " + start + " before " + before + " count " + count);
				lastLength = start;
			}
			
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
				//System.out.println("beforeTextChanged s= " + s + " start " + start + " after " + after + " count " + count);
			}
			
			public void afterTextChanged(Editable s) {
				//validation for the amount
				//check that there is only 0-9 or .(if and only if we can have decimal point digits) in the string OR if we start with dot ('.')
				
				int decPntDgts;
				int defaultDecPntDgts = 2;
				if(((AnyoptionApplication)((Activity)contextd).getApplication()).getUser() != null){
					decPntDgts  = Integer.valueOf(((AnyoptionApplication)((Activity)contextd).getApplication()).getUser().getCurrency().getDecimalPointDigits());
				}else {
					
					decPntDgts = defaultDecPntDgts;
				}
				
				String pattern = (decPntDgts > 0) ? "^[0-9.]+$" : "^[0-9]+$";
				if (!s.toString().matches(pattern) || s.toString().startsWith(".")) {
					s.delete(lastLength, s.length());
				} else {
					//check that we dont have more then 1 dot ('.')
					if (s.toString().endsWith(".") && (s.toString().substring(0, s.length() - 1).indexOf(".") > -1)) { 
						s.delete(lastLength, s.length());
					} else { //check there is no more than the allowed decimal numbers
						if (s.toString().contains(".") && !s.toString().endsWith(".")) {
							String[] arr = s.toString().split("\\.");
							if (arr.length > 0 && arr[1].length() > decPntDgts) {
								s.delete(lastLength, s.length());
							}
						}
					}
				}
				
				//calculate win lose
				String amount = s.toString();
				if (amount.length() == 0 || amount.equals("")) {
					amount = "0";
				}
				TextView upAmount = (TextView)findViewById(R.id.textViewExpireLineUpAmount);
				TextView downAmount = (TextView)findViewById(R.id.textViewExpireLineDownAmount);
				
				Utils.updateWinLose(new BigDecimal(amount), upAmount, downAmount, winOdds, loseOdds, currencyLeftSymbol, currencySymbol, decPntDgts);
			}
		});
        
        Button b = (Button) mainSlip.findViewById(R.id.buttonDot);
        b.setOnClickListener(this);
        b = (Button) mainSlip.findViewById(R.id.buttonDel);
        b.setOnClickListener(this);
        for (int i = 0; i < 10; i++) {
        	int valueId = context.getResources().getIdentifier("button" + i, "id", context.getPackageName());
        	b = (Button) mainSlip.findViewById(valueId);
        	b.setOnClickListener(this);
		}
        b = (Button) mainSlip.findViewById(R.id.buttonBuy);
        b.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				String amount = ((EditText)mainSlip.findViewById(R.id.editTextAmount)).getText().toString();
				Log.d(TAG, "going to buy inv with amount " + amount);
				AnyoptionApplication ap = (AnyoptionApplication) contextd.getApplicationContext();
				if (null == ap.getUser()) {
					Utils.showNotLoginAlert(contextd.getString(R.string.error_investment_notLogin1), contextd);
					return;
				} else {
		        	if (ap.getUser().isAcceptedTerms() == false) {
		        		Intent intent = new Intent(contextd, TermsActivity.class).putExtra(Constants.ACTIVITY_BEFORE_TERMS, "markets_group");
		        		Activity a = (Activity) getContext();
		        		a.startActivity(intent);
		        		return;
		        	} 
				}
				String res = Utils.isFieldEmpty(contextd, amount, contextd.getString(R.string.golden_header_amount));
				if (null != res) {
					Utils.showAlert(contextd, "", res, contextd.getString(R.string.alert_title));
					return;
				}
				Display display = ((WindowManager) contextd.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		        int orientation = display.getOrientation();
		        boolean isFromGraph = orientation == Surface.ROTATION_90 || orientation == Surface.ROTATION_270;
		        
				InvestmentMethodResult result = new InvestmentMethodResult();
				InsertInvestmentMethodRequest request = new InsertInvestmentMethodRequest();
				request.setPageLevel(pageLevel);
				request.setPageOddsLose(loseOdds);
				request.setPageOddsWin(winOdds);
				request.setChoice(slipType);
				request.setOpportunityId(oppId);
				request.setFromGraph(isFromGraph);
				double requestAmount = Double.parseDouble(amount);
				request.setRequestAmount(requestAmount);
				Utils.requestService(SlipView.this, responseH, "insertInvestmentCallBack", request, result, "insertInvestment", (Activity)contextd);
				
			}
		});
        addView(mainSlip);
    }

    public void insertInvestmentCallBack(Object resultObj) {
    	Log.d(TAG, "insertInvestmentCallBack");
    	InvestmentMethodResult result = (InvestmentMethodResult) resultObj;
    	int errorCode = result.getErrorCode();
    	if(errorCode == Constants.ERROR_CODE_SUCCESS){
    		//show the alert box with details of investment
			showInvestmentDetails(result);
    	} else {
    		Utils.handleResultError(contextd, result);
    	}
  
    }
	
	protected void showInvestmentDetails(InvestmentMethodResult result) {
		Investment inv = result.getInvestment();
		inv.setOddsWin(inv.getOddsWin() - 1);
		inv.setOddsLose(1 - inv.getOddsLose());
		chart.addInvestment(inv);
		Activity a = (Activity) getContext();
		Display display = ((WindowManager) a.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		int orientation = display.getOrientation();
		if (orientation == Surface.ROTATION_0 || orientation == Surface.ROTATION_180) {
			((MyOptionView)a.findViewById(R.id.myOptionView)).checkIfCloseAndOpen();
		}
		((AnyoptionApplication) a.getApplication()).setUser(result.getUser());
		((HeaderLayout) a.findViewById(R.id.headerLayout)).refresh();
		((EditText) mainSlip.findViewById(R.id.editTextAmount)).setText("");
		setVisibility(GONE);
		chart.closeSlip();
	}
    
    public void showKeyboard(View v) {
    	if (v.getTag().toString().equals("1")) {
    		slipType = 1;
    		((TextView)findViewById(R.id.textViewExpireLineDown)).setText(R.string.slip_expire_below);
    		((TextView)findViewById(R.id.textViewExpireLineUp)).setText(R.string.slip_expire_above);
    	} else {
    		slipType = 2;
    		((TextView)findViewById(R.id.textViewExpireLineDown)).setText(R.string.slip_expire_above);
    		((TextView)findViewById(R.id.textViewExpireLineUp)).setText(R.string.slip_expire_below);
    	}
    	((RelativeLayout)findViewById(R.id.relativeLayoutKeyboard)).setVisibility(View.VISIBLE);
    }
    
    public void typeKey(View v) {
    	EditText amount =  (EditText)findViewById(R.id.editTextAmount);
    	String buttonText = ((Button)v).getText().toString();
    	if (buttonText.length() == 0 || buttonText.equals("")) {
    		if (amount.getText().length() > 0) {
    			amount.getText().delete(amount.getText().length() - 1, amount.getText().length());
    		}
    	} else {
    		amount.append(buttonText);
    	}
    	
    }

    public void onClick(View v) {      
    	EditText amount =  (EditText)findViewById(R.id.editTextAmount);
    	String buttonText = ((Button)v).getText().toString();
    	int cursorPositionStart = amount.getSelectionStart();
    	int cursorPositionEnd = amount.getSelectionEnd();
    	if (buttonText.length() == 0 || buttonText.equals("")) {
    		if (amount.getText().length() > 0) {
    			int startPos = cursorPositionStart;
    			if (cursorPositionStart == cursorPositionEnd) {
    				startPos = cursorPositionStart - (cursorPositionStart > 0 ? 1 : 0);
    			}
    			amount.getText().delete(startPos, cursorPositionEnd);
    		}    		
    	} else {
    		amount.append(buttonText);
    	}
    }

    
	public double getWinOdds() {
		return winOdds;
	}

	public void setWinOdds(float winOdds) {
		this.winOdds = winOdds;
	}

	public double getLoseOdds() {
		return loseOdds;
	}

	public void setLoseOdds(float loseOdds) {
		this.loseOdds = loseOdds;
	}

	public int getSlipType() {
		return slipType;
	}

	public void setSlipType(int slipType) {
		this.slipType = slipType;
	}

	public double getPageLevel() {
		return pageLevel;
	}

	public void setPageLevel(double pageLevel) {
		this.pageLevel = pageLevel;
	}

	public long getOppId() {
		return oppId;
	}

	public void setOppId(long oppId) {
		this.oppId = oppId;
	}

    public ChartDataListener getChart() {
        return chart;
    }

    public void setChart(ChartDataListener chart) {
        this.chart = chart;
    }
    
   

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public boolean isCurrencyLeftSymbol() {
        return currencyLeftSymbol;
    }

    public void setCurrencyLeftSymbol(boolean currencyLeftSymbol) {
        this.currencyLeftSymbol = currencyLeftSymbol;
    }
    public long getCommission() {
		return commission;
	}

	public void setCommission(long commission) {
		this.commission = commission;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
}