package com.anyoption.android.app;


import java.util.Calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.User;

/**
 * @author IdanZ
 *
 */
public class BalancesView extends MenuViewBase {
	
	private AnyoptionApplication ap;

	public BalancesView(Context context) {
		this(context, null);
	}

	public BalancesView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initComponent(attrs);
	}

	public void initComponent(AttributeSet attrs) {
		Log.d("BonusView", "init Component");
		ap = (AnyoptionApplication) context.getApplicationContext();
		
		//set table params
		tlMain.setColumnStretchable(0, true);
		tlMain.setColumnShrinkable(1, true);
		
		TableRow myAccountBalances = (TableRow) inflater.inflate(R.layout.my_account_balances, this, false);
		User user = ap.getUser();
        TextView balance = (TextView) myAccountBalances.findViewById(R.id.textViewMyAccountBalance);
        TextView taxBalance = (TextView) myAccountBalances.findViewById(R.id.textViewMyAccountTaxBalance);
        TextView taxBalanceNote = (TextView) myAccountBalances.findViewById(R.id.textViewTaxBalanceNote);
        balance.setText(ap.getString(R.string.my_account_balance, user.getBalanceWF()));
        taxBalance.setText(ap.getString(R.string.my_account_tax_balance, user.getTaxBalanceWF()));
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.MONTH, 0);
        String formatFrom = Utils.getDateFormat(c.getTime(), null);
        String formattedTaxBalanceNote = getResources().getString(R.string.my_account_tax_balance_note, formatFrom, null); 
        taxBalanceNote.setText(formattedTaxBalanceNote);
		tlMain.addView(myAccountBalances);
	}

	public String getMenuStripText() {
		return getResources().getString(R.string.my_account_balances_header);
	}
	
	@Override
	public void removeAllRows() {
		// TODO Auto-generated method stub
		
	}
}