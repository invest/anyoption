package com.anyoption.android.app;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Wire;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.FeeMethodRequest;
import com.anyoption.json.requests.WireMethodRequest;
import com.anyoption.json.results.TransactionMethodResult;

/**
 * @author AviadH
 *
 */
public class BaropayWithdrawActivity extends WithdrawalBase {
	
	private final static String SERIVCE_NAME = "insertWithdrawBaropay";
	private User user;
	private WireMethodRequest request;
	private FeeMethodRequest FeeRequest;
	private Wire wire;

	@Override
    public void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);        
		 wire = new Wire();
	     setContentView(buildLayout(Constants.PAYMENT_TYPE_WITHDRAW));	  
	     PaymentMethod payment = (PaymentMethod) getIntent().getExtras().get(Constants.PUT_EXTRA_BANKING_PAYMENT);
	     findViewById(R.id.imageViewInfo).setTag(payment);
	     activity = this;
	     //set the currency symbol
	     AnyoptionApplication ap = (AnyoptionApplication) getApplication();
	     user = ap.getUser();
	     ((TextView) findViewById(R.id.textViewCurrencySymbol)).setText(user.getCurrencySymbol());	     
	}
	
	public void baropayWithdrawHandler(View target) {
		//Client validations
		//Credit card was selected		
    	String error = null;
    	//amount
    	EditText amount = (EditText) findViewById(R.id.amountToWithdraw);
    	error = Utils.validateDoubleAndEmpty(this, amount.getText().toString(), getString(R.string.withdrawal_amount));
    	if (null != error ){
    		Utils.showAlert(this, "", error, this.getString(R.string.alert_title));
    		return;
    	}
    	EditTextCustom bankName = (EditTextCustom)findViewById(R.id.editTextBankName);
    	//bank name
		error = Utils.isFieldEmpty(this, bankName.getText().toString(), getString(R.string.bank_wire_bank_name));
		if (error != null) {
			bankName.requestFocus();
			Utils.showAlert(this, "", error,  this.getString(R.string.alert_title));
    		return ;
		}
    	EditTextCustom bankOwner = (EditTextCustom)findViewById(R.id.editTextBeneficiary);
    	EditTextCustom accountNumber = (EditTextCustom)findViewById(R.id.editTextAccountNumber);
    	//account number
		error = Utils.validateDigitsOnlyAndEmpty(this, accountNumber.getText().toString(), getString(R.string.bank_wire_account_number));
		if (error != null) {
			accountNumber.requestFocus();
			Utils.showAlert(this, "", error,  this.getString(R.string.alert_title));
    		return ;
		}
    	wire.setAmount(amount.getText().toString());
    	wire.setBankNameTxt(bankName.getText().toString());
    	wire.setBeneficiaryName(bankOwner.getText().toString());
    	wire.setAccountNum(accountNumber.getText().toString());
    	request = new WireMethodRequest();
    	request.setWire(wire);
    	FeeRequest = new FeeMethodRequest();
    	FeeRequest.setTranTypeId(Constants.TRANS_TYPE_BARO_PAY);
		Utils.requestService(activity, responseH, "insertBaropayWithdrawCallBack", request, new TransactionMethodResult(), "validateBaroPayWithdraw");
   }
	
	/**
	 * In case of success navigate to Important note Popup
	 * @param result
	 */
	public void insertBaropayWithdrawCallBack(Object result) {
		Log.d(getLocalClassName(), "insertBaropayWithdrawCallBack");
		TransactionMethodResult tmr = (TransactionMethodResult) result;		
		if (tmr.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(getLocalClassName(), "showImportantNotePopup");
			showImportantNotePopUp(SERIVCE_NAME, R.string.withdraw_cc_success, request, tmr.getFee());
		} else {
			 String error = tmr.getUserMessages()[0].getMessage();
			 String field = tmr.getUserMessages()[0].getField();
			 if (tmr.getErrorCode() == Constants.ERROR_CODE_VALIDATION_WITHOUT_FIELD) {
				 field = Constants.EMPTY_STRING;
			 }			
			 Utils.showAlert(this, error, field, this.getString(R.string.alert_title));
		}
	}
		
}
