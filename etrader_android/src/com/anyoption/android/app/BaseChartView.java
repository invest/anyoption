package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.SetTextRunnable;
import com.anyoption.android.app.util.SetVisibilityRunnable;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Investment;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.json.results.ChartDataMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

public class BaseChartView extends LinearLayout implements ChartDataListener {
    protected static final int COLOR_CRR_LVL_UP = 0xFF15A01F;
    protected static final int COLOR_CRR_LVL_DOWN = 0xFFDF0000;
    protected static final int COLOR_CRR_LVL_VOID = 0xFF000000;
    
    protected static final int CLEAR_ALPHA_MASK = 0x00FFFFFF;

    protected Handler handler;
    protected TextView name;
    protected TextView returnPercent;
    protected TextView expTime;
    protected ChartView chart;
    protected ImageView currentReturnIcon;
    protected TextView totalInvestments;
    protected double totalInvestmentsAmount;
    protected TextView currentReturn;
    protected double currentReturnAmount;
    protected ImageView btnCall;
    protected TextView level;
    protected ImageView btnPut;
    protected SlipView slip;
    protected long opportunityId;
    
    protected int callId;
    protected int callOnId;
    protected int putId;
    protected int putOnId;
    protected int callTransparentId;
    protected int putTransparentId;
    
    protected Context context;
    
    protected SkinGroup skinGroup;

    public BaseChartView(Context context) {
        this(context, null);
        this.context = context;
    }

    public BaseChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }
    
    public BaseChartView(Context context, AttributeSet attrs, int layoutId, int cId, int cOnId, int pId, int pOnId, int cTransId, int pTransId) {
        super(context, attrs);
        this.context = context;
        callId = cId;
        callOnId = cOnId;
        putId = pId;
        putOnId = pOnId;
        callTransparentId = cTransId;
        putTransparentId = pTransId;
        
        handler = new Handler();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        inflater.inflate(layoutId, this, true);

        name = (TextView) findViewById(R.id.baseChartName);
        returnPercent = (TextView) findViewById(R.id.baseChartReturn);
        expTime = (TextView) findViewById(R.id.baseChartExpTime);
        chart = (ChartView) findViewById(R.id.baseChartChart);
        currentReturnIcon = (ImageView) findViewById(R.id.baseChartCurrentReturnIcon);
        totalInvestments = (TextView) findViewById(R.id.baseChartTotalInvestment);
        currentReturn = (TextView) findViewById(R.id.baseChartCurrentReturn);
        btnCall = (ImageView) findViewById(R.id.chartSlipCall);
        btnPut = (ImageView) findViewById(R.id.chartSlipPut);
        btnCall.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (chart.getOppState() == Opportunity.STATE_OPENED || chart.getOppState() == Opportunity.STATE_LAST_10_MIN) {
                    closeSlip();
                    chart.setCall(true);
                    btnCall.setImageResource(callOnId);
                    btnPut.setImageResource(putTransparentId);
    
                    slip.setSlipType(Investment.INVESTMENT_TYPE_CALL);
                    ((TextView) slip.findViewById(R.id.textViewExpireLineDown)).setText(R.string.slip_expire_below);
                    ((TextView) slip.findViewById(R.id.textViewExpireLineUp)).setText(R.string.slip_expire_above);
                    slip.setVisibility(VISIBLE);
                }
            }
        });
        level = (TextView) findViewById(R.id.chartSlipLevel);        
        btnPut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (chart.getOppState() == Opportunity.STATE_OPENED || chart.getOppState() == Opportunity.STATE_LAST_10_MIN) {
                    closeSlip();
                    chart.setPut(true);
                    btnPut.setImageResource(putOnId);
                    btnCall.setImageResource(callTransparentId);
    
                    slip.setSlipType(Investment.INVESTMENT_TYPE_PUT);
                    ((TextView) slip.findViewById(R.id.textViewExpireLineDown)).setText(R.string.slip_expire_above);
                    ((TextView) slip.findViewById(R.id.textViewExpireLineUp)).setText(R.string.slip_expire_below);
                    slip.setVisibility(VISIBLE);
                }
            }
        });
        
        AnyoptionApplication application = (AnyoptionApplication) context.getApplicationContext();
        skinGroup = application.getSkins().get(application.getSkinId()).getSkinGroup();
    }

    public void setChartData(ChartDataMethodResult data) {
        name.setText(data.getMarketName());
        returnPercent.setText("");
        expTime.setText("");
        btnCall.getDrawable().setAlpha(0xFF);
        level.setVisibility(VISIBLE);
        btnPut.getDrawable().setAlpha(0xFF);
        opportunityId = data.getOpportunityId();
        chart.setChartOffsetRight(btnCall.getWidth() + 20);
        chart.setChartData(data);
        if (null != data.getInvestments() && data.getInvestments().length > 0) {
            showCurrentReturn();
        } else {
            hideCurrentReturn();
        }

        if (null != data.getUser()) {
            Activity a = (Activity) getContext();
            ((AnyoptionApplication) a.getApplication()).setUser(data.getUser());
            ((HeaderLayout) a.findViewById(R.id.headerLayout)).refresh(); 
        }
        
//        btnCall.setVisibility(VISIBLE);
//        ((AnimationDrawable) btnCall.getBackground()).start();
//        btnPut.setVisibility(VISIBLE);
//        ((AnimationDrawable) btnPut.getBackground()).start();
    }
    
    public void updateItem(int itemPos, String itemName, UpdateInfo update) {
        if (update.isValueChanged(skinGroup.getColorUpdateKey())) {
            String color = update.getNewValue(skinGroup.getColorUpdateKey());
            Log.v("chart", "color: " + color);
            int c = COLOR_CRR_LVL_VOID;
            if (color.equals("0")) {
                c = COLOR_CRR_LVL_DOWN;
            } else if (color.equals("2")) {
                c = COLOR_CRR_LVL_UP;
            }
            c = c & (chart.getChartAlpha() << 24 | 0x00FFFFFF);
            handler.post(new SetTextColorRunnable(level, c));
        }
        if (update.isValueChanged(skinGroup.getLevelUpdateKey())) {
            String lvl = update.getNewValue(skinGroup.getLevelUpdateKey());
            if (lvl.equals("0")) {
                lvl = getContext().getResources().getString(R.string.chart_wait);
            }
            handler.post(new SetTextRunnable(level, lvl));
        }
        chart.updateItem(itemPos, itemName, update);
        if (update.isValueChanged("ET_STATE")) {
            if (chart.getOppState() != Opportunity.STATE_OPENED && chart.getOppState() != Opportunity.STATE_LAST_10_MIN) {
                handler.post(new SetVisibilityRunnable(slip, GONE));
                handler.post(new CloseSlipRunnable());
            }
            handler.post(new SetChartAlphaRunnable());
        }
        updateTotalFields();

        /* update slip details level/oppid */
        if (null != slip) {
            slip.setPageLevel(Double.parseDouble(update.getNewValue(skinGroup.getLevelUpdateKey()).replaceAll(",", "")));
            if (update.isValueChanged("ET_OPP_ID")) {
                slip.setOppId(Long.parseLong(update.getNewValue("ET_OPP_ID")));
            }
            if (update.isValueChanged("ET_ODDS_LOSE") || slip.getLoseOdds() == 0) {
                slip.setLoseOdds(Float.parseFloat(update.getNewValue("ET_ODDS_LOSE")));
            }
            if (update.isValueChanged("ET_ODDS_WIN") || slip.getWinOdds() == 0) {
                slip.setWinOdds(Float.parseFloat(update.getNewValue("ET_ODDS_WIN")));
            }
        }
    }
    
    protected void updateTotalFields() {
        if (currentReturnAmount != chart.getTotalProfit() && null != currentReturn) {
            currentReturnAmount = chart.getTotalProfit();
            handler.postDelayed(new SetTextRunnable(currentReturn, Utils.formatCurrencyAmount(currentReturnAmount, slip.getCurrencySymbol(), slip.isCurrencyLeftSymbol(), slip.getCurrencyId())), 0);
        }
        if (totalInvestmentsAmount != chart.getTotalAmount() && null != totalInvestments) {
            totalInvestmentsAmount = chart.getTotalAmount();
            handler.postDelayed(new SetTextRunnable(totalInvestments, Utils.formatCurrencyAmount(totalInvestmentsAmount, slip.getCurrencySymbol(), slip.isCurrencyLeftSymbol(), slip.getCurrencyId())), 0);
        }
    }
    
    public void addInvestment(Investment inv) {
        chart.addInvestment(inv);
        updateTotalFields();
        if (chart.getInvestmentsCount() == 1) {
            showCurrentReturn();
        }
    }
    
    public void removeInvestment(long invId) {
        chart.removeInvestment(invId);
        updateTotalFields();
    }

    public void closeSlip() {
        chart.closeSlip();
        btnCall.setImageResource(callId);
        btnPut.setImageResource(putId);
    }

    public void setChartSlip(SlipView slip) {
        this.slip = slip;
        chart.setChartSlip(slip);
    }
    
    protected void showCurrentReturn() {
        View v = findViewById(R.id.assetPageChartVSep);
        if (null != v) {
            v.setVisibility(VISIBLE);
        }
        if (null != currentReturnIcon) {
            currentReturnIcon.setVisibility(VISIBLE);
        }
        findViewById(R.id.baseChartTotalInvestmentLbl).setVisibility(VISIBLE);
        totalInvestments.setVisibility(VISIBLE);
        if (chart.getOppTypeId() == Opportunity.TYPE_REGULAR) {
            findViewById(R.id.baseChartCurrentReturnLbl).setVisibility(VISIBLE);
            currentReturn.setVisibility(VISIBLE);
        }
    }
    
    protected void hideCurrentReturn() {
        View v = findViewById(R.id.assetPageChartVSep);
        if (null != v) {
            v.setVisibility(GONE);
        }
        if (null != currentReturnIcon) {
            currentReturnIcon.setVisibility(GONE);
        }
        findViewById(R.id.baseChartTotalInvestmentLbl).setVisibility(GONE);
        totalInvestments.setVisibility(GONE);
        findViewById(R.id.baseChartCurrentReturnLbl).setVisibility(GONE);
        currentReturn.setVisibility(GONE);
    }
    
    protected void setAlpha() {
        int alpha = chart.getChartAlpha();
        Log.v("chart", "BaseChartView.setAlpha: " + alpha);
        btnCall.getDrawable().setAlpha(alpha == ChartView.ALPHA_OPENED ? alpha : 0);
        btnPut.getDrawable().setAlpha(alpha == ChartView.ALPHA_OPENED ? alpha : 0);
        if (null != currentReturnIcon) {
            currentReturnIcon.getDrawable().setAlpha(alpha);
        }
        int alphaMask = alpha << 24;
        level.setTextColor(level.getCurrentTextColor() & CLEAR_ALPHA_MASK | alphaMask);
        TextView v = (TextView) findViewById(R.id.baseChartTotalInvestmentLbl);
        v.setTextColor(v.getCurrentTextColor() & CLEAR_ALPHA_MASK | alphaMask);
        totalInvestments.setTextColor(totalInvestments.getCurrentTextColor() & CLEAR_ALPHA_MASK | alphaMask);
        v = (TextView) findViewById(R.id.baseChartCurrentReturnLbl);
        v.setTextColor(v.getCurrentTextColor() & CLEAR_ALPHA_MASK | alphaMask);
        currentReturn.setTextColor(currentReturn.getCurrentTextColor() & CLEAR_ALPHA_MASK | alphaMask);
    }
    
    protected String composeSuspendedMessage(String msg) {
        try {
            String arr[] = msg.split("s");
            Log.v("chart", "Suspend msg - msg: " + msg + " arr[0]: " + arr[0] + " arr[1]: " + arr[1] + " len: " + arr.length);
            int l1 = Integer.parseInt(arr[0]);
            int l2 = Integer.parseInt(arr[1]);
            String sl1 = null;
            switch (l1) {
            case 1:
                sl1 = getResources().getString(R.string.suspend_upperline1);
                break;
            case 2:
                sl1 = getResources().getString(R.string.chart_suspend_upperline2);
                break;
            case 3:
                sl1 = getResources().getString(R.string.chart_suspend_upperline3);
                break;
            case 4:
                sl1 = getResources().getString(R.string.chart_suspend_upperline4);
                break;
            }
            String sl2 = null;
            switch (l2) {
            case 1:
                sl2 = getResources().getString(R.string.suspend_lowerline1);
                break;
            case 2:
                sl2 = getResources().getString(R.string.chart_suspend_lowerline2);
                break;
            case 3:
                sl2 = getResources().getString(R.string.chart_suspend_lowerline3);
                break;
            case 4:
                sl2 = getResources().getString(R.string.chart_suspend_lowerline4);
                break;
            }
            String result = sl1 + "\n" + sl2;
            if (arr.length > 2 && !arr[2].equals("")) {
                result = result + "\n" + Utils.reformatTimeLong(getContext(), arr[2]);
            }
            return result;
        } catch (Exception e) {
            Log.e("chart", "Suspend msg compose error.", e);
        }
        return "";
    }
    
    class SetTextColorRunnable implements Runnable {
        private TextView tv;
        private int color;
        
        public SetTextColorRunnable(TextView tv, int color) {
            this.tv = tv;
            this.color = color;
        }
        
        public void run() {
            tv.setTextColor(color);
        }
    }
    
    class SetChartAlphaRunnable implements Runnable {
        public void run() {
            setAlpha();
        }
    }
    
    class CloseSlipRunnable implements Runnable {
        public void run() {
            closeSlip();
        }
    }
}