package com.anyoption.android.app;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Register;
import com.anyoption.common.beans.base.Country;
import com.anyoption.json.requests.InsertUserMethodRequest;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.results.CountryIdMethodResult;
import com.anyoption.json.results.UserMethodResult;

/**
 * @author EranL
 *
 */
public abstract class ForgotPasswordCommonActivity extends BaseActivity {
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.forgot_password);
        Utils.requestService(this, responseH, "getCountryIdCallBack", new MethodRequest(), new CountryIdMethodResult(), "getCountryId");  
        init();
    }

    public void getCountryIdCallBack(Object result) {
        Log.d(getPackageName(), "getCountryIdCallBack");
        CountryIdMethodResult omr = (CountryIdMethodResult) result;
        long countryId = omr.getCountryId();
        Log.d(getPackageName(), "countryId: " + countryId);
        Country userCountry = ((AnyoptionApplication) getApplication()).getCountries().get(countryId);
        if (null != userCountry) {
            TextView mobilePhonePrefix = (TextView) findViewById(R.id.textViewMobilePhonePrefix);        	            
            mobilePhonePrefix.setText(userCountry.getPhoneCode());
        }
    }

    public abstract void init();
    
    public void forgotPasswordSubmitHandler(View target){
    	validateRegisterForm();
    }      
                
	public void validateRegisterForm(){
    	EditText email   = (EditText)findViewById(R.id.editTextEmail);
    	EditText mobilePhone = (EditText) findViewById(R.id.editTextMobilePhone);
    	EditText mobilePhonePrefix = (EditText) findViewById(R.id.textViewMobilePhonePrefix);
    	RadioGroup sendingMethod = (RadioGroup) findViewById(R.id.sendingMethod);
    	
    	Context context = ForgotPasswordCommonActivity.this;
    	//Client validations
    	String error = Utils.isFieldEmpty(context, mobilePhone.getText().toString(), getString(R.string.register_mobilephone));
    	if (null != error) {
            Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
            return ;
    	}
    	error = Utils.validateEmailAndEmpty(context, email.getText().toString(), getString(R.string.register_email)); 
    	if (error!= null){
    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	
    	//Service validations     	
    	AnyoptionApplication ap = (AnyoptionApplication) getApplication();
    	InsertUserMethodRequest request = new InsertUserMethodRequest();
        UserMethodResult result = new UserMethodResult();    
        Register user = new Register();
    	user.setEmail(email.getText().toString());	
    	user.setMobilePhone(mobilePhone.getText().toString());
    	user.setMobilePhonePrefix(mobilePhonePrefix.getText().toString());
    	user.setSkinId(ap.getSkinId());
    	///
    	int checkedRadioButtonId = sendingMethod.getCheckedRadioButtonId();
    	if(checkedRadioButtonId == R.id.emailRadio) {
    		request.setSendMail(true);
    		request.setSendSms(false);
    	} else {
    		request.setSendMail(false);
    		request.setSendSms(true);
    	}
    	///
    	error = setParams(user, error);
		if (error != null) {
			return ;
		}
        request.setRegister(user);
    	Utils.requestService(this, responseH, "sendPasswordCallBack", request, result, "sendPassword");
    	
	}
	
	public abstract String setParams(Register user, String error);
	
	public void sendPasswordCallBack(Object resultObj){
		Log.d(getPackageName(), "insertUserCallBack");
		Context context = ForgotPasswordCommonActivity.this;
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {			
			Log.d(getPackageName(), "Password sent successfully");	
			String error = result.getUserMessages()[0].getMessage();
			String field = "";
			Utils.showAlertAndGoLogin(context, field, error);			 		
			//TODO: what to do after success msg?
			
			/*((Activity) context).startActivityForResult(
			  new Intent(context,
			      LoginActivity.class),
			      Constants.REQUEST_CODE_LOGIN_PAGE);*/			
		} else {
			 Log.d(getPackageName(), "Error send forgotten password ");
			 String error = result.getUserMessages()[0].getMessage();
			 String field = result.getUserMessages()[0].getField();
			 Utils.showAlert(context, field, error, context.getString(R.string.alert_title));			 			
		}
	}
}