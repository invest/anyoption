package com.anyoption.android.app;
import java.math.BigDecimal;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.questionnaire.MandatoryQuestionnaireActivity;
import com.anyoption.android.app.questionnaire.QuestionnaireUtils;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.json.requests.DepositReceiptMethodRequest;
import com.anyoption.json.results.MethodResult;
import com.anyoption.json.results.TransactionMethodResult;
import com.google.android.apps.analytics.Transaction;

/**
 * 
 */

/**
 * @author AviadH
 *
 */
public class ReceiptActivity extends BaseActivity {
	
	protected long transId;
	protected Context context;
	protected Handler responseH;
	private boolean isDismissedFromButton = false;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
	}
	
	public void sendReceiptDeposit() {
    	DepositReceiptMethodRequest request = new DepositReceiptMethodRequest();
    	request.setTransactionId(transId);
    	Utils.requestService(this, responseH, "sendReceiptDepositCallback", request, new MethodResult(), "SendDepositReceipt");
    }
    
    public void sendReceiptDepositCallback(Object resultObj) {
    	MethodResult result = (MethodResult) resultObj;
    	if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
    		//TODO:wait for production.. meanwhile don't do anything
	    } else {
			Log.d(getPackageName(), "Send receipt fail");
			Utils.showAlert(this, "", result.getUserMessages()[0].getMessage(), context.getString(R.string.alert_title));
	    }
    }
    
    public void insertDepositCallBack(Object resultObj){
		Log.d(getPackageName(), "insertDepositCallBack");
		TransactionMethodResult result = (TransactionMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(getPackageName(), "Insert deposit succefully");
			handleSuccessfulDeposit(result);
		} else {
			Log.d(getPackageName(), "Insert deposit failed");
			Utils.handleResultError(context, result);
		}
	}

	private void handleSuccessfulDeposit(TransactionMethodResult result) {
		String msg = getString(R.string.creditcard_deposit_success) + " " + ((TextView)findViewById(R.id.TextViewCurrencySymbol)).getText() + getString(R.string.receipt_add, ((EditText)findViewById(R.id.editTextAmount)).getText().toString()) + "\n" + getString(R.string.receipt_transactionid, result.getTransactionId());
		transId = result.getTransactionId();
		showSuccessPopup(msg, true);
		
		//send deposit to google analytics
		tracker.addTransaction(new Transaction.Builder(
		        String.valueOf(result.getTransactionId()),
		        result.getDollarAmount())
		.setStoreName(String.valueOf(Utils.getCombinationId(this)))
		.setTotalTax(0)
		.setShippingCost(0)
		.build());
		tracker.trackTransactions();
		if (result.isFirstDeposit()) {
			try {
				BigDecimal amount = new BigDecimal(result.getDollarAmount());
				tracker.trackEvent(
					     "Deposit",// Category
					     "Success First Deposit",// Action
					     "Deposit",// Label
					     amount.intValue());// Value
			} catch (Exception e) {
				Log.e(getLocalClassName(), "cant send track event Success First Deposit", e);
			}
		}
		tracker.dispatch();
	}
    
    /**
     * Shows a success popup with "Trade now" and 
     * "Send receipt by email" options
     * 
     * @param  msg  the message to be shown in the popup
     * @param  showSendReceipt  whether to show the "Send receipt by email" option
     */
	protected void showSuccessPopup(String msg, final boolean showSendReceipt) {
		// to not start the questionnaire in onDismiss if 
		LayoutInflater inflater = getLayoutInflater();
		LinearLayout popup = (LinearLayout)inflater.inflate(R.layout.cc_deposit_success_pop_up, null);
		TextView textViewMsg = (TextView) popup.findViewById(R.id.textViewMsg);
		textViewMsg.setText(msg);
		
		final AlertDialog ad = Utils.showCustomAlert(this, popup, null);
		
		Button tradeNow = (Button) popup.findViewById(R.id.buttonTradeNow);
		tradeNow.setOnClickListener(new OnClickListener() {				
			public void onClick(View arg0) {
				
				if(Utils.isRegulated(context) && !QuestionnaireUtils.isMandQuestionnaireAnswered(context)) {
					Intent intent = new Intent(context, MandatoryQuestionnaireActivity.class);
					intent.putExtra(Constants.ACTIVITY_BEFORE_QUESTIONNAIRE, Constants.FROM_DEPOSIT);
					startActivity(intent);
				} else {
					Intent intent =  new Intent(context, MarketsGroupActivity.class);
					startActivityForResult(intent, Constants.REQUEST_CODE_MARKETS_GROUP_PAGE);
					
				}
				isDismissedFromButton = true;
				ad.dismiss();
				finish();
					
			}
		});
		
		Button sendReceipt = (Button) popup.findViewById(R.id.buttonReceiptEmail);
		if(showSendReceipt){
			sendReceipt.setOnClickListener(new OnClickListener() {				
				public void onClick(View arg0) {
						sendReceiptDeposit();
				    if(Utils.isRegulated(context) && !QuestionnaireUtils.isMandQuestionnaireAnswered(context)){
				    	Intent intent = new Intent(context, MandatoryQuestionnaireActivity.class);
				    	intent.putExtra(Constants.ACTIVITY_BEFORE_QUESTIONNAIRE, Constants.FROM_DEPOSIT);
						startActivity(intent);
				    } else {
				    	Utils.openTradingPage(ReceiptActivity.this);
				    }
				    isDismissedFromButton = true;
				    ad.dismiss();
				}
			});
		} else {
			sendReceipt.setVisibility(View.GONE);
		}
		
		ad.setOnDismissListener(new OnDismissListener() {
			
			public void onDismiss(DialogInterface dialog) {
				if(Utils.isRegulated(context) && !QuestionnaireUtils.isMandQuestionnaireAnswered(context) && !isDismissedFromButton){
					Intent intent = new Intent(context, MandatoryQuestionnaireActivity.class);
			    	intent.putExtra(Constants.ACTIVITY_BEFORE_QUESTIONNAIRE, Constants.FROM_DEPOSIT);
					startActivity(intent);
			    }
				// setting to false because it is a field and it stays true after opening multiple popups
		    	isDismissedFromButton = false;
		    	
			}
		});
		
	}
}
