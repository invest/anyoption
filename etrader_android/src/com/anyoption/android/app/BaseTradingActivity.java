package com.anyoption.android.app;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.anyoption.beans.base.Investment;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.results.ChartDataMethodResult;
import com.lightstreamer.ls_client.ExtendedTableInfo;
import com.lightstreamer.ls_client.UpdateInfo;

public class BaseTradingActivity extends BaseInsuranceActivity implements ChartDataListener {
    protected ExtendedTableInfo lsTable;
    protected ChartDataListener chart;
    protected MyOptionView myOptions;
    protected SlipView slip;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        chart = (ChartDataListener) findViewById(R.id.chart);
        slip = (SlipView) findViewById(R.id.relativeLayoutKeyboard);
        chart.setChartSlip(slip);
        slip.setChart(this);

        AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        User user = ap.getUser();
        if (null != user) {
            slip.setCurrencySymbol(user.getCurrencySymbol());
            slip.setCurrencyLeftSymbol(user.isCurrencyLeftSymbol());
            slip.setCurrencyId(user.getCurrencyId());
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        chartRequest();
    }
    
    protected void chartRequest() {
        // Do nothing
    }
    
    @Override
    public void onPause() {
        if (null != ls) {
            ls.removeTable(lsTable);
            lsTable = null;
        }
        super.onPause();
    }
    
    protected boolean closeKeyboardSearchOrBackToPortrait() {
        boolean processed = false;
        View keyboard = findViewById(R.id.relativeLayoutKeyboard);
        if (null != keyboard && keyboard.getVisibility() == View.VISIBLE) {
            keyboard.setVisibility(View.GONE);
            ((ChartDataListener) findViewById(R.id.chart)).closeSlip();
            processed = true;
        }
        View search = findViewById(R.id.relativeLayoutSearch);
        if (null != search && search.getVisibility() == View.VISIBLE) {
            search.setVisibility(View.GONE);
            processed = true;
        }
//        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//            processed = true;
//        }
        return processed;
    }
    
    @Override
    public void onBackPressed() {
        if (!closeKeyboardSearchOrBackToPortrait()) {
            super.finish();
        }
    }
    
    public void setChartData(ChartDataMethodResult data) {
        // do nothing. The chartCallBack handle that
    }
    
    @Override
    public void updateItem(int itemPos, String itemName, UpdateInfo update) {
        try {
        	if (itemName.startsWith("insurances")) {
        		super.updateItem(itemPos, itemName, update);
        		return;
        	}
            
            chart.updateItem(itemPos, itemName, update);
            if (null != myOptions) {
                myOptions.updateItem(itemPos, itemName, update);
            }

            if (update.isValueChanged("ET_STATE")) {
                if (null != update.getOldValue("ET_STATE")) {
                    int oldState = Integer.parseInt(update.getOldValue("ET_STATE"));
                    if (oldState == Opportunity.STATE_CREATED || oldState == Opportunity.STATE_PAUSED) {
                        chartRequest();
                        return;
                    }
                }
                int oppState = Integer.parseInt(update.getNewValue("ET_STATE"));
                if (oppState == Opportunity.STATE_DONE) {
                    chartRequest();
                }
            }
        } catch (Exception e) {
            Log.e(getLocalClassName() + " chart", "Problem processing updateItem.", e);
        }
    }
    
    public void setChartSlip(SlipView slip) {
        // do nothing
    }
    
    public void addInvestment(Investment inv) {
        chart.addInvestment(inv);
    }
    
    public void removeInvestment(long invId) {
        chart.removeInvestment(invId);
    }
    
    public void closeSlip() {
        chart.closeSlip();
    }
}