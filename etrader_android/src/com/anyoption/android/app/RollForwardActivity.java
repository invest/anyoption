package com.anyoption.android.app;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.SetTextRunnable;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Investment;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.InsurancesMethodRequest;
import com.anyoption.json.results.InsurancesMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

public class RollForwardActivity extends BaseLightstreamerActivity {
	private static final String CHECKBOX_STATUS = "status";
    private int insurancesType;
    private long insuranceCloseTime;
    private boolean[] checkBoxStatus = new boolean[4];
    private Bundle savedInstanceState;

    private LinearLayout timeView;
    private LinearLayout llMain;

	private CountDownTimer timer;
	private HashMap<String, LinearLayout> invs;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		this.savedInstanceState = savedInstanceState;
        initPage(savedInstanceState);
    }
	
	public void initPage(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.roll_forward);
	    Bundle extras = getIntent().getExtras();
	    if (null != extras) {
	        if (null != extras.get(Constants.PUT_EXTRA_INSURANCES_TYPE)) {
	            insurancesType = extras.getInt(Constants.PUT_EXTRA_INSURANCES_TYPE);
	        }
            if (null != extras.get(Constants.PUT_EXTRA_INSURANCES_CLOSE_TIME)) {
                insuranceCloseTime = extras.getLong(Constants.PUT_EXTRA_INSURANCES_CLOSE_TIME);
            }
	    }
	    
	    Calendar c = Calendar.getInstance();
	    c.setTime(Utils.getCurrentServerTime());
        int min = c.get(Calendar.MINUTE);
        if (min < 30) {
            c.set(Calendar.MINUTE, 30);
        } else {
            c.add(Calendar.HOUR_OF_DAY, 1);
            c.set(Calendar.MINUTE, 0);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
	    if (insurancesType == Investment.INSURANCE_TAKE_PROFIT) {
	        ((ImageView) findViewById(R.id.imageViewIconInsurances)).setImageResource(R.drawable.icon_takeprofit);
	        ((TextView) findViewById(R.id.textViewInsurancesHeader)).setText(getResources().getString(R.string.new_features_take_profit).toUpperCase());
            ((TextView) findViewById(R.id.textViewInsurancesDescription)).setText(getResources().getString(R.string.insurances_take_profit_description, sdf.format(c.getTime())));
	    } else {
            ((ImageView) findViewById(R.id.imageViewIconInsurances)).setImageResource(R.drawable.icon_rollforward);
            ((TextView) findViewById(R.id.textViewInsurancesHeader)).setText(getResources().getString(R.string.newFeatures_roll_title).toUpperCase());
            String crrExp = sdf.format(c.getTime());
            c.add(Calendar.HOUR, 1);
            ((TextView) findViewById(R.id.textViewInsurancesDescription)).setText(getResources().getString(R.string.insurances_roll_forward_description, crrExp, sdf.format(c.getTime())));
	    }
	    timeView = (LinearLayout) findViewById(R.id.linearLayoutClock);
	    llMain = (LinearLayout) findViewById(R.id.linearLayoutRollForwardInvestments);

	    ((Button) findViewById(R.id.buttonSell)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                buyInsurances();
            }
        });
	    ((Button) findViewById(R.id.buttonNotNow)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
	    });
	    
	    invs = new HashMap<String, LinearLayout>();
	    User user = ((AnyoptionApplication) getApplication()).getUser();
	    if (null != ls) {
	    	ls.stop();
	    	ls = null;
	    }
	    if (null != user) {
	        ls = getLightstreamerConnectionHandler(user);
	        subscribeInsurancesTable(user);
	        ls.start();
	        
	    }
	}

	private void startTimer() {
	    long timeLeft = insuranceCloseTime - Utils.getCurrentServerTime().getTime();
	    if (timeLeft <= 0) {
	        finish();
	        return;
	    }
        timer = new CountDownTimer(timeLeft, 300) {  
            @Override
            public void onTick(long millisUntilFinished) {
				char time[] = Utils.timeToMinutesAndSecondsFormat(millisUntilFinished).toCharArray();
				for (int i = 0 ; i < timeView.getChildCount() ; i++) {
					((TextView) timeView.getChildAt(i)).setText(Character.toString(time[i]));
				}
            }
            
            @Override
            public void onFinish() {
                Log.v("anyoption", "Insurances time is up.");
                finish();
            }
        }.start();
	}
	
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        
        if (null != checkBoxStatus) {
        	outState.putBooleanArray(CHECKBOX_STATUS, checkBoxStatus);
        }
    }
    
    @Override
    public void onResume() {
        llMain.removeAllViews();
        invs.clear();
        updateTotal();
        super.onResume();
        startTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (null != timer) {
        	timer.cancel();
        }
    }

    @Override
    public void updateItem(int itemPos, String itemName, UpdateInfo update) {
        try {
            if (itemName.startsWith("insurances")) {
                String oppId = update.getNewValue("key");
                String cmd = update.getNewValue("command");
                if (cmd.equals("ADD")) {
                    Log.v(getLocalClassName() + " lightstreamer", "Adding insurance - market: " + update.getNewValue("INS_MARKET") + " opp: " + oppId + " type: " + update.getNewValue("INS_INS_TYPE") + " close: " + update.getNewValue("INS_CLOSE_TIME") + " inv level: " + update.getNewValue("INS_LEVEL") + " inv pos: " + update.getNewValue("INS_POSITION"));
                    llMain.post(new AddRowRunnable(update));
                    ls.sendMessage("viewedInvId_" + update.getNewValue("key"));
                } else if (cmd.equals("UPDATE")) {
                    Log.v(getLocalClassName() + " lightstreamer", "UPDATE opp: " + oppId);
                    LinearLayout row = invs.get(oppId);
                    llMain.post(new SetTextRunnable((TextView) row.findViewById(R.id.textViewCurrentLevel), update.getNewValue(skinGroup.getInsCurrentLevelUpdateKey())));
                } else { // only DELETE left
                    Log.v(getLocalClassName() + " lightstreamer", "Removing insurance - market: " + update.getNewValue("INS_MARKET") + " opp: " + oppId + " type: " + update.getNewValue("INS_INS_TYPE") + " close: " + update.getNewValue("INS_CLOSE_TIME"));
                    llMain.post(new DelRowRunnable(oppId));
                }
            }
        } catch (Exception e) {
            Log.e(getLocalClassName() + " lightstreamer", "Problem processing updateItem.", e);
        }
    }
    
    private String calculateTotal() {
        double total = 0;
        try {
            NumberFormat nf = NumberFormat.getInstance(Locale.US);
            for (Iterator<LinearLayout> i = invs.values().iterator(); i.hasNext();) {
                LinearLayout row = i.next();
                if (((CheckBox) row.findViewById(R.id.checkBoxRoll)).isChecked()) {
                    total += nf.parse(((TextView) row.findViewById(R.id.textViewPremiumPrice)).getText().toString().substring(1)).doubleValue();// 1 for without symobl
                }
            }
        } catch (Exception e) {
            Log.e("anyoption", "Can't calc total.", e);
        }
        User user = ((AnyoptionApplication) getApplication()).getUser();
        return Utils.formatCurrencyAmount(total * 100, user.getCurrencySymbol(), user.isCurrencyLeftSymbol(), user.getCurrencyId());
    }
    
    private void updateTotal() {
        ((TextView) findViewById(R.id.textViewInsurancesTotal)).setText(calculateTotal());
    }
    
    private void buyInsurances() {
        try {
            ArrayList<Long> invsToSend = new ArrayList<Long>();
            ArrayList<Double> amountsToSend = new ArrayList<Double>();
            NumberFormat nf = NumberFormat.getInstance(Locale.US);
            for (Iterator<String> i = invs.keySet().iterator(); i.hasNext();) {
                String key = i.next();
                LinearLayout row = invs.get(key);
                if (((CheckBox) row.findViewById(R.id.checkBoxRoll)).isChecked()) {
                    amountsToSend.add(nf.parse(((TextView) row.findViewById(R.id.textViewPremiumPrice)).getText().toString().substring(1)).doubleValue());// 1 for remove symbol
                    invsToSend.add(Long.parseLong(key));
                }
            }
            if (invsToSend.size() > 0) {
                InsurancesMethodRequest request = new InsurancesMethodRequest();
                request.setInsuranceType(insurancesType);
                request.setInvestmentIds(invsToSend.toArray(new Long[0]));
                request.setInsuranceAmounts(amountsToSend.toArray(new Double[0]));
                timer.cancel();
                Utils.requestService(this, responseH, "buyInsurancesCallback", request, new InsurancesMethodResult(), "buyInsurances");
            } else {
            	LayoutInflater inflater = getLayoutInflater();
                LinearLayout l = (LinearLayout) inflater.inflate(R.layout.rotate_phone_popup, null);
                ((TextView)l.findViewById(R.id.textViewContent)).setText(getString(R.string.golden_no_inv_selected_RU));
                Button closeButton  = (Button) l.findViewById(R.id.buttonRotatePhonePopup);
                Utils.showCustomAlert(this, l, closeButton);
            }
        } catch (Exception e) {
            Log.e(getLocalClassName() + " anyoption", "Can't send buyInsurances request", e);
        }
    }
    
    public void buyInsurancesCallback(Object resultObj) {
        InsurancesMethodResult result = (InsurancesMethodResult) resultObj;
        if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            LinearLayout l = (LinearLayout) getLayoutInflater().inflate(R.layout.roll_forward_popup, null);
            LinearLayout llMainTitle = (LinearLayout) getLayoutInflater().inflate(R.layout.gm_plus_popup_header, null);
            if (insurancesType == Investment.INSURANCE_TAKE_PROFIT) {
            	((TextView) llMainTitle.findViewById(R.id.textViewMarketName)).setText(getResources().getString(R.string.new_features_take_profit).toUpperCase());
        		((ImageView) llMainTitle.findViewById(R.id.imageViewPlusBig)).setImageResource(R.drawable.icon_takeprofit);
                ((TextView) l.findViewById(R.id.textViewInsurancesSuccess)).setText(getResources().getString(R.string.insurances_take_profit_success, result.getCredit()));
            } else {
        		((ImageView) llMainTitle.findViewById(R.id.imageViewPlusBig)).setImageResource(R.drawable.icon_rollforward);
                ((TextView) llMainTitle.findViewById(R.id.textViewMarketName)).setText(getResources().getString(R.string.newFeatures_roll_title).toUpperCase());
                ((TextView) l.findViewById(R.id.textViewInsurancesSuccess)).setText(getResources().getString(R.string.golden_recipt_txt1_RU));
            }
            String str = "";
            for (int i = 0; i < result.getResults().length; i++) {
                if (result.getResults()[i].startsWith("OK")) {
                	long invId = result.getInvestmentIds()[i];
                    str += getResources().getString(R.string.insurances_success_option, invId);
                    ls.sendMessage("invId_" + invId);
                } else {
                    str += getResources().getString(R.string.insurances_success_option, result.getInvestmentIds()[i] + " - " + result.getResults()[i]);
                }
                str += "\n";
            }
            ((TextView) l.findViewById(R.id.textViewInsurancesOptionId)).setText(str);
            Button b = (Button) l.findViewById(R.id.buttonInsurancesOK);
            Utils.showCustomAlert(this, l, b, new DialogInterface.OnDismissListener() {	
                public void onDismiss(DialogInterface dialog) {
                	if (invs.isEmpty()) {
                		finish();
                	}
                    startTimer();
                }
            }, llMainTitle, 100);
        }
    }
    
    @Override 
    public void onConfigurationChanged(Configuration newConfig) { 
        super.onConfigurationChanged(newConfig);
        initPage(savedInstanceState);
    }
    
    class AddRowRunnable implements Runnable {
        private UpdateInfo update;
        
        public AddRowRunnable(UpdateInfo update) {
            this.update = update;
        }
        
        public void run() {
            LayoutInflater inflater = getLayoutInflater();
            LinearLayout row = (LinearLayout) inflater.inflate(R.layout.roll_forward_option_roll, llMain, false);
            final int position = Integer.valueOf(update.getNewValue("INS_POSITION"));
            row.setTag(position);
            User user = ((AnyoptionApplication) getApplication()).getUser();
            ((TextView) row.findViewById(R.id.textViewAssetName)).setText(update.getNewValue("INS_MARKET_NAME"));
            int rId = update.getNewValue("INS_TYPE").equals(String.valueOf(Investment.INVESTMENT_TYPE_CALL)) ? R.drawable.arrow_call_small_gm : R.drawable.arrow_put_small_gm;
            ((ImageView) row.findViewById(R.id.imageViewInvestmentType)).setImageResource(rId);
            ((TextView) row.findViewById(R.id.textViewInvestmentLevel)).setText(update.getNewValue("INS_LEVEL"));
            ((TextView) row.findViewById(R.id.textViewCurrentLevel)).setText(update.getNewValue(skinGroup.getInsCurrentLevelUpdateKey()));
            ((TextView) row.findViewById(R.id.textViewInvestment)).setText(Utils.formatCurrencyAmount(100 * Double.parseDouble(update.getNewValue("INS_AMOUNT").replaceAll(",", "")), user.getCurrencySymbol(), user.isCurrencyLeftSymbol(), user.getCurrencyId()));
            if (insurancesType == Investment.INSURANCE_TAKE_PROFIT) {
                ((TextView) row.findViewById(R.id.textViewReturn)).setText(Utils.formatCurrencyAmount(100 * Double.parseDouble(update.getNewValue("INS_WIN").replaceAll(",", "")), user.getCurrencySymbol(), user.isCurrencyLeftSymbol(), user.getCurrencyId()));
            } else {
                ((TextView) row.findViewById(R.id.textViewReturnLbl)).setVisibility(View.GONE);
                ((TextView) row.findViewById(R.id.textViewReturn)).setVisibility(View.GONE);
            }
            ((TextView) row.findViewById(R.id.textViewPremiumPrice)).setText(Utils.formatCurrencyAmount(100 * Double.parseDouble(update.getNewValue("INS_INS").replaceAll(",", "")), user.getCurrencySymbol(), user.isCurrencyLeftSymbol(), user.getCurrencyId()));
            CheckBox check = (CheckBox) row.findViewById(R.id.checkBoxRoll);
            check.setChecked(checkBoxStatus[position]);
            check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                	checkBoxStatus[position] = !checkBoxStatus[position];
                	updateTotal();
                }
            });
            int index = 0;
            View tr;
            int rowPosition;
            for (int i = 0; i < llMain.getChildCount(); i++) {
                tr = llMain.getChildAt(i);
                rowPosition = (Integer) tr.getTag();
                if (rowPosition > position) {
                    index = i;
                    break;
                }
                index = i + 1;
            }
            llMain.addView(row, index);
            invs.put(update.getNewValue("key"), row);
            updateTotal();
        }
    }
    
    class DelRowRunnable implements Runnable {
        private String oppId;
        
        public DelRowRunnable(String oppId) {
            this.oppId = oppId;
        }
        
        public void run() {
        	LinearLayout row = invs.get(oppId);
            row.setVisibility(View.GONE);
            ((CheckBox) row.findViewById(R.id.checkBoxRoll)).setChecked(false);
            invs.remove(oppId);
            updateTotal();
        }
    }
    
    @Override
    public void finish() {
    	Intent result = new Intent();
    	result.putExtra(Constants.PUT_EXTRA_INSURANCES_COUNT, invs.size());
    	setResult(RESULT_OK, result); 
    	super.finish();
    }
}