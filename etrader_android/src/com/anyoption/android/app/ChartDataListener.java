package com.anyoption.android.app;

import com.anyoption.beans.base.Investment;
import com.anyoption.json.results.ChartDataMethodResult;

public interface ChartDataListener extends ChartLightstreamerListener {
    public void setChartData(ChartDataMethodResult data);
    public void setChartSlip(SlipView slip);
    public void addInvestment(Investment inv);
    public void removeInvestment(long invId);
    public void closeSlip();
}