package com.anyoption.android.app;

public interface Initializable {
	void initialize();
}
