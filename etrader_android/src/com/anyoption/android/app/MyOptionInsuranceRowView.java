package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.TableRow;

public class MyOptionInsuranceRowView extends TableRow {
    protected InsuranceView insuranceView;
    
    public MyOptionInsuranceRowView(Context context) {
        super(context);

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        inflater.inflate(R.layout.my_option_row_insurance, this, true);
        
        insuranceView = (InsuranceView) findViewById(R.id.myOptionsInsuranceView);
    }
    
    public void initAndStart(int insurancesType, long insuranceCloseTime) {
        insuranceView.initAndStart(insurancesType, insuranceCloseTime);
    }
}