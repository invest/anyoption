package com.anyoption.android.app;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.User;

public class OtherMenuActivity extends BaseMenuActivity {
	
	private User user;
	private ChangeLanguageView changeLanguage;
	private DetailsView riskDisclosureView;
	
	 /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        user = ap.getUser();  
        responseH = new Handler();
        setContentView(R.layout.other_menu_login);
        menuScrollView = (ScrollView)findViewById(R.id.scrollViewMenu);
        changeLanguage = (ChangeLanguageView) findViewById(R.id.changeLanguageView);
        riskDisclosureView = (DetailsView) findViewById(R.id.otherRiskDisclosureView);
        
        if(!Utils.isRegulated(this)){
        	riskDisclosureView.setVisibility(View.GONE);
        }
		if (null == user) {
			//setContentView(R.layout.other_menu_logout);
			HeaderLayout header = (HeaderLayout) findViewById(R.id.headerLayout);
			header.setVisibility(View.GONE);
			LogoutView logout = (LogoutView) findViewById(R.id.logoutView);
			logout.setVisibility(View.GONE);
			
			if (Utils.isEtraderProject(this)){
				changeLanguage.setVisibility(View.GONE);
			}
		} else {
				changeLanguage.setVisibility(View.GONE);
			}
		// No language selector in all cases for Chinese only apk
		if(Constants.IS_CHINESE_ONLY){
			changeLanguage.setVisibility(View.GONE);
		}
		//set the manage menu custom event to menu strips
		LinearLayout menu = (LinearLayout)findViewById(R.id.linearLayoutMenu);
        for (int i = 0 ; i < menu.getChildCount() ; i++) {
        	((MenuViewBase)menu.getChildAt(i)).setOnMenuStripClickListener(new OnMenuStripClickListener() {
				public void onClickCloseAll(MenuViewBase menuViewBase) {
					manageOpenMenuStrip(menuViewBase);
				}
			});
        }
    }
}
