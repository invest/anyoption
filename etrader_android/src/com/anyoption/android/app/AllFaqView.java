/**
 * 
 */
package com.anyoption.android.app;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.Utils;

/**
 * @author AviadH
 *
 */
public class AllFaqView extends MenuViewBase {

	protected View lastOpenGroup;

	public AllFaqView(Context context) {
		this(context, null);
	}
	
	public AllFaqView(Context context, AttributeSet attrs) {
		super(context, null);
		initComponent();
	}
	
	public void initComponent() {
		TableRow tr = (TableRow) inflater.inflate(R.layout.all_faq_row, tlMain, false);
		tlMain.addView(tr);
		LinearLayout menu = (LinearLayout)findViewById(R.id.linearLayoutMenu);
	       for (int i = 0 ; i < menu.getChildCount() ; i++) {
	    	//for the menu close open control
	    	FaqView faq = (FaqView) menu.getChildAt(i);
	        ((MenuViewBase)menu.getChildAt(i)).setOnMenuStripClickListener(new OnMenuStripClickListener() {
				public void onClickCloseAll(MenuViewBase menuViewBase) {
					manageOpenMenuStrip(menuViewBase);
				}
			});
	        
	        //set visual changes
	        faq.menuLine.findViewById(R.id.linearLayoutGroup).setBackgroundColor(0xA0E1E1E1);
	        TextView textHeader = (TextView) faq.menuLine.findViewById(R.id.textViewGroupName);
	        textHeader.setTextColor(0xFFFFFFFF);
	        textHeader.setText(Utils.getStringFromResource(context, "faq." + faq.getMenuType(), null));
	        faq.setPadding(faq.getPaddingLeft(), -2, faq.getPaddingRight(), faq.getPaddingBottom());
	        ((ImageView) faq.menuLine.findViewById(R.id.imageViewGroupArrow)).setImageResource(R.drawable.arrow_nav_right);
	    }
	}
	
	public String getMenuStripText() {
		String header = getResources().getString(R.string.faq_header,"");
		return header.substring(0, header.length() - 2);
	}
	
	
	
	 /*manage the menu on her activiy and close and open according to type of component
	 * (if he is custom ,the close and open will occured in the custom view itself
	 * an if he is created dynmically inside an activity the close and open will occure in the activity*/
   public void manageOpenMenuStrip(View target) {
   	boolean shouldOpen = false;
   	//check if target and last are menuBase (custom component menu)
   	boolean isTargetMenuBase = false;
   	boolean isLastMenuBase = false;
   	if (target instanceof MenuViewBase) {
   		isTargetMenuBase = true;
   	}
   	if (lastOpenGroup instanceof MenuViewBase) {
   		isLastMenuBase = true;
   	}
		if (lastOpenGroup != target) {
   		shouldOpen = true;
   		if (null != lastOpenGroup) {
   			if (isLastMenuBase) {
   				((MenuViewBase)lastOpenGroup).closeGroup();
   			} else {
   				closeGroup(lastOpenGroup);
   			}
   		}
   	} else {
   		if (isTargetMenuBase) {
   			((MenuViewBase)target).closeGroup();
   		} else {
   			closeGroup(target);
   		}
   		lastOpenGroup = null;
   	}
   	if (shouldOpen) {
   		if (isTargetMenuBase) {
   			((MenuViewBase)target).openGroup();
   		} else {
   			openGroup(target);
   		}
   		if (menuScrollView == null) {
    		menuScrollView = ((BaseMenuActivity) context).getMenuScrollView();
    	}
   		if (null != menuScrollView) {
			menuScrollView.post(new ScrollViewScrollRunnable(menuScrollView, target));
		}
   		lastOpenGroup = target;
   	}
   }
   
   public void openGroup(View target) { 	
   }
   
   public void closeGroup(View target) { 	
   }
   
   public void removeAllRows() {
   }

}
