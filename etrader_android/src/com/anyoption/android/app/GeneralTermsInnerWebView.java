package com.anyoption.android.app;

import android.app.Dialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class GeneralTermsInnerWebView extends WebView {


	public GeneralTermsInnerWebView(final Context context) {
		super(context);
		final ProgressBar pb = new ProgressBar(context);
		pb.setIndeterminate(true);
		setWebViewClient(new WebViewClient() {

			private Dialog networkProgressPopup;

			@Override
			public void onLoadResource(WebView view, String url) {
				networkProgressPopup = new Dialog(context,
						R.style.network_progress_dialog);
				networkProgressPopup
						.setContentView(R.layout.network_progress_popup);
				networkProgressPopup.setCancelable(false);
				networkProgressPopup.show();

				super.onLoadResource(view, url);
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				networkProgressPopup.dismiss();
			}

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				networkProgressPopup.dismiss();
			}
		});
	}

	public GeneralTermsInnerWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public GeneralTermsInnerWebView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		requestDisallowInterceptTouchEvent(true);
		return super.onTouchEvent(event);
	}

}