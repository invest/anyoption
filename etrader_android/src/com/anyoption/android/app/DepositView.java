package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.UserMethodRequest;
import com.anyoption.json.results.CreditCardsMethodResult;

public class DepositView extends BankingViewBase {
	
	private final static int CREDIT_DEBIT_ROW_INDEX = 0;
	private final static int BANK_WIRE_INDEX = 1;
	private final static int DOMESTIC_PAYMENTS_INDEX = 2;
	private final static int MONEY_BOOKERS = 3;
	private final static int CUP_DEPOSIT = 4;
	private final static int BAROPAY_DEPOSIT = 5;
	private final static String DEPOSIT_CC = "deposit_creditcards";
	public final static String DEPOSIT_CC_NEW = "new_card";
	private final static String DEPOSIT_BANK_WIRE = "deposit_bank_wire";
	private final static String DEPOSIT_ENVOY = "deposit_envoy";
	private final static String DEPOSIT_MOBEY_BOOKERS = "deposit_money_bookers";
	private final static String DEPOSIT_CUP = "cup_deposit";
	private final static String DEPOSIT_BARO = "baropay_deposit";
	private final static long CURRENCY_CNY_ID = 7;
	
	private User user;
	private AnyoptionApplication ap;
	private CreditCard[] ccListDeposit;
	private String cctype;
	private PaymentMethod payment;
	
	public DepositView(Context context) {
		this(context, null);
	}
	
	public DepositView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initComponent();
	}
	
	public void initComponent() {
		ap = (AnyoptionApplication) ((Activity) context).getApplication();
		user = ap.getUser();
		makeFirstRows();
		makePaymentMethodsRows(Constants.PAYMENT_TYPE_DEPOSIT);
	}
	
	public void makeFirstRows() {
		String [] rowsText = getResources().getStringArray(R.array.banking_deposit_rows);
		for (int i = 0 ; i < rowsText.length; i++) {
			if (i == MONEY_BOOKERS && (Utils.isEtraderProject(context) || user.getSkinId() == Skin.SKIN_KOREAN || 
					user.getCurrencyId() == CURRENCY_CNY_ID)) {
				//Do not display MoneyBookers in ET project, for Korean skin or if the currency is CNY.
				continue; 
			}
			if(i == CUP_DEPOSIT && user.getCurrencyId() != Constants.CURRENCY_YUAN){
				continue;
			}
			/*if(i == BAROPAY_DEPOSIT && (user.getCountryId() != Constants.COUNTRY_SOUTH_KOREA_ID || 
				   user.getCurrencyId() != Constants.CURRENCY_WON ||
				   user.getSkinId() != Skin.SKIN_KOREAN)){
				continue;
			}*/
			if(i == BAROPAY_DEPOSIT) {
				// This deposit type should not be shown.
				continue;
			}
			if (i == DOMESTIC_PAYMENTS_INDEX && (user.getSkinId() == Skin.SKIN_GERMAN || user.getSkinId() == Skin.SKIN_KOREAN)) {
				continue; //Do not display Domestic Payments in DE/KR skin 
			} else {
				TableRow tr = (TableRow) inflater.inflate(R.layout.banking_deposit_withdrawal_row, tlMain, false);
				TextView textContent = (TextView) tr.findViewById(R.id.textViewBankingRowText);
				ImageView imageContent = (ImageView) tr.findViewById(R.id.textViewBankingRowImage);
				textContent.setText(Utils.getStringFromResource(context, rowsText[i] + "_text", null));
				if(rowsText[i].contains("credit") && Utils.isRegulated(context)){
					imageContent.setImageResource(Utils.getImageFromResource(context, rowsText[i]+"_regulated"));
				} else {
					imageContent.setImageResource(Utils.getImageFromResource(context, rowsText[i]));
				}
				setClickOnRow(i, tr); //e.g instead of header_creditcards is deposit_creditcards
				tlMain.addView(tr);
			}
		}
	}
	
	public void setClickOnRow(int index, TableRow tr) {
		payment = new PaymentMethod();
		if (index == CREDIT_DEBIT_ROW_INDEX) {
			tr.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					initCCDeposit(v);
				}
			});
		} else if (index == BANK_WIRE_INDEX) {
			tr.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					payment = new PaymentMethod();
					payment.setId(PaymentMethod.PAYMENT_TYPE_WIRE);	    
				    payment.setBigLogo("logo_creditcards_big");
				    payment.setTypeId(Constants.PAYMENT_TYPE_DEPOSIT);
					bankingRowClick(DEPOSIT_BANK_WIRE, null, 0, R.string.deposit_bank_wire, payment);
				}
			});
		} else if (index == DOMESTIC_PAYMENTS_INDEX) {
			tr.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					payment = new PaymentMethod();
					payment.setId(PaymentMethod.PAYMENT_TYPE_ENVOY);	
					payment.setTypeId(Constants.PAYMENT_TYPE_DEPOSIT);
					payment.setDisplayName("payment.type.domestic.payment");
					bankingRowClick(DEPOSIT_ENVOY, null, 0, R.string.header_envoy, payment);
				}
			});
		} else if (index == MONEY_BOOKERS && !Utils.isEtraderProject(context)) {
			tr.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					payment = new PaymentMethod();
					payment.setDisplayName("payment.type.moneybookers");
					payment.setTypeId(Constants.PAYMENT_TYPE_DEPOSIT);
					payment.setBigLogo("logo_moneybookers_big");
					bankingRowClick(DEPOSIT_MOBEY_BOOKERS, null, R.drawable.header_moneybookers, 
							R.string.header_moneybookers_text, payment);
				}
			});
		} else if (index == CUP_DEPOSIT && !Utils.isEtraderProject(context) && user.getCurrencyId() == Constants.CURRENCY_YUAN){ 
			tr.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					payment = new PaymentMethod();
					payment.setDisplayName("payment.type.cc");
					payment.setTypeId(Constants.PAYMENT_TYPE_DEPOSIT);
					payment.setBigLogo("logo_cup_big");
					bankingRowClick(DEPOSIT_CUP, null, R.drawable.logo_cup_small, 
							R.string.header_cup_deposit_text, payment);
				}
			});
		} /*else if (index == BAROPAY_DEPOSIT && !Utils.isEtraderProject(context) && 
				   user.getCountryId() == Constants.COUNTRY_SOUTH_KOREA_ID && 
				   user.getCurrencyId() == Constants.CURRENCY_WON && 
				   user.getSkinId() == Skin.SKIN_KOREAN){
			
			tr.setOnClickListener(new OnClickListener() {				
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					payment = new PaymentMethod();
					payment.setDisplayName("payment.type.baropay"); //set display name for PaymentMethodPopupActivity (text content)
					payment.setTypeId(Constants.PAYMENT_TYPE_DEPOSIT); // //set type for PaymentMethodPopupActivity (text content)
					payment.setBigLogo("logo_baro_big");
					bankingRowClick(DEPOSIT_BARO, null, R.drawable.logo_baro_small, 
							R.string.header_baropay_deposit_text, payment);
				}
			});
			
		}*/
	}

//	public void navigateToDomesticPayment(View target) { 
//    	context.startActivity(new Intent(context, DomesticPaymentDepositActivity.class)); 
//    }
	
	public void initCCDeposit(View target) { 
    	CreditCardsMethodResult result = new CreditCardsMethodResult(); 
    	Utils.requestService(view, responseH, "ccListDepositCallBack", new UserMethodRequest(), result, "getAvailableCreditCardsWithEmptyLine", (Activity) context);
    }
	
//    public void navigateToBankWire(View target) { 
//    	context.startActivity(new Intent(context, BankWireDepositActivity.class)); 
//    }
	
    public void ccListDepositCallBack(Object result) {        
        CreditCardsMethodResult omr = (CreditCardsMethodResult) result;
        ccListDeposit = omr.getOptions();           
    	Intent intent = null;
    	String type = null;
    	if (null != ccListDeposit && ccListDeposit.length > 1) {
    		intent = new Intent(context, CcDepositActivity.class);    	
    		intent.putExtra(Constants.PUT_EXTRA_CC_LIST, ccListDeposit);
    		type = DEPOSIT_CC;
    	} else {
    		Class<Activity> redirectActivity = Utils.getActivityClass("NewCardActivity", context);
    		intent = new Intent(context, redirectActivity);
    		type = DEPOSIT_CC_NEW;
    	}
    	payment = new PaymentMethod();
    	payment.setId(PaymentMethod.PAYMENT_TYPE_CC);
    	payment.setDisplayName("payment.type.cc");
	    payment.setBigLogo("logo_creditcards_big");
	    payment.setTypeId(Constants.PAYMENT_TYPE_DEPOSIT);
	    
    	int headerCreditCards = R.drawable.header_creditcards;
    	if(Utils.isRegulated(context)){
    		headerCreditCards = R.drawable.header_creditcards_regulated;
    	}
    	
		bankingRowClick(type, intent, headerCreditCards ,R.string.depositNewCardSubHeader, payment);
    }

	public String getMenuStripText() {
		return getResources().getString(R.string.profitLine_deposit);
	}
	
	public void openGroup() {
		super.openGroup();
		findViewById(R.id.textViewDepositDescription).setVisibility(View.VISIBLE);
		
	}
	
	public void closeGroup() {
		super.closeGroup();
		findViewById(R.id.textViewDepositDescription).setVisibility(View.GONE);
	}
	
}
