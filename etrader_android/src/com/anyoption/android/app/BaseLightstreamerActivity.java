package com.anyoption.android.app;

import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.app.util.Constants;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.enums.SkinGroup;
import com.lightstreamer.ls_client.ConnectionInfo;
import com.lightstreamer.ls_client.ExtendedTableInfo;
import com.lightstreamer.ls_client.UpdateInfo;

public class BaseLightstreamerActivity extends BaseMenuActivity implements LightstreamerConnectionHandler.StatusListener, ChartLightstreamerListener {
    protected LightstreamerConnectionHandler ls;

    protected SkinGroup skinGroup;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        AnyoptionApplication application = (AnyoptionApplication) getApplication();
        skinGroup = application.getSkins().get(application.getSkinId()).getSkinGroup();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (null != ls) {
            ls.start();
        }
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if (null != ls) {
            ls.stop();
        }
    }
    
    protected LightstreamerConnectionHandler getLightstreamerConnectionHandler(User user) {
        ConnectionInfo cInfo = new ConnectionInfo();
        cInfo.pushServerUrl = Constants.LS_URL;
        cInfo.adapter = Constants.LS_ADAPTER;
//        cInfo.streamingTimeoutMillis = 3000;
        if (null != user) {
            cInfo.user = "a" + user.getUserName() + "a"; // Leading and trailing "a"s are added because if the name starts with digits Lightstreamer is dropping them
            cInfo.password = user.getHashedPassword();
        }
        return new LightstreamerConnectionHandler(cInfo, this);
    }

    protected void subscribeInsurancesTable(User user) {
        try {
            ExtendedTableInfo ti = new ExtendedTableInfo(new String[] {"insurances_" + user.getUserName()}, "COMMAND", new String[] {"key", "command", "INS_MARKET", "INS_LEVEL", skinGroup.getInsCurrentLevelUpdateKey(), "INS_AMOUNT", "INS_WIN", "INS_INS", "INS_TYPE", "INS_POSITION", "INS_INS_TYPE", "INS_LEVEL_CLR", "INS_CLOSE_TIME", "INS_MARKET_NAME"}, true);
            ti.setDataAdapter(Constants.LS_DATA_ADAPTER);
            ti.setRequestedMaxFrequency(1.0);
            ls.addTable(ti, new AOTPSTableListener(this));
        } catch (Exception e) {
            Log.e("lightstreamer", "Can't subscribe insurances.", e);
        }
    }
    
    public void updateItem(int itemPos, String itemName, UpdateInfo update) {
        // do nothing
    }

    public void onStatusChange(int status) {
        Log.v("lightstreamer", "Status change: " + status);
    }
}