package com.anyoption.android.app;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.anyoption.android.app.util.ChartSlideListener;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.json.requests.ChartDataMethodRequest;
import com.anyoption.json.requests.CheckUpdateMethodRequest;
import com.anyoption.json.results.ChartDataMethodResult;
import com.anyoption.json.results.MethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

public class Home_pageActivity extends BaseTradingActivity {

	private SharedPreferences sharedPreferences;
	private ViewFlipper viewFlipper;
	private PageControl viewFlipperControl;
	private HPChartSlideListener chartSlideListener;
	boolean isFirstTime;
	private LoginHelper loginHelper;

	public Home_pageActivity() {
		refreshHeaderOnResume = false;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.home_page);
		super.onCreate(savedInstanceState);

		// check user status first visit/second visit +/returning user
		sharedPreferences = getSharedPreferences(Constants.SHARED_PREF,
				MODE_PRIVATE);
		// is first time user
		isFirstTime = sharedPreferences.getBoolean(
				Constants.PREF_FIRST_TIME_USER, true);
		String userNameSP = sharedPreferences.getString(
				Constants.PREF_USER_NAME, null);
		viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);
		viewFlipperControl = (PageControl) findViewById(R.id.pageControl);
		if (isFirstTime) { // first visit
			sharedPreferences.edit()
					.putBoolean(Constants.PREF_FIRST_TIME_USER, false).commit();
			chart = null;
			slip = null;
			ls = null;
			addDemo();
		} else if (null == userNameSP) { // second visit +
			// do nothing
		} else { // returning user
			findViewById(R.id.linearLayoutBottom).setVisibility(View.GONE);
			findViewById(R.id.customViewLoginView).setVisibility(View.VISIBLE);
		}
		viewFlipperControl.setPageCount(viewFlipper.getChildCount());
		chartSlideListener = new HPChartSlideListener(this,
				(HomePageChartView) chart, viewFlipper, viewFlipperControl);
		viewFlipper.setOnTouchListener(chartSlideListener);
		viewFlipperControl.setListener(chartSlideListener);

		// set login text with space in front and underline
		SpannableString content = new SpannableString(" "
				+ getString(R.string.header_login));
		content.setSpan(new UnderlineSpan(), 1, content.length(), 0);
		TextView loginLink = ((TextView) findViewById(R.id.TextViewLogin));
		loginLink.setText(content);
		loginLink.setBackgroundDrawable(this.getResources().getDrawable(
				R.drawable.on_click_bg));

		if (null != getIntent().getExtras()
				&& null != getIntent().getExtras().get(
						Constants.PUT_EXTRA_UPDATE_SKIN)) {
			CheckUpdateMethodRequest request = new CheckUpdateMethodRequest();
			Utils.requestService(this, responseH, "skinUpdateCallBack",
					request, new MethodResult(), "skinUpdate");
		}
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		loginHelper = new LoginHelper(this);
	}

	private void addDemo() {
		viewFlipper.removeAllViews();
		viewFlipper.setForegroundGravity(Gravity.CENTER_HORIZONTAL);
		LayoutParams lp = new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT);
		ImageView iv = new ImageView(this);
		iv.setImageResource(R.drawable.homepage_demo_1);
		viewFlipper.addView(iv, lp);
		iv = new ImageView(this);
		iv.setImageResource(R.drawable.homepage_demo_2);
		viewFlipper.addView(iv, lp);
		iv = new ImageView(this);
		iv.setImageResource(R.drawable.homepage_demo_3);
		viewFlipper.addView(iv, lp);
		iv = new ImageView(this);
		iv.setImageResource(R.drawable.homepage_demo_4);
		viewFlipper.addView(iv, lp);
		iv = new ImageView(this);
		iv.setImageResource(R.drawable.homepage_demo_5);
		viewFlipper.addView(iv, lp);
		iv = new ImageView(this);
		iv.setImageResource(R.drawable.homepage_demo_6);
		viewFlipper.addView(iv, lp);
	}

	public void chartRequest() {
		if (null != ls && null != lsTable) {
			ls.removeTable(lsTable);
			lsTable = null;
		}
		ChartDataMethodRequest request = new ChartDataMethodRequest();
		request.setBox(viewFlipperControl.getCurrentPage());
		request.setOpportunityTypeId(Opportunity.TYPE_REGULAR);
		Utils.requestService(this, responseH, "chartCallBack", request,
				new ChartDataMethodResult(), "getChartData");
	}

	public void chartCallBack(Object resultObj) {
		Log.d(getPackageName(), "chartCallBack");
		ChartDataMethodResult result = (ChartDataMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			ChartView.fixDates(result);
			if (null != chart) {
				chart.setChartData(result);
			}
			if (viewFlipper.getDisplayedChild() != 0) {
				viewFlipper.setInAnimation(null);
				viewFlipper.setOutAnimation(null);
				viewFlipper.setDisplayedChild(0);
			}
			if (null != ls) {
				lsTable = Utils.addLightstreamerTable(this,
						new String[] { "aotps_" + result.getScheduled() + "_"
								+ result.getMarketId() }, ls,
						new AOTPSTableListener(this), 1.0);
			}
		}
	}

	public void skinUpdateCallBack(Object resultObj) {
		Log.v(getPackageName(), "skinUpdateCallBack - "
				+ ((MethodResult) resultObj).getErrorCode());
	}

	@Override
	public void onResume() {
		Log.v("Home_pageActivity", "onResume");
		loginHelper.handleOnResume();
		super.onResume();
	}

	public void openPage(View target) {
		Log.d(getPackageName(), "open page");
		openPageInternal(target.getId());
	}

	private void openPageInternal(int targetId) {
		Log.d(getPackageName(), "open page");
		Intent intent = null;
		int requestCode = 0;
		if (targetId == R.id.TextViewLogin) {
			requestCode = Constants.REQUEST_CODE_LOGIN_PAGE;
			intent = new Intent(this, LoginActivity.class);
		} else if (targetId == R.id.buttonOpenAccount) {
			requestCode = Constants.REQUEST_CODE_REGISTER;
			Class<Activity> actvityRedirect = Utils.getActivityClass(
					Constants.ACTIVITY_REDIRECT_REGISTER, this);
			intent = new Intent(this, actvityRedirect);
		} else if (targetId == R.id.chart) {
			requestCode = Constants.REQUEST_CODE_MARKETS_GROUP_PAGE;
			intent = new Intent(this, MarketsGroupActivity.class);
		}

		if (null != intent) {
			startActivityForResult(intent, requestCode);
		}
	}

	public void updateItem(int itemPos, String itemName, UpdateInfo update) {
		if (chartSlideListener.isDragStarted()) {
			Log.v("chart", "dragging... ignore update");
			return; // ignore updates after dragging started because they cause
					// repaint that cause flicker of the chart on its original
					// position
		}
		super.updateItem(itemPos, itemName, update);
		if (update.isValueChanged("ET_STATE")) {
			if (null != update.getOldValue("ET_STATE")) {
				int oppState = Integer.parseInt(update.getNewValue("ET_STATE"));
				if (oppState == Opportunity.STATE_CLOSING) {
					// If the oldValue is not null that means we were on the
					// page before the state changed - this should save us from
					// infinite market requests.
					// If the new state is waiting for expiry the 1 min of
					// waiting for expiry just expired so we should check for
					// new market
					chartRequest();
				}
			}
		}
	}

	class HPChartSlideListener extends ChartSlideListener {
		public HPChartSlideListener(Activity activity, HomePageChartView chart,
				ViewFlipper viewFlipper, PageControl pageControl) {
			super(activity, chart, viewFlipper, pageControl);
		}

		public void getChartData() {
			chartRequest();
		}

		public void openAssetPage(long marketId) {
			// if (marketId > 0) {
			// Market m = new Market();
			// m.setId(marketId);
			// View v = (HomePageChartView) chart;
			// v.setTag(m);
			// openPage(v);
			// } else {
			openPageInternal(R.id.chart);
			// }
		}
	}

	public boolean onPrepareOptionsMenu(Menu menu) {
		if (isFirstTime) {
			menu.getItem(3).setVisible(false);
		}
		return true;
	}

	@Override
	public int getInfoPageId() {
		return R.layout.info_page;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		loginHelper.handleTermsActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}

}