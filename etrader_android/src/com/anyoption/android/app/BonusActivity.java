/**
 * 
 */
package com.anyoption.android.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.BonusUsers;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.BonusMethodRequest;
import com.anyoption.json.results.MethodResult;

/**
 * @author AviadH
 *
 */
public class BonusActivity extends BaseActivity {
	
	private BonusUsers bonus;
	private WebView webView;
	private Activity activity;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bonus_details);
        activity = this;
        bonus = (BonusUsers)getIntent().getSerializableExtra("BonusUsers");
        webView = (WebView) findViewById(R.id.webViewBonusTerms);
		webView.getSettings().setJavaScriptEnabled(true);
	    webView.getSettings().setPluginsEnabled(true);
        setTextInformation();
    }
	 
	 public void setTextInformation() {
		 getBonusDescription();
		 ((TextView) findViewById(R.id.textViewBonusId)).setText(Long.toString(bonus.getId()));
		 ((TextView) findViewById(R.id.textViewEndDate)).setText(bonus.getEndDateTxt());
		 ((TextView) findViewById(R.id.textViewBonusStatus)).setText(bonus.getBonusStateTxt());
		 ((TextView) findViewById(R.id.textViewBonusDescription)).setText(bonus.getBonusDescriptionTxt());
		 if (bonus.getBonusStateId() == Constants.BONUS_STATE_GRANTED ||
				 bonus.getBonusStateId() == Constants.BONUS_STATE_ACTIVE) {
			 findViewById(R.id.rowWaive).setVisibility(View.VISIBLE);
			 TextView waive = (TextView) findViewById(R.id.textViewBonusWaiveClick);
			 waive.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.on_click_bg));
			 waive.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					cancelBonus(v);
				}
			});
		 }
	 }
	 
	 public void cancelBonus(View target) {
		 LinearLayout ll = (LinearLayout) getLayoutInflater().inflate(R.layout.reverse_withdrawal_pop_up, null);
		 //change text values
		 ll.findViewById(R.id.textViewReverseHeader).setVisibility(View.GONE);
		 ll.findViewById(R.id.textViewReverseReadMore).setVisibility(View.GONE);
		 ((TextView) ll.findViewById(R.id.textViewReverseAmount)).setText(getString(R.string.bonus_waive_popup));
		 final Button no = (Button) ll.findViewById(R.id.buttonCancel);
		 Button yes = (Button) ll.findViewById(R.id.buttonSubmit);
		 no.setText(getString(R.string.bonus_waive_popup_no));
		 yes.setText(getString(R.string.bonus_waive_popup_yes));
		 yes.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				no.performClick();
				BonusMethodRequest request = new BonusMethodRequest();
				request.setBonusId(bonus.getId());
				request.setBonusStateId(Constants.BONUS_STATE_CANCELED);
				Utils.requestService(activity, responseH, "updateBonusCallBack", request, new MethodResult(), "updateBonusStatus");
			}
		});
		 Utils.showCustomAlert(this, ll, no);
	 }
	 
	 public void updateBonusCallBack(Object result) {
		 MethodResult methodResult = (MethodResult) result;
		if (methodResult.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
			Utils.showAlert(this, methodResult.getUserMessages()[0].getField(), 
					methodResult.getUserMessages()[0].getMessage(), getString(R.string.alert_title));
			return;
		}
		Intent data = new Intent();
		data.putExtra(Constants.RESULT_BONUS, Constants.RESULT_BONUS);//sign for refresh in the activity
		setResult(Activity.RESULT_OK, data);
		finish();
	 }
	 
	 public void getBonusDescription() {
		 AnyoptionApplication ap = (AnyoptionApplication) getApplication();
		 String web_site_link = Utils.getStringFromResource(this, Constants.WEB_SITE_LINK, null);
		 String locale =  null;
		 if (ap.getLocale().equals("he")){
			 locale = "iw";
		 } else {
			 locale = ap.getLocale();
		 }
		 String actionURL;
		 actionURL = web_site_link + "termsBonus" + bonus.getTypeId() + ".jsf?s=" + ap.getSkinId() + "&fromApp=true";
		 String text = " <html> " +
				" 			<head> " +
				" 				<meta HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html;charset=UTF-8\" /> " +
				" 				<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\" /> " +
				" 			</head> " +
				" 			<body onload=\"document.termsForm.submit();\" dir=\"ltr\"> " +
						 		" <form method=\"POST\" style=\"display:none;\" id=\"termsForm\" name=\"termsForm\" action=\"" + actionURL +"\"> " +
								"	<input name=\"bonusAmount\" id=\"bonusAmount\" value=\"{bonusAmount}\"/> " +
								"	<input name=\"numOfActions\" id=\"numOfActions\"  value=\"{numOfActions}\"/> " +
								"	<input name=\"startDate\" id=\"startDate\" value=\"{startDate}\"/> " +
								"	<input name=\"endDate\" id=\"endDate\" value=\"{endDate}\"/> " +
								"	<input name=\"minDeposit\" id=\"minDeposit\" value=\"{minDeposit}\"/> " +
								"	<input name=\"maxDeposit\" id=\"maxDeposit\" value=\"{maxDeposit}\"/> " +
								"	<input name=\"wageringParam\" id=\"wageringParam\" value=\"{wageringParam}\"/> " +
								"	<input name=\"bonusPercent\" id=\"bonusPercent\" value=\"{bonusPercent}\"/> " +
								"	<input name=\"sumInvQualify\" id=\"sumInvQualify\" value=\"{sumInvQualify}\"/> " +
								"	<input name=\"bonusStateId\" id=\"bonusStateId\" value=\"{bonusStateId}\"/> " +
								"	<input name=\"bonusExpired\" id=\"bonusExpired\" value=\"{bonusExpired}\"/> " +
								"	<input name=\"hasSteps\" id=\"hasSteps\" value=\"{hasSteps}\"/> " +
								"	<input name=\"bonusSteps\" id=\"bonusSteps\" value=\"{bonusSteps}\"/> " +
								"	<input name=\"symbol\" id=\"symbol\" value=\"{symbol}\"/> " +
								"	<input name=\"bonusAmountNoFormat\" id=\"bonusAmountNoFormat\" value=\"{bonusAmountNoFormat}\"/> " +
								"	<input name=\"minInvestAmount\" id=\"minInvestAmount\" value=\"{minInvestAmount}\"/> " +
								"	<input name=\"maxInvestAmount\" id=\"maxInvestAmount\" value=\"{maxInvestAmount}\"/> " +
								"	<input name=\"bonusId\" id=\"bonusId\" value=\"{bonusId}\"/> " +
								"	<input name=\"sumInvQualifyNoFormat\" id=\"sumInvQualifyNoFormat\" value=\"{sumInvQualifyNoFormat}\"/> " +
								" </form> " +
				" 			</body> " +
				" 		</html>";

            // Convert the buffer into a string.
            User user = ap.getUser();
            text = text.replace("{bonusPercent}", String.valueOf(bonus.getBonusPercent()*100));
            text = text.replace("{minDeposit}", Utils.formatCurrencyAmount(bonus.getMinDepositAmount(), user.getCurrencySymbol(), user.isCurrencyLeftSymbol(), user.getCurrencyId()));
           
            String bonusAmount;
            if(Utils.isEtraderProject(this)){
            	bonusAmount = String.valueOf(bonus.getBonusAmount());
            } else {
            	bonusAmount = Utils.formatCurrencyAmount(bonus.getBonusAmount(), user.getCurrencySymbol(), user.isCurrencyLeftSymbol(), user.getCurrencyId());
            }
            
            text = text.replace("{bonusAmount}",bonusAmount);
            text = text.replace("{endDate}", String.valueOf(bonus.getEndDateTxt()));
            text = text.replace("{numOfActions}", String.valueOf(bonus.getNumOfActions()));
            text = text.replace("{startDate}", String.valueOf(bonus.getStartDateTxt()));
            text = text.replace("{maxDeposit}", String.valueOf(bonus.getMaxDepositAmount()));
            text = text.replace("{wageringParam}", String.valueOf(bonus.getWageringParam()));
            text = text.replace("{sumInvQualify}", Utils.formatCurrencyAmount(bonus.getSumInvQualify(), user.getCurrencySymbol(), user.isCurrencyLeftSymbol(), user.getCurrencyId()));
            text = text.replace("{bonusStateId}", String.valueOf(bonus.getBonusStateId()));
            text = text.replace("{bonusExpired}", String.valueOf(bonus.isBonusExpired()));
            text = text.replace("{hasSteps}", String.valueOf(bonus.isHasSteps()));
            text = text.replace("{bonusSteps}", String.valueOf(bonus.getBonusStepsForHTML()));
            text = text.replace("{symbol}", user.getCurrencySymbol());
            text = text.replace("{bonusAmountNoFormat}", String.valueOf(bonus.getBonusAmount()));
            text = text.replace("{minInvestAmount}", Utils.formatCurrencyAmount(bonus.getMinInvestAmount(), user.getCurrencySymbol(), user.isCurrencyLeftSymbol(), user.getCurrencyId()));
            text = text.replace("{maxInvestAmount}", Utils.formatCurrencyAmount(bonus.getMaxInvestAmount(), user.getCurrencySymbol(), user.isCurrencyLeftSymbol(), user.getCurrencyId()));
            text = text.replace("{bonusId}", String.valueOf(bonus.getBonusId()));
            text = text.replace("{sumInvQualifyNoFormat}", String.valueOf(bonus.getSumInvQualify()));
            
            webView.loadData(text , "text/html", "utf-8");
	 }
}
