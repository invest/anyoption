package com.anyoption.android.app.questionnaire;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.TextView;
import android.view.View;
import com.anyoption.android.app.MenuViewBase;
import com.anyoption.android.app.R;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;

public class QuestionnaireMenuItemView extends MenuViewBase {

	private TextView groupName;
	private Context mContext;

	public QuestionnaireMenuItemView(Context context) {
		this(context, null);
		// TODO Auto-generated constructor stub
	}

	public QuestionnaireMenuItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		buttonMenuStrip.setBackgroundResource(R.drawable.bg_option_row_press);
		groupName = (TextView) findViewById(R.id.textViewGroupName);
		groupName.setTextColor(Color.WHITE);
		groupName.setText(getResources()
				.getString(R.string.questionnaire_title));
		buttonMenuStrip.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent intent = null;
				if (Utils.isSuspended(mContext)) {
					handleSuspension();
				} else if (!QuestionnaireUtils
						.isMandQuestionnaireAnswered(mContext)) {
					intent = new Intent(mContext,
							MandatoryQuestionnaireActivity.class);
				} else {
					intent = new Intent(mContext,
							OptionalQuestionnaireActivity.class);
				}

				if (intent != null) {
					intent.putExtra(Constants.ACTIVITY_BEFORE_QUESTIONNAIRE,
							Constants.FROM_MYACCOUNT);
					mContext.startActivity(intent);
				}
			}

			protected void handleSuspension() {
				if (Utils.isSuspendedForDocuments(mContext)) {
					Utils.showSuspendedForDocumentsPopoup(mContext);
				} else {
					Utils.showGeneralSuspendedPopup(mContext);
				}
			}
		});
	}

	@Override
	public void removeAllRows() {
		// TODO Auto-generated method stub

	}

}
