package com.anyoption.android.app.questionnaire;

import com.anyoption.android.app.R;
import com.anyoption.common.beans.base.QuestionnaireAnswer;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

/**
 * @author AviadH
 */
public class YesNoQuestionView extends BaseQuestionView {

	private RadioButton radioYes;
	private RadioButton radioNo;
	private RadioGroup radioGroup;

	public YesNoQuestionView(Context context) {
		this(context, null);
	}

	public YesNoQuestionView(final Context context, AttributeSet attrs) {
		super(context, attrs, 0);

		radioYes = (RadioButton) getmRootLayout().findViewById(R.id.radioYes);
		radioNo = (RadioButton) getmRootLayout().findViewById(R.id.radioNo);

		radioGroup = (RadioGroup) getmRootLayout().findViewById(
				R.id.yesNoRadioGroup);
		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(RadioGroup group, int checkedId) {
				QuestionnaireUserAnswer currentAnswer = getAnswer();
				QuestionnaireAnswer newAnswer;
				if (checkedId == radioYes.getId()) {
					newAnswer = getQuestion().getAnswers().get(0);

				} else {
					newAnswer = getQuestion().getAnswers().get(1);
				}

				if (isAnswerChanged(currentAnswer.getAnswerId(),
						newAnswer.getId())) {
					getAnswer().setAnswerId(newAnswer.getId());
					getOnAnswerChangedListener().onAnswerChanged();
				}
			}
		});

	}

	public void refreshChecked() {
		if (getAnswer().getAnswerId() == null) {
			// question not answered
			return;
		}

		int answerIndex = 0;
		for (QuestionnaireAnswer answ : getQuestion().getAnswers()) {
			if (answ.getId() == getAnswer().getAnswerId()) {
				answerIndex = getQuestion().getAnswers().indexOf(answ);
			}
		}

		if (answerIndex == 0) {
			radioYes.setChecked(true);
			radioNo.setChecked(false);
		} else {
			radioYes.setChecked(false);
			radioNo.setChecked(true);
		}
	}

	public QuestionnaireUserAnswer getCurrentAnswer() {
		QuestionnaireUserAnswer oldAnswer = getOldAnswer();
		if (radioYes.isChecked()) {
			oldAnswer.setAnswerId(Long.valueOf(1));
		} else if (radioNo.isChecked()) {
			oldAnswer.setAnswerId(Long.valueOf(2));
		} else {
			// not answered
			oldAnswer.setAnswerId(null);
		}
		return oldAnswer;
	}

	/**
	 * Sets the current answer by checking the radio button
	 * 
	 * @param yes
	 *            true sets YES button, false sets NO button
	 * 
	 * */
	public void setAnswer(boolean yes) {
		if (yes) {
			radioYes.setChecked(true);
		} else {
			radioNo.setChecked(true);
		}
	}

	@Override
	protected int getLayoutId() {
		return R.layout.questionnaire_yes_no_question;
	}

	@Override
	public boolean isAnswered() {
		return (radioYes.isChecked() || radioNo.isChecked());
	}
}