package com.anyoption.android.app.questionnaire;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnDismissListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.R;
import com.anyoption.android.app.util.Utils;

public class QuestionnaireAlertBuilder {
	public enum ButtonsOrientation {
		HORIZONTAL, VERTICAL
	};

	private Context context;
	private ButtonsOrientation orientation = ButtonsOrientation.HORIZONTAL;
	private CharSequence text = "";
	private OnClickListener yellowButtonListener;
	private OnClickListener greyButtonListener;
	private OnDismissListener onDismissListener;
	private Integer openTradePageDelay;
	private String greyButtonText = "";
	private String yellowButtonText = "";
	private int yellowButtonVisibility = View.VISIBLE;
	private int greyButtonVisibility = View.VISIBLE;

	public QuestionnaireAlertBuilder(Context context) {
		this.context = context;
	}

	public AlertDialog build() {
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		ViewGroup popup = (ViewGroup) inflater.inflate(
				R.layout.questionnaire_pop_up, null);
		ViewGroup buttonsContainer = (ViewGroup) popup
				.findViewById(R.id.buttonsContainer);

		switch (orientation) {
		case HORIZONTAL:
			inflater.inflate(
					R.layout.questionnaire_buttons_horizontal, buttonsContainer);
			break;

		case VERTICAL:
			inflater.inflate(
					R.layout.questionnaire_buttons_vertical, buttonsContainer);
			break;
		default:
			inflater.inflate(
					R.layout.questionnaire_buttons_horizontal, buttonsContainer);
			break;
		}

		final AlertDialog dialog = Utils.showCustomAlert(context, popup, null);
		TextView message = (TextView) dialog.findViewById(R.id.textView);

		dialog.getWindow().setLayout(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);

		Button buttonYellow = (Button) dialog.findViewById(R.id.buttonYellow);
		Button buttonGrey = (Button) dialog.findViewById(R.id.buttonGrey);

		message.setText(text);
		buttonGrey.setText(greyButtonText);
		buttonYellow.setText(yellowButtonText);

		if (yellowButtonListener != null) {
			buttonYellow.setOnClickListener(yellowButtonListener);
		}

		if (greyButtonListener != null) {
			buttonGrey.setOnClickListener(greyButtonListener);
		}

		if (onDismissListener != null) {
			dialog.setOnDismissListener(onDismissListener);
		}// TODO implement VVV
			// startOpenTradingPageTimer(context, dialog, 7);
		if (openTradePageDelay != null) {
			startOpenTradingPageTimer(context, dialog, openTradePageDelay);
		}

		buttonGrey.setVisibility(greyButtonVisibility);
		buttonYellow.setVisibility(yellowButtonVisibility);

		return dialog;
	}

	public QuestionnaireAlertBuilder setYellowButtonListener(
			OnClickListener yellowButtonListener) {
		this.yellowButtonListener = yellowButtonListener;
		return this;
	}

	public QuestionnaireAlertBuilder setGreyButtonListener(
			OnClickListener greyButtonListener) {
		this.greyButtonListener = greyButtonListener;
		return this;
	}

	public QuestionnaireAlertBuilder setOnDismissListener(
			OnDismissListener onDismissListener) {
		this.onDismissListener = onDismissListener;
		return this;
	}

	public QuestionnaireAlertBuilder setOrientation(
			ButtonsOrientation orientation) {
		this.orientation = orientation;
		return this;
	}

	public QuestionnaireAlertBuilder setText(CharSequence text) {
		this.text = text;
		return this;
	}

	public QuestionnaireAlertBuilder setOpenTradingPageTimer(int seconds) {
		this.openTradePageDelay = seconds;
		return this;
	}

	public QuestionnaireAlertBuilder setGreyButtonText(String text) {
		this.greyButtonText = text;
		return this;
	}

	public QuestionnaireAlertBuilder setYellowButtonText(String text) {
		this.yellowButtonText = text;
		return this;
	}

	private static void startOpenTradingPageTimer(final Context context,
			final AlertDialog dialog, int secondsDelay) {
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				if (dialog.isShowing()) {
					Utils.openTradingPage(context);
				}
			}
		}, secondsDelay * 1000);
	}

	public QuestionnaireAlertBuilder hideGreyButton() {
		greyButtonVisibility = View.GONE;
		return this;
	}
	
	public QuestionnaireAlertBuilder hideYellowButton(){
		yellowButtonVisibility = View.GONE;
		return this;
	}
}
