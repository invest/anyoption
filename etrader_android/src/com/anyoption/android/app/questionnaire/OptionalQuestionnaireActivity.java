package com.anyoption.android.app.questionnaire;

import java.util.Arrays;
import java.util.List;

import com.anyoption.android.app.AnyoptionApplication;
import com.anyoption.android.app.R;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

public class OptionalQuestionnaireActivity extends BaseQuestionnaireActivity {

	public static final String GET_USER_ANSWERS_METHOD = "getUserOptionalQuestionnaire";
	public static final String SUBMIT_REQUEST_METHOD = "updateOptionalQuestionnaireDone";
	private BaseQuestionView spinnerQuestionView3;
	private SpinnerQuestionView spinnerQuestionView2;
	private SpinnerQuestionView spinnerQuestionView1;
	private SpinnerQuestionView spinnerQuestionView4;
	private SpinnerQuestionView spinnerQuestionView6;
	private SpinnerQuestionView spinnerQuestionView10;
	private SpinnerQuestionView spinnerQuestionView9;
	private SpinnerQuestionView spinnerQuestionView8;
	private SpinnerQuestionView spinnerQuestionView7;
	private MultipleChoiceAnswerView multichoiceQuestionView;
	private SpinnerQuestionView spinnerQuestionView5;
	private Button btnNotNow;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		btnNotNow = (Button) findViewById(R.id.buttonNotNow);
		btnNotNow.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				QuestionnaireUtils.showRiskPopup(mContext);
			}
		});

		setOnKeyboardDismissListener();
	}

	@Override
	protected void onResume() {
		super.onResume();
		AnyoptionApplication ap = (AnyoptionApplication) getApplication();
		if (ap.getUserRegulation().isOptionalQuestionnaireDone()) {
			freezeQuestionnaire();
			showThankYouPopup();
		}
	}

	@Override
	protected List<BaseQuestionView> initQuestionViews() {
		spinnerQuestionView1 = (SpinnerQuestionView) findViewById(R.id.questionView1);
		spinnerQuestionView2 = (SpinnerQuestionView) findViewById(R.id.questionView2);
		spinnerQuestionView3 = (SpinnerQuestionView) findViewById(R.id.questionView3);
		spinnerQuestionView4 = (SpinnerQuestionView) findViewById(R.id.questionView4);
		spinnerQuestionView5 = (SpinnerQuestionView) findViewById(R.id.questionView5);

		multichoiceQuestionView = (MultipleChoiceAnswerView) findViewById(R.id.questionView6);

		spinnerQuestionView6 = (SpinnerQuestionView) findViewById(R.id.questionView7);
		spinnerQuestionView7 = (SpinnerQuestionView) findViewById(R.id.questionView8);
		spinnerQuestionView8 = (SpinnerQuestionView) findViewById(R.id.questionView9);
		spinnerQuestionView9 = (SpinnerQuestionView) findViewById(R.id.questionView10);
		spinnerQuestionView10 = (SpinnerQuestionView) findViewById(R.id.questionView11);

		return Arrays.asList(spinnerQuestionView1, spinnerQuestionView2,
				spinnerQuestionView3, spinnerQuestionView4,
				spinnerQuestionView5, multichoiceQuestionView,
				spinnerQuestionView6, spinnerQuestionView7,
				spinnerQuestionView8, spinnerQuestionView9,
				spinnerQuestionView10);
	}

	@Override
	protected int getLayoutId() {
		return R.layout.questionnaire_optional;
	}

	@Override
	protected void showThankYouPopup() {
		QuestionnaireUtils.showOptionalThankYouPopup(mContext);
	}

	@Override
	protected String getSubmitRequestMethod() {
		return SUBMIT_REQUEST_METHOD;
	}

	@Override
	protected String getCurrentUserAnswersMethod() {
		// TODO Auto-generated method stub
		return GET_USER_ANSWERS_METHOD;
	}

	protected void setOnKeyboardDismissListener() {
		final View activityRootView = findViewById(R.id.rootLayout);
		activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(
				new OnGlobalLayoutListener() {
					public void onGlobalLayout() {
						int heightDiff = activityRootView.getRootView()
								.getHeight() - activityRootView.getHeight();
						if (heightDiff > 100) { // if more than 100 pixels, its
												// probably a keyboard...

						} else {
							if (multichoiceQuestionView
									.getOnAnswerChangedListener() != null) {
								multichoiceQuestionView
										.getOnAnswerChangedListener()
										.onAnswerChanged();
							}
						}
					}
				});
	}
}
