package com.anyoption.android.app.questionnaire;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.anyoption.android.app.R;
import com.anyoption.common.beans.base.QuestionnaireAnswer;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;

public class MultipleChoiceAnswerView extends BaseQuestionView {

	private static final int EMPLOYMENT = 0;
	private static final int INVESTMENT = 1;
	private static final int INHERITANCE = 2;
	private static final int OTHER = 3;
	private CheckBox checkBoxEmployment;
	private CheckBox checkBoxInvestment;
	private CheckBox checkBoxInheritance;
	private CheckBox checkBoxOther;
	private EditText editTextOther;
	private List<QuestionnaireUserAnswer> answers = new ArrayList<QuestionnaireUserAnswer>();
	private List<QuestionnaireQuestion> questions = new ArrayList<QuestionnaireQuestion>();
	private List<CheckBox> checkboxes;

	public MultipleChoiceAnswerView(Context context, AttributeSet attrs) {
		super(context, attrs, 0);
		checkBoxEmployment = (CheckBox) findViewById(R.id.checkBoxEmployment);
		checkBoxInvestment = (CheckBox) findViewById(R.id.checkBoxInvestment);
		checkBoxInheritance = (CheckBox) findViewById(R.id.checkBoxInheritance);
		checkBoxOther = (CheckBox) findViewById(R.id.checkBoxOther);

		checkboxes = Arrays.asList(checkBoxEmployment, checkBoxInvestment,
				checkBoxInheritance, checkBoxOther);
		setCheckBoxesListeners();

		editTextOther = (EditText) findViewById(R.id.editTextOther);
		setOtherTextListeners(editTextOther);
	}

	@Override
	protected int getLayoutId() {
		// TODO Auto-generated method stub
		return R.layout.questionnaire_multichoice_question;
	}

	@Override
	public void refreshChecked() {
		for (int i = EMPLOYMENT; i <= OTHER; ++i) {
			QuestionnaireUserAnswer answer2 = answers.get(i);
			for (QuestionnaireAnswer answ : questions.get(0).getAnswers()) {
				if (answer2.getAnswerId() == null) {
					break;
				}
				if (answ.getId() == answer2.getAnswerId()) {
					switch (i) {
					case EMPLOYMENT:
						checkBoxEmployment.setChecked(true);
						break;
					case INVESTMENT:
						checkBoxInvestment.setChecked(true);
						break;
					case INHERITANCE:
						checkBoxInheritance.setChecked(true);
						break;
					case OTHER:
						checkBoxOther.setChecked(true);
						if (answer2.getTextAnswer() != null) {
							editTextOther.setText(answer2.getTextAnswer());
						}
						break;
					}
				}
			}

		}

	}

	@Override
	public boolean isAnswered() {
		return (checkBoxEmployment.isChecked()
				|| checkBoxInheritance.isChecked()
				|| checkBoxInvestment.isChecked() || checkBoxOther.isChecked());
	}

	public void addAnswer(QuestionnaireUserAnswer answer) {
		answers.add(answer);
	}

	public void addQuestion(QuestionnaireQuestion question) {
		questions.add(question);
	}

	private final class OnCheckedChangeListenerImplementation implements
			OnCheckedChangeListener {
		private int position;

		public OnCheckedChangeListenerImplementation(int position) {
			this.position = position;
		}

		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			QuestionnaireUserAnswer currentAnswer = answers.get(position);
			Long newId;

			if (isChecked) {
				newId = questions.get(position).getAnswers().get(position)
						.getId();
			} else {
				newId = null;

			}

			if (isAnswerChanged(currentAnswer.getAnswerId(), newId)) {
				if (position == 3) {
					if (isChecked) {

					} else {
						editTextOther.setText(null);
						currentAnswer.setTextAnswer(null);
					}
				}
				currentAnswer.setAnswerId(newId);
				getOnAnswerChangedListener().onAnswerChanged();
			}
		}
	}

	private void updateOtherText() {
		answers.get(OTHER).setTextAnswer(editTextOther.getText().toString());
		if (getOnAnswerChangedListener() != null) {
			getOnAnswerChangedListener().onAnswerChanged();
		}
	}

	private void setOtherTextListeners(EditText text) {
		text.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (count > 0) {
					checkBoxOther.setChecked(true);
				}
				answers.get(OTHER).setTextAnswer(s.toString());
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		// Updating user answers when done editing the text
		text.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					updateOtherText();
					return false;
				}
				return false;
			}

		});
		text.setOnFocusChangeListener(new OnFocusChangeListener() {

			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					updateOtherText();
				}

			}
		});
	}

	private void setCheckBoxesListeners() {
		for (int i = 0; i < checkboxes.size(); i++) {
			checkboxes.get(i).setOnCheckedChangeListener(
					new OnCheckedChangeListenerImplementation(i));
		}
	}
}

