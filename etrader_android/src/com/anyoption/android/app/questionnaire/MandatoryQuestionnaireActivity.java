package com.anyoption.android.app.questionnaire;

import java.util.Arrays;
import java.util.List;

import com.anyoption.android.app.R;
import com.anyoption.android.app.questionnaire.BaseQuestionView;
import com.anyoption.android.app.questionnaire.BaseQuestionnaireActivity;
import com.anyoption.android.app.questionnaire.YesNoQuestionView;

import android.os.Bundle;
import android.view.View;

public class MandatoryQuestionnaireActivity extends BaseQuestionnaireActivity {
	public static final String GET_USER_ANSWERS_METHOD = "getUserMandatoryQuestionnaire";
	public static final String SUBMIT_REQUEST_METHOD = "updateMandatoryQuestionnaireDone";
	private BaseQuestionView questionView1;
	private BaseQuestionView questionView2;
	private BaseQuestionView questionView3;
	private BaseQuestionView questionView4;
	private BaseQuestionView questionView5;
	private BaseQuestionView questionView6;
	private BaseQuestionView questionView7;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	protected int getLayoutId() {
		return R.layout.questionnaire_mandatory;
	}

	@Override
	protected List<BaseQuestionView> initQuestionViews() {
		questionView1 = (BaseQuestionView) findViewById(R.id.questionView1);
		questionView2 = (BaseQuestionView) findViewById(R.id.questionView2);
		questionView3 = (BaseQuestionView) findViewById(R.id.questionView3);
		questionView4 = (BaseQuestionView) findViewById(R.id.questionView4);
		questionView5 = (BaseQuestionView) findViewById(R.id.questionView5);
		questionView5.setVisibility(View.GONE);
		questionView6 = (BaseQuestionView) findViewById(R.id.questionView6);
		questionView7 = (BaseQuestionView) findViewById(R.id.questionView7);

		return Arrays.asList(questionView1, questionView2, questionView3,
				questionView4, questionView5, questionView6, questionView7);
	}

	@Override
	protected void showThankYouPopup() {
		QuestionnaireUtils.showMandatoryThankYouPopup(mContext);
	}

	@Override
	protected String getSubmitRequestMethod() {
		return SUBMIT_REQUEST_METHOD;
	}

	@Override
	protected String getCurrentUserAnswersMethod() {
		// TODO Auto-generated method stub
		return GET_USER_ANSWERS_METHOD;
	}

	@Override
	public void updateUserAnswersCallback(Object resultObj) {
		// TODO Auto-generated method stub
		super.updateUserAnswersCallback(resultObj);
		((YesNoQuestionView) questionView5).setAnswer(true);
	}
}
