package com.anyoption.android.app.questionnaire;

import com.anyoption.android.app.R;
import com.anyoption.common.beans.base.QuestionnaireAnswer;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class CheckboxQuestionView extends BaseQuestionView {

	private CheckBox checkbox;

	public CheckboxQuestionView(Context context, AttributeSet attrs) {
		super(context, attrs, 0);
		checkbox = (CheckBox) findViewById(R.id.questionText);
		checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				QuestionnaireAnswer newAnswer;
				if(isChecked){
					newAnswer = getQuestion().getAnswers().get(0);
				} else {
					newAnswer = getQuestion().getAnswers().get(1);
				}
				
				QuestionnaireUserAnswer currentAnswer = getAnswer();
				if (isAnswerChanged(currentAnswer .getAnswerId(),
						newAnswer.getId())) {
					getAnswer().setAnswerId(newAnswer.getId());
					getOnAnswerChangedListener().onAnswerChanged();
				}
			}
		});
	}

	@Override
	protected int getLayoutId() {
		return R.layout.questionnaire_checkbox_question;
	}

	@Override
	protected void refreshChecked() {
		if (getAnswer().getAnswerId() == null) {
			// question not answered - make it checked by default
			checkbox.setChecked(true);
			return;
		}

		int answerIndex = 0;
		for (QuestionnaireAnswer answ : getQuestion().getAnswers()) {
			if (answ.getId() == getAnswer().getAnswerId()) {
				answerIndex = getQuestion().getAnswers().indexOf(answ);
			}
		}

		if(answerIndex == 0){
			checkbox.setChecked(true);
		} else {
			checkbox.setChecked(false);
		}
	}

	@Override
	public boolean isAnswered() {
		// This type of question is always considered answered
		return true;
	}

}
