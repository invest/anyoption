package com.anyoption.android.app.questionnaire;

import com.anyoption.android.app.R;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * @author AviadH
 */
public abstract class BaseQuestionView extends LinearLayout {

	protected Context mContext;
	private OnAnswerChangedListener listener;
	private LayoutInflater inflater;
	private ViewGroup mRootLayout;
	private TextView questionTextView;
	private QuestionnaireQuestion question;
	private QuestionnaireUserAnswer answer;

	public BaseQuestionView(Context context) {
		this(context, null, 0);
	}

	public BaseQuestionView(final Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs);
		mContext = context;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mRootLayout = (ViewGroup) inflater.inflate(getLayoutId(), this);

		questionTextView = (TextView) getmRootLayout().findViewById(
				R.id.questionText);

		// getting the question text from the xml attribute "text"
		String text = getTextAttribute(context, attrs, defStyle);
		questionTextView.setText(Html.fromHtml(text));

	}

	protected String getTextAttribute(final Context context,
			AttributeSet attrs, int defStyle) {
		TypedArray typedArray = context.getTheme().obtainStyledAttributes(
				attrs, R.styleable.questionView, defStyle, 0);
		String text;
		try {
			text = typedArray.getString(R.styleable.questionView_android_text);
		} finally {
			typedArray.recycle();
		}
		return text;
	}

	protected abstract int getLayoutId();

	protected abstract void refreshChecked();

	protected QuestionnaireUserAnswer getOldAnswer() {
		return new QuestionnaireUserAnswer();
	}

	public abstract boolean isAnswered();

	public ViewGroup getmRootLayout() {
		return mRootLayout;
	}

	public QuestionnaireQuestion getQuestion() {
		return question;
	}

	public void setQuestion(QuestionnaireQuestion question) {
		this.question = question;
	}

	public QuestionnaireUserAnswer getAnswer() {
		return answer;
	}

	public void setAnswer(QuestionnaireUserAnswer answer) {
		this.answer = answer;
	}

	protected OnAnswerChangedListener getOnAnswerChangedListener() {
		return listener;
	}

	public void setOnAnswerChangedListener(OnAnswerChangedListener listener) {
		this.listener = listener;
	}

	protected boolean isAnswerChanged(Long oldId, Long newId) {
		if (oldId == null && newId == null) {
			return false;
		} else if ((oldId == null && newId != null)
				|| (oldId != null && newId == null)) {
			return true;
		} else {
			return !oldId.equals(newId);
		}
	}
}