package com.anyoption.android.app.questionnaire;

import java.util.Locale;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.View.OnClickListener;
import com.anyoption.android.app.AnyoptionApplication;
import com.anyoption.android.app.BankingActivity;
import com.anyoption.android.app.MyAccountActivity;
import com.anyoption.android.app.R;
import com.anyoption.android.app.questionnaire.QuestionnaireAlertBuilder.ButtonsOrientation;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.UserRegulationBase;

public class QuestionnaireUtils {

	public static boolean isMandQuestionnaireAnswered(Context context) {
		AnyoptionApplication app = (AnyoptionApplication) context
				.getApplicationContext();
		int regulationStep = app.getUserRegulation()
				.getApprovedRegulationStep();
		return regulationStep >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE;
	}

	public static boolean isOptQuestionnaireAnswered(Context context) {
		AnyoptionApplication app = (AnyoptionApplication) context
				.getApplicationContext();
		return app.getUserRegulation().isOptionalQuestionnaireDone();
	}

	public static AlertDialog showOptionalThankYouPopup(final Context context) {
		final DialogHelper helper = new DialogHelper();
		Spanned text = Html.fromHtml(context
				.getString(R.string.questionnaire_optional_thankyou));
		OnClickListener buttonGreyListener = new OnClickListener() {

			public void onClick(View v) {
				helper.getDialog().dismiss();
				Utils.openTradingPage(context);
			}
		};

		String greyButtonText = context.getString(R.string.alert_button);
		helper.setDialog(new QuestionnaireAlertBuilder(context)
				.setOrientation(ButtonsOrientation.VERTICAL).setText(text)
				.setGreyButtonListener(buttonGreyListener)
				.setGreyButtonText(greyButtonText).hideYellowButton()
				.setOnDismissListener(new OnThankYouDismissListener(context))
				.setOpenTradingPageTimer(7).build());

		return helper.getDialog();
	}

	public static void showRiskPopup(final Context context) {
		String text = context.getString(R.string.questionnaire_risknote);
		String yellowButtonText = context
				.getString(R.string.mobile_button_next).toUpperCase(
						Locale.getDefault());
		OnClickListener yellowButtonListener = new OnClickListener() {

			public void onClick(View v) {
				Utils.openTradingPage(context);
			}
		};

		new QuestionnaireAlertBuilder(context)
				.setOrientation(ButtonsOrientation.VERTICAL).setText(text)
				.setYellowButtonText(yellowButtonText).hideGreyButton()
				.setYellowButtonListener(yellowButtonListener)
				.setOpenTradingPageTimer(5).build();

	}

	public static void showFillQuestionnairePopup(final Context context) {
		final DialogHelper helper = new DialogHelper();

		Spanned text = Html.fromHtml(context
				.getString(R.string.questionnaire_must_fill));
		OnClickListener buttonGreyListener = new OnClickListener() {

			public void onClick(View v) {
				helper.getDialog().dismiss();
			}
		};
		OnClickListener buttonYellowListener = new OnClickListener() {

			public void onClick(View v) {
				Intent intent = new Intent(context,
						MandatoryQuestionnaireActivity.class);
				intent.putExtra(Constants.ACTIVITY_BEFORE_QUESTIONNAIRE,
						Constants.FROM_INVESTING);
				context.startActivity(intent);
				helper.getDialog().dismiss();
			}
		};

		String greyButtonText = context.getString(R.string.alert_button);
		String yellowButtonText = context.getString(R.string.questionnaire_fill);
		helper.setDialog(new QuestionnaireAlertBuilder(context).setText(text)
				.setGreyButtonListener(buttonGreyListener).setGreyButtonText(greyButtonText).setYellowButtonText(yellowButtonText)
				.setYellowButtonListener(buttonYellowListener).build());

	}

	private static class DialogHelper {
		private AlertDialog dialog;

		public AlertDialog getDialog() {
			return dialog;
		}

		public void setDialog(AlertDialog dialog) {
			this.dialog = dialog;
		}
	}

	public static AlertDialog showMandatoryThankYouPopup(final Context context) {
		final DialogHelper helper = new DialogHelper();

		Spanned text = Html.fromHtml(context
				.getString(R.string.questionnaire_mandatory_thankyou));

		OnClickListener yellowButtonListener = new OnClickListener() {

			public void onClick(View v) {
				((BaseQuestionnaireActivity) context)
						.setPopupDismissedFromButton(true);
				helper.getDialog().dismiss();
				Utils.openTradingPage(context);
			}
		};

		OnClickListener greyButtonListener = new OnClickListener() {

			public void onClick(View v) {
				((BaseQuestionnaireActivity) context)
						.setPopupDismissedFromButton(true);
				context.startActivity(new Intent(context,
						OptionalQuestionnaireActivity.class));
				helper.getDialog().dismiss();
				((Activity) context).finish();
			}
		};

		String greyButtonText = context.getString(R.string.questionnaire_fill);
		String yellowButtonText = context
				.getString(R.string.optionPlus_button_not_now);
		helper.setDialog(new QuestionnaireAlertBuilder(context).setText(text)
				.setOrientation(ButtonsOrientation.VERTICAL)
				.setGreyButtonText(greyButtonText)
				.setYellowButtonText(yellowButtonText)
				.setYellowButtonListener(yellowButtonListener)
				.setGreyButtonListener(greyButtonListener)
				.setOnDismissListener(new OnThankYouDismissListener(context))
				.setOpenTradingPageTimer(7).build());
		return helper.getDialog();
	}

	public static void handleBackPressed(Context context) {
		switch (((BaseQuestionnaireActivity) context).getActivityBefore()) {
		case Constants.FROM_DEPOSIT:
			context.startActivity(new Intent(context, BankingActivity.class));
			((Activity) context).finish();
			break;

		case Constants.FROM_LOGIN:
			Utils.openTradingPage(context);
			break;

		case Constants.FROM_INVESTING:
			Utils.openTradingPage(context);
			break;

		case Constants.FROM_MYACCOUNT:
			context.startActivity(new Intent(context, MyAccountActivity.class));
			((Activity) context).finish();
			break;
		default:
			Utils.openTradingPage(context);
			break;
		}

	}

	public static class OnThankYouDismissListener implements OnDismissListener {
		private Context context;

		public OnThankYouDismissListener(Context context) {
			this.context = context;
		}

		public void onDismiss(DialogInterface dialog) {
			if (((BaseQuestionnaireActivity) context)
					.isPopupDismissedFromButton()) {
				// popup dismissed from button - let the button listener
				// handle the redirect
			} else {
				handleBackPressed(context);
			}
		}

	};

}
