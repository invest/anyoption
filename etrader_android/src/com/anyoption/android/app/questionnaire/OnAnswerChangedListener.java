package com.anyoption.android.app.questionnaire;

public interface OnAnswerChangedListener {
	void onAnswerChanged();
}
