package com.anyoption.android.app.questionnaire;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.anyoption.android.app.R;
import com.anyoption.common.beans.base.QuestionnaireAnswer;

public class SpinnerQuestionView extends BaseQuestionView {

	private Spinner spinner;
	private ArrayAdapter<String> spinnerAdapter;

	public SpinnerQuestionView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public SpinnerQuestionView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		spinner = (Spinner) getmRootLayout().findViewById(R.id.answerSpinner);

		List<String> entries = getEntries(context, attrs, defStyle);
		if (entries != null) {
			spinnerAdapter = new ArrayAdapter<String>(mContext,
					android.R.layout.simple_spinner_item, entries);
			spinnerAdapter
					.setDropDownViewResource(R.layout.multiline_spinner_dropdown_item);
			spinner.setAdapter(spinnerAdapter);

			spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					QuestionnaireAnswer newAnswer = getQuestion().getAnswers()
							.get(position);

					if (isAnswerChanged(getAnswer().getAnswerId(), newAnswer.getId())) {
						getAnswer().setAnswerId(newAnswer.getId());
						getOnAnswerChangedListener().onAnswerChanged();
					}
				}

				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});
		}
	}

	private List<String> getEntries(Context context, AttributeSet attrs,
			int defStyle) {
		TypedArray typedArray = context.getTheme().obtainStyledAttributes(
				attrs, R.styleable.questionView, defStyle, 0);
		CharSequence[] mEntries;
		List<String> entriesList = new ArrayList<String>();

		try {
			mEntries = typedArray
					.getTextArray(R.styleable.questionView_android_entries);
			if (mEntries != null) {
				for (CharSequence ch : mEntries) {
					entriesList.add(ch.toString());
				}

			}
		} finally {
			typedArray.recycle();

		}
		return entriesList;
	}

	@Override
	protected int getLayoutId() {
		return R.layout.questionnaire_spinner_question;
	}

	@Override
	public void refreshChecked() {
		if (getAnswer().getAnswerId() == null) {
			// question not answered
			return;
		}

		int answerIndex = 0;
		for (QuestionnaireAnswer answ : getQuestion().getAnswers()) {
			if (answ.getId() == getAnswer().getAnswerId()) {
				answerIndex = getQuestion().getAnswers().indexOf(answ);
			}
		}
		spinner.setSelection(answerIndex);

	}

	@Override
	public boolean isAnswered() {
		return spinner.getSelectedItemPosition() != -1;
	}

}
