package com.anyoption.android.app.questionnaire;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.AnyoptionApplication;
import com.anyoption.android.app.BaseActivity;
import com.anyoption.android.app.R;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.json.requests.UpdateUserQuestionnaireAnswersRequest;
import com.anyoption.json.requests.UserMethodRequest;
import com.anyoption.json.results.UserMethodResult;
import com.anyoption.json.results.UserQuestionnaireResult;

public abstract class BaseQuestionnaireActivity extends BaseActivity {
	protected Context mContext;
	User user;

	protected List<BaseQuestionView> questionViews;
	private int activityBefore;
	private boolean frozen;
	private boolean isPopupDismissedFromButton;
	private List<QuestionnaireQuestion> questions;
	private List<QuestionnaireUserAnswer> answers;
	private LinearLayout questionsLayout;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(getLayoutId());
		
		TextView headerTV = (TextView)findViewById(R.id.TextViewHeader);
		headerTV.setText(getString(R.string.questionnaire_title).toUpperCase());
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			activityBefore = extras
					.getInt(Constants.ACTIVITY_BEFORE_QUESTIONNAIRE);
		}

		questionsLayout = (LinearLayout)findViewById(R.id.questionsLayout);
		// Hiding the questions before they are synced
		questionsLayout.setVisibility(View.GONE);
		
		
		questionViews = initQuestionViews();

		Utils.requestService(this, responseH, "getCurrentUserAnswersCallback",
				new UserMethodRequest(), new UserQuestionnaireResult(),
				getCurrentUserAnswersMethod(), this);
	}

	public void getCurrentUserAnswersCallback(Object resultObj) {
		Log.d(getPackageName(), "getCurrentUserAnswersCallback");
		UserQuestionnaireResult result = (UserQuestionnaireResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			questions = result.getQuestions();
			answers = result.getUserAnswers();
			setOnAnswerChangedListeners();
			restoreAnswers();
			questionsLayout.setVisibility(View.VISIBLE);
		} else {
			Log.d(getPackageName(), "Getting user answers failed");
		}
	}

	protected void setOnAnswerChangedListeners() {
		for (BaseQuestionView v : questionViews) {
			OnAnswerChangedListener listener = new OnAnswerChangedListener() {

				public void onAnswerChanged() {
					UpdateUserQuestionnaireAnswersRequest request = new UpdateUserQuestionnaireAnswersRequest();
					request.setUserAnswers(answers);
					Utils.requestService(mContext, responseH,
							"updateUserAnswersCallback", request,
							new UserQuestionnaireResult(),
							"updateUserQuestionnaireAnswers",
							(Activity) mContext, false);
				}
			};
			v.setOnAnswerChangedListener(listener);
		}
	}

	public void updateUserAnswersCallback(Object resultObj) {
		Log.d(getPackageName(), "updateUserAnswersCallback");
		UserQuestionnaireResult result = (UserQuestionnaireResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			
		} else {
			Log.d(getPackageName(), "Updating user answers failed");
			if(result.getErrorCode() == Constants.ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE){
				QuestionnaireUtils.handleBackPressed(this);
			}
		}
	}

	protected abstract String getCurrentUserAnswersMethod();

	protected abstract List<BaseQuestionView> initQuestionViews();

	protected abstract int getLayoutId();

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// do nothing - stopping the default state saving
	}

	public void onSubmit(View v) {
		if (frozen) {
			return;
		}

		if (allAnswered()) {
			Utils.requestService(this, responseH, "getSubmitResultCallback",
					new UserMethodRequest(), new UserMethodResult(),
					getSubmitRequestMethod(), this);
		} else {
			Utils.showAlert(
					mContext,
					"",
					getString(R.string.questionnaire_error_allquestionsmandatory),
					getString(R.string.alert_title));
		}
	}

	public void getSubmitResultCallback(Object resultObj) {
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			((AnyoptionApplication) getApplication()).setUser(result.getUser());
			((AnyoptionApplication) getApplication()).setUserRegulation(result
					.getUserRegulation());
			if (getSubmitRequestMethod().equals(
					MandatoryQuestionnaireActivity.SUBMIT_REQUEST_METHOD)) {
				if (result.getUserRegulation().getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE) {
					showThankYouPopup();
				} else if(result.getUserRegulation().isSuspended() && !result.getUserRegulation().isSuspendedDueDocuments()){
					boolean toFinishActivity = true;
					Utils.showGeneralSuspendedPopup(this, toFinishActivity);
				}
			} else if (getSubmitRequestMethod().equals(
					OptionalQuestionnaireActivity.SUBMIT_REQUEST_METHOD)) {
				if (result.getUserRegulation().isOptionalQuestionnaireDone()) {
					showThankYouPopup();
				}
			}
		}
	}

	protected abstract String getSubmitRequestMethod();

	protected abstract void showThankYouPopup();

	protected void freezeQuestionnaire() {
		for (BaseQuestionView v : questionViews) {
			v.setClickable(false);
		}
		frozen = true;
	}

	protected boolean allAnswered() {
		for (BaseQuestionView v : questionViews) {
			if (!v.isAnswered()) {
				return false;
			}
		}
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	private void restoreAnswers() {
		// shifting the questions and answers because of the multichoice
		// questions
		for (int i = 0; i < questionViews.size(); ++i) {
			BaseQuestionView view = questionViews.get(i);
			if (view instanceof MultipleChoiceAnswerView) {
				for(QuestionnaireQuestion q : questions){
					if(q.getOrderId() == i+1){
						((MultipleChoiceAnswerView)view).addQuestion(q);
					}
				}
				for(int j=answers.size()-4; j<answers.size(); ++j){
					((MultipleChoiceAnswerView)view).addAnswer(answers.get(j));
				}
				view.refreshChecked();
				
			} else {
				QuestionnaireQuestion question = null;
				for(QuestionnaireQuestion q : questions){
					if(q.getOrderId() == i+1){
						question = q;
					}
				}
				QuestionnaireUserAnswer answer = null;
				for(QuestionnaireUserAnswer a : answers){
					if(a.getQuestionId() == question.getId()){
						answer = a;
					}
				}
				view.setQuestion(question);
				view.setAnswer(answer);
				view.refreshChecked();
			}
		}
	}

	@Override
	public void onBackPressed() {
		QuestionnaireUtils.handleBackPressed(this);
	}

	public int getActivityBefore() {
		return activityBefore;
	}

	public boolean isPopupDismissedFromButton() {
		return isPopupDismissedFromButton;
	}

	public void setPopupDismissedFromButton(boolean isPopupDismissedFromButton) {
		this.isPopupDismissedFromButton = isPopupDismissedFromButton;
	}

}
