package com.anyoption.android.app;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Wire;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.WireMethodRequest;
import com.anyoption.json.results.TransactionMethodResult;

/**
 * @author AviadH
 *
 */
public abstract class BankWireWithdrawCommonActivity extends WithdrawalBase {
	
	private final static String SERIVCE_NAME = "insertWithdrawBank";
	private WireMethodRequest request; 
	protected static final String BANK_DETAILS_ERROR = "error";
	protected static final String BANK_DETAILS_CITY = "bank";
	protected long eTBankId = 0;
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        setContentView(buildLayout(Constants.PAYMENT_TYPE_WITHDRAW));
        responseH = new Handler();
        activity = this;
        PaymentMethod payment = (PaymentMethod) getIntent().getExtras().get(Constants.PUT_EXTRA_BANKING_PAYMENT);
        findViewById(R.id.imageViewInfo).setTag(payment);
        //set currency symbol in amount edittext hint
        AnyoptionApplication ap = (AnyoptionApplication)getApplication();
        User user = ap.getUser();
        ((EditText) findViewById(R.id.editTextAmount)).setHint(getString(R.string.bank_wire_hint_amount, user.getCurrencySymbol()));
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        init();
	}
	
	public abstract void init();
	
	public void sendWithdrawalToService(View target) {
		
		EditText accountNumber = (EditText) findViewById(R.id.editTextAccountNumber);		
		EditText branchNumber = (EditText) findViewById(R.id.editTextBranch);		
		EditText amount = (EditText) findViewById(R.id.editTextAmount);
		
		//Clinet validations
		String error = null;
		
		//account number
		error = Utils.validateDigitsOnlyAndEmpty(this, accountNumber.getText().toString(), getString(R.string.bank_wire_account_number));
		if (error != null) {
			accountNumber.requestFocus();
			Utils.showAlert(this, "", error,  this.getString(R.string.alert_title));
    		return ;
		}
		
		//branch number
		AnyoptionApplication app = (AnyoptionApplication) getApplication();
		if(app.getSkinId() != Skin.SKIN_GERMAN && app.getSkinId() != Skin.SKIN_REG_DE){
			error = Utils.validateDigitsOnlyAndEmpty(this, branchNumber.getText().toString(), getString(R.string.bank_wire_branch_number));
			if (error != null) {
				branchNumber.requestFocus();
				Utils.showAlert(this, "", error, this.getString(R.string.alert_title));
	    		return ;
			}
		}
		//amount number
		error = Utils.validateDigitsOnlyAndEmpty(this, amount.getText().toString(), getString(R.string.bank_wire_hint_amount, ""));
		if (error != null) {
			amount.requestFocus();
			Utils.showAlert(this, "", error,  this.getString(R.string.alert_title));  
    		return ;
		} 
		request = new WireMethodRequest();
		Wire wire = new Wire();			
		wire.setAccountNum(accountNumber.getText().toString());		
		if(branchNumber.getText().toString().length()==0){
			wire.setBranch(null);
		} else {
			wire.setBranch(branchNumber.getText().toString());		
		}
		wire.setAmount(amount.getText().toString());
		error = setParams(wire, error);
		if (error != null) {
			return ;
		}
		if (Utils.isEtraderProject(this)) {
			wire.setBankId(String.valueOf(eTBankId));
		}
		request.setWire(wire);
		
		if (clickedWithdrawal == false) {
			clickedWithdrawal = true;
			Utils.requestService(activity, responseH, "validateWithdrawBankCallBack", request, new TransactionMethodResult(), "validateWithdrawBank");	
		}
	}
	
	public abstract String setParams(Wire wire, String error); 
	
	/**
	 * In case of success navigate to important note popup
	 * @param result
	 */
	public void validateWithdrawBankCallBack(Object result) {
		Log.d(getLocalClassName(), "validateWithdrawBankCallBack");
		clickedWithdrawal = false;
		TransactionMethodResult tmr = (TransactionMethodResult) result;		
		if (tmr.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(getLocalClassName(), "showImportantNotePopup");
			showImportantNotePopUp(SERIVCE_NAME, R.string.bank_wire_success, request, tmr.getFee());
		} else {
			 String error = tmr.getUserMessages()[0].getMessage();
			 String field = tmr.getUserMessages()[0].getField();
			 if (tmr.getErrorCode() == Constants.ERROR_CODE_VALIDATION_WITHOUT_FIELD) {
				 field = Constants.EMPTY_STRING;
			 }			
			 Utils.showAlert(this, error, field, this.getString(R.string.alert_title));
		}
	}			
}
