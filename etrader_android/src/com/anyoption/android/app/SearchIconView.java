package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;


public class SearchIconView extends LinearLayout {
    /** Called when the activity is first created. */
	
    private LinearLayout main;
    private Context contextd;
    
    private final String TAG = "SearchIconView";
    
    public SearchIconView(Context context) {
        this(context, null);
        this.contextd = context;
    }

    public SearchIconView(final Context context, AttributeSet attrs) {
        super(context, attrs);
        this.contextd = context;
        
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        main = (LinearLayout) inflater.inflate(R.layout.search_icon, this, false);
        main.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// Get instance of Vibrator from current Context
		        Vibrator va = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		        
		        // Vibrate for 50 milliseconds
		        //va.vibrate(50);
				((Activity)contextd).findViewById(R.id.relativeLayoutSearch).setVisibility(View.VISIBLE);
				AutoCompleteTextView search = (AutoCompleteTextView) ((Activity)contextd).findViewById(R.id.autoCompleteTextViewSearch);
				search.requestFocus();
				((InputMethodManager)contextd.getSystemService(Context.INPUT_METHOD_SERVICE))
				.showSoftInput(search, InputMethodManager.SHOW_FORCED); 
			}
		});
        addView(main);
    }

}