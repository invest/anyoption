package com.anyoption.android.app;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.BaropayDepositMethodRequest;
import com.anyoption.json.requests.FirstEverDepositCheckRequest;
import com.anyoption.json.results.FirstEverDepositCheckResult;
import com.anyoption.json.results.TransactionMethodResult;
import com.anyoption.json.util.OptionItem;

/**
 * @author EranL
 *
 */
public class BaropayDepositActivity extends ReceiptActivity {
		
	private CreditCard currenctCC;
	private PaymentMethod payment;
	private EditText amountToDeposit;
	private Spinner spinnerMobilePref;
	private EditText mobilePhone;
	private boolean isFirstEverDeposit;
	
	public void init() {
		
	}
	
	 
	 
	 /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(buildLayout(Constants.PAYMENT_TYPE_DEPOSIT));
        responseH = new Handler();
        context = this;
        //for the plus info sign
        payment = (PaymentMethod) getIntent().getExtras().get(Constants.PUT_EXTRA_BANKING_PAYMENT);
        findViewById(R.id.imageViewInfo).setTag(payment);
        AnyoptionApplication ap = (AnyoptionApplication) getApplication();  
      //set mobile prefix spinner  
	    spinnerMobilePref = (Spinner) findViewById(R.id.spinnerMobilePrefix);
        ArrayAdapter spinnerMobilePrefArrayAdapter = new ArrayAdapter(this,
	              android.R.layout.simple_spinner_item, ((AnyoptionApplication)getApplication()).getKoreanMobilePref());  
        spinnerMobilePrefArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinnerMobilePref.setAdapter(spinnerMobilePrefArrayAdapter);
		
        ArrayList<OptionItem> mobilePrefList = ap.getKoreanMobilePref();
        int userMobilePrefId = 0;
        
        //Display relevant currency 
        User user = ap.getUser();
        amountToDeposit = (EditText)findViewById(R.id.baropayAmountToDeposit);
        mobilePhone = (EditText)findViewById(R.id.baropayMobilePhone);
       ((TextView)findViewById(R.id.TextViewCurrencySymbol)).setText(user.getCurrencySymbol());
       ((TextView)findViewById(R.id.TextViewHeader)).setText(getString(R.string.profitLine_deposit).toUpperCase());
       this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
       
		if (!Utils.isEtraderProject(context)) {
			getFirstEverDepositCheck();
		}
	}
    
    public void  sendBaropayDeposit(View target){  	
		if (!Utils.isEtraderProject(this) && isFirstEverDeposit) {
			DepositExtraFieldsView depositExtraFieldsView = (DepositExtraFieldsView) findViewById(R.id.DepositExtraFieldsView);
			depositExtraFieldsView.validateAndUpdate("sendBaropayDeposit");
		} else {
			sendBaropayDeposit();
		}
    }
    
    public void sendBaropayDeposit() {
    	String error = null;
    	//amount
		EditText depositAmount = (EditText)findViewById(R.id.baropayAmountToDeposit);
		EditText senderName = (EditText)findViewById(R.id.baropaySenderName);
    	error = Utils.validateDoubleAndEmpty(this, depositAmount.getText().toString(), getString(R.string.deposit_deposit));
    	if (null != error ){
    		Utils.showAlert(this, "", error, context.getString(R.string.alert_title));
    		return;
    	}
    	error = Utils.validateDigitsOnlyAndEmpty(context, mobilePhone.getText().toString(), getString(R.string.register_mobilephone));
    	if (null != error ){
    		Utils.showAlert(this, "", error, context.getString(R.string.alert_title));
    		return;
    	}
    	error = Utils.minimumLenthValidator(context, mobilePhone.getText().toString(), getString(R.string.register_mobilephone), 7);    			
    	if (null != error ){
    		Utils.showAlert(this, "", error, context.getString(R.string.alert_title));
    		return;
    	}    	    	
    	error = Utils.validateLettersOnly(context, senderName.getText().toString(), getString(R.string.sender_name));
    	if (null != error ){
    		Utils.showAlert(this, "", error, context.getString(R.string.alert_title));
    		return;
    	}    	
    	String text = spinnerMobilePref.getSelectedItem().toString();
    	//server validation 
    	BaropayDepositMethodRequest request = new BaropayDepositMethodRequest();
		request.setAmount(depositAmount.getText().toString());
		request.setSenderName(senderName.getText().toString());
		request.setLandPhone(text + mobilePhone.getText().toString());
		Utils.requestService(this, responseH, "insertBaropayDepositCallBack", request, new TransactionMethodResult(), "insertBaropayDeposit");
    } 
    
    public void insertBaropayDepositCallBack(Object resultObj){
    	Log.d(getPackageName(), "insertBaropayDepositCallBack");
    	TransactionMethodResult result = (TransactionMethodResult) resultObj;
    	if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
    		Log.d(getPackageName(), "send baro pay deposit");
    		handleSuccessfulBaropayDeposit(result);
    	}else {
			Log.d(getPackageName(), "Insert baropay deposit failed");
			Utils.handleResultError(context, result);
		}
    }

	protected void handleSuccessfulBaropayDeposit(TransactionMethodResult result) {
		String msg = getString(R.string.cup_deposit_success) + " " + ((TextView)findViewById(R.id.TextViewCurrencySymbol)).getText() + 
				     getString(R.string.cup_receipt_add, ((EditText)findViewById(R.id.baropayAmountToDeposit)).getText().toString()) + "\n" + 
				     getString(R.string.receipt_transactionid, result.getTransactionId());
		LayoutInflater inflater = getLayoutInflater();
		LinearLayout popup = (LinearLayout)inflater.inflate(R.layout.cup_deposit_success_pop_up, null);
		TextView textViewMsg = (TextView) popup.findViewById(R.id.textViewMsg);
		textViewMsg.setText(msg);
		
		transId = result.getTransactionId();
		final AlertDialog ad = Utils.showCustomAlert(this, popup, null);
		Button tradeNow = (Button) popup.findViewById(R.id.buttonTradeNow);
		tradeNow.setOnClickListener(new OnClickListener() {				
			public void onClick(View arg0) {
				Intent intent =  new Intent(context, MarketsGroupActivity.class);
			    startActivityForResult(intent, Constants.REQUEST_CODE_MARKETS_GROUP_PAGE);
			    ad.dismiss();
			    finish();
			}
		});
	}
	
	private void getFirstEverDepositCheck() {
		FirstEverDepositCheckRequest request = new FirstEverDepositCheckRequest();
		AnyoptionApplication application = (AnyoptionApplication) getApplication();
		User user = application.getUser();
		request.setUserId(user.getId());

		Utils.requestService(this, responseH,
				"getFirstEverDepositCheckCallback", request,
				new FirstEverDepositCheckResult(), "getFirstEverDepositCheck");
	}

	public void getFirstEverDepositCheckCallback(Object resultObject) {
		FirstEverDepositCheckResult result = (FirstEverDepositCheckResult) resultObject;

		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			isFirstEverDeposit = result.isFirstEverDeposit();
		} else {
			isFirstEverDeposit = false;
		}

		DepositExtraFieldsView extraFieldsView = (DepositExtraFieldsView) findViewById(R.id.DepositExtraFieldsView);
		extraFieldsView.setVisibility(View.VISIBLE);

		if (isFirstEverDeposit) {
			extraFieldsView.setCurrencyEnabled(true);
		} else {
			// Will be displayed only currency in disabled state.
			extraFieldsView.setCurrencyEnabled(false);
			extraFieldsView.setStreetVisibility(View.GONE);
			extraFieldsView.setCityVisibility(View.GONE);
			extraFieldsView.setZipCodeVisibility(View.GONE);
		}
	}
	
}
