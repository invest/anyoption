package com.anyoption.android.app;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.database.Cursor;
import android.util.Log;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.DataBaseHelper;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.City;
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.json.util.OptionItem;

/**
 * Application class
 * store data in application scope for example: Locale, skinId etc. 
 * 
 * @author KobiM
 */
public class AnyoptionApplication extends Application {     
    private static final String LOG_TAG = "AnyoptionApplication";
    public static boolean HAVE_INTERNET_CONNECTION = true;
    
    private Locale locale = null;
	private User user;
	private UserRegulationBase userRegulation;
	private long skinId;
	private long countryId;
		
	private HashMap<Long, Skin> skins;
	private HashMap<Long, Country> countries;
	private HashMap<Long, City> cities;
	private ArrayList<Long> currencies;
	private Market[] marketListSorted;
	private String[] creditCardYears;
	private City[] citiesArray;
	private Country[] countriesArray;
	private ArrayList<OptionItem> mobilePref;
	private ArrayList<OptionItem> etLandPref;
	private ArrayList<OptionItem> koreanMobilePref;
	
	@Override     
	public void onConfigurationChanged(Configuration newConfig) {         
		super.onConfigurationChanged(newConfig);         
		if (locale != null)  {             
		    Configuration config = new Configuration(newConfig);
		    config.locale = locale;
			Locale.setDefault(locale);             
			getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());         
		}     
	}     
	
	@Override    
	public void onCreate() {        
		super.onCreate();
		SharedPreferences settings = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
		Configuration config = getBaseContext().getResources().getConfiguration();
		String lang;
		if(Constants.IS_CHINESE_ONLY){
			setSkinId(settings.getLong(Constants.PREF_SKIN_ID, Skin.SKIN_CHINESE));
			lang = settings.getString(Constants.PREF_LOCALE, "zh");
		}else {
			setSkinId(settings.getLong(Constants.PREF_SKIN_ID, Skin.SKIN_ENGLISH));
			lang = settings.getString(Constants.PREF_LOCALE, "en");
	
		}
		locale = new Locale(lang);
		if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) { 
			Locale.setDefault(locale);
			config.locale = locale;             
			getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		}
		//save first time visit
		long timeFirstVisit = settings.getLong(Constants.PREF_TIME_FIRST_VISIT, 0);
		if (timeFirstVisit == 0) {
			settings.edit().putLong(Constants.PREF_TIME_FIRST_VISIT, new Date().getTime()).commit();
		}
		mobilePref = new ArrayList<OptionItem>();
		mobilePref.add(new OptionItem(0,getString(R.string.error_register_prefix)));
		mobilePref.add(new OptionItem(1,"050"));
		mobilePref.add(new OptionItem(2,"052"));
		mobilePref.add(new OptionItem(3,"053"));
		mobilePref.add(new OptionItem(4,"054"));
		mobilePref.add(new OptionItem(5,"055"));
		mobilePref.add(new OptionItem(6,"057"));
		mobilePref.add(new OptionItem(7,"058"));
					
		etLandPref = new ArrayList<OptionItem>();
		etLandPref.add(new OptionItem(0,getString(R.string.error_register_prefix)));
		etLandPref.add(new OptionItem(1,"02"));
		etLandPref.add(new OptionItem(2,"03"));
		etLandPref.add(new OptionItem(3,"04"));
		etLandPref.add(new OptionItem(4,"08"));
		etLandPref.add(new OptionItem(5,"09"));
		etLandPref.add(new OptionItem(6,"072"));
		etLandPref.add(new OptionItem(7,"073"));
		etLandPref.add(new OptionItem(8,"074"));
		etLandPref.add(new OptionItem(9,"076"));
		etLandPref.add(new OptionItem(10,"077"));
		etLandPref.add(new OptionItem(11,"078"));
		
		koreanMobilePref = new ArrayList<OptionItem>();
		koreanMobilePref.add(new OptionItem(0,"010"));
		koreanMobilePref.add(new OptionItem(1,"011"));
		koreanMobilePref.add(new OptionItem(2,"016"));
		koreanMobilePref.add(new OptionItem(3,"017"));
		koreanMobilePref.add(new OptionItem(4,"018"));
		koreanMobilePref.add(new OptionItem(5,"019"));
				// Set up Lightstreamer connection timeouts
//        System.setProperty("lightstreamer.client.defaultConnectTimeout", "10000");
//        System.setProperty("lightstreamer.client.defaultReadTimeout", "10000");
	}
	
	public void addPaymentMethod(long paymentTypeId, String paymentTypeName, String displayName,
			String smallLogo, String bigLogo, int typeId, Country country){
		PaymentMethod pm = new PaymentMethod();
		pm.setId(paymentTypeId);
		pm.setName(paymentTypeName);
		pm.setDisplayName(displayName);
		pm.setSmallLogo(smallLogo);
		pm.setBigLogo(bigLogo);
		pm.setTypeId(typeId);
		country.getPayments().add(pm);
	}
	
	public void changeLocale(String lang) {
		Configuration config = getBaseContext().getResources().getConfiguration();
		locale = new Locale(lang);
		Locale.setDefault(locale);
		config.locale = locale;             
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		marketListSorted = null;
		SharedPreferences.Editor edit = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE).edit();
		edit.putString(Constants.PREF_LOCALE, lang);
		edit.commit();
	}
	
	public String getLocale() {
		return this.locale.getLanguage();
	}

	
	public ArrayList<OptionItem> getMobilePref() {
		return mobilePref;
	}

	public void setMobilePref(ArrayList<OptionItem> mobilePref) {
		this.mobilePref = mobilePref;
	}

	public ArrayList<OptionItem> getKoreanMobilePref() {
		return koreanMobilePref;
	}

	public void setKoreanMobilePref(ArrayList<OptionItem> koreanMobilePref) {
		this.koreanMobilePref = koreanMobilePref;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
		if (null != user && user.getSkinId() != skinId) {
		    skinId = user.getSkinId();
		    SharedPreferences.Editor edit = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE).edit();
			edit.putLong(Constants.PREF_SKIN_ID, skinId);
			edit.commit();
		    changeLocale(user.getLocale().getLanguage());
		}
	}

	public HashMap<Long, Skin> getSkins() {
		return skins;
	}

	public ArrayList<Long> getCurrencies() {
		return currencies;
	}

	public long getSkinId() {
		return skinId;
	}

	public void setSkinId(long skinId) {
		this.skinId = skinId;
        try {
            DataBaseHelper.DB_PATH = getPackageManager().getPackageInfo(getPackageName(), 0).applicationInfo.dataDir + "/databases/";
            Log.i(getPackageName(), "db path = " + DataBaseHelper.DB_PATH);
        } catch (NameNotFoundException e1) {
            Log.d(getPackageName(), "cant take db path", e1);
        }
        // Create DB and load lists
        DataBaseHelper db = null;
        Cursor c = null;
        try {
            db = new DataBaseHelper(getApplicationContext());
            db.createDataBase();
            db.openDataBase();
            
            skins = new HashMap<Long, Skin>();
            currencies = new ArrayList<Long>();
            countries = new HashMap<Long, Country>();
            cities = new HashMap<Long, City>();
            
            ArrayList<Country> countriesArrayList = new ArrayList<Country>();
            ArrayList<City> citiesArrayList = new ArrayList<City>();
            
            c = db.getSkins();
            while (c.moveToNext()) {
                Skin s = new Skin();
                s.setId(c.getInt(c.getColumnIndex("skin_id")));
                s.setName(c.getString(c.getColumnIndex("display_name")));
                s.setDefaultCountryId(c.getInt(c.getColumnIndex("defaultCountryId")));
                s.setDefaultCurrencyId(c.getInt(c.getColumnIndex("defaultCurrencyId")));
                s.setDefaultLanguageId(c.getInt(c.getColumnIndex("defaultLanguageId")));
                s.setSupportStartTime(c.getString(c.getColumnIndex("SUPPORT_START_TIME")));
                s.setSupportEndTime(c.getString(c.getColumnIndex("SUPPORT_END_TIME")));
                s.setSkinGroup(SkinGroup.getById(c.getLong(c.getColumnIndex("skin_group_id"))));
                skins.put(Long.valueOf(s.getId()), s);
            }

            c = db.getSkinCurreines(skinId);            
            while (c.moveToNext()) {
                currencies.add(c.getLong(c.getColumnIndex("currency_id")));
            }
            
            if (skinId == Constants.SKIN_ET) {
	            c = db.getCities();            
	            while (c.moveToNext()) {
	            	City city = new City();
	//                ArrayList<PaymentMethod> pmArray = new ArrayList<PaymentMethod>();
	                city.setId(c.getLong(c.getColumnIndex("id")));
	                city.setName(c.getString(c.getColumnIndex("name")));
	                cities.put(Long.valueOf(city.getId()), city);                  
	                citiesArrayList.add(city);               
	            }
	            citiesArray = citiesArrayList.toArray(new City[citiesArrayList.size()]);          
            }
            
            c = db.getCountries(skins.get(skinId).getDefaultLanguageId());
            while (c.moveToNext()) {
            	if (Utils.isEtraderProject(getBaseContext())) {
            		if (c.getLong(c.getColumnIndex("id")) != Constants.COUNTRY_IRAN_ID){
            			 Country country = new Country();
                         ArrayList<PaymentMethod> pmArray = new ArrayList<PaymentMethod>();
                         country.setId(c.getLong(c.getColumnIndex("id")));
                         country.setA2(c.getString(c.getColumnIndex("a2")));
                         country.setName(c.getString(c.getColumnIndex("name")));
                         country.setPhoneCode(c.getString(c.getColumnIndex("phone_code")));
                         country.setSupportPhone(c.getString(c.getColumnIndex("support_phone")));
                         country.setSupportFax(c.getString(c.getColumnIndex("support_fax")));                
                         Long countryId = new Long (Long.valueOf(country.getId()));              
                         long paymentTypeId = c.getLong(c.getColumnIndex("paymentTypeId")); 
                         String paymentTypeName = c.getString(c.getColumnIndex("paymentTypeName"));
                         String paymentTypeDisplayName = c.getString(c.getColumnIndex("paymentTypeDisplayName"));
                         String smallLogo = c.getString(c.getColumnIndex("small_logo"));
                         String bigLogo = c.getString(c.getColumnIndex("big_logo"));
                         int typeId = c.getInt(c.getColumnIndex("type_id"));
                         if (countries.get(countryId) != null && paymentTypeId != 0){                        
                             addPaymentMethod(paymentTypeId, paymentTypeName, paymentTypeDisplayName, smallLogo, 
                                     bigLogo, typeId, countries.get(countryId));                 
                         } else {
                             country.setPayments(pmArray);
                             countries.put(Long.valueOf(country.getId()), country);                  
                             countriesArrayList.add(country);
                             if (paymentTypeId != 0){
                                 addPaymentMethod(paymentTypeId, paymentTypeName, paymentTypeDisplayName, smallLogo, 
                                         bigLogo, typeId, country);
                             }
                         }
            		}
            	} else if (c.getLong(c.getColumnIndex("id")) != Constants.COUNTRY_ISRAEL_ID 
            					&& c.getLong(c.getColumnIndex("id")) != Constants.COUNTRY_IRAN_ID && c.getLong(c.getColumnIndex("id")) != Constants.COUNTRY_UNITED_STATES_ID){
                    Country country = new Country();
                    ArrayList<PaymentMethod> pmArray = new ArrayList<PaymentMethod>();
                    country.setId(c.getLong(c.getColumnIndex("id")));
                    country.setA2(c.getString(c.getColumnIndex("a2")));
                    country.setName(c.getString(c.getColumnIndex("name")));
                    country.setPhoneCode(c.getString(c.getColumnIndex("phone_code")));
                    country.setSupportPhone(c.getString(c.getColumnIndex("support_phone")));
                    country.setSupportFax(c.getString(c.getColumnIndex("support_fax")));                
                    Long countryId = new Long (Long.valueOf(country.getId()));              
                    long paymentTypeId = c.getLong(c.getColumnIndex("paymentTypeId")); 
                    String paymentTypeName = c.getString(c.getColumnIndex("paymentTypeName"));
                    String paymentTypeDisplayName = c.getString(c.getColumnIndex("paymentTypeDisplayName"));
                    String smallLogo = c.getString(c.getColumnIndex("small_logo"));
                    String bigLogo = c.getString(c.getColumnIndex("big_logo"));
                    int typeId = c.getInt(c.getColumnIndex("type_id"));
                    if (countries.get(countryId) != null && paymentTypeId != 0){                        
                        addPaymentMethod(paymentTypeId, paymentTypeName, paymentTypeDisplayName, smallLogo, 
                                bigLogo, typeId, countries.get(countryId));                 
                    } else {
                        country.setPayments(pmArray);
                        countries.put(Long.valueOf(country.getId()), country);                  
                        countriesArrayList.add(country);
                        if (paymentTypeId != 0){
                            addPaymentMethod(paymentTypeId, paymentTypeName, paymentTypeDisplayName, smallLogo, 
                                    bigLogo, typeId, country);
                        }
                    }
                }                               
            }           
            //Collections.sort(countriesArrayList, new Country.AlphaComparator());          
            countriesArray = countriesArrayList.toArray(new Country[countriesArrayList.size()]);            
        } catch (Exception e) {
            Log.e(LOG_TAG, "Problem with DB creation/loading ", e);
        } finally {
            if (null != c) {
                c.close();
            }
            if (null != db) {
                db.close();
            }
        }
	}

	/**
	 * @return the countries
	 */
	public HashMap<Long, Country> getCountries() {
		return countries;
	}

	/**
	 * @param countries the countries to set
	 */
	public void setCountries(HashMap<Long, Country> countries) {
		this.countries = countries;
	}

	public Market[] getMarketListSorted() {
		return marketListSorted;
	}

	public void setMarketListSorted(Market[] marketListSorted) {
		this.marketListSorted = marketListSorted;
	}

	public String[] getCreditCardYears() {
		return creditCardYears;
	}

	public void setCreditCardYears(String[] creditCardYears) {
		this.creditCardYears = creditCardYears;
	}		

	public Country[] getCountriesArray() {
		return countriesArray;
	}

	public void setCountriesArray(Country[] countriesArray) {
		this.countriesArray = countriesArray;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public ArrayList<OptionItem> getEtLandPref() {
		return etLandPref;
	}

	public void setEtLandPref(ArrayList<OptionItem> etLandPref) {
		this.etLandPref = etLandPref;
	}

	public HashMap<Long, City> getCities() {
		return cities;
	}

	public void setCities(HashMap<Long, City> cities) {
		this.cities = cities;
	}

	public City[] getCitiesArray() {
		return citiesArray;
	}

	public void setCitiesArray(City[] citiesArray) {
		this.citiesArray = citiesArray;
	}

	public UserRegulationBase getUserRegulation() {
		return userRegulation;
	}

	public void setUserRegulation(UserRegulationBase userRegulation) {
		this.userRegulation = userRegulation;
	}

} 