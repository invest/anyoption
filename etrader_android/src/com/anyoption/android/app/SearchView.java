package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.Market;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.results.MarketsMethodResult;

public class SearchView extends RelativeLayout {
    /** Called when the activity is first created. */
	
    private LinearLayout mainSearch;
    private Context contextd;
    private Market[] marketList;
    private Handler responseH;
    private AnyoptionApplication ap;
    
    private final String TAG = "SearchView";
    
    public SearchView(Context context) {
        this(context, null);
        this.contextd = context;
    }

    public SearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.contextd = context;
        responseH = new Handler();
        
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        mainSearch = (LinearLayout) inflater.inflate(R.layout.search, this, false);
        ap = (AnyoptionApplication) context.getApplicationContext();
        marketList = ap.getMarketListSorted();
        if (null != marketList) {
        	setAutoCompleteAdapter();
        } else {
        	Utils.requestService(this, responseH, "searchViewCallBack", new MethodRequest(), new MarketsMethodResult(), "getMarketList", (Activity) context);
        }
        addView(mainSearch);
    }
    
    public void searchViewCallBack(Object result) {
    	MarketsMethodResult marketsResult = (MarketsMethodResult) result;
    	marketList = marketsResult.getMarkets();
    	ap.setMarketListSorted(marketList);
    	setAutoCompleteAdapter();
    }
    
    public void setAutoCompleteAdapter() {
    	ArrayAdapter<Market> adapter = new ArrayAdapter<Market>(contextd,
        		android.R.layout.simple_dropdown_item_1line, marketList);
        AutoCompleteTextView textView = (AutoCompleteTextView) mainSearch.findViewById(R.id.autoCompleteTextViewSearch);
        textView.setThreshold(1);
        textView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
				Log.d(TAG, "item click listener pos = " + pos);
				ArrayAdapter<Market> adapter = (ArrayAdapter<Market>) parent.getAdapter();
				Market market = adapter.getItem(pos);
				Utils.gotToAssetPage((Activity) contextd, market);
				((InputMethodManager)contextd.getSystemService(Context.INPUT_METHOD_SERVICE))
				.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0); 
			}
		});
        textView.setAdapter(adapter);
    }
}