package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.questionnaire.MandatoryQuestionnaireActivity;
import com.anyoption.android.app.questionnaire.QuestionnaireUtils;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.json.requests.UserMethodRequest;
import com.anyoption.json.results.UserMethodResult;

/**
 * @author AviadH
 */
public class LoginView extends LinearLayout{
	
	private LayoutInflater inflater;
	private SharedPreferences settings;	
	private EditText userNameET;
	private EditText passwordET;
	private Handler responseH;
	private final Context context;
	private boolean needToErase;
	
	public LoginView(Context context) {
		this(context, null);
	}
	
	public LoginView(final Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		needToErase = true;
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
		LinearLayout loginLayout = (LinearLayout) inflater.inflate(R.layout.login_view, this, false);
		this.addView(loginLayout);
        responseH = new Handler();
        userNameET = (EditText)loginLayout.findViewById(R.id.editTextUserName);
        passwordET = (EditText)loginLayout.findViewById(R.id.editTextPassword);
        //fix font problem
        passwordET.setTypeface(Typeface.DEFAULT); 
        passwordET.setTransformationMethod(new PasswordTransformationMethod()); 
        
        Button loginButton = (Button)loginLayout.findViewById(R.id.buttonLogin);
        final TextView forgotPass = (TextView) loginLayout.findViewById(R.id.textViewForgot);
        forgotPass.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.on_click_bg));
                
        forgotPass.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {				
				forgotPasswordHandler();
			}
		});
        
        loginButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {				
				validateLoginForm();
			}
		});
        
        settings = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        String userName = settings.getString(Constants.PREF_USER_NAME, null);
        userNameET.setHint(context.getString(R.string.register_email) + "\\" + context.getString(R.string.register_username));
        if (Utils.isEtraderProject(context)) {
        	userNameET.setHint(context.getString(R.string.register_username));
        }
        
        if (null != userName) {
        	userNameET.setText(userName);
        }
        
        userNameET.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				if (count > after && needToErase) {
					needToErase = false;
					userNameET.setText("");
				}
				
			}
			
			public void afterTextChanged(Editable s) {
				if (needToErase) {
					needToErase = false;
					if (s.length() > 0) {
						userNameET.setText(s.subSequence(s.length()-1, s.length()));
					}
					userNameET.setSelection(userNameET.getText().length());
				}	
			}
		});
	}
	
	public void setNeedToErase(boolean needToErase) {
		this.needToErase = needToErase;
		userNameET.setSelection(userNameET.getText().length());
	}
    
	public void resetPassword() {
	    passwordET.setText("");
	}
	
	public void validateLoginForm() {		
    	//Client validations
    	String error = Utils.validateUserName(context, userNameET.getText().toString()); 
    	if (error != null) {
    		error = Utils.validateEmail(context, userNameET.getText().toString()); 
    		if (error != null) {
	    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
	    		return;
    		}
    	}
    	error = Utils.validatePassword(context, passwordET.getText().toString()); 
    	if (error != null){
    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
    		return;
    	}    	
    	//Service validations
        UserMethodRequest request = new UserMethodRequest();
        request.setUserName(userNameET.getText().toString());
        request.setLogin(true);
        if (Utils.isEtraderProject(context)) {
        	request.setPassword(passwordET.getText().toString().toUpperCase());
        } else {
        	request.setPassword(passwordET.getText().toString());
        }        
        UserMethodResult result = new UserMethodResult();  
        Utils.requestService(this, responseH, "getUserCallBack", request, result, "getUser", (Activity)context);
	}
	
	public void getUserCallBack(Object result) {
	    UserMethodResult res = (UserMethodResult) result;
	    if (res.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
	        Utils.showAlert(context, res.getUserMessages()[0].getField(), 
	        		res.getUserMessages()[0].getMessage(), context.getString(R.string.alert_title));
	    } else {
	    	if((res.getUser().getSkinId() == Skin.SKIN_ETRADER && !Utils.isEtraderProject(context)) 
	    			|| (res.getUser().getSkinId() != Skin.SKIN_ETRADER && Utils.isEtraderProject(context))) {
	    		// Etrader user should not log in AO app and AO user should not log in Etrader app.
	    		return;
	    	}

			// save user instance
			Utils.setmIdIfValid(context, res.getmId());
			Utils.setEtsMId(context, res.getEtsMid());
	    	
	    	if(Constants.IS_CHINESE_ONLY){
	    		if(res.getUser().getSkinId() == Skin.SKIN_CHINESE){
	    			logAndSaveUser(res);
	    		} else {
	    			String errorMsg = "Sorry, this application is only available for Chinese users";
					Utils.showAlert(context, "" , errorMsg  , context.getString(R.string.alert_title));
	    		}
	    	} else {
	    		logAndSaveUser(res);
	    	} 

	    }
	}

	private void logAndSaveUser(UserMethodResult res) {
		AnyoptionApplication ap = (AnyoptionApplication) context.getApplicationContext();
		ap.setUser(res.getUser());
		ap.setUserRegulation(res.getUserRegulation());
		
		// save in sharedPreferences
		String userNameSP = settings.getString(Constants.PREF_USER_NAME, null);
		String userNameView = userNameET.getText().toString();
		String pass = ap.getUser().getEncryptedPassword();
		if (null == userNameSP || (null != userNameSP && !userNameSP.equalsIgnoreCase(userNameView))) {
		    SharedPreferences.Editor edit = settings.edit();
		    edit.putString(Constants.PREF_USER_NAME, userNameView);
		    edit.putString(Constants.PREF_PASSWORD, pass);
		    edit.commit();
		}
		if (null != ap.getUser()) {
			if (!ap.getUser().isAcceptedTerms()) {
				Intent intent = new Intent(context, TermsActivity.class).putExtra(Constants.ACTIVITY_BEFORE_TERMS, "markets_group");
				((Activity) context).startActivityForResult(intent, Constants.REQUEST_CODE_TERMS);
			}  else if(Utils.isRegulated(context) && Utils.isSuspendedForDocuments(context)){
				context.startActivity(new Intent(context, SuspendedActivity.class));
			} else if (Utils.isRegulated(context) && Utils.isAfterFirstDeposit(context) && !QuestionnaireUtils.isMandQuestionnaireAnswered(context) && !Utils.isSuspended(context)){
			
				Intent intent = new Intent(context, MandatoryQuestionnaireActivity.class);
				intent.putExtra(Constants.ACTIVITY_BEFORE_QUESTIONNAIRE, Constants.FROM_LOGIN);
				((Activity) context).startActivity(intent);
			} else {
		        ((Activity) context).startActivityForResult(
		                new Intent(context,
		                        MarketsGroupActivity.class).putExtra(Constants.PUT_EXTRA_SHOW_LOG_OUT_ALERT, true),
		                        Constants.REQUEST_CODE_MARKETS_GROUP_PAGE);
			}
		} else {
		    ((Activity) context).startActivityForResult(
		            new Intent(context,
		                    MarketsGroupActivity.class).putExtra(Constants.PUT_EXTRA_SHOW_LOG_OUT_ALERT, true),
		                    Constants.REQUEST_CODE_MARKETS_GROUP_PAGE);
		}
	}
	
	public void forgotPasswordHandler(){
		 //start forgotPassword
		Class<Activity> actvityRedirect = Utils.getActivityClass(Constants.ACTIVITY_REDIRECT_FORGOT, context);
	    ((Activity) context).startActivity(new Intent(context, actvityRedirect)); 
		
	}
}