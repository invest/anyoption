package com.anyoption.android.app;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Contact;
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.ContactMethodRequest;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.results.ContactMethodResult;
import com.anyoption.json.results.CountryIdMethodResult;


public class SendEmailActivity extends BaseActivity {
	
	private User user;
	private EditText firstName;
	private EditText lastName;
	private EditText email;
	private EditText mobile;
	private EditText description;
	private Spinner spinner;
	//In order to take the default countryId from the service
	private long countryId;

	public static final long ISRAEL_PHONE_CODE = 972;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_an_email);
        Utils.requestService(this, responseH, "getCountryIdCallBack", new MethodRequest(), new CountryIdMethodResult(), "getCountryId");  
                         
        AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        
        spinner = (Spinner) findViewById(R.id.spinnerIssue);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.email_issues, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        
        firstName = (EditText) findViewById(R.id.editTextFirstname);
		lastName =  (EditText) findViewById(R.id.editTextLastname);
		email = (EditText) findViewById(R.id.editTextEmail);
		mobile = (EditText) findViewById(R.id.editTextMobilePhone);
		description = (EditText) findViewById(R.id.editTextEmailDescription);
		
        user = ap.getUser();
        LinearLayout llUserDetails = (LinearLayout)findViewById(R.id.linearLayoutUserDetails);    
		if (null != user) {
			llUserDetails.setVisibility(View.GONE);
	    	firstName.setText(user.getFirstName());
	    	lastName.setText(user.getLastName());
	    	email.setText(user.getEmail());
	    	mobile.setText(user.getMobilePhone());
	    	TextView prefix = (TextView) findViewById(R.id.textViewMobilePhonePrefix);
	    	if (!Utils.isEtraderProject(this)){
		    	Country country = ((AnyoptionApplication)getApplication()).getCountries().get(user.getCountryId());		 		
		 		prefix.setText("+" + country.getPhoneCode());
		 		
	    	} else {
	    		prefix.setText("+" + ISRAEL_PHONE_CODE);
	    	}
	    	prefix.setVisibility(View.VISIBLE);
		} else {
			llUserDetails.setVisibility(View.VISIBLE);
		} 
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void sendEmailHandler(View target){    
    	//client validations
    	String error = null;
    	if (null == user){    		    	
	    	//First Name
	    	if (Utils.isEtraderProject(this)){
	    		error = Utils.validateLettersOnlyAndEmpty(this, firstName.getText().toString(), getString(R.string.register_firstname));
	    	} else {
	    		error = Utils.validateEnglishLettersOnlyAndEmpty(this, firstName.getText().toString(), getString(R.string.register_firstname));
	    	}    	
	    	if (error!= null){
	    		Utils.showAlert(this, "", error, this.getString(R.string.alert_title));  
	    		return ;
	    	}
	    	//Last Name
	    	if (Utils.isEtraderProject(this)){
	    		error = Utils.validateLettersOnlyAndEmpty(this, lastName.getText().toString(), getString(R.string.register_lastname));
	    	} else {
	    		error = Utils.validateEnglishLettersOnlyAndEmpty(this, lastName.getText().toString(), getString(R.string.register_lastname));
	    	}
	    	if (error!= null){
	    		Utils.showAlert(this, "", error, this.getString(R.string.alert_title));  
	    		return ;
	    	}
	    	//Email
	    	error = Utils.validateEmailAndEmpty(this, email.getText().toString(), getString(R.string.register_email)); 
	    	if (error!= null){
	    		Utils.showAlert(this, "", error, this.getString(R.string.alert_title));  
	    		return ;
	    	}
	    	//Mobile Phone
	    	error = Utils.validateDigitsOnlyAndEmpty(this, mobile.getText().toString(), getString(R.string.register_mobilephone));
	    	if (error!= null){
	    		Utils.showAlert(this, "", error, this.getString(R.string.alert_title));  
	    		return ;
	    	}
	    	if (mobile.getText().toString().length() < 7){
				String[] params = new String[2];
				params[0] = getString(R.string.register_mobilephone);
				params[1] = "7";    		
	    		Utils.showAlert(this, "", String.format(this.getString(R.string.javax_faces_validator_LengthValidator_MINIMUM), params), 
	    				this.getString(R.string.alert_title));  
	    		return ;
	    	}
    	}
    	//Description
    	error = Utils.isFieldEmpty(this, description.getText().toString(), getString(R.string.send_email_description));
    	if (error!= null) {
    		Utils.showAlert(this, "", error, this.getString(R.string.alert_title));  
    		return ;
    	}

    	if (error == null) {
    		ContactMethodRequest request = new ContactMethodRequest();
    		Contact contact = new Contact();
    		contact.setType(Contact.CONTACT_US_TYPE);    		
    		//send the conutry phone prefix
    		if (null != user) {
    			contact.setCountryId(user.getCountryId());
    			contact.setName(user.getFirstName() + user.getLastName());
        		contact.setEmail(user.getEmail());
        		contact.setPhone(user.getMobilePhone());
    		} else {
    			contact.setPhone(mobile.getText().toString());
    			contact.setName(firstName.getText() + " " + lastName.getText());
        		contact.setEmail(email.getText().toString());
        		contact.setCountryId(countryId);
    		}
    		
    		contact.setCombId(Utils.getCombinationId(this));
    		contact.setDynamicParameter(Utils.getDynamicParam(this));
    		contact.setHttpReferer(Utils.getReferrerParam(this));
    		contact.setmId(Utils.getmId(this));
    		contact.setEtsMId(Utils.getEtsMId(this));
    		
    		request.setContact(contact); 
    		request.setComments(description.getText().toString());
    		request.setIssue((spinner.getAdapter().getItem(spinner.getSelectedItemPosition())).toString());
    		Utils.requestService(this, responseH, "callMeCallBack", request, new ContactMethodResult(), "insertContact");
    	}
    }
    
	public void callMeCallBack(Object result) {
		ContactMethodResult contanctResult = (ContactMethodResult) result;
		 if (contanctResult.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
			 String newmId = contanctResult.getContact().getmId();
			 Utils.setmIdIfValid(this, newmId);
			 
			 String newEtsMId = contanctResult.getContact().getEtsMId();
			 Utils.setEtsMId(this, newEtsMId);
			 
			 Utils.showAlert(this, contanctResult.getUserMessages()[0].getField(), 
					 contanctResult.getUserMessages()[0].getMessage(), this.getString(R.string.alert_title));
	     } else {
	    	 Log.d(this.getPackageName(), "Contanct Name: " + contanctResult.getContact().getName() + " Inserted");
	    	 LayoutInflater inflater = getLayoutInflater();
	    	 LinearLayout popUp = (LinearLayout) inflater.inflate(R.layout.call_me_pop_up, null, false);
	    	 Button closeButton = (Button)popUp.findViewById(R.id.buttonOk);
	    	 Utils.showCustomAlert(this, popUp, closeButton, new OnDismissListener() {
				public void onDismiss(DialogInterface arg0) {
					finish();
				}
			});
	     }
    }
	
	 public void getCountryIdCallBack(Object result) {
    	Log.d(getPackageName(), "getCountryIdCallBack");
    	CountryIdMethodResult omr = (CountryIdMethodResult) result;
    	countryId = omr.getCountryId();
     }
	              
}