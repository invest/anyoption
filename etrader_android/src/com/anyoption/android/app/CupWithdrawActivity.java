package com.anyoption.android.app;

import java.io.UnsupportedEncodingException;

import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.CcDepositMethodRequest;
import com.anyoption.json.requests.FeeMethodRequest;
import com.anyoption.json.requests.UserMethodRequest;
import com.anyoption.json.results.CupWithdrawalMethodResult;
import com.anyoption.json.results.TransactionMethodResult;

public class CupWithdrawActivity extends WithdrawalBase{
	
	private final static String SERIVCE_NAME = "insertWithdrawDeltapay";
	private static final String WITHDRAW_WIRE = "bank_wire_withdrawal";
	Button submitButton;
	EditText amountText;
	private User user;
	private FeeMethodRequest request;
	private TextView maxAmountText;
	private long maxWithdrawAmount;
	private TextView gotoWireText;
	
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
		 setContentView(buildLayout(Constants.PAYMENT_TYPE_WITHDRAW));
	     PaymentMethod payment = (PaymentMethod) getIntent().getExtras().get(Constants.PUT_EXTRA_BANKING_PAYMENT);
	     findViewById(R.id.imageViewInfo).setTag(payment);
	     activity = this;
	     //set the currency symbol
	     AnyoptionApplication ap = (AnyoptionApplication) getApplication();
	     user = ap.getUser();
	     ((TextView) findViewById(R.id.textViewCurrencySymbol)).setText(user.getCurrencySymbol());
	     
	     gotoWireText = (TextView) findViewById(R.id.gotoWireText);
	     gotoWireText.setText(Html.fromHtml(getResources().getString(R.string.cup_withdraw_gotowire)));
	     
	     CharSequence sequence = Html.fromHtml(getString(R.string.cup_withdraw_gotowire));
	     SpannableStringBuilder strBuilder = Utils.makeUnderlineClickable(sequence, new ClickableSpan() {
	          public void onClick(View view) {
	        	  openWirePage();  
	          }

			protected void openWirePage() {
				PaymentMethod wirePayment = new PaymentMethod();
	        	  wirePayment .setId(PaymentMethod.PAYMENT_TYPE_WIRE);
	        	  wirePayment.setTypeId(Constants.PAYMENT_TYPE_WITHDRAW);
	        	  wirePayment.setBigLogo("logo_creditcards_big");
	        	  WithdrawalView.bankingRowClick(WITHDRAW_WIRE, null, 0, R.string.header_bank_wire_text, wirePayment, activity);
			}
	        });

	     gotoWireText.setText(strBuilder);
	     gotoWireText.setMovementMethod(LinkMovementMethod.getInstance());
	     
	     
		 submitButton = (Button)findViewById(R.id.SubmitWithdrawal);
		 amountText = (EditText)findViewById(R.id.amountToWithdraw);
		 maxAmountText = (TextView) findViewById(R.id.textViewMessage);
		 maxAmountText.setVisibility(View.VISIBLE);
		 
		
		 Utils.requestService(activity, responseH, "getMaxCupWithdrawalCallback", new UserMethodRequest(), new CupWithdrawalMethodResult(), "getMaxCupWithdrawal");		
	 }


	 
	 public void getMaxCupWithdrawalCallback(Object resultObj){
		 Log.d(getPackageName(), "checkMaxAmountCallback");
			CupWithdrawalMethodResult result = (CupWithdrawalMethodResult) resultObj;
			if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
				maxWithdrawAmount = result.getMaxCupWithdrawal();
				String maxAmountMessage = getResources().getString(R.string.error_maxwithdraw, Utils.formatCurrencyAmount(maxWithdrawAmount, user.getCurrencySymbol(), user.isCurrencyLeftSymbol(), user.getCurrencyId()));
				maxAmountText.setText(maxAmountMessage);
			} else {
				String error = result.getUserMessages()[0].getMessage();
				 String field = result.getUserMessages()[0].getField();
				 if (result.getErrorCode() == Constants.ERROR_CODE_VALIDATION_WITHOUT_FIELD) {
					 field = Constants.EMPTY_STRING;
				 }			
				 Utils.showAlert(this, error, field, this.getString(R.string.alert_title));
			}
	 }
	 
	 public void insertCupWithdraw(View view){
		 String error = null;
		 error = Utils.validateDoubleAndEmpty(this, amountText.getText().toString(), getString(R.string.withdrawal_amount));
		 if (null != error ){
	    		Utils.showAlert(this, "", error, this.getString(R.string.alert_title));
	    		return;
		 }
	    	
		 if(Double.parseDouble(amountText.getText().toString()) <= maxWithdrawAmount){
			 request = new FeeMethodRequest();
			 request.setTranTypeId(Constants.TRANS_TYPE_DELTAPAY_CHINA_WITHDRAW);
			 Utils.requestService(activity, responseH, "insertDeltaPayChinaWithdrawCallBack", request, new TransactionMethodResult(), "getFeeAmountByType");		
		 } else {
			 error = getString(R.string.error_maxwithdraw, maxWithdrawAmount);
			Utils.showAlert(this, "", error, this.getString(R.string.alert_title));
		 }
	 }
	 
	 public void insertDeltaPayChinaWithdrawCallBack(Object resultObj) throws UnsupportedEncodingException{		 
		Log.d(getPackageName(), "insertDeltaPayChinaWithdrawCallBack");
		TransactionMethodResult result = (TransactionMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(getLocalClassName(), "showImportantNotePopup");
			CcDepositMethodRequest ccRequest = new CcDepositMethodRequest();
			ccRequest.setAmount(((EditText) findViewById(R.id.amountToWithdraw)).getText().toString());
			String strFee = result.getFee();
			double dFee = Double.parseDouble(strFee);
			String formattedFee = Utils.formatCurrencyAmount(dFee, user.getCurrencySymbol(), user.isCurrencyLeftSymbol(), user.getCurrencyId());
			showImportantNotePopUp(SERIVCE_NAME, R.string.withdraw_cc_success, ccRequest, formattedFee);
		} else {
			String error = result.getUserMessages()[0].getMessage();
			 String field = result.getUserMessages()[0].getField();
			 if (result.getErrorCode() == Constants.ERROR_CODE_VALIDATION_WITHOUT_FIELD) {
				 field = Constants.EMPTY_STRING;
			 }			
			 Utils.showAlert(this, error, field, this.getString(R.string.alert_title));
		}
	 }

	@Override
	    public void onResume() {
	    	Log.v("event", "onResume");
			amountText.setVisibility(EditText.VISIBLE);
			submitButton.setVisibility(Button.VISIBLE);
			super.onResume();
	 }
}
