package com.anyoption.android.app;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.webkit.WebView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.CurrencyMethodRequest;
import com.anyoption.json.results.CurrencyMethodResult;

public class DomesticPaymentsActivity extends BaseActivity  {
	
	private static final String TAG = "DomesticPaymentsActivity";
	private final String MERCHANT_REFERENCE = "AYP";
	public static final int USER_ID_REFERENCE_DIGITS_NUMBER = 7;
	private final String PAGE_DIRECTION = "ltr";
	
    // for Envoy

	private String redirectUrl;
    private String merchantId;
    private String customerRef;
    private String language;
    private String amountStr;
    private String requestReference;
    private String xmlString;
    private String service;
    private String userOffset;
    private String currencyLetter;
    private String url;
    private WebView webView;
    private String userCurrencyCode;

	
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.domestic_payments);
		AnyoptionApplication ap = (AnyoptionApplication) getApplication();
		User user = ap.getUser();
		
		userCurrencyCode = getUserCurrencyCode(user);
		
		webView = (WebView) findViewById(R.id.webViewDomesticPayments);
		webView.getSettings().setJavaScriptEnabled(true);
	    webView.getSettings().setPluginsEnabled(true);
	    
	    responseH = new Handler();	    
	    CurrencyMethodRequest request = new CurrencyMethodRequest();
	    request.setCurrencyId(user.getCurrencyId());
		Utils.requestService(this, responseH, "getCurrencyLettersCallBack", request, new CurrencyMethodResult(), "getCurrencyLetters");
	    
	}
	
	
	private String getUserCurrencyCode(User user) {
		if(user != null && user.getCurrency() != null){
			return user.getCurrency().getNameKey();
		} else {
			Log.d(TAG, "Couldn't set customer's prefered currency. Setting default value - cur");
			return "cur";
		}
	}


	public String getReferencedUserId(){
		AnyoptionApplication ap = (AnyoptionApplication) getApplication();
		User user = ap.getUser();
		String userIdString = String.valueOf(user.getId());

		while (userIdString.length() < USER_ID_REFERENCE_DIGITS_NUMBER){
			userIdString = "0" + userIdString;
		}

		return userIdString;
	}
	
	
    public void getCurrencyLettersCallBack(Object result) {
    	Log.d(getPackageName(), "getCurrencyLettersCallBack");
    	CurrencyMethodResult res = (CurrencyMethodResult) result;
    	currencyLetter = res.getCurrencyLetters();
    	
    	url = 	" <html> " +
		" 			<head> " +
		" 				<meta HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html;charset=UTF-8\" /> " +
		" 				<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\" /> " +
		" 			</head> " +
		" 			<body onload=\"document.envoyDepositForm.submit();\" dir=\"ltr\"> " +
		" 				<form name=\"envoyDepositForm\" method=\"post\" action=\"#{envoyInfo.redirectUrl}\"> " +
		" 					<input name=\"merchantId\" type=\"hidden\" value=\"#{envoyInfo.merchantId}\"/> " +
		" 					<input name=\"country\" type=\"hidden\" value=\"#{envoyInfo.countryA2}\"/> " +
		" 					<input name=\"language\" type=\"hidden\" value=\"#{envoyInfo.language}\"/> " +
		" 					<input name=\"customerRef\" type=\"hidden\" value=\"#{envoyInfo.customerRef}\"/> " +
		"					<input name=\"receiveCurrency\" type=\"hidden\" value=\"#{envoyInfo.currencySymbol}\"/> " +
		" 					<input name=\"email\" type=\"hidden\" value=\"#{envoyInfo.email}\"/> " +
		" 					<input name=\"service\" type=\"hidden\" value=\"#{envoyInfo.service}\"/> " +
		" 					<input type=\"image\" src=\"#{applicationData.imagesPath}/#{applicationData.userLocale.language}_#{applicationData.skinId}/redirecting.jpg\" name=\"continue\" value=\"Continue\"></input> " +
		" 				</form> " +
		" 			</body> " +
		" 		</html>";	    	
 			

		/*if (ap.getIsLive()){
		    String redirectUrl = ONE_CLICK_URL_LIVE;
			merchantId = MERCHANT_ID_LIVE;
			log.debug("Live parameters marchent id = " + merchantId);
		}else {
			redirectUrl = ONE_CLICK_URL_TEST;
			merchantId = MERCHANT_ID_TEST;
			log.debug("Test parameters marchent id = " + merchantId);
		}*/
		
		
		
		AnyoptionApplication ap = (AnyoptionApplication) getApplication();
		User user = ap.getUser();
		String countryA2 = ((AnyoptionApplication)getApplication()).getCountries().get(user.getCountryId()).getA2();
		user.getLocale().toString();
		
		url = url.replace("#{applicationData.pageDirection}", PAGE_DIRECTION);
		url = url.replace("#{envoyInfo.redirectUrl}", Constants.ONE_CLICK_URL);
		url = url.replace("#{envoyInfo.merchantId}", Constants.MERCHANT_ID);		
		url = url.replace("#{envoyInfo.countryA2}", countryA2 );
		url = url.replace("#{envoyInfo.language}", user.getLocale().toString());
		url = url.replace("#{envoyInfo.customerRef}", MERCHANT_REFERENCE + userCurrencyCode + getReferencedUserId());
		url = url.replace("#{envoyInfo.currencySymbol}", currencyLetter);
		url = url.replace("#{envoyInfo.email}", user.getEmail());
		url = url.replace("#{envoyInfo.service}","");
		
		webView.loadData(url , "text/html", "utf-8");
    }
	
}
