/**
 * 
 */
package com.anyoption.android.app.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.protocol.HTTP;

import android.util.Log;

import com.google.gson.Gson;

/**
 * Communicate with our JSON service
 * 
 * @author KobiM
 *
 */
public class ServiceRequest {	
	private String method;
	private Object requestObj;
	private Object resultObj;
	
	public ServiceRequest(String method, Object requestObj, Object resultObj) {
		this.method = method;
		this.requestObj = requestObj;
		this.resultObj = resultObj;
	}
	
	public Object callMethod() {
        InputStream is = null;
        InputStreamReader isr = null;
        OutputStream os = null;
        
		System.setProperty("sun.net.client.defaultConnectTimeout", "30000");
        System.setProperty("sun.net.client.defaultReadTimeout", "30000");
        try {     
        	URL u = new URL(Constants.SERVICE_URL + method);
        	HttpURLConnection httpCon = (HttpURLConnection)u.openConnection();
            httpCon.setDoOutput(true);
            httpCon.setDoInput(true);
            httpCon.setRequestMethod("POST");                
            httpCon.setRequestProperty(HTTP.CONTENT_TYPE, "application/json; charset=UTF-8"); 
            
            Gson gson = new Gson();
            byte[] data = gson.toJson(requestObj).getBytes("UTF-8");                
            httpCon.setRequestProperty("Content-Length", String.valueOf(data.length));
            
            os = httpCon.getOutputStream();
            os.write(data, 0, data.length);
            os.flush();
                             
            /*
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(httpCon.getOutputStream()));
            Gson gson = new Gson();
            gson.toJson(mr, MethodRequest.class, writer);
            writer.close();
            */           
           
            int respCode = httpCon.getResponseCode();
            Log.d("ServiceCall: " + method + ", ResponseCode: ", String.valueOf(respCode));
            
            is = httpCon.getInputStream();
            isr = new InputStreamReader(is);
            resultObj = gson.fromJson(isr, resultObj.getClass());                            
        } catch (IOException ioe) {               
        	Log.e("Problem executing ServiceCall: " + method + " ", ioe.toString());
        } finally {               
            if (null != isr) {
                try {
                    isr.close();
                } catch (Exception e) {                        
                }
            }
            if (null != is) {
                try {
                    is.close();
                } catch (Exception e) {               
                }
            }
            if (null != os) {
            	try {
            		os.close();
            	}
            	catch (Exception e) {                		
            	}
            }                
        }
        return resultObj;
	}
	
}
