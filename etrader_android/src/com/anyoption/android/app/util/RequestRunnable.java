package com.anyoption.android.app.util;

import java.lang.reflect.Method;

import android.os.Handler;

import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.results.MethodResult;

public class RequestRunnable extends Thread {
	private MethodRequest request;
	private MethodResult result;
	private Object className;
	private Method responseMethod;
	private String requestMethod;
	private Handler handler;
	
	
	/**
	 * runnable class to call service 
	 * @param request MethodRequest
	 * @param result MethodResult
	 * @param className the class we want to send the response object to
	 * @param responseMethod the response method in the className we want to call
	 * @param requestMethod the request method in the service we want to call
	 * @param responseH 
	 */
	public RequestRunnable(MethodRequest request, MethodResult result, Object className, Method responseMethod, String requestMethod, Handler responseH) {
		this.request = request;
		this.result = result;
		this.className = className;
		this.responseMethod = responseMethod;
		this.requestMethod = requestMethod;
		this.handler = responseH;
	}
	
	public void run() {
        ServiceRequest call = new ServiceRequest(requestMethod, request, result);
        result = (MethodResult) call.callMethod();
    //    chartCallBack(result);
        Object[] parameters = new Object[1];
        parameters[0] = result;
        handler.post(new ResponseRunnable(parameters, className, responseMethod));

	}
}
