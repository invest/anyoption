package com.anyoption.android.app.util;

import com.anyoption.android.app.R;

/**
 * Constants class                  
 *
 * @author KobiM
 */
public class Constants {
	/**
	 * Set it to true if you want to build a Chinese only APK, false for a general APK
	 * Chinese APK only allows login for users with Chinese skin and disables language change
	 * 
	 * To select the target Chinese store change apkNameChinese in strings.xml
	 * to the desired value from MOBILE_VERSIONS table in the database
	 */
	public static final boolean IS_CHINESE_ONLY = false;
	
	//test/local env
//	//public static final String SERVICE_URL = "http://liors.testenv.anyoption.com/jsonService/AnyoptionService/";
//	public static final String SERVICE_URL = "http://www.testenv.anyoption.com/jsonService/AnyoptionService/";
////	public static final String SERVICE_URL = "http://www.testenv.etrader.co.il/jsonService/AnyoptionService/";	
//	public static final String LS_URL = "http://lstest.anyoption.com:80";      
////	public static final String LS_URL = "http://lstest.etrader.co.il:80";	
////	
//	public static final String MERCHANT_ID = "000203AYP";
//	public static final String ONE_CLICK_URL = "http://test.envoytransfers.com";	
//	public static final String WEB_SITE_LINK = "WEB_SITE_LINK_TEST";
	
	//live env
//	public static final String SERVICE_URL = "http://www.anyoption.com/jsonService/AnyoptionService/";
	public static final String SERVICE_URL = "http://www.etrader.co.il/jsonService/AnyoptionService/";	
	
//	public static final String LS_URL = "http://ls.anyoption.com:80"; 
	public static final String LS_URL = "http://ls.etrader.co.il:80";	
	public static final String MERCHANT_ID = "000266AYP";
	public static final String ONE_CLICK_URL = "https://www.envoytransfers.com";
	public static final String WEB_SITE_LINK = "WEB_SITE_LINK";
	
	public static final int ERROR_CODE_SUCCESS 					= 0;
	public static final int ERROR_CODE_VALIDATION_WITHOUT_FIELD = 208;
	public static final int ERROR_CODE_UNKNOWN          		= 999;
	public static final int ERROR_CODE_REGULATION_MISSING_QUESTIONNAIRE = 300;
	public static final int ERROR_CODE_REGULATION_SUSPENDED    = 301;
	public static final int ERROR_CODE_REGULATION_SUSPENDED_DOCUMENTS = 302;
	public static final int ERROR_CODE_QUESTIONNAIRE_NOT_FULL   = 400;
    public static final int ERROR_CODE_QUESTIONNAIRE_NOT_SUPPORTED  = 401;
    public static final int ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE  = 402;
	
	public static final String EMPTY_STRING = "";
	
	public static final String LS_ADAPTER = "REUTERSJMS";
	public static final String LS_DATA_ADAPTER = "JMS_ADAPTER";
    
	public static final int DIALOG_ALERT_ID = 1;
	public static final int DIALOG_DATE_ID = 2;
	
	public static final long COUNTRY_UNITED_STATES_ID = 220;
	public static final long COUNTRY_ISRAEL_ID = 1;
	public static final long COUNTRY_IRAN_ID = 99;
	public static final long COUNTRY_NORTH_KOREA_ID = 154;
	public static final long COUNTRY_SOUTH_KOREA_ID = 191;
	public static final long COUNTRY_CHINA_ID = 44;
	public static final long COUNTRY_GERMANY_ID = 80;
	
	public static final long MOBILE_WRITER_ID = 200;
	public static final String NO_ID_NUM = "No-Id";
	
	public static final String SHARED_PREF = "com.anyoption.android.app.anyoption.pref";
	public static final String PREF_FIRST_TIME_USER = "first time user";
	public static final String PREF_FIRST_APP_OPEN = "first app open";
	public static final String PREF_USER_NAME = "un";
	public static final String PREF_PASSWORD = "up";
	public static final String PREF_LOCALE = "locale_pref";	
	public static final String PREF_COMB_ID = "combId";
	public static final String PREF_DEVICE_REGISTRATION_ID = "deviceRegistrationID";
	public static final String PREF_SKIN_ID = "skinId";
	public static final String PREF_VERSION_CODE = "versionCode";
	public static final String PREF_TIME_FIRST_VISIT = "timeFirstVisit";
	public static final String PREF_LAST_ACTIVITY = "lastActivity";
	public static final String PREF_DYNAMIC_PARAM = "DynamicParam";
	public static final String PREF_MID = "mid";
	public static final String PREF_ETS_MID = "ets_mid";
	public static final String PREF_REFERRER_PARAM = "referrerParam";
	public static final String PREF_SHOW_QUESTIONNAIRE = "showQuestionnaireParam";
	
	//request code for every new intent!!
	public static final int REQUEST_CODE_LOGIN_PAGE = 1;
	public static final int REQUEST_CODE_MARKETS_GROUP_PAGE = 2;	
	public static final int REQUEST_CODE_ASSET_PAGE = 3;
	public static final int REQUEST_CODE_REGISTER = 4;
	public static final int REQUEST_CODE_HOMEPAGE = 5;
	public static final int REQUEST_CODE_PAYMENT_METHOD_POPUP = 6;
	public static final int REQUEST_CODE_GET_QUOTE = 7;
	public static final int REQUEST_CODE_BANKING = 8;
	public static final int REQUEST_CODE_NEW_CARD = 9;
	public static final int REQUEST_CODE_BONUS_DETAILS = 10;
	public static final int REQUEST_CODE_CC_DEPOSIT = 11;
	public static final int REQUEST_CODE_CHANGE_PERSONAL_DETAILS = 12;
	public static final int REQUEST_CODE_MY_ACCOUNT = 13;
	public static final int REQUEST_CODE_GENERAL_TERMS = 14;
	public static final int REQUEST_CODE_INBOX_CONTENT = 15;
	public static final int REQUEST_CODE_WEB_VIEW = 16;
	public static final int REQUEST_CODE_INSURANCES = 17;
	public static final int REQUEST_CODE_TERMS = 18;
	
	//strings for put in extra for new intent for example
	public static final String PUT_EXTRA_MARKET = "market";
    public static final String PUT_EXTRA_CC_LIST = "ccList";
	public static final String PUT_EXTRA_UPDATE_SKIN = "updateSkin";
	public static final String PUT_EXTRA_BANKING_MORE_INFO_ID = "moreInfo";
	public static final String PUT_EXTRA_BANKING_PAYMENT = "payment";
	public static final String PUT_EXTRA_BANKING_TYPE = "bankingType";
	public static final String PUT_EXTRA_BANKING_HEADER_IMAGE_ID = "imageId";
	public static final String PUT_EXTRA_BANKING_SUB_HEADER = "subHeader";
	public static final String PUT_EXTRA_KEY_WEBVIEW = "webview";
	public static final String PUT_EXTRA_GENERAL_TERMS = "General";
	public static final String PUT_EXTRA_GENERAL_AGREEMENT = "agreement";
	public static final String PUT_EXTRA_ABOUTUS = "AboutUs";
	public static final String PUT_EXTRA_SECURITY = "Security";
	public static final String PUT_EXTRA_PRIVACY = "Privacy";
	public static final String PUT_EXTRA_INSURANCES_TYPE = "insurancesType";
	public static final String PUT_EXTRA_INSURANCES_CLOSE_TIME = "insurancesCloseTime";
	public static final String PUT_EXTRA_INSURANCES_COUNT = "insurancesCount";
	public static final String PUT_EXTRA_INFO_PAGE = "infoLayoutId";
	public static final String PUT_EXTRA_LAYOUT_BANKING = "layoutBanking";
	public static final String PUT_EXTRA_SHOW_LOG_OUT_ALERT = "showLogOutAlert";
	public static final Object PUT_EXTRA_RISK_DISCLOSURE = "RiskDisclosure";
	public static final String PUT_EXTRA_COUNTRY_ID = "country_id";
	
	public static final long MARKET_GROUP_OPTION_PLUS_ID = 99;
		
	public static final int PAYMENT_TYPE_DEPOSIT = 1;
	public static final int PAYMENT_TYPE_WITHDRAW = 2;
	
	public static final int MARKET_GROUP_ID_TA = 3;
	
	public static final int AMEX_CARD_TYPE = 5;
	
	//bonus
	public static final long BONUS_STATE_GRANTED_ACTIVE = 4;
	public static final long BONUS_STATE_ACTIVE = 2;
	public static final long BONUS_STATE_GRANTED = 1;
	public static final long BONUS_STATE_CANCELED = 7;
	
	//transactions 
	public static final int TRANS_STATUS_REQUESTED = 4;
	public static final int TRANS_STATUS_APPROVED = 9;	
	
	//first pop up language code
	public static final int EN_LANG = R.id.textEnglish;
	public static final String EN_LANG_CODE = "en";
	
	//result from activity
	public static final String RESULT_USER_OBJECT = "User";
	public static final String RESULT_BONUS = "Bonus";
	public static final String RESULT_UPDATES_ARRAY = "updates";
	public static final String RESULT_UPDATES_DELETE = "delete";
	
	//menu type
	public static final String MENU_TYPE_TRADING = "TRADING";
	public static final String MENU_TYPE_BANKING = "BANKING";
	public static final String MENU_TYPE_MY_ACCOUNT = "MY ACCOUNT";
	public static final String MENU_TYPE_FAQ_TYPE_TECHNICAL = "TECHNICAL";
	public static final String MENU_TYPE_OTHER = "OTHER";
	
	public static final String MOBILE_VERSIONS_ANDROID_APK = "android apk";
	
	public static final String ETRADER_PACKAGE = "com.etrader.android.app.";
	public static final String ANYOPTION_PACKAGE = "com.anyoption.android.app.";
	
	public static final String ACTIVITY_REDIRECT_REGISTER = "RegisterActivity";
	public static final String ACTIVITY_REDIRECT_NEWCARD = "NewCardActivity";
	public static final String ACTIVITY_REDIRECT_CHANGE_PERSONAL = "ChangePersonalDetailsActivity";
	public static final String ACTIVITY_REDIRECT_FORGOT = "ForgotPasswordActivity";
	
	public static final int SKIN_ET = 1;
	public static final String ACTIVITY_BEFORE_TERMS = "activityBeforeTerms";
	public static final int CURRENCY_YUAN = 7;
	public static final int CURRENCY_WON = 8;
	public static final long TRANS_TYPE_DELTAPAY_CHINA_WITHDRAW = 39;
	public static final long TRANS_TYPE_BARO_PAY = 45;
	public static final int NUM_DIGITS_AMOUNT_KR = 9;
	
	public static final String ACTIVITY_BEFORE_QUESTIONNAIRE = "activityBeforeQuestionnaire";
	

	public static final int FROM_DEPOSIT = 1;
	public static final int FROM_LOGIN = 2;
	public static final int FROM_INVESTING = 3;
	public static final int FROM_MYACCOUNT = 4;
	public static final int MARKET_BITCOIN_ID = 638;
	
	public static final int SUSPENDED_NONE = 0;
	public static final int SUSPENDED_QUESTIONNAIRE = 1;
	public static final int SUSPENDED_DOCUMENTS = 2;
	
	public static final long BANK_WIRE_OTHER_BANK = 40;

}