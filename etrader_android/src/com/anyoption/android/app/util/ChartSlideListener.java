package com.anyoption.android.app.util;

import com.anyoption.android.app.HomePageChartView;
import com.anyoption.android.app.PageControl;
import com.anyoption.android.app.PageControlListener;
import com.anyoption.android.app.util.ViewFlipperAnimationHelper.AnimationHelper;

import android.app.Activity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ViewFlipper;

public abstract class ChartSlideListener implements View.OnTouchListener, PageControlListener {
    private static final int DRAG_THRESHOLD = 10;
    
    private float startX;
    private float downX;
    private float downY;
    private boolean dragStarted;
    private HomePageChartView chart;
    private Activity activity;
    private ViewFlipper viewFlipper;
    private PageControl viewFlipperControl;
    
    public ChartSlideListener(Activity activity, HomePageChartView chart, ViewFlipper viewFlipper, PageControl pageControl) {
        this.activity = activity;
        this.chart = chart;
        this.viewFlipper = viewFlipper;
        this.viewFlipperControl = pageControl;
    }
    
    public boolean onTouch(View v, MotionEvent event) {
        Log.v("event", "onTouchEvent - x: " + event.getX() + " y: " + event.getY() + " action: " + event.getAction());

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (null != chart && (chart.isHitCall(event.getX(), event.getY()) || chart.isHitPut(event.getX(), event.getY()))) {
                Log.v("event", "Hit CALL or PUT.");
                return false; // let the chart onTouchEvent handle that
            }

            startX = viewFlipper.getCurrentView().getLeft();
            downX = event.getX();
            downY = event.getY();
            dragStarted = false;
        }
        
        float xmove = downX - event.getX();
        float ymove = downY - event.getY();
        if ((Math.abs(xmove) > DRAG_THRESHOLD || Math.abs(ymove) > DRAG_THRESHOLD) && Math.abs(xmove) < Math.abs(ymove) && !dragStarted) {
            return true;
        }
        
        if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
            if (Math.abs(xmove) <= DRAG_THRESHOLD && Math.abs(ymove) <= DRAG_THRESHOLD && !dragStarted) {
                Log.v("event", "Touch the chart.");
                if (null != chart) {
                    openAssetPage(chart.getMarketId());
                } else {
                    openAssetPage(0); // Demo page. No need market to go to trading page
                }
                return false;
            }
            
            if (dragStarted) {
                if (xmove < 0) {
                    Log.v("event", "showPrevious");
                    viewFlipperControl.showPrevious();
                    showPrevious();
                } else {
                    Log.v("event", "showNext");
                    viewFlipperControl.showNext();
                    showNext();
                }
            }
        }
        if ((Math.abs(xmove) > DRAG_THRESHOLD || dragStarted) && (event.getAction() == MotionEvent.ACTION_MOVE || event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)) {
            Log.v("event", "Drag the chart.");
            dragStarted = true;
            int width = activity.getWindowManager().getDefaultDisplay().getWidth();
            int crrIndex = viewFlipper.getDisplayedChild();
            View currentView = viewFlipper.getCurrentView();
            currentView.layout((int) (startX + event.getX() - downX), currentView.getTop(), (int) (startX + event.getX() - downX) + currentView.getWidth(), currentView.getBottom());
            int space = (width - currentView.getWidth()) /  2;
            int rightIndex = crrIndex == viewFlipper.getChildCount() - 1 ? 0 : crrIndex + 1;
            View rightView = viewFlipper.getChildAt(rightIndex);
            int leftIndex = crrIndex == 0 ? viewFlipper.getChildCount() - 1 : crrIndex - 1;
            View leftView = viewFlipper.getChildAt(leftIndex);
            if (event.getX() <= downX) {
                leftView.setVisibility(View.GONE); // cover the case when you have drag first in the other direction
                if (rightView.getVisibility() == View.GONE) {
                    rightView.setVisibility(View.INVISIBLE);
                } else /*if (rightView.getLeft() < 0)*/ { // layout from first pass done so you don't see blink of the view over the current
                    rightView.setVisibility(View.VISIBLE);
                }
                rightView.layout(
                        currentView.getRight() + space,
                        rightView.getTop(),
                        currentView.getRight() + space + rightView.getWidth(),
                        rightView.getBottom());
            } else if (event.getX() > downX) {
                rightView.setVisibility(View.GONE); // cover the case when you have drag first in the other direction
                if (leftView.getVisibility() == View.GONE) {
                    leftView.setVisibility(View.INVISIBLE);
                } else /*if (leftView.getLeft() > width)*/ { // layout from first pass done so you don't see blink of the view over the current
                    leftView.setVisibility(View.VISIBLE);
                }
                leftView.layout(
                        currentView.getLeft() - space - leftView.getWidth(),
                        leftView.getTop(),
                        currentView.getLeft() - space,
                        leftView.getBottom());
            }
        }
        if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
            dragStarted = false;
        }
        return true;
    }
    
    public void showPrevious() {
        Log.v("slide", "showPrevious");
        int crrIndex = viewFlipper.getDisplayedChild();
        int leftIndex = crrIndex == 0 ? viewFlipper.getChildCount() - 1 : crrIndex - 1;
        View leftView = viewFlipper.getChildAt(leftIndex);
        viewFlipper.setInAnimation(AnimationHelper.inFromLeftAnimation(leftView.getLeft() - startX, 0));
        viewFlipper.setOutAnimation(AnimationHelper.outToRightAnimation(0, -leftView.getLeft() + startX));
        viewFlipper.showPrevious();
        if (null != chart) {
            getChartData();
        }
    }
    
    public void showNext() {
        Log.v("slide", "showNext");
        int crrIndex = viewFlipper.getDisplayedChild();
        int rightIndex = crrIndex == viewFlipper.getChildCount() - 1 ? 0 : crrIndex + 1;
        View rightView = viewFlipper.getChildAt(rightIndex);
        viewFlipper.setInAnimation(AnimationHelper.inFromRightAnimation(rightView.getLeft() - startX, 0));
        viewFlipper.setOutAnimation(AnimationHelper.outToLeftAnimation(0, -rightView.getLeft() + startX));
        viewFlipper.showNext();
        if (null != chart) {
            getChartData();
        }
    }
    
    public abstract void getChartData();
    public abstract void openAssetPage(long marketId);

    public boolean isDragStarted() {
        return dragStarted;
    }

    public void setDragStarted(boolean dragStarted) {
        this.dragStarted = dragStarted;
    }
}