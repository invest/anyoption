package com.anyoption.android.app.util;

import android.graphics.drawable.Drawable;
import android.view.View;

public class SetBackgroundColorRunnable implements Runnable {
    private View view;
    private Drawable color;
    private int colorHex;
    
    public SetBackgroundColorRunnable(View view, Drawable color) {
        this.view = view;
        this.color = color;
    }
    
    public SetBackgroundColorRunnable(View view, int color) {
        this.view = view;
        this.colorHex = color;
    }
    
    public void run() {
    	if (null != color) {
    		view.setBackgroundDrawable(color);
    	} else {
    		view.setBackgroundColor(colorHex);
    	}
    }
}