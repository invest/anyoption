/*
 * Copyright 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anyoption.android.app.util;

import com.anyoption.json.requests.CheckUpdateMethodRequest;
import com.anyoption.json.results.MethodResult;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.util.Log;

public class DeviceRegistrar 
{
    public static final String STATUS_EXTRA = "Status";
    public static final int REGISTERED_STATUS = 1;
    public static final int AUTH_ERROR_STATUS = 2;
    public static final int UNREGISTERED_STATUS = 3;
    public static final int ERROR_STATUS = 4;

    private static final String TAG = "DeviceRegistrar";
    public static final String SENDER_ID = "sk@anyoption.com";
    public static final String SENDER_NAME = "anyoption";

    public static void registerWithServer(final Context context, final String deviceRegistrationID) {
        new Thread(new Runnable() {
            public void run() {
                //Intent updateUIIntent = new Intent("com.google.ctp.UPDATE_UI");
                try {
                	MethodResult res = makeRequest(context, deviceRegistrationID);
                	if (res.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
                        SharedPreferences settings = context.getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(Constants.PREF_DEVICE_REGISTRATION_ID, deviceRegistrationID);
                        editor.commit();
                    } else {
                        Log.w(TAG, "Registration error " + res.getErrorCode());
                    }
                } catch (Exception e) {
                    Log.w(TAG, "Registration error " + e.getMessage());
                    
                }
            }
        }).start();
    }

    public static void unregisterWithServer(final Context context, final String deviceRegistrationID) {
        new Thread(new Runnable() {
            public void run() {
                try {
                	MethodResult res = makeRequest(context, null);
                    if (res.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
                        Log.w(TAG, "Unregistration error " + res.getErrorCode());
                    }
                } catch (Exception e) {
                    Log.w(TAG, "Unregistration error " + e.getMessage());
                } finally {
                    SharedPreferences settings = context.getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.remove(Constants.PREF_DEVICE_REGISTRATION_ID);
                    editor.remove("accountName");
                    editor.commit();
                }
            }
        }).start();
    }

    private static MethodResult makeRequest(Context context, String deviceRegistrationID) throws Exception {
    	Log.d("C2DM", "Sending registration ID to my application server");
        CheckUpdateMethodRequest request = new CheckUpdateMethodRequest();
        request.setDeviceId(Utils.getUDID(context));
        request.setC2dmRegistrationId(deviceRegistrationID);
        ServiceRequest call = new ServiceRequest("c2dmUpdate", request, new MethodResult());
        return (MethodResult) call.callMethod();	
    }
}
