package com.anyoption.android.app.util;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.anyoption.android.app.AnyoptionApplication;
import com.anyoption.android.app.AssetPageActivity;
import com.anyoption.android.app.LightstreamerConnectionHandler;
import com.anyoption.android.app.LoginActivity;
import com.anyoption.android.app.MarketsGroupActivity;
import com.anyoption.android.app.R;
import com.anyoption.android.app.questionnaire.QuestionnaireUtils;
import com.anyoption.beans.base.Investment;
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.requests.UserMethodRequest;
import com.anyoption.json.results.MethodResult;
import com.anyoption.json.results.OptionsMethodResult;
import com.lightstreamer.ls_client.ExtendedTableInfo;
import com.lightstreamer.ls_client.HandyTableListener;
import com.lightstreamer.ls_client.SubscrException;

public class Utils {
	public final static String TAG = "Utils";
	
	private static Date serverTimeOnUpdate;
    private static long serverTimeOffset;
	
	public static String getStringFromResource(Context context, String value, String[] params){
		int valueId = context.getResources().getIdentifier(value, "string", context.getPackageName());
		if (valueId > 0) {
			return String.format(context.getString(valueId), params);
		} else {
			return null;
		}
	}
	
	public static int getImageFromResource(Context context, String value){
		int valueId = context.getResources().getIdentifier(value, "drawable", context.getPackageName());			
		return valueId;		
	}
	
	public static int getLayoutFromResource(Context context, String value){
		int valueId = context.getResources().getIdentifier(value, "layout", context.getPackageName());			
		return valueId;		
	}
	
	public static int getIdFromResource(Context context, String value){
		int valueId = context.getResources().getIdentifier(value, "id", context.getPackageName());			
		return valueId;		
	}
	
	/**
	 * show alert with error/success msg
	 * @param context
	 * @param field if its empty("") we will show only the errormsg
	 * @param errorMsg the msg to show
	 */
	public static AlertDialog showAlert(final Context context, String field, String errorMsg, String titleMsg){
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		if (!field.equals(Constants.EMPTY_STRING)){
			alertDialog.setMessage(field + " " + errorMsg);
		} else {
			alertDialog.setMessage(errorMsg);
		}
		alertDialog.setTitle(titleMsg);
		alertDialog.setButton(context.getString(R.string.alert_button), new DialogInterface.OnClickListener() {   
		    public void onClick(DialogInterface dialog, int which) {  
		    	dialog.dismiss();		    	
		    }});   
		alertDialog.show();
		if (Utils.isEtraderProject(context)) {
			TextView messageView = (TextView)alertDialog.findViewById(android.R.id.message);
			messageView.setGravity(Gravity.CENTER);
		}
		return alertDialog;
    }
	
	
	/**
	 * show alert with error msg and when dismiss go to login
	 * @param context
	 * @param field if its empty("") we will show only the errormsg
	 * @param errorMsg the msg to show
	 */
	public static void showAlertAndGoLogin(final Context context, String field, String errorMsg){
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		if (!field.equals(Constants.EMPTY_STRING)){
			alertDialog.setMessage(field + " " + errorMsg);
		} else {
			alertDialog.setMessage(errorMsg);
		}
		alertDialog.setTitle(context.getString(R.string.alert_title_success));
		alertDialog.setButton(context.getString(R.string.alert_button), new DialogInterface.OnClickListener() {   
		    public void onClick(DialogInterface dialog, int which) {  
		    	dialog.dismiss();
		    	((Activity) context).startActivityForResult(
		  			  new Intent(context,
		  			      LoginActivity.class),
		  			      Constants.REQUEST_CODE_LOGIN_PAGE);
		    }});   
		alertDialog.show();
    }
	
	
    /**
     * Show Custom alert
     * @param Context context
     * @param View alertView the view that you like to show as alert
     * @param Button closeDialogButton the button the should close the popup
     * @return void show the popup
     */
    public static AlertDialog showCustomAlert(Context context, View alertView, Button closeDialogButton) {
    	return showCustomAlert(context, alertView, closeDialogButton, null);
    }
    
    /**
	 * Show Custom alert
	 * @param Context context
	 * @param View alertView the view that you like to show as alert
	 * @param Button closeDialogButton the button the should close the popup
	 * @param dismissListener
	 * @return void show the popup
     */
    public static AlertDialog showCustomAlert(Context context, View alertView, Button closeDialogButton, DialogInterface.OnDismissListener dismissListener) {
    	return showCustomAlert(context, alertView, closeDialogButton, dismissListener, null);
	}
    
    /**
	 * Show Custom alert
	 * @param Context context
	 * @param View alertView the view that you like to show as alert
	 * @param Button closeDialogButton the button the should close the popup
	 * @param dismissListener
	 * @param titleView
	 * @return void show the popup
     */
    public static AlertDialog showCustomAlert(Context context, View alertView, Button closeDialogButton, DialogInterface.OnDismissListener dismissListener, View titleView) {
    	return showCustomAlert(context, alertView, closeDialogButton, dismissListener, titleView, 0);
    }
	public static AlertDialog showCustomAlert(Context context, View alertView, Button closeDialogButton, DialogInterface.OnDismissListener dismissListener, View titleView, int moveToTop) {
//        final Dialog dialog = new Dialog(context);
//        dialog.setContentView(alertView);
//        dialog.setCancelable(true);
//        if (null != closeDialogButton) {
//	        closeDialogButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dialog.dismiss();
//				}
//			});
//        }
//        if (null != dismissListener) {
//            dialog.setOnDismissListener(dismissListener);
//        }
//        dialog.show();
        AlertDialog.Builder builder;
        final AlertDialog alertDialog;            
        builder = new AlertDialog.Builder(context);
        builder.setView(alertView);
        if (null != titleView) {
        	builder.setCustomTitle(titleView);
        }
        alertDialog = builder.create();
        alertDialog.setCancelable(true);
        if (moveToTop > 0) {
        	alertDialog.getWindow().getAttributes().y = -1 * moveToTop;
        }
        if (null != closeDialogButton) {
	        closeDialogButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					alertDialog.dismiss();
				}
			});
        }
        if (null != dismissListener) {
        	alertDialog.setOnDismissListener(dismissListener);
        }
        alertDialog.show();	 
        return alertDialog;
    }

	public static void showInsertInvestmentSuccessAlert(Context context, Investment inv, boolean currencyLeftSymbol, String currencySymbol, DialogInterface.OnDismissListener dismissListener) {
        LinearLayout llMain = null;
        LinearLayout llMainTitle = null;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        llMain = (LinearLayout)inflater.inflate(R.layout.market_details, null);
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int orientation = display.getOrientation();
        if (orientation == Surface.ROTATION_90 || orientation == Surface.ROTATION_270) {
        	llMainTitle = (LinearLayout) inflater.inflate(R.layout.market_details_title, null);
        }
        
        TextView name = (TextView)llMain.findViewById(R.id.textViewMarketName);
        if (null != llMainTitle) {
        	name = (TextView)llMainTitle.findViewById(R.id.textViewMarketName);
        }
        TextView id = (TextView)llMain.findViewById(R.id.textViewInvestmentId);
        TextView type = (TextView)llMain.findViewById(R.id.textViewInvestmentType);
        TextView level = (TextView)llMain.findViewById(R.id.textViewInvestmentLevel);
        TextView expiry = (TextView)llMain.findViewById(R.id.textViewInvestmentExpiryTime);
        TextView investment = (TextView)llMain.findViewById(R.id.textViewInvestment);
        TextView expAbove = (TextView)llMain.findViewById(R.id.textViewInvestmentAbove);
        TextView expBelow = (TextView)llMain.findViewById(R.id.textViewInvestmentBelow);
        
        name.setText(inv.getAsset());
        id.setText("#" + inv.getId());
        
        if (inv.getTypeId() == Investment.INVESTMENT_TYPE_CALL ||
                (inv.getTypeId() == Investment.INVESTMENT_TYPE_ONE &&
                 inv.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_CALL )) {
            type.setText(context.getString(R.string.investments_call));
        }
        if (inv.getTypeId() == Investment.INVESTMENT_TYPE_PUT ||
                (inv.getTypeId() == Investment.INVESTMENT_TYPE_ONE &&
                 inv.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_PUT  )) {
            type.setText(context.getString(R.string.investments_put));
        }
        
        if (inv.getOptionPlusFee() > 0) {
        	if (null == llMainTitle) {
        		llMain.findViewById(R.id.imageViewPlusIcon).setVisibility(View.VISIBLE);
        	} else {
        		llMainTitle.findViewById(R.id.imageViewPlusBig).setVisibility(View.VISIBLE);
        	}
        }
        level.setText(inv.getLevel());
//        if (null == llMainTitle) {
//        	level.setText(inv.getLevel());
//        } else {
//        	type.setText(type.getText() +  " " + inv.getLevel());
//        }
        
        expiry.setText(inv.getTimeEstClosingTxt());
        investment.setText(inv.getAmountTxt());
        
//      String amount = Utils.formatCurrencyAmount(inv.getAmount());
        int decPntDgts = Integer.valueOf(((AnyoptionApplication)((Activity)context).getApplication()).getUser().getCurrency().getDecimalPointDigits());
        BigDecimal amount = new BigDecimal(inv.getAmount() - inv.getInsuranceAmountRU() - inv.getOptionPlusFee()).divide(new BigDecimal("100")).setScale(2, RoundingMode.HALF_UP);
        Utils.updateWinLose(amount, expAbove, expBelow, 1 + inv.getOddsWin(), 1 - inv.getOddsLose(), currencyLeftSymbol, currencySymbol, decPntDgts);

        Button closeButton  = (Button)llMain.findViewById(R.id.buttonAssetPopUp);
        Utils.showCustomAlert(context, llMain, closeButton, dismissListener, llMainTitle);
	}
    
    public static void updateWinLose(BigDecimal amount, TextView winTextView, TextView loseTextView, double winOdds, double loseOdds, boolean currencyLeftSymbol, String currencySymbol, int decPntDgts) {
        Log.d(TAG + " updatewinLose", "winOdds: " + winOdds + " loseOdds: " + loseOdds + " amount: " + amount.toString());
        StringBuilder pattern = new StringBuilder("###,###,##0");
        if (decPntDgts > 0) {
        	pattern.append('.');
        	for (int i = 0; i < decPntDgts; i++) {
        		pattern.append('0');
        	}
        }
        
        // Explicitly setting the separators
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
        formatSymbols.setDecimalSeparator('.');
        formatSymbols.setGroupingSeparator(',');
        
        DecimalFormat sd = new DecimalFormat(pattern.toString(), formatSymbols);
    //  if (skinId == 4 || !isLeftCurrencySymbol) {
            String str = sd.format(amount.multiply(new BigDecimal(winOdds)).setScale(decPntDgts, RoundingMode.HALF_UP).doubleValue());
            winTextView.setText(currencyLeftSymbol ? currencySymbol + str : str + currencySymbol);
            str = sd.format(amount.multiply(new BigDecimal(loseOdds)).setScale(decPntDgts, RoundingMode.HALF_UP).doubleValue());
            loseTextView.setText(currencyLeftSymbol ? currencySymbol + str : str + currencySymbol);
/*      } else {
            document.getElementById(id + "_win").innerHTML = currencySymbol + decRound(amt * winOdds, 2);
            document.getElementById(id + "_lose").innerHTML = currencySymbol + decRound(amt * loseOdds, 2);
        }*/
    }
			
	public static String isFieldEmptyCD(Context context, String value, String field){
		return isFieldEmptyCommon(context, value, field, R.string.error_must_be_entered);
	}
	
	public static String isFieldEmpty(Context context, String value, String field){
		return isFieldEmptyCommon(context, value, field, R.string.error_single_required);
	}
	
	private static String isFieldEmptyCommon(Context context, String value, String field, int errorTextId){
		if (value.equalsIgnoreCase("") || value.length() == 0){
			String[] params = new String[1];
			params[0] = field;
			return String.format(context.getString(errorTextId), params);			
		}
		return null;
	}
	
	public static String minimumLenthValidator(Context context, String value, String field, int length){
		if (value.equalsIgnoreCase("") || value.length() < length) {
			String[] params = new String[2];
			params[0] = field;
			params[1] = String.valueOf(length);
			return context.getString(R.string.javax_faces_validator_LengthValidator_MINIMUM, field, String.valueOf(length));			
		}
		
		return null;
	}
	
	public static String validateEqualPass(Context context, String value1, String value2) {
		if (!value2.equals(value1)) {
			return context.getString(R.string.org_apache_myfaces_equal_invalid_detail);			
		}
		return null;
	}
	
	public static String validateLettersAndNumbersOnly(Context context, String value, String field) {		
		String pattern = context.getString(R.string.general_validate_letters_and_numbers);
		if (!value.matches(pattern)) {
			String[] params = new String[1];
			params[0] = field;
			return String.format(context.getString(R.string.error_lettersandnumbers_only), params);
		}
		return null;
	}
	
	public static String validateIdNum(Context context, String idNum) {
		String error = null;
		error = Utils.isFieldEmpty(context, idNum, context.getString(R.string.register_id_num));
		if (error != null){		
			return error;
		} 
		
		try {
			long id = Long.parseLong(idNum);
			if (id < 1000) {
				return context.getString(R.string.error_id_num);
			}
		} catch (NumberFormatException e) {
			return context.getString(R.string.error_id_num);
		}

		if (idNum.length() < 9) {
			return context.getString(R.string.error_id_num);
		}

		int idNum1 = Integer.parseInt(idNum.substring(0, 1)) * 1;
		int idNum2 = Integer.parseInt(idNum.substring(1, 2)) * 2;
		int idNum3 = Integer.parseInt(idNum.substring(2, 3)) * 1;
		int idNum4 = Integer.parseInt(idNum.substring(3, 4)) * 2;
		int idNum5 = Integer.parseInt(idNum.substring(4, 5)) * 1;
		int idNum6 = Integer.parseInt(idNum.substring(5, 6)) * 2;
		int idNum7 = Integer.parseInt(idNum.substring(6, 7)) * 1;
		int idNum8 = Integer.parseInt(idNum.substring(7, 8)) * 2;
		int idNum9 = Integer.parseInt(idNum.substring(8, 9)) * 1;

		if (idNum1 > 9) {
			idNum1 = (idNum1 % 10) + 1;
        }
		if (idNum2 > 9) {
			idNum2 = (idNum2 % 10) + 1;
        }
		if (idNum3 > 9) {
			idNum3 = (idNum3 % 10) + 1;
        }
		if (idNum4 > 9) {
			idNum4 = (idNum4 % 10) + 1;
        }
		if (idNum5 > 9) {
			idNum5 = (idNum5 % 10) + 1;
        }
		if (idNum6 > 9) {
			idNum6 = (idNum6 % 10) + 1;
        }
		if (idNum7 > 9) {
			idNum7 = (idNum7 % 10) + 1;
        }
		if (idNum8 > 9) {
			idNum8 = (idNum8 % 10) + 1;
        }
		if (idNum9 > 9) {
			idNum9 = (idNum9 % 10) + 1;
        }

		int sumval = idNum1 + idNum2 + idNum3 + idNum4 + idNum5 + idNum6 + idNum7 + idNum8 + idNum9;

		sumval = sumval % 10;

		if (sumval > 0) {
			return context.getString(R.string.error_id_num);
		}
		return null;
	}
	
	public static String validateStreet(Context context, String value) {
		String pattern = context.getString(R.string.general_validate_street);
		if (!value.matches(pattern) || value.trim().equals("")) {
			return context.getString(R.string.error_street);			 
		}
		return null;
	}
		
	public static String validateLettersOnly(Context context, String value, String field) {
		String pattern = context.getString(R.string.general_validate_letters_only);		
		if (!value.matches(pattern) || value.trim().equals("")) {
			String[] params = new String[1];
			params[0] = field;
			return String.format(context.getString(R.string.error_letters_only), params);
		}
		return null;
	}
	
	public static String validateDigitsOnly(Context context, String value, String field) {
		String pattern = context.getString(R.string.general_validate_digits);		
		if (!value.matches(pattern) || value.trim().equals("")) {
			String[] params = new String[1];
			params[0] = field;
			return String.format(context.getString(R.string.javax_faces_validator_LongRangeValidator_TYPE), params);
		}
		return null;
	}

	public static String validateEnglishLettersOnly(Context context, String value) {
		String pattern = context.getString(R.string.general_validate_english_letters_only);				
		if (!value.matches(pattern) || value.trim().equals("")) {
			return context.getString(R.string.error_english_letters_only);
		}
		return null;
	}
	
	public static String validateStreetNo(Context context, String value) {	
		String pattern = context.getString(R.string.general_validate_streetnum);			
		if (!value.matches(pattern)) {
			return context.getString(R.string.error_streetnum);			
		}
		return null;
	}
	
	public static String validateEmail(Context context, String value) {	
		String pattern = context.getString(R.string.general_validate_email);			
		if (!value.matches(pattern)) {
			return context.getString(R.string.error_banner_contactme_email);						
		}
		return null;
	}
	
	public static String validateUserName(Context context, String value){
		String error = null;
		error = isFieldEmpty(context, value, context.getString(R.string.register_username));
		if (error != null){
			return error;
		} 
		error = validateLettersAndNumbersOnly(context, value, context.getString(R.string.register_username));
		if (error != null){
			return error;
		}
		if (value.length() < 6){
			return context.getString(R.string.error_username_ajaxerror);
		}
		return null;
	}
	
	/**
	 * validate password for change datiles
	 * @param context
	 * @param value
	 * @return
	 */
	public static String validatePasswordCD(Context context, String value) {
		String error = null;
		error = isFieldEmptyCD(context, value, context.getString(R.string.register_password));
		if (error != null){
			return error;
		} 
		error = validatePasswordCommon(context, value);
		if (error != null){
			return error;
		}
		return null;
	}
	
	public static String validatePassword(Context context, String value) {
		String error = null;
		error = isFieldEmpty(context, value, context.getString(R.string.register_password));
		if (error != null){
			return error;
		} 
		error = validatePasswordCommon(context, value);
		if (error != null){
			return error;
		}
		return null;
	}
	
	private static String validatePasswordCommon(Context context, String value) {
		String error = null;
		error = validateLettersAndNumbersOnly(context, value, context.getString(R.string.register_password));
		if (error != null){
			return error;
		}
		if (value.length() < 6) {			
			return String.format(context.getString(R.string.error_password_smallsize), context.getString(R.string.register_password));
		}	
		return null;
	}
	
	public static String validateRetypePassword(Context context, String value1, String value2){
		String error = validatePassword(context, value2);
		if (error == null){
			error = validateEqualPass(context, value1, value2);
		}
		return error;
	}
	
	public static String validateEmailAndEmpty(Context context, String value, String field){
		String error = null;
		error = isFieldEmpty(context, value, field);
		if (error != null){
			return error;
		} 
		error = validateEmail(context, value);		
		return error;
	}
	
	public static String validateName(Context context, String value, String field) {
		String pattern = context.getString(R.string.general_validate_name);		
		if (!value.matches(pattern) || value.trim().equals("")) {
			String[] params = new String[1];
			params[0] = field;
			return String.format(context.getString(R.string.error_letters_only), params);
		}
		return null;
	}
	
	public static String validateNameAndEmpty(Context context, String value, String field){
		String error = null;
		error = isFieldEmpty(context, value, field);
		if (error != null){
			return error;
		} 
		error = validateName(context, value, field);		
		return error;
	}
		
	public static String validateEnglishLettersOnlyAndEmpty(Context context, String value, String field){
		String error = null;
		error = isFieldEmpty(context, value, field);
		if (error != null){
			return error;
		} 
		error = validateEnglishLettersOnly(context, value);		
		return error;
	}
	
	public static String validateStreetAndEmpty(Context context, String value, String field){
		String error = null;
		error = isFieldEmpty(context, value, field);
		if (error != null){
			return error;
		} 
		error = validateStreet(context, value);		
		return error;	
	}
	
	public static String validateLettersOnlyAndEmpty(Context context, String value, String field) {
		String error = null;
		error = isFieldEmpty(context, value, field);
		if (error != null){
			return error;
		} 
		error = validateLettersOnly(context, value, field);		
		return error;
	}

	public static String validateDigitsOnlyAndEmpty(Context context, String value, String field) {
		String error = null;
		error = isFieldEmpty(context, value, field);
		if (error != null){
			return error;
		} 
		error = validateDigitsOnly(context, value, field);		
		return error;
	}
	
	public static String validateDigitsOnlyAndEmptyAndLength(Context context, String value, String field) {
		String error = null;
		error = validateDigitsOnlyAndEmpty(context, value, field);
		if (error != null){
			return error;
		} 
		error = minimumLenthValidator(context, value, field, 7);		
		return error;
	}
	
	
	/**
	 * show progress bar with waiting... 
	 * @param context the activity we came from
	 * @param progDailog
	 * @return running progress bar
	 */
	public static ProgressDialog showPrograssBar(Context context, ProgressDialog progDailog) {
		progDailog = new ProgressDialog(context);
		progDailog.setTitle(context.getString(R.string.connecting));
		progDailog.setMessage(context.getString(R.string.please_wait));
		progDailog.show();
		progDailog.setCancelable(false);
		return progDailog;
	}
	
	/**
	 * check if there is Internet connection
	 * @param context the activity we came from
	 * @return true we have Internet false we don't have
	 */
	public static boolean isHaveInternet(Context context) {
		ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() != null
				&& cm.getActiveNetworkInfo().isAvailable()
				&& cm.getActiveNetworkInfo().isConnected()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Calling service requests
	 * @param classObject the class we call the service from
	 * @param responseH the handler to return the response to
	 * @param callBackFunction name of the call back function in the class we call service from
	 * @param request MethodRequest
	 * @param result MethodResult
	 * @param responseMethod the response method in the className we want to call
	 * @param requestMethod the request method in the service we want to call
	 * @return true if the request was sent, false if we had error;
	 */
	public static boolean requestService(Object classObject, Handler responseH, String callBackFunction, MethodRequest request, MethodResult result, String requestMethod) {
		return requestService(classObject, responseH, callBackFunction, request, result, requestMethod, (Activity)classObject);
	}
	
	/**
	 * Calling service requests
	 * @param classObject the class we call the service from
	 * @param responseH the handler to return the response to
	 * @param callBackFunction name of the call back function in the class we call service from
	 * @param request MethodRequest
	 * @param result MethodResult
	 * @param responseMethod the response method in the className we want to call
	 * @param requestMethod the request method in the service we want to call
	 * @param activity the activity we came from (need to take the user info)
	 * @return true if the request was sent, false if we had error;
	 */
	public static boolean requestService(final Object classObject, final Handler responseH, final String callBackFunction, final MethodRequest request, final MethodResult result, final String requestMethod, final Activity activity){
		return requestService(classObject, responseH, callBackFunction, request, result, requestMethod, activity, true);
	}
	
	/**
	 * Calling service requests
	 * @param classObject the class we call the service from
	 * @param responseH the handler to return the response to
	 * @param callBackFunction name of the call back function in the class we call service from
	 * @param request MethodRequest
	 * @param result MethodResult
	 * @param responseMethod the response method in the className we want to call
	 * @param requestMethod the request method in the service we want to call
	 * @param activity the activity we came from (need to take the user info)
	 * @param toShowDialog indicates if the progress dialog should be shown
	 * @return true if the request was sent, false if we had error;
	 */
	public static boolean requestService(final Object classObject, final Handler responseH, final String callBackFunction, final MethodRequest request, final MethodResult result, final String requestMethod, final Activity activity, boolean toShowDialog) {
		try {
			//check Internet connection
			ConnectivityManager conStatus = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        	if (null != conStatus
        	        && (null == conStatus.getActiveNetworkInfo()
        	                || !conStatus.getActiveNetworkInfo().isConnected())) {
//        		if (AnyoptionApplication.HAVE_INTERNET_CONNECTION) {
	    			AnyoptionApplication.HAVE_INTERNET_CONNECTION = false;
	                AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
	                alertDialog.setTitle(activity.getString(R.string.alert_title));
	                alertDialog.setMessage(activity.getString(R.string.noConnectionMsg));
	                DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface dialog, int which) {
	                        if (which == DialogInterface.BUTTON_POSITIVE) {
	                            dialog.dismiss();
	                            requestService(classObject, responseH, callBackFunction, request, result, requestMethod, activity);
//	                        } else {
//	                            activity.moveTaskToBack(true);
	                        }
	                    }};
	                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, activity.getString(R.string.button_no_internet_again), listener);
//	                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, activity.getString(R.string.alert_button), listener);
	                alertDialog.show();
//        		}
    			return false;
        	}
        	request.setDeviceId(getUDID(activity));
        	request.setWriterId(Constants.MOBILE_WRITER_ID);
        	AnyoptionApplication.HAVE_INTERNET_CONNECTION = true;
			AnyoptionApplication ap = (AnyoptionApplication) activity.getApplication();
			request.setSkinId(ap.getSkinId());
			Log.d(TAG, "set skin id: " + request.getSkinId());
			Date date = new Date();        
			request.setUtcOffset(date.getTimezoneOffset());
			Log.d(TAG, "set utc offset: " + request.getUtcOffset());
			if (null != ap.getUser() && request instanceof UserMethodRequest) {
			    UserMethodRequest umr = (UserMethodRequest) request;
				SharedPreferences sp = activity.getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
				String userName = sp.getString(Constants.PREF_USER_NAME, null);
				if (null != userName) {
				    umr.setUserName(userName);
				}
				
				if (null == umr.getPassword() || umr.getPassword().equals("")) { // in case i am login and want to check password again(e.g change personal details
				    umr.setPassword(sp.getString(Constants.PREF_PASSWORD, null));
				    umr.setEncrypt(true);
				}
				Log.d(TAG, requestMethod + " set username: " + userName + " set password: " + umr.getPassword());
			} else {
			    Log.v(TAG, requestMethod + " no user for");
			}
			Method method = classObject.getClass().getMethod(callBackFunction, Object.class);
			//responseH.post(new RequestRunnable(request, result, classObject, method, requestMethod));
//			new RequestRunnable(request, result, classObject, method, requestMethod, responseH).start();
			RequestServiceTask.startServiceRequest(activity, responseH, requestMethod, classObject, method, request, result.getClass(), toShowDialog);
		} catch (Exception e) {
			Log.d(TAG, "Cant complete service request for " + requestMethod, e);
			return false;
		}
		return true;
	}
	
	public static String formatCurrencyAmount(double amount, String currencySymbol, boolean currencyLeftSymbol, long currencyId) {
        DecimalFormat sd = new DecimalFormat("#,##0.00");
        if (currencyId == Constants.CURRENCY_WON) {
        	sd = new DecimalFormat("#,##0");
        }
        amount /= 100;
        String out = sd.format(amount);
        return currencyLeftSymbol ? currencySymbol + out : out + currencySymbol;
	}
	
	public static String timeToSecondsFormat(long millsecond) {
        SimpleDateFormat sd = new SimpleDateFormat("ss.SSS");
        String out = sd.format(millsecond).substring(0, 5);
        return out;
    }
	
	public static String timeToMinutesAndSecondsFormat(long millsecond) {
        SimpleDateFormat sd = new SimpleDateFormat("mm.ss");
        String out = sd.format(millsecond);
        return out;
    }
	
	 /**
    *
    * @param d
    * @return date in format: dd/MM/yyyy HH:mm:ss
    */
   public static String getDateAndTimeFormat(Date d, String timeZoneName) {
		if (d == null) {
			return "";
		}
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		TimeZone tz = null;
       if (null != timeZoneName) {
            tz = TimeZone.getTimeZone(timeZoneName);
       }
		if (null != tz) {
           sd.setTimeZone(tz);
      }
		String out = sd.format(d);
		return out;
	}
   
   /**
   *
   * @param d
   * @return date in format: dd/MM/yyyy
   */
  public static String getDateFormat(Date d, String timeZoneName) {
		if (d == null) {
			return "";
		}
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone tz = null;
      if (null != timeZoneName) {
           tz = TimeZone.getTimeZone(timeZoneName);
      }
		if (null != tz) {
          sd.setTimeZone(tz);
     }
		String out = sd.format(d);
		return out;
  }
   
   public static String reformatTimeShort(String timeStr) throws ParseException {
       Date time = parseTimeFromLSUpdate(timeStr);
       SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
       return sdf.format(time);
   }
   
   public static String reformatTimeLong(Context context, String timeStr) throws ParseException {
       Date time = parseTimeFromLSUpdate(timeStr);
       Calendar now = Calendar.getInstance();
       Calendar timeCal = Calendar.getInstance();
       timeCal.setTime(time);
       if (now.get(Calendar.YEAR) == timeCal.get(Calendar.YEAR) && now.get(Calendar.DAY_OF_YEAR) == timeCal.get(Calendar.DAY_OF_YEAR)) {
           SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
           return context.getResources().getString(R.string.time_today, sdf.format(time));
       } else {
           SimpleDateFormat sdf = new SimpleDateFormat("HH:mm, dd.MM.yy");
           return sdf.format(time);
       }
   }
   
	public static Date parseTimeFromLSUpdate(String timeStr) throws ParseException {
       SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.US);
       sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
       return sdf.parse(timeStr);
   }
   
   public static double getInvestmentReturnAt(Investment inv, double currentLevel) {
       if ((inv.getTypeId() == Investment.INVESTMENT_TYPE_CALL && currentLevel > inv.getCurrentLevel()) ||
               (inv.getTypeId() == Investment.INVESTMENT_TYPE_PUT && currentLevel < inv.getCurrentLevel())) { // win
           return (inv.getAmount() - inv.getInsuranceAmountRU() - inv.getOptionPlusFee()) * (1 + inv.getOddsWin());
       } else if ((inv.getTypeId() == Investment.INVESTMENT_TYPE_CALL && currentLevel < inv.getCurrentLevel()) ||
                    (inv.getTypeId() == Investment.INVESTMENT_TYPE_PUT && currentLevel > inv.getCurrentLevel())) { // lose
           return (inv.getAmount() - inv.getInsuranceAmountRU() - inv.getOptionPlusFee()) * (1 - inv.getOddsLose());
       } else { // tie left
           return inv.getAmount() - inv.getInsuranceAmountRU() - inv.getOptionPlusFee();
       }
   }
   
   public static void gotToAssetPage(Activity activity, Market market) {
	   Intent intent = new Intent(activity, AssetPageActivity.class);
	   intent.putExtra(Constants.PUT_EXTRA_MARKET, market);
	   activity.startActivityForResult(intent, Constants.REQUEST_CODE_ASSET_PAGE);
   }
   
   /**
    * get credit card years array YY from app or Initialize if not set yet
    * @return string array of years YY
    */
   public static String[] getCreditCardYearsArray(Context context) {
	   AnyoptionApplication ap = (AnyoptionApplication) context.getApplicationContext();
	   if (null != ap.getCreditCardYears()) {
		   return ap.getCreditCardYears();
	   }
	   //not in app need to Initialize
	   GregorianCalendar g = new GregorianCalendar();
	   int currentYear = g.get(GregorianCalendar.YEAR);
	   String[] creditCardYears = new String[22];
	   creditCardYears[0] = context.getString(R.string.mobile_card_exp_year);
	   //Extend year selection for CC electron
	   for (int i = currentYear; i <= currentYear + 20; i++) {
		   String yearStr = String.valueOf(i);
		   yearStr = yearStr.substring(yearStr.length() - 2); // taking the 2 last numbers from the year number
		   Log.d(TAG, "i - currentYear = " + (i - currentYear));
		   creditCardYears[i - currentYear + 1] = yearStr;
	   }
	   ap.setCreditCardYears(creditCardYears);
	   return creditCardYears;
   }
   
   public static String validateCVV(Context context, String cvv, long cardTypeValue) {
	   String error = validateDigitsOnlyAndEmpty(context, cvv, context.getString(R.string.creditcards_ccPass));
	   if (null == error) {
		   if (cvv.length() != 3) {
			   error = context.getString(R.string.error_cvvLength);
		   }		   
	   }
	   return error;
   }

   public static String validateCCExpDate(Context context,
			int year, int month) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, 1);
	
		Calendar expCal = Calendar.getInstance();
		expCal.clear();
		
		expCal.set(year + 2000, month, 1);
		if (cal.after(expCal)) {
			return context.getString(R.string.error_creditcard_expdateError);
		}
		return null;
   }
   
	public static String validateDouble(Context context, String value, String field) {		
//		String pattern = context.getString(R.string.general_validate_digits_double);
		Object[] formatArgs = new Integer[1];
		formatArgs[0] = Integer.valueOf(((AnyoptionApplication)((Activity)context).getApplication()).getUser().getCurrency().getDecimalPointDigits());
		String pattern = context.getString(R.string.general_validate_input_field, formatArgs);
		if (!value.matches(pattern) || value.trim().equals("")) {
			String[] params = new String[1];
			params[0] = field;
			return String.format(context.getString(R.string.javax_faces_validator_DoubleRangeValidator_TYPE), params);
		}
		return null;
	}
	
	public static String validateDoubleAndEmpty(Context context, String value, String field){
		String error = null;
		error = isFieldEmpty(context, value, field);
		if (error != null){
			return error;
		} 
		error = validateDouble(context, value, field);		
		return error;
	}
		
	

	/**
	 * Show Custom alert dialog - Without header (Helps to remove the space on the top of the alert)
	 * @param context
	 * @param alertView
	 * @param closeDialogButton
	 * @param dismissListener
	 */
	public static void showCustomAlertDialog(Context context, View alertView, 
			Button closeDialogButton, DialogInterface.OnDismissListener dismissListener) {       
        AlertDialog.Builder builder;
        final AlertDialog alertDialog;            
        builder = new AlertDialog.Builder(context);
        builder.setView(alertView);
        alertDialog = builder.create();
        alertDialog.setCancelable(true);
        if (null != closeDialogButton) {
	        closeDialogButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					alertDialog.dismiss();
				}
			});
        }
        if (null != dismissListener) {
        	alertDialog.setOnDismissListener(dismissListener);
        }
        alertDialog.show();	       
    } 

	public static ExtendedTableInfo addLightstreamerTable(Activity activity, String[] items, LightstreamerConnectionHandler ls, HandyTableListener listener, double maxFrequency) {
	    ExtendedTableInfo ti = null;
        try {
        	AnyoptionApplication application = ((AnyoptionApplication) activity.getApplication());
            long skinId = application.getSkinId();
            SkinGroup skinGroup = application.getSkins().get(skinId).getSkinGroup();
            
            ti = new ExtendedTableInfo(items, "COMMAND", new String[] {"key", "command", "ET_NAME", skinGroup.getLevelUpdateKey(), "ET_ODDS", "ET_EST_CLOSE", "ET_OPP_ID", "ET_ODDS_WIN", "ET_ODDS_LOSE", "ET_STATE", skinGroup.getColorUpdateKey(), "ET_PRIORITY", "ET_GROUP_CLOSE", "ET_SCHEDULED", "ET_RND_FLOOR", "ET_RND_CEILING", "ET_GROUP", "ET_SUSPENDED_MESSAGE", "AO_HP_MARKETS_" + skinId, "AO_STATES", "FILTER_SCHEDULED", "ET_LAST_INV", "AO_TS", "AO_FLAGS"}, true);
            ti.setDataAdapter(Constants.LS_DATA_ADAPTER);
            ti.setRequestedMaxFrequency(maxFrequency);
            Log.v("lightstreamer", "RequestedMaxFrequency: " + ti.getRequestedMaxFrequency() + " UnfilteredDispatching:" + ti.isUnfilteredDispatching());
        } catch (SubscrException e) {
            Log.e("lightstreamer", "Error creating LS table.", e);
        }
        
        ls.addTable(ti, listener);
	    return ti;
	}
	
	
	public static void showNotLoginAlert(String errorMsg, final Context context){
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		LinearLayout popup = (LinearLayout) inflater.inflate(R.layout.not_login_pop_up, null, false);
		TextView errorTextView = ((TextView) popup.findViewById(R.id.textViewError));
		if (null != errorMsg) {
			errorTextView.setText(errorMsg);
		} else {
			errorTextView.setVisibility(View.GONE);
		}
		Button openAccountButton = (Button) popup.findViewById(R.id.buttonMyAccountPersonalDetailsPopupClose);
		Button loginButton = (Button) popup.findViewById(R.id.buttonCallUsPopupSubmit);
		
		openAccountButton.setText(context.getString(R.string.menuStrip_openaccount));
		final AlertDialog ad = showCustomAlert((Activity) context, popup, null);
		openAccountButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Class<Activity> actvityRedirect = getActivityClass(Constants.ACTIVITY_REDIRECT_REGISTER, context);
		        Intent intent = new Intent(context, actvityRedirect);
		        ((Activity) context).startActivityForResult(intent, Constants.REQUEST_CODE_REGISTER);
		    	ad.dismiss();
			}
		});
		
		loginButton.setText(context.getString(R.string.header_login));
		loginButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent =  new Intent(context, LoginActivity.class);
			    context.startActivity(intent);
			    ad.dismiss();
			}
		});
	}
	
	/**
	 * Get combination id if we have it, else return null.
	 * 
	 * @param context
	 * @return combination id or null if we don't have.
	 */
	public static Long getCombinationId(Context context){
		 SharedPreferences sp = context.getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
		 String combId = sp.getString(Constants.PREF_COMB_ID, null);
		 if (null != combId) {
			 try {
				 Log.d(TAG, "get combId = " + combId);
				return Long.parseLong(combId);
			} catch (NumberFormatException e) {
				Log.w(TAG, "Cant get combId " + combId, e);
			}
		 }
		 Log.d(TAG, "get combId = " + combId);
		 return null;
	}
	
	/**
	 * get dp if we have it else return null
	 * @param context
	 * @return dp or null if we dont have
	 */
	public static String getDynamicParam(Context context){
		 SharedPreferences sp = context.getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
		 Log.d(TAG, "get dp = " + sp.getString(Constants.PREF_DYNAMIC_PARAM, null));
		 return sp.getString(Constants.PREF_DYNAMIC_PARAM, null);
	}
	
	/**
	 * get Referrer if we have it else return null
	 * @param context
	 * @return Referrer or null if we don't have
	 */
	public static String getReferrerParam(Context context){
		 SharedPreferences sp = context.getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
		 return sp.getString(Constants.PREF_REFERRER_PARAM, null);
	}	
	
	/**
	 * get MID if we have it else return null
	 * @param context
	 * @return MID or null if we don't have
	 */
	public static String getmId(Context context){
		 SharedPreferences sp = context.getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
		 Log.d(TAG, "get mid = " + sp.getString(Constants.PREF_MID, null));
		 return sp.getString(Constants.PREF_MID, null);
	}	
	
	public static void setmIdIfValid(Context context, String newmId){
		if(newmId != null && newmId.length() > 0) {		
			SharedPreferences sp = context.getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
			Editor edit = sp.edit();
			edit.putString(Constants.PREF_MID, newmId);
			edit.commit();
		}
	}	
	
	/**
	 * Gets EtsMId value.
	 * 
	 * @param context
	 * @return EtsMId or null if we don't have it saved.
	 */
	public static String getEtsMId(Context context) {
		SharedPreferences sp = context.getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
		Log.d(TAG, "get etsmid = " + sp.getString(Constants.PREF_ETS_MID, null));
		return sp.getString(Constants.PREF_ETS_MID, null);
	}

	/**
	 * Sets the new EtsMId value if it is valid.
	 * 
	 * @param context
	 * @param newEtsMId the value to set.
	 */
	public static void setEtsMId(Context context, String newEtsMId) {
		if (newEtsMId != null && newEtsMId.length() > 0) {
			SharedPreferences sp = context.getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
			Editor edit = sp.edit();
			edit.putString(Constants.PREF_ETS_MID, newEtsMId);
			edit.commit();
		}
	}
	
	/**
	 * get url string of values and keys and return them in map
	 * @param urlString url query string 
	 * @return map with the keys and values
	 */
	public static Map<String, String> parseUrlToStringPairs(String urlString) {
		Map<String, String> urlMapping = new HashMap<String, String>();
		
		String[] params = urlString.split("&");
	    for (String param : params) {
	    	String[] pair = param.split("=");
	    	if(pair.length == 2) {
	    		urlMapping.put(pair[0], pair[1]);
	    	}
	    }       
		return urlMapping;
	}
	
	/**
	 * get the appliction version code
	 * @param context
	 * @return version code of the app
	 */
	public static int getAppVersionCode(Context context) {
		int version = 1;
        try {
	        PackageManager manager = context.getPackageManager();
	        PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
	        version = info.versionCode;
        } catch (Exception e) {
        	Log.e(TAG, "Error getting version sending 1 as defualt");
        }
        return version;
	}

	public static void setServerTimeOffset(long serverTime) {
	    serverTimeOffset = serverTime - System.currentTimeMillis();
	    Log.d("anyoption", "serverTimeOffset: " + serverTimeOffset);
	}
	
	public static void fixTime(Date d) {
	    d.setTime(d.getTime() + serverTimeOffset);
	}
	
	public static Date getCurrentServerTime() {
	    Date d = new Date();
	    fixTime(d);
	    return d;
	}

	/**
	 * Use this method to set the time got from the server on the last update.
	 * 
	 * @param serverTime time in milliseconds.
	 * @see #getServerTimeOnUpdate()
	 */
	public static void setServerTimeOnUpdate(long serverTime) {
		serverTimeOnUpdate = new Date();
		serverTimeOnUpdate.setTime(serverTime);
	}
	
	/**
	 * Returns the time on the server during the last update.
	 * 
	 * @see #setServerTimeOnUpdate(long)
	 */
	public static Date getServerTimeOnUpdate() {
		if(serverTimeOnUpdate == null) {
			serverTimeOnUpdate = new Date(0);
		}
		return serverTimeOnUpdate;
	}
    
    public static Date getNextRefreshTime() {
        Calendar c = Calendar.getInstance();
        int m = c.get(Calendar.MINUTE);
        if (m >= 1 && m < 16) {
            c.set(Calendar.MINUTE, 16);
        } else if (m >= 16 && m < 31) {
            c.set(Calendar.MINUTE, 31);
        } else if (m >= 31 && m < 46) {
            c.set(Calendar.MINUTE, 46);
        } else {
            if (m != 0) {
                c.add(Calendar.HOUR, 1);
            }
            c.set(Calendar.MINUTE, 1);
        }
        c.set(Calendar.SECOND, 0);
        Log.v("Utils", "Next refresh time: " + c.getTime());
        return c.getTime();
    }
    
    public static String stringArrayToString(String[] arr) {
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        for (int i = 0; i < arr.length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(arr[i]);
        }
        sb.append("]");
        return sb.toString();
    }
    
    public static boolean isEtraderProject(Context context) {
    	return context.getPackageName().toLowerCase().contains("etrader");
    }
    
    public static Class<Activity> getActivityClass(String className, Context context) {
    	Class<Activity> activityClass = null;
    	if (isEtraderProject(context)) {
	    	try {
	    		activityClass = (Class<Activity>) Class.forName(Constants.ETRADER_PACKAGE + className);
	    	} catch (Exception e) {
				Log.d("getActivityClass",Constants.ETRADER_PACKAGE + className + " not found in Etrader Package");
			}
    	} else {
	    	try {
	    		activityClass = (Class<Activity>) Class.forName(Constants.ANYOPTION_PACKAGE + className);
	    	} catch (Exception e) {
				Log.d("getActivityClass",Constants.ANYOPTION_PACKAGE + className + " not found in Anyoption Package");
			}
    	}
    	return activityClass;
    }
    
    public static void fillSpinner(Spinner spinner, Object result, Context context) {
		OptionsMethodResult omr = (OptionsMethodResult) result;
		ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(context,
				android.R.layout.simple_spinner_item, omr.getOptions());
		spinnerArrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spinner.setAdapter(spinnerArrayAdapter);
	}
    
    static public String md5(String toConvert) {
        String retVal = "";      
        MessageDigest algorithm;
        try {
            algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(toConvert.getBytes());
            byte messageDigest[] = algorithm.digest();
            
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            }
            
            retVal = hexString + "";
        } catch (NoSuchAlgorithmException e) {
            Log.d(TAG, "cant md5 string " + toConvert);
        }
        
        return retVal;
    }
    
    static public String getUDID(Context c) {
        // Requires READ_PHONE_STATE
        TelephonyManager tm =
          (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
        
        // gets the imei (GSM) or MEID/ESN (CDMA)
        String imei = tm.getDeviceId();
        if (null != imei) {
        	return imei;
        }
        
        // Get some of the hardware information
        String buildParams = Build.BOARD + Build.BRAND + Build.CPU_ABI
            + Build.DEVICE + Build.DISPLAY + Build.FINGERPRINT + Build.HOST
            + Build.ID + Build.MANUFACTURER + Build.MODEL + Build.PRODUCT
            + Build.TAGS + Build.TYPE + Build.USER;

        // gets the android-assigned id
        String androidId = Secure.getString(c.getContentResolver(),
                           Secure.ANDROID_ID);

        // requires ACCESS_WIFI_STATE
        WifiManager wm =
            (WifiManager) c.getSystemService(Context.WIFI_SERVICE);

        // gets the MAC address
        String mac = wm.getConnectionInfo().getMacAddress();

        // concatenate the string
        String fullHash = buildParams + imei + androidId + mac;

        return md5(fullHash);
    }
    
    public static boolean isShowBaroPay(User user) {
    	// Will never be shown.
    	/*try {
	    	if (user.getCountryId() == Constants.COUNTRY_SOUTH_KOREA_ID || 
					   user.getCurrencyId() == Constants.CURRENCY_WON ||
					   user.getSkinId() == Skin.SKIN_KOREAN) {
	    		return true;
	    	}    		
    	} catch (Exception e) {
    		Log.e(TAG, "Error while checking showBaroPay");
		}*/
    	return false;
    }
    public static boolean isRegulated(Context context){
    	if(Utils.isEtraderProject(context)){
    		return false;
    	} else {
	    	AnyoptionApplication ap = (AnyoptionApplication) context.getApplicationContext();
			long skinId = ap.getSkinId();
	        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
	        dataBaseHelper.openDataBase();
			boolean isRegulated = dataBaseHelper.isSkinRegulated(skinId);
			dataBaseHelper.close();
			
			return isRegulated;
    	}
    }
    /**
	 * Returns the regulated version of the provided skin id
	 * 
	 * @param  notRegulatedSkinId not regulated skin id
	 * @return The corresponding regulated skin id. Returns -1 if no skin found.
	 */
    public static long getAlternativeRegulatedSkin(Context context, long notRegulatedSkinId){
	        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
			dataBaseHelper .openDataBase();
			long regulatedSkinId = dataBaseHelper.getRegulatedAlternative(notRegulatedSkinId);
			dataBaseHelper.close();
			
			return regulatedSkinId;
    }
    
    public static boolean isAfterFirstDeposit(Context context){
		int regulationStep = getUserRegulation(context).getApprovedRegulationStep();
		return regulationStep>=UserRegulationBase.REGULATION_FIRST_DEPOSIT;
    }
    public static boolean isSuspended(Context context){
    	return getUserRegulation(context).isSuspended();
    }
    
    public static boolean isSuspendedForDocuments(Context context){
		return getUserRegulation(context).isSuspendedDueDocuments();
    }

	protected static UserRegulationBase getUserRegulation(Context context) {
		AnyoptionApplication app = (AnyoptionApplication) context.getApplicationContext();
    	UserRegulationBase userRegulation = app.getUserRegulation();
		return userRegulation;
	}
     
	public static void showSuspendedForDocumentsPopoup(Context context){
		AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context).getApplication();
		String resText = context.getString(R.string.suspended_fordocuments, ap.getUser().getFirstName());
		Spanned htmlText = Html.fromHtml(resText);
		showSuspendedPopup(context, htmlText, false);
	}
	
	public static void showGeneralSuspendedPopup(final Context context){
		showGeneralSuspendedPopup(context, false);
	}
		
	
	public static void showGeneralSuspendedPopup(final Context context, boolean toFinishActivity ){
		AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context).getApplication();
		String resText = context.getString(R.string.suspended_forquestionnaire, ap.getUser().getFirstName(),context.getString(R.string.suspended_support_email),getSupportPhone(context));
		CharSequence htmlText = Html.fromHtml(resText);
		SpannableStringBuilder clickableText = makeUnderlineClickable(htmlText, new ClickableSpan() {
			
			@Override
			public void onClick(View widget) {
				Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
				emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{context.getString(R.string.suspended_support_email)});
				emailIntent.setType("plain/text");
				context.startActivity(Intent.createChooser(emailIntent, ""));
				
			}
		});
		showSuspendedPopup(context, clickableText, toFinishActivity);
	}
	private static void showSuspendedPopup(final Context context, Spanned htmlText, final boolean toFinishActivity) {
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		LinearLayout popup = (LinearLayout) inflater.inflate(
				R.layout.questionnaire_thankyou_popup, null);

		final AlertDialog dialog = Utils.showCustomAlert(context, popup, null);
		TextView message = (TextView) dialog.findViewById(R.id.textViewMsg);
		message.setText(htmlText);
		message.setMovementMethod(LinkMovementMethod.getInstance());

		Button buttonNotNow = (Button) dialog.findViewById(R.id.buttonFillQuestionnaire);
		buttonNotNow.setText(context.getString(R.string.alert_button));
		buttonNotNow.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				dialog.dismiss();
				if(toFinishActivity){
					openTradingPage(context);
					((Activity) context).finish();
				}
			}
		});

		Button buttonOpenQuestionnaire = (Button) dialog
				.findViewById(R.id.buttonNotNow);
		buttonOpenQuestionnaire.setVisibility(View.GONE);
	}
	public static void openTradingPage(final Context context) {
		Intent intent = new Intent(context, MarketsGroupActivity.class);
		intent.putExtra(Constants.PUT_EXTRA_SHOW_LOG_OUT_ALERT, true);

		((Activity) context).startActivityForResult(intent,
				Constants.REQUEST_CODE_MARKETS_GROUP_PAGE);
		((Activity) context).finish();
	}
	
	public static String getSupportPhone(Context context){
		AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context).getApplication();
		DataBaseHelper helper = new DataBaseHelper(context);
		String supportPhone = null;
		helper.openDataBase();
		Cursor phoneCursor = helper.getSupportPhone(ap.getSkinId());
		if(phoneCursor.getCount()>0){
			phoneCursor.moveToFirst();
			supportPhone = phoneCursor.getString(0);
		}
		helper.close();
		return supportPhone;
	}
	
	public static long getCorrespondingNotRegulatedSkinId(Context context){
		AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context).getApplication();
		DataBaseHelper helper = new DataBaseHelper(context);
		long notRegulatedSkinId = 0;
		helper.openDataBase();
		Cursor cursor = helper.getCorrespondingNotRegulatedSkinId(ap.getSkinId());
		if(cursor.getCount()>0){
			cursor.moveToFirst();
			notRegulatedSkinId = cursor.getLong(0);
		}
		helper.close();
		return notRegulatedSkinId;
	}
	
	public static SpannableStringBuilder makeUnderlineClickable(CharSequence sequence, ClickableSpan clickableSpan){
		return makeUnderlineClickable(sequence, clickableSpan, 0);
	}
		
	public static SpannableStringBuilder makeUnderlineClickable(CharSequence sequence, ClickableSpan clickableSpan, int color) {
		SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
	     UnderlineSpan[] underlines = strBuilder.getSpans(0,sequence.length(),UnderlineSpan.class);
	     for(UnderlineSpan span : underlines) {
	        int start = strBuilder.getSpanStart(span);
	        int end = strBuilder.getSpanEnd(span);
	        int flags = strBuilder.getSpanFlags(span);
	        strBuilder.setSpan(clickableSpan, start, end, flags);
	        if(color != 0){
	        	strBuilder.setSpan(new ForegroundColorSpan(color), start, end, flags);
	        }
	     }
		return strBuilder;
	}
	
	public static void handleResultError(Context context, MethodResult result) {
		String errorMsg;
		
		switch(result.getErrorCode()){
    	case Constants.ERROR_CODE_REGULATION_MISSING_QUESTIONNAIRE:
    		QuestionnaireUtils.showFillQuestionnairePopup(context);
    		break;
    		
    	case Constants.ERROR_CODE_REGULATION_SUSPENDED_DOCUMENTS:
    		Utils.showSuspendedForDocumentsPopoup(context);
    		break;
    		
    	case Constants.ERROR_CODE_REGULATION_SUSPENDED:
    		Utils.showGeneralSuspendedPopup(context);
    		break;
    	
    	case Constants.ERROR_CODE_UNKNOWN:
    		errorMsg = context.getString(R.string.error_investment);
    		Utils.showAlert(context, "", errorMsg, context.getString(R.string.alert_title));
    		break;
    		
    	default:
        	errorMsg = result.getUserMessages()[0].getMessage();
        	Utils.showAlert(context, "", errorMsg, context.getString(R.string.alert_title));
        	break;
    	}
	}
	
	/**
	 * Returns the regulated countries from the database. Can be used only for Anyoption application.
	 * 
	 * @param context
	 * @return array of the regulated countries or null if this is Etrader project.
	 */
	public static Country[] getRegulatedCountries(Context context) {
		if(isEtraderProject(context)) {
			// In Etrader database is_regulated column is not added to countries. 
			return null;
		}
		
		AnyoptionApplication ap = (AnyoptionApplication)((Activity)context).getApplication();
		HashMap<Long, Country> countriesMap = ap.getCountries();
		
        DataBaseHelper db = null;
        Cursor c = null;
        try {
            db = new DataBaseHelper(context);
            db.openDataBase();
            
            c = db.getRegulatedCountriesIds();
            Country[] countries = new Country[c.getCount()];
            int i = 0;
            
            while (c.moveToNext()) {
            	countries[i] = countriesMap.get(c.getLong(c.getColumnIndex("id")));
            	i++;
            }
            
            return countries;
	    } catch (Exception e) {
	        Log.e(TAG, "Problem with DB during regulated countries loading!", e);
	    } finally {
	        if (null != c) {
	            c.close();
	        }
	        if (null != db) {
	            db.close();
	        }
	    }
		
		return new Country[0];
	}
	
	/**
	 * Returns the non-regulated countries from the database. Can be used only for Anyoption application.
	 * 
	 * @param context
	 * @return array of the non-regulated countries or null if this is Etrader project.
	 */
	public static Country[] getNonRegulatedCountries(Context context) {
		if(isEtraderProject(context)) {
			// In Etrader database is_regulated column is not added to countries. 
			return null;
		}
		
		AnyoptionApplication ap = (AnyoptionApplication)((Activity)context).getApplication();
		HashMap<Long, Country> countriesMap = ap.getCountries();
		
        DataBaseHelper db = null;
        Cursor c = null;
        try {
            db = new DataBaseHelper(context);
            db.openDataBase();
            
            c = db.getNonRegulatedCountriesIds();
            ArrayList<Country> nonRegCountries = new ArrayList<Country>();
            while (c.moveToNext()) {
            	Country country = countriesMap.get(c.getLong(c.getColumnIndex("id")));
            	if(country != null) {
            		nonRegCountries.add(country);
            	}
            }
            
            // Used to get countries sorted.
            Country[] allCountriesArr = ap.getCountriesArray();
            // Array of sorted non-regulated countries.
            Country[] sortedCountries = new Country[nonRegCountries.size()];
            
            int i = 0;
            for(Country cnt: allCountriesArr) {
            	if(nonRegCountries.contains(cnt)) {
            		sortedCountries[i] = cnt;
            		i++;
            	}
            }
            
            return sortedCountries;
	    } catch (Exception e) {
	        Log.e(TAG, "Problem with DB during non-regulated countries loading!", e);
	    } finally {
	        if (null != c) {
	            c.close();
	        }
	        if (null != db) {
	            db.close();
	        }
	    }
		
		return new Country[0];
	}

	/**
	 * 
	 * @param context
	 * @param value
	 * @param field
	 * @return error as string (if exist)
	 */
	public static String validateChineseLettersOnly(Context context, String value, String field) {
		String pattern = context.getString(R.string.general_validate_chinese_letters_only);				
		if (!value.matches(pattern) || value.trim().equals("")) {
			String[] params = new String[1];
			params[0] = field;
			return String.format(context.getString(R.string.error_chinese_letters_only), params);
		}
		return null;
	}
	
	/**
	 * validate Chinese letters only and empty
	 * @param context
	 * @param value
	 * @param field
	 * @return error as string (if exist) 
	 */
	public static String validateChineseLettersOnlyAndEmpty(Context context, String value, String field){
		String error = null;
		error = isFieldEmpty(context, value, field);
		if (error != null){
			return error;
		} 
		error = validateChineseLettersOnly(context, value, field);		
		return error;
	}
	
}