package com.anyoption.android.app.util;

import com.anyoption.android.app.R;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;

/**
 * Common set of utility functions for launching apps.
 */
public class LauncherUtils {
    

   /* public static Intent getLaunchIntent(Context context, String title, String url, String sel) {
        //maybe in future we will use it to know which page to open in the app from the push msg
    }*/

   public static void generateNotification(Context context, String msg, String title, Intent intent) {
       int icon = R.drawable.notification_icon;
       long when = System.currentTimeMillis();

       Notification notification = new Notification(icon, msg, when);
       notification.setLatestEventInfo(context, title, msg, PendingIntent.getActivity(context, 0, intent, 0));
       notification.flags |= Notification.FLAG_AUTO_CANCEL;
       notification.flags |= Notification.FLAG_ONGOING_EVENT;

       SharedPreferences settings = context.getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
       int notificatonID = settings.getInt("notificationID", 0); // allow multiple notifications

       NotificationManager nm =
               (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
       nm.notify(notificatonID, notification);
       playNotificationSound(context);

       SharedPreferences.Editor editor = settings.edit();
       editor.putInt("notificationID", ++notificatonID % 32);
       editor.commit();
   }

   public static void playNotificationSound(Context context) {
       Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
       if (uri != null) {
           Ringtone rt = RingtoneManager.getRingtone(context, uri);
           if (rt != null) {
               rt.setStreamType(AudioManager.STREAM_NOTIFICATION);
               rt.play();
           }
       }
   }
}
