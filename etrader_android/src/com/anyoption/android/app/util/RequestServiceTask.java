package com.anyoption.android.app.util;

import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.anyoption.android.app.R;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.results.MethodResult;
import com.google.gson.Gson;

public class RequestServiceTask extends AsyncTask<MethodRequest, Void, MethodResult> {
    private static List<RequestServiceTask> runningTasks = new ArrayList<RequestServiceTask>();
    private static Dialog networkProgressPopup = null;

    private Activity activity;
    private Handler handler;
    private String serviceMethod;
    private Class<? extends MethodResult> resultClass;
    private Method receiverMethod;
    private Object receiver;
	private boolean toShowDialog;
    
    public RequestServiceTask(Activity activity, Handler handler, String serviceMethod, Object receiver, Method receiverMethod, Class<? extends MethodResult> resultClass, boolean toShowDialog) {
        this.activity = activity;
        this.handler = handler;
        this.serviceMethod = serviceMethod;
        this.resultClass = resultClass;
        this.receiverMethod = receiverMethod;
        this.receiver = receiver;
        this.toShowDialog = toShowDialog;
    }
    public RequestServiceTask(Activity activity, Handler handler, String serviceMethod, Object receiver, Method receiverMethod, Class<? extends MethodResult> resultClass) {
    	this(activity, handler, serviceMethod, receiver, receiverMethod, resultClass, true);
    }
    	   
    
    @Override
    protected void onPreExecute() {
        Log.v("anyoption", "Start network opp: " + serviceMethod);
        if (null != activity && null != handler) {
            handler.post(new Runnable() {
                public void run() {
                    synchronized (runningTasks) {
                        if (null == networkProgressPopup && toShowDialog) {
                            Log.v("anyoption", "Show network progress popup.");
                            try {
                                networkProgressPopup = new Dialog(activity, R.style.network_progress_dialog);
                                networkProgressPopup.setContentView(R.layout.network_progress_popup);
                                networkProgressPopup.setCancelable(false);
                                networkProgressPopup.show();
                            } catch (Exception e) {
                                // Most likely the activity was stopped.
                                Log.e("anyoption", "Can't show network progress popup.", e);
                            }
                        }
                    }
                }
            });
        }
    }

    @Override
    protected MethodResult doInBackground(MethodRequest... params) {
        Log.v("anyoption", "doInBackground - " + serviceMethod);
        MethodResult result = null;
        HttpURLConnection httpCon = null;
        try {     
            URL u = new URL(Constants.SERVICE_URL + serviceMethod);
            httpCon = (HttpURLConnection) u.openConnection();
            httpCon.setDoOutput(true);
            
            Gson gson = new Gson();
            byte[] data = gson.toJson(params[0]).getBytes("UTF-8");                
            httpCon.getOutputStream().write(data, 0, data.length);

            if (!isCancelled()) {
                int respCode = httpCon.getResponseCode();
                Log.d("anyoption", "ServiceCall: " + serviceMethod + ", ResponseCode: " + String.valueOf(respCode));
                result = gson.fromJson(new InputStreamReader(httpCon.getInputStream(), "UTF-8"), resultClass);
                Log.v("anyoption", "ServiceCall: " + serviceMethod + ", Done parsing response.");
            }
        } catch (Exception e) {
            Log.e("anyoption", "Problem executing ServiceCall: " + serviceMethod, e);
        } finally {
            if (null != httpCon) {
                httpCon.disconnect();
            }
        }
        return result;
    }

    @Override
    protected void onCancelled() {
        Log.v("anyoption", "onCancelled - " + serviceMethod);
        removeCurrentTask();
    }

    @Override
    protected void onPostExecute(MethodResult result) {
        Log.v("anyoption", "onPostExecute - " + serviceMethod);
        removeCurrentTask();
        if (null != receiver && null != receiverMethod) {
            try {
                receiverMethod.invoke(receiver, result);
            } catch (Exception e) {
                Log.e("anyoption", "", e);
            }
        }
    }
    
    private void removeCurrentTask() {
        synchronized (runningTasks) {
            runningTasks.remove(this);
            int size = runningTasks.size();
            if (size == 0) {
                hideNetworkProgressPopup();
            } else {
                Log.v("anyoption", "Tasks still running: " + size);
            }
        }
    }
    
    private static void hideNetworkProgressPopup() {
        if (null != networkProgressPopup) {
            Log.v("anyoption", "Hide network progress popup.");
            try {
                networkProgressPopup.dismiss();
            } catch (Exception e) {
                Log.e("anyoption", "Problem closing network progress.", e);
            }
            networkProgressPopup = null;
        }
    }
    
    public static void startServiceRequest(Activity activity, Handler handler, String serviceMethod, Object receiver, Method receiverMethod, MethodRequest request, Class<? extends MethodResult> resultClass, boolean toShowDialog) {
        synchronized (runningTasks) {
        	RequestServiceTask task = (RequestServiceTask)new RequestServiceTask(activity, handler, serviceMethod, receiver, receiverMethod, resultClass, toShowDialog).execute(request);
        	runningTasks.add(task);
        }
        Log.v("anyoption", "Running tasks: " + runningTasks.size());
    }
    
    public static void cancelAllRequests() {
        synchronized (runningTasks) {
            for (Iterator<RequestServiceTask> i = runningTasks.iterator(); i.hasNext();) {
                i.next().cancel(true);
            }
            runningTasks.clear();
            hideNetworkProgressPopup();
        }
    }
    public void setToShowDialog(boolean toShow){
    	toShowDialog = toShow;
    }
}