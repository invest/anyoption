package com.anyoption.android.app.util;

import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;

public class ViewFlipperAnimationHelper {
    private static final int ANIMATION_LENGTH = 250;
    
    public static class AnimationHelper {
        public static Animation inFromRightAnimation() {
            Animation inFromRight = new TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, +1.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f);
            inFromRight.setDuration(ANIMATION_LENGTH);
            inFromRight.setInterpolator(new LinearInterpolator());
            return inFromRight;
        }
        
        public static Animation inFromRightAnimation(float startX, float endX) {
            Animation inFromRight = new TranslateAnimation(
                    Animation.ABSOLUTE, startX,
                    Animation.ABSOLUTE, endX,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f);
            inFromRight.setDuration(ANIMATION_LENGTH);
            inFromRight.setInterpolator(new LinearInterpolator());
            return inFromRight;
        }

        public static Animation outToLeftAnimation() {
            Animation outtoLeft = new TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, -1.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f);
            outtoLeft.setDuration(ANIMATION_LENGTH);
            outtoLeft.setInterpolator(new LinearInterpolator());
            return outtoLeft;
        }

        public static Animation outToLeftAnimation(float startX, float endX) {
            Animation outtoLeft = new TranslateAnimation(
                    Animation.ABSOLUTE, startX,
                    Animation.ABSOLUTE, endX,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f);
            outtoLeft.setDuration(ANIMATION_LENGTH);
            outtoLeft.setInterpolator(new LinearInterpolator());
            return outtoLeft;
        }

        public static Animation inFromLeftAnimation() {
            Animation inFromLeft = new TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, -1.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f);
            inFromLeft.setDuration(ANIMATION_LENGTH);
            inFromLeft.setInterpolator(new LinearInterpolator());
            return inFromLeft;
        }

        public static Animation inFromLeftAnimation(float startX, float endX) {
            Animation inFromLeft = new TranslateAnimation(
                    Animation.ABSOLUTE, startX,
                    Animation.ABSOLUTE, endX,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f);
            inFromLeft.setDuration(ANIMATION_LENGTH);
            inFromLeft.setInterpolator(new LinearInterpolator());
            return inFromLeft;
        }

        public static Animation outToRightAnimation() {
            Animation outtoRight = new TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, +1.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f);
            outtoRight.setDuration(ANIMATION_LENGTH);
            outtoRight.setInterpolator(new LinearInterpolator());
            return outtoRight;
        }

        public static Animation outToRightAnimation(float startX, float endX) {
            Animation outtoRight = new TranslateAnimation(
                    Animation.ABSOLUTE, startX,
                    Animation.ABSOLUTE, endX,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f);
            outtoRight.setDuration(ANIMATION_LENGTH);
            outtoRight.setInterpolator(new LinearInterpolator());
            return outtoRight;
        }
    }
}