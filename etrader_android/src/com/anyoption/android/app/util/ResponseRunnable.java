package com.anyoption.android.app.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ResponseRunnable extends Thread {
	private Object className;
	private Object[] parameters;
	private Method responseMethod;
	
	/**
	 * runnable class to call service 
	 * @param Object[] parameters
	 * @param className the class we want to send the response object to
	 */
	public ResponseRunnable(Object[] parameters, Object className, Method responseMethod) {
		this.parameters = parameters;
		this.className = className;
		this.responseMethod = responseMethod;
	}
	
	public void run() {
		try {
			responseMethod.invoke(className, parameters);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
