package com.anyoption.android.app.util;

import android.view.View;

public class SetVisibilityRunnable implements Runnable {
    private View v;
    private int visibility;
    
    public SetVisibilityRunnable(View v, int visibility) {
        this.v = v;
        this.visibility = visibility;
    }

    public void run() {
        v.setVisibility(visibility);
    }
}