package com.anyoption.android.app.util;

import android.widget.TextView;

public class SetTextRunnable implements Runnable {
    private TextView textView;
    private String text;
    
    public SetTextRunnable(TextView textView, String text) {
        this.textView = textView;
        this.text = text;
    }
    
    public void run() {
        if (null != textView) {
            textView.setText(text);
        }
    }
}