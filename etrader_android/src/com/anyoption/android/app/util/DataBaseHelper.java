/**
 * 
 */
package com.anyoption.android.app.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * DataBase Helper class
 * 
 * @author KobiM
 *
 */
public class DataBaseHelper extends SQLiteOpenHelper {
	public final static String TAG = "DataBaseHelper";
	public static String DB_PATH ;//= "/data/data/com.anyoption.android.app/databases/";
    private static String DB_NAME = "anyoptionDB";
    private SQLiteDatabase myDataBase; 
    private final Context myContext;
 
    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     * @param context
     */
    public DataBaseHelper(Context context) {
    	super(context, DB_NAME, null, 1);
        this.myContext = context;
    }	
 
    /**
     * Creates a empty database on the system and rewrites it with your own database. 
     */
    public void createDataBase() throws IOException {
     	boolean dbExist = checkDataBase();
     	SharedPreferences sp = myContext.getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE); 
     	int versionCode = Utils.getAppVersionCode(myContext);
     	int SPversionCode = sp.getInt(Constants.PREF_VERSION_CODE, 0);
     	if (dbExist && versionCode == SPversionCode) {
    		//do nothing - database already exist
    	} else {
    		//By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
    		if (dbExist) {
    			Log.d(TAG, "got new version deleting exist db");
    			deleteDB();
    		}
        	this.getReadableDatabase();
        	try {
     			copyDataBase();
     		} catch (IOException e) {
         		throw new Error("Error copying database");
         	}
    	}
     	sp.edit().putInt(Constants.PREF_VERSION_CODE, versionCode).commit();
     }
 
    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase() {
    	SQLiteDatabase checkDB = null;
    	try {
    		String myPath = DB_PATH + DB_NAME;
    		checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    	} catch(SQLiteException e) {
    		//database does't exist yet.
    	}
    	if (checkDB != null) {
    		checkDB.close();
    	}
    	return checkDB != null ? true : false;
    }
 
    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException {
    	//Open your local db as the input stream
    	InputStream myInput = myContext.getAssets().open(DB_NAME);
    	// Path to the just created empty db
    	String outFileName = DB_PATH + DB_NAME;
    	//Open the empty db as the output stream
    	OutputStream myOutput = new FileOutputStream(outFileName);
    	//transfer bytes from the inputfile to the outputfile
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0){
    		myOutput.write(buffer, 0, length);
    	}
    	//Close the streams
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
    }
 
    public void openDataBase() throws SQLException { 	
    	myDataBase = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null, SQLiteDatabase.OPEN_READONLY);
    }
 
	public synchronized void close() {
		if (myDataBase != null) {
			myDataBase.close();
		}
    	super.close();
	}
 
	public void onCreate(SQLiteDatabase db) {
	}
 
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
 
	public Cursor getSkins() {
		return myDataBase.query("skins",
				new String[] {"skin_id",
					"display_name",
					"defaultCountryId",
					"defaultLanguageId",
					"defaultCurrencyId",
					"SUPPORT_START_TIME",
					"SUPPORT_END_TIME",
					"skin_group_id"},
				null,
				null,
				null,
				null,
				null);
	}
	public Cursor getSkinCurreines(long skinId) {
		return myDataBase.query("skin_currencies",
				new String[] {"currency_id"},
					"skin_id = ? ",
				new String[] {String.valueOf(skinId)},
				null,
				null,
				null);
	}
	
	public Cursor getCountries(long languageId) {
		String sql = 
			"SELECT " +
				"c.id, " +
				"c.A2, " +
				"t.value AS name, " +
				"c.support_phone, " +
				"c.support_fax, " +
				"c.phone_code, " +
				"pm.id AS paymentTypeId, " +
				"pm.name AS paymentTypeName, " +
				"pm.display_name AS paymentTypeDisplayName, " +
				"pm.big_logo, " +
				"pm.small_logo, " +
				"pm.type_id " +
			"FROM " +
				"translations t, " +
				"countries c LEFT JOIN  payment_methods_countries pmc ON c.id = pmc.country_id " +
					"LEFT JOIN  payment_methods pm ON pm.id = pmc.payment_method_type_id " +     				
			"WHERE " +
				"c.display_name = t.key AND " +
				"c.id != " + Constants.COUNTRY_NORTH_KOREA_ID + " AND " +
				"t.language_id = ? " +
			"ORDER BY " +
				"name ";						
		return myDataBase.rawQuery(sql, new String[] {String.valueOf(languageId)});				
	}
	
	/**
	 * Returns cursor from the regulated countries with only one column with name "id".
	 * 
	 * @return
	 */
	public Cursor getRegulatedCountriesIds() {
		String sql = 
			"SELECT " +
				"id " +
			"FROM " +
				"countries " +     				
			"WHERE " +
				"is_regulated = '1' ";				
		return myDataBase.rawQuery(sql, new String[] {});				
	}
	
	/**
	 * Returns cursor from the non-regulated countries with only one column with name "id".
	 * 
	 * @return
	 */
	public Cursor getNonRegulatedCountriesIds() {
		String sql = 
			"SELECT " +
				"id " +
			"FROM " +
				"countries " +     				
			"WHERE " +
				"is_regulated = '0' ";				
		return myDataBase.rawQuery(sql, new String[] {});				
	}
	
	public Cursor getCities() { //for ET
		String sql = 
			"SELECT " +
				"c.id, " +
				"c.name " +				
			"FROM " +
				"cities c " +				     						
			"ORDER BY " +
				"name ";						
		return myDataBase.rawQuery(sql, new String[] {});				
	}
	
	public Cursor getMarketInfo(long languageId, long marketId ) {
		String sql = 
			" SELECT " +
				" t.value " +
			" FROM " +
				" asset_index ai, " +
				" translations t " +
			" WHERE	" +
				" ai.market_info_key = t.key " +
				" AND ai.market_id = ? " +
				" AND t.language_id = ? ";
						
		return myDataBase.rawQuery(sql, new String[] {String.valueOf(marketId), String.valueOf(languageId)});				
	}
	
	public Cursor getSupportPhone(long skinId) {
		String sql = "SELECT " +
	            " support_phone from skins s, countries co "+
                " WHERE " +
                " s.defaultCountryId = co.id "+
                " and s.skin_id = ?";
						
		return myDataBase.rawQuery(sql, new String[] {String.valueOf(skinId)});				
	}
	
	public Cursor getCorrespondingNotRegulatedSkinId(long regulatedSkinId) {
		String sql = "SELECT " +
	            " not_regulated_skin_id from skins "+
                " WHERE " +
                " skin_id = ?";
						
		return myDataBase.rawQuery(sql, new String[] {String.valueOf(regulatedSkinId)});				
	}
	
	public boolean isSkinRegulated(long skindId){
		Cursor cursor = myDataBase.query("skins",
				new String[] { "is_regulated"}, "skin_id="+skindId, null,
				null, null, null);
		cursor.moveToFirst();
		return (cursor.getInt(0)==1);
	}
	
	public int getRegulatedAlternative(long skinId){
		Cursor cursor = myDataBase.query("skins",
				new String[] { "skin_id"}, "not_regulated_skin_id = "+skinId, null,
				null, null, null);
		if(cursor.getCount()==0){
			return -1;
		}
		cursor.moveToFirst();
		
		return cursor.getInt(0);
	
	}
	
	public void deleteDB() {
		myContext.deleteDatabase(DB_NAME);
	}
}
