package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.anyoption.android.app.util.Constants;

public class LoginHelper {
	private Context mContext;
	private boolean isComingFromTerms;

	public LoginHelper(Context context) {
		mContext = context;
	}

	public void handleTermsActivityResult(int requestCode, int resultCode,
			Intent data) {
		if (requestCode == Constants.REQUEST_CODE_TERMS
				&& resultCode == Activity.RESULT_OK) {
			setComingFromTerms(true);
			String activityBeforeTerms = data.getExtras().getString(
					Constants.ACTIVITY_BEFORE_TERMS);
			Intent intent = null;
			if (activityBeforeTerms.equals("markets_group")) {
				intent = new Intent(mContext, MarketsGroupActivity.class);
				intent.putExtra(Constants.PUT_EXTRA_SHOW_LOG_OUT_ALERT, true);
			} else if (activityBeforeTerms.equals("my_account")) {
				intent = new Intent(mContext, MyAccountActivity.class);
			}
			if (null != intent) {
				mContext.startActivity(intent);
			}
		}
	}

	public boolean isComingFromTerms() {
		return isComingFromTerms;
	}

	public void setComingFromTerms(boolean isComingFromTerms) {
		this.isComingFromTerms = isComingFromTerms;
	}

	public void handleOnResume() {
		AnyoptionApplication ap = (AnyoptionApplication) mContext
				.getApplicationContext();

		// if we are coming from TermsActivity through onActivityResult we
		// should not set the user to null
		if (null != ap.getUser() && !isComingFromTerms()) {
			ap.setUser(null);
		}
		/*
		 * resetting isComingFromTerms to false, so the next time this activity
		 * is resumed the user will be set to null
		 */
		setComingFromTerms(false);

		LoginView loginView = (LoginView) ((Activity) mContext)
				.findViewById(R.id.customViewLoginView);
		loginView.resetPassword();
		loginView.setNeedToErase(true);
	}
}
