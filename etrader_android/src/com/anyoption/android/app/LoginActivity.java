package com.anyoption.android.app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class LoginActivity extends BaseActivity {
	private LoginHelper loginHelper;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		findViewById(R.id.viewLogin).setBackgroundColor(0xFFFFFFFF);
		loginHelper = new LoginHelper(this);
	}

	@Override
	public void onResume() {
		Log.v("Home_pageActivity", "onResume");
		loginHelper.handleOnResume();
		super.onResume();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		loginHelper.handleTermsActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}
}