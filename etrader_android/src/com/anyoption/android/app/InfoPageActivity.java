/**
 * 
 */
package com.anyoption.android.app;


import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;

/**
 * @author IdanZ
 *
 */
public class InfoPageActivity extends BaseMenuActivity {
	
	private TextView rmButton1;
	private TextView rmButton2;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int id = getIntent().getIntExtra(Constants.PUT_EXTRA_INFO_PAGE, 0);
        if (id == Utils.getLayoutFromResource(this, "info_page")){
        	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
		setContentView(id); 
		if(id == R.layout.info_page){
			if(!Utils.isEtraderProject(this)){
        		ViewGroup aboutUsTexts =(ViewGroup) findViewById(R.id.about_us);
        		for(int i = 0 ; i<aboutUsTexts.getChildCount(); ++i){
        			aboutUsTexts.getChildAt(i).setVisibility(View.GONE);
        		}
        		String web_site_link = Utils.getStringFromResource(this, Constants.WEB_SITE_LINK, null);
        		AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        		WebView aboutUsWebView = new WebView(this);
        		aboutUsWebView.setBackgroundColor(0);
        		aboutUsWebView.getSettings().setJavaScriptEnabled(true);
        		aboutUsTexts.addView(aboutUsWebView);
        		aboutUsWebView.loadUrl(web_site_link + "/aboutus_content.jsf?fromApp=true&s=" + ap.getSkinId());
        		
        	}
		}
		if(id == R.layout.security){
			AnyoptionApplication ap = (AnyoptionApplication) getApplication();
			String web_site_link = Utils.getStringFromResource(this, Constants.WEB_SITE_LINK, null);
			WebView securityWebView = (WebView) findViewById(R.id.securityWebView);
			if (Utils.isEtraderProject(this)){
				securityWebView.loadUrl(web_site_link + "iw" + "/security.jsf?fromApp=true");
			} else {
				securityWebView.loadUrl(web_site_link + "/security_content.jsf?fromApp=true&s=" + ap.getSkinId());
			}
			securityWebView.setBackgroundColor(0);
			securityWebView.getSettings().setJavaScriptEnabled(true);
		}
		menuScrollView = (ScrollView)findViewById(R.id.ScrollViewMarketGroups);
	}
	
	public void openReadMoreInfo1(View v){		
		LinearLayout rm;
		if(v.getId() == R.id.LinearLayoutReadMoreInfo1 || v.getId() == R.id.LinearLayoutReadMoreInfo11) { 
        	rm = (LinearLayout) findViewById(R.id.LinearLayoutReadMore1);
    		if(rm.getVisibility() == View.GONE){
    			rm.setVisibility(View.VISIBLE);
    			changeText(false, "1");
    		} else {
    			rm.setVisibility(View.GONE);
    			changeText(true, "1");
    		}    			
		} else if(v.getId() == R.id.LinearLayoutReadMoreInfo2 || v.getId() == R.id.LinearLayoutReadMoreInfo22) {	
        	rm = (LinearLayout) findViewById(R.id.LinearLayoutReadMoreWhat);
    		if(rm.getVisibility() == View.GONE){
    			rm.setVisibility(View.VISIBLE);
    			changeText(false, "2");
    		} else {
    			rm.setVisibility(View.GONE);
    			changeText(true, "2");
    		}
		} else if(v.getId() == R.id.LinearLayoutReadMoreInfo3 || v.getId() == R.id.LinearLayoutReadMoreInfo33) {	
        	rm = (LinearLayout) findViewById(R.id.LinearLayoutReadMoreBenefits);
    		if(rm.getVisibility() == View.GONE){
    			rm.setVisibility(View.VISIBLE);
    			changeText(false, "3");
    		} else {
    			rm.setVisibility(View.GONE);
    			changeText(true, "3");
    		}
		} else if(v.getId() == R.id.LinearLayoutReadMoreInfo4 || v.getId() == R.id.LinearLayoutReadMoreInfo44) {	
        	rm = (LinearLayout) findViewById(R.id.LinearLayoutReadMoreAboutUs);
    		if(rm.getVisibility() == View.GONE){
    			rm.setVisibility(View.VISIBLE);
    			changeText(false, "4");
    		} else {
    			rm.setVisibility(View.GONE);
    			changeText(true, "4");
    		}
		} else if(v.getId() == R.id.LinearLayoutReadMoreInfo5 || v.getId() == R.id.LinearLayoutReadMoreInfo55) {	
        	rm = (LinearLayout) findViewById(R.id.LinearLayoutReadMoreHowDoes);
    		if(rm.getVisibility() == View.GONE){
    			rm.setVisibility(View.VISIBLE);
    			changeText(false, "5");
    		} else {
    			rm.setVisibility(View.GONE);
    			changeText(true, "5");
    		}
		} else if(v.getId() == R.id.LinearLayoutReadMoreInfo6 || v.getId() == R.id.LinearLayoutReadMoreInfo66) {	
        	rm = (LinearLayout) findViewById(R.id.LinearLayoutReadMoreHowToUse);
    		if(rm.getVisibility() == View.GONE){
    			rm.setVisibility(View.VISIBLE);
    			changeText(false, "6");
    		} else {
    			rm.setVisibility(View.GONE);
    			changeText(true, "6");
    		}
		} else if(v.getId() == R.id.LinearLayoutReadMoreInfo7 || v.getId() == R.id.LinearLayoutReadMoreInfo77) {
        	rm = (LinearLayout) findViewById(R.id.LinearLayoutReadMoreHowGraphWorks);
    		if(rm.getVisibility() == View.GONE){
    			rm.setVisibility(View.VISIBLE);
    			changeText(false, "7");
    		} else {
    			rm.setVisibility(View.GONE);
    			changeText(true, "7");
    		}
		}
		//scroll to the right Y so the group will be in the top
	    menuScrollView.post(new ScrollViewScrollRunnable(menuScrollView, v));
	}
	
	public void changeText(boolean visible, String textViewNumber){
		String rId = "TextViewReadMoreInfo";
		rmButton1 = (TextView) findViewById(Utils.getIdFromResource(this, rId + textViewNumber)); 
		rmButton2 = (TextView) findViewById(Utils.getIdFromResource(this, rId + textViewNumber + textViewNumber)); 
		if (visible){			
			rmButton1.setText(R.string.info_page_read_more);
			rmButton2.setText(R.string.info_page_read_more);
		}
		else{
			rmButton1.setText(R.string.info_page_read_more_close);
			rmButton2.setText(R.string.info_page_read_more_close);
		}
	}
}
