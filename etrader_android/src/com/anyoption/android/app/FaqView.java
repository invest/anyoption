/**
 * 
 */
package com.anyoption.android.app;

import java.util.Locale;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;

/**
 * @author AviadH
 *
 */
public class FaqView extends MenuViewBase {
	
	private final String TAG = "FaqView";
	
	private View lastOpenRow;
	private String menuType;
	private int arrowRightId;
	private int arrowDownId;
	
	public FaqView(Context context) {
		super(context, null);
	}
	
	public FaqView(Context context,String type) {
		super(context, null);
		this.menuType = type;
		initComponent(false);
	}

	public FaqView(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.faqView);  
		menuType = attributes.getString(R.styleable.faqView_menuType);
		boolean isBlueArrow = attributes.getBoolean(R.styleable.faqView_isArrowBlue, false);
		initComponent(isBlueArrow);
	}
	
	public void initComponent(boolean isBlueArrow) {
		Log.d(TAG, "InitComponent");
		tlMain.setColumnShrinkable(1, true);
		
		if (isBlueArrow) {
			arrowRightId = R.drawable.arrow_nav_right;
			arrowDownId = R.drawable.arrow_nav_bottom;
		} else {
			arrowRightId = R.drawable.list_arrow_off;
			arrowDownId = R.drawable.list_arrow_on;
		}

		String[] questionAr = null;
		String[] answerAr = null;
		TextView groupName = (TextView)menuLine.findViewById(R.id.textViewGroupName);
		
		if (menuType.equals(Constants.MENU_TYPE_FAQ_TYPE_TECHNICAL)) {
			questionAr = getResources().getStringArray(R.array.technicalFaqQuetions);
			answerAr = getResources().getStringArray(R.array.technicalFaqAnswers);
			groupName.setText(getResources().getString(R.string.faq_header, getResources().getString(R.string.faq_technical)));
		} else if (menuType.equals(Constants.MENU_TYPE_MY_ACCOUNT)) {
			questionAr = getResources().getStringArray(R.array.myAccountFaqQuetions);
			answerAr = getResources().getStringArray(R.array.myAccountFaqAnswers);
			groupName.setText(getResources().getString(R.string.faq_header, getResources().getString(R.string.faq_myaccount)));
		} else if (menuType.equals(Constants.MENU_TYPE_TRADING)) {
			questionAr = getResources().getStringArray(R.array.tradingFaqQuetions);
			answerAr = getResources().getStringArray(R.array.tradingFaqAnswers);
			groupName.setText(getResources().getString(R.string.faq_header, getResources().getString(R.string.faq_trading)));
		} else if (menuType.equals(Constants.MENU_TYPE_BANKING)) {
			questionAr = getResources().getStringArray(R.array.bankingFaqQuestions);
			answerAr = getResources().getStringArray(R.array.bankingFaqAnswers);
			if(Utils.isRegulated(context)){
				answerAr[0] = context.getString(R.string.bankingFaqFirstAnswerRegulated);
			}
			groupName.setText(getResources().getString(R.string.faq_header, getResources().getString(R.string.faq_banking)));
		}
		
		for (int i = 0 ; i < questionAr.length ; i++) {
			TableRow trQuestion = (TableRow) inflater.inflate(R.layout.faq_question_row, tlMain, false);
			TextView question = (TextView) trQuestion.findViewById(R.id.tradingFaqQuestion);
			
			TableRow trAnswer = (TableRow) inflater.inflate(R.layout.faq_answer_row, tlMain, false);
			TextView answer = (TextView) trAnswer.findViewById(R.id.tradingFaqAnswer);
			
			trQuestion.setTag(trAnswer);
			//add row click to trading
			trQuestion.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					clickRowLogoutHandler(v);
				}
			});
			
			question.setText(questionAr[i]);
			answer.setText(answerAr[i]);
			tlMain.addView(trQuestion);	
			//tlMain.addView((TableRow) inflater.inflate(R.layout.table_row_seperate, tlMain, false));
			tlMain.addView(trAnswer);
		}
	}
	
    public void clickRowLogoutHandler(View target){
    	boolean shouldOpen = false;
    	if (lastOpenRow != target) {
    		shouldOpen = true;
    		if (null != lastOpenRow) {
    			closeRow(lastOpenRow);
    		}
    	} else {
    		closeRow(lastOpenRow);
    		lastOpenRow = null;
    	} 
    	if (shouldOpen) {
    		//Change image to on mode
			ImageView iv = (ImageView)target.findViewById(R.id.tradingFaqImage);
			iv.setImageResource(R.drawable.arrow_nav_bottom);
			//set answer visible
			TableRow answer = (TableRow) target.getTag();
    		answer.setVisibility(View.VISIBLE);
    		if (null != menuScrollView) {
    			menuScrollView.post(new ScrollViewScrollRunnable(menuScrollView, target));
    		}
    		lastOpenRow = target;
    	}
    }
    
    private void closeRow(View target) {
    	ImageView iv = (ImageView)target.findViewById(R.id.tradingFaqImage);
		iv.setImageResource(R.drawable.arrow_nav_right);
		//set answer and white line visible
		TableRow answer = (TableRow) target.getTag();
		answer.setVisibility(View.GONE);
    }
    
    @Override
    public void closeGroup() {
    	ImageView iv = (ImageView) findViewById(R.id.imageViewGroupArrow);
	    iv.setImageResource(arrowRightId);
     	tlMain.setVisibility(View.GONE);
	}
	
    @Override
	public void openGroup() {
    	if (menuScrollView == null) {
    		menuScrollView = ((BaseMenuActivity) context).getMenuScrollView();
    	}
		ImageView iv = (ImageView) findViewById(R.id.imageViewGroupArrow);
		iv.setImageResource(arrowDownId);
    	tlMain.setVisibility(View.VISIBLE);
	}
    
    public String getMenuType() {
    	return menuType.toLowerCase(new Locale("en")).replace(" ", "");
    }

	@Override
	public void removeAllRows() {
		// TODO Auto-generated method stub
		
	}
}
