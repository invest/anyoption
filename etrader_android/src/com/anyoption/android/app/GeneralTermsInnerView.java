package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;

public class GeneralTermsInnerView extends MenuViewBase{

	private boolean isOpened;
	private boolean loaded;

	
	public GeneralTermsInnerView(final Context context, String itemName, final String anchor) {
		super(context);

		buttonMenuStrip.setBackgroundResource(R.drawable.bg_option_row_press);
		TextView groupName = (TextView) findViewById(R.id.textViewGroupName);
		groupName.setTextColor(Color.WHITE);
		groupName.setText(itemName);
		final WebView webView = new GeneralTermsInnerWebView(context);
		tlMain.addView(webView);
		android.view.ViewGroup.LayoutParams webViewParams = webView.getLayoutParams();
		webViewParams.height = android.view.ViewGroup.LayoutParams.FILL_PARENT;
		webView.setLayoutParams(webViewParams);
		
		webView.setBackgroundColor(0);
		webView.getSettings().setJavaScriptEnabled(true);
		int dps = 400;
		final float scale = getContext().getResources().getDisplayMetrics().density;
		int pixels = (int) (dps * scale + 0.5f);
		tlMain.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, pixels));
		buttonMenuStrip.setOnClickListener(new OnClickListener() {
			

			public void onClick(View target) {
				if(!loaded){
					webView.loadUrl(getContentURL(anchor));
					loaded = true;
					if (menuScrollView == null) {
			    		menuScrollView = ((BaseMenuActivity) context).getMenuScrollView();
			    	}
					
				}
				toggleGroup(target);
			}
		});
		
	}
	
	private String getContentURL(String anchor) {
		String contentURL;
		String web_site_link = Utils.getStringFromResource(context, Constants.WEB_SITE_LINK, null);
		AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context).getApplication();
		String currencyParameter = ap.getUser() != null ? "&curid=" + ap.getUser().getCurrencyId() : "";
		contentURL = web_site_link + "general_terms_content.jsf?fromApp=true&s=" + ap.getSkinId() + currencyParameter + anchor;
		return contentURL;
	}
	
	private void toggleGroup(View target){
		if(isOpened){
			isOpened = false;
			closeGroup();
		}else {
			if(menuScrollView!=null){
				menuScrollView.post(new ScrollViewScrollRunnable(menuScrollView, target));
			}
			isOpened = true;
			openGroup();
		}
	}

	@Override
	public void removeAllRows() {
		// TODO Auto-generated method stub
		
	}
}