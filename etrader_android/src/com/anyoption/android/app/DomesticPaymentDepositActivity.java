/**
 * 
 */
package com.anyoption.android.app;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.FirstEverDepositCheckRequest;
import com.anyoption.json.requests.UserMethodRequest;
import com.anyoption.json.results.FirstEverDepositCheckResult;
import com.anyoption.json.results.UserMethodResult;

/**
 * @author IdanZ
 *
 */
public class DomesticPaymentDepositActivity extends BaseActivity {
	
		private Handler responseH;
	    private User user;	    
	    private boolean isFirstEverDeposit;
	    
		@Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);      
	        AnyoptionApplication ap = (AnyoptionApplication) getApplication();
	        user = ap.getUser();  
	        responseH = new Handler();
	        setContentView(buildLayout(Constants.PAYMENT_TYPE_DEPOSIT));
	        PaymentMethod payment = (PaymentMethod) getIntent().getExtras().get(Constants.PUT_EXTRA_BANKING_PAYMENT);
	        findViewById(R.id.imageViewInfo).setTag(payment);
			if (null == user) {
				//setContentView(R.layout.deposit_domestic_payment_logout);
			} else {
				//setContentView(R.layout.deposit_domestic_payment_login);				
				if (!Utils.isEtraderProject(this)) {
					getFirstEverDepositCheck();
				}
			}	
		}

		public void openPaymentMethodsPerCountry(View target) {	    	
	    	if (!Utils.isEtraderProject(this) && isFirstEverDeposit) {
				DepositExtraFieldsView depositExtraFieldsView = (DepositExtraFieldsView) findViewById(R.id.DepositExtraFieldsView);
				depositExtraFieldsView.validateAndUpdate("openPaymentMethodsPerCountry");
			} else {
				openPaymentMethodsPerCountry();
			}
		}
		
		public void openPaymentMethodsPerCountry() {
			// Executing getUser request to check if the user is suspended, before he makes a deposit
			Utils.requestService(this, responseH, "getIsSuspended", new UserMethodRequest(), new UserMethodResult(), "getUser", this);
		}
		
		public void getIsSuspended(Object result) {
		    UserMethodResult res = (UserMethodResult) result;
		    if (res.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
		        Utils.showAlert(this, res.getUserMessages()[0].getField(), 
		        		res.getUserMessages()[0].getMessage(), getString(R.string.alert_title));
		    } else {
		         ((AnyoptionApplication) getApplication()).setUser(res.getUser());
		         ((AnyoptionApplication) getApplication()).setUserRegulation(res.getUserRegulation());
		         if(Utils.isRegulated(this) && Utils.isSuspended(this)){
		        	 if(res.getUserRegulation().isSuspendedDueDocuments()){
		        		 Utils.showSuspendedForDocumentsPopoup(this);
		        	 } else {
		        		 Utils.showGeneralSuspendedPopup(this);
		        	 }
		         } else {
		        	//TODO: open payment methods by country - method in web DepositForm --> insertDirectDepositEnvoy
		        	 startActivityForResult(new Intent(this,
					          DomesticPaymentsActivity.class), Constants.REQUEST_CODE_GENERAL_TERMS);
		         }
		    }
		}

	private void getFirstEverDepositCheck() {
		FirstEverDepositCheckRequest request = new FirstEverDepositCheckRequest();
		request.setUserId(user.getId());

		Utils.requestService(this, responseH,
				"getFirstEverDepositCheckCallback", request,
				new FirstEverDepositCheckResult(), "getFirstEverDepositCheck");
	}

	public void getFirstEverDepositCheckCallback(Object resultObject) {
		FirstEverDepositCheckResult result = (FirstEverDepositCheckResult) resultObject;

		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			isFirstEverDeposit = result.isFirstEverDeposit();
		} else {
			isFirstEverDeposit = false;
		}

		DepositExtraFieldsView extraFieldsView = (DepositExtraFieldsView) findViewById(R.id.DepositExtraFieldsView);
		extraFieldsView.setVisibility(View.VISIBLE);

		if (isFirstEverDeposit) {
			extraFieldsView.setCurrencyEnabled(true);
		} else {
			// Will be displayed only currency in disabled state.
			extraFieldsView.setCurrencyEnabled(false);
			extraFieldsView.setStreetVisibility(View.GONE);
			extraFieldsView.setCityVisibility(View.GONE);
			extraFieldsView.setZipCodeVisibility(View.GONE);
		}
	}
	
}
