package com.anyoption.android.app;

import java.util.Calendar;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.Utils;

public abstract class MenuViewBase extends LinearLayout {
	
	protected static final int PAGE_SIZE = 3;
	
	protected LayoutInflater inflater;
	protected Calendar fromCal;
	protected Handler responseH;
	protected Calendar toCal;
	protected TableLayout tlMain;
	protected Context context;
	protected int startRow;
	protected LinearLayout menuLine;
	protected boolean showLoadMore;
	protected MenuViewBase view;
	protected LinearLayout buttonMenuStrip;
	protected OnMenuStripClickListener onMenuStripClickListener; 
	protected int rowsToRemoveInLoadMore;
	protected ScrollView menuScrollView;
	
	public MenuViewBase(Context context) {
		super(context, null);
		init(context);
	}
	
	public MenuViewBase(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	public void init(Context context) {
		this.context = context;
		view = this;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		startRow = 0; 
		menuLine = (LinearLayout) inflater.inflate(R.layout.menu_group, this, false);
		tlMain = (TableLayout) menuLine.findViewById(R.id.tableLayoutGroup);
		tlMain.setVisibility(View.GONE);
		responseH = new Handler();
		buttonMenuStrip = (LinearLayout)menuLine.findViewById(R.id.linearLayoutGroup);
		TextView groupName = (TextView)menuLine.findViewById(R.id.textViewGroupName);
		groupName.setText(getMenuStripText());
		
		buttonMenuStrip.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				OnMenuClick(view);
			}
		});
		addView(menuLine);
	}

	public void setOnMenuStripClickListener(OnMenuStripClickListener listener) {  
		this.onMenuStripClickListener = listener;  
	} 
	
	public void OnMenuClick(MenuViewBase menuViewBase){
		// Check if the Listener was set, otherwise we'll get an Exception when we try to call it
		if(onMenuStripClickListener != null) {
			// Only trigger the event
			onMenuStripClickListener.onClickCloseAll(menuViewBase);
		}
	}
	
	public void makeFirstDatesRow(Calendar from, Calendar to) {
		//making the first row for from {date} to {date} by format
    	TableRow trFirst = (TableRow) inflater.inflate(R.layout.first_dates_row, tlMain, false);   	
		TextView tvFirst = (TextView) trFirst.findViewById(R.id.textViewFromAndToDates);
		String formatFrom = Utils.getDateFormat(from.getTime(), null);
		String formatTo = Utils.getDateFormat(to.getTime(), null);
		String formattedRow = getResources().getString(R.string.menuStrip_myOptions_settled_dates, formatFrom, formatTo);
		tvFirst.setText(formattedRow);
		tvFirst.setTextColor(0xFFFFFFFF);
		tlMain.addView(trFirst);
	}
	
	public void makeNoInformationRow(String noDataSentence) {
		if (tlMain.getChildCount() == 0) {
	    	TableRow trFirst = (TableRow) inflater.inflate(R.layout.first_dates_row, tlMain, false);
	    	trFirst.setTag(MyOptionView.NO_DATA_ROW);
			TextView tvFirst = (TextView) trFirst.findViewById(R.id.textViewFromAndToDates);
			tvFirst.setText(noDataSentence);
			tvFirst.setTextColor(0xFFFFFFFF);
			tlMain.addView(trFirst);
		}
	}
	
	public void makeLastRow(String lastText, final String datesHeader) {
		TableLayout tl = null;
		TableRow tr = null;
		TextView tv = null;
		rowsToRemoveInLoadMore = 0;
		if (null != fromCal && null != toCal && showLoadMore) {
			tl = new TableLayout(this.getContext());
			tr = (TableRow) inflater.inflate(R.layout.more_last_row_menu, tlMain, false);
			tr.findViewById(R.id.imageViewOtherlArrow).setVisibility(View.GONE);
			tv = (TextView) tr.findViewById(R.id.textViewOther);
			tv.setText(getResources().getString(R.string.menuStrip_load_more));
			tv.setTextColor(0xFFFFFFFF);
			
			//click on load more.. to get the next X rows
			tr.setOnClickListener(new OnClickListener() {	
				public void onClick(View v) {
					loadXRows();
				}
			});
			tl.addView(tr);
			tlMain.addView(tl);
			rowsToRemoveInLoadMore++;
		}
		
		//make last row
		tl = new TableLayout(this.getContext());
		tr = (TableRow) inflater.inflate(R.layout.more_last_row_menu, tlMain, false);
		tv = (TextView) tr.findViewById(R.id.textViewOther);
		tv.setText(lastText);
		tv.setTextColor(0xFFFFFFFF);
		
		//click on more to show the dates popup
		tr.setOnClickListener(new OnClickListener() {	
			public void onClick(View v) {
				lastRowClickForPopUp(datesHeader);
			}
		});
		rowsToRemoveInLoadMore++;
		tl.addView(tr);
		tlMain.addView(tl);
	}
	
	public void lastRowClickForPopUp(String datesHeader) {
		// get the current date 
        final Calendar c = Calendar.getInstance(); 
        int mYear = c.get(Calendar.YEAR); 
        int mMonth = c.get(Calendar.MONTH); 
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        
        LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.more_dates_pop_up, null);
        TextView header = (TextView) ll.findViewById(R.id.textViewDateHeader);
        header.setText(datesHeader);
        final DatePicker from = (DatePicker) ll.findViewById(R.id.datePickerFrom);
        final DatePicker to = (DatePicker) ll.findViewById(R.id.datePickerTo);
        from.updateDate(mYear, mMonth, mDay);
        to.updateDate(mYear, mMonth, mDay);
        final Button close = (Button) ll.findViewById(R.id.buttonMoreSettledInvestmentPopUpClose);
        Button submit = (Button) ll.findViewById(R.id.buttonMoreSettledInvestmentPopUpSubmit);
        submit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//set the calender's (from and to) that the client choosed
				fromCal = Calendar.getInstance();
				toCal = Calendar.getInstance();
				
				//init the calender FROM to start of the day and TO to the end of the day
				fromCal.set(from.getYear(), from.getMonth(), from.getDayOfMonth());
				toCal.set(to.getYear(), to.getMonth(), to.getDayOfMonth());
				Log.e("", fromCal.getTime() +"        " + toCal.getTime());
				if (fromCal.compareTo(toCal) <= 0) {
					fromCal.set(Calendar.HOUR_OF_DAY, 0);
					fromCal.set(Calendar.MINUTE, 0);
					fromCal.set(Calendar.SECOND, 0);
					fromCal.set(Calendar.MILLISECOND, 0);
					
					toCal.set(Calendar.HOUR_OF_DAY, 23);
					toCal.set(Calendar.MINUTE, 59);
					toCal.set(Calendar.SECOND, 59);
					toCal.set(Calendar.MILLISECOND, 999);
					Log.e("", fromCal.getTime() +"        " + toCal.getTime());
					
					startRow = 0;
					removeAllRows();
					loadXRows();
					close.performClick();
				} else {
					fromCal = null;
					toCal = null;
					String error = getResources().getString(R.string.error_to_bigger_from);
					Utils.showAlert(context, "", error,  context.getString(R.string.alert_title));
				}
			}
		});
        Utils.showCustomAlert(context, ll, close);
	}
	
	public void closeGroup() {
    	ImageView iv = (ImageView) findViewById(R.id.imageViewGroupArrow);
    	iv.setImageResource(R.drawable.list_arrow_off); 
     	tlMain.setVisibility(View.GONE);
	}
	
	public void openGroup() {
		ImageView iv = (ImageView) findViewById(R.id.imageViewGroupArrow);
    	iv.setImageResource(R.drawable.list_arrow_on);
    	tlMain.setVisibility(View.VISIBLE);
	}
	
	public void loadXRows(){}
	
	public String getNoInformationString() {
		return "";
	}
	
	public String getMenuStripText() {
		return "";
	}
	
	public void restartLoadMore() {
		startRow = 0;
		fromCal = null;
		toCal = null;
	}
	
	public abstract void removeAllRows();
	
	class ScrollViewScrollRunnable implements Runnable {
	    private ScrollView sView;
	    private View v;
	    
	    public ScrollViewScrollRunnable(ScrollView sView, View v) {
	        this.sView = sView;
	        this.v = v;
	    }
	    
	    public void run() {
	        //scroll to the right Y so the group will be in the top
	        int[] corners = new int[2]; 
	        v.getLocationInWindow(corners);
	        int targetY = corners[1];
	        sView.getLocationInWindow(corners);
	        int groupY = targetY - corners[1] + sView.getScrollY();
	        Log.e("",groupY + " ");
	        sView.scrollTo(0, groupY - 10);
	    }
	}
}

