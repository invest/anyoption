/**
 * 
 */
package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Transaction;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.FormsMethodRequest;
import com.anyoption.json.requests.TransactionMethodRequest;
import com.anyoption.json.results.TransactionMethodResult;
import com.anyoption.json.results.TransactionsMethodResult;

/**
 * @author AviadH
 *
 */
public class BankingHistoryView extends MenuViewBase {	
	
	private final int PAGE_SIZE = 5;
	
	private Transaction[] transactions;
	protected boolean isFirst;

	protected boolean isPopUpShown;

	protected boolean isReverseWithdrawalConfirmButtonEnabled = true;
	
	public BankingHistoryView(Context context) {
		this(context, null);
	}
	
	public BankingHistoryView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initComponent();
	}
	
	public void initComponent() {
		tlMain.setColumnStretchable(0, true);
		isFirst = true;
		loadXRows();
	}
	
	public String getMenuStripText() {
		return getResources().getString(R.string.header_transactions);
	}
	
	public void transactionsCallBack(Object result) {
		showLoadMore = false;
		TransactionsMethodResult transactionsResult = (TransactionsMethodResult) result;
		if (transactionsResult.getTransactions().length == 0 && isFirst) {
			makeNoInformationRow(getNoInformationString());
		} else {
			isFirst = false;
			setTransactions(transactionsResult.getTransactions());
			startRow += PAGE_SIZE;
		}
	}
	
	public void loadXRows() {
		Log.d("Banking", "Load from row: " + startRow + " pageSize: " + PAGE_SIZE);
		FormsMethodRequest request = new FormsMethodRequest();
		request.setFrom(fromCal);
		request.setTo(toCal);
		request.setStartRow(startRow);
		request.setPageSize(PAGE_SIZE);
		Utils.requestService(this, responseH, "transactionsCallBack", request, new TransactionsMethodResult(), "getUserTransactions", (Activity) context);
	}
	
	public void setTransactions(Transaction[] trans) {
		//remove the last rows in load more option
		if (startRow > 0) {// if it is not the first time
			for (int i = 1 ; i <= rowsToRemoveInLoadMore ; i++) {
				tlMain.removeViewAt(tlMain.getChildCount()-1);
			}
		}
		
		if (trans.length >= PAGE_SIZE) {
			showLoadMore = true;
		}
		//tlMain.removeAllViews();
		if (trans.length > PAGE_SIZE + 1) {
			showLoadMore = true;
		}
		//in case of dates and when it is the first time after we choose date
		if (fromCal != null && toCal != null && startRow == 0) {
		   	makeFirstDatesRow(fromCal, toCal);
		}
		transactions = trans;
		//get all the transactions show only the one we need
    	for (int i = 0 ; i < transactions.length ; i++) {
    		makeRowForTransaction(transactions[i]);
	    }
    	makeLastRow(getResources().getString(R.string.banking_history_last_row), 
    			getResources().getString(R.string.header_transactions));
	}

	public void makeRowForTransaction(Transaction trans) {
		TableRow tr = (TableRow) inflater.inflate(R.layout.banking_history_row, tlMain, false);
		tr.setBackgroundColor(0x50E1E1E1);
		TextView time = (TextView) tr.findViewById(R.id.textViewBankingHistoryTime);
		TextView id = (TextView) tr.findViewById(R.id.textViewBankingHistoryId);
		TextView isCredit = (TextView) tr.findViewById(R.id.textViewBankingHistoryDebitOrCredit);
		TextView type = (TextView) tr.findViewById(R.id.textViewBankingHistoryType);
		TextView ammount = (TextView) tr.findViewById(R.id.textViewBankingHistoryAmmount);
		
		String[] dateTime = trans.getTimeCreatedTxt().split(" ");
		time.setText(dateTime[1] + " " + dateTime[0]);
		id.setText(getResources().getString(R.string.banking_history_transaction_id, Long.toString(trans.getId())));
		String creditDebit;
		if (trans.getCredit() == true) {
			creditDebit = getResources().getString(R.string.transactions_credit);
		} else {
			creditDebit = getResources().getString(R.string.transactions_debit);
		}
		isCredit.setText(creditDebit);
		type.setText(trans.getTypeName());
		ammount.setText(trans.getAmountWF());
		
		time.setTextColor(Color.parseColor("#FFFFFF"));
		id.setTextColor(Color.parseColor("#FFFFFF"));
		isCredit.setTextColor(Color.parseColor("#FFFFFF"));
		type.setTextColor(Color.parseColor("#FFFFFF"));
		ammount.setTextColor(Color.parseColor("#FFFFFF"));
		if (!trans.getCredit() && 
				(trans.getStatusId() == Constants.TRANS_STATUS_REQUESTED ||
				 trans.getStatusId() == Constants.TRANS_STATUS_APPROVED)) {
			TableRow trReverse = (TableRow) tr.findViewById(R.id.tableRowReverse);
			AnyoptionApplication ap = null;
			User user = null;
			try {
				ap = (AnyoptionApplication) ((Activity) context).getApplication();
				user = ap.getUser();
			} catch (Exception e) {
				
			}
			if (null != user && user.isStopReverseWithdrawOption()) {
				trReverse.setVisibility(View.INVISIBLE);
			} else {
				trReverse.setVisibility(View.VISIBLE);
			}
			
			tr.setPadding(tr.getPaddingLeft(), tr.getPaddingTop(), tr.getPaddingRight(), 0);
			trReverse.setTag(trans);
			trReverse.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					//Check whether the PopUp is already shown:
					if(!isPopUpShown){
						showPopUp(v);
					}
				}
			});
		}
		tlMain.addView(tr);
	}
	
	public void showPopUp(View v) {
		isPopUpShown = true;
		Transaction tran = (Transaction) v.getTag();
		LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.reverse_withdrawal_pop_up, null);
		final Button closeButton = (Button) ll.findViewById(R.id.buttonCancel);
		TextView amount = (TextView) ll.findViewById(R.id.textViewReverseAmount);
		amount.setText(getResources().getString(R.string.banking_history_pop_up_reverse_amount, tran.getAmountWF()));
		Button confirm = (Button) ll.findViewById(R.id.buttonSubmit);
		confirm.setTag(tran);
		confirm.setOnClickListener(new OnClickListener() {		
			public void onClick(View v) {
				if (isReverseWithdrawalConfirmButtonEnabled) {
					isReverseWithdrawalConfirmButtonEnabled = false;
					TransactionMethodRequest transRequest = new TransactionMethodRequest();
					transRequest.setTrans(new Transaction[] { (Transaction) v.getTag() });
					// close popup
					closeButton.performClick();
					((HeaderLayout) ((Activity) context).findViewById(R.id.headerLayout)).refresh();

					Utils.requestService(view, responseH, "reverseCallBack",transRequest, new TransactionMethodResult(),"insertWithdrawsReverse", (Activity) context);
				}
			}
		});
		//read more click
		final TextView readMore = (TextView) ll.findViewById(R.id.textViewReverseReadMore);
		readMore.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.on_click_bg));
		readMore.setOnClickListener(new OnClickListener() { 
			public void onClick(View v) {
				readMore.setText(getResources().getString(R.string.banking_history_pop_up_read_more_extend));
				readMore.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
				readMore.setClickable(false);
			}
		});
		Utils.showCustomAlert(context, ll, closeButton, new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				isPopUpShown = false;
			}
		});
	}
	
	public void reverseCallBack(Object result) {
		isReverseWithdrawalConfirmButtonEnabled = true;
		TransactionMethodResult transactionResult = (TransactionMethodResult) result;
		if (transactionResult.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
			Utils.showAlert(context, transactionResult.getUserMessages()[0].getField(), 
					transactionResult.getUserMessages()[0].getMessage(), context.getString(R.string.alert_title));
			return;
		}
		closeGroup();
		openGroup();
		if (transactionResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Utils.showAlert(context, transactionResult.getUserMessages()[0].getField(), 
					transactionResult.getUserMessages()[0].getMessage(), context.getString(R.string.alert_title_success));
			
			//Refresh Balance
			AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context).getApplication();
			User user = ap.getUser();
			HeaderLayout header = (HeaderLayout)((Activity) context).findViewById(R.id.headerLayout);
			((TextView) header.findViewById(R.id.textViewHeaderBalance)).setText(ap.getString(R.string.header_balance, Utils.formatCurrencyAmount(transactionResult.getBalance(), user.getCurrencySymbol(), user.isCurrencyLeftSymbol(), user.getCurrencyId())));			
		}		
	}
	
    @Override
    public void closeGroup() {
    	super.closeGroup();
    	tlMain.removeAllViews();
    	restartLoadMore();
    	loadXRows();
    }
    
    @Override
    public String getNoInformationString() {
    	return getResources().getString(R.string.transactions_emptylist);
	}
    
    public void removeAllRows() {
    	tlMain.removeAllViews();
    }
}
