/**
 * 
 */
package com.anyoption.android.app;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.UserMethodRequest;
import com.anyoption.json.results.TransactionMethodResult;

/**
 * @author IdanZ
 *
 */
public class BankWireDepositActivity extends BaseActivity {

	    private User user;
	    
	    //CONSTANTS
	    public static final long CURRENCY_ILS_ID = 1;
	    public static final String CURRENCY_ILS = "ILS" ;
	    
	    public static final long CURRENCY_USD_ID = 2;
	    public static final String CURRENCY_USD_IBAN = "DE 94 5123 0800 0000 0501 44";
	    public static final String CURRENCY_USD_ACCOUNT_NUMBER = "50144" ;
	    public static final String CURRENCY_USD = "USD" ;
	    
	    public static final long CURRENCY_EUR_ID = 3;
	    public static final String CURRENCY_EUR_IBAN = "DE 58 5123 0800 0000 0501 13";
	    public static final String CURRENCY_EUR_ACCOUNT_NUMBER = "50113" ;
	    public static final String CURRENCY_EUR = "EUR" ;
	    
	    public static final long CURRENCY_GBP_ID = 4;
	    public static final String CURRENCY_GBP_IBAN = "DE67 5123 0800 0000 0501 45";
	    public static final String CURRENCY_GBP_ACCOUNT_NUMBER = "50145" ;
	    public static final String CURRENCY_GBP = "GBP" ;
	    
	    public static final long CURRENCY_TRY_ID = 5;
	    public static final String CURRENCY_TRY_IBAN = "DE 42 5123 0800 0000 0502 07";
	    public static final String CURRENCY_TRY_ACCOUNT_NUMBER = "50207" ;
	    public static final String CURRENCY_TRY = "TRY" ;
	    
	    public static final long CURRENCY_RUB_ID = 6;
	    public static final String CURRENCY_RUB = "RUB" ;
	    
	    public static final long CURRENCY_CNY_ID = 7;
	    public static final String CURRENCY_CNY = "CNY" ;
	    
	    public static final long CURRENCY_KRW_ID = 8;
	    public static final String CURRENCY_KRW = "KRW" ;
	    
		@Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
			this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			setContentView(buildLayout(Constants.PAYMENT_TYPE_DEPOSIT));
			PaymentMethod payment = (PaymentMethod) getIntent().getExtras().get(Constants.PUT_EXTRA_BANKING_PAYMENT);
	        findViewById(R.id.imageViewInfo).setTag(payment);	
			if (!(this instanceof PaymentMethodPopupActivity)){
				setParamsGeneral();
			}
			getMinFirstDeposit();
			AnyoptionApplication app = (AnyoptionApplication) getApplication();
			TextView textView;
			String supportEmail = getString(R.string.suspended_support_email);
			String supportFax;
			if(app.getUser() != null){
				supportFax = app.getCountries().get(app.getUser().getCountryId()).getSupportFax();
				textView = (TextView) findViewById(R.id.wireDepositInterested);
				Spanned text = Html.fromHtml(getString(R.string.wire_deposit_text_interested, supportFax, supportEmail));
				
				 SpannableStringBuilder clickableText = Utils.makeUnderlineClickable(text, new ClickableSpan() {
						
						@Override
						public void onClick(View widget) {
							Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
							emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{getString(R.string.suspended_support_email)});
							emailIntent.setType("plain/text");
							startActivity(Intent.createChooser(emailIntent, ""));
							
						}
					}, 0xFFF6CD10);
				 if(textView!=null){
					 textView.setMovementMethod(LinkMovementMethod.getInstance());
					 textView.setText(clickableText);
				 }
			} else {
				long defaultCountryId = app.getSkins().get(app.getSkinId()).getDefaultCountryId();
  			  	supportFax = app.getCountries().get(defaultCountryId).getSupportFax();
  			  	if(Utils.isRegulated(this)){
  			//  		findViewById(R.id.bankDetails).setVisibility(View.GONE);
  			  	}
			}
		}
		
		public void setParamsGeneral() {
			AnyoptionApplication ap = (AnyoptionApplication) getApplication();
	        user = ap.getUser();  
	        responseH = new Handler();
			if (null != user) {	
				if (Utils.isEtraderProject(this)) {
					showILSBankDetails(findViewById(R.id.LinearLayoutDetails));
				} else {
					showEURBankDetails(findViewById(R.id.LinearLayoutEUR));
				}				
				// Fill bank details
				long userCurId = user.getCurrencyId();	
				
				// Choose bank details to be displayed.
				if(Utils.isRegulated(this)){
					if(userCurId == CURRENCY_EUR_ID){
						showRegulatedBankDetails(findViewById(R.id.LinearLayoutDetails));
					} else {
						showRegulatedNonEurBankDetails(findViewById(R.id.LinearLayoutDetails));
					}
				} else if (userCurId == CURRENCY_ILS_ID) {
					showILSBankDetails(findViewById(R.id.LinearLayoutDetails));
				} else if (userCurId == CURRENCY_USD_ID || userCurId == CURRENCY_GBP_ID || 
						userCurId == CURRENCY_TRY_ID || userCurId == CURRENCY_CNY_ID ||
						userCurId == CURRENCY_RUB_ID || userCurId == CURRENCY_KRW_ID) {
					showUSDBankDetails(findViewById(R.id.LinearLayoutDetails));
				} else if (userCurId == CURRENCY_EUR_ID) {
					showEURBankDetails(findViewById(R.id.LinearLayoutDetails));
				}
			}
		}
		
		public void setTabsColors(int id) {
			 LinearLayout tabs =(LinearLayout)	findViewById(R.id.linearLayoutTabs);
			 for (int i = 0 ; i < tabs.getChildCount() ; i++) {
				 LinearLayout childTab = (LinearLayout)tabs.getChildAt(i);
				 ImageView arrow = (ImageView) childTab.findViewById(R.id.imageIconArrowSelected);
				 if (childTab.getId() == id) {		
					 arrow.setVisibility(View.VISIBLE);
					 childTab.setBackgroundColor(0xFFE2E2E2);
				 } else {
					 arrow.setVisibility(View.GONE);
					 childTab.setBackgroundColor(0x80E2E2E2);
				 }
			 }
		}
		
		public void showEURBankDetails(View target){ 
			if (user == null){			
				setTabsColors(target.getId());
			}
			setDetailsEURGBPTRYBank();
			
	    }
		
		public void showUSDBankDetails(View target){
			if (user == null){
				setTabsColors(target.getId());
			}			
			setDetailsUSDBank();		
			
	    }
		
		public void showRegulatedBankDetails(View target){
			if (user == null){
				setTabsColors(target.getId());
			}
			setDetailsRegulated();
	    }
		
		/**
		 * Show the details for regulated skins, when the currency is not EUR.
		 */
		public void showRegulatedNonEurBankDetails(View target){
			if (user == null){
				setTabsColors(target.getId());
			}
			setDetailsRegulatedNonEur();
	    }
		
		public void showILSBankDetails(View target){
//			if (user == null){
//				setTabsColors(target.getId());
//			}			
			setDetailsILSBank();		
			
	    }
		
		public void setDetailsRegulated(){
			((TextView)	findViewById(R.id.textViewBeneficiary)).setText(getResources().getString(R.string.bank_wire_deposit_beneficiary,getResources().getString(R.string.bank_wire_reg_data_account_name)));
			((TextView)	findViewById(R.id.textViewAccountNumber)).setText(getResources().getString(R.string.bank_wire_deposit_account_number,getResources().getString(R.string.bank_wire_reg_data_account_number)));
			((TextView)	findViewById(R.id.textViewIban)).setText(getResources().getString(R.string.bank_wire_deposit_iban,getResources().getString(R.string.bank_wire_reg_data_iban)));
			((TextView)	findViewById(R.id.textViewSwift)).setText(getResources().getString(R.string.bank_wire_deposit_swift,getResources().getString(R.string.bank_wire_reg_data_swift)));
			((TextView)	findViewById(R.id.textViewBankName)).setText(getResources().getString(R.string.bank_wire_deposit_bank_name,getResources().getString(R.string.bank_wire_reg_data_bank_name)));
			AnyoptionApplication app = (AnyoptionApplication) getApplication();
			User user = app.getUser();
			if (user != null && user.getCountryId() == Constants.COUNTRY_GERMANY_ID) {
				((TextView)	findViewById(R.id.textViewBankCode)).setText(getResources().getString(R.string.bank_wire_reg_data_bank_code));
				findViewById(R.id.textViewBankCode).setVisibility(View.VISIBLE);
			}
			((TextView)	findViewById(R.id.textViewBankAddress)).setText(getResources().getString(R.string.bank_wire_deposit_bank_address,getResources().getString(R.string.bank_wire_reg_address_line1)));
			((TextView)	findViewById(R.id.textViewBankAddress2)).setText(getResources().getString(R.string.bank_wire_reg_address_line2));
			((TextView)	findViewById(R.id.textViewBankAddress3)).setText(getResources().getString(R.string.bank_wire_reg_address_line3));
		}
		
		public void setDetailsRegulatedNonEur(){
			((TextView)	findViewById(R.id.textViewBeneficiary)).setText(getResources().getString(R.string.bank_wire_deposit_beneficiary,getResources().getString(R.string.bank_wire_reg_noneur_data_account_name)));
			((TextView)	findViewById(R.id.textViewAccountNumber)).setText(getResources().getString(R.string.bank_wire_deposit_account_number,getResources().getString(R.string.bank_wire_reg_noneur_data_account_number)));
			((TextView)	findViewById(R.id.textViewIban)).setText(getResources().getString(R.string.bank_wire_deposit_iban,getResources().getString(R.string.bank_wire_reg_noneur_data_iban)));
			((TextView)	findViewById(R.id.textViewSwift)).setText(getResources().getString(R.string.bank_wire_deposit_swift,getResources().getString(R.string.bank_wire_reg_noneur_data_swift)));
			((TextView)	findViewById(R.id.textViewBankName)).setText(getResources().getString(R.string.bank_wire_deposit_bank_name,getResources().getString(R.string.bank_wire_reg_noneur_data_bank_name)));		
			((TextView)	findViewById(R.id.textViewBankAddress)).setText(getResources().getString(R.string.bank_wire_deposit_bank_address,getResources().getString(R.string.bank_wire_reg_noneur_address_line1)));
			((TextView)	findViewById(R.id.textViewBankAddress2)).setText(getResources().getString(R.string.bank_wire_reg_noneur_address_line2));
			((TextView)	findViewById(R.id.textViewBankAddress3)).setText(getResources().getString(R.string.bank_wire_reg_noneur_address_line3));
		}
		
		public void setDetailsEURGBPTRYBank(){
			((TextView)	findViewById(R.id.textViewBeneficiary)).setText(getResources().getString(R.string.bank_wire_deposit_beneficiary,getResources().getString(R.string.bank_wire_data_account_name)));
			((TextView)	findViewById(R.id.textViewAccountNumber)).setText(getResources().getString(R.string.bank_wire_deposit_account_number,getResources().getString(R.string.bank_wire_data_account_number)));
			((TextView)	findViewById(R.id.textViewIban)).setText(getResources().getString(R.string.bank_wire_deposit_iban,getResources().getString(R.string.bank_wire_data_iban)));
			((TextView)	findViewById(R.id.textViewSwift)).setText(getResources().getString(R.string.bank_wire_deposit_swift,getResources().getString(R.string.bank_wire_data_swift)));
			((TextView)	findViewById(R.id.textViewBankName)).setText(getResources().getString(R.string.bank_wire_deposit_bank_name,getResources().getString(R.string.bank_wire_data_bank_name)));
			((TextView)	findViewById(R.id.textViewBankAddress)).setText(getResources().getString(R.string.bank_wire_deposit_bank_address,getResources().getString(R.string.bank_wire_address_line1)));
			((TextView)	findViewById(R.id.textViewBankAddress2)).setText(getResources().getString(R.string.bank_wire_address_line2));
			((TextView)	findViewById(R.id.textViewBankAddress3)).setText(getResources().getString(R.string.bank_wire_address_line3));
		}
		
		public void setDetailsUSDBank(){
			((TextView)	findViewById(R.id.textViewBeneficiary)).setText(getResources().getString(R.string.bank_wire_deposit_beneficiary, getResources().getString(R.string.bank_wire_usd_data_account_name)));
			((TextView)	findViewById(R.id.textViewAccountNumber)).setText(getResources().getString(R.string.bank_wire_deposit_account_number,getResources().getString(R.string.bank_wire_usd_data_account_number)));
			((TextView)	findViewById(R.id.textViewIban)).setText(getResources().getString(R.string.bank_wire_deposit_iban,getResources().getString(R.string.bank_wire_usd_data_iban)));
			((TextView)	findViewById(R.id.textViewSwift)).setText(getResources().getString(R.string.bank_wire_deposit_swift,getResources().getString(R.string.bank_wire_usd_data_swift)));
			((TextView)	findViewById(R.id.textViewBankName)).setText(getResources().getString(R.string.bank_wire_deposit_bank_name,getResources().getString(R.string.bank_wire_usd_data_bank_name)));
			((TextView)	findViewById(R.id.textViewBankAddress)).setText(getResources().getString(R.string.bank_wire_deposit_bank_address,getResources().getString(R.string.bank_wire_usd_address_line1)));
			((TextView)	findViewById(R.id.textViewBankAddress2)).setText(getResources().getString(R.string.bank_wire_usd_address_line2));
			((TextView)	findViewById(R.id.textViewBankAddress3)).setText(getResources().getString(R.string.bank_wire_usd_address_line3));
		}
		
		public void setDetailsILSBank(){
			((TextView)	findViewById(R.id.textViewBeneficiary)).setText(getResources().getString(R.string.bank_wire_deposit_beneficiary));
			((TextView)	findViewById(R.id.textViewAccountNumber)).setText(getResources().getString(R.string.bank_wire_deposit_account_number));
			((TextView)	findViewById(R.id.textViewIban)).setText(getResources().getString(R.string.bank_wire_deposit_iban));
			((TextView)	findViewById(R.id.textViewSwift)).setText(getResources().getString(R.string.bank_wire_deposit_swift));
			((TextView)	findViewById(R.id.textViewBankName)).setText(getResources().getString(R.string.bank_wire_deposit_bank_name));
		}
		
		/**
		 * get minimum first deposit
		 */
		protected void getMinFirstDeposit() {
	        Utils.requestService(this, responseH, "getMinFirstDepositCallBack", new UserMethodRequest(), new TransactionMethodResult(), "getMinFirstDeposit");
	    }
		
		/**
		 * get minimum first deposit CallBack
		 */
		public void getMinFirstDepositCallBack(Object resultObj) {
			Log.d(getLocalClassName(), "getMinFirstDepositCallBack");
			TransactionMethodResult result = (TransactionMethodResult) resultObj;
			TextView textView = ((TextView)	findViewById(R.id.textViewBankWireMinimum));
			if (textView != null) {
				textView.setText(getResources().getString(R.string.bank_wire_popup_minimum, result.getFormattedAmount()));
			}
		}
}
