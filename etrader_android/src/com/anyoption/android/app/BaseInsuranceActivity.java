package com.anyoption.android.app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Investment;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.UserMethodRequest;
import com.anyoption.json.results.ChartDataMethodResult;
import com.anyoption.json.results.MethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

public class BaseInsuranceActivity extends BaseLightstreamerActivity implements ChartDataListener {
    protected int insurancesCount;
    protected int insurancesType;
    protected long insuranceCloseTime;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        User user = ap.getUser();
        
        ls = getLightstreamerConnectionHandler(user);
        if (null != user) {
            subscribeInsurancesTable(user);
        }
    }
    
    @Override
    public void onResume() {
        insurancesCount = 0;
        super.onResume();
    }
    
    @Override
    public void onPause() {
    	hideInsurancesButton();
        super.onPause();
    }
    
    @Override
    public void updateItem(int itemPos, String itemName, UpdateInfo update) {
        try {
            if (itemName.startsWith("insurances")) {
                if (update.getNewValue("command").equals("ADD")) {
                    Log.v(getLocalClassName() + " lightstreamer", "Adding insurance - market: " + update.getNewValue("INS_MARKET") + " opp: " + update.getNewValue("key") + " type: " + update.getNewValue("INS_INS_TYPE") + " close: " + update.getNewValue("INS_CLOSE_TIME") + " insurancesCount: " + insurancesCount);
                    if (insurancesCount == 0) {
                        insurancesType = Integer.parseInt(update.getNewValue("INS_INS_TYPE"));
                        insuranceCloseTime = Long.parseLong(update.getNewValue("INS_CLOSE_TIME"));
                        showInsurancesButton();
                    }
                    insurancesCount++;
                } else if (update.getNewValue("command").equals("DELETE")) {
                    Log.v(getLocalClassName() + " lightstreamer", "Removing insurance - market: " + update.getNewValue("INS_MARKET") + " opp: " + update.getNewValue("key") + " type: " + update.getNewValue("INS_INS_TYPE") + " close: " + update.getNewValue("INS_CLOSE_TIME") + " insurancesCount: " + insurancesCount);
                    insurancesCount--;
                    if (insurancesCount == 0) {
                        responseH.post(new Runnable() {
                            public void run() {
                                hideInsurancesButton();
                            }
                        });
                    }
                }
            }
        } catch (Exception e) {
            Log.e(getLocalClassName() + " chart", "Problem processing updateItem.", e);
        }
    }
    
    protected void showInsurancesButton() {
        Utils.requestService(this, responseH, "insuranceBannerSeenCallback", new UserMethodRequest(), new MethodResult(), "insuranceBannerSeen");
        // do nothing
    }
    
    protected void hideInsurancesButton() {
        // do nothing
    }
    
    public void insuranceBannerSeenCallback(Object resultObj) {
    	//do nothing
    }

	public void addInvestment(Investment inv) {
		// TODO Auto-generated method stub
		
	}

	public void closeSlip() {
		// TODO Auto-generated method stub
		
	}

	public void removeInvestment(long invId) {
		// TODO Auto-generated method stub
		
	}

	public void setChartData(ChartDataMethodResult data) {
		// TODO Auto-generated method stub
		
	}

	public void setChartSlip(SlipView slip) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(getLocalClassName(), "onActivityResult function requestCode: " + requestCode);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CODE_INSURANCES) { 
	        Bundle extras = data.getExtras(); 
	        insurancesCount = extras.getInt(Constants.PUT_EXTRA_INSURANCES_COUNT, 0); 
		} 
	}
	 
}