package com.anyoption.android.app;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;

public class WebViewActivity extends BaseActivity  {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.web_view);
		AnyoptionApplication ap = (AnyoptionApplication) getApplication();
		WebView webView = (WebView) findViewById(R.id.webViewGeneralTerms);
		webView.getSettings().setJavaScriptEnabled(true);
	    webView.getSettings().setPluginsEnabled(true);
	    String from = (String) getIntent().getExtras().get(Constants.PUT_EXTRA_KEY_WEBVIEW);
	    String web_site_link = Utils.getStringFromResource(this, Constants.WEB_SITE_LINK, null);
	    if (from.equals(Constants.PUT_EXTRA_GENERAL_TERMS)) {
	    	if (Utils.isEtraderProject(this)){
				webView.loadUrl(web_site_link + "iw" + "/general_terms.jsf?fromApp=true");
			} else {
	        	webView.loadUrl(web_site_link +"/general_terms_content.jsf?fromApp=true&s=" + ap.getSkinId());
			}
	    } else if (from.equals(Constants.PUT_EXTRA_ABOUTUS)){	 
	    	if (Utils.isEtraderProject(this)){
				webView.loadUrl(web_site_link + "iw" + "/aboutus.jsf?fromApp=true");
			} else {
				webView.loadUrl(web_site_link + "/aboutus.jsf?fromApp=true&s=" + ap.getSkinId());
			}
	    } else if (from.equals(Constants.PUT_EXTRA_SECURITY)){	        	
	    	if (Utils.isEtraderProject(this)){
				webView.loadUrl(web_site_link + "iw" + "/security.jsf?fromApp=true");
			} else {
				webView.loadUrl(web_site_link + "/security.jsf?fromApp=true&s=" + ap.getSkinId());
			}
	    } else if (from.equals(Constants.PUT_EXTRA_PRIVACY)){
	    	if (Utils.isEtraderProject(this)){
				webView.loadUrl(web_site_link + "iw" + "/privacy.jsf?fromApp=true");
			} else {
	        	webView.loadUrl(web_site_link + "/privacy.jsf?fromApp=true&s=" + ap.getSkinId());
			}
	    } else if (from.equals(Constants.PUT_EXTRA_GENERAL_AGREEMENT)) {
	    	webView.setBackgroundColor(0);
	    	webView.getSettings().setJavaScriptEnabled(true);
	    	if (Utils.isEtraderProject(this)){
				webView.loadUrl(web_site_link + "iw" + "/etrader_agreement.jsf?fromApp=true");
			} else {
				long countryId = getIntent().getExtras().getLong(Constants.PUT_EXTRA_COUNTRY_ID);
	        	webView.loadUrl(web_site_link + "/anyoption_agreement.jsf?fromApp=true&s=" + ap.getSkinId() + "&cid=" + countryId);
			}
	    }
	    webView.setVisibility(View.VISIBLE);
	}
}
