package com.anyoption.android.app;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.anyoption.android.app.ls.LSUiUpdater;
import com.anyoption.android.app.ls.MarketGroupListener;
import com.anyoption.android.app.util.ChartSlideListener;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.MarketGroup;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.ChartDataMethodRequest;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.results.ChartDataMethodResult;
import com.anyoption.json.results.MarketsGroupsMethodResult;
import com.lightstreamer.ls_client.ExtendedTableInfo;
import com.lightstreamer.ls_client.UpdateInfo;

public class MarketsGroupActivity extends BaseTradingActivity {
    
	private ExtendedTableInfo groupTable;
    private ViewFlipper viewFlipper;
    private PageControl pageControl;
    private MGChartSlideListener chartSlideListener;
	
    public MarketsGroupActivity() {
        refreshHeaderOnResume = false;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.markets);
        super.onCreate(savedInstanceState);
        lastOpenGroup = null;
        groupTable = null;
        menuScrollView = (ScrollView)findViewById(R.id.ScrollViewMarketGroups);
        viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);

        pageControl = (PageControl) findViewById(R.id.pageControl);
        pageControl.setPageCount(4);

        chartSlideListener = new MGChartSlideListener(this, (HomePageChartView) chart, viewFlipper, pageControl);
        viewFlipper.setOnTouchListener(chartSlideListener);
        pageControl.setListener(chartSlideListener);

        Utils.requestService(this, responseH, "marketsGroupsCallBack", new MethodRequest(), new MarketsGroupsMethodResult(), "getMarketGroups");
    }
    
    @Override
	public void chartRequest() {
        if (null != ls && null != lsTable) {
            ls.removeTable(lsTable);
            lsTable = null;
        }
    	ChartDataMethodRequest requestChart = new ChartDataMethodRequest();
        requestChart.setBox(pageControl.getCurrentPage());
        requestChart.setOpportunityTypeId(Opportunity.TYPE_REGULAR);
        Utils.requestService(this, responseH, "chartCallBack", requestChart, new ChartDataMethodResult(), "getChartData");
    }
    
    public void marketsGroupsCallBack(Object result) {
    	Log.d(getPackageName(), "marketsGroupsCallBack");
    	MarketsGroupsMethodResult MarketsGroupsResult = (MarketsGroupsMethodResult) result;
    	LinearLayout llMain = (LinearLayout) findViewById(R.id.linearLayoutMain);
    	
    	//adding my option and settled option menu strips to login user
    	AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        if (ap.getUser() != null) {
			myOptions = new MyOptionTradePage(this, false, true, ls);
			myOptions.setMenuScrollView(menuScrollView);
			myOptions.setOnMenuStripClickListener(new OnMenuStripClickListener() {
				public void onClickCloseAll(MenuViewBase menuViewBase) {
					manageOpenMenuStrip(menuViewBase);
				}
			});
			refreshMyOptions();
			llMain.addView(myOptions);
			if (insurancesCount > 0) {
			    myOptions.showInsurancesRow(insurancesType, insuranceCloseTime);
			}
         }
    	LayoutInflater inflater = getLayoutInflater();
    	for (MarketGroup groupItem : MarketsGroupsResult.getGroups()) {
    		Log.d(getPackageName(), groupItem.getName());
    		LinearLayout LL = (LinearLayout)inflater.inflate(R.layout.markets_groups, null, false);
    		LinearLayout LLGroup = (LinearLayout) LL.findViewById(R.id.linearLayoutMraketGroup);
    		TableLayout tlMarkets = (TableLayout) LL.findViewById(R.id.tableLayoutMarkets);
    		tlMarkets.setTag(groupItem.getMarkets());
    		LLGroup.setTag(tlMarkets);
    		TextView groupName = (TextView) LL.findViewById(R.id.textViewGroupName);
    		groupName.setText(groupItem.getName());
    		groupName.setTag(groupItem.getId());
    		if (Constants.MARKET_GROUP_OPTION_PLUS_ID == groupItem.getId()) {
    			if (Utils.isEtraderProject(this)){
    				groupName.setText(R.string.optionPlus_option);
    			}
    			LL.findViewById(R.id.textViewOptionPlus).setVisibility(View.VISIBLE);
    			LL.findViewById(R.id.imageViewOptionPlus).setVisibility(View.VISIBLE);
    		}
    		llMain.addView(LL);
		}	
    	if (ap.getUser() != null) {
    		MyOptionView mySetteled = new MyOptionTradePage(this, true, true, null);
    		mySetteled.setMenuScrollView(menuScrollView);
			mySetteled.setOnMenuStripClickListener(new OnMenuStripClickListener() {
				public void onClickCloseAll(MenuViewBase menuViewBase) {
					manageOpenMenuStrip(menuViewBase);
				}
			});
			llMain.addView(mySetteled);
    	}
    	FaqView faqTrading = new FaqView(this, Constants.MENU_TYPE_TRADING);
    	faqTrading.setOnMenuStripClickListener(new OnMenuStripClickListener() {
			public void onClickCloseAll(MenuViewBase menuViewBase) {
				manageOpenMenuStrip(menuViewBase);
			}
		});
		llMain.addView(faqTrading);
    }
    
	@Override
	public void openGroup(View target) {
    	HashMap<String, TableRow> itemMap = new HashMap<String, TableRow>();    	
    		long groupId = (Long)((target.findViewById(R.id.textViewGroupName)).getTag());
    		if (Constants.MARKET_GROUP_OPTION_PLUS_ID == groupId) {
    			target.findViewById(R.id.textViewOptionPlusOpen).setVisibility(View.VISIBLE);
    			target.findViewById(R.id.textViewOptionPlus).setVisibility(View.GONE);
    		}
	    	ImageView iv = (ImageView) target.findViewById(R.id.imageViewMarketGroupArrow);
	    	iv.setImageResource(R.drawable.list_arrow_on);
	    	TableLayout tl = (TableLayout) target.getTag();//(TableLayout) findViewById(R.id.tableLayoutMarkets);
	    	tl.removeAllViews();
	    	int columnToCollapsed = 1;
            if (Utils.isEtraderProject(this)) {
                columnToCollapsed = 5;
            }
            if (Constants.MARKET_GROUP_OPTION_PLUS_ID == groupId) {
                tl.setColumnCollapsed(columnToCollapsed, false);
            } else {
                tl.setColumnCollapsed(columnToCollapsed, true);
            } 
	    	ArrayList<Market> markets = (ArrayList<Market>) tl.getTag();
	    	String[] items = new String[markets.size()];
		    LayoutInflater inflater = getLayoutInflater();
		    int i = 0;
		    for (Market market : markets) {
			    TableRow tr = (TableRow) inflater.inflate(R.layout.markets_groups_row, tl, false);
			    
			    // Add market Name
			    TextView tvName = (TextView) tr.findViewById(R.id.textViewGroupMarketName);
			    tvName.setText(market.getDisplayName());
			    
			    //save the market for on click
			    tr.setTag(market);
			    
			    tl.addView(tr);
			    String itemName = "aotps_1_";
			    if (market.isOptionPlusMarket()) {
			    	itemName = "op_";
			    }
			    itemName += market.getId();
			    items[i] = itemName;
			    itemMap.put(itemName, tr);
			    i++;
		    }
	    subscribeGroupTable(itemMap, items, tl); 
	    //scroll to the right Y so the group will be in the top
	    menuScrollView.post(new ScrollViewScrollRunnable(menuScrollView, target));
	}
    
    @Override
	public void closeGroup(View group) {
    	if (null != groupTable) { //unsubscribe the ls from this group
//    		ls.unSubscribeOne(groupTable);
    	    ls.removeTable(groupTable);
    		groupTable = null;
    	}
    	ImageView iv = (ImageView) group.findViewById(R.id.imageViewMarketGroupArrow);
    	iv.setImageResource(R.drawable.list_arrow_off);
    	TableLayout tl = (TableLayout) group.getTag();
    	long groupId = (Long)((group.findViewById(R.id.textViewGroupName)).getTag());
		if (Constants.MARKET_GROUP_OPTION_PLUS_ID == groupId) {
			group.findViewById(R.id.textViewOptionPlusOpen).setVisibility(View.GONE);
			group.findViewById(R.id.textViewOptionPlus).setVisibility(View.VISIBLE);
		}
    	tl.removeAllViews();
	}    

	public void subscribeGroupTable(HashMap<String, TableRow> itemMap, String[] items, TableLayout tableLayout) {
	    ScrollView sView = (ScrollView)findViewById(R.id.ScrollViewMarketGroups);
	    int[] coordinates = new int[2];
	    sView.getLocationInWindow(coordinates);
	    groupTable = Utils.addLightstreamerTable(this, items, ls, new MarketGroupListener(new LSUiUpdater(this, itemMap, coordinates[1], sView.getHeight(), tableLayout)), 1.0);
    }
	
	public void chartCallBack(Object resultObj) {
    	Log.d(getPackageName(), "chartCallBack");
    	ChartDataMethodResult result = (ChartDataMethodResult) resultObj;
        if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            ChartView.fixDates(result);
            chart.setChartData(result);
            if (viewFlipper.getDisplayedChild() != 0) {
                viewFlipper.setInAnimation(null);
                viewFlipper.setOutAnimation(null);
                viewFlipper.setDisplayedChild(0);
            }
            lsTable = Utils.addLightstreamerTable(this, new String[] {"aotps_" + result.getScheduled() + "_" + result.getMarketId()}, ls, new AOTPSTableListener(this), 1.0);
        }
    }
	
	public void goToAssetPage(View target) {
        Log.d(getPackageName(), "goToAssetPage");
        Market market = (Market) target.getTag();
        Utils.gotToAssetPage(this, market);
	}
    
    @Override
	public void updateItem(int itemPos, String itemName, UpdateInfo update) {
        if (chartSlideListener.isDragStarted()) {
            Log.v("chart", "dragging... ignore update");
            return; // ignore updates after dragging started because they cause repaint that cause flicker of the chart on its original position
        }
        super.updateItem(itemPos, itemName, update);
        if (itemName.startsWith("insurances")) {
    		return;
    	}
        if (update.isValueChanged("ET_STATE")) {
            if (null != update.getOldValue("ET_STATE")) {
                int oppState = Integer.parseInt(update.getNewValue("ET_STATE"));
                if (oppState == Opportunity.STATE_CLOSING) {
                    // If the oldValue is not null that means we were on the page before the state changed - this should save us from infinite market requests.
                    // If the new state is waiting for expiry the 1 min of waiting for expiry just expired so we should check for new market
                    chartRequest();
                }
            }
        }
    }
    
    @Override
	public void addInvestment(Investment inv) {
        chart.addInvestment(inv);
        myOptions.unsubscribeLs();
        myOptions.makeRowForInvestment(inv);
        myOptions.subscribeLs();

        AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        User user = ap.getUser();
        Utils.showInsertInvestmentSuccessAlert(this, inv, user.isCurrencyLeftSymbol(), user.getCurrencySymbol(), null);
    }
    
    @Override
	public void removeInvestment(long invId) {
        chart.removeInvestment(invId);
        myOptions.unsubscribeLs();
        myOptions.removeInvestment(invId);
        myOptions.subscribeLs();
    }
    
    class MGChartSlideListener extends ChartSlideListener {
        public MGChartSlideListener(Activity activity, HomePageChartView chart, ViewFlipper viewFlipper, PageControl pageControl) {
            super(activity, chart, viewFlipper, pageControl);
        }
        
        @Override
		public void getChartData() {
            chartRequest();
        }
        
        @Override
		public void openAssetPage(long marketId) {
            Market m = new Market();
            m.setId(marketId);
            View v = (HomePageChartView) chart;
            v.setTag(m);
            goToAssetPage(v);
        }
    }
    
    @Override
    protected void showInsurancesButton() {
        Log.v(getLocalClassName(), "Showing insurance button - type: " + insurancesType + " close: " + insuranceCloseTime);
        super.showInsurancesButton();
        if (null != myOptions) {
            myOptions.showInsurancesRow(insurancesType, insuranceCloseTime);
        }
    }

    @Override
    protected void hideInsurancesButton() {
        Log.v("insruance", "Hide insurance button.");
        if (null != myOptions) {
            myOptions.hideInsurancesRow();
        }
    }
    
    @Override
    public void onResume() {
        Log.v("anyoption", "MarketsGroupActivity.onResume");
        super.onResume();
//        if (insurancesCount > 0) {
//            myOptions.showInsurancesRow(insurancesType, insuranceCloseTime);
//        }
        if (null != myOptions) {
        	myOptions.onResume();
        	if (!myOptions.isEmpty()) {
        		refreshMyOptions();
        	}
        }
    }
    
    @Override
    public void onPause() {
    	Log.v("anyoption", "MarketsGroupActivity.onPause");
    	if (null != myOptions) {
            myOptions.onPause();
            if (lastOpenGroup == myOptions) {
            	manageOpenMenuStrip(myOptions);
            	myOptions.setFirst(true);
            }
        }
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (!closeKeyboardSearchOrBackToPortrait()) {
        	if (null != lastOpenGroup) {
        		manageOpenMenuStrip(lastOpenGroup);
        	} else {
	        	if (getIntent().getBooleanExtra(Constants.PUT_EXTRA_SHOW_LOG_OUT_ALERT, false)) {
		            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		            alertDialog.setMessage(getString(R.string.logout_confirm));
		            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
		                public void onClick(DialogInterface dialog, int which) {
		                    dialog.dismiss();
		                    if (which == DialogInterface.BUTTON_POSITIVE) {
		                        finish();
		                    }
		                }};
		            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.alert_button), listener);
		            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.button_cancel), listener);
		            alertDialog.show();
	        	} else {
	        		finish();
	        	}
        	}
        }
    }
    
    public void refreshMyOptions() {
    	manageOpenMenuStrip(myOptions);
		manageOpenMenuStrip(myOptions);
    }
    
    @Override
    public int getInfoPageId() {
    	return R.layout.info_page;
    }
}