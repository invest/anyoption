package com.anyoption.android.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.json.requests.ChangePasswordMethodRequest;
import com.anyoption.json.results.MethodResult;

public class ChangePasswordActivity extends BaseActivity {
	
	
	private Handler responseH;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
        responseH = new Handler();
    }
    
    public void changePasswordClickHandler(View target) {    	    	
    	validateChangePasswordForm();
    }
    
	public void validateChangePasswordForm(){
    	EditText password   = (EditText)findViewById(R.id.editTextOldPassword);
    	EditText passwordNew   = (EditText)findViewById(R.id.editTextNewPassword);
    	EditText passwordReType   = (EditText)findViewById(R.id.editTextRetypePassword);
    	Context context = ChangePasswordActivity.this;
    	//Client validations
    	String error = Utils.validatePassword(context, password.getText().toString()); //Old Pass
    	if (error!= null){
    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	error = Utils.validatePassword(context, passwordNew.getText().toString()); //New Pass
    	if (error!= null){
    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	//Retype password 
    	error = Utils.validateRetypePassword(context, passwordNew.getText().toString(), passwordReType.getText().toString());
    	if (error!= null){
    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	
    	//Service validations
        ChangePasswordMethodRequest request = new ChangePasswordMethodRequest();
        request.setPassword(password.getText().toString());
        request.setPasswordNew(passwordNew.getText().toString()); 
        request.setPasswordReType(passwordReType.getText().toString());
        MethodResult result = new MethodResult();
        
        Utils.requestService(this, responseH, "changePasswordCallBack", request, result, "getChangePassword");  
        
	}
	
	public void changePasswordCallBack(Object resultObj){
		Log.d(getPackageName(), "insertUserCallBack");
		MethodResult result = (MethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(getPackageName(), "Insert user successfully");	    	
			startActivityForResult(
			new Intent(this, Home_pageActivity.class), 1);		    			
		} else {			 
			 String error = result.getUserMessages()[0].getMessage();
			 Utils.showAlert(this, "", error, this.getString(R.string.alert_title));			 			
		}
	}
	
}