package com.anyoption.android.app;

import java.util.Date;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Register;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.json.requests.InsertUserMethodRequest;
import com.anyoption.json.requests.RegisterAttemptMethodRequest;
import com.anyoption.json.results.OptionsMethodResult;
import com.anyoption.json.results.RegisterAttemptMethodResult;
import com.anyoption.json.results.UserMethodResult;

/**
 * @author EranL
 *
 */
public abstract class RegisterCommonActivity extends BaseActivity {
	
	private static final String TAG = RegisterCommonActivity.class.getSimpleName();
	
	protected static final String CITY_DETAILS_ERROR = "error";
	protected static final String CITY_DETAILS_CITY = "city";
	protected static final String CITY_DETAILS_STREETNO = "streetNo";
	private final static int CUP_DEPOSIT = 4;
	private final static String DEPOSIT_CUP = "cup_deposit";
	
    protected int birthYear;
    protected int birthMonth;
    protected int birthDay;

	protected Dialog dateDialog;
	protected Dialog alertDialog;
	protected Context context;		
	protected Handler responseH;
	
	//For register attempt
	protected EditText editTextEmail; 
	protected EditText editTextMobile;
	protected long registerAttemptId;
	protected String mobilePrefixText;
	
	
	//In order to take the default countryId from the service
	protected long countryId;
            
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.register); 
        responseH = new Handler();
        context = this; 
        
      //Add register attempt triggers for email, mobile, contact by mail or sms
        editTextEmail = ((EditText)findViewById(R.id.editTextEmail));
                
        editTextMobile = ((EditText)findViewById(R.id.editTextMobilePhone));

        init();
        
        TextView readMore = (TextView)findViewById(R.id.TextViewReadMore);
        readMore.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.on_click_bg));
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        
        //set font to password fields
        EditText pass  = ((EditText)findViewById(R.id.editTextPassword));
        pass.setTypeface(Typeface.DEFAULT); 
        pass.setTransformationMethod(new PasswordTransformationMethod()); 
        pass = ((EditText)findViewById(R.id.editTextPassword2));
        pass.setTypeface(Typeface.DEFAULT); 
        pass.setTransformationMethod(new PasswordTransformationMethod());
        
    }
    
    public abstract void init();
        
    public void registerClickHandler(View target){
    	validateRegisterForm();
    }
    
    public void navigateTermsPage(View target){    	    	 
    	Intent intent = new Intent(this, WebViewActivity.class);
    	intent.putExtra(Constants.PUT_EXTRA_KEY_WEBVIEW, Constants.PUT_EXTRA_GENERAL_AGREEMENT);
    	intent.putExtra(Constants.PUT_EXTRA_COUNTRY_ID, countryId);
    	startActivityForResult(intent, Constants.REQUEST_CODE_WEB_VIEW);    	
    }
                
    // the callback received when the user "sets" the date in the dialog
    private DatePickerDialog.OnDateSetListener mDateSetListener =
		    new DatePickerDialog.OnDateSetListener() {
		
		        public void onDateSet(DatePicker view, int year, 
		                              int monthOfYear, int dayOfMonth) {
		        	birthYear = year;
		        	birthMonth = monthOfYear;
		        	birthDay = dayOfMonth;
		        	Button dateOfBirth = (Button)findViewById(R.id.buttonDateOfBirth);
		        	dateOfBirth.setText(new StringBuilder()
		        		// Month is 0 based so add 1
		            		.append(birthMonth + 1).append("-")
		            		.append(birthDay).append("-")
		            		.append(birthYear).append(" "));
		        }
		    };
    
    
    @Override
    protected Dialog onCreateDialog(int id) {
    	Dialog dialog;
        switch (id) {
	        case Constants.DIALOG_DATE_ID:
	        	dialog =  new DatePickerDialog(this,
	                        mDateSetListener,
	                        birthYear, birthMonth, birthDay);
	        	dateDialog = dialog;
	        	break;
	        case Constants.DIALOG_ALERT_ID:
	        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
//	        	builder.setMessage(alertJSInterface.msg)
//	        		   .setCancelable(false)
//	        	       .setPositiveButton(this.getString(R.string.alerButton), alertListener);
	        	dialog = builder.create();
	        	alertDialog = dialog;
	        	break;	        	
			default:
	   			dialog = super.onCreateDialog(id);
	    }
        return dialog;
    }
    
    @Override
    public void onResume() {
    	 super.onResume();
    	if (null != dateDialog && dateDialog.isShowing()) {
    		dismissDialog(Constants.DIALOG_DATE_ID);
    	}
    	if (null != alertDialog && alertDialog.isShowing()) {
    		dismissDialog(Constants.DIALOG_ALERT_ID);
    	}
    }
    
    
    /**
     * Handle on prepare dialog event
     * Called every time the dialog is opened  	
     */
    protected void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
        case Constants.DIALOG_DATE_ID:
            ((DatePickerDialog) dialog).updateDate(birthYear, birthMonth, birthDay);            
            break;
        case Constants.DIALOG_ALERT_ID:        	
//        	((AlertDialog) dialog).setMessage(alertJSInterface.msg);
        	break;
   		default:
   			break;
        }	    	
    }
    
    public void fillSpinner(Spinner spinner, Object result) {
		OptionsMethodResult omr = (OptionsMethodResult) result;
		ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, omr.getOptions());
		spinnerArrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spinner.setAdapter(spinnerArrayAdapter);
	}
            
	public void validateRegisterForm(){
		
    	EditText password   = (EditText)findViewById(R.id.editTextPassword);
    	EditText retypePassword   = (EditText)findViewById(R.id.editTextPassword2);
    	EditText email   = (EditText)findViewById(R.id.editTextEmail);
    	EditText firstName   = (EditText)findViewById(R.id.editTextFirstname);
    	EditText lastName   = (EditText)findViewById(R.id.editTextLastname);
    	EditText mobilePhone   = (EditText)findViewById(R.id.editTextMobilePhone);
    	CheckBox terms = (CheckBox)findViewById(R.id.checkBoxTerms);
    	   	
    	Context context = RegisterCommonActivity.this;
    	//Client validations
    	String error = null;
   		//User Name    	 
    	error = validateUserName();
    	if (error != null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	//Password
    	error = Utils.validatePassword(context, password.getText().toString()); 
    	if (error != null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	//Retype password 
    	error = Utils.validateRetypePassword(context, password.getText().toString(), retypePassword.getText().toString());
    	if (error!= null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	//Email
    	error = Utils.validateEmailAndEmpty(context, email.getText().toString(), getString(R.string.register_email)); 
    	if (error != null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	//First Name
    	error = Utils.validateNameAndEmpty(context, firstName.getText().toString(), getString(R.string.register_firstname));
    	if (error != null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	//Last Name
    	error = Utils.validateNameAndEmpty(context, lastName.getText().toString(), getString(R.string.register_lastname));
    	if (error!= null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	//Street
    	error = validateStreet(); 
    	if (error != null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	
    	//CityDetails
    	HashMap<String, String> cityDetails = validateCityDetails();
    	error = cityDetails.get(CITY_DETAILS_ERROR);
    	if (error != null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	
    	//only et
    	validateDateOfBirth();
    	
    	//Mobile prefix for etrader
    	mobilePrefixText = "";
    	error = mobilePrefixValidate();//only implemented in etrader
    	if (error != null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	
    	//Mobile Phone    	
    	error = Utils.validateDigitsOnlyAndEmpty(context, mobilePhone.getText().toString(), getString(R.string.register_mobilephone));
    	if (error != null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return ;
    	}
    	if (mobilePhone.getText().toString().length() < 7){
			String[] params = new String[2];
			params[0] = getString(R.string.register_mobilephone);
			params[1] = "7";    		
    		Utils.showAlert(context, Constants.EMPTY_STRING, String.format(context.getString(R.string.javax_faces_validator_LengthValidator_MINIMUM), params), 
    				context.getString(R.string.alert_title));  
    		return ;
    	}
    	
    	error = validateNumId();// implemented only on etrader
    	if (error != null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return ;
    	}
        
    	//Terms    	
    	if (!terms.isChecked()){
    		Utils.showAlert(context, Constants.EMPTY_STRING, getString(R.string.error_register_terms), context.getString(R.string.alert_title));  
    		return ;
    	}
    	    	 	
    	
    	
    	Register user = new Register();
    	user.setPassword(password.getText().toString());
    	user.setPassword2(retypePassword.getText().toString());
    	user.setEmail(email.getText().toString());
    	
    	user.setFirstName(firstName.getText().toString());
    	user.setLastName(lastName.getText().toString());
    	user.setMobilePhone(mobilePhone.getText().toString());
    	
    	setUserUniqueDetalis(user);
    	
    	if (Utils.isEtraderProject(context)) {
    		user.setCityId(Long.parseLong(cityDetails.get(CITY_DETAILS_CITY)));
    		user.setStreetNo(cityDetails.get(CITY_DETAILS_STREETNO));
    		user.setMobilePhonePrefix(mobilePrefixText);
    	}/* else {
    		user.setCityNameAO(cityDetails.get(CITY_DETAILS_CITY));
    	}*/
    	
  
    	
    	
    	user.setContactByEmail(true);
    	user.setContactBySms(true);
    	user.setTerms(terms.isChecked());
    	

    	user.setCombinationId(Utils.getCombinationId(this));
    	user.setDynamicParam(Utils.getDynamicParam(this));
    	user.setHttpReferer(Utils.getReferrerParam(this));
    	
    	SharedPreferences settings = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
    	long timeFirstVisit = settings.getLong(Constants.PREF_TIME_FIRST_VISIT, 0);
    	Date timeFirstVisitDate = new Date(timeFirstVisit);
    	user.setTimeFirstVisit(timeFirstVisitDate);
    	user.setRegisterAttemptId(registerAttemptId);
    	
    	//Service validations   
    	InsertUserMethodRequest request = new InsertUserMethodRequest();
        UserMethodResult result = new UserMethodResult();        
             
        request.setRegister(user);
        request.setmId(Utils.getmId(context));
        request.setEtsMId(Utils.getEtsMId(this));
        
        if(Utils.isRegulated(this)){
        	// setting skinId to the corresponding not regulated skindId,
        	// so the skinId in the insertUser request is not regulated 
        	setCorrespondingNotRegulatedSkin();
        }
        
        Log.d(TAG, "-------insertUser--------");
		Log.d(TAG, "user combId = " + user.getCombinationId());
		Log.d(TAG, "user dp = " + user.getDynamicParam());
		Log.d(TAG, "request mid = " + request.getmId());
		Log.d(TAG, "request etsmid = " + request.getEtsMId());
		Log.d(TAG, "--------------------------");
    	Utils.requestService(this, responseH, "insertUserCallBack", request, result, "insertUser");
	}
	
	public abstract String validateUserName();

	public abstract void validateDateOfBirth();

	public abstract String validateStreet();

	private void setCorrespondingNotRegulatedSkin() {
		long notRegulatedSkinId = Utils.getCorrespondingNotRegulatedSkinId(this);
		AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context).getApplication();
		ap.setSkinId(notRegulatedSkinId);
	}

	public abstract String validateNumId();
	
	public abstract void setUserUniqueDetalis(Register user);
	
	public abstract String mobilePrefixValidate();
	
	/*this method will validate according to the etrader/anyoption app.
	 * for example: in anyoption we have city in edittext, in etrader its in spinner
	 */
	public abstract HashMap<String, String> validateCityDetails();
	
	public void insertUserCallBack(Object resultObj){
		Log.d(getPackageName(), "insertUserCallBack");
		Context context = RegisterCommonActivity.this;
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {			
			Log.d(getPackageName(), "Insert user successfully");	
	    	AnyoptionApplication ap = (AnyoptionApplication)context.getApplicationContext();
			ap.setUser(result.getUser());
			ap.setUserRegulation(result.getUserRegulation());
			
			String newmId = result.getmId();
			Utils.setmIdIfValid(this, newmId);

			String newEtsMId = result.getEtsMid();
			Utils.setEtsMId(this, newEtsMId);
			
			Log.d(TAG, "result user combId = " + result.getUser().getCombinationId());
			Log.d(TAG, "result user dp = " + result.getUser().getDynamicParam());
			Log.d(TAG, "result mid = " + result.getmId());
			Log.d(TAG, "result etsmid = " + result.getEtsMid());
			
			// save in sharedPreferences
			SharedPreferences settings = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
			String userNameSP = settings.getString(Constants.PREF_USER_NAME, null);
			String userNameView = result.getUser().getUserName();
			String pass = ap.getUser().getEncryptedPassword();
			if (null == userNameSP || (null != userNameSP && !userNameSP.equalsIgnoreCase(userNameView))) {
				SharedPreferences.Editor edit = settings.edit();
				edit.putString(Constants.PREF_USER_NAME, userNameView);
				edit.putString(Constants.PREF_PASSWORD, pass);
				edit.commit();
			}
			try {
				tracker.trackEvent (
					     "Registration",// Category
					     "Success Registration",// Action
					     "Register",// Label
					     1);// Value
			} catch (Exception e) {
				Log.e(getLocalClassName(), "cant send track event Success Registration", e);
			}

	    	PaymentMethod payment = new PaymentMethod();
	    	if (result.getUser().getCurrencyId() == Constants.CURRENCY_YUAN && result.getUser().getCountryId() == Constants.COUNTRY_CHINA_ID) {	    
				Class<Activity> redirectActivity = Utils.getActivityClass("CupDepositActivity", context);				
	    		Intent intent = new Intent(context, redirectActivity);
	    		String type = DepositView.DEPOSIT_CC_NEW;
		    	intent.putExtra(Constants.PUT_EXTRA_SHOW_LOG_OUT_ALERT, true);
				payment.setId(CUP_DEPOSIT);
				payment.setDisplayName("payment.type.cc");
				payment.setTypeId(Constants.PAYMENT_TYPE_DEPOSIT);
				payment.setBigLogo("logo_cup_big");
				DepositView.bankingRowClick(DEPOSIT_CUP, intent, R.drawable.logo_cup_small ,R.string.header_cup_deposit_text, payment, this);
	    	} else {
				Class<Activity> redirectActivity = Utils.getActivityClass("NewCardActivity", context);
	    		Intent intent = new Intent(context, redirectActivity);
	    		String type = DepositView.DEPOSIT_CC_NEW;
		    	intent.putExtra(Constants.PUT_EXTRA_SHOW_LOG_OUT_ALERT, true);
		    	payment.setId(PaymentMethod.PAYMENT_TYPE_CC);
		    	payment.setDisplayName("payment.type.cc");
			    payment.setBigLogo("logo_creditcards_big");
			    payment.setTypeId(Constants.PAYMENT_TYPE_DEPOSIT);
			    int headerCreditCards;
				if(Utils.isRegulated(this)){
					 headerCreditCards = R.drawable.header_creditcards_regulated;
				} else {
					 headerCreditCards= R.drawable.header_creditcards;
				}
		    	DepositView.bankingRowClick(type, intent, headerCreditCards ,R.string.depositNewCardSubHeader, payment, this);
	    	}
		} else {
			 Log.d(getPackageName(), "Error Insert user ");
			 String error = result.getUserMessages()[0].getMessage();
			 String field = result.getUserMessages()[0].getField();
			 if (result.getErrorCode() == Constants.ERROR_CODE_VALIDATION_WITHOUT_FIELD) {
				 field = Constants.EMPTY_STRING;
			 }
			 Utils.showAlert(context, field, error, context.getString(R.string.alert_title));			 			
		}
	}
	
	public void insertRegisterAttempt() {
		RegisterAttemptMethodRequest request = new RegisterAttemptMethodRequest();
		RegisterAttemptMethodResult result = new RegisterAttemptMethodResult(); 
    	
    	EditText firstName   = (EditText)findViewById(R.id.editTextFirstname);
    	EditText lastName   = (EditText)findViewById(R.id.editTextLastname);    
 
    	request.setEmail(editTextEmail.getText().toString());
    	request.setFirstName(firstName.getText().toString());
    	request.setLastName(lastName.getText().toString());
    	request.setMobilePhone(editTextMobile.getText().toString());
    	request.setContactByEmail(true); 	
    	request.setCombinationId(Utils.getCombinationId(this)); 
    	request.setDynamicParameter(Utils.getDynamicParam(this));
    	request.setHttpReferer(Utils.getReferrerParam(this));
    	request.setmId(Utils.getmId(this));
    	request.setEtsMId(Utils.getEtsMId(this));
    	setInsertRegisterRequestAttempt(request);  
    	
    	Log.d(getPackageName(), "-------insertRegisterAttempt--------");
    	Log.d(TAG, "request user combId = " + request.getCombinationId());
		Log.d(TAG, "request user dp = " + request.getDynamicParameter());
		Log.d(TAG, "request mid = " + request.getmId());
		Log.d(TAG, "request etsmid = " + request.getEtsMId());
		
    	Utils.requestService(this, responseH, "registerAttemptCallBack", request, result, "insertRegisterAttempt");  
	}
	
	public abstract void setInsertRegisterRequestAttempt(RegisterAttemptMethodRequest request);
	
    public void registerAttemptCallBack(Object resultObj) {
    	Log.d(getPackageName(), "registerAttemptCallBack");
    	RegisterAttemptMethodResult result = (RegisterAttemptMethodResult) resultObj;    	    	
    	registerAttemptId = result.getRegisterAttemptId();
    	String newmId = result.getmId();
		Utils.setmIdIfValid(this, newmId);
		String newEtsMId = result.getEtsMId();
		Utils.setEtsMId(this, newEtsMId);
		
		Log.d(TAG, "result mid = " + result.getmId());
		Log.d(TAG, "result etsmid = " + result.getEtsMId());
		Log.d(TAG, "saved mid = " + Utils.getmId(this));
		Log.d(TAG, "saved etsmid = " + Utils.getEtsMId(this));
    }
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	Log.d(getPackageName(), "onActivityResult*******************************************");
    	if (requestCode != Constants.REQUEST_CODE_WEB_VIEW) {            
                this.finish();            
        }
    }

              
}