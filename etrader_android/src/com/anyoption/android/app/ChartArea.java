package com.anyoption.android.app;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

import com.anyoption.beans.base.Investment;

public class ChartArea {
    public static final int COLOR_WIN = 0xFFB7DCF4; // 0xFF022838;
    public static final int COLOR_WIN_CRR = 0xFFD7EBF8; // 0xFF0675A3;
    public static final int COLOR_LOSE = 0xFFBECBD3; // 0xFF2E2E2E;
    public static final int COLOR_LOSE_CRR = 0xFFE4E4E4; // 0xFF777777;
    
    public static final int COLOR_INVESTMENT_LINE = 0xFFF6CD10;
    
    protected static final int TYPE_PROFITABLE = 1;
    protected static final int TYPE_NOT_PROFITABLE = 2;
    
    protected float bottomLevel;
    protected float topLevel;
    protected int type;
    protected boolean last;
    protected boolean topLabelVisible;
    
    public ChartArea(float bottomLevel, float topLevel, int type, boolean last) {
        this.bottomLevel = bottomLevel;
        this.topLevel = topLevel;
        this.type = type;
        this.last = last;
        topLabelVisible = true;
        Log.v("data", "ChartArea - bottomLevel: " + bottomLevel + " topLevel: " + topLevel + " typeId: " + type + " last: " + last);
    }
    
    public void onDraw(Canvas canvas, Paint p, float x, float y, float width, float height, float yMinVal, float yMaxVal, double currentLevel, int decimalPoint, boolean selectedInList, int alphaMask) {
        float spread = yMaxVal - yMinVal;
        float ay = y + height * (yMaxVal - topLevel) / spread;
        float ah = height * (topLevel - bottomLevel) / spread;
        
        int clr = getColor(currentLevel);
        p.setColor(clr & alphaMask);
        p.setStyle(Paint.Style.FILL);
        canvas.drawRect(x, ay, x + width, ay + ah, p);

        if (!last) {
            p.setColor(COLOR_INVESTMENT_LINE & alphaMask);
            ChartView.dashTo(canvas, p, x, ay, x + width, ay, 3, 2);
        }
    }
    
    public static boolean isProfitable(double bottomLevel, double topLevel, ArrayList<Investment>investments, long currentMarketId) {
        long totalAmount = 0;
        long totalProfit = 0;
        double checkLevel = bottomLevel + (topLevel - bottomLevel) / 2;
        
        for (int i = 0; i < investments.size(); i++) {
            if (investments.get(i).getMarketId() == currentMarketId) {
                totalAmount += investments.get(i).getAmount();
                if ((checkLevel < investments.get(i).getCurrentLevel() && investments.get(i).getTypeId() == 1) ||
                        (investments.get(i).getCurrentLevel() < checkLevel && investments.get(i).getTypeId() == 2)) {
                    totalProfit += investments.get(i).getAmount() * (1 - investments.get(i).getOddsLose());
                } else {
                    totalProfit += investments.get(i).getAmount() * (1 + investments.get(i).getOddsWin());
                }
            }
        }
        return totalProfit > totalAmount;
    }

    protected int getColor(double crrLvl) {
        boolean current = false;
        if (bottomLevel < crrLvl && crrLvl < topLevel) {
            current = true;
        }
        if (type == TYPE_PROFITABLE) {
            if (current) {
                return COLOR_WIN_CRR;
            } else {
                return COLOR_WIN;
            }
        } else {
            if (current) {
                return COLOR_LOSE_CRR;
            } else {
                return COLOR_LOSE;
            }
        }
    }

    public float getBottomLevel() {
        return bottomLevel;
    }

    public void setBottomLevel(float bottomLevel) {
        this.bottomLevel = bottomLevel;
    }

    public float getTopLevel() {
        return topLevel;
    }

    public void setTopLevel(float topLevel) {
        this.topLevel = topLevel;
    }
}