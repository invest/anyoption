package com.anyoption.android.app;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.CcDepositMethodRequest;
import com.anyoption.json.results.TransactionMethodResult;

/**
 * @author AviadH
 *
 */
public class CcWithdrawalActivity extends WithdrawalBase {
	
	private final static String SERIVCE_NAME = "insertWithdrawCard";	
	private Spinner ccSpinner;					
	private CreditCard currentCC;
	private User user;
	private CcDepositMethodRequest request;
	private AlertDialog validationResultDialog;

	@Override
    public void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);        
	     setContentView(buildLayout(Constants.PAYMENT_TYPE_WITHDRAW));	  
	     PaymentMethod payment = (PaymentMethod) getIntent().getExtras().get(Constants.PUT_EXTRA_BANKING_PAYMENT);
	     findViewById(R.id.imageViewInfo).setTag(payment);
	     activity = this;
	     //set the currency symbol
	     AnyoptionApplication ap = (AnyoptionApplication) getApplication();
	     user = ap.getUser();
	     ((TextView) findViewById(R.id.textViewCurrencySymbol)).setText(user.getCurrencySymbol());
	     
	     Object cards[] = (Object[]) getIntent().getSerializableExtra(Constants.PUT_EXTRA_CC_LIST);

	     ccSpinner = (Spinner) findViewById(R.id.spinnerCcList);
	     ArrayAdapter spinnerCCArrayAdapter = null;
	     
	     if (Utils.isEtraderProject(this)){
	    	spinnerCCArrayAdapter = new ArrayAdapter(this, R.layout.custom_spinner, cards);
	     } else {
	    	 spinnerCCArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, cards);
	     }
	     spinnerCCArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	     ccSpinner.setAdapter(spinnerCCArrayAdapter);
	     
	     
	     ccSpinner.setOnItemSelectedListener(
	        	    new AdapterView.OnItemSelectedListener() {
	        	    	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {          	    	
	        	    		ArrayAdapter<CreditCard> adapter = (ArrayAdapter<CreditCard>) parent.getAdapter();          	    		
	        	    		currentCC = adapter.getItem(pos);
	        	    		TextView message = (TextView) findViewById(R.id.textViewMessage);
	        	    		if (currentCC.getId() != 0 && !currentCC.isCftAvailable()) {
		        	    		message.setVisibility(View.VISIBLE);
		        	    		message.setText(getString(R.string.withdrawals_credit_option_msg, currentCC.getCreditAmountTxt()));
	        	    		} else {
	        	    			message.setVisibility(View.GONE);
	        	    		}
						}
						public void onNothingSelected(AdapterView<?> arg0) {												
						}
	        	    }
	        	);
	     this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
	
	public void ccWithdrawHandler(View target) {
		//Client validations
		//Credit card was selected
		if (currentCC.getId() == 0) {
    		Utils.showAlert(this, getString(R.string.creditcards_type), 
    				getString(R.string.error_mandatory), this.getString(R.string.alert_title));
    		return ;
    	}
    	String error = null;
    	//amount
    	EditText amount = (EditText) findViewById(R.id.editTextAmount);
    	error = Utils.validateDoubleAndEmpty(this, amount.getText().toString(), getString(R.string.withdrawal_amount));
    	if (null != error ){
    		Utils.showAlert(this, "", error, this.getString(R.string.alert_title));
    		return;
    	}
    	
    	request = new CcDepositMethodRequest();
    	request.setAmount(((EditText) findViewById(R.id.editTextAmount)).getText().toString());
    	request.setCardId(currentCC.getId());
    	if (clickedWithdrawal == false && !dialogIsShowing()) {
			clickedWithdrawal = true;
			Utils.requestService(activity, responseH, "validateWithdrawCardCallBack", request, new TransactionMethodResult(), "validateWithdrawCard");
			System.out.println("WITHDRAW REQUEST EXECUTED");
    	}
	}
	
	private boolean dialogIsShowing() {
		return validationResultDialog!= null && validationResultDialog.isShowing();
	}

	/**
	 * In case of success navigate to Important note Popup
	 * @param result
	 */
	public void validateWithdrawCardCallBack(Object result) {
		Log.d(getLocalClassName(), "validateWithdrawCardCallBack");
		TransactionMethodResult tmr = (TransactionMethodResult) result;		
		if (tmr.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(getLocalClassName(), "showImportantNotePopup");
			showImportantNotePopUp(SERIVCE_NAME, R.string.withdraw_cc_success, request, tmr.getFee());
		} else {
			 String error = tmr.getUserMessages()[0].getMessage();
			 String field = tmr.getUserMessages()[0].getField();
			 if (tmr.getErrorCode() == Constants.ERROR_CODE_VALIDATION_WITHOUT_FIELD) {
				 field = Constants.EMPTY_STRING;
			 }			
			 validationResultDialog = Utils.showAlert(this, error, field, this.getString(R.string.alert_title));
			 clickedWithdrawal = false;
		}
	}
		
}
