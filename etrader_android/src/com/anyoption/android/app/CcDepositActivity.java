package com.anyoption.android.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.CardMethodRequest;
import com.anyoption.json.requests.CcDepositMethodRequest;
import com.anyoption.json.results.CardMethodResult;
import com.anyoption.json.results.TransactionMethodResult;

/**
 * @author EranL
 *
 */
public class CcDepositActivity extends ReceiptActivity {
		
	private CreditCard currenctCC;
	private PaymentMethod payment;
	
	//for edit card
	private Button cancel;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(buildLayout(Constants.PAYMENT_TYPE_DEPOSIT));
        responseH = new Handler();
        context = this;
        //for the plus info sign
        payment = (PaymentMethod) getIntent().getExtras().get(Constants.PUT_EXTRA_BANKING_PAYMENT);
        findViewById(R.id.imageViewInfo).setTag(payment);
        
        AnyoptionApplication ap = (AnyoptionApplication) getApplication();         
        Object o[] = (Object[]) getIntent().getSerializableExtra(Constants.PUT_EXTRA_CC_LIST);
        Spinner CcSpinner = (Spinner) findViewById(R.id.spinnerCcList);       
        ArrayAdapter spinnerArrayAdapter = null;
        if (Utils.isEtraderProject(this)) {
        	spinnerArrayAdapter = new ArrayAdapter(this,
        			R.layout.custom_spinner, o);  
        } else {
            spinnerArrayAdapter = new ArrayAdapter(this,
                    android.R.layout.simple_spinner_item, o); 	
        }
 
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        CcSpinner.setAdapter(spinnerArrayAdapter);      
        //Card details
        CcSpinner = (Spinner) findViewById(R.id.spinnerCcList);
        CcSpinner.setOnItemSelectedListener(
        	    new AdapterView.OnItemSelectedListener() {
        	    	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {          	    	
        	    		ArrayAdapter<CreditCard> adapter = (ArrayAdapter<CreditCard>) parent.getAdapter();          	    		
        	    		currenctCC = adapter.getItem(pos);  
        	    		if (currenctCC.getId() != 0){
        	    			displayCardDetails(true);
        	    			setCardDetails();
        	    		} else {
        	    			displayCardDetails(false);
        	    		}
					}
					public void onNothingSelected(AdapterView<?> arg0) {												
					}
        	    }
        	);	 
        //Display relevant currency 
        User user = ap.getUser();
       ((TextView)findViewById(R.id.TextViewCurrencySymbol)).setText(user.getCurrencySymbol());
       ((TextView)findViewById(R.id.TextViewHeader)).setText(getString(R.string.profitLine_deposit).toUpperCase());
       this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    
    private void setCardDetails(){
    	TextView holderName = (TextView) findViewById(R.id.textViewHolderName);
    	holderName.setText(currenctCC.getHolderName());
    	TextView cardType = (TextView) findViewById(R.id.textViewCardType);
    	cardType.setText(currenctCC.getTypeName());
    	TextView cardNum = (TextView) findViewById(R.id.textViewCcNum);
    	cardNum.setText(currenctCC.getCcNumberXXXX());
    	TextView expDate = (TextView) findViewById(R.id.textViewExpDate);
    	expDate.setText(currenctCC.getExpDateStr());
    	TextView ccPass = (TextView) findViewById(R.id.textViewCcPass);
    	ccPass.setText(currenctCC.getCcPass());    
    }
    
    public void insertNewCardClickHandler(View target) {
    	Class<Activity> actvityRedirect = Utils.getActivityClass(Constants.ACTIVITY_REDIRECT_NEWCARD, this);
    	Intent intent = new Intent(this, actvityRedirect).setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);    	
    	intent.putExtra(Constants.PUT_EXTRA_BANKING_PAYMENT, payment);
    	intent.putExtra(Constants.PUT_EXTRA_LAYOUT_BANKING, "new_card");
    	
		int headerCreditCards;
		if(Utils.isRegulated(this)){
			 headerCreditCards = R.drawable.header_creditcards_regulated;
		} else {
			 headerCreditCards= R.drawable.header_creditcards;
		}
		
		intent.putExtra(Constants.PUT_EXTRA_BANKING_HEADER_IMAGE_ID, headerCreditCards);
		intent.putExtra(Constants.PUT_EXTRA_BANKING_SUB_HEADER, R.string.depositNewCardSubHeader);
		context.startActivity(intent);
    }
    
    public void  ccDepositHandler(View target){
    	//client validation
    	//selecting card
    	if (currenctCC.getId() == 0){
    		Utils.showAlert(this, getString(R.string.creditcards_type), 
    				getString(R.string.error_mandatory), context.getString(R.string.alert_title));
    		return ;
    	}
    	String error = null;
    	//amount
		EditText depositAmount = (EditText)findViewById(R.id.editTextAmount);		
    	error = Utils.validateDoubleAndEmpty(this, depositAmount.getText().toString(), getString(R.string.deposit_deposit));
    	if (null != error ){
    		Utils.showAlert(this, "", error, context.getString(R.string.alert_title));
    		return;
    	}    	
    	//server validation 
    	CcDepositMethodRequest request = new CcDepositMethodRequest();    	
		request.setAmount(depositAmount.getText().toString());  
		request.setCardId(currenctCC.getId());
		request.setFirstNewCardChanged(false); //For repeat failure  
		Utils.requestService(this, responseH, "insertDepositCallBack", request, new TransactionMethodResult(), "insertDepositCard");
    } 
        
    public void editCardClickHandler(View target){    	
    	LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.edit_card, null);
     	TextView holderName = (TextView) ll.findViewById(R.id.textViewHolderName);
    	holderName.setText(currenctCC.getHolderName());
    	TextView cardType = (TextView) ll.findViewById(R.id.textViewCardType);
    	cardType.setText(currenctCC.getTypeName());
    	TextView cardNum = (TextView) ll.findViewById(R.id.textViewCcNum);
    	cardNum.setText(currenctCC.getCcNumberXXXX());
    	
    	Spinner spinnerMonth = (Spinner) ll.findViewById(R.id.spinnerMonth);    	
    	ArrayAdapter monthAdap = new ArrayAdapter(this,R.layout.custom_spinner_item, getResources().getStringArray(R.array.monthes)); 
    	monthAdap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	spinnerMonth.setAdapter(monthAdap);
    	int monthPosition = monthAdap.getPosition(currenctCC.getExpMonth());
    	spinnerMonth.setSelection(monthPosition);
    	
    	Spinner spinnerYear = (Spinner) ll.findViewById(R.id.spinnerYear);
        ArrayAdapter spinnerYearTypeArrayAdapter = new ArrayAdapter(this,R.layout.custom_spinner_item, Utils.getCreditCardYearsArray(this));  
        spinnerYearTypeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
	    ((Spinner) ll.findViewById(R.id.spinnerYear)).setAdapter(spinnerYearTypeArrayAdapter);    	    	 
    	int yearPosition = spinnerYearTypeArrayAdapter.getPosition(currenctCC.getExpYear());  
    	spinnerYear.setSelection(yearPosition);    	
    	EditText ccPass = (EditText)ll.findViewById(R.id.editTextCcPass);
    	ccPass.setText(currenctCC.getCcPass());    
    	 
         cancel = (Button) ll.findViewById(R.id.buttonCancel);
         Button confirm = (Button) ll.findViewById(R.id.buttonConfirm);
         confirm.setOnClickListener(new OnClickListener() {
 			public void onClick(View v) {
 				//client validation 	
 				String error = null;
 				LinearLayout ll = (LinearLayout)v.getParent().getParent(); 				
 		    	EditText ccPass = (EditText) ll.findViewById(R.id.editTextCcPass); 
 		    	Spinner spinnerMonth = (Spinner) ll.findViewById(R.id.spinnerMonth); 
 		    	Spinner spinnerYear = (Spinner) ll.findViewById(R.id.spinnerYear);
 		    	Log.i("", currenctCC.getTypeId() +  " " + ccPass.getText().toString())  ;
 		    	//CVV
 		    	error = Utils.validateCVV(context, ccPass.getText().toString(), currenctCC.getTypeId());
 		    	if (null != error){
 		    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title)); 				
 					return;
 		    	}
 		    	//validate year
 		        int year = spinnerYear.getSelectedItemPosition();
 		        if (year == 0) {
 		        	String[] params = new String[1];
 					params[0] = context.getString(R.string.creditcards_year);
 					error = String.format(context.getString(R.string.error_single_required), params);	 		        	
 					Utils.showAlert(context, "", error, context.getString(R.string.alert_title));
 		            return ;
 		        }
 		        //validate month
 		        int month = spinnerMonth.getSelectedItemPosition();
 		        if (month == 0) {
 		        	String[] params = new String[1];
 					params[0] = context.getString(R.string.month);
 					error = String.format(context.getString(R.string.error_single_required), params);	 		        	
 					Utils.showAlert(context, "", error, context.getString(R.string.alert_title));		        	
 		            return ;
 		        } 
 		        //Check exp date
 		        error = Utils.validateCCExpDate(context, Integer.parseInt(spinnerYear.getSelectedItem().toString()),
                        Integer.parseInt(spinnerMonth.getSelectedItem().toString()));
 		        if (null != error){
 		        	Utils.showAlert(context, "", error, context.getString(R.string.alert_title));		        	
			        return;
 		        }
 		        //server validation 
 				currenctCC.setCcPass(ccPass.getText().toString());
 				currenctCC.setExpMonth(spinnerMonth.getSelectedItem().toString());
 				currenctCC.setExpYear(spinnerYear.getSelectedItem().toString());
 				CardMethodRequest request = new CardMethodRequest();
 				request.setCard(currenctCC);
 				Utils.requestService(context, responseH, "updateCardCallBack", request, new CardMethodResult(), "updateCard");	
 			} 			
         });
         Utils.showCustomAlert(this, ll, cancel);                        
	}
    
    public void updateCardCallBack(Object resultObj){
		Log.d(getPackageName(), "updateCardCallBack");
		CardMethodResult result = (CardMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {			
			Log.d(getPackageName(), "Update card successfully");			
			setCardDetails();
			cancel.performClick();		
		} else {
			Log.d(getPackageName(), "Error Updating card");
			String error = result.getUserMessages()[0].getMessage();
			AlertDialog alertDialog = new AlertDialog.Builder(context).create();
			alertDialog.setMessage(error);
			alertDialog.setTitle(context.getString(R.string.alert_title));
			alertDialog.setButton(context.getString(R.string.alert_button), new DialogInterface.OnClickListener() {   
			      public void onClick(DialogInterface dialog, int which) {  
			        return;  
			      } });   
			alertDialog.show();			
		}		    	
    }
    
    private void displayCardDetails(boolean displayDetails){
    	LinearLayout ll = (LinearLayout) findViewById(R.id.linearLayoutCardDetails);
    	if (displayDetails){
    		ll.setVisibility(View.VISIBLE);
    	} else {
    		ll.setVisibility(View.GONE);
    	}
    }   
}
