/**
 * 
 */
package com.anyoption.android.app;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.View;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.MailBoxUser;
import com.anyoption.json.requests.InboxMethodResult;
import com.anyoption.json.requests.UserMethodRequest;

/**
 * @author AviadH
 *
 */
public class InboxView extends MenuViewBase{
	
	public static final int ACTION_REMOVE_NEW = 0;
	public static final int ACTION_DELETE = 1;
	
	private ArrayList<MailBoxUser> emails;
	private ArrayList<Pair<MailBoxUser, Integer>> emailsForView;
	private int cntUnreadEmails;
	protected boolean isFirst;

	public InboxView(Context context) {
		this(context, null);
	}
	
	public InboxView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initComponent(context);
	}
	
	public void initComponent(Context context) {
		Log.d("InboxView", "init Component");
		emailsForView = new ArrayList<Pair<MailBoxUser,Integer>>(); 
		isFirst = true;
		//set table params
		tlMain.setColumnStretchable(0, true);
		tlMain.setColumnStretchable(1, true);
		UserMethodRequest request = new UserMethodRequest();
    	Utils.requestService(this, responseH, "inboxCallBack", request, new InboxMethodResult(), "getUserInboxMails", (Activity) context);
	}
	
	public String getMenuStripText() {
		return getResources().getString(R.string.inbox_header, "");
	}

	public void loadXRows() {
		showLoadMore = false;
		buildViewMails();
		setMailBox();
	}
	
	public void setMailBox() {
		tlMain.removeAllViews();
		int size = startRow + PAGE_SIZE;
		if (size >= emailsForView.size()) {
			size = emailsForView.size();
		} else {
			showLoadMore = true;
		}
		
		if (null != fromCal && null != toCal) {// in case we are after more bonuses popup 
			makeFirstDatesRow(fromCal, toCal);
		}
		for (int i = startRow ; i <  size ; i++) {
			makeRowForInbox(emailsForView.get(i).first, emailsForView.get(i).second);
	    }
		
		TextView groupName = (TextView) menuLine.findViewById(R.id.textViewGroupName);
		if (cntUnreadEmails > 0) {
			groupName.setText(getResources().getString(R.string.inbox_header, "(" + Integer.toString(cntUnreadEmails) + ")"));
		} else {
			groupName.setText(getResources().getString(R.string.inbox_header, ""));
		}
		makeLastRow(getResources().getString(R.string.inbox_more_last_row), getResources().getString(R.string.inbox_header, ""));
		startRow += PAGE_SIZE;
	}
	
	
	public void buildViewMails() {
		cntUnreadEmails =0;
		emailsForView = new ArrayList<Pair<MailBoxUser,Integer>>();
		for ( int i = 0 ; i < emails.size() ; i++) {
			if (!(emails.get(i).getStatusId() == MailBoxUser.MAILBOX_STATUS_DETETED)) {
				if (fromCal != null && toCal != null) { // if we get dates params
					if (emails.get(i).getTimeCreated().compareTo(fromCal.getTime()) >= 0 &&
							emails.get(i).getTimeCreated().compareTo(toCal.getTime()) <= 0) {
						
						emailsForView.add(new Pair<MailBoxUser, Integer>(emails.get(i), i));
					}	
				} else { //for the first time to load emails
					emailsForView.add(new Pair<MailBoxUser, Integer>(emails.get(i), i));
				}
				if (emails.get(i).getStatusId() == MailBoxUser.MAILBOX_STATUS_NEW) { // for unread mail
					cntUnreadEmails++;
				}
			}	
		}
	}

	public void inboxCallBack(Object result) {
		InboxMethodResult res = (InboxMethodResult) result;
		Log.d("InboxView", "InboxViewCallBack - number of email:" + res.getEmails().size());
		if (res.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
			 Utils.showAlert(context, res.getUserMessages()[0].getField(), 
					 res.getUserMessages()[0].getMessage(), context.getString(R.string.alert_title));
	    } else { 
	    	emails = res.getEmails();
	    }
		if (emails.size() == 0 && isFirst) {
			makeNoInformationRow(getNoInformationString());
		} else {
			isFirst = false;
			loadXRows();
		}
	}
	
	public void makeRowForInbox(final MailBoxUser mailBoxUser, final int index) {
		if (!(mailBoxUser.getStatusId() == MailBoxUser.MAILBOX_STATUS_DETETED)) {
			TableRow tr = (TableRow) inflater.inflate(R.layout.bonus_row, tlMain, false);
			if (mailBoxUser.getStatusId() == MailBoxUser.MAILBOX_STATUS_NEW) {
				tr.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_strong_white));
			}
			TextView date = (TextView) tr.findViewById(R.id.textViewBonusValid);
			TextView subject = (TextView) tr.findViewById(R.id.textViewBonusName);
			
			//set text size and content
			date.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15f);
			subject.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15f);
			
			date.setText(mailBoxUser.getTimeCreatedTxt());
			subject.setText(mailBoxUser.getSubjectShortTxt());
			
			tr.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Intent intent = new Intent(context, InboxActivity.class); 
					Object[] emailsObjects = emails.toArray();
					intent.putExtra("emails", emailsObjects);
					intent.putExtra("index", index);
					((Activity) context).startActivityForResult(intent, Constants.REQUEST_CODE_INBOX_CONTENT);
				}
			});
		tlMain.addView(tr);
		}
	}
	
	public void markRowsAsRead(ArrayList<Integer> updates) {
		for (int i= 0 ; i < updates.size() ; i++) {
			((MailBoxUser)emails.get(updates.get(i))).setStatusId(MailBoxUser.MAILBOX_STATUS_READ);
		}
	}
	
	public void markAsDeleted(int index) {
		((MailBoxUser)emails.get(index)).setStatusId(MailBoxUser.MAILBOX_STATUS_DETETED);
	}
	
   @Override
    public void openGroup() {
    	super.openGroup();
    	if (!isFirst) {
	    	restartLoadMore();
	    	loadXRows();
    	}
    }
   
   @Override
   public String getNoInformationString() {
	   return getResources().getString(R.string.inbox_emptylist);
   }

@Override
public void removeAllRows() {
	// TODO Auto-generated method stub
	
}
}
