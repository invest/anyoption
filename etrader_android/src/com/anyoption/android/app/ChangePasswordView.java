package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableRow;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.ChangePasswordMethodRequest;
import com.anyoption.json.results.UserMethodResult;

/**
 * @author AviadH
 *
 */
public class ChangePasswordView extends MenuViewBase {
	
	private String token = "Change Password View";


	public ChangePasswordView(Context context) {
		this(context, null);
	}

	public ChangePasswordView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initComponent(attrs);
	}

	public void initComponent(AttributeSet attrs) {
		Log.d("BonusView", "init Component");;

		//set table params
		tlMain.setColumnStretchable(0, true);
		tlMain.setColumnShrinkable(1, true);

		TableRow changePass = (TableRow) inflater.inflate(R.layout.change_password, this, false);
		Button buttonSubmit = (Button) changePass.findViewById(R.id.buttonSubmit);
		buttonSubmit.setOnClickListener(new OnClickListener() {			
			public void onClick(View view) {
				changePasswordClickHandler(view);
			}
		});
		tlMain.addView(changePass);
	}

	public String getMenuStripText() {
		return getResources().getString(R.string.changepass_header);
	}

	public void changePasswordClickHandler(View target) {    	    	
		validateChangePasswordForm();
	}

	public void validateChangePasswordForm(){
		EditText password   = (EditText)findViewById(R.id.editTextOldPassword);
		EditText passwordNew   = (EditText)findViewById(R.id.editTextNewPassword);
		EditText passwordReType   = (EditText)findViewById(R.id.editTextRetypePassword);

		//Client validations
		String error = Utils.validatePassword(context, password.getText().toString()); //Old Pass
		if (error!= null){
			Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
			return ;
		}
		error = Utils.validatePassword(context, passwordNew.getText().toString()); //New Pass
		if (error!= null){
			Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
			return ;
		}
		//Retype password 
		error = Utils.validateRetypePassword(context, passwordNew.getText().toString(), passwordReType.getText().toString());
		if (error!= null){
			Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
			return ;
		}

//Service validations
	    	AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context).getApplication();
            User user = ap.getUser();
	        ChangePasswordMethodRequest request = new ChangePasswordMethodRequest();
	        request.setUserName(user.getUserName());
	        request.setSkinId(user.getSkinId());	
	        request.setPassword(password.getText().toString());
	        request.setPasswordNew(passwordNew.getText().toString()); 
	        request.setPasswordReType(passwordReType.getText().toString());
	        
	        Utils.requestService(this, responseH, "changePasswordCallBack", request, new UserMethodResult(), "getChangePassword", (Activity) context);  
	}

	public void changePasswordCallBack(Object resultObj){
			Log.d(token, "insertUserCallBack");
			UserMethodResult result = (UserMethodResult) resultObj;
			if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
				Log.d(token, "Changed password successfully");
				// put the new encrypted password in application and remove from shared preferences
				SharedPreferences settings = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
				AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context).getApplication();
			    SharedPreferences.Editor edit = settings.edit();
	            edit.putString(Constants.PREF_PASSWORD, result.getUser().getPassword());
	            edit.commit();
	            ap.setUser(result.getUser());
	            ap.setUserRegulation(result.getUserRegulation());
	            
				String error = result.getUserMessages()[0].getMessage();
				String field = "";
				Utils.showAlertAndGoLogin(context, field, error);		
			} else {			
				Log.d(token, "Changed password Failed");	
				 String error = result.getUserMessages()[0].getMessage();
				 Utils.showAlert(context, "", error, context.getString(R.string.alert_title));			 			
			}
		}

	@Override
	public void removeAllRows() {
		// TODO Auto-generated method stub
		
	}
}