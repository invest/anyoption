/**
 * 
 */
package com.anyoption.android.app;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.MailBoxUser;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.MailBoxMethodRequest;
import com.anyoption.json.results.MethodResult;

/**
 * @author AviadH
 *
 */
public class InboxActivity extends BaseActivity{
	
	private Object[] emails;
	private int indexCurrentMail;
	private ArrayList<Integer> emailsIndexesUpdateRead;
	private Integer emailDeleted;
	private User user;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.email_content);
        emailsIndexesUpdateRead = new ArrayList<Integer>();
        emailDeleted = null;
        user = ((AnyoptionApplication) getApplication()).getUser();
        emails = (Object[]) getIntent().getSerializableExtra("emails");
        indexCurrentMail =  (Integer) getIntent().getSerializableExtra("index");
        setTextInformation((MailBoxUser)emails[indexCurrentMail]);
    }
	 
	public void setTextInformation(MailBoxUser mailBoxUser) {
		((TextView) findViewById(R.id.textViewInboxFrom)).setText(mailBoxUser.getSenderName().split("\\.")[2]);
		((TextView) findViewById(R.id.textViewInboxReceived)).setText(mailBoxUser.getTimeCreatedTxt());
		((TextView) findViewById(R.id.textViewInboxContent)).setText(mailBoxUser.getTemplateContent());
		((TextView) findViewById(R.id.textViewInboxTo)).setText(user.getFirstName());
		((TextView) findViewById(R.id.textViewSubject)).setText(mailBoxUser.getSubject());
		updateStatusIfNedded(mailBoxUser, MailBoxUser.MAILBOX_STATUS_NEW, MailBoxUser.MAILBOX_STATUS_READ);
	}
	
	public void updateStatusIfNedded(MailBoxUser mailBoxUser, int statusFrom, int statusTo) {
		if (mailBoxUser.getStatusId() == statusFrom) {
			if (statusTo == MailBoxUser.MAILBOX_STATUS_READ) {
				emailsIndexesUpdateRead.add(indexCurrentMail);
			}
			mailBoxUser.setStatusId(statusTo);
			MailBoxMethodRequest request = new MailBoxMethodRequest();
			request.setMailBoxUser(mailBoxUser);
			Utils.requestService(this, responseH, "changeStatusCallBack", request, new MethodResult(), "updateMailBox");
			saveDataToActivty();
		}
	}
	
	public void changeStatusCallBack(Object result) {
		MethodResult res = (MethodResult) result;
		if (res.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
			 Utils.showAlert(this, res.getUserMessages()[0].getField(), 
					 res.getUserMessages()[0].getMessage(), this.getString(R.string.alert_title));
	    }
	}
	
	public void makeInboxAction(View target) {
		if (target.getId() == R.id.linearLayoutPrevious) {
			goPrev();
		} else if (target.getId() == R.id.linearLayoutDelete) {
			delete();
		} else if (target.getId() == R.id.linearLayoutNext) {
			goNext();
		}
	}
	public void delete() {
		MailBoxUser mb = (MailBoxUser) emails[indexCurrentMail];
		emailDeleted = indexCurrentMail;
		updateStatusIfNedded(mb, MailBoxUser.MAILBOX_STATUS_READ, MailBoxUser.MAILBOX_STATUS_DETETED);
		finish();
	}
	
	public void goPrev() {
		indexCurrentMail--;
		if (indexCurrentMail == -1) { // in case i am in the start 
			Utils.showAlert(this, getString(R.string.inbox_content_warning), 
					getString(R.string.inbox_content_prev_error), this.getString(R.string.alert_title));
			indexCurrentMail = 0;
			return;
		}
		if (((MailBoxUser) emails[indexCurrentMail]).getStatusId() == MailBoxUser.MAILBOX_STATUS_DETETED) {
			goPrev();
		} else { 
			setTextInformation((MailBoxUser) emails[indexCurrentMail]);
		}
	}
	
	public void goNext() { 
		indexCurrentMail++;
		if (indexCurrentMail == emails.length) { // in case i am in the end
			Utils.showAlert(this, getString(R.string.inbox_content_warning), 
					getString(R.string.inbox_content_next_error), this.getString(R.string.alert_title));
			indexCurrentMail = emails.length - 1;
		}
		if (((MailBoxUser) emails[indexCurrentMail]).getStatusId() == MailBoxUser.MAILBOX_STATUS_DETETED) {
			goNext();
		} else {
			setTextInformation((MailBoxUser) emails[indexCurrentMail]);
		}
	}
	
	public void saveDataToActivty()	{
		Log.d("InboxActivity", "Sending data to myAccount");
		Intent data = new Intent();
		data.putExtra(Constants.RESULT_UPDATES_ARRAY, emailsIndexesUpdateRead);
		data.putExtra(Constants.RESULT_UPDATES_DELETE, emailDeleted);
		setResult(Activity.RESULT_OK, data);
	}
}