/**
 * 
 */
package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.User;

/**
 * @author AviadH
 *
 */
public class ContactButtons extends LinearLayout{
	
	private LayoutInflater inflater;
	private Context context;
	
	public ContactButtons(Context context) {
		this(context, null);
	}
	
	public ContactButtons(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		initComponent();
	}
	
	public void initComponent() {
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
		LinearLayout content = (LinearLayout) inflater.inflate(R.layout.contact_buttons, null, false);
		((Button) content.findViewById(R.id.buttonMyAccountEmail)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				((Activity) context).startActivity(new Intent(context, SendEmailActivity.class));
			}
		});
        ((Button) content.findViewById(R.id.buttonMyAccountPhone)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				openCallUs(v);
			}
		});
        addView(content);
	}
	
	 public void openCallUs(View target){
	    	AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context).getApplication();
	    	User user = ap.getUser();
	    	LinearLayout checkPass = (LinearLayout) inflater.inflate(R.layout.call_us_pop_up, null, false);
	    	final TextView supportPhone = (TextView) checkPass.findViewById(R.id.textViewSupportPhone);
	    	if (user != null && !Utils.isEtraderProject(ap)) {
	    		supportPhone.setText(ap.getCountries().get(user.getCountryId()).getSupportPhone());
	    	} else {
	    		supportPhone.setText(getResources().getString(R.string.contact_phone_default));
	    	}
			Button cancelDialogButton = (Button) checkPass.findViewById(R.id.buttonMyAccountPersonalDetailsPopupClose);
			Button submitDialogButton = (Button) checkPass.findViewById(R.id.buttonCallUsPopupSubmit);
			Utils.showCustomAlert((Activity) context, checkPass, cancelDialogButton);
			submitDialogButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						callService(supportPhone.getText().toString());
					}
			}); 				        	
	    }

	    private void callService(String phoneNumber){
	    	 Intent callIntent = new Intent(Intent.ACTION_CALL);
	    	 callIntent.setData(Uri.parse("tel:" + phoneNumber ));
	    	 ((Activity) context).startActivity(callIntent);
	    }
}
