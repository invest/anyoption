package com.anyoption.android.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.RequestServiceTask;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.UserMethodRequest;
import com.anyoption.json.results.UserMethodResult;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

/**
 * @author EranL
 */
public class BaseActivity extends Activity {
    protected boolean refreshHeaderOnResume;
    protected Handler responseH;
    protected static GoogleAnalyticsTracker tracker;

    public BaseActivity() {
        refreshHeaderOnResume = true;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        responseH = new Handler();
        trackPage(getLocalClassName());
    }
    

	public void trackPage(String page) {
		try {
			if (null == tracker) {
	        	tracker = GoogleAnalyticsTracker.getInstance();
	        }
			tracker.startNewSession(getString(R.string.UANumber), this);
			tracker.trackPageView(page);
			tracker.dispatch();
		} catch (Exception e) {
			Log.d(getLocalClassName(), "can't start Google Analytics Tracker", e);
		}
	}
    
    /**
     * Handle the optionsMenu initiate
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();          
        inflater.inflate(R.menu.menu, menu);  
        AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        User user = ap.getUser();  
        if (null != user) {
        	menu.findItem(R.id.myAccountMItem).setVisible(true);
        } else {
        	MenuItem register = menu.findItem(R.id.registerMItem);
        	register.setVisible(true);
        	String title = String.valueOf(register.getTitle()).toLowerCase();
        	register.setTitle(title.substring(0, 1).toUpperCase() + title.substring(1, title.length()));            	        	
        }                
        return true;
    }
    
    /**
     * called when optionItem selected
     * load the proper activity by the selected item
     */
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	if(item.getItemId() == R.id.tradeMItem) {
        	 startActivity (new Intent (this, MarketsGroupActivity.class));
    	} else if (item.getItemId() == R.id.otherMItem) {
        	startActivity (new Intent (this, OtherMenuActivity.class));
    	} else if (item.getItemId() == R.id.myAccountMItem) {   
           	 startActivity (new Intent (this, MyAccountActivity.class));
    	} else if (item.getItemId() == R.id.contactMItem) {	
        	startActivity (new Intent (this, ContactUsActivity.class));
    	} else if (item.getItemId() == R.id.bankingMItem) {       	
	       	 startActivity (new Intent (this, BankingActivity.class));
    	} else if (item.getItemId() == R.id.registerMItem) {   
        	Utils.showNotLoginAlert(null, this);	    	 
    	} else {
    		return false;
    	}
    	
    	return true;            
    }

    @Override
    protected void onResume() {
        super.onResume();
        
        HeaderLayout hl = (HeaderLayout) findViewById(R.id.headerLayout);
        if (refreshHeaderOnResume && null != ((AnyoptionApplication) getApplication()).getUser() && null != hl) {
            refreshUser();
        }
        if (null != hl) {
            hl.onResume();
        }
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        HeaderLayout hl = (HeaderLayout) findViewById(R.id.headerLayout);
        if (null != hl) {
            hl.onPause();
        }
        RequestServiceTask.cancelAllRequests();
    }

    public void refreshUser() {
        Utils.requestService(this, responseH, "getUserCallback", new UserMethodRequest(), new UserMethodResult(), "getUser", this);
    }
    
    public void getUserCallback(Object resultObj) {
        UserMethodResult result = (UserMethodResult) resultObj;
 
        ((AnyoptionApplication) getApplication()).setUser(result.getUser());
        ((AnyoptionApplication) getApplication()).setUserRegulation(result.getUserRegulation());

        ((HeaderLayout) findViewById(R.id.headerLayout)).refresh();
    }
    
    public void goToInfoActivity(View v) {
    	Intent intent = new Intent(this, InfoPageActivity.class);    	
    	intent.putExtra(Constants.PUT_EXTRA_INFO_PAGE, getInfoPageId());
    	startActivity(intent);	    	  
    }

	public int getInfoPageId() {
		return R.layout.security;
	}
	
	public LinearLayout buildLayout(int paymentType) {
		Intent intent = getIntent();
		String type = intent.getStringExtra(Constants.PUT_EXTRA_LAYOUT_BANKING);
		int imageId = intent.getIntExtra(Constants.PUT_EXTRA_BANKING_HEADER_IMAGE_ID, 0);
		int subHeaderId = intent.getIntExtra(Constants.PUT_EXTRA_BANKING_SUB_HEADER, 0);
		LayoutInflater inflater = getLayoutInflater();
		LinearLayout linearLayoutResult = (LinearLayout) inflater.inflate(R.layout.banking_row_click_template, null, false);
		TextView header = (TextView) linearLayoutResult.findViewById(R.id.TextViewHeader);
		ImageView imageIcon = (ImageView) linearLayoutResult.findViewById(R.id.imagePaymentSign);
		TextView subHeader = (TextView) linearLayoutResult.findViewById(R.id.textViewSubHeader);
		
		if (paymentType == Constants.PAYMENT_TYPE_WITHDRAW) {
			header.setText(getString(R.string.menuStrip_withdrawal_capital));
		} else {
			header.setText(getString(R.string.profitLine_deposit).toUpperCase());;
		}		
		if (subHeaderId > 0) {
			subHeader.setText(getString(subHeaderId).toUpperCase());
			if (subHeaderId == R.string.deposit_bank_wire || (Utils.isEtraderProject(this) && subHeaderId == R.string.header_envoy)) {				
				linearLayoutResult.findViewById(R.id.imageViewInfo).setVisibility(View.GONE);
				View paymentImage = linearLayoutResult.findViewById(R.id.LinearLayoutImagePaymentSign);
				((LinearLayout) paymentImage).setGravity(Gravity.RIGHT);
			}
		} else {
			subHeader.setVisibility(View.GONE);
		}
		if (imageId > 0) {
			imageIcon.setImageResource(imageId);
		} else {
			imageIcon.setVisibility(View.GONE);
		}
		
		LinearLayout include = (LinearLayout) linearLayoutResult.findViewById(R.id.linearLayoutInclude);
		LinearLayout content = null;
		try {
			content = (LinearLayout) inflater.inflate(Utils.getLayoutFromResource(this, "banking_" + type), include, false);
			include.addView(content);
		} catch (Exception e) {
			Log.w("BaseAcivity","Layout banking_" + type + " loading problem -- excpetion " + e );
		}
		return linearLayoutResult;
	}
	
	 public void paymentMethodPopupHandler(View target){
    	Intent intent; 	
		intent = new Intent(this, PaymentMethodPopupActivity.class);
    	intent.putExtra(Constants.PUT_EXTRA_BANKING_PAYMENT, (PaymentMethod) target.getTag());
    	startActivity(intent);
    }

	@Override
	 protected void onDestroy() {
		 super.onDestroy();
		 // Stop the tracker when it is no longer needed.
		 tracker.stopSession();
	}
	 
}