package com.anyoption.android.app;

import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.anyoption.android.app.util.SetTextRunnable;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.json.results.ChartDataMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

public class AssetPageChartView extends BaseChartView {
    private TextView timeLeft;
    private ProgressBar timeLeftProgress;
    private TextView waitingForExpiry;
    private Date timeLastInvest;
    private TextView timeOpen;
    private Last10MinProgressUpdater progressUpdater;
    private boolean hasInsurances;
 
    public AssetPageChartView(Context context) {
        this(context, null);
    }

    public AssetPageChartView(Context context, AttributeSet attrs) {
        super(context, attrs, R.layout.asset_page_chart, R.drawable.call, R.drawable.call_on, R.drawable.put, R.drawable.put_on, R.drawable.call_transparent, R.drawable.put_transparent);
        
        hasInsurances = false;
        chart.setOnAssetPage(true);
        timeLeft = (TextView) findViewById(R.id.assetPageChartTimeLeftTime);
        timeLeftProgress = (ProgressBar) findViewById(R.id.assetPageChartTimeLeftProgress);
        waitingForExpiry = (TextView) findViewById(R.id.assetPageChartWaitingForExpiry);
        timeOpen = (TextView) findViewById(R.id.assetPageChartNextOpen);
        
        RotatePhoneOnClickListener ocl = new RotatePhoneOnClickListener();
        chart.setOnClickListener(ocl);
        View v = findViewById(R.id.baseChartCurrentReturnIcon);
        if (null != v) {
            v.setOnClickListener(ocl);
        }
    }
    
    public void setChartData(ChartDataMethodResult data) {
        super.setChartData(data);
        
        if (data.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS) {
            findViewById(R.id.assetPageChartPlusIcon).setBackgroundResource(R.drawable.icon_optionplus);
        }
        findViewById(R.id.assetPageChartInfoButton).setBackgroundResource(data.getOpportunityTypeId() == Opportunity.TYPE_REGULAR ? R.drawable.icon_information : R.drawable.icon_information_yellow);
        timeLastInvest = data.getTimeLastInvest();
        findViewById(R.id.assetPageChartClosingLevel).setVisibility(GONE);
    }
    
    public void updateItem(int itemPos, String itemName, UpdateInfo update) {
        long oppId = Integer.parseInt(update.getNewValue("ET_OPP_ID"));
        if (opportunityId != 0 && oppId != opportunityId) {
            Log.v("chart", "opportunityId: " + opportunityId + " oppId: " + oppId);
            return;
        }
        int opportunityState = Integer.parseInt(update.getNewValue("ET_STATE"));
        if (update.isValueChanged("ET_STATE")) {
            if (opportunityState == Opportunity.STATE_CREATED) {
                handler.post(new ShowCreatedRunnable());
                try {
                    handler.post(new SetTextRunnable(timeOpen, getResources().getString(R.string.chart_next_open, Utils.reformatTimeLong(getContext(), update.getNewValue("ET_EST_CLOSE")))));
                } catch (Exception e) {
                    Log.e("chart", "Can't process ET_EST_CLOSE.", e);
                }
                handler.post(new SetTextRunnable(expTime, ""));
            } else if (opportunityState == Opportunity.STATE_OPENED) {
                handler.post(new ShowOpenedRunnable());
            } else if (opportunityState == Opportunity.STATE_LAST_10_MIN) {
                // last 10 min - show progress
                handler.post(new ShowLast10MinRunnable());
            } else if (opportunityState == Opportunity.STATE_CLOSING_1_MIN || opportunityState == Opportunity.STATE_CLOSING) {
                // waiting for expiry - show waiting for expiry
                handler.post(new ShowWaitingForExpiryRunnable());
            } else if (opportunityState == Opportunity.STATE_CLOSED) {
                // closed - show closing level
                handler.post(new ShowClosingLevelRunnable(update.getNewValue(skinGroup.getLevelUpdateKey())));
            } else if (opportunityState == Opportunity.STATE_SUSPENDED) {
                handler.post(new SetTextRunnable(timeOpen, composeSuspendedMessage(update.getNewValue("ET_SUSPENDED_MESSAGE"))));
                handler.post(new SetTextRunnable(expTime, ""));
                handler.post(new ShowCreatedRunnable());
            }
        }

        super.updateItem(itemPos, itemName, update);
        
        if (update.isValueChanged("ET_ODDS") || (returnPercent != null && returnPercent.getText().length() == 0)) {
            Log.v("chart", "odds: " + update.getNewValue("ET_ODDS"));
            handler.post(new SetTextRunnable(returnPercent, update.getNewValue("ET_ODDS")));
        }
        if (update.isValueChanged("ET_EST_CLOSE")) {
            Log.v("chart", "time: " + update.getNewValue("ET_EST_CLOSE"));
            if (opportunityState != Opportunity.STATE_CREATED && opportunityState != Opportunity.STATE_SUSPENDED) {
                try {
                    handler.post(new SetTextRunnable(expTime, Utils.reformatTimeLong(getContext(), update.getNewValue("ET_EST_CLOSE"))));
                } catch (Exception e) {
                    Log.e("chart", "Can't process ET_EST_CLOSE.", e);
                }
            }
        }
    }
    
    @Override
    protected void showCurrentReturn() {
        super.showCurrentReturn();
        chart.setOnClickListener(null);
    }

    @Override
    protected void hideCurrentReturn() {
        super.hideCurrentReturn();
        chart.setOnClickListener(new RotatePhoneOnClickListener());
    }

    private void showRotatePhonePopup() {
        LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
        LinearLayout l = (LinearLayout) inflater.inflate(R.layout.rotate_phone_popup, null);
        Button closeButton  = (Button) l.findViewById(R.id.buttonRotatePhonePopup);
        Utils.showCustomAlert(getContext(), l, closeButton);
    }

    public boolean isHasInsurances() {
        return hasInsurances;
    }

    public void setHasInsurances(boolean hasInsurances) {
        this.hasInsurances = hasInsurances;
    }

    /**
     * Bring back the look as it is in the layout (opened state).
     */
    private void resetChartOppState() {
        // created
        timeOpen.setVisibility(GONE);
        findViewById(R.id.assetPageChartClosingLevel).setVisibility(GONE);
        level.setVisibility(VISIBLE);
        
        // last 10 min
        findViewById(R.id.assetPageChartTimeLeft).setVisibility(GONE);
        timeLeft.setVisibility(GONE);
        timeLeftProgress.setVisibility(GONE);
        if (null != progressUpdater) {
            progressUpdater.stopProgressUpdater();
            progressUpdater = null;
        }
        
        // waiting for expiry
        waitingForExpiry.setVisibility(GONE);
    }
    
    class ShowCreatedRunnable implements Runnable {
        public void run() {
            resetChartOppState();
            timeOpen.setVisibility(VISIBLE);
        }
    }
    
    class ShowOpenedRunnable implements Runnable {
        public void run() {
            resetChartOppState();
        }
    }
    
    class ShowLast10MinRunnable implements Runnable {
        public void run() {
            resetChartOppState();
            if (!hasInsurances) {
                findViewById(R.id.assetPageChartTimeLeft).setVisibility(VISIBLE);
                timeLeft.setVisibility(VISIBLE);
                timeLeftProgress.setVisibility(VISIBLE);
            }
            if (null == progressUpdater) {
                progressUpdater = new Last10MinProgressUpdater();
                progressUpdater.start();
            }
        }
    }
    
    class ShowWaitingForExpiryRunnable implements Runnable {
        public void run() {
            resetChartOppState();
            waitingForExpiry.setVisibility(VISIBLE);
        }
    }

    class Last10MinProgressRunnable implements Runnable {
        private int progress;
        
        public Last10MinProgressRunnable(int progress) {
            this.progress = progress;
        }
        
        public void run() {
            timeLeftProgress.setProgress(progress);
        }
    }
    
    // TODO: Refactor with CountDownTimer
    class Last10MinProgressUpdater extends Thread {
        private boolean running;
        
        public void run() {
            running = true;
            try {
                Date ct = null;
                do {
                    ct = new Date();
                    Utils.fixTime(ct);
                    int secLeft = (int) (timeLastInvest.getTime() - ct.getTime()) / 1000;
                    if (secLeft < 0) {
                        break;
                    }
                    String str = String.format("%1$02d:%2$02d", secLeft / 60, secLeft % 60);
                    handler.postDelayed(new SetTextRunnable(timeLeft, str), 0);
                    handler.postDelayed(new Last10MinProgressRunnable(600 - secLeft), 0);
                    
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ie) {
                        // do nothing
                    }
                } while (running && ct.getTime() < timeLastInvest.getTime());
            } catch (Exception e) {
                Log.e("chart", "Progress updater error.", e);
            }
        }
        
        public void stopProgressUpdater() {
            running = false;
        }
    }
    
    class ShowClosingLevelRunnable implements Runnable {
        private String str;
        
        public ShowClosingLevelRunnable(String str) {
            this.str = str;
        }
        
        public void run() {
            resetChartOppState();
            level.setVisibility(INVISIBLE);
            findViewById(R.id.assetPageChartClosingLevel).setVisibility(VISIBLE);
            ((TextView) findViewById(R.id.assetPageChartClosingLevelLevel)).setText(str);
        }
    }
    
    class RotatePhoneOnClickListener implements View.OnClickListener {
        public void onClick(View v) {
            if (chart.isPortrait()) {
                showRotatePhonePopup();
//            	((Activity)getContext()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }
    }
}