package com.anyoption.android.app;

import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.SetTextRunnable;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Investment;

public class InsuranceView extends LinearLayout {
    protected Handler handler;
    protected TextView timeLeft;
    protected TimeLeftUpdater timeLeftUpdater;
    
    public InsuranceView(Context context) {
        this(context, null);
    }
    
    public InsuranceView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        inflater.inflate(R.layout.insurance, this, true);
        
        timeLeft = (TextView) findViewById(R.id.textViewInsuranceTime);
        handler = new Handler();
    }

    public void initAndStart(final int insurancesType, final long insuranceCloseTime) {
        ImageView iv = (ImageView) findViewById(R.id.imageViewInsuranceIcon);
        TextView tv = (TextView) findViewById(R.id.textViewInsuranceType);
        if (insurancesType == Investment.INSURANCE_TAKE_PROFIT) {
            iv.setImageResource(R.drawable.icon_takeprofit_gm);
            tv.setText(getResources().getString(R.string.new_features_take_profit));
        } else {
            iv.setImageResource(R.drawable.icon_rollforward_gm);
            tv.setText(getResources().getString(R.string.new_features_roll_forward));
        }
        
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("anyoption", "InsurancesView.onClick");
                Intent intent = new Intent(getContext(), RollForwardActivity.class);
                intent.putExtra(Constants.PUT_EXTRA_INSURANCES_TYPE, insurancesType);
                intent.putExtra(Constants.PUT_EXTRA_INSURANCES_CLOSE_TIME, insuranceCloseTime);
                ((Activity) getContext()).startActivityForResult(intent, Constants.REQUEST_CODE_INSURANCES);
            }
        });

        timeLeftUpdater = new TimeLeftUpdater(insuranceCloseTime);
        timeLeftUpdater.start();
    }
    
    public void stopTimeLeftUpdater() {
        if (null != timeLeftUpdater) {
            timeLeftUpdater.stopTimeLeftUpdater();
            timeLeftUpdater = null;
        }
    }
    
    class TimeLeftUpdater extends Thread {
        private boolean running;
        private long insuranceCloseTime;
        
        public TimeLeftUpdater(long insuranceCloseTime) {
            this.insuranceCloseTime = insuranceCloseTime;
        }
        
        public void run() {
            running = true;
            try {
                Date ct = null;
                do {
                    ct = new Date();
                    Utils.fixTime(ct);
                    int secLeft = (int) (insuranceCloseTime - ct.getTime()) / 1000;
                    if (secLeft < 0) {
                        break;
                    }
                    String str = String.format("%1$02d:%2$02d", secLeft / 60, secLeft % 60);
                    handler.post(new SetTextRunnable(timeLeft, str));
                    
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ie) {
                        // do nothing
                    }
                } while (running && ct.getTime() < insuranceCloseTime);
            } catch (Exception e) {
                Log.e("chart", "Progress updater error.", e);
            }
        }
        
        public void stopTimeLeftUpdater() {
            running = false;
        }
    }
}