package com.anyoption.android.app;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.CUPInfoMethodRequest;
import com.anyoption.json.requests.FirstEverDepositCheckRequest;
import com.anyoption.json.requests.UserMethodRequest;
import com.anyoption.json.results.CUPInfoMethodResult;
import com.anyoption.json.results.FirstEverDepositCheckResult;
import com.anyoption.json.results.UserMethodResult;

public class CupDepositActivity extends ReceiptActivity{
	
	protected Context context;
	Button submitButton;
	EditText amountText;
	private PaymentMethod payment;
	private String msg;
	private Activity activity;
	public static final long CDPAY_PROVIDER_ID = 30;
	public static final long DELTAPAY_CHINA_PROVIDER_ID = 22; //delta pay china
	private boolean isFirstEverDeposit;
	private WebViewClient myWebViewClient = new WebViewClient(){
	   

		@Override
	    public void onLoadResource(WebView view, String url) {
			Uri uri = Uri.parse(url);
			if(uri.getHost().indexOf("anyoption.com") > -1 ) {
				if(url.contains("deltaPayStatus")){
			        Utils.requestService(activity, responseH, "getUserBalanceCallback", new UserMethodRequest(), new UserMethodResult(), "getUser");
				}else {
					// it's not deltaPay anyoption url, do nothing
				}
			}
		}
		
		@Override
	    public boolean shouldOverrideUrlLoading(WebView view, String url) {
	        if(url.contains("vixipay.com")){ // Could be cleverer and use a regex
	            return super.shouldOverrideUrlLoading(view, url); // Leave webview and use browser
	        } else {
	            return true;
	        }
	    }
		
		public void onPageStarted(WebView view, String url, android.graphics.Bitmap favicon) {
			// stops the redirect to the anyoption website after the deposit
			if(url.contains("anyoption.com") && !url.contains("deltaPayStatus")){
				view.stopLoading();
			}
		};
	};
	public void getUserBalanceCallback(Object resultObj) {
        UserMethodResult result = (UserMethodResult) resultObj;
        AnyoptionApplication app = (AnyoptionApplication) getApplication();
        
        long oldBalance = app.getUser().getBalance();
        long newBalance = result.getUser().getBalance();
		
        app.setUser(result.getUser());
        app.setUserRegulation(result.getUserRegulation());
        
        HeaderLayout headerLayout = (HeaderLayout) findViewById(R.id.headerLayout);
		headerLayout.refresh();
		// TODO Find a better way to check if the transaction was successful
		if(newBalance > oldBalance){
			// toShowSendReceipt set to false, because in CUP deposit
			// the receipt is sent always from the external provider
			showSuccessPopup(msg, false);
		}else {
			String error = getResources().getString(R.string.cup_deposit_fail);
			Utils.showAlert(this, "", error , this.getString(R.string.alert_title));  
		}
		hideWebView();
    }
	
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
		 this.activity = this;
		 context = this;
		 setContentView(buildLayout(Constants.PAYMENT_TYPE_DEPOSIT));
		 submitButton = (Button)findViewById(R.id.SubmitDeposit);
		 amountText = (EditText)findViewById(R.id.amountToDeposit);
		 payment = (PaymentMethod) getIntent().getExtras().get(Constants.PUT_EXTRA_BANKING_PAYMENT);
		 findViewById(R.id.imageViewInfo).setTag(payment);
		 AnyoptionApplication ap = (AnyoptionApplication) getApplication();
		 User user = ap.getUser();
	     ((TextView)findViewById(R.id.TextViewCurrencySymbol)).setText(user.getCurrencySymbol());
	     ((TextView)findViewById(R.id.TextViewHeader)).setText(getString(R.string.profitLine_deposit).toUpperCase());
	     findViewById(R.id.progressBarWebView).setVisibility(View.GONE);
	     msg = "";

		if (!Utils.isEtraderProject(context)) {
			getFirstEverDepositCheck();
		}
	 }
	 
	 public void sendToDeltapay(View view){
		if (!Utils.isEtraderProject(this) && isFirstEverDeposit) {
			DepositExtraFieldsView depositExtraFieldsView = (DepositExtraFieldsView) findViewById(R.id.DepositExtraFieldsView);
			depositExtraFieldsView.validateAndUpdate("sendToDeltapay");
		} else {
			sendToDeltapay();
		}
	 }
	 
	 public void sendToDeltapay(){
		CUPInfoMethodRequest request = new CUPInfoMethodRequest();
		User user = ((AnyoptionApplication) getApplicationContext()).getUser();
		String error = null;
		error = Utils.validateDoubleAndEmpty(this, amountText.getText().toString(), getString(R.string.deposit_deposit));
    	if (null != error ){
    		Utils.showAlert(this, "", error, context.getString(R.string.alert_title));
    		return;
    	}
		request.setAmount(amountText.getText().toString());
		request.setUserId(user.getId());
		Utils.requestService(this, responseH, "getCUPInfoCallBack", request, new CUPInfoMethodResult(), "getCUPInfo");	
	}
	 
	 public void getCUPInfoCallBack(Object resultObj) throws UnsupportedEncodingException{		 
		Log.d(getPackageName(), "getDeltapayInfoCallBack");
		CUPInfoMethodResult result = (CUPInfoMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			loadCupWebView(result);
		} else {
			Log.d(getPackageName(), "Insert deposit failed");
			Utils.handleResultError(activity, result);
		}
	 }

	protected void loadCupWebView(CUPInfoMethodResult result) {
		WebView webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setBuiltInZoomControls(true);
		amountText.setVisibility(EditText.GONE);
		submitButton.setVisibility(Button.GONE);
		((TextView)findViewById(R.id.TextViewCurrencySymbol)).setVisibility(View.GONE);
		String[] paramsArr = result.getParams().split("&");
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		for(String str : paramsArr){
			String[] tempArr = str.split("=");
			String name = tempArr[0];
			String value = tempArr[1];
			parameters.add(new BasicNameValuePair(name, value));
		}
		UrlEncodedFormEntity entity = null;
		try {
			entity = new UrlEncodedFormEntity(parameters);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setSupportZoom(true);
		webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		webView.setScrollbarFadingEnabled(true);          
		webView.setVisibility(View.VISIBLE);
		webView.setWebChromeClient(new WebChromeClient() {
		    public void onProgressChanged(WebView view, int progress)   
		    {
		    findViewById(R.id.progressBarWebView).setVisibility(View.VISIBLE);
		     //Make the bar disappear after URL is loaded, and changes string to Loading...
		    activity.setTitle("Loading...");
		    activity.setProgress(progress * 100); //Make the bar disappear after URL is loaded

		     // Return the app name after finish loading
		        if(progress == 100)
		        	findViewById(R.id.progressBarWebView).setVisibility(View.GONE);
		      }
		});
		webView.setWebViewClient(myWebViewClient);
		try {
			webView.postUrl(result.getPostURL(), EntityUtils.toByteArray(entity));
			msg = getString(R.string.cup_deposit_success) + " " + ((TextView)findViewById(R.id.TextViewCurrencySymbol)).getText() + getString(R.string.cup_receipt_add, ((EditText)findViewById(R.id.amountToDeposit)).getText().toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
	}

	 @Override
	    public void onResume() {
	    	Log.v("event", "onResume");
	    	hideWebView();
			super.onResume();
	 }
	private void hideWebView() {
		WebView webView = (WebView) findViewById(R.id.webview);
		webView.setVisibility(View.INVISIBLE);
		amountText.setVisibility(EditText.VISIBLE);
		submitButton.setVisibility(Button.VISIBLE);
	}
	
	private void getFirstEverDepositCheck() {
		FirstEverDepositCheckRequest request = new FirstEverDepositCheckRequest();
		AnyoptionApplication application = (AnyoptionApplication) getApplication();
		User user = application.getUser();
		request.setUserId(user.getId());

		Utils.requestService(this, responseH,
				"getFirstEverDepositCheckCallback", request,
				new FirstEverDepositCheckResult(), "getFirstEverDepositCheck");
	}

	public void getFirstEverDepositCheckCallback(Object resultObject) {
		FirstEverDepositCheckResult result = (FirstEverDepositCheckResult) resultObject;

		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			isFirstEverDeposit = result.isFirstEverDeposit();
		} else {
			isFirstEverDeposit = false;
		}

		DepositExtraFieldsView extraFieldsView = (DepositExtraFieldsView) findViewById(R.id.DepositExtraFieldsView);
		extraFieldsView.setVisibility(View.VISIBLE);

		if (isFirstEverDeposit) {
			extraFieldsView.setCurrencyEnabled(true);
		} else {
			// Will be displayed only currency in disabled state.
			extraFieldsView.setCurrencyEnabled(false);
			extraFieldsView.setStreetVisibility(View.GONE);
			extraFieldsView.setCityVisibility(View.GONE);
			extraFieldsView.setZipCodeVisibility(View.GONE);
		}
	}
	
}
