package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;

import com.anyoption.android.app.questionnaire.MandatoryQuestionnaireActivity;
import com.anyoption.android.app.questionnaire.QuestionnaireUtils;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.UpdateUserMethodRequest;
import com.anyoption.json.results.MethodResult;

public class TermsActivity extends BaseActivity{
	private Button btnSubmit;
	private CheckBox checkBox;
	protected Context context;	
	User user;
	private String activityBeforeTerms;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.terms);
        context = this;	
        activityBeforeTerms = (String) getIntent().getExtras().get(Constants.ACTIVITY_BEFORE_TERMS);
        
        btnSubmit = (Button)findViewById(R.id.button_submit);
        btnSubmit.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				checkBox = (CheckBox)findViewById(R.id.checkBoxTerms);
				if(null != checkBox) {
					if (checkBox.isChecked()) {
						AnyoptionApplication ap = (AnyoptionApplication) getApplication();
						user = ap.getUser();
						user.setAcceptedTerms(true);
						UpdateUserMethodRequest request = new UpdateUserMethodRequest();
						MethodResult result = new MethodResult();
						
						request.setUser(user);				
						Utils.requestService(context, responseH, "updateUserCallBack", request, result, "updateUser");
						
					} else 
						Utils.showAlert(context, Constants.EMPTY_STRING, getString(R.string.error_register_terms), context.getString(R.string.alert_title));  
						return ;
				}
			}
		});
	}
	
    public void navigateTermsPage(View target){    	    	 
    	Intent intent = new Intent(this, WebViewActivity.class);
    	intent.putExtra(Constants.PUT_EXTRA_KEY_WEBVIEW, Constants.PUT_EXTRA_GENERAL_AGREEMENT);
    	startActivityForResult(intent, Constants.REQUEST_CODE_WEB_VIEW);    	
    }
    
    public void updateUserCallBack(Object resultObj){
		Log.d(getPackageName(), "updateUserCallBack");
		MethodResult result = (MethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			AnyoptionApplication ap = (AnyoptionApplication) context.getApplicationContext();
			ap.setUser(user);
			
			if(Utils.isRegulated(this) && Utils.isAfterFirstDeposit(this) && !QuestionnaireUtils.isMandQuestionnaireAnswered(this)){
				Intent intent = new Intent(context, MandatoryQuestionnaireActivity.class);
				intent.putExtra(Constants.ACTIVITY_BEFORE_QUESTIONNAIRE, Constants.FROM_LOGIN);
				((Activity) context).startActivity(intent);
				finish();
			} else {
				Intent data = new Intent();
				data.putExtra(Constants.ACTIVITY_BEFORE_TERMS, activityBeforeTerms);
				data.putExtra(Constants.RESULT_USER_OBJECT, user);
				setResult(Activity.RESULT_OK, data);
				finish();
			}
		} else {
			 Log.d(getPackageName(), "Error Update user");
			 String error = result.getUserMessages()[0].getMessage();
			 String field = result.getUserMessages()[0].getField();
			 Utils.showAlert(context, field, error, context.getString(R.string.alert_title));			
		}
	}
}
