package com.anyoption.android.app;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.User;

public class HeaderLayout extends LinearLayout {
	private AnyoptionApplication ap;
	private Timer timer;
	
	public HeaderLayout(Context context) {
		this(context, null);
	}
	
	public HeaderLayout(final Context context, AttributeSet attrs) {
		super(context, attrs);
        ap = (AnyoptionApplication) context.getApplicationContext();
        if (null != ap.getUser()) {
    		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            inflater.inflate(R.layout.header, this, true);
    		refresh();
        }
        if (!(context instanceof MyAccountActivity)){
		    this.setOnClickListener(new OnClickListener() {
				
				public void onClick(View arg0) {
					((Activity) context).startActivityForResult(new Intent(context, MyAccountActivity.class), 
							Constants.REQUEST_CODE_MY_ACCOUNT);
				}
			});
        }
	}
	
	public void refresh() {
	    try {
    		User user = ap.getUser();
            TextView headerUserTextview = (TextView) findViewById(R.id.textViewHeaderUserName);
            TextView headerBalanceTextview = (TextView) findViewById(R.id.textViewHeaderBalance);
            headerUserTextview.setText(user.getFirstName());
            headerBalanceTextview.setText(ap.getString(R.string.header_balance, user.getBalanceWF()));
            ImageView star = (ImageView) findViewById(R.id.imageViewBonusStar);
            if (user.isHasBonus()) {
            	star.setVisibility(View.VISIBLE);
            } else {
            	star.setVisibility(View.GONE);
            }
	    } catch (Exception e) {
	        Log.e("header", "Can't refresh header.", e);
	    }
	}
	
	public void onResume() {
	    stopRefreshTimer();
	    Log.v("anyoption", "Start header refresh timer.");
	    timer = new Timer();
	    timer.schedule(new RefreshUserTimerTask(), Utils.getNextRefreshTime());
	}
	
	public void onPause() {
        stopRefreshTimer();
	}
	
	private void stopRefreshTimer() {
        if (null != timer) {
            Log.v("anyoption", "Stop header refresh timer.");
            timer.cancel();
            timer = null;
        }
	}
	
	class RefreshUserTimerTask extends TimerTask {
	    public void run() {
	        ((BaseActivity) getContext()).refreshUser();
	    }
	}
}