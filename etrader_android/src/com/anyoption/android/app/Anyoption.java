package com.anyoption.android.app;

import android.content.Intent;
import android.os.Bundle;

/**
 * Anyoption - Base Activity
 * Start the Notification Service and run the AnyoptionWebView activity
 * 
 * @author Kobi
 *
 */
public class Anyoption extends BaseActivity {
	private static final int ACTIVITY_ANYOPTION = 1;
	
	/**
	 * Called when the activity is first created.
	 * Start the notification service & anyoption activity
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	        
	    // Start the notification service
    	startService(new Intent(this, NotificationService.class));
    		
    	// Start Anyoption activity
    	startActivityForResult(new Intent(this, AnyoptionWebView.class), ACTIVITY_ANYOPTION);    		
	}
	
	/**
	 * Called when an activity you launched exits
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ACTIVITY_ANYOPTION) {			
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	protected void onDestroy() {
		stopService(new Intent(this, NotificationService.class));		
		super.onDestroy();
	}    	  

}