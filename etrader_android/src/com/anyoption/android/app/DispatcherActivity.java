/**
 * 
 */
package com.anyoption.android.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


/**
 * @author Eyal
 *
 */
public class DispatcherActivity extends Activity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        if (null != ap.getUser()) {
        	startActivity(new Intent(this, MarketsGroupActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | 
        		Intent.FLAG_ACTIVITY_CLEAR_TOP));
        } else {
        	startActivity(new Intent(this, Home_pageActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | 
        		Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }
        finish();
    }
}
