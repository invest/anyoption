package com.anyoption.android.app;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.anyoption.android.app.util.SetBackgroundColorRunnable;
import com.anyoption.android.app.util.SetTextRunnable;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.Opportunity;
import com.lightstreamer.ls_client.UpdateInfo;

public class HomePageChartView extends BaseChartView {
    private View header;
    private View chartWrapper;
    private View btnWrapper;
    private TextView closed;
    
    public HomePageChartView(Context context) {
        this(context, null);
    }

    public HomePageChartView(Context context, AttributeSet attrs) {
        super(context, attrs, R.layout.home_page_chart, R.drawable.call_tp, R.drawable.call_on_tp, R.drawable.put_tp, R.drawable.put_on_tp, R.drawable.call_tp_transparent, R.drawable.put_tp_transparent);

        header = findViewById(R.id.homePageChartHeader);
        chartWrapper = findViewById(R.id.chartWrapper);
        btnWrapper = findViewById(R.id.buttonWrapper);
        closed = (TextView) findViewById(R.id.homePageChartClosed);
    }

    public void updateItem(int itemPos, String itemName, UpdateInfo update) {
        long oppId = Integer.parseInt(update.getNewValue("ET_OPP_ID"));
        if (opportunityId != 0 && oppId != opportunityId) {
            Log.v("chart", "opportunityId: " + opportunityId + " oppId: " + oppId);
            return;
        }

        super.updateItem(itemPos, itemName, update);

        if (update.isValueChanged("ET_ODDS")) {
            Log.v("lightstreamer", "odds: " + update.getNewValue("ET_ODDS"));
            handler.postDelayed(new SetTextRunnable(returnPercent, update.getNewValue("ET_ODDS")), 0);
        }
        if (update.isValueChanged("ET_EST_CLOSE")) {
            Log.v("lightstreamer", "time: " + update.getNewValue("ET_EST_CLOSE"));
            try {
                handler.postDelayed(new SetTextRunnable(expTime, Utils.reformatTimeShort(update.getNewValue("ET_EST_CLOSE"))), 0);
            } catch (Exception e) {
                Log.e("chart", "Can't process EET_EST_CLOSE.", e);
            }
        }
        if (update.isValueChanged("ET_STATE")) {
        	if (Utils.isEtraderProject(getContext())) {
        		handler.post(new SetBackgroundColorRunnable(header, 0x00566764 | (chart.getChartAlpha() << 24)));
        	} else {
        		handler.post(new SetBackgroundColorRunnable(header, 0x00F2F2F2 | (chart.getChartAlpha() << 24)));
        	}            
            if (chart.getOppState() >= Opportunity.STATE_OPENED && chart.getOppState() < Opportunity.STATE_DONE) {
                handler.post(new ShowOpenedRunnable());
            } else {
                String msg = null;
                if (chart.getOppState() == Opportunity.STATE_SUSPENDED) {
                    msg = composeSuspendedMessage(update.getNewValue("ET_SUSPENDED_MESSAGE"));
                } else {
                    msg = getResources().getString(R.string.profitLine_offhours);
                }
                handler.post(new SetTextRunnable(closed, msg));
                handler.post(new ShowCreatedRunnable());
            }
        }
    }
    
    protected void setAlpha() {
        super.setAlpha();
        
        int alpha = chart.getChartAlpha();
        Log.v("chart", "HomePageChartView.setAlpha: " + alpha);
        int alphaMask = alpha << 24;
        name.setTextColor(name.getCurrentTextColor() & CLEAR_ALPHA_MASK | alphaMask);
        expTime.setTextColor(expTime.getCurrentTextColor() & CLEAR_ALPHA_MASK | alphaMask);
        returnPercent.setTextColor(returnPercent.getCurrentTextColor() & CLEAR_ALPHA_MASK | alphaMask);
    }
    
    public boolean isHitCall(float x, float y) {
//        Log.v("event", "wl: " + btnWrapper.getLeft() + " bl: " + btnCall.getLeft() + " bw: " + btnCall.getWidth() + " ct: " + chartWrapper.getTop() + " wt: " + btnWrapper.getTop() + " bt: " + btnCall.getTop() + " bh: " + btnCall.getHeight());
        return btnWrapper.getLeft() + btnCall.getLeft() <= x
                && x <= btnWrapper.getLeft() + btnCall.getLeft() + btnCall.getWidth()
                && chartWrapper.getTop() + btnWrapper.getTop() + btnCall.getTop() <= y
                && y <= chartWrapper.getTop() + btnWrapper.getTop() + btnCall.getTop() + btnCall.getHeight();
    }
    
    public boolean isHitPut(float x, float y) {
        return btnWrapper.getLeft() + btnPut.getLeft() <= x
                && x <= btnWrapper.getLeft() + btnPut.getLeft() + btnPut.getWidth()
                && chartWrapper.getTop() + btnWrapper.getTop() + btnPut.getTop() <= y
                && y <= chartWrapper.getTop() + btnWrapper.getTop() + btnPut.getTop() + btnPut.getHeight();
    }
    
    public long getMarketId() {
        return chart.getMarketId();
    }

    class ShowCreatedRunnable implements Runnable {
        public void run() {
            // from last 10 min state when suspend
            closed.setVisibility(VISIBLE);
            View v = findViewById(R.id.homePageChartWaitingForExpiry);
            if (null != v) {
                v.setVisibility(GONE);
            }
        }
    }
    
    class ShowOpenedRunnable implements Runnable {
        public void run() {
            closed.setVisibility(GONE);
            View v = findViewById(R.id.homePageChartWaitingForExpiry);
            if (null != v) {
                if (chart.getOppState() == Opportunity.STATE_CLOSING_1_MIN || chart.getOppState() == Opportunity.STATE_CLOSING) {
                    v.setVisibility(VISIBLE);
                } else {
                    v.setVisibility(GONE);
                }
            }
        }
    }
}