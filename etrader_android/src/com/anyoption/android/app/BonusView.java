package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.BonusUsers;
import com.anyoption.json.requests.BonusMethodRequest;
import com.anyoption.json.results.BonusMethodResult;

/**
 * @author AviadH
 *
 */
public class BonusView extends MenuViewBase {
	
	private BonusUsers[] bonuses;
	private int cntMarkedBonuses;
	protected boolean isFirst;
	
	public BonusView(Context context) {
		this(context, null);
	}
	
	public BonusView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initComponent(attrs);
	}
	
	public void initComponent(AttributeSet attrs) {
		Log.d("BonusView", "init Component");
		isFirst = true;
		//set table params
		if (!Utils.isEtraderProject(context)) {
			tlMain.setColumnStretchable(0, true);
			tlMain.setColumnShrinkable(1, true);
		}

		loadXRows();
	}
	
	public String getMenuStripText() {
		return getResources().getString(R.string.header_bonus, "");
	}
	
	public void bonusCallBack(Object result) {
		showLoadMore = false;
		BonusMethodResult bonusResult = (BonusMethodResult) result;
		if (bonusResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			if (bonusResult.getBonuses().length == 0 && isFirst) {
				makeNoInformationRow(getNoInformationString());
			} else {
				setBonuses(bonusResult.getBonuses());
				isFirst = false;
				startRow += PAGE_SIZE;
			}
		} else {
			Utils.showAlert(context, bonusResult.getUserMessages()[0].getField(), 
					bonusResult.getUserMessages()[0].getMessage(), context.getString(R.string.alert_title));
		}
	}
	
	public void loadXRows() {
		BonusMethodRequest request = new BonusMethodRequest();
		request.setBonusStateId(0);
		request.setFrom(fromCal);
		request.setTo(toCal);
		request.setStartRow(startRow);
		request.setPageSize(PAGE_SIZE);
		Utils.requestService(this, responseH, "bonusCallBack", request, new BonusMethodResult(), "getUserBonuses", (Activity) context);
	}
	
	public void setBonuses(BonusUsers[] bonusUsers) {
		//remove the last rows in load more option
		if (startRow > 0) {// if it is not the first time
			for (int i = 1 ; i <= rowsToRemoveInLoadMore ; i++) {
				tlMain.removeViewAt(tlMain.getChildCount()-1);
			}
		}
		if (bonusUsers.length >= PAGE_SIZE) {
			showLoadMore = true;
		}
		cntMarkedBonuses = 0;
		if (null != fromCal && null != toCal && startRow == 0) {// in case we are after more bonuses popup 
			makeFirstDatesRow(fromCal, toCal);
		}
		bonuses = bonusUsers;
		for (int i = 0 ; i < bonuses.length ; i++) {
    		makeRowForBonus(bonuses[i]);
	    }
    	makeLastRow(getResources().getString(R.string.bonus_last_row), getResources().getString(R.string.header_bonus, ""));
		TextView groupName = (TextView) menuLine.findViewById(R.id.textViewGroupName);
		if (cntMarkedBonuses > 0) {
			groupName.setText(getResources().getString(R.string.header_bonus, "(" + Integer.toString(cntMarkedBonuses) + ")"));
			menuLine.findViewById(R.id.imageViewStar).setVisibility(View.VISIBLE);
		} else {
			groupName.setText(getResources().getString(R.string.header_bonus,""));
			menuLine.findViewById(R.id.imageViewStar).setVisibility(View.GONE);
		}
	}
	
	public void makeRowForBonus(final BonusUsers bonus) {
		TableRow tr = (TableRow) inflater.inflate(R.layout.bonus_row, tlMain, false);
		TextView valid = (TextView) tr.findViewById(R.id.textViewBonusValid);
		TextView name = (TextView) tr.findViewById(R.id.textViewBonusName);
		valid.setText(getResources().getString(R.string.bonus_end_date) + " " + bonus.getEndDateTxt());
		name.setText(bonus.getBonusDescriptionTxt());
		
		if (bonus.getBonusStateId() == Constants.BONUS_STATE_GRANTED ||
				bonus.getBonusStateId() == Constants.BONUS_STATE_ACTIVE) {
			tr.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_strong_white));
			cntMarkedBonuses++;
		}
		tr.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(context, BonusActivity.class); 
				intent.putExtra("BonusUsers", bonus);
				((Activity) context).startActivityForResult(intent, Constants.REQUEST_CODE_BONUS_DETAILS);
			}
		});
	    
		tlMain.addView(tr);
	}
	
    @Override
    public void closeGroup() {
    	super.closeGroup();
    	removeAllRows();
    	restartLoadMore();
    	loadXRows();
    }
    
    @Override
    public String getNoInformationString() {
 	   return getResources().getString(R.string.bonus_emptylist);
    }

	@Override
	public void removeAllRows() {
		tlMain.removeAllViews();
	}
}