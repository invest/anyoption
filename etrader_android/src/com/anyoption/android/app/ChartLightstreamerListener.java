package com.anyoption.android.app;

import com.lightstreamer.ls_client.UpdateInfo;

public interface ChartLightstreamerListener {
    public void updateItem(int itemPos, String itemName, UpdateInfo update);
}