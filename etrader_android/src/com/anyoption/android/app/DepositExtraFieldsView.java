package com.anyoption.android.app;

import java.lang.reflect.Method;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.requests.UpdateUserMethodRequest;
import com.anyoption.json.results.OptionsMethodResult;
import com.anyoption.json.results.UserMethodResult;
import com.anyoption.json.util.OptionItem;

/**
 * @author Eyal G
 */
public class DepositExtraFieldsView extends LinearLayout{
	
	private LayoutInflater inflater;
	private Handler responseH;
	private final Context context;
	private String methodName;
	
	public DepositExtraFieldsView(Context context) {
		this(context, null);
	}
	
	public DepositExtraFieldsView(final Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
		LinearLayout loginLayout = (LinearLayout) inflater.inflate(R.layout.deposit_extra_fields, this, false);
		this.addView(loginLayout);
        responseH = new Handler();
        OptionsMethodResult result = new OptionsMethodResult();
        MethodRequest request = new MethodRequest();
        Utils.requestService(this, responseH, "currencyCallBack", request, result, "getSkinCurrenciesOptions", (Activity)context);
        AnyoptionApplication ap = (AnyoptionApplication) ((Activity)context).getApplication();
		User user = ap.getUser();
		if (null != user.getStreet() && user.getStreet().length() > 0) {
			((EditText)findViewById(R.id.editTextStreet)).setText(user.getStreet());
		}
		if (null != user.getCityName() && user.getCityName().length() > 0) {
			((EditText) findViewById(R.id.editTextCity)).setText(user.getCityName());
		}
		if (null != user.getZipCode() && user.getZipCode().length() > 0) {
			((EditText)findViewById(R.id.editTextZipCode)).setText(user.getZipCode());
		}
	}
	
	public void currencyCallBack(Object result) {
		Log.d("DepositExtraFieldsView", "currencyCallBack");
		Spinner spinner = (Spinner) findViewById(R.id.spinnerCurrency);
		fillSpinner(spinner, result);
	}
	
	public void fillSpinner(Spinner spinner, Object result) {
		OptionsMethodResult omr = (OptionsMethodResult) result;
		ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(context,
				android.R.layout.simple_spinner_item, omr.getOptions());
		spinnerArrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		

		spinner.setAdapter(spinnerArrayAdapter);
		AnyoptionApplication ap = (AnyoptionApplication) ((Activity)context).getApplication();
		User user = ap.getUser();  
        for (OptionItem oi : omr.getOptions()) {
			if (oi.getId() == user.getCurrencyId()) {
				spinner.setSelection(spinnerArrayAdapter.getPosition(oi));
				break;
			}
		}
	}
	
	public void validateAndUpdate(String methodName) {
		//Street
		EditText street = (EditText)findViewById(R.id.editTextStreet);
    	String error = Utils.validateStreetAndEmpty(context, street.getText().toString(), context.getString(R.string.register_street));
    	if (error != null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return;
    	}
    	
    	//city
    	String city = ((EditText) findViewById(R.id.editTextCity)).getText().toString();
    	error = Utils.validateLettersOnlyAndEmpty(context, city, context.getString(R.string.register_city));
    	if (error != null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return;
    	}
    	
    	User userUpdate = new User();
    	Spinner spinnerCurrency = (Spinner) findViewById(R.id.spinnerCurrency);
    	userUpdate.setCurrencyId(((OptionItem)spinnerCurrency.getAdapter().getItem(spinnerCurrency.getSelectedItemPosition())).getId());
    	
		EditText zipCode = (EditText)findViewById(R.id.editTextZipCode);
		userUpdate.setStreet(street.getText().toString());
		userUpdate.setCityName(city);
		userUpdate.setZipCode(zipCode.getText().toString());
		AnyoptionApplication ap = (AnyoptionApplication) ((Activity)context).getApplication();
		User user = ap.getUser();
		userUpdate.setId(user.getId());
		this.methodName = methodName;
		UpdateUserMethodRequest request = new UpdateUserMethodRequest();
		request.setUser(userUpdate);
		Utils.requestService(this, responseH, "validateAndUpdateCallBack", request, new UserMethodResult(), "updateUserAdditionalFields", (Activity)context);
	}
	
	public void validateAndUpdateCallBack(Object resultObj) {
		Log.d("DepositExtraFieldsView", "validateAndUpdateCallBack");
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			AnyoptionApplication ap = (AnyoptionApplication) context.getApplicationContext();
			User user = ap.getUser();
			user.setStreet(((EditText) findViewById(R.id.editTextStreet)).getText().toString());
			user.setCityName(((EditText) findViewById(R.id.editTextCity)).getText().toString());
			user.setZipCode(((EditText) findViewById(R.id.editTextZipCode)).getText().toString());
			try {
				Log.d("DepositExtraFieldsView", "method name " + methodName);
				Method method = context.getClass().getMethod(methodName, null);
				method.invoke(context);
			} catch (Exception e) {
				Log.d("DepositExtraFieldsView", "can't run method " + methodName, e);
			}
		} else {
			 Log.d("DepositExtraFieldsView", "Error Update user");		
		}
	}
	
	public void setCurrencyChangeListner(final TextView currencyTextView) {
		Spinner spinner = (Spinner) findViewById(R.id.spinnerCurrency);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				long currencyId = ((OptionItem)parent.getAdapter().getItem(parent.getSelectedItemPosition())).getId();
				currencyTextView.setText(Utils.getStringFromResource(context, "currency." + currencyId, null));		
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});
	}
	
	/**
	 * Sets the enabled state of the currency field.
	 * 
	 * @param enabled
	 */
	public void setCurrencyEnabled(boolean enabled) {
		View currencyView = findViewById(R.id.spinnerCurrency);
		currencyView.setEnabled(enabled);
	}

	/**
	 * Changes the visibility of currency field.
	 * 
	 * @param visibility
	 *            should be one of {@link View#VISIBLE}, {@link View#INVISIBLE},
	 *            or {@link View#GONE}.
	 */
	public void setCurrencyVisibility(int visibility) {
		View currencyView = findViewById(R.id.spinnerCurrency);
		currencyView.setVisibility(visibility);
	}

	/**
	 * Changes the visibility of street field.
	 * 
	 * @param visibility
	 *            should be one of {@link View#VISIBLE}, {@link View#INVISIBLE},
	 *            or {@link View#GONE}.
	 */
	public void setStreetVisibility(int visibility) {
		View streetView = findViewById(R.id.editTextStreet);
		streetView.setVisibility(visibility);
	}

	/**
	 * Changes the visibility of city field.
	 * 
	 * @param visibility
	 *            should be one of {@link View#VISIBLE}, {@link View#INVISIBLE},
	 *            or {@link View#GONE}.
	 */
	public void setCityVisibility(int visibility) {
		View cityView = findViewById(R.id.editTextCity);
		cityView.setVisibility(visibility);
	}

	/**
	 * Changes the visibility of zip code field.
	 * 
	 * @param visibility
	 *            should be one of {@link View#VISIBLE}, {@link View#INVISIBLE}
	 *            or {@link View#GONE}.
	 */
	public void setZipCodeVisibility(int visibility) {
		View zipCodeView = findViewById(R.id.editTextZipCode);
		zipCodeView.setVisibility(visibility);
	}

}