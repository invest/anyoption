package com.anyoption.android.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;

/**
 * @author IdanZ
 *
 */
public class LogoutView extends MenuViewBase {
	SharedPreferences settings = null;
	String langCode = null;
	
	public LogoutView(Context context) {
		this(context, null);
	}
	
	public LogoutView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initComponent(attrs);
	}
	
	public void initComponent(AttributeSet attrs) {
		Log.d("LogoutView", "init Component");
		//set table params
		tlMain.setColumnStretchable(0, true);
		tlMain.setColumnShrinkable(1, true);
		
		buttonMenuStrip.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Logout();
			}
		});
	}
	 
	public String getMenuStripText() {
		return getResources().getString(R.string.header_loggedin_logout);
	}
	
	public void Logout(){
		final Context c = context;
    	LinearLayout logoutAlert = (LinearLayout) inflater.inflate(R.layout.logout_pop_up, null, false);
    	final TextView title = (TextView) logoutAlert.findViewById(R.id.textViewTitle);
    	
    	title.setText(context.getString(R.string.logout_msg));
		Button cancelDialogButton = (Button) logoutAlert.findViewById(R.id.buttonMyAccountPersonalDetailsPopupClose);
		cancelDialogButton.setText(R.string.button_cancel);
		Button submitDialogButton = (Button) logoutAlert.findViewById(R.id.buttonCallUsPopupSubmit);
		submitDialogButton.setText(R.string.header_loggedin_logout);
		final AlertDialog ad = Utils.showCustomAlert(context, logoutAlert, cancelDialogButton);
		submitDialogButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					AnyoptionApplication ap = (AnyoptionApplication) c.getApplicationContext();
					ap.setUser(null);
					ad.dismiss();
					Intent intent = new Intent(c, Home_pageActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					((Activity) c).startActivityForResult(intent, Constants.REQUEST_CODE_LOGIN_PAGE);
				}
		}); 				    
	}

	@Override
	public void removeAllRows() {
		// TODO Auto-generated method stub
		
	}
}