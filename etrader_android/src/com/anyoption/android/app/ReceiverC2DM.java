package com.anyoption.android.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.c2dm.C2DMBaseReceiver;

public class ReceiverC2DM extends BroadcastReceiver
{
	final String TAG = "Reciver";

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "Received intent action  = " + intent.getAction());
        C2DMBaseReceiver.runIntentInService(context, intent);
        setResult(Activity.RESULT_OK, null /* data */, null /* extra */);   
	}
}
