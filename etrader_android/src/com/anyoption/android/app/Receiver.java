package com.anyoption.android.app;

import java.util.Map;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;

public class Receiver extends BroadcastReceiver
{
	final String TAG = "Reciver";

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "Received intent action  = " + intent.getAction());
		Log.i(TAG, "----------------------------------------");
		Log.i(TAG, "RECEIVED REFERRAL INFO");
		
		// call Analytics tracker 
        com.google.android.apps.analytics.AnalyticsReceiver ar = new com.google.android.apps.analytics.AnalyticsReceiver(); 
        ar.onReceive(context, intent);
		
		String uri = intent.toURI();
		
		Log.i(TAG, "referrer: [" + uri + "]");
		Log.i(TAG, intent.getAction());
		
		if (uri != null && uri.length() > 0) {
			int index = uri.indexOf("referrer=");
			
			if (index > -1) {
				// Gets the referrer decoded. 
				String referrer = intent.getStringExtra("referrer");

				Log.i(TAG, "Referrer: [" + referrer + "]");
				
				// referralURL contains both referrer parameter and its value
				// ex: referralURL="referrer=com.package.app"
				Map<String, String> map = Utils.parseUrlToStringPairs(referrer);
				SharedPreferences settings = context.getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
				SharedPreferences.Editor editor = settings.edit();
				Log.i(TAG, "combId = " + map.get("combid"));
				if (null != map.get("combid")) {
					editor.putString(Constants.PREF_COMB_ID, map.get("combid"));
					editor.commit();
				}
				Log.i(TAG, "dp = " + map.get("dp"));
				if (null != map.get("dp")) {
					editor.putString(Constants.PREF_DYNAMIC_PARAM, map.get("dp"));
					editor.commit();
				}
				
				Log.i(TAG, "mid = " + map.get("mid"));
				if (null != map.get("mid")) {
					editor.putString(Constants.PREF_MID, map.get("mid"));
					editor.commit();
				}
				
				Log.i(TAG, "etsmid = " + map.get("etsmid"));
				if (null != map.get("etsmid")) {
					editor.putString(Constants.PREF_ETS_MID, map.get("etsmid"));
					editor.commit();
				}
												
				editor.putString(Constants.PREF_REFERRER_PARAM, referrer);
				editor.commit();
				
				Log.i(TAG, "Saved combId = " + settings.getString(Constants.PREF_COMB_ID, null));
				Log.i(TAG, "Saved dp = " + settings.getString(Constants.PREF_DYNAMIC_PARAM, null));
				Log.i(TAG, "Saved mid = " + settings.getString(Constants.PREF_MID, null));
				Log.i(TAG, "Saved etsmid = " + settings.getString(Constants.PREF_ETS_MID, null));
				
				Log.i(TAG, "utils combId = " + Utils.getCombinationId(context));
				Log.i(TAG, "utils dp = " + Utils.getDynamicParam(context));
				Log.i(TAG, "utils mid = " + Utils.getmId(context));
				Log.i(TAG, "utils etsmid = " + Utils.getEtsMId(context));
			} else {
				Log.i(TAG, "No Referral URL.");
			}
		}
		
		Log.i(TAG, "----------------------------------------");
	}
}
