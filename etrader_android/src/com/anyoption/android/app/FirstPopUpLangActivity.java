package com.anyoption.android.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.json.requests.CheckUpdateMethodRequest;
import com.anyoption.json.results.UpdateMethodResult;

public class FirstPopUpLangActivity extends BaseActivity {
	
	private static final String TAG = FirstPopUpLangActivity.class.getSimpleName();
	
	private int lang = 0;	
	String langCode = null;
	SharedPreferences settings = null;
	String downloadUrl;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); 
        setContentView(R.layout.first_pop_up_lang);
        
        //check if need update
        CheckUpdateMethodRequest request = new CheckUpdateMethodRequest();
        request.setApkVersion(Utils.getAppVersionCode(this));
        String apkName = getString(R.string.apkName);
        if(Constants.IS_CHINESE_ONLY){
        	apkName = getString(R.string.apkNameChinese);
        }
        request.setApkName(apkName);
        request.setCombinationId(Utils.getCombinationId(this));
        request.setDynamicParam(Utils.getDynamicParam(this));
        request.setmId(Utils.getmId(this));
        request.setEtsMId(Utils.getEtsMId(this));
        request.setFirstOpening(isFirstOpening());

        Log.d(TAG, "----checkForUpdate-----");
		Log.d(TAG, "combId = " + request.getCombinationId());
		Log.d(TAG, "dp = " + request.getDynamicParam());
		Log.d(TAG, "mid = " + request.getmId());
		Log.d(TAG, "etsmid = " + request.getEtsMId());
		Log.d(TAG, "isFirstOpening = " + request.isFirstOpening());
		Log.d(TAG, "-----------------------");
		
        Utils.requestService(this, responseH, "checkVersionUpdateCallBack", request, new UpdateMethodResult(), "checkForUpdate");
    }

	private boolean isFirstOpening() {
		SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREF,
					MODE_PRIVATE);
		boolean isFirstOpening = sharedPreferences.getBoolean(
					Constants.PREF_FIRST_APP_OPEN, true);
		if(isFirstOpening){
			sharedPreferences.edit()
			.putBoolean(Constants.PREF_FIRST_APP_OPEN, false).commit();
		}
		return isFirstOpening;
	}
    
    public void checkVersionUpdateCallBack(Object result) {
	    Log.d(getLocalClassName(), "CheckVersionUpdateCallBack");
	    AnyoptionApplication app = (AnyoptionApplication) getApplication();
	    final UpdateMethodResult updateResult = (UpdateMethodResult) result;		   
	    if (null!= updateResult && updateResult.getCountryId() != 0){
	    	app.setCountryId(updateResult.getCountryId());
	    }	    
	    if (updateResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
	    	SharedPreferences settings = getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
			SharedPreferences.Editor editor = settings.edit();
			
			Log.d(TAG, "result combId = " + updateResult.getCombId());
		    if (updateResult.getCombId() > 0) {
				editor.putString(Constants.PREF_COMB_ID, String.valueOf(updateResult.getCombId()));
				editor.commit();
				Log.d(TAG, "updated combId = " + Utils.getCombinationId(this));
			}
		    
		    Log.d(TAG, "result dp = " + updateResult.getDynamicParam());
			if (null != updateResult.getDynamicParam()) {
				editor.putString(Constants.PREF_DYNAMIC_PARAM, updateResult.getDynamicParam());
				editor.commit();
				Log.d(TAG, "updated dp = " + Utils.getDynamicParam(this));
			}
			
			Utils.setmIdIfValid(this, updateResult.getmId());
			Log.d(TAG, "result mid = " + updateResult.getmId());
			Utils.setEtsMId(this, updateResult.getEtsMid());
			Log.d(TAG, "result ets_mid = " + updateResult.getEtsMid());
			
	    	Utils.setServerTimeOffset(updateResult.getServerTime());
	    	// Sets the server time during this update.
	    	Utils.setServerTimeOnUpdate(updateResult.getServerTime());
	    	
			 if (updateResult.isNeedAPKUpdate()) {
				 downloadUrl = "market://details?id=" + FirstPopUpLangActivity.this.getPackageName();
				 if (null != updateResult.getDownloadLink()) {
					 downloadUrl = updateResult.getDownloadLink();
				 }
				 Log.d(getLocalClassName(), downloadUrl + "   " + updateResult.getDownloadLink());
				 LayoutInflater inflater = getLayoutInflater();
				 LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.popup_new_update, null, false);
				 TextView msg = (TextView) ll.findViewById(R.id.textViewMsg);
				 Button updateNow = (Button) ll.findViewById(R.id.buttonUpdateNow);
				 TextView smallLink = (TextView) ll.findViewById(R.id.TextViewSmallLink);
				 AlertDialog.Builder builder;
				 final AlertDialog alertDialog;            
				 builder = new AlertDialog.Builder(this);
				 builder.setView(ll);
				 alertDialog = builder.create();
				 alertDialog.setCancelable(true);
				 alertDialog.setOnCancelListener(new OnCancelListener() { //if u click on back button
					public void onCancel(DialogInterface arg0) {
						finish();
					}
				 });
				 msg.setText(getResources().getString(R.string.updateMsg));
				 updateNow.setOnClickListener(new OnClickListener() {
					public void onClick(View arg0) {
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(downloadUrl));
	    		        startActivity(intent);
						alertDialog.dismiss();
						showScreen(updateResult.getSkinId());
					}
				 });
				 smallLink.setOnClickListener(new OnClickListener() {
					public void onClick(View arg0) {
						alertDialog.dismiss();
						showScreen(updateResult.getSkinId());
					}
				 });
				 
				 String smallLinkTxt = getString(R.string.email_auth_activation_refused);
				 if (updateResult.isForceAPKUpdate()) {
					 msg.setText(getResources().getString(R.string.mustUpdateMsg));
					 updateNow.setOnClickListener(new OnClickListener() {
						public void onClick(View arg0) {
							Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(downloadUrl));
		    		        startActivity(intent);
		    		        alertDialog.dismiss();
							finish();
						}
					 });
					 
					 smallLink.setOnClickListener(new OnClickListener() {
						public void onClick(View arg0) {
							alertDialog.dismiss();
							finish();
						}
					 });
					 smallLinkTxt = getString(R.string.insurances_not_now);
				 }
				 SpannableString content = new SpannableString(smallLinkTxt); 
				 content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
				 smallLink.setText(content);
				 alertDialog.show();
				 return;
			 }
		} else {
			Log.d(getLocalClassName(), "CheckVersion error code " + updateResult.getErrorCode());
			Utils.showAlert(this, updateResult.getUserMessages()[0].getField(), updateResult.getUserMessages()[0].getMessage(), 
					this.getString(R.string.alert_title));
			return;
		}
		showScreen(updateResult.getSkinId());
    }
    
    public void showScreen(long skinId) {
    	if(Constants.IS_CHINESE_ONLY){
    		startHomePage(Skin.SKIN_CHINESE, "zh");
    	}else if (Utils.isEtraderProject(this)) {
 	    	startHomePage(1, "iw");
 	    } else { 
	    	settings = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
	        langCode = settings.getString(Constants.PREF_LOCALE, null);
	        if (langCode != null){
	        	startActivityForResult(new Intent(this, Home_pageActivity.class), Constants.REQUEST_CODE_HOMEPAGE);
	        } else {
	            View langList = null;
	            if (skinId == Skin.SKIN_EN_US || skinId == Skin.SKIN_ES_US) {
	                langList = findViewById(R.id.linearLayoutUS);
	            } else {
	                langList = findViewById(R.id.linearLayout);
	            }
	            langList.setVisibility(TextView.VISIBLE);
		        //Default language will be English
		        TextView tv = (TextView) langList.findViewById(R.id.textEnglish);        
		        tv.setBackgroundColor(0x30ffffff);
		        lang = tv.getId();
		        langCode = (String) tv.getTag();
	        }
 	    }
        SharedPreferences sp = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        Log.d(getLocalClassName(), "c2dm id " + sp.getString(Constants.PREF_DEVICE_REGISTRATION_ID, null));
        if (null == sp.getString(Constants.PREF_DEVICE_REGISTRATION_ID, null)) {
	      //Registering for C2DM
	        Intent registrationIntent = new Intent("com.google.android.c2dm.intent.REGISTER");
	        registrationIntent.putExtra("app", PendingIntent.getBroadcast(this, 0, new Intent(), 0)); //boilerplate 
	        registrationIntent.putExtra("sender", C2DMReceiver.SENDER_ID);
	        startService(registrationIntent);
    	}
        
        // Start the notification service
        Log.e("NotificationService","NotificationService");
        startService(new Intent(this, NotificationService.class));
    }
    
    public void changeBg(View target) { 
    	if (lang == 0) {
    		target.setBackgroundColor(0x30ffffff);
    		lang = target.getId();    
    		langCode = (String) target.getTag();
    	} else if (lang != 0 && target.getId() != lang) {  
    	    TextView tv = (TextView) findViewById(lang);
    	    tv.setBackgroundColor(Color.TRANSPARENT);
    	    target.setBackgroundColor(0x30ffffff);
    	    lang = target.getId();
    	    langCode = (String) target.getTag();
    	} 
    }
    
    public void openHomePage(View target) {
    	//Log.d(getPackageName(), "open page");
    	if (langCode == null) { // if the user didn't choose a language
    		Utils.showAlert(this, "", getString(R.string.error_choose_language), this.getString(R.string.alert_title));
    		//app.changeLocale(langCode);
    	} else {
    		String[] a = langCode.split("_");
    		long skinId = Long.parseLong(a[1]);
    		if(Utils.isRegulated(this)){
    			long regulatedSkinId = Utils.getAlternativeRegulatedSkin(this, skinId);
    			if(regulatedSkinId != -1){
    				skinId = regulatedSkinId;
    			}
    		}
    		startHomePage(skinId, a[0]);
    	}
    }   
    
    public void startHomePage(long skinId, String locale) {
    	AnyoptionApplication app = (AnyoptionApplication) getApplication();
    	app.changeLocale(locale);
		app.setSkinId(skinId);
		Intent intent = new Intent(this, Home_pageActivity.class);
		intent.putExtra(Constants.PUT_EXTRA_UPDATE_SKIN, "ture");
		startActivityForResult(intent, Constants.REQUEST_CODE_HOMEPAGE);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	stopService(new Intent(this, NotificationService.class));		
    	finish();
    }   
}