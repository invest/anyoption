package com.anyoption.android.app;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.DeviceRegistrar;
import com.anyoption.android.app.util.LauncherUtils;
import com.anyoption.android.app.util.RequestServiceTask;
import com.anyoption.android.app.util.Utils;
import com.anyoption.json.requests.CheckUpdateMethodRequest;
import com.anyoption.json.results.MethodResult;
import com.google.android.c2dm.C2DMBaseReceiver;
import com.google.android.c2dm.C2DMessaging;

public class C2DMReceiver extends C2DMBaseReceiver {
    private static final String TAG = "C2DMReceiver";
    public static final String SENDER_ID = "sk@anyoption.com";
    public static final String SENDER_NAME = "anyoption";
    
    private String registration;

    public C2DMReceiver() {
        super(DeviceRegistrar.SENDER_ID);
    }

    @Override
    public void onRegistered(Context context, String registration) {
        this.registration = registration;
    	Log.w("C2DMReceiver-onRegistered", registration);
    	String currentRegistrationId = C2DMessaging.getRegistrationId(getApplicationContext());
    	if(registration != currentRegistrationId){
    	    try {
                CheckUpdateMethodRequest request = new CheckUpdateMethodRequest();
                request.setDeviceId(Utils.getUDID(context));
                request.setC2dmRegistrationId(registration);
        	    new RequestServiceTask(null, null, "c2dmUpdate", this, this.getClass().getMethod("onRegisteredCallback", MethodResult.class), MethodResult.class).execute(request);
    	    } catch (NoSuchMethodException e) {
    	        // do nothing
    	    }
    	}
    }
    
    public void onRegisteredCallback(MethodResult result) {
        Log.i(TAG, "Registration result code " + result.getErrorCode());
        if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            SharedPreferences settings = getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(Constants.PREF_DEVICE_REGISTRATION_ID, registration);
            editor.commit();
        }
    }

    @Override
    public void onUnregistered(Context context) {
    	Log.w("C2DMReceiver-onUnregistered", "got here!");
        try {
            CheckUpdateMethodRequest request = new CheckUpdateMethodRequest();
            request.setDeviceId(Utils.getUDID(context));
            request.setC2dmRegistrationId(null);
            new RequestServiceTask(null, null, "c2dmUpdate", this, this.getClass().getMethod("onUnregisteredCallback", MethodResult.class), MethodResult.class).execute(request);
        } catch (NoSuchMethodException e) {
            // do nothing
        }
    }
    
    public void onUnregisteredCallback(MethodResult result) {
        Log.i(TAG, "Unregistration result code " + result.getErrorCode());
        SharedPreferences settings = getSharedPreferences(Constants.SHARED_PREF, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(Constants.PREF_DEVICE_REGISTRATION_ID);
        editor.remove("accountName");
        editor.commit();
    }

    @Override
    public void onError(Context context, String errorId) {
    	Log.w("C2DMReceiver-onError", errorId);
    	//TODO: we got error need to do somthing?
        //context.sendBroadcast(new Intent("com.google.ctp.UPDATE_UI"));
    }

    @Override
    public void onMessage(Context context, Intent intent) {
        Log.w("C2DMReceiver", intent.getStringExtra("payload"));
        Bundle extras = intent.getExtras();
        if (extras != null) {
//        	String debug = (String) extras.get("debug");
            String payload = (String) extras.get("payload");
            Map<String, String> payloadMapping = new HashMap<String, String>();
            payloadMapping = Utils.parseUrlToStringPairs(payload);

            /*if (debug != null) {
            	// server-controlled debug - the server wants to know
                // we received the message, and when. This is not user-controllable,
                // we don't want extra traffic on the server or phone. Server may
                // turn this on for a small percentage of requests or for users
                // who report issues.
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet get = new HttpGet(AppEngineClient.BASE_URL + "/debug?id=" + extras.get("collapse_key"));
                // No auth - the purpose is only to generate a log/confirm delivery
                // (to avoid overhead of getting the token)
                try {
                	client.execute(get);
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                   e.printStackTrace();
                }
            }*/

            Intent launchIntent = new Intent(this, PushDisplayActivity.class);
            launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchIntent.putExtra("title", payloadMapping.get("title"));
            launchIntent.putExtra("message", payloadMapping.get("message"));
            LauncherUtils.generateNotification(context, payloadMapping.get("message"), payloadMapping.get("title"), launchIntent);
        }
    }
}