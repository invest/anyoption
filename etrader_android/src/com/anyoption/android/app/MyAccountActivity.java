package com.anyoption.android.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.InvestmentsMethodRequest;
import com.anyoption.json.results.InvestmentsMethodResult;
import com.anyoption.json.results.UserMethodResult;

public class MyAccountActivity extends BaseInsuranceActivity {
	private MenuViewBase lastOpenGroup;
	protected MyOptionView myOptions;
	private MenuViewBase personalDetails;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_account);
        menuScrollView = (ScrollView)findViewById(R.id.scrollViewMenu);
        LinearLayout menu = (LinearLayout)findViewById(R.id.linearLayoutMenu);
        for (int i = 0 ; i < menu.getChildCount() ; i++) {
        	((MenuViewBase)menu.getChildAt(i)).setOnMenuStripClickListener(new OnMenuStripClickListener() {
				public void onClickCloseAll(MenuViewBase menuViewBase) {
					manageOpenMenuStrip(menuViewBase);
				}
			});
        }
        
        myOptions = (MyOptionView) findViewById(R.id.myOptionView);
        myOptions.setLs(ls);
        myOptions.setMenuScrollView(menuScrollView);
        ((MyOptionView) findViewById(R.id.settledOptionView)).setMenuScrollView(menuScrollView);
        manageOpenMenuStrip(myOptions);
        ((TextView)findViewById(R.id.TextViewHeader)).setText(getString(R.string.header_myaccount).toUpperCase());
    }

	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	 // in case we return from personal details activity after succsed update
    	if (resultCode == Activity.RESULT_OK &&	
    			requestCode == Constants.REQUEST_CODE_CHANGE_PERSONAL_DETAILS && 
    			data != null ) {
    		User user = (User) data.getExtras().getSerializable(Constants.RESULT_USER_OBJECT);
    		if (null != user) {
    			((Initializable) personalDetails).initialize();
    		}
    	}
    	//in case we return from bonus details
    	if (resultCode == Activity.RESULT_OK &&	
    			requestCode == Constants.REQUEST_CODE_BONUS_DETAILS && 
    			data != null ) {
    			BonusView bonus = (BonusView) findViewById(R.id.myAccountBonusView);
    			//twice to close and open again(refresh the tab because we had waived
    			manageOpenMenuStrip(bonus);
    			manageOpenMenuStrip(bonus);
    		}
    	
    	
//    	//in case we return from inboxActivity
//    	if (resultCode == Activity.RESULT_OK &&	data != null) {
//    		InboxView inbox = (InboxView) findViewById(R.id.myAccountInboxView);
//    		// for updating the new arrays
//    		if (requestCode == Constants.REQUEST_CODE_INBOX_CONTENT) {
//	    		ArrayList<Integer> update = (ArrayList<Integer>) data.getExtras().getSerializable(Constants.RESULT_UPDATES_ARRAY);
//	    		if (null != update) {
//	    			inbox.markRowsAsRead(update);
//	    		}
//    		}
//    		//for delete
//    		if (requestCode == Constants.REQUEST_CODE_INBOX_CONTENT) {
//	    		Integer delete = (Integer) data.getExtras().getSerializable(Constants.RESULT_UPDATES_DELETE);
//	    		if (null != delete) {
//	    			inbox.markAsDeleted(delete);
//	    		}
//    		}
//    		inbox.loadXRows();// just for test :TODO: change and move from here	
//    	}
    }   
    
    @Override
	public void getUserCallback(Object resultObj) {
        super.getUserCallback(resultObj);
        UserMethodResult result = (UserMethodResult) resultObj;
        ((TextView)findViewById(R.id.textViewMyAccountBalance)).setText(result.getUser().getBalanceWF());
        InvestmentsMethodRequest request = new InvestmentsMethodRequest();
        request.setSettled(false);
        showPersonalDetailsView();
        Utils.requestService(this, responseH, "myOptionsCallBack", request, new InvestmentsMethodResult(), "getUserInvestments");
    }

	protected void showPersonalDetailsView() {
		if(Utils.isRegulated(this) && Utils.isAfterFirstDeposit(this)){
        	personalDetails = (MenuViewBase) findViewById(R.id.myAccountPersonalDetailsMain);
        } else {
        	personalDetails = (MenuViewBase) findViewById(R.id.myAccountPersonalDetails);
        }
        personalDetails.setVisibility(View.VISIBLE);
	}
    
    public void myOptionsCallBack(Object resultObj) {
    	InvestmentsMethodResult investmentsResult = (InvestmentsMethodResult) resultObj;
    	((TextView) findViewById(R.id.textViewMyAccountMyOptions)).setText(investmentsResult.getSumAllInvAmount());
    }
    
    @Override
    protected void showInsurancesButton() {
        Log.v(getLocalClassName(), "Showing insurance button - type: " + insurancesType + " close: " + insuranceCloseTime);
        super.showInsurancesButton();
        if (null != myOptions) {
            myOptions.showInsurancesRow(insurancesType, insuranceCloseTime);
        }
    }

    @Override
    protected void hideInsurancesButton() {
        Log.v(getLocalClassName(), "Hide insurance button.");
        if (null != myOptions) {
            myOptions.hideInsurancesRow();
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        User user = ap.getUser();
        if (null != user) {
        	if (user.isAcceptedTerms() == false) {
        		Intent intent = new Intent(this, TermsActivity.class).putExtra(Constants.ACTIVITY_BEFORE_TERMS, "my_account");
        		startActivity(intent);
        		finish();
        	}
        }
//        if (insurancesCount > 0) {
//            myOptions.showInsurancesRow(insurancesType, insuranceCloseTime);
//        }
        if (null != myOptions) {
        	myOptions.onResume();
        }
    }
    
    @Override
    public void onPause() {
    	Log.v("anyoption", "MyAccountActivity.onPause");
    	if (null != myOptions) {
            myOptions.onPause();
        }
        super.onPause();
    }
    
    @Override
    public int getInfoPageId() {     	
        return R.layout.security;
    }
}