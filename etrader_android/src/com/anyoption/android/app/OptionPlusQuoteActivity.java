package com.anyoption.android.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Investment;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.OptionPlusMethodRequest;
import com.anyoption.json.results.MethodResult;
import com.anyoption.json.results.OptionPlusMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;


public class OptionPlusQuoteActivity extends BaseLightstreamerActivity {
	private LinearLayout timeView;
	private Investment inv;
	private Context context;
	private String priceOfferValue;
	private double price;
	private CountDownTimer timer;
	private Button sell;
	private boolean clockIsRun;
	private boolean isAvailable;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	this.context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_quote_pop_up);
        inv = (Investment)getIntent().getSerializableExtra("Investment");
        getQuoteSetParamAndStartTimer();
        AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        User user = ap.getUser();
        ls = getLightstreamerConnectionHandler(user);
        Utils.addLightstreamerTable(this, new String[] {"op_" + inv.getMarketId()}, ls, new AOTPSTableListener(this), 1.0);
        isAvailable = true;
    }
    
    public void getQuoteSetParamAndStartTimer() {
    	((TextView)findViewById(R.id.textViewGetPriceNote)).setText(getString(R.string.optionPlus_getPrice_note));
        OptionPlusMethodRequest request = new OptionPlusMethodRequest();
        request.setInvestmentId(inv.getId());
        setButtonClicks();
        Utils.requestService(this, responseH, "priceCallBack", request, new OptionPlusMethodResult(), "getPrice");
    }
    
    public void setButtonClicks() {
    	sell = (Button) findViewById(R.id.buttonSell);
    	sell.setText(R.string.optionPlus_buy);
    	
    	sell.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				timer.cancel();
				OptionPlusMethodRequest request = new OptionPlusMethodRequest();
				request.setPrice(price);
				request.setInvestmentId(inv.getId());
				Utils.requestService(context, responseH, "sellCallBack", request, new MethodResult(), "optionPlusFlashSell");
			}
		});
    }
    
    public void setTextViewByInvestment() {
    	 Button notNow = (Button) findViewById(R.id.buttonNotNow);
         notNow.setOnClickListener(new OnClickListener() {
 			public void onClick(View v) {
 				finish();
 			}
 		});
    	if (clockIsRun) {
    		findViewById(R.id.linearLayoutTimer).setVisibility(View.VISIBLE);
    	}
    	//setting the investment details
        TextView assetName = (TextView) findViewById(R.id.textViewAssetName);
        TextView currentLevel = (TextView) findViewById(R.id.textViewCurrentLevel);
        TextView typeOption = (TextView) findViewById(R.id.textViewOption);
        TextView level = (TextView) findViewById(R.id.textViewLevel);
        TextView purchasedAt = (TextView) findViewById(R.id.textViewPurchasedAt);
        TextView expireTime = (TextView) findViewById(R.id.textViewInvestmentExpireTime);
        TextView investment = (TextView) findViewById(R.id.textViewInvestment);
        TextView price = (TextView) findViewById(R.id.textViewPrice);
        
        assetName.setText(inv.getAsset());
        currentLevel.setText(inv.getCurrentLevelTxt());
        
        String type = "";
		if (inv.getTypeId() == Investment.INVESTMENT_TYPE_CALL) {
			type = context.getString(R.string.investments_call);
		}
		if (inv.getTypeId() == Investment.INVESTMENT_TYPE_PUT) {
			type = context.getString(R.string.investments_put);
		}
		typeOption.setText(type);
        level.setText(inv.getLevel());
        purchasedAt.setText(inv.getTimePurchaseTxt());
        expireTime.setText(inv.getTimeEstClosingTxt());
        investment.setText(inv.getAmountTxt());
        price.setText(priceOfferValue);
    }
    
    public void priceCallBack(Object result) {
    	OptionPlusMethodResult optionPlusResult = (OptionPlusMethodResult)result;
    	priceOfferValue = optionPlusResult.getPriceTxt();
   
    	price = Double.valueOf(optionPlusResult.getPrice());
    	
    	timeView = (LinearLayout) findViewById(R.id.linearLayoutClock);
    	clockIsRun = true;
    	setTextViewByInvestment();
    	timer = new CountDownTimer(7000, 10) {
    				
					@Override
					public void onTick(long millisUntilFinished) {
						char time[] = Utils.timeToSecondsFormat(millisUntilFinished).toCharArray();
						
						for (int i = 0 ; i < timeView.getChildCount() ; i++) {
							((TextView) timeView.getChildAt(i)).setText(Character.toString(time[i]));
						}
					}
					
					@Override
					public void onFinish() {
						changeDesignForWaitStage();
						clockIsRun = false;
					}
				}.start();
    }
    
    public void changeDesignForWaitStage() {
    	findViewById(R.id.linearLayoutTimer).setVisibility(View.INVISIBLE);
    	sell = (Button) findViewById(R.id.buttonSell);
    	sell.setText(R.string.optionPlus_getQuote);
    	sell.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				getQuoteSetParamAndStartTimer();
			}
		});
    	((TextView)findViewById(R.id.textViewGetPriceNote)).setText(getString(R.string.optionPlus_getPrice_note_expired));
    	((TextView)findViewById(R.id.textViewPrice)).setText("");
    }
    
    public void sellCallBack(Object result) {
    	MethodResult res = (MethodResult)result;
    	Log.d(this.getPackageName(), "sellCallBack - start");
    	if (res.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
    		Log.d(this.getPackageName(), "sellCallBack - Success");
    		LayoutInflater inflater = getLayoutInflater();
    		LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.option_plus_pop_up, null, false);
    		//building the title layout in flip mode
        	LinearLayout llMainTitle = (LinearLayout) inflater.inflate(R.layout.gm_plus_popup_header, null);
    		((TextView) llMainTitle.findViewById(R.id.textViewMarketName)).setText(inv.getAsset()) ;
    		TextView amount = (TextView) ll.findViewById(R.id.textViewOptionPlusAmount);
    		amount.setText(getResources().getString(R.string.optionPlus_getPrice_success_note,priceOfferValue));
    		Button ok = (Button) ll.findViewById(R.id.buttonOptionPlusSellPopUp);
    		Utils.showCustomAlert(this, ll, ok, new OnDismissListener() {
				public void onDismiss(DialogInterface arg0) {
					finish();
				}
			}, llMainTitle);
    	} else {
    		Utils.showAlert(context, "", res.getUserMessages()[0].getMessage(), context.getString(R.string.alert_title));
    		Log.d(this.getPackageName(), "sellCallBack - failed");
    	}
    }
    
        public void updateItem(int itemPos, String itemName, UpdateInfo update) {
    	try {
//    		Log.d(getLocalClassName(), "got new itemName = " + itemName);
        	if (update.isValueChanged("ET_STATE")) {
                int oppState = Integer.parseInt(update.getNewValue("ET_STATE"));
               // Log.d(getLocalClassName(), "got new oppState = " + oppState);
                if (oppState >= Opportunity.STATE_CLOSING_1_MIN) {
                 //   Log.d(getLocalClassName(), "got new state = " + oppState);
                    responseH.post(new HideButtonRunnable());
                }
            }
        	
	    	if (update.isValueChanged(skinGroup.getLevelUpdateKey())) {
	    		TextView view = (TextView)findViewById(R.id.textViewCurrentLevel);
	    		responseH.post(new MessageRunnable(update.getNewValue(skinGroup.getLevelUpdateKey()), view));
	    	}
        } catch (Exception e) {
            Log.e(getLocalClassName() + " chart", "Problem processing updateItem.", e);
        }
    }
    
    class HideButtonRunnable implements Runnable {
        
        public void run() {
        	isAvailable = false;
        	Log.d(getLocalClassName(), "hide sell/get qoute Button");
        	findViewById(R.id.buttonSell).setVisibility(View.GONE);
        	((TextView)findViewById(R.id.textViewGetPriceNote)).setText(getString(R.string.optionPlus_getPrice_note_waiting_for_expiry));
        	((TextView)findViewById(R.id.textViewPrice)).setText("");
        }
    }
    
    private class MessageRunnable implements Runnable {
    	private String message;
    	private TextView view;
    	MessageRunnable(String message, TextView view) {
    		super();
    		this.message = message;
    		this.view = view;
    	}

		public void run() {
		    if (null != view) {
    			view.setText(this.message);
    			inv.setCurrentLevelTxt(message);
		    }
		}
    	
    }
    
    @Override 
    public void onConfigurationChanged(Configuration newConfig) { 
        super.onConfigurationChanged(newConfig); 
        String langCode = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE).getString(Constants.PREF_LOCALE, null);
        if (null != langCode) {
        	AnyoptionApplication app = (AnyoptionApplication) getApplication();
    		app.changeLocale(langCode);
        }
        setContentView(R.layout.get_quote_pop_up);
        setTextViewByInvestment();
        if (clockIsRun) {
        	setButtonClicks();
          	timeView = (LinearLayout) findViewById(R.id.linearLayoutClock);
        } else {
        	changeDesignForWaitStage();
        	if (!isAvailable) {
        		responseH.post(new HideButtonRunnable());
        	}
        }
    }
    
    @Override
    public int getInfoPageId() {
    	return R.layout.info_page_option_plus;
    }
}