package com.anyoption.android.app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


public class AnyoptionWebView extends Activity {
	
	private ImageView header;
	private WebView webView;
	private TextView clock;
	private long serverTimeOffset;
	private Handler clockHandler;
	private ClockTick clockTick;
	private ProgressBar progress;
	private boolean isRestoredState;       // indication when rotate is done
	private boolean isHasInternetProblem;  // indication when network connection not available

	private DateJavaScriptInterface dateJSInterface;
	private AlertJavaScriptInterface alertJSInterface;
	
	private final int DIALOG_DATE = 1;
	private final int DIALOG_ALERT = 2;
	private final int DIALOG_NO_CONNECTION = 3;	
	private final String HANDLER_JS_CODE = "jscode";
	
	private Dialog dateDialog;
	private Dialog alertDialog;
	private Dialog netErrorDialog;
		
	/** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);        
        header = (ImageView) findViewById(R.id.header);
        clock = (TextView) findViewById(R.id.clock);
        progress = (ProgressBar) findViewById(R.id.progress);
        webView = (WebView) findViewById(R.id.webview);
        //webView.clearCache(true);
        isRestoredState = false; 
        isHasInternetProblem = false;              
       	Log.i("Notification service","starting");
    }           
    
    /**
     * Called after orientation change, for restoring the last webView instance
     * Note: not in use cause i added onConfigurationChanged handle
     */
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
    	Log.v("event", "onRestoreInstanceState");    	    
        if (null != savedInstanceState) {  // on orientation change    
    		webView.restoreState(savedInstanceState);    		
        	header.setVisibility(View.VISIBLE);
        	webView.setVisibility(View.VISIBLE);
        	clock.setVisibility(View.VISIBLE);
        	progress.setVisibility(View.INVISIBLE);  
    		isRestoredState = true;
    	} else {
    		isRestoredState = false;
    	}
        super.onRestoreInstanceState(savedInstanceState);        
    }         
    
    /**
     * Called after onCreate / onPause. Redirect to home page with duid parameter
     */
    @Override
    public void onResume() {
    	Log.v("event", "onResume");      	
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginsEnabled(true);
        webView.setWebViewClient(new AyoptionWebViewClient());                        
        // add JS interfaces
        dateJSInterface = new DateJavaScriptInterface();
        webView.addJavascriptInterface(dateJSInterface, "DateInterface");
        alertJSInterface = new AlertJavaScriptInterface();
        webView.addJavascriptInterface(alertJSInterface, "AlertInterface");
        isHasInternetProblem = false;
    	if (!isRestoredState) {
            TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String uid = tManager.getDeviceId();
            PackageInfo packageInfo;
            String appVer = "1.3"; 
			try {
				packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
	            if (null != packageInfo.versionName) {
	            	appVer = packageInfo.versionName;
	            }				
			} catch (NameNotFoundException e) {				
				Log.i("Get version name", e.toString());
			}
			
	    	if (null != dateDialog && dateDialog.isShowing()) {
	    		dismissDialog(DIALOG_DATE);
	    	}
	    	if (null != alertDialog && alertDialog.isShowing()) {
	    		dismissDialog(DIALOG_ALERT);
	    	}
	    	if (null != netErrorDialog && netErrorDialog.isShowing()) {
	    		dismissDialog(DIALOG_NO_CONNECTION);	
	    	}
	    	String web_site_link = getString(R.string.WEB_SITE_LINK);
    		webView.loadUrl(web_site_link + "iphoneapp.jsf?duid=" + uid + "&appVer=" + appVer);    	           
    	} else {
    		isRestoredState = false;
    	}
		getServerTimeOffset();
        clockHandler = new Handler();
        clockTick = new ClockTick();
        clockHandler.postDelayed(clockTick, 500);
        super.onResume();        
    }
    
    /**
     * For saving the webView state on orientation change  
     * Note: not in use cause i added onConfigurationChanged handle
     */
    protected void onSaveInstanceState(Bundle outState) {
    	Log.v("event", "onSaveInstanteState");    	    	    
    	webView.saveState(outState);        	
    	super.onSaveInstanceState(outState);
     }
    
    /**
     * Handle configuration change event.
     * For now we are using just orientation change (defined in the AndroidManifest.xml)
     */
/*    public void onConfigurationChanged(Configuration newConfig) {
    	if (null != webView && 
    			null != webView.getUrl()) {
    		webView.loadUrl(webView.getUrl());
    	}
    	super.onConfigurationChanged(newConfig);
   	}*/
    
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    protected void onDestroy() {
    	clockHandler.removeCallbacks(clockTick);
    	isRestoredState = false;    	
    	super.onDestroy();
    }    
    
    private void getServerTimeOffset() {
    	try {
    		String web_site_link = getString(R.string.WEB_SITE_LINK);
	    	URL url = new URL(web_site_link + "time.jsp");
	    	HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
	    	InputStreamReader isr = null;
	    	BufferedReader br = null;
	    	try {
	    		isr = new InputStreamReader(urlConn.getInputStream());
	    		br = new BufferedReader(isr);
	    		String timeStr = br.readLine();
		    	Log.v("time", "ServerTimeStr: " + timeStr);
		    	serverTimeOffset = System.currentTimeMillis() - Long.parseLong(timeStr);
	    	} finally {
	    		try {
	    			br.close();
	    		} catch (Exception e) {
	    			Log.e("time", "Can't close.", e);
	    		}
	    		try {
	    			isr.close();
	    		} catch (Exception e) {
	    			Log.e("time", "Can't close.", e);
	    		}
	    		urlConn.disconnect();
	    	}
    	} catch (Exception e) {
    		Log.e("time", "Can't get server time.", e);
    		serverTimeOffset = 0;
    	}
    }
   
    private class ClockTick implements Runnable {
    	public void run() {
    		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
   			clock.setText(sdf.format(new Date(System.currentTimeMillis() - serverTimeOffset)));
   			clockHandler.postDelayed(clockTick, 500);
    	}
    }
    
    private class AyoptionWebViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
        	Log.v("url", url);
        	// Handle session timeout
        	if (url.indexOf("/jsp/") > -1) {  // if the web got redirect from the mobile app
        		TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                String uid = tManager.getDeviceId(); 
                String web_site_link = getString(R.string.WEB_SITE_LINK);
        		view.loadUrl(web_site_link + "iphoneapp.jsf?duid=" + uid);
        	} else {	
        		view.loadUrl(url);
        	}	
            
        	header.setVisibility(View.INVISIBLE);
        	webView.setVisibility(View.INVISIBLE);
        	clock.setVisibility(View.INVISIBLE);
        	Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        	progress.setPadding((display.getWidth() - progress.getWidth() + progress.getPaddingLeft()) / 2, (display.getHeight() - progress.getHeight() + progress.getPaddingTop()) / 2, 0, 0); 
        	progress.setVisibility(View.VISIBLE);
            return true;
        }               
              
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
        	Log.v("url", "Started: " + url);        	       
        	// Handle session timeout
        	if (url.indexOf("/jsp/") > -1) {  // if the web got redirect from the mobile app
                TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                String uid = tManager.getDeviceId(); 
                String web_site_link = getString(R.string.WEB_SITE_LINK);
                view.loadUrl(web_site_link + "iphoneapp.jsf?duid=" + uid);      		    	    		    		        		
        	} else {	        	
	        	header.setVisibility(View.INVISIBLE);
	        	webView.setVisibility(View.INVISIBLE);
	        	clock.setVisibility(View.INVISIBLE);
	        	Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
	        	progress.setPadding((display.getWidth() - progress.getWidth() + progress.getPaddingLeft()) / 2, ((display.getHeight() - progress.getHeight() + progress.getPaddingTop()) / 2), 0, 0); 
	        	progress.setVisibility(View.VISIBLE);
        	}	        	       	
        }              
                
        public void onPageFinished(WebView view, String url) {        	        
        	Log.v("url", "Done: " + url);
        	if (!isHasInternetProblem) {
	        	header.setVisibility(View.VISIBLE);
	        	webView.setVisibility(View.VISIBLE);
	        	clock.setVisibility(View.VISIBLE);        	              	        
	        	progress.setVisibility(View.INVISIBLE);
        	}	
        }               

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        	Log.v("url", "Err: " + errorCode + " description: " + description + " url: " + failingUrl);
    		isHasInternetProblem = true;
    		header.setVisibility(View.INVISIBLE);
        	webView.setVisibility(View.INVISIBLE);
        	clock.setVisibility(View.INVISIBLE);        		  
        	progress.setVisibility(View.INVISIBLE); 
    		showDialog(DIALOG_NO_CONNECTION);          	
        	// check connectivity status
        	/*ConnectivityManager conStatus = (ConnectivityManager) view.getContext().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        	if (null != conStatus &&
        				((null != conStatus.getActiveNetworkInfo() &&
        						!conStatus.getActiveNetworkInfo().isConnected()) || 
        						null == conStatus.getActiveNetworkInfo())) {
        		isHasInternetProblem = true;
        		header.setVisibility(View.INVISIBLE);
            	webView.setVisibility(View.INVISIBLE);
            	clock.setVisibility(View.INVISIBLE);        		  
            	progress.setVisibility(View.INVISIBLE); 
        		showDialog(DIALOG_NO_CONNECTION);        		
        	} else {
        		isHasInternetProblem = false;
        		header.setVisibility(View.VISIBLE);
            	webView.setVisibility(View.VISIBLE);
            	clock.setVisibility(View.VISIBLE);        		  
            	progress.setVisibility(View.INVISIBLE);  
        	}*/        	      
        }
    }  
    
    /**
     * Handle the optionsMenu initiate
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
    
//    /**
//     * called when optionItem selected
//     * load the proper url by the selected item
//     */
//    public boolean onOptionsItemSelected(MenuItem item) {
//    	switch (item.getItemId()) {    	
//        case R.id.tradeMItem:        	        
//            webView.loadUrl(SERVER_URL + "/mobile/trade_binary_options.jsf");                
//            return true;
//        case R.id.moreMItem:        	  
//            webView.loadUrl(SERVER_URL + "/mobile/mainMenu.jsf");                
//            return true;
//        case R.id.assetindexMItem:        	        
//            webView.loadUrl(SERVER_URL + "/mobile/assetIndex.jsf");                
//            return true;
//        case R.id.contactMItem:			        	           	
//            webView.loadUrl(SERVER_URL + "/mobile/contactus.jsf");                
//            return true;
//        case R.id.depositMItem:        	
//        	webView.loadUrl(SERVER_URL + "/mobile/conditionDeposit.jsf");                
//            return true;
//    	}
//    	return false;            
//    }
       
    /**
     * Java Script Interface class
     * For operating the date picker dialog from the webView
     *  
     * @author Kobi
     *
     */
    public class DateJavaScriptInterface {  
    	private String value;  // input field
    	private String lable;  // label field
    	private Calendar cal;
    	    	
        public void pickDate(String value, String lable, String date) {
        	this.value = value;
        	this.lable = lable;
        	SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yy");
            try {
            	this.cal = Calendar.getInstance();
				this.cal.setTime(sd.parse(date));
			} catch (ParseException e) {
				Log.e("DateparsingError", e.toString());
			}
            showDialog(DIALOG_DATE);          
        }               
    }
    
    /**
     * Java Script Interface class
     * For operating the alert dialog from the webView
     *  
     * @author Kobi
     *
     */
    public class AlertJavaScriptInterface {  
    	private String msg;
    	    	
        public void showAlert(String msg) {
        	this.msg = msg;
            showDialog(DIALOG_ALERT);          
        }  
    }    
    
    /**
     * Run Java Script code on the webView
     * @param script
     */
    public void loadScript(String script) {      
        webView.loadUrl("javascript:(function() { " + script + "})()");             
    } 
    
    /**
     * Listener for DatePicker change
     * Run java script code on the webView for setting the selected date 
     */
    private DatePickerDialog.OnDateSetListener mDateSetListener =
        new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            	Calendar c = Calendar.getInstance();
            	c.set(Calendar.YEAR, year);
            	c.set(Calendar.MONTH, monthOfYear);
            	c.set(Calendar.DAY_OF_MONTH, dayOfMonth); 
            	SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yy");
            	String dateStr = sd.format(c.getTime());            	            	
            	String value = dateJSInterface.value;
            	String lable = dateJSInterface.lable;
                       
            	Message msg1 = new Message();
            	Bundle b1 = new Bundle();
            	b1.putString(HANDLER_JS_CODE, "document.getElementById('" + value + "').value = '" + dateStr + "';");
            	msg1.setData(b1);           	
            	loadJSHandler.sendMessage(msg1);

            	Message msg2 = new Message();
            	Bundle b2 = new Bundle();
            	b2.putString(HANDLER_JS_CODE, "document.getElementById('" + lable + "').innerHTML = '" + dateStr + "';");
            	msg2.setData(b2);           	
            	loadJSHandler.sendMessage(msg2);            	
            	
            	if (value.indexOf("lastLevelsForm:from") > -1) {  // operate onClick
            		Message msg3 = new Message();
                	Bundle b3 = new Bundle();
                	b3.putString(HANDLER_JS_CODE, "document.getElementById('lastLevelsForm:hiddenbtn').onclick()");
                	msg3.setData(b3);           	
                	loadJSHandler.sendMessage(msg3);            	            		            		
            	}
            }
       	};
   
   /**
    * Handler for loading java script code inside the webView thread
    * Note: when loading the js from DatePickerDialog.OnDateSetListener() it cause to the exception below:
    * android.view.ViewRoot$CalledFromWrongThreadException
    */
   final Handler loadJSHandler = new Handler() {
	    public void handleMessage(Message msg) {
	        String jsCode = msg.getData().getString(HANDLER_JS_CODE);
	        if (null != jsCode) {
	        	loadScript(jsCode);
	        } else {
	        	Log.w("Handler", "No JS code to load!");
	        }
	    }	   
   };

   /**
	* Listener for Alert dialog click
	*/
    private DialogInterface.OnClickListener alertListener =
    	new DialogInterface.OnClickListener() {			
			public void onClick(DialogInterface dialog, int which) {
				Log.v("alertDialog", "message from meb finished");
			}
		};
        
   /**
	* Listener for No connection Alert dialog click
	*/
    private DialogInterface.OnClickListener noConnectionAlertListener =
    	new DialogInterface.OnClickListener() {			
			public void onClick(DialogInterface dialog, int which) {
				Log.v("alertDialog", "no connection");
				AnyoptionWebView.this.finish();  // close the activity
			}
		};
		
    /**
     * Handle on create dialog event
     * Called in the first time the dialog created.  	
     */
    @Override
    protected Dialog onCreateDialog(int id) {
    	Dialog dialog;
        switch (id) {
        case DIALOG_DATE:
            dialog = new DatePickerDialog(this,
                        mDateSetListener, 
                        dateJSInterface.cal.get(Calendar.YEAR),
                        dateJSInterface.cal.get(Calendar.MONTH),
                        dateJSInterface.cal.get(Calendar.DAY_OF_MONTH));
            dateDialog = dialog;
            break;
        case DIALOG_ALERT:
        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
        	builder.setMessage(alertJSInterface.msg)
        		   .setCancelable(false)
        	       .setPositiveButton(this.getString(R.string.alert_button), alertListener);
        	dialog = builder.create();
        	alertDialog = dialog;
        	break;
        case DIALOG_NO_CONNECTION:
        	AlertDialog.Builder ncbuilder = new AlertDialog.Builder(this);
        	ncbuilder.setMessage(this.getString(R.string.noConnectionMsg))
        		   .setTitle(this.getString(R.string.alert_title))	
        		   .setCancelable(false)
        	       .setPositiveButton(this.getString(R.string.alert_button), noConnectionAlertListener);
        	dialog = ncbuilder.create();
        	netErrorDialog = dialog;
        	break;        	
   		default:
   			dialog = super.onCreateDialog(id);
        }	
        return dialog;
    }
    
    /**
     * Handle on prepare dialog event
     * Called every time the dialog is opened  	
     */
    protected void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
        case DIALOG_DATE:
            ((DatePickerDialog) dialog).updateDate(dateJSInterface.cal.get(Calendar.YEAR),
            		dateJSInterface.cal.get(Calendar.MONTH),
            		dateJSInterface.cal.get(Calendar.DAY_OF_MONTH));            
            break;
        case DIALOG_ALERT:        	
        	((AlertDialog) dialog).setMessage(alertJSInterface.msg);
        	break;
   		default:
   			break;
        }	    	
    }

    /**
     * In from right translate animation
     */
    private Animation inFromRightAnimation() {    
    	Animation inFromRight = new TranslateAnimation(
    	Animation.RELATIVE_TO_PARENT,  +1.0f, Animation.RELATIVE_TO_PARENT,  0.0f,
    	Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,   0.0f
    	);    
    	
    	inFromRight.setDuration(500);
    	inFromRight.setFillAfter(true);
    	inFromRight.setFillEnabled(true);
    	inFromRight.setInterpolator(new AccelerateInterpolator()); 
    	return inFromRight;
    }	    	
    
    /**
     * Out to left translate animation
     */
    private Animation outToLeftAnimation() {
    	Animation outtoLeft = new TranslateAnimation(
	    	Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,  -1.0f,
	    	Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,   0.0f
    	);
    	
    	outtoLeft.setAnimationListener(new AnimationListener() {
    		  public void onAnimationStart(Animation animation) {
    		   Log.v("animation", "start");    		  
    		  }

    		  public void onAnimationRepeat(Animation animation) {
    			  Log.v("animation", "repet");    		   
    		  }

    		   public void onAnimationEnd(Animation animation) {
    			   Log.v("animation", "end");      			       			   
    		   }
    	});      	
    	
    	outtoLeft.setDuration(500);
    	outtoLeft.setFillAfter(true);
    	outtoLeft.setFillEnabled(true);
    	outtoLeft.setInterpolator(new AccelerateInterpolator());
    	return outtoLeft;
   	}
    
    /**
     * Store an object on orientationChange and get it in the onCreate with getLastNonConfigurationInstance()   
     */    
//    public Object onRetainNonConfigurationInstance() {
//      if (null != obj) {
//          return(obj);
//      }    
//      return super.onRetainNonConfigurationInstance();
//    }
    
}