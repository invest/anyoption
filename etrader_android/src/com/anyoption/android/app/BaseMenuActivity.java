/**
 * 
 */
package com.anyoption.android.app;

import android.util.Log;
import android.view.View;
import android.widget.ScrollView;

/**
 * @author AviadH
 *
 */
public class BaseMenuActivity extends BaseActivity {
	protected View lastOpenGroup;
	protected ScrollView menuScrollView;
	
	
	 /*manage the menu on her activiy and close and open according to type of component
	 * (if he is custom ,the close and open will occured in the custom view itself
	 * an if he is created dynmically inside an activity the close and open will occure in the activity*/
    public void manageOpenMenuStrip(View target) {
    	Log.d(this.getPackageName(), "manageOpenMenuStrip method");
    	boolean shouldOpen = false;
    	//check if target and last are menuBase (custom component menu)
    	boolean isTargetMenuBase = false;
    	boolean isLastMenuBase = false;
    	if (target instanceof MenuViewBase) {
    		isTargetMenuBase = true;
    	}
    	if (lastOpenGroup instanceof MenuViewBase) {
    		isLastMenuBase = true;
    	}
		if (lastOpenGroup != target) {
    		shouldOpen = true;
    		if (null != lastOpenGroup) {
    			if (isLastMenuBase) {
    				((MenuViewBase)lastOpenGroup).closeGroup();
    			} else {
    				closeGroup(lastOpenGroup);
    			}
    		}
    	} else {
    		if (isTargetMenuBase) {
    			((MenuViewBase)target).closeGroup();
    		} else {
    			closeGroup(target);
    		}
    		lastOpenGroup = null;
    	}
    	if (shouldOpen) {
    		if (isTargetMenuBase) {
    			((MenuViewBase)target).openGroup();
    		} else {
    			openGroup(target);
    		}
    		if (null != menuScrollView) {
    			menuScrollView.post(new ScrollViewScrollRunnable(menuScrollView, target));
    		}
    		lastOpenGroup = target;
    	}
    }
    
    public ScrollView getMenuScrollView() {
    	return menuScrollView;
    }
    
    public void openGroup(View target) { 	
    }
    
    public void closeGroup(View target) { 	
    }
    
    @Override
    public void onBackPressed() {
    	// TODO Auto-generated method stub
    	if (null != lastOpenGroup) {
    		manageOpenMenuStrip(lastOpenGroup);
    	} else {
    		super.onBackPressed();
    	}
    }
    
    class ScrollViewScrollRunnable implements Runnable {
	    private ScrollView sView;
	    private View v;
	    
	    public ScrollViewScrollRunnable(ScrollView sView, View v) {
	        this.sView = sView;
	        this.v = v;
	    }
	    
	    public void run() {
	        //scroll to the right Y so the group will be in the top
	        int[] corners = new int[2]; 
	        v.getLocationInWindow(corners);
	        int targetY = corners[1];
	        sView.getLocationInWindow(corners);
	        int groupY = targetY - corners[1] + sView.getScrollY();
	        sView.scrollTo(0, groupY - 10);
	    }
	}
}
