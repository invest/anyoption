package com.anyoption.android.app.ls;

import android.util.Log;

import com.lightstreamer.ls_client.HandyTableListener;
import com.lightstreamer.ls_client.UpdateInfo;

public class MarketGroupListener implements HandyTableListener {

    private LSUiUpdater ui;
    private final String TAG = "MarketGroupListener";
    
    public MarketGroupListener(LSUiUpdater ui) {
        this.ui = ui;
    }

	public void onRawUpdatesLost(int arg0, String arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	public void onSnapshotEnd(int arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	public void onUnsubscr(int arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	public void onUnsubscrAll() {
		// TODO Auto-generated method stub
		
	}

	public void onUpdate(int itemPos, String itemName, UpdateInfo update) {
		// TODO Auto-generated method stub
		Log.d(TAG, "itemPos = " + itemPos + " itemName = " + itemName);
		this.ui.updateLightstreamerItem(itemPos, itemName, update);
	}
}