package com.anyoption.android.app.ls;

import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.AnyoptionApplication;
import com.anyoption.android.app.R;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.enums.SkinGroup;
import com.lightstreamer.ls_client.UpdateInfo;

/**
 * This is where StockListDemo application interacts
 * with Android user interface.
 */
public class LSUiUpdater {
	private final String TAG = "LSUiUpdater";
	private Activity activity;
    private TextView statusTextView;
    private ImageView statusImageView;
    private Handler handler;
    private HashMap<String, TableRow> itemMap;
    private int scrollViewY;
    private int scrollViewHeight;
    private boolean[] snapshot;
    private TableLayout tableLayout;
    
    protected SkinGroup skinGroup;

	public LSUiUpdater(Activity activity, HashMap<String, TableRow> itemMap, int scrollViewY, int scrollViewHeight, TableLayout tableLayout) {
		this.activity = activity;
		this.handler = new Handler();
        this.itemMap = itemMap;
        this.scrollViewY = scrollViewY;
        this.scrollViewHeight = scrollViewHeight;
        snapshot = new boolean[itemMap.size() + 1];
        this.tableLayout = tableLayout;
        
        AnyoptionApplication application = (AnyoptionApplication) activity.getApplication();
        skinGroup = application.getSkins().get(application.getSkinId()).getSkinGroup();
	}

    /**
     * Throws error message at user.
     * @param title error title
     * @param message error message
     */
    public void alert(String title, String message, boolean fatal) {
    	Builder dialog = new AlertDialog.Builder(this.activity);
    	if (message != null) {
    		dialog.setMessage(message);
    	}
    	if (title != null) {
    		dialog.setTitle(title);
    	}
    	dialog.setCancelable(false);
    	if (fatal) {
	    	dialog.setNeutralButton("OK", new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					LSUiUpdater.this.activity.finish();
				}
	    	});
    	} else {
    		dialog.setNeutralButton("OK", null);
    	}
    	dialog.show();
    }

    private class MessageRunnable implements Runnable {
    	private String message;
    	private TextView view;
    	MessageRunnable(String message, TextView view) {
    		super();
    		this.message = message;
    		this.view = view;
    	}

		public void run() {
		    if (null != view) {
    			Log.d(TAG, "view text = " + view.getText().toString() + " , Level " + message);
    			view.setText(this.message);
		    }
		}
    	
    }
    
    private class ChangeRowStateRunnable implements Runnable {
        private String itemName;
    	private TableRow tr;
    	private Boolean isOpen;
    	private UpdateInfo update;
    	ChangeRowStateRunnable(String itemName, TableRow tr, Boolean isOpen, UpdateInfo update) {
    		super();
    		this.itemName = itemName;
    		this.tr = tr;
    		this.isOpen = isOpen;
    		this.update = update;
    	}

		public void run() {
		    try {
    		    String marketName = ((TextView) tr.findViewById(R.id.textViewGroupMarketName)).getText().toString();
    		    int rowIndex = tableLayout.indexOfChild(tr);
    		    tableLayout.removeView(tr);
    		    TableRow newRow = (TableRow) activity.getLayoutInflater().inflate(isOpen ? R.layout.markets_groups_row : R.layout.markets_groups_row_closed, tableLayout, false);
    		    ((TextView) newRow.findViewById(R.id.textViewGroupMarketName)).setText(marketName);
    		    if (isOpen) {
    		        TextView level = (TextView) newRow.findViewById(R.id.textViewGroupMarketLevel);
    		        level.setText(update.getNewValue(skinGroup.getLevelUpdateKey()));
    		        updateCellColor(level, update.getNewValue(skinGroup.getColorUpdateKey()));
                    ((TextView) newRow.findViewById(R.id.textViewGroupMarketExpTime)).setText(Utils.reformatTimeShort(update.getNewValue("ET_EST_CLOSE")));
    		    } else {
    		        int state = Integer.parseInt(update.getNewValue("ET_STATE"));
    		        if (state == Opportunity.STATE_SUSPENDED) {
    		            newRow.findViewById(R.id.textViewGroupAvailableAt).setVisibility(View.GONE);
    		        } else {
    		            ((TextView) newRow.findViewById(R.id.textViewGroupOpenTime)).setText(Utils.reformatTimeLong(activity, update.getNewValue("ET_EST_CLOSE")));
    		        }
    		    }
                ((TextView) newRow.findViewById(R.id.textViewGroupMarketReturn)).setText(update.getNewValue("ET_ODDS"));
                newRow.setTag(tr.getTag());
    		    tableLayout.addView(newRow, rowIndex);
                itemMap.put(itemName, newRow);
		    } catch (Exception e) {
		        Log.e("lightstreamer", "Can't change row state.", e);
		    }
		}
    	
    }

    private class BackgroundRunnable implements Runnable {
    	private int resId;
    	private ImageView view;
    	BackgroundRunnable(int resId, ImageView view) {
    		super();
    		this.resId = resId;
    		this.view = view;
    	}

		public void run() {
			view.setBackgroundResource(this.resId);
		}
    	
    }

    /**
     * Update application status message.
     * @param message message text
     * @param resId Android Application resource Id
     */
    public void updateStatus(String message, int resId) {
    	this.handler.postDelayed(
    			new MessageRunnable(message, this.statusTextView), 0);
    	if (resId != -1) {
	    	this.handler.postDelayed(
	    			new BackgroundRunnable(resId, this.statusImageView), 0);
    	}
    }

    private class ColorRunnable implements Runnable {
    	private int color;
    	private View view;
    	private boolean valid = true;

    	ColorRunnable(int color, View view) {
    		super();
    		this.color = color;
    		this.view = view;
    	}

		public void run() {
		    try {
    			if (this.valid) {
    				((TextView)view).setTextColor(this.color);	
    			}
		    } catch (Exception e) {
		        Log.e("anyoption", "Can't update TextView color.", e);
		    }
		}

//		public void invalidate() {
//			this.valid = false;
//		}

    }

    private void updateCellColor(View view, String upDown) {
    	int color = 0xFF31f616; /* green */
    	if (upDown.equals("0")) {
    		color = 0xFFf0371f; /* red */
    	}
    	this.handler.postDelayed(new ColorRunnable(color, view), 0);
    }

    /**
     * This method is called by LSTableListener during onUpdate callback,
     * thus is the place where new data is feeded to the Android UI.
     * To avoid event loss, it is advised to keep this method as faster as
     * possible. 
     * @param itemName name of the item being updated
     * @param update UpdateInfo object from Lightstreamer server containing
     * all the update information.
     */
    public void updateLightstreamerItem(int itemPos, String itemName, UpdateInfo update) {
        long t = System.currentTimeMillis();
    	Log.d(TAG, "item name = " + itemName + " , update " + update.toString() + " snapshot: " + update.isSnapshot());

    //	if (this.fields != null) {
	    	TableRow row = this.itemMap.get(itemName);
	  //  	boolean snapshot = update.isSnapshot();
	    	Boolean isOpenState = true;
	    	if (update.isValueChanged("ET_STATE")) {
                int oppState = Integer.parseInt(update.getNewValue("ET_STATE"));
                if (null != update.getOldValue("ET_STATE")) {
                    int oldState = Integer.parseInt(update.getOldValue("ET_STATE"));
                    if (!(oldState >= Opportunity.STATE_OPENED && oldState <= Opportunity.STATE_DONE) &&
                            oppState >= Opportunity.STATE_OPENED && oppState <= Opportunity.STATE_DONE) {
                    	this.handler.post(new ChangeRowStateRunnable(itemName, row, true, update));
                    	return;
                    }
                }
                if (!(oppState >= Opportunity.STATE_OPENED && oppState <= Opportunity.STATE_DONE)) {
                	isOpenState = false;
                	this.handler.post(new ChangeRowStateRunnable(itemName, row, false, update));
                	return;
                }
            }
	    	
	    	if (update.isValueChanged(skinGroup.getLevelUpdateKey()) && isOpenState) {
	    		int[] coordinates = new int[2];
	    		row.getLocationInWindow(coordinates);
	    	    if (!snapshot[itemPos]
	    	            || (coordinates[1] >= scrollViewY && coordinates[1] <= scrollViewY + scrollViewHeight)
	    	            || (coordinates[1] + row.getHeight() >= scrollViewY && coordinates[1] + row.getHeight() <= scrollViewY + scrollViewHeight)) {
	    	        snapshot[itemPos] = true;
    	    		String value = update.getNewValue(skinGroup.getLevelUpdateKey());
    	    		Log.d(TAG, "item name = " + itemName + " , Level " + value);
    	    		TextView view = (TextView)row.findViewById(R.id.textViewGroupMarketLevel);
    		    	this.handler.post(new MessageRunnable(value, view));
	    	    } else {
	    	        Log.v("lightstreamer", "Do not update level.");
	    	    }
	    	}
	    	
	    	if (update.isValueChanged("ET_ODDS") || update.isValueChanged("ET_STATE")) {
	    		String value = update.getNewValue("ET_ODDS");
	    		Log.d(TAG, "item name = " + itemName + " , ET_ODDS " + value);
	    		TextView view = (TextView) row.findViewById(R.id.textViewGroupMarketReturn);
		    	this.handler.post(new MessageRunnable(value, view));
	    	}
	    	
			if (update.isValueChanged("ET_EST_CLOSE")) {
				
				String value = update.getNewValue("ET_EST_CLOSE");
				Log.d(TAG, "item name = " + itemName + " , ET_EST_CLOSE " + value);
				TextView view = null;
				if (isOpenState) {
				    view = (TextView) row.findViewById(R.id.textViewGroupMarketExpTime);
				} else {
				    view = (TextView) row.findViewById(R.id.textViewGroupOpenTime);
				}
				String closeText = "";
				try {
    				if (isOpenState) {
    				    closeText = Utils.reformatTimeShort(value);
    				} else {
    				    closeText = Utils.reformatTimeLong(activity, value);
    				}
				} catch (Exception e) {
				    Log.e("anyoption", "Can't parse closing time.", e);
				}
				this.handler.post(new MessageRunnable(closeText, view));
			}
			
			if (update.isValueChanged(skinGroup.getColorUpdateKey()) && isOpenState) {
				String value = update.getNewValue(skinGroup.getColorUpdateKey());
				Log.d(TAG, "item name = " + itemName + " , " + skinGroup.getColorUpdateKey() + " " + value);
				TextView view = (TextView)row.findViewById(R.id.textViewGroupMarketLevel);
				this.updateCellColor(view, value);
			}
    //	}
	    	/*for (int i = 0; i < this.fields.length; i++) {

		    	double upDown = 0.0;
	    		String field = this.fields[i];
		    	String value = update.getNewValue(field);

		    	if (update.isValueChanged(field)) {
			    	TextView view = (TextView)row.getChildAt(i);
			    	this.handler.postDelayed(
			    			new MessageRunnable(value, view), 0);

		    		if (!snapshot) {
		    			 update cell color 
				    	String oldValue = update.getOldValue(field);
				    	try {
				    		double valueInt = Double.parseDouble(value);
				    		double oldValueInt = Double.parseDouble(oldValue);
				    		upDown = valueInt - oldValueInt;
				    	} catch (NumberFormatException nfe) {  ignore  }
				    	this.updateCellColor(view, upDown);
		    		} else {
		    			 mark entire row as updated 
		    			this.updateCellColor(row, 1.0);
		    		}
			    	
		    	}
	    	}
    	}
*/
		Log.v("lightstreamer", "Time: " + (System.currentTimeMillis() - t));
    }
 }