/**
 * 
 */
package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.SetBackgroundColorRunnable;
import com.anyoption.android.app.util.SetTextRunnable;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Investment;
import com.anyoption.common.beans.base.User;
import com.lightstreamer.ls_client.UpdateInfo;

/**
 * @author AviadH
 *
 */
public class MyOptionAccountPage extends MyOptionView{
	
	public MyOptionAccountPage(Context context) {
		this(context, null);
	}
	
	public MyOptionAccountPage(Context context, boolean isSettled, boolean loadInvestments, LightstreamerConnectionHandler ls) {
		super(context, isSettled, loadInvestments, ls);
	}
	
	public MyOptionAccountPage(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public void makeRowForInvestment(final Investment inv) {
		super.makeRowForInvestment(inv);
		final TableRow tr = (TableRow) inflater.inflate(R.layout.my_option_row, tlMain, false);
		tr.setTag(inv);
		
		//make visible the row design only for her components
		tr.findViewById(R.id.linearLayoutReturn).setVisibility(inv.getOptionPlusFee() > 0 && !isSettled ? View.INVISIBLE : View.VISIBLE);
		tr.findViewById(R.id.linearLayoutAmount).setVisibility(View.VISIBLE);
		tr.findViewById(R.id.linearLayoutTimeAsset).setVisibility(View.VISIBLE);
		if (!isSettled) {
			tr.findViewById(R.id.linearLayoutLevel).setVisibility(View.VISIBLE);
		} else {
			tr.findViewById(R.id.linearLayoutLevel).setVisibility(View.GONE);
		}
		if (inv.getAdditionalInfoType() == Investment.INVESTMENT_ADDITIONAL_INFO_NOT_HOURLY) {
			tr.findViewById(R.id.linearLayoutReturn).setVisibility(View.GONE);
		}

		//take params for filling the components with data
		TextView time = (TextView) tr.findViewById(R.id.textViewMyOptionTime);
		TextView level = (TextView) tr.findViewById(R.id.textViewMyOptionsLevel);
		TextView returnAmount = (TextView) tr.findViewById(R.id.textViewMyOptionsReturn);
		TextView investment = (TextView) tr.findViewById(R.id.textViewMyOptionsInvestment);
		TextView dateOrAsset = (TextView)tr.findViewById(R.id.textViewMyOptionAsset);
		
		if (!isSettled) {
			time.setText(inv.getTimePurchaseTxt().split(" ")[1]);
		} else {
			time.setText(inv.getTimePurchaseTxt().split(" ")[0]);
		}
		dateOrAsset.setText(inv.getAsset());
		level.setText(inv.getLevel());
		investment.setText(inv.getAmountTxt());
		returnAmount.setText(inv.getAmountReturnWF());
		if (isSettled) {
			((TextView)tr.findViewById(R.id.textViewMyOptionsReturnText)).setText(getResources().getString(R.string.investments_refund));
		} 
		
		
		//set the colors for row
		if (isSettled) {
			tr.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_option_row_press));
    		tr.setBackgroundColor(0x50E1E1E1);
			time.setTextColor(0xFFFFFFFF);
			dateOrAsset.setTextColor(0xFFFFFFFF);
			investment.setTextColor(0xFFFFFFFF);
			returnAmount.setTextColor(0xFFFFFFFF);
			((TextView) tr.findViewById(R.id.textViewMyOptionsInvestmentText)).setTextColor(0xFFFFFFFF);
			((TextView) tr.findViewById(R.id.textViewMyOptionsReturnText)).setTextColor(0xFFFFFFFF);
		} else {
			tr.setBackgroundColor(0xFFC8DEEB);
			tr.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_my_option_row_blue));
		}
	
		//define the arrow for call/put for regular investment and one touch
		if (inv.getTypeId() == Investment.INVESTMENT_TYPE_CALL ||
				(inv.getTypeId() == Investment.INVESTMENT_TYPE_ONE &&
				 inv.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_CALL	)) {
			tr.findViewById(R.id.imageViewOptionArrowCall).setVisibility(View.VISIBLE);
			tr.findViewById(R.id.imageViewOptionArrowPut).setVisibility(View.GONE);
		}
		if (inv.getTypeId() == Investment.INVESTMENT_TYPE_PUT ||
				(inv.getTypeId() == Investment.INVESTMENT_TYPE_ONE &&
	    		 inv.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_PUT	)) {
			tr.findViewById(R.id.imageViewOptionArrowPut).setVisibility(View.VISIBLE);
			tr.findViewById(R.id.imageViewOptionArrowCall).setVisibility(View.GONE);
		}
		
		//show option plus sign or the bonus sign
		ImageView sign = null;
		sign = (ImageView) tr.findViewById(R.id.imageViewSign);
		ImageView roll = (ImageView) tr.findViewById(R.id.imageViewRollOrPlus);
		int infoType = inv.getAdditionalInfoType();
		switch (infoType) {
			case Investment.INVESTMENT_ADDITIONAL_INFO_RU:
				roll.setImageResource(R.drawable.icon_rollforward_small);
				roll.setVisibility(View.VISIBLE);
				break;
			case Investment.INVESTMENT_ADDITIONAL_INFO_RU_BOUGHT:
			roll.setImageResource(R.drawable.icon_rollforward_small);
			roll.setVisibility(View.VISIBLE);
				break;
			case Investment.INVESTMENT_ADDITIONAL_INFO_GM:
				roll.setImageResource(R.drawable.icon_takeprofit_small);
				roll.setVisibility(View.VISIBLE);
				break;
			case Investment.INVESTMENT_ADDITIONAL_INFO_BONUS:
				sign.setImageResource(R.drawable.star_bonus);
				sign.setVisibility(View.VISIBLE);
				break;
			case Investment.INVESTMENT_ADDITIONAL_INFO_OPTION_PLUS:
				sign.setImageResource(R.drawable.icon_optionplus_small);
				sign.setVisibility(View.VISIBLE);
				break;
		}
		
		//click on investment row and make the popup
		tr.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				investmentPopup = tableRowClickPopUp((Investment) v.getTag(), isSettled, getContext(), inflater, new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        investmentPopup = null;
                    }
                });
			}
		});
		tlMain.addView(tr);
	}

	@Override
    public void updateItem(int itemPos, String itemName, UpdateInfo update) {
        try {
            long oppId = Long.parseLong(update.getNewValue("ET_OPP_ID"));
            TableRow tr = null;
            Investment inv = null;
            TextView ret = null;
            String levelStr = update.getNewValue(skinGroup.getLevelUpdateKey());
            double currentLevel = Double.parseDouble(levelStr.replaceAll(",", ""));
            for (int i = 0; i < tlMain.getChildCount(); i++) {
                tr = (TableRow) tlMain.getChildAt(i);
                inv = getInvestmentFromRow(tr);
                if (null != inv && inv.getOpportunityId() == oppId) {
                    ret = (TextView) tr.findViewById(R.id.textViewMyOptionsReturn);
                    Activity a = (Activity) getContext();
                    User user = ((AnyoptionApplication) a.getApplication()).getUser();
                    String returnFormatted = Utils.formatCurrencyAmount(Utils.getInvestmentReturnAt(inv, currentLevel), user.getCurrencySymbol(), user.isCurrencyLeftSymbol(), user.getCurrencyId());
                    responseH.post(new SetTextRunnable(ret, returnFormatted));
                    if (null != investmentPopup) {
                        try {
                            Investment popupInv = (Investment) investmentPopup.getTag();
                            if (inv == popupInv) {
                                responseH.post(new SetTextRunnable((TextView) investmentPopup.findViewById(R.id.textViewInvestmentCurrerntLevelNum), levelStr));
                                responseH.post(new SetTextRunnable((TextView) investmentPopup.findViewById(R.id.textViewInvestmentCurrerntReturnNum), returnFormatted));
                            }
                        } catch (Exception e) {
                            // do nothing. most likely NPE the popup can be closed at anytime.
                        }
                    }
                    
                    // store values to be used later if popup is opened for this row
                    inv.setCurrentLevelTxt(levelStr);
                    inv.setAmountReturnWF(returnFormatted);
        
                    double level = Double.parseDouble(inv.getLevel().replace(",", ""));
                    double param1 = 0;
                    double param2 = 0;
                    if (inv.getTypeId() == Investment.INVESTMENT_TYPE_CALL ||
            				(inv.getTypeId() == Investment.INVESTMENT_TYPE_ONE &&
            				 inv.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_CALL	)) {
            			param1 = currentLevel;
            			param2 = level; 
            		} 
            		if (inv.getTypeId() == Investment.INVESTMENT_TYPE_PUT ||
            				(inv.getTypeId() == Investment.INVESTMENT_TYPE_ONE &&
            	    		 inv.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_PUT	)) {
            			param1 = level;
            			param2 = currentLevel; 
            		}
                    if (param1 > param2) { // if the current level is bigger than the purcahsed level
                    	//blue xml 0xFFC8DEEB
                    	responseH.post(new SetBackgroundColorRunnable(tr, context.getResources().getDrawable(R.drawable.bg_my_option_row_blue)));
                    } else {
                    	//grey xml 0xFFD9D9D9
                    	responseH.post(new SetBackgroundColorRunnable(tr, context.getResources().getDrawable(R.drawable.bg_my_option_row_grey)));
                    }
                }
            }
        } catch (Exception e) {
            Log.e("chart", "Error processing updateItem.", e);
        }
    }
}
