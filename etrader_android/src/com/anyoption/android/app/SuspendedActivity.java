package com.anyoption.android.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;

public class SuspendedActivity extends BaseActivity {
	private Context mContext;
	private TextView suspendedText;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.suspended_documents_activity);
		mContext = this;
		AnyoptionApplication app = (AnyoptionApplication) getApplication();
		suspendedText = (TextView) findViewById(R.id.suspendedTextView);
		suspendedText.setMovementMethod(LinkMovementMethod.getInstance());
		CharSequence fromHtml = Html.fromHtml(getString(R.string.suspended_fordocuments_long, app.getUser().getFirstName(), getString(R.string.suspended_support_email)));
		ClickableSpan clickableSpan = new ClickableSpan() {
			
			@Override
			public void onClick(View widget) {
				Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
				emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{getString(R.string.suspended_support_email)});
				emailIntent.setType("plain/text");
				startActivity(Intent.createChooser(emailIntent, ""));
			
			}
		};
		
		
		SpannableStringBuilder clickableText = Utils.makeUnderlineClickable(fromHtml, clickableSpan, 0xFFF6CD10);
		suspendedText.setText(clickableText);
		
		Button buttonOk = (Button) findViewById(R.id.buttonOk);
		buttonOk.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Intent intent = new Intent(mContext, MarketsGroupActivity.class);
				intent.putExtra(Constants.PUT_EXTRA_SHOW_LOG_OUT_ALERT, true);
				mContext.startActivity(intent);
				finish();
			}
		});
	}
	@Override
	public void onBackPressed() {
		Utils.openTradingPage(this);
	}
}
