package com.anyoption.android.app;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Contact;
import com.anyoption.common.beans.base.Country;
import com.anyoption.json.requests.ContactMethodRequest;
import com.anyoption.json.results.ContactMethodResult;


public class CallMeActivity extends BaseActivity {
	
	private LayoutInflater inflater;
	private Spinner edspinnerCountry;
	private Handler responseH;
	private TextView name;
	private TextView email;
	private TextView mobilePhone;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_me);
        responseH = new Handler();
        inflater = getLayoutInflater();
        edspinnerCountry = (Spinner) findViewById(R.id.spinnerCountry);
        ArrayAdapter spinnerCountriesArrayAdapter = new ArrayAdapter(this,
        		android.R.layout.simple_spinner_item, ((AnyoptionApplication)getApplication()).getCountriesArray());  
        spinnerCountriesArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        edspinnerCountry.setAdapter(spinnerCountriesArrayAdapter);

        edspinnerCountry.setOnItemSelectedListener(
        		new AdapterView.OnItemSelectedListener() {
        			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {          	    	
        				ArrayAdapter<Country> adapter = (ArrayAdapter<Country>) parent.getAdapter();        	    		
        				Country c = adapter.getItem(pos);  
        				//Mobile phone prefix 
        				TextView addPhonePrefix = (TextView)findViewById(R.id.textViewMobilePhonePrefix);        	   	
        				addPhonePrefix.setText( "+" + c.getPhoneCode());
        			}
        			public void onNothingSelected(AdapterView<?> arg0) {												
        			}
        		}
        );                               

        name = (EditText) findViewById(R.id.editTextFirstname);
		email = (EditText) findViewById(R.id.editTextEmail);
		mobilePhone = (EditText) findViewById(R.id.editTextMobilePhone);
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
    }
           
    public void callMeButtonSubmit(View target){
    	//client validations
    	String error = null;
    	//first name
    	if (Utils.isEtraderProject(this)){
    		error = Utils.validateLettersOnlyAndEmpty(this, name.getText().toString(), getString(R.string.register_firstname));
    	} else {
    		error = Utils.validateEnglishLettersOnlyAndEmpty(this, name.getText().toString(), getString(R.string.register_firstname));
    	}
    	if (error!= null){
    		Utils.showAlert(this, "", error, this.getString(R.string.alert_title));  
    		return ;
    	}
    	//Email
    	error = Utils.validateEmailAndEmpty(this, email.getText().toString(), getString(R.string.register_email)); 
    	if (error!= null){
    		Utils.showAlert(this, "", error, this.getString(R.string.alert_title));  
    		return ;
    	}
    	//Mobile Phone
    	error = Utils.validateDigitsOnlyAndEmpty(this, mobilePhone.getText().toString(), getString(R.string.register_mobilephone));
    	if (error!= null){
    		Utils.showAlert(this, "", error, this.getString(R.string.alert_title));  
    		return ;
    	}
    	if (mobilePhone.getText().toString().length() < 7){
			String[] params = new String[2];
			params[0] = getString(R.string.register_mobilephone);
			params[1] = "7";    		
    		Utils.showAlert(this, "", String.format(this.getString(R.string.javax_faces_validator_LengthValidator_MINIMUM), params), 
    				 this.getString(R.string.alert_title));  
    		return ;
    	}
    	
    	if (error == null) {
    		ContactMethodRequest request = new ContactMethodRequest();
    		Contact contact = new Contact();
    		contact.setType(Contact.CONTACT_ME_TYPE);
    		contact.setName(name.getText().toString());
    		contact.setEmail(email.getText().toString());
    		contact.setCombId(Utils.getCombinationId(this));
    		contact.setDynamicParameter(Utils.getDynamicParam(this));
    		contact.setHttpReferer(Utils.getReferrerParam(this));
    		contact.setmId(Utils.getmId(this));
    		contact.setEtsMId(Utils.getEtsMId(this));
    		
    		Country country = (Country)edspinnerCountry.getAdapter().getItem(edspinnerCountry.getSelectedItemPosition());
    		contact.setCountryId(country.getId());
    		contact.setPhone(country.getPhoneCode() + mobilePhone.getText());
    		request.setContact(contact);
    		Utils.requestService(this, responseH, "callMeCallBack", request, new ContactMethodResult(), "insertContact");
    	}
    }
    
    public void callMeCallBack(Object result) {
		ContactMethodResult contanctResult = (ContactMethodResult) result;
		 if (contanctResult.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
			 String newmId = contanctResult.getContact().getmId();
			 Utils.setmIdIfValid(this, newmId);
			 
			 String newEtsMid = contanctResult.getContact().getEtsMId();
			 Utils.setEtsMId(this, newEtsMid);
			 
			 Utils.showAlert(this, contanctResult.getUserMessages()[0].getField(), 
					 contanctResult.getUserMessages()[0].getMessage(), this.getString(R.string.alert_title));
	     } else {
	    	 Log.d(this.getPackageName(), "Contanct Name: " + contanctResult.getContact().getName() + " Inserted");
	    	 LinearLayout popUp = (LinearLayout)inflater.inflate(R.layout.call_me_pop_up, null, false);
	    	 Button closeButton = (Button)popUp.findViewById(R.id.buttonOk);
	    	 Utils.showCustomAlert(this, popUp, closeButton, new OnDismissListener() {
				public void onDismiss(DialogInterface arg0) {
					finish();
				}
			});
	     }
    }
}