package com.anyoption.android.app;

public interface PageControlListener {
    public void showNext();
    public void showPrevious();
}