package com.anyoption.android.app;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.json.requests.InvestmentsMethodRequest;
import com.anyoption.json.results.InvestmentsMethodResult;
import com.lightstreamer.ls_client.ExtendedTableInfo;
import com.lightstreamer.ls_client.UpdateInfo;

/**
 * @author AviadH
 *
 */
public abstract class MyOptionView extends MenuViewBase implements ChartLightstreamerListener {
	
	public static final int NO_DATA_ROW = 1;
	
	protected boolean isSettled;
	protected Investment[] investments;
	protected boolean loadInvestments;
	protected MyOptionView myOption;
    protected LightstreamerConnectionHandler ls;
    protected ExtendedTableInfo lsTable;
    protected LinearLayout investmentPopup;
    protected Timer timer;
    private TextView groupName;
    
    private TableRow insurancesRow;
    private int insurancesType;
    private long insuranceCloseTime;
    private boolean isFirst;
    private int rowsNumber;
    private boolean requestFromTimerRefresh;
    
	//Date pick params
	protected int mYear; 
    protected int mMonth; 
    protected int mDay;
    
    private ScrollView menuScrollView;
    
    protected SkinGroup skinGroup;

	public MyOptionView(Context context) {
		this(context, null);
	}
	
	//constructor for create the component from runtime
	public MyOptionView(Context context, boolean isSettled, boolean loadInvestments, LightstreamerConnectionHandler ls) {
		super(context, null);
        this.isSettled = isSettled;
        this.loadInvestments = loadInvestments;
        this.ls = ls;
		initComponent(null);
	}
	
	//constructor for component that created in xml and attirbutes define in xml(custom attributes for custom component, can find in attr.xml) 
	public MyOptionView(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.myOptionView);  
		isSettled = attributes.getBoolean(R.styleable.myOptionView_isSettled, false);
		this.loadInvestments = true;
		initComponent(attrs);
	}
	
	public void initComponent(AttributeSet attrs) {
		Log.d("myOptionView", "Init component");
		myOption = this;  

		//set the tables column params
		if (Utils.isEtraderProject(context)) {
			tlMain.setColumnStretchable(5, false);
			tlMain.setColumnStretchable(4, true);
			tlMain.setColumnShrinkable(3, true);
			tlMain.setColumnShrinkable(1, true);
			tlMain.setColumnShrinkable(0, true);
		} else {
			tlMain.setColumnStretchable(0, false);
			tlMain.setColumnStretchable(1, true);
			tlMain.setColumnShrinkable(3, true);
			tlMain.setColumnShrinkable(4, true);
			tlMain.setColumnShrinkable(5, true);
		}
		groupName = (TextView) menuLine.findViewById(R.id.textViewGroupName);
		if (isSettled) {
			groupName.setText(R.string.assetIndex_group_investments_settled);
		} else {
			groupName.setText(R.string.header_investments);
		}
		if (context instanceof AssetPageActivity){
			groupName.setText(groupName.getText() + " " + context.getString(R.string.assetIndex_group_investments_settled_this_asset));
		}
		isFirst = true;
		if (!isSettled) {
			this.setId(R.id.myOptionView);
		}
		
		AnyoptionApplication application = (AnyoptionApplication) context.getApplicationContext();
        skinGroup = application.getSkins().get(application.getSkinId()).getSkinGroup();
	}

	public boolean isSettled() {
		return isSettled;
	}

	public void setSettled(boolean isSettled) {
		this.isSettled = isSettled;
	}
	
	@Override
	public void loadXRows() {
		InvestmentsMethodRequest request = new InvestmentsMethodRequest();
		request.setSettled(isSettled);
		if (isSettled) {
			// in case that we are on asset page we get only the market investments
    		if (context instanceof AssetPageActivity) {
    			request.setMarketId(((AssetPageActivity) context).getMarketId());
    		}
    		request.setFrom(fromCal);
    		request.setTo(toCal);
    		request.setStartRow(startRow);
			request.setPageSize(PAGE_SIZE);
		}
		Utils.requestService(this, responseH, "myOptionsCallBack", request, new InvestmentsMethodResult(), "getUserInvestments", (Activity) context);
	}
	
	public void myOptionsCallBack(Object result) {
		showLoadMore = false;
		InvestmentsMethodResult investmentsResult = (InvestmentsMethodResult) result;
		if (investmentsResult.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
			Utils.showAlert(context, investmentsResult.getUserMessages()[0].getField(), 
					investmentsResult.getUserMessages()[0].getMessage(), context.getString(R.string.alert_title_success));
			return;
		}
		Log.d("myOptionView", "myOptionsCallBack - num of investments: " + investmentsResult.getInvestments().length);
		unsubscribeLs();
		hideInsurancesRowInternal();
		if (requestFromTimerRefresh) {
			removeAllRows();
	       	startRow = 0;
	       	requestFromTimerRefresh = false;
		}
		if (investmentsResult.getInvestments().length == 0) {
//			if (!isSettled && isFirst) {
//				buttonMenuStrip.performClick();
//			}
			if (!isSettled && context instanceof MyAccountActivity) { // in case we dont have open investments
		    	makeNoDataRow();
			} else {
				makeNoInformationRow(getNoInformationString());
			}
		} else {
			setInvestments(investmentsResult.getInvestments());
			startRow += PAGE_SIZE;	
		}
        Activity a = (Activity) getContext();
        ((AnyoptionApplication) a.getApplication()).setUser(investmentsResult.getUser());
        ((HeaderLayout) a.findViewById(R.id.headerLayout)).refresh();
        startRefreshTimer();
		subscribeLs();
		if (a instanceof MyAccountActivity) {
			if (!isSettled) {
				TextView myOptions = (TextView) a.findViewById(R.id.textViewMyAccountMyOptions);
				if (null != myOptions) {
					myOptions.setText(investmentsResult.getSumAllInvAmount());
				}
			}
			TextView myBalance = (TextView) a.findViewById(R.id.textViewMyAccountBalance);
			if (null != myBalance) {
				myBalance.setText(investmentsResult.getUser().getBalanceWF());
			}
		}
		
		if (insurancesType != 0 && null == insurancesRow) {
            tlMain.post(new ShowInsuranceRunnable(insurancesType, insuranceCloseTime));
        }
		isFirst = false;
		menuScrollView.post(new ScrollViewScrollRunnable(menuScrollView, this));
	}
	
	public boolean isEmpty() {
		if (tlMain.getChildCount() > 0 &&
				null != tlMain.getChildAt(0).getTag() &&
				tlMain.getChildAt(0).getTag().equals(NO_DATA_ROW)) {
			return true;
		}
		return false;
	}

	public void subscribeLs() {
        if (null != ls && null != investments && investments.length > 0) {
            String items = null;
            for (int i = 0; i < investments.length; i++) {
                Log.v("MyOptionView", "market name: " + investments[i].getMarketName());
                if (!investments[i].getMarketName().endsWith(".plus")) {
                    if (null == items || items.indexOf(Long.toString(investments[i].getMarketId())) == -1) {
                        items = (null != items ? items + " " : "") + "aotps_1_" + investments[i].getMarketId();
                    }
                } else {
                    if (null == items || items.indexOf(Long.toString(investments[i].getMarketId())) == -1) {
                        items = (null != items ? items + " " : "") + "op_" + investments[i].getMarketId();
                    }
                }
            }
            Log.v("lightstreamer", "Going to subscribe for " + items);
            lsTable = Utils.addLightstreamerTable((Activity) getContext(), items.split(" "), ls, new AOTPSTableListener(this), 1.0);
        }
	}
	
	public void unsubscribeLs() {
        if (null != lsTable) {
            ls.removeTable(lsTable);
            lsTable = null;
        }
	}
	
	public void setInvestments(Investment[] invs) {
		//remove the last rows in load more option
		Log.e("start row " + startRow + "invs.kenth " +invs.length ," rowsremove " + rowsToRemoveInLoadMore);
		if (startRow > 0) {// if it is not the first time
			for (int i = 1 ; i <= rowsToRemoveInLoadMore ; i++) {
				tlMain.removeViewAt(tlMain.getChildCount()-1);
			}
		}
		if (invs.length >= PAGE_SIZE) {
			showLoadMore = true;
		}
		hideInsurancesRowInternal();
		//removeAllInvestments();
	    if (fromCal != null && toCal != null && startRow == 0) {
	    	makeFirstDatesRow(fromCal, toCal);
	    }
	    
	    investments = invs;
    	for (int i = 0 ; i < invs.length ; i++) {
    		makeRowForInvestment(investments[i]);
	    }
    	if (isSettled) {
	    	makeLastRow(getResources().getString(R.string.menuStrip_myOptions_settled), 
	    			getResources().getString(R.string.investments_close));
    	}
        if (insurancesType != 0 && null == insurancesRow) {
            tlMain.post(new ShowInsuranceRunnable(insurancesType, insuranceCloseTime));
        }
	}
	
	public void checkIfCloseAndOpen() {
		if (tlMain.getVisibility() == View.GONE) {
			buttonMenuStrip.performClick();
		}
	}
	
	public void makeNoDataRow() {
		if (tlMain.getChildCount() == 0) {
	    	TableRow tr = (TableRow) inflater.inflate(R.layout.first_dates_row, tlMain, false);
	    	tr.findViewById(R.id.linearLayoutNoData).setVisibility(View.VISIBLE);
			TextView tvData = (TextView) tr.findViewById(R.id.textViewFromAndToDates);
			tvData.setText(getResources().getString(R.string.menuStrip_myOptions_no_data));
			((TextView) tr.findViewById(R.id.textViewStartTrade)).setTextColor(0xFFFFFFFF);
			tvData.setTextColor(0xFFFFFFFF);
			
			tr.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					((Activity) context).startActivity(new Intent(context, MarketsGroupActivity.class)); 
				}
			});
			tlMain.addView(tr);
		}
	}
	
	public void makeRowForInvestment(final Investment inv) {
		if (tlMain.getChildCount() > 0 && !isSettled) {
			if (null != tlMain.getChildAt(0).getTag() && tlMain.getChildAt(0).getTag().equals(NO_DATA_ROW)) {
				removeAllRows();
			}
		}
		if (!isSettled) {
			rowsNumber++;
			addNumberOfInvestmets(rowsNumber);
		}
	}
	
	public void addNumberOfInvestmets(int investmentsNumber) {
		if (!isSettled) {
			if (investmentsNumber > 0) {
				groupName.setText(getResources().getString(R.string.header_investments) + " (" + investmentsNumber + ")");
			} else {
				groupName.setText(getResources().getString(R.string.header_investments));
			}
			if (context instanceof AssetPageActivity){
				groupName.setText(groupName.getText() + " " + context.getString(R.string.assetIndex_group_investments_settled_this_asset));
			}
		}
	}
	
	public static LinearLayout tableRowClickPopUp(final Investment inv, boolean isSettled, final Context context, LayoutInflater inflater, DialogInterface.OnDismissListener dismissListener) {
		LinearLayout llMain = null;
		LinearLayout llMainTitle = null;
		if (isSettled) {
			llMain = (LinearLayout) inflater.inflate(R.layout.settled_investment_details, null);
		} else {
			llMain = (LinearLayout) inflater.inflate(R.layout.investment_details, null);
			Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
	        int orientation = display.getOrientation();
	        if (orientation == Surface.ROTATION_90 || orientation == Surface.ROTATION_270) {
	        	try {
	        		llMainTitle = (LinearLayout) inflater.inflate(R.layout.investment_details_title, null);
	        	} catch (Exception e) {
					Log.d("MyOptionView", "Cant get R.layout.investment_details_title !!", e);
				}
	        }
		}
		
		
		
		TextView marketName = (TextView) llMain.findViewById(R.id.textViewMarketName);
		if (null != llMainTitle) {
			marketName = (TextView) llMainTitle.findViewById(R.id.textViewMarketName);
		}
		TextView level = (TextView) llMain.findViewById(R.id.textViewInvestmentTypeandLevel);
		TextView purchased = (TextView) llMain.findViewById(R.id.textViewInvestmentPurchasedTime);
		TextView expiry = (TextView) llMain.findViewById(R.id.textViewInvestmentExpireTime);
		TextView investment = (TextView) llMain.findViewById(R.id.textViewInvestmentAmount);
		TextView currenLevel = (TextView) llMain.findViewById(R.id.textViewInvestmentCurrerntLevelNum);
		TextView currentRetrun = (TextView) llMain.findViewById(R.id.textViewInvestmentCurrerntReturnNum);
				
		TableRow tr = (TableRow) llMain.findViewById(R.id.tableRowAdditionalInfo);
		tr.setVisibility(View.GONE);
		
		if (inv.getAdditionalInfoType() != 0) {			
			tr.setVisibility(View.VISIBLE);
			ImageView iv = (ImageView) tr.findViewById(R.id.imageViewAdditionalImage);
			TextView tv = (TextView) tr.findViewById(R.id.textViewAdditionalText);
			tv.setText(inv.getAdditionalInfoText());
			int infoType = inv.getAdditionalInfoType();
			switch (infoType) {
				case Investment.INVESTMENT_ADDITIONAL_INFO_RU:
					iv.setImageResource(R.drawable.icon_rollforward_small);
					break;
				case Investment.INVESTMENT_ADDITIONAL_INFO_RU_BOUGHT:
					iv.setImageResource(R.drawable.icon_rollforward_small);
					break;
				case Investment.INVESTMENT_ADDITIONAL_INFO_GM:
					iv.setImageResource(R.drawable.icon_takeprofit_small);
					break;
				case Investment.INVESTMENT_ADDITIONAL_INFO_BONUS:
					iv.setImageResource(R.drawable.star_bonus);
					break;
				case Investment.INVESTMENT_ADDITIONAL_INFO_OPTION_PLUS:
					iv.setImageResource(R.drawable.list_optionplus_small);
					break;
				case Investment.INVESTMENT_ADDITIONAL_INFO_NOT_HOURLY:
					iv.setVisibility(GONE);
					break;
			}
		}
		
		if (inv.getOptionPlusFee() > 0){
			if (null != llMainTitle) {
				llMainTitle.findViewById(R.id.imageViewPlusBig).setVisibility(View.VISIBLE);
				llMain.findViewById(R.id.tablerowMarketName).setVisibility(View.GONE);
			} else {
				llMain.findViewById(R.id.imageViewPlusBig).setVisibility(View.VISIBLE);
			}
		}
		
		marketName.setText(inv.getAsset());
		if (isSettled) {
			TextView optionId = (TextView) llMain.findViewById(R.id.textViewInvestmentOptionId);
			optionId.setText("#" + inv.getId());
		}
					
		String type = "";
		if (inv.getTypeId() == Investment.INVESTMENT_TYPE_CALL ||
				(inv.getTypeId() == Investment.INVESTMENT_TYPE_ONE &&
				 inv.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_CALL	)) {
			type = context.getString(R.string.investments_call);
		}
		if (inv.getTypeId() == Investment.INVESTMENT_TYPE_PUT ||
				(inv.getTypeId() == Investment.INVESTMENT_TYPE_ONE &&
	    		 inv.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_PUT	)) {
			type = context.getString(R.string.investments_put);
		}
		level.setText(type + " " + inv.getLevel());
		purchased.setText(inv.getTimePurchaseTxt()); 
		expiry.setText(inv.getTimeEstClosingTxt());
		investment.setText(inv.getAmountTxt());
		
		
		if (isSettled) {
			currenLevel.setText(inv.getExpiryLevel());
		} else {
			currenLevel.setText(inv.getCurrentLevelTxt());
		}
		
		currentRetrun.setText(inv.getAmountReturnWF());
		
		if (inv.getAdditionalInfoType() == Investment.INVESTMENT_ADDITIONAL_INFO_NOT_HOURLY) {
			currenLevel.setVisibility(GONE);
			currentRetrun.setVisibility(GONE);
			llMain.findViewById(R.id.textViewInvestmentCurrerntLevel).setVisibility(GONE);
			llMain.findViewById(R.id.textViewInvestmentCurrerntReturn).setVisibility(GONE);
		}
		
		final Button closeButton;
		if (isSettled) {
			closeButton	= (Button) llMain.findViewById(R.id.buttonSettledInvestmentPopUpClose);
			if (context instanceof MyAccountActivity || context instanceof MarketsGroupActivity) {
	        	Button goToAsset = (Button) llMain.findViewById(R.id.buttonSettledInvestmentPopUpShare);
	        	goToAsset.setVisibility(View.VISIBLE);
	        	goToAsset.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						Market market = new Market();
						market.setId(inv.getMarketId());
						closeButton.performClick();
						Utils.gotToAssetPage((Activity)context, market);
					}
				});
			}
		} else {
			closeButton	= (Button) llMain.findViewById(R.id.buttonInvestmentPopUp);
			if (context instanceof MyAccountActivity || context instanceof MarketsGroupActivity) {
		        	Button goToAsset = (Button) llMain.findViewById(R.id.buttonInvestmentPopUpGoToAsset);
		        	goToAsset.setVisibility(View.VISIBLE);
		        	if (inv.getMarketId() == Constants.MARKET_BITCOIN_ID) {
		        		goToAsset.setVisibility(View.GONE);
		        	}   	
		        	goToAsset.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							Market market = new Market();
							market.setId(inv.getMarketId());
							closeButton.performClick();
							Utils.gotToAssetPage((Activity)context, market);
						}
					});
			}
		}
		llMain.setTag(inv);
		
		Utils.showCustomAlert(context, llMain, closeButton, dismissListener, llMainTitle);
		return llMain;
	}
	
	@Override
    public void openGroup() {
		super.openGroup();
    	if (loadInvestments) {
    		//remove all rows and keep the visual number
    		int rowsNumForTempShow = rowsNumber;
    		removeAllRows();
    		addNumberOfInvestmets(rowsNumForTempShow);
    		
    		restartLoadMore();
    		loadXRows();
		} else if (insurancesType != 0 && null == insurancesRow) {
            tlMain.post(new ShowInsuranceRunnable(insurancesType, insuranceCloseTime));
        }
    }
	
    @Override
    public void closeGroup() {
        hideInsurancesRowInternal();
    	super.closeGroup();
     	if (null != ls && null != lsTable) {
     	    ls.removeTable(lsTable);
     	}
     	if (isSettled) {
     	    stopRefreshTimer();
     	}
	}
    
    @Override
    public String getNoInformationString() {
    	return getResources().getString(R.string.investments_emptylist);
	}

    public abstract void updateItem(int itemPos, String itemName, UpdateInfo update);

    public void removeInvestment(long invId) {
        TableRow tr = null;
        Investment inv = null;
        for (int i = 0; i < tlMain.getChildCount(); i++) {
            tr = (TableRow) tlMain.getChildAt(i);
            inv = (Investment) tr.getTag();
            if (inv.getId() == invId) {
                tlMain.removeViewAt(i);
                rowsNumber--;
    			addNumberOfInvestmets(rowsNumber);
                return;
            }
        }
        makeNoInformationRow(getNoInformationString());
    }
    
    @Override
	public void removeAllRows() {
    	Log.d("removeAllIvestments", "Remove rows and init myOption (x) TYPE: IS_SETTLED = " + isSettled);
        tlMain.removeAllViews();
        rowsNumber = 0;
        addNumberOfInvestmets(rowsNumber);
    }
    
    public LightstreamerConnectionHandler getLs() {
        return ls;
    }

    public void setLs(LightstreamerConnectionHandler ls) {
        this.ls = ls;
    }
    
    private void showInsurancesRowInternal() {
        if (tlMain.getVisibility() == VISIBLE && null == insurancesRow) {
            tlMain.post(new ShowInsuranceRunnable(insurancesType, insuranceCloseTime));
        }
    }
    
    public void showInsurancesRow(int insurancesType, long insuranceCloseTime) {
	    this.insurancesType = insurancesType;
	    this.insuranceCloseTime = insuranceCloseTime;
	    showInsurancesRowInternal();
	}
	
    private void hideInsurancesRowInternal() {
        if (null != insurancesRow) {
            InsuranceView iv = (InsuranceView) insurancesRow.findViewById(R.id.myOptionsInsuranceView);
            iv.stopTimeLeftUpdater();
            tlMain.removeView(insurancesRow);
            insurancesRow = null;
        }
    }
    
	public void hideInsurancesRow() {
	    hideInsurancesRowInternal();
        insurancesType = 0;
        insuranceCloseTime = 0;
	}
	
	private void stopRefreshTimer() {
        if (null != timer) {
            Log.v("anyoption", "Stop My Options refresh timer.");
            timer.cancel();
            timer = null;
        }
	}
	
	private void startRefreshTimer() {
	    stopRefreshTimer();
	    Log.v("anyoption", "Start My Options refresh timer.");
        timer = new Timer();
        timer.schedule(new RefreshMyOptionViewRunnable(this), Utils.getNextRefreshTime());
	}
	
	public void onResume() {
	    startRefreshTimer();
	}
	
	public void onPause() {
	    stopRefreshTimer();
	}
	
	class ShowInsuranceRunnable implements Runnable {
        private int insurancesType;
        private long insuranceCloseTime;
        
        public ShowInsuranceRunnable(int insurancesType, long insuranceCloseTime) {
            this.insurancesType = insurancesType;
            this.insuranceCloseTime = insuranceCloseTime;
        }
        
        public void run() {
            if (null == insurancesRow) {
                insurancesRow = (TableRow) inflater.inflate(R.layout.my_option_row_insurance, tlMain, false);
                TableRow ftr = (TableRow) tlMain.getChildAt(0);
                int colCount = null != ftr ? ftr.getChildCount() : 1;
                Log.v("anyoption", "ShowInsuranceRunnable - colCount: " + colCount);
                InsuranceView iv = (InsuranceView) insurancesRow.findViewById(R.id.myOptionsInsuranceView);
                TableRow.LayoutParams trlp = (TableRow.LayoutParams) iv.getLayoutParams();
                trlp.span = colCount;
                iv.initAndStart(insurancesType, insuranceCloseTime);
                iv.setVisibility(View.VISIBLE);
                tlMain.addView(insurancesRow, 0);
            }
        }
    }

    class RefreshMyOptionViewRunnable extends TimerTask {
        private Object resultReceiver;
        
        public RefreshMyOptionViewRunnable(Object resultReceiver) {
            this.resultReceiver = resultReceiver;
        }
        
        @Override
		public void run() {
        	requestFromTimerRefresh = true;
        	loadXRows();
       }
    }
    
    public Investment getInvestmentFromRow(TableRow tr) {
    	Object o = tr.getTag();
    	if (null != o && o instanceof Investment) {
    		return (Investment) o;
    	}
    	return null;
    }

	public int getRowsNumber() {
		return rowsNumber;
	}

	public void setRowsNumber(int rowsNumber) {
		this.rowsNumber = rowsNumber;
	}

	public void setFirst(boolean isFirst) {
		this.isFirst = isFirst;
	}

	public void setMenuScrollView(ScrollView menuScrollView) {
		this.menuScrollView = menuScrollView;
	}

	public ScrollView getMenuScrollView() {
		return menuScrollView;
	}   
}