package com.anyoption.android.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Notification service
 * 
 * @author Kobi
 *
 */
public class NotificationService extends Service {	
	private NotificationManager mNotificationManager; 	
    private static final int NOTIFICATION_ID = 1;
	
	@Override
	public void onCreate() {
		super.onCreate();
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);		
		startService();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mNotificationManager.cancel(NOTIFICATION_ID);
		Log.v("Service", "stopped");		
	}
	
    @Override    
    public IBinder onBind(Intent intent) {        
    	return null;    
    }
    
	private void startService() {		
        Notification notification = new Notification(R.drawable.notification_icon, getString(R.string.scroller_binary_options_trading_header)
        		, System.currentTimeMillis());        
        
        Context context = getApplicationContext();
        CharSequence contentTitle = getString(R.string.notifAppName);
        CharSequence contentText = getString(R.string.scroller_binary_options_trading_header);
        Intent notificationIntent = new Intent(this, DispatcherActivity.class);
        
        
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);   
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notification.flags |= Notification.FLAG_NO_CLEAR;
        
       
        mNotificationManager.notify(NOTIFICATION_ID, notification);
		         
		Log.i("Notification service", "started");
	}
	

}
