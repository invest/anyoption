package com.anyoption.android.app;

import java.util.Arrays;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.UserMethodRequest;
import com.anyoption.json.results.TransactionMethodResult;

public class PaymentMethodPopupActivity extends BankWireDepositActivity {
	private final static String DEPOSIT = ".deposit";
	private final static String WITHDRAW = ".withdraw";
	private final static String LOGOUT = ".logout";
	
	private User user;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) { 
       super.onCreate(savedInstanceState);
       AnyoptionApplication ap = (AnyoptionApplication) getApplication();
       LayoutInflater inflater = getLayoutInflater();
       user = ap.getUser(); 
       PaymentMethod payment = (PaymentMethod) getIntent().getExtras().get(Constants.PUT_EXTRA_BANKING_PAYMENT);
       if (null != payment) { 
    	   String contentDepositText;    	   
    	   if (Utils.isEtraderProject(this) && user == null) {
    		   contentDepositText = Utils.getStringFromResource(this, payment.getDisplayName() + DEPOSIT + LOGOUT, null);
    	   } else {
    		   contentDepositText = Utils.getStringFromResource(this, payment.getDisplayName() + DEPOSIT, null);
    	   }
    	   String contentWithdrawText = Utils.getStringFromResource(this, payment.getDisplayName() + WITHDRAW, null);
    	   //Display only deposit/withdraw text on login
    	   if (user != null && (payment.getId() == PaymentMethod.PAYMENT_TYPE_CC || payment.getId() == PaymentMethod.PAYMENT_TYPE_WIRE || payment.getDisplayName().equals("payment.type.baropay"))) {  
    		   if (payment.getTypeId() == Constants.PAYMENT_TYPE_DEPOSIT) {
    			   contentWithdrawText = null;
    		   } else {
    			   contentDepositText = null;  
    		   }  
    	   } 
    	   int imageId = 0;
    	   if (null != payment.getBigLogo()) {
    		   if(Utils.isRegulated(this) && payment.getBigLogo().contains("credit")){
    			   imageId = Utils.getImageFromResource(this, payment.getBigLogo() + "_regulated");
    		   } else {
    			   imageId = Utils.getImageFromResource(this, payment.getBigLogo());
    		   }
    	   }
    	   if (payment.getId() == PaymentMethod.PAYMENT_TYPE_WIRE) {
    		  int bankingInfoLayoutId;
    		  boolean setPramas = true;
    		  if (user == null) {
    			  //when user logged in we want to display withdraw/deposit text
    			  bankingInfoLayoutId = R.layout.bank_wire_logout_popup;
    			  setContentView(bankingInfoLayoutId);
    			  
    			  if(Utils.isRegulated(this) || isNotRegulatedEUSkin()){
    				  findViewById(R.id.bankDetails).setVisibility(View.GONE);
    			  }
    			  long defaultCountryId = ap.getSkins().get(ap.getSkinId()).getDefaultCountryId();
    			  String fax = ap.getCountries().get(defaultCountryId).getSupportFax();
    			  TextView basicsText = (TextView)findViewById(R.id.bankWireLogoutPopupBasicsText);
    			  Spanned text = Html.fromHtml(getString(R.string.bank_wire_popup_basics_to_make, fax, getString(R.string.suspended_support_email)));
    			  
    			  SpannableStringBuilder clickableText = Utils.makeUnderlineClickable(text, new ClickableSpan() {
					
					@Override
					public void onClick(View widget) {
						Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
						emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{getString(R.string.suspended_support_email)});
						emailIntent.setType("plain/text");
						startActivity(Intent.createChooser(emailIntent, ""));
						
					}
				}, 0xFFF6CD10);
    			  basicsText.setText(clickableText);
    			  basicsText.setMovementMethod(LinkMovementMethod.getInstance());
    			  
    			  if (!Utils.isEtraderProject(this)) {
						long skinId = ap.getSkinId();
						// Display currencies depending on the skin.
						if (skinId == Skin.SKIN_CHINESE || skinId == Skin.SKIN_CHINESE_VIP) {
							setCnyVisibility(View.VISIBLE, true);
							showUSDBankDetails(findViewById(R.id.LinearLayoutCNY));
						} else if (skinId == Skin.SKIN_TURKISH) {
							setTryVisibility(View.VISIBLE);
							setCnyVisibility(View.VISIBLE, false);
							showUSDBankDetails(findViewById(R.id.LinearLayoutTRY));
						} else if (skinId == Skin.SKIN_RUSSIAN) {
							setRubVisibility(View.VISIBLE);
							setCnyVisibility(View.VISIBLE, false);
							showUSDBankDetails(findViewById(R.id.LinearLayoutRUB));
						} else if (skinId == Skin.SKIN_KOREAN) {
							setKrwVisibility(View.VISIBLE);
							showUSDBankDetails(findViewById(R.id.LinearLayoutKRW));
						}
    			  }    
    			  
    		  } else {    			  
    			  bankingInfoLayoutId = R.layout.deposit_bank_wire_popup;
    			  if (payment.getTypeId() == Constants.PAYMENT_TYPE_WITHDRAW) {
    				  bankingInfoLayoutId = R.layout.bank_wire_withdraw_popup;
    				  setPramas = false;
    			  }
    			  setContentView(bankingInfoLayoutId);     			  
    		  } 
    		  if (setPramas){
    			  setParamsGeneral();
    		  }
    		  if (bankingInfoLayoutId == R.layout.bank_wire_logout_popup || bankingInfoLayoutId == R.layout.deposit_bank_wire_popup) {
    			  getMinFirstDeposit();
    		  }
    	   } else if (payment.getId() == PaymentMethod.PAYMENT_TYPE_ENVOY) {
    		   if (Utils.isEtraderProject(this)) {
    			   LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.cash_logout_popup, null, false);
    			   setContentView(layout);
    		   } else {
    			   ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.payment_method_popup_logout, null, false);
	    		   layout.findViewById(R.id.linearLayoutDeposit).setVisibility(View.VISIBLE);
	    		   ((TextView) layout.findViewById(R.id.textViewDepositContent)).setText(contentDepositText);
	    		   //Instead of image we display text
	    		   String displayName = Utils.getStringFromResource(this, "header_envoy_text", null);
	    		   TextView tv = ((TextView) layout.findViewById(R.id.textViewPaymentName));
	    		   tv.setText(displayName.toUpperCase());
	    		   tv.setVisibility(View.VISIBLE);
	    		   setContentView(layout);
    		   }
    	   } else {	    
	    	   ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.payment_method_popup_logout, null, false);
	    	   if (null != contentDepositText) {
	    		   layout.findViewById(R.id.linearLayoutDeposit).setVisibility(View.VISIBLE);
	    		   ((TextView) layout.findViewById(R.id.textViewDepositContent)).setText(Html.fromHtml(contentDepositText));
	    	   }
	    	   if (null != contentWithdrawText) {
	    		   layout.findViewById(R.id.linearLayoutWithdrawal).setVisibility(View.VISIBLE);
	    		   ((TextView) layout.findViewById(R.id.textViewWithdrawContent)).setText(Html.fromHtml(contentWithdrawText));
	    	   }
	    	   if (imageId > 0) {
	    		   ((ImageView) layout.findViewById(R.id.paymentLogo)).setImageResource(imageId);
	    	   } else {
	    		   String displayName = Utils.getStringFromResource(this, payment.getDisplayName(), null);
	    		   TextView tv = ((TextView) layout.findViewById(R.id.textViewPaymentName));
	    		   tv.setText(displayName.toUpperCase());
	    		   tv.setVisibility(View.VISIBLE);
	    	   }
	    	   
	    	   setContentView(layout);
    	   }
       }
   }
    
    private boolean isNotRegulatedEUSkin() {
    	AnyoptionApplication app = (AnyoptionApplication) getApplication();
    	Long[] skins = { Skin.SKIN_FRANCE, Skin.SKIN_ITALIAN, Skin.SKIN_ENGLISH, Skin.SKIN_SPAIN, Skin.SKIN_GERMAN};
    	List<Long> skins2 = Arrays.asList(skins);
    	return (skins2.contains(app.getSkinId()));
	}

	/**
	 * get minimum first deposit
	 */
    protected void getMinFirstDeposit() {
        Utils.requestService(this, responseH, "getMinFirstDepositCallBack", new UserMethodRequest(), new TransactionMethodResult(), "getMinFirstDeposit");
    }
	
    /**
	 * get minimum first deposit CallBack
	 */
	public void getMinFirstDepositCallBack(Object resultObj) {
		Log.d(getLocalClassName(), "getMinFirstDepositCallBack");
		TransactionMethodResult result = (TransactionMethodResult) resultObj;
		TextView textView = ((TextView)	findViewById(R.id.textViewBankWireMinimum));
		if (textView != null) {
			textView.setText(getResources().getString(R.string.bank_wire_popup_minimum, result.getFormattedAmount()));
		}
	}

	/**
	 * Change the visibility of CNY currency.
	 * 
	 * @param visibility one of {@link View#VISIBLE}, {@link View#INVISIBLE} or {@link View#GONE}.
	 * @param startPosition true if the currency has to be the first currency, else the currency is at the end.
	 */
	private void setCnyVisibility(int visibility, boolean startPosition) {
		View cnyLayout = null;
		if(startPosition) {
			cnyLayout = findViewById(R.id.LinearLayoutCNY);
		} else {
			cnyLayout = findViewById(R.id.LinearLayoutCNYEnd);
		}
		cnyLayout.setVisibility(visibility);
	}
	
	/**
	 * Change the visibility of TRY currency.
	 * 
	 * @param visibility one of {@link View#VISIBLE}, {@link View#INVISIBLE} or {@link View#GONE}.
	 */
	private void setTryVisibility(int visibility) {
		View tryLayout = findViewById(R.id.LinearLayoutTRY);
		tryLayout.setVisibility(visibility);
	}
	
	/**
	 * Change the visibility of RUB currency.
	 * 
	 * @param visibility one of {@link View#VISIBLE}, {@link View#INVISIBLE} or {@link View#GONE}.
	 */
	private void setRubVisibility(int visibility) {
		View rubLayout = findViewById(R.id.LinearLayoutRUB);
		rubLayout.setVisibility(visibility);
	}
	
	/**
	 * Change the visibility of KRW currency.
	 * 
	 * @param visibility one of {@link View#VISIBLE}, {@link View#INVISIBLE} or {@link View#GONE}.
	 */
	private void setKrwVisibility(int visibility) {
		View krwLayout = findViewById(R.id.LinearLayoutKRW);
		krwLayout.setVisibility(visibility);
	}
	
}