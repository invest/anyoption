package com.anyoption.android.app;



import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.Skin;

/**
 * @author IdanZ
 *
 */
public class ChangeLanguageView extends MenuViewBase {
	
	SharedPreferences settings = null;
	private int lang = 0;
	String langCode = null;
	String[] splittedArray;

	
	public ChangeLanguageView(Context context) {
		this(context, null);
	}
	
	public ChangeLanguageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initComponent(attrs);
	}
	
	public void initComponent(AttributeSet attrs) {
		Log.d("ChangeLanguageView", "init Component");

		//set table params
		tlMain.setColumnStretchable(0, true);
		tlMain.setColumnShrinkable(1, true);
		
		TableLayout changeLang = (TableLayout) inflater.inflate(R.layout.change_language_other_menu, this, false);	
		settings = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        langCode = settings.getString(Constants.PREF_LOCALE, null);
        long skinId = ((AnyoptionApplication)((Activity) context).getApplication()).getSkinId();
        View langList = null;
        if (skinId == Skin.SKIN_EN_US || skinId == Skin.SKIN_ES_US) {
            langList = changeLang.findViewById(R.id.linearLayoutUS);
        } else {
            langList = changeLang.findViewById(R.id.linearLayout);
        }
        langList.setVisibility(TextView.VISIBLE);
        //Default language will be English
        TextView tv = (TextView) langList.findViewById(R.id.textEnglish);        
        tv.setBackgroundColor(0x60ffffff);
        lang = tv.getId();
        langCode = (String) tv.getTag();
    
		tlMain.addView(changeLang);

        //Listener for language rows
        for (int i = 0 ; i < ((LinearLayout) langList).getChildCount() ; i++) {
        	(((LinearLayout) langList).getChildAt(i)).setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					changeBg(v);
					validateChangeLanguage();
				}
			});
        }
	}
	
	public String getMenuStripText() {
		return getResources().getString(R.string.mobile_select_language_title);
	}
	
	public void changeBg(View target) { 
    	if (lang == 0) {
    		target.setBackgroundColor(0x60ffffff);
    		lang = target.getId();    
    		langCode = (String) target.getTag();
    	} else if (lang != 0 && target.getId() != lang) {  
    	    TextView tv = (TextView) findViewById(lang);
    	    tv.setBackgroundColor(0x50e1e1e1);
    	    target.setBackgroundColor(0x60ffffff);
    	    lang = target.getId();
    	    langCode = (String) target.getTag();
    	} 
    }
	
	public void validateChangeLanguage(){		
		String languageSelected = null;
    	LinearLayout logoutAlert = (LinearLayout) inflater.inflate(R.layout.call_us_pop_up, null, false);
    	final TextView title = (TextView) logoutAlert.findViewById(R.id.textViewTitle);
		
		if (langCode == null) { // if the user didn't choose a language
    		Utils.showAlert(context, "", getResources().getString(R.string.error_choose_language), context.getString(R.string.alert_title));
    	} else {
    	    splittedArray = langCode.split("_");
    	    int skinId = Integer.parseInt(splittedArray[1]);
    	    switch (skinId){
	    	    case 2: case 13: 
	    	    	languageSelected = context.getString(R.string.languages_eng);
	            	break;
	    	    case 3:  languageSelected = context.getString(R.string.languages_tur);
            		break;
	    	    case 5:  case 1:
	    	    	languageSelected = context.getString(R.string.languages_spa);
        			break;
	    	    case 8:  languageSelected = context.getString(R.string.languages_de);
        			break;
	    	    case 9:  languageSelected = context.getString(R.string.languages_ita);
        			break;
	    	    case 10:  languageSelected = context.getString(R.string.languages_rus);
        			break;
	    	    case 12:  languageSelected = context.getString(R.string.languages_fr);
        			break;
	    	    case 15: languageSelected = context.getString(R.string.languages_zh);
	    	    	break;
                case 17: languageSelected = context.getString(R.string.languages_kr);
                break;
    	    }    	
	    	title.setText(context.getString(R.string.change_language_msg, languageSelected));
	    	((TextView) logoutAlert.findViewById(R.id.textViewSupportPhone)).setVisibility(View.GONE);
			Button cancelDialogButton = (Button) logoutAlert.findViewById(R.id.buttonMyAccountPersonalDetailsPopupClose);
			cancelDialogButton.setText(R.string.button_cancel_en);
			Button submitDialogButton = (Button) logoutAlert.findViewById(R.id.buttonCallUsPopupSubmit);
			submitDialogButton.setText(R.string.button_submit_en);
			final AlertDialog ad = Utils.showCustomAlert(context, logoutAlert, cancelDialogButton);
			submitDialogButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						openHomePage();
						ad.dismiss();
					}
			});
    	}
	}
	
	public void openHomePage() {
    	Log.d("!!!!!!!!!", "!!!!!!!!!!!!!!!!!!!!!!open page!!!!!!!!!!!!!!!!!!!!!!!");
    	AnyoptionApplication app = (AnyoptionApplication) ((Activity) context).getApplication();
    		app.changeLocale(splittedArray[0]);
    		long skinId = Long.parseLong(splittedArray[1]);
    		if(Utils.isRegulated(context)){
    			long regulatedSkinId = Utils.getAlternativeRegulatedSkin(context, skinId);
    			if(regulatedSkinId != -1){
    				skinId = regulatedSkinId;
    			}
    		}
    		app.setSkinId(skinId);
			Intent intent = new Intent(context, Home_pageActivity.class);
			intent.putExtra(Constants.PUT_EXTRA_UPDATE_SKIN, "ture");
			((Activity) context).startActivityForResult(intent, Constants.REQUEST_CODE_HOMEPAGE);
    	}

	@Override
	public void removeAllRows() {
		// TODO Auto-generated method stub
	}   
}