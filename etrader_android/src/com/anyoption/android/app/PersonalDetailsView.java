/**
 * 
 */
package com.anyoption.android.app;

import java.text.SimpleDateFormat;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.UserMethodRequest;
import com.anyoption.json.results.UserMethodResult;

/**
 * @author IdanZ
 *
 */
public class PersonalDetailsView extends MenuViewBase implements Initializable{
	private AnyoptionApplication ap;
	private EditText passwordET;
	private boolean shouldOpen;
	private Button cancelDialogButton;
	private boolean isOpened;
	
	public PersonalDetailsView(Context context) {
		this(context, null);
	}
	
	public PersonalDetailsView(final Context context, AttributeSet attrs) {
		super(context, attrs);
		ap = (AnyoptionApplication) context.getApplicationContext();
		
		tlMain = (TableLayout) inflater.inflate(R.layout.personal_details, this, false);
		tlMain.getChildAt(0).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Class<Activity> actvityRedirect = Utils.getActivityClass(Constants.ACTIVITY_REDIRECT_CHANGE_PERSONAL, context);
				 ((Activity) context).startActivityForResult(new Intent(context, actvityRedirect).setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY), 
						 	Constants.REQUEST_CODE_CHANGE_PERSONAL_DETAILS);
			}
		});
		
		tlMain.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				openGroup();
			}
		});
		menuLine.findViewById(R.id.tableLayoutGroup).setVisibility(View.GONE);
		menuLine.addView(tlMain);
		tlMain.setVisibility(View.GONE);
		initMyAccountPersonalDetails();
	}
	
	public String getMenuStripText() {
		return getResources().getString(R.string.header_personal);
	}
	
	private void validatePassword() {
		LinearLayout checkPass = (LinearLayout) inflater.inflate(R.layout.my_account_edit_field_box, this, false);
		passwordET = (EditText)checkPass.findViewById(R.id.editTextMyAccount);
		cancelDialogButton = (Button) checkPass.findViewById(R.id.buttonMyAccountPersonalDetailsPopupClose);
		Button submitDialogButton = (Button) checkPass.findViewById(R.id.buttonMyAccountPersonalDetailsPopupSubmit);
		Utils.showCustomAlert(context, checkPass, cancelDialogButton, new OnDismissListener() {
			public void onDismiss(DialogInterface arg0) {
				if (!shouldOpen) {
					manageStrip();
				} else {
					view.openGroup();
				}
			}

		});
		submitDialogButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					validateLoginForm();
				}
		}); 
	}
	protected void manageStrip() {
		((BaseMenuActivity)context).manageOpenMenuStrip(view);
	}
	public void validateLoginForm() {		
    	//Client validations
		Log.d(getClass().getSimpleName(), "validate login form");
		String error = null;
		if (Utils.isEtraderProject(context)) {
			error = Utils.validatePasswordCD(context, passwordET.getText().toString());
		} else {
			error = Utils.validatePassword(context, passwordET.getText().toString());
		}
    	if (error != null){
    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
    		return;
    	}    	
    	//Service validations
        UserMethodRequest request = new UserMethodRequest();
        request.setUserName(ap.getUser().getUserName());
        Log.d("", passwordET.getText().toString());
        if (Utils.isEtraderProject(context)) {
        	request.setPassword(passwordET.getText().toString().toUpperCase());
        } else {
        	request.setPassword(passwordET.getText().toString());
        }
        Utils.requestService(this, responseH, "getUserCallBack", request, new UserMethodResult(), "getUser", (Activity)context);
	}
	
	public void getUserCallBack(Object result) {
		 Log.i("PersonalDetailsView", "getUserCallBack - After Client Validation");
		 UserMethodResult res = (UserMethodResult) result;
		 if (res.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
			 Utils.showAlert(context, res.getUserMessages()[0].getField(), 
					 res.getUserMessages()[0].getMessage(), context.getString(R.string.alert_title));
	     } else {
	    	 shouldOpen = true;
	    	 cancelDialogButton.performClick();
	    	 menuLine.findViewById(R.id.tableLayoutGroup).setVisibility(View.VISIBLE);
	     }
	}

	public void initMyAccountPersonalDetails() {
		User user = ap.getUser();
		Log.d("personal details" , "init user :" + user.getUserName() + " " + user.getId() + " details");
		if (!Utils.isEtraderProject(context) && user.getUserName().equalsIgnoreCase(user.getEmail()) ) { //anyoption and new customer
			findViewById(R.id.rowMyAccountUsername).setVisibility(View.GONE);
		} else {
			TextView userNameTextView = (TextView)tlMain.findViewById(R.id.textViewMyAccountUsername);
	        userNameTextView.setText(user.getUserName());
		}
        TextView firstNameTextView = (TextView)tlMain.findViewById(R.id.textViewMyAccountFirstName);
        firstNameTextView.setText(user.getFirstName());
        TextView lastNameTextView = (TextView)tlMain.findViewById(R.id.textViewMyAccountLastName);
        lastNameTextView.setText(user.getLastName());
        TextView streetNameTextView = (TextView)tlMain.findViewById(R.id.textViewMyAccountStreet);
        streetNameTextView.setText(user.getStreet());
        TextView streetNoTextView = (TextView)tlMain.findViewById(R.id.textViewMyAccountStreetNo);
        streetNoTextView.setText(user.getStreetNo());
        TextView postalTextView = (TextView)tlMain.findViewById(R.id.textViewMyAccountPostal);
        postalTextView.setText(user.getZipCode());
        TextView cityTextView = (TextView)tlMain.findViewById(R.id.textViewMyAccountCity);
        cityTextView.setText(user.getCityName());
        TextView birthDateTextView = (TextView)tlMain.findViewById(R.id.textViewMyAccountBirthdate);
		if (user.getTimeBirthDate() != null) {
			SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yy");
			birthDateTextView.setText(formatDate.format(user.getTimeBirthDate()));
		}
        TextView genderTextView = (TextView)tlMain.findViewById(R.id.textViewMyAccountGender);
        genderTextView.setText(user.getGenderTxt());
        TextView emailTextView = (TextView)tlMain.findViewById(R.id.textViewMyAccountEmail);
        emailTextView.setText(user.getEmail());
        TextView mobileTextView = (TextView)tlMain.findViewById(R.id.textViewMyAccountMobilephone);
        if (user.getMobilePhonePrefix() != null && user.getMobilePhone() != null){
        	mobileTextView.setText(user.getMobilePhonePrefix()+"-"+user.getMobilePhoneSuffix());
        }        
        TextView addPhoneTextView = (TextView)tlMain.findViewById(R.id.textViewMyAccountAddPhone);
        if (user.getLandLinePhonePrefix() != null && user.getLandLinePhone() != null){        	     
        	addPhoneTextView.setText(user.getLandLinePhonePrefix()+"-"+user.getLandLinePhoneSuffix());
        }
	}
	
	@Override
	public void openGroup() {
		if (shouldOpen) {
			super.openGroup();
			isOpened = true;
		} else {
			validatePassword();
		}
	}
	
	@Override
	public void closeGroup() {
		super.closeGroup();
     	menuLine.findViewById(R.id.tableLayoutGroup).setVisibility(View.GONE);
     	isOpened = false;
	}

	public void toggleGroup(){
		if(isOpened){
			closeGroup();
		}else {
			openGroup();
		}
	}
	@Override
	public void removeAllRows() {
		// TODO Auto-generated method stub
		
	}
	
	public void onResume() {
	    initMyAccountPersonalDetails();
	}

	public void initialize() {
		initMyAccountPersonalDetails();
	}
}
