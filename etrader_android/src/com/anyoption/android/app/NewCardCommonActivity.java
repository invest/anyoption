package com.anyoption.android.app;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.CardMethodRequest;
import com.anyoption.json.requests.CcDepositMethodRequest;
import com.anyoption.json.requests.FirstEverDepositCheckRequest;
import com.anyoption.json.results.CardMethodResult;
import com.anyoption.json.results.FirstEverDepositCheckResult;
import com.anyoption.json.results.TransactionMethodResult;

/**
 * @author KobiM
 *
 */
public abstract class NewCardCommonActivity extends ReceiptActivity {
            
	/**
	 * True if the user hasn't made his first deposit (successful or not).
	 */
	private boolean isFirstEverDeposit;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        setContentView(buildLayout(Constants.PAYMENT_TYPE_DEPOSIT));
        responseH = new Handler();
        context = this;
        PaymentMethod payment = (PaymentMethod) getIntent().getExtras().get(Constants.PUT_EXTRA_BANKING_PAYMENT);

        findViewById(R.id.imageViewInfo).setTag(payment);
        ArrayAdapter spinnerYearTypeArrayAdapter = new ArrayAdapter(this,
	              android.R.layout.simple_spinner_item, Utils.getCreditCardYearsArray(this));  
        spinnerYearTypeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
	    ((Spinner) findViewById(R.id.spinnerYear)).setAdapter(spinnerYearTypeArrayAdapter);
	    
	    Spinner spinnerCardType = (Spinner) findViewById(R.id.spinnerCardType);
	    ArrayList<String> cardTypes = new ArrayList<String>();
	    Collections.addAll(cardTypes, getResources().getStringArray(R.array.cc_type_array_value));
	    
	    ArrayAdapter<String> cardTypeAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,cardTypes);
	    cardTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    spinnerCardType.setAdapter(cardTypeAdapter);
	    
	    AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        User user = ap.getUser();
        ((TextView)findViewById(R.id.TextViewCurrencySymbol)).setText(user.getCurrencySymbol());
        ((TextView)findViewById(R.id.TextViewHeader)).setText(getString(R.string.profitLine_deposit).toUpperCase());
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        
        checkAndRemoveMaestro(cardTypes);

        if (!Utils.isEtraderProject(context)) {
			getFirstEverDepositCheck();
        }
    }

	private void checkAndRemoveMaestro(ArrayList<String> cardTypes) {
		AnyoptionApplication app = (AnyoptionApplication)getApplication();
	    long currencyId = app.getUser().getCurrencyId();
	    
		// Index of Maestro from cc_type_array_value string array from resources
		int maestroIndex = 1;
	    
	    if(Utils.isRegulated(app) || currencyId == Constants.CURRENCY_WON || currencyId == Constants.CURRENCY_YUAN) {
	    	// This will cause problems if the order of the values in cc_type_array_value array is changed or removing more than one card type
	    	cardTypes.remove(maestroIndex);
	    }
	}
    
	/**
	 * insert new card and deposit
	 * if its first deposit update the user extra fields also
	 * @param target the button that was clicked
	 */
    public void insertNewCardClickHandler(View target) {
    	if (!Utils.isEtraderProject(context) && isFirstEverDeposit) {
			DepositExtraFieldsView depositExtraFieldsView = (DepositExtraFieldsView) findViewById(R.id.DepositExtraFieldsView);
			depositExtraFieldsView
					.validateAndUpdate("insertNewCardClickHandler");
		} else {
			insertNewCardClickHandler();
		}
    }
    
    /**
     * insert new card and deposit
     */
    public void insertNewCardClickHandler() {
		EditText cardNum = (EditText)findViewById(R.id.editTextCardNum);
		EditText cvv = (EditText)findViewById(R.id.editTextCcPass);
		EditText holderName = (EditText)findViewById(R.id.editTextHolderName);			
		EditText depositAmount = (EditText)findViewById(R.id.editTextAmount);				   	
    	Spinner spinnerCardType = (Spinner) findViewById(R.id.spinnerCardType);
    	Spinner spinnerMonth = (Spinner) findViewById(R.id.spinnerMonth);
    	Spinner spinnerYear = (Spinner) findViewById(R.id.spinnerYear);
    	EditText amountTxt = (EditText)findViewById(R.id.editTextAmount);    	
    	
    	//user.setCurrencyId(((OptionItem)spinnerCardType.getAdapter().getItem(spinnerCardType.getSelectedItemPosition())).getId());
    	
    	//Client validations
    	String error = null;
    	
    	//validate card type
    	int cardTypeSpinnerPos = spinnerCardType.getSelectedItemPosition();
    	if (cardTypeSpinnerPos == 0) {
    		Utils.showAlert(this, getString(R.string.creditcards_type_edit), getString(R.string.error_mandatory), 
    				this.getString(R.string.alert_title));
    		return ;
    	}
    	
   		//validate card number 	 
    	error = Utils.validateDigitsOnlyAndEmpty(this, cardNum.getText().toString() , getString(R.string.creditcards_ccNum)); 
    	if (null != error) {
    		Utils.showAlert(this, Constants.EMPTY_STRING, error, this.getString(R.string.alert_title));  
    		return ;
    	}
    	
    	//validate cvv
    	long cardTypeKey = 0;
		try {
			
			ArrayList<String> cardTypeKeys = new ArrayList<String>();
			Collections.addAll(cardTypeKeys, getResources().getStringArray(R.array.cc_type_array_key));
			
			checkAndRemoveMaestro(cardTypeKeys);
			
			cardTypeKey = Long.valueOf(cardTypeKeys.get(cardTypeSpinnerPos));
			error = Utils.validateCVV(this, cvv.getText().toString(), cardTypeKey);
			if (null != error) {
				Utils.showAlert(this, Constants.EMPTY_STRING, error, this.getString(R.string.alert_title));  
				return ;
			}
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			Log.d(getPackageName(), "problem", e);
			e.printStackTrace();
		}    	
    	
		//validate year
    	int year = spinnerYear.getSelectedItemPosition();
    	if (year == 0) {
    		Utils.showAlert(this, getString(R.string.creditcards_year), getString(R.string.error_mandatory), 
    				this.getString(R.string.alert_title));
    		return ;
    	}
    	
    	//validate month
    	int month = spinnerMonth.getSelectedItemPosition();
    	if (month == 0) {
    		Utils.showAlert(this, getString(R.string.month), getString(R.string.error_mandatory), 
    				this.getString(R.string.alert_title));
    		return ;
    	}
    	
    	error = Utils.validateCCExpDate(this, Integer.parseInt(spinnerYear.getSelectedItem().toString()),
    					Integer.parseInt(spinnerMonth.getSelectedItem().toString()));
    	if (null != error) {
    		Utils.showAlert(this, getString(R.string.creditcards_expDate), error, this.getString(R.string.alert_title));  
    		return ;
    	}
    	
    	//validate holdername
    	error = Utils.validateEnglishLettersOnlyAndEmpty(this, holderName.getText().toString(), getString(R.string.creditcards_holderName));
    	if (null != error) {
    		Utils.showAlert(this, Constants.EMPTY_STRING, error, this.getString(R.string.alert_title));  
    		return ;
    	}    	
    	

    	CardMethodRequest request = new CardMethodRequest();           
        CreditCard card = new CreditCard();
      
       //validate holder Id
        error = setHolderId(card);
        if (error != null) {
			return ;
		}
        
      //validate deposit Amount
    	error = Utils.validateDoubleAndEmpty(this, depositAmount.getText().toString(), getString(R.string.deposit_deposit));
    	if (null != error) {
    		Utils.showAlert(this, Constants.EMPTY_STRING, error, this.getString(R.string.alert_title));  
    		return ;
    	}
    	
    	//Service validations
        card.setCcNumber(Long.valueOf(cardNum.getText().toString()));
        card.setCcPass(cvv.getText().toString());
        card.setHolderName(holderName.getText().toString());
        card.setExpMonth(spinnerMonth.getSelectedItem().toString()); //maybe need to do -1
        card.setExpYear(spinnerYear.getSelectedItem().toString());     
        card.setTypeId(cardTypeKey);
        request.setCard(card); 
    	Utils.requestService(this, responseH, "insertCardCallBack", request, new CardMethodResult(), "insertCard");
    }
    
    public abstract String setHolderId(CreditCard card);
    
	public void insertCardCallBack(Object resultObj){
		Log.d(getPackageName(), "insertCardCallBack");
		CardMethodResult result = (CardMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(getPackageName(), "Insert card succefully");
			CcDepositMethodRequest request = new CcDepositMethodRequest();
			EditText depositAmount = (EditText)findViewById(R.id.editTextAmount);
			request.setAmount(depositAmount.getText().toString());
			request.setCardId(result.getCard().getId());
			request.setError(result.getError());
			request.setFirstNewCardChanged(false); //For repeat failure  
			Utils.requestService(this, responseH, "insertDepositCallBack", request, new TransactionMethodResult(), "insertDepositCard");
		} else {
			Log.d(getPackageName(), "Error inserting failed");
			Utils.showAlert(this, "", result.getUserMessages()[0].getMessage(), this.getString(R.string.alert_title));
		}
	}
	
	 @Override
     public void onBackPressed() {
     	if (getIntent().getBooleanExtra(Constants.PUT_EXTRA_SHOW_LOG_OUT_ALERT, false)) {
     		((Activity) context).startActivityForResult(
	                new Intent(context,
	                        MarketsGroupActivity.class).putExtra(Constants.PUT_EXTRA_SHOW_LOG_OUT_ALERT, true),
	                        Constants.REQUEST_CODE_MARKETS_GROUP_PAGE);
     	} else {
     		finish();
     	}
     }
	 
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if (getIntent().getBooleanExtra(Constants.PUT_EXTRA_SHOW_LOG_OUT_ALERT, false)) {            
                this.finish();            
        }
    }

	private void getFirstEverDepositCheck() {
		FirstEverDepositCheckRequest request = new FirstEverDepositCheckRequest();
		AnyoptionApplication application = (AnyoptionApplication) getApplication();
		User user = application.getUser();
		request.setUserId(user.getId());
		
		Utils.requestService(this, responseH,
				"getFirstEverDepositCheckCallback", request,
				new FirstEverDepositCheckResult(), "getFirstEverDepositCheck");
	}

	public void getFirstEverDepositCheckCallback(Object resultObject) {
		FirstEverDepositCheckResult result = (FirstEverDepositCheckResult) resultObject;

		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			isFirstEverDeposit = result.isFirstEverDeposit();
		} else {
			isFirstEverDeposit = false;
		}

		DepositExtraFieldsView extraFieldsView = (DepositExtraFieldsView) findViewById(R.id.DepositExtraFieldsView);
		extraFieldsView.setVisibility(View.VISIBLE);

		if (isFirstEverDeposit) {
			extraFieldsView.setCurrencyEnabled(true);
			extraFieldsView
					.setCurrencyChangeListner((TextView) findViewById(R.id.TextViewCurrencySymbol));
		} else {
			// Will be displayed only currency in disabled state.
			extraFieldsView.setCurrencyEnabled(false);
			extraFieldsView.setStreetVisibility(View.GONE);
			extraFieldsView.setCityVisibility(View.GONE);
			extraFieldsView.setZipCodeVisibility(View.GONE);
		}
	}
}