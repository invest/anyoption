/**
 * 
 */
package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.UpdateUserMethodRequest;
import com.anyoption.json.results.MethodResult;

/**
 * @author AviadH
 *
 */
public abstract class ChangePersonalDetailsCommonActivity extends BaseActivity {
	
	protected Context context;
	protected Handler responseH;
	protected User user;
	protected String landPrefix;
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
		 Log.d("ChangePersonalDetailsView", "Personal details - on create");
		 setContentView(R.layout.change_pesonal_details);
		 context = this;
		 responseH = new Handler();
		 user = ((AnyoptionApplication) getApplicationContext()).getUser(); //get the user from application

		 ((TextView)findViewById(R.id.personal_details_change_username)).setText(user.getUserName());
		 ((TextView)findViewById(R.id.personal_details_change_firstname)).setText(user.getFirstName());
		 ((TextView)findViewById(R.id.personal_details_change_lastname)).setText(user.getLastName());
		 ((EditText)findViewById(R.id.personal_details_change_streetaddress)).setText(user.getStreet());
		 ((EditText)findViewById(R.id.personal_details_change_house)).setText(user.getStreetNo());
		 ((EditText)findViewById(R.id.personal_details_change_zipcode)).setText(user.getZipCode());
		 
		 ((TextView)findViewById(R.id.personal_details_change_dateofbirth)).setText(user.getTimeBirthDateTxt());

		 
		 ((TextView)findViewById(R.id.personal_details_change_gender)).setText(user.getGenderTxt());
		 ((EditText)findViewById(R.id.personal_details_change_email)).setText(user.getEmail());
		 if (Utils.isEtraderProject(this)) {
			 ((EditText)findViewById(R.id.personal_details_change_mobile)).setText(user.getMobilePhoneSuffix());
			 ((EditText)findViewById(R.id.personal_details_change_phone)).setText(user.getLandLinePhoneSuffix());
		 } else {
			 ((EditText)findViewById(R.id.personal_details_change_mobile)).setText(user.getMobilePhone());
			 ((EditText)findViewById(R.id.personal_details_change_phone)).setText(user.getLandLinePhone());
		 }
		 init(user);
		 setButtonClicks();
	}
	
	public abstract void init(User user);			        
	
	public void setButtonClicks() {
		Button save = (Button) findViewById(R.id.buttonMyAccountPersonalDetailsSubmit);
		Button cancel = (Button) findViewById(R.id.buttonMyAccountPersonalDetailsClose);
		cancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
		save.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				UpdateUserMethodRequest request = new UpdateUserMethodRequest();
				user = ((AnyoptionApplication) getApplicationContext()).getUser();
		    	
				
				EditText house = (EditText)findViewById(R.id.personal_details_change_house);
				EditText zipcode = (EditText)findViewById(R.id.personal_details_change_zipcode);
				EditText email =(EditText)findViewById(R.id.personal_details_change_email);
				EditText mobilePhone = (EditText)findViewById(R.id.personal_details_change_mobile);
				EditText phone = (EditText)findViewById(R.id.personal_details_change_phone);
		    	
				
				
		    	//Client validations
		    	String error = null;
		    	//Email
		    	error = Utils.validateEmailAndEmpty(context, email.getText().toString(), getString(R.string.register_email)); 
		    	if (error!= null){
		    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
		    		return ;
		    	}
		    		    	
	    		//Mobile Phone    	
		    	error = Utils.validateDigitsOnlyAndEmpty(context, mobilePhone.getText().toString(), getString(R.string.register_mobilephone));
		    	if (error!= null){
		    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
		    		return ;
		    	}
		    	if (mobilePhone.getText().toString().length() < 7){
					String[] params = new String[2];
					params[0] = getString(R.string.register_mobilephone);
					params[1] = "7";    		
		    		Utils.showAlert(context, "", String.format(context.getString(R.string.javax_faces_validator_LengthValidator_MINIMUM), params), 
		    				context.getString(R.string.alert_title));  
		    		return ;
		    	}
		    	//LandLine Phone 
		    	if(!phone.getText().toString().equals("")) {
			    	error = Utils.validateDigitsOnlyAndEmpty(context, phone.getText().toString(), getString(R.string.register_phone));
			    	if (error!= null){
			    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
			    		return ;
			    	}
			    	if (phone.getText().toString().length() < 7){
						String[] params = new String[2];
						params[0] = getString(R.string.register_phone);
						params[1] = "7";    		
			    		Utils.showAlert(context, "", String.format(context.getString(R.string.javax_faces_validator_LengthValidator_MINIMUM), params), 
			    				context.getString(R.string.alert_title));  
			    		return ;
			    	}
		    	}
		    	
		    	
		    	user.setEmail(email.getText().toString());		    	
		    		    	
		    	
		    	user.setZipCode(zipcode.getText().toString());
		    	user.setStreetNo(house.getText().toString());
		    	
		    	error = validateAndSet(error, phone);
				if (error != null) {
					return ;
				}				
				if(Utils.isEtraderProject(context)) {
					if(!phone.getText().toString().equals("")) {
						String landLinePhone = landPrefix + phone.getText().toString();
						user.setLandLinePhonePrefix(landPrefix);
						user.setLandLinePhoneSuffix(phone.getText().toString());
						user.setLandLinePhoneCD(landLinePhone);
					}
		    		user.setMobilePhoneSuffix(mobilePhone.getText().toString());
		    		user.setMobilePhone(user.getMobilePhonePrefix()+user.getMobilePhoneSuffix());		    		
		    	} else {
		    		user.setMobilePhone(mobilePhone.getText().toString());
			    	user.setLandLinePhone(phone.getText().toString());
		    	}	
		        MethodResult result = new MethodResult();     
		        
		        request.setSkinId(user.getSkinId());
		        request.setUser(user);
		                
		    	Utils.requestService(context, responseH, "updateUserCallBack", request, result, "updateUser");
			}
		});
	}
	
	public abstract String validateAndSet(String error, EditText phone);
	
	public void updateUserCallBack(Object resultObj){
		Log.d(getPackageName(), "updateUserCallBack");
		MethodResult result = (MethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			AnyoptionApplication ap = (AnyoptionApplication) context.getApplicationContext();
			ap.setUser(user);
			Intent data = new Intent();
			data.putExtra(Constants.RESULT_USER_OBJECT, user);
			setResult(Activity.RESULT_OK, data);
			finish();
		} else {
			 Log.d(getPackageName(), "Error Update user");
			 String error = result.getUserMessages()[0].getMessage();
			 String field = result.getUserMessages()[0].getField();
			 Utils.showAlert(context, field, error, context.getString(R.string.alert_title));			
		}
	}
}
