package com.anyoption.android.app;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.PaymentMethod;
import com.anyoption.common.beans.base.User;

public class BankingViewBase extends MenuViewBase{
	
	protected static HashMap<String, String> xmlKeysForClasses;
	protected int type;
	
	public BankingViewBase(Context context) {
		this(context, null);
	}
	
	public BankingViewBase(Context context, AttributeSet attrs) {
		super(context, attrs);
		xmlKeysForClasses = new HashMap<String, String>();
		String packageName = "";
		if (Utils.isEtraderProject(context)) {
			packageName = Constants.ETRADER_PACKAGE;
		} else {
			packageName = Constants.ANYOPTION_PACKAGE;
		}
		xmlKeysForClasses.put("deposit_creditcards", Constants.ANYOPTION_PACKAGE + "CcDepositActivity");
		xmlKeysForClasses.put("deposit_bank_wire", Constants.ANYOPTION_PACKAGE + "BankWireDepositActivity");
		xmlKeysForClasses.put("deposit_envoy", Constants.ANYOPTION_PACKAGE + "DomesticPaymentDepositActivity");
		xmlKeysForClasses.put("cc_withdraw", Constants.ANYOPTION_PACKAGE + "CcWithdrawalActivity");
		xmlKeysForClasses.put("bank_wire_withdrawal", packageName + "BankWireWithdrawActivity");
		xmlKeysForClasses.put("banking_not_available", Constants.ANYOPTION_PACKAGE + "NotAvailableActivity");
		xmlKeysForClasses.put("cup_deposit", Constants.ANYOPTION_PACKAGE + "CupDepositActivity");
		xmlKeysForClasses.put("cup_withdraw", Constants.ANYOPTION_PACKAGE + "CupWithdrawActivity");
		xmlKeysForClasses.put("baropay_deposit", Constants.ANYOPTION_PACKAGE + "BaropayDepositActivity");
		xmlKeysForClasses.put("baropay_withdraw", Constants.ANYOPTION_PACKAGE + "BaropayWithdrawActivity");
	}
	
	public void makePaymentMethodsRows(final int paymentType) {
		type = paymentType;
		AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context).getApplication();
		User user = ap.getUser();
		ArrayList<PaymentMethod> pm = ap.getCountries().get(user.getCountryId()).getPayments();
		if (pm.size() > 0){			        	
			for (final PaymentMethod payment : pm) {				
				if (payment.getTypeId() == paymentType && (user.getSkinId() == 3 && payment.getTypeId() != PaymentMethod.PAYMENT_TYPE_UKASH)) {
					final TableRow tr = (TableRow) inflater.inflate(R.layout.banking_deposit_withdrawal_row, tlMain, false);
					TextView textContent = (TextView) tr.findViewById(R.id.textViewBankingRowText);
					ImageView imageContent = (ImageView) tr.findViewById(R.id.textViewBankingRowImage);
					imageContent.setImageResource(this.getResources().getIdentifier(payment.getSmallLogo(), "drawable", context.getPackageName()));    			
		    		textContent.setText(Utils.getStringFromResource(context, payment.getDisplayName(), null));
		    		tr.setTag(payment);
		    		tr.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							bankingRowClick(payment.getDisplayName(), null, 
									view.getResources().getIdentifier(payment.getSmallLogo(), "drawable", context.getPackageName()),
									context.getResources().getIdentifier(payment.getDisplayName(), "string", context.getPackageName()), (PaymentMethod) tr.getTag());	
						}
					});
		    		tlMain.addView(tr);
				}
			}
        }	        
	}
	
	public void bankingRowClick(String type, Intent intent, int imageId, int subHeaderId, PaymentMethod payment) {
		bankingRowClick(type, intent, imageId, subHeaderId, payment, context);
	}
	
	public static void bankingRowClick(String type, Intent intent, int imageId, int subHeaderId, PaymentMethod payment, Context context) {
		Class<Activity> activityClass = null;
		try {
			if (null == intent) {
				activityClass = (Class<Activity>) Class.forName(xmlKeysForClasses.get(type));
				intent = new Intent(context, activityClass);
			}
		} catch (Exception e) {
			Log.w("BankingViewBase", "Taking default class(not available) -- exception: " + e);
			try {
				activityClass = (Class<Activity>) Class.forName(xmlKeysForClasses.get("banking_not_available"));
				type = "not_available";
				intent = new Intent(context, activityClass);
			} catch (ClassNotFoundException e1) {
				//TODO: remove this... temporary because we missing some classes of payment methods
			}
		} finally {
			intent.putExtra(Constants.PUT_EXTRA_BANKING_PAYMENT, payment);
			intent.putExtra(Constants.PUT_EXTRA_BANKING_TYPE, type);
			intent.putExtra(Constants.PUT_EXTRA_LAYOUT_BANKING, type);
			intent.putExtra(Constants.PUT_EXTRA_BANKING_HEADER_IMAGE_ID, imageId);
			intent.putExtra(Constants.PUT_EXTRA_BANKING_SUB_HEADER, subHeaderId);
			((Activity)context).startActivityForResult(intent, Constants.REQUEST_CODE_CC_DEPOSIT);
		}
	}

	@Override
	public void removeAllRows() {
		tlMain.removeAllViews();
	}
}
