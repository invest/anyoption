package com.anyoption.android.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;


public class ContactUsActivity extends BaseMenuActivity {
	
	private View lastOpenRow;
	private LayoutInflater inflater;
	private Handler responseH;
	private AnyoptionApplication ap;
	LinearLayout viewButtons;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);     
        setContentView(R.layout.contact_us);
        ((TextView)findViewById(R.id.TextViewHeader)).setText(getString(R.string.rightnav_contact).toUpperCase());
        inflater = getLayoutInflater();
        ap = (AnyoptionApplication) getApplication();
        //String supportPhone = ap.getCountries().get(user.getCountryId()).getSupportPhone();
        
        LinearLayout menu = (LinearLayout)findViewById(R.id.linearLayoutMenu);
        for (int i = 0 ; i < menu.getChildCount() ; i++) {
        	((MenuViewBase)menu.getChildAt(i)).setOnMenuStripClickListener(new OnMenuStripClickListener() {
				public void onClickCloseAll(MenuViewBase menuViewBase) {
					manageOpenMenuStrip(menuViewBase);
				}
			});
        }
        if (!Utils.isEtraderProject(this)){        	       
	        Skin skin = ap.getSkins().get(ap.getSkinId());
	        ((TextView) findViewById(R.id.openningHours)).setText(getString(R.string.time_open ,skin.getSupportStartTime(), skin.getSupportEndTime()));
        } else {
        	((TextView) findViewById(R.id.openningHours)).setText(getString(R.string.time_open));
        }
        if (null == ap.getUser()) {
        	findViewById(R.id.linearLayoutAdvisor).setVisibility(View.VISIBLE);
        }
        viewButtons = (LinearLayout)findViewById(R.id.LinearLayoutViewButtons);
    }
        
    public void openCallMe(View target){
    	startActivityForResult(new Intent(this,
    	          CallMeActivity.class), Constants.REQUEST_CODE_REGISTER);
    }
    
    public void openSendAnEmail(View target){
    	startActivityForResult(new Intent(this,
  	          SendEmailActivity.class), Constants.REQUEST_CODE_REGISTER);
    }
    
    public void openCallUs(View target){
    	AnyoptionApplication ap = (AnyoptionApplication) (this).getApplication();
    	User user = ap.getUser();
    	LinearLayout checkPass = (LinearLayout) inflater.inflate(R.layout.call_us_pop_up, null, false);
    	final TextView supportPhone = (TextView) checkPass.findViewById(R.id.textViewSupportPhone);
    	if (user != null && !Utils.isEtraderProject(this)) {
    		supportPhone.setText(ap.getCountries().get(user.getCountryId()).getSupportPhone());
    	} else {
    		supportPhone.setText(getResources().getString(R.string.contact_phone_default));
    	}
		Button cancelDialogButton = (Button) checkPass.findViewById(R.id.buttonMyAccountPersonalDetailsPopupClose);
		Button submitDialogButton = (Button) checkPass.findViewById(R.id.buttonCallUsPopupSubmit);
		Utils.showCustomAlert(this, checkPass, cancelDialogButton);
		submitDialogButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					callService(supportPhone.getText().toString());
				}
		}); 				        	
    }

    private void callService(String phoneNumber){
    	 Intent callIntent = new Intent(Intent.ACTION_CALL);
    	 callIntent.setData(Uri.parse("tel:" + phoneNumber ));
    	 startActivity(callIntent);
    }    
    
    public void manageOpenMenuStrip(View target) {
    	super.manageOpenMenuStrip(target);
    	if (lastOpenGroup != null) {
    		viewButtons.setVisibility(View.GONE);
    	} else {
    		viewButtons.setVisibility(View.VISIBLE);
    	}
    }
}