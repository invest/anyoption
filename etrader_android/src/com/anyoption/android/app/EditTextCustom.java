package com.anyoption.android.app;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.EditText;

import com.anyoption.android.app.util.Utils;

/**
 * @author EyalG
 */
public class EditTextCustom extends EditText{
	
	private Context context;
	
	public EditTextCustom(Context context) {
		this(context, null);
		this.context = context;
		fixGravity();
	}
	
	public EditTextCustom(final Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		fixGravity();
	}
	
	public EditTextCustom(Context context, AttributeSet attrs, int defStyle) { 
        super(context, attrs, defStyle);
        this.context = context;
        fixGravity();
    } 
	
	//if we r in ice cream and etrader use gravity right
	public void fixGravity() {
		if (android.os.Build.VERSION.SDK_INT > 13 && Utils.isEtraderProject(context)) {
			this.setGravity(Gravity.RIGHT);
		}
	}
}