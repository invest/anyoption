package com.anyoption.android.app;

import java.util.Calendar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.DataBaseHelper;
import com.anyoption.android.app.util.SetVisibilityRunnable;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.AssetIndexMethodRequest;
import com.anyoption.json.requests.ChartDataMethodRequest;
import com.anyoption.json.requests.PastExpiresMethodRequest;
import com.anyoption.json.results.AssetIndexMethodResult;
import com.anyoption.json.results.ChartDataMethodResult;
import com.anyoption.json.results.OpportunitiesMethodResult;

public class AssetPageActivity extends BaseTradingActivity {
    private static final String INVESTMENT_SUCCESS_POPUP_ID = "com.anyoption.android.app.AssetPageActivity.INVESTMENT_SUCCESS_POPUP_ID";

    private Opportunity[] oppResult;
    private Market market;
    private Context context;
    private boolean portrait;
    private Investment addedInvestment;
    private Long addedInvestmentId;
    private boolean isFirst;

    public AssetPageActivity() {
        refreshHeaderOnResume = false;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.asset_page);
        super.onCreate(savedInstanceState);
        context = this;
        market = (Market) getIntent().getExtras().get(Constants.PUT_EXTRA_MARKET);
        Log.d(this.getLocalClassName(), "market id, on create: " + market.getId());
        lastOpenGroup = null;
        isFirst = true;

        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int orientation = display.getOrientation();
        portrait = orientation == Surface.ROTATION_0 || orientation == Surface.ROTATION_180;

        if (portrait) {
        	menuScrollView = (ScrollView) findViewById(R.id.scrollViewMenu);
            PastExpiresMethodRequest pastExpRequest = new PastExpiresMethodRequest();
            pastExpRequest.setMarketId(market.getId());
            pastExpRequest.setPageSize(3);
            pastExpRequest.setStartRow(0);
            pastExpRequest.setDate(null);
			Utils.requestService(this, responseH, "pastExpireCallBack", pastExpRequest, new OpportunitiesMethodResult(), "getPastExpiries");
            
            AssetIndexMethodRequest assetIndexRequest = new AssetIndexMethodRequest();
            //TODO: SET dynamic parameters
            assetIndexRequest.setMarketId(market.getId());
            Utils.requestService(this, responseH, "assetInfoCallBack", assetIndexRequest, new AssetIndexMethodResult(), "getAssetIndexByMarket");
        }
        
        if (null != savedInstanceState) {
            addedInvestmentId = savedInstanceState.getLong(INVESTMENT_SUCCESS_POPUP_ID);
        }
    }
    
    @Override
	protected void chartRequest() {
        Log.d(getPackageName(), "chartRequest - market id = " + market.getId());
        if (null != ls && null != lsTable) {
            ls.removeTable(lsTable);
            lsTable = null;
        }
        ChartDataMethodRequest request = new ChartDataMethodRequest();
        request.setMarketId(market.getId());
        Utils.requestService(this, responseH, "chartCallBack", request, new ChartDataMethodResult(), "getChartData");
    }
    
    public void chartCallBack(Object resultObj) {
    	Log.d(getPackageName(), "chartCallBack");
    	ChartDataMethodResult result = (ChartDataMethodResult) resultObj;
        if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            ChartView.fixDates(result);
            chart.setChartData(result);
            Log.d(getLocalClassName(), "insurancesCount " + insurancesCount);
            if (portrait) {
            	AnyoptionApplication ap = (AnyoptionApplication) getApplication();
            	if (null != ap.getUser()) {
	                if (null == myOptions) {
	                	LinearLayout llMain = (LinearLayout) findViewById(R.id.linearLayoutMain);
	                    MyOptionView mySetteled  = new MyOptionAssetPage(this, true, true, null);
	                    mySetteled.setMenuScrollView(menuScrollView);
	                    mySetteled.setOnMenuStripClickListener(new OnMenuStripClickListener() {
	                        public void onClickCloseAll(MenuViewBase menuViewBase) {
	                            manageOpenMenuStrip(menuViewBase);
	                        }
	                    });
	                    llMain.addView(mySetteled, 0);
	                    myOptions = new MyOptionAssetPage(this, false, false, null);
	                    myOptions.setMenuScrollView(menuScrollView);
	                    myOptions.setOnMenuStripClickListener(new OnMenuStripClickListener() {
	                        public void onClickCloseAll(MenuViewBase menuViewBase) {
	                            manageOpenMenuStrip(menuViewBase);
	                        }
	                    });
	                    manageOpenMenuStrip(myOptions);
	                    llMain.addView(myOptions, 0);
	                } else {
	                    myOptions.hideInsurancesRow();
	                    myOptions.removeAllRows();
	                }
	                if (null != myOptions) {
		                Investment[] invs = result.getInvestments();
		                if (null != invs && invs.length > 0) {
		                	myOptions.setRowsNumber(0);
		                	myOptions.addNumberOfInvestmets(invs.length);
		                    myOptions.setInvestments(invs);
		                } else {
		                	myOptions.makeNoInformationRow(((MyOptionAssetPage) myOptions).getNoInformationString());
		                	if (isFirst) {
		                		manageOpenMenuStrip(myOptions);
		                	}
		                }
		                if (insurancesCount > 0) {
		                    myOptions.showInsurancesRow(insurancesType, insuranceCloseTime);
		                }
	                }
            	}
            } else {
                if (insurancesCount > 0) {
                    showInsurancesButton();
                }
            }
            
            String group = null;
            if (result.getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
                group = "aotps_" + result.getScheduled() + "_" + result.getMarketId();
            } else {
                group = "op_" + result.getMarketId();
            }
            if (result.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS) {
            	slip.setCommission(result.getCommission());
     	        TextView commission = (TextView) findViewById(R.id.textViewCommission);
     	        commission.setVisibility(View.VISIBLE);
     	        commission.setText(getResources().getString(R.string.slip_comission, Utils.formatCurrencyAmount(slip.getCommission(), slip.getCurrencySymbol(), slip.isCurrencyLeftSymbol(), slip.getCurrencyId())));
            }
            lsTable = Utils.addLightstreamerTable(this, new String[] {group}, ls, new AOTPSTableListener(this), 1.0);

            if (null != addedInvestmentId) {
                Investment[] invs = result.getInvestments();
                if (null != invs) {
                    for (int i = 0; i < invs.length; i++) {
                        if (addedInvestmentId == invs[i].getId()) {
                            addedInvestment = invs[i];
                            showInvestmentSuccessPopup();
                        }
                    }
                }
            }
            isFirst = false;
        }
    }
    
    public void pastExpireCallBack(Object resultObj) {
    	Log.d(getPackageName(), "pastExpipreCallBack");
    	OpportunitiesMethodResult opportunitiesResult = (OpportunitiesMethodResult) resultObj;
    	oppResult = opportunitiesResult.getOpportunities();
    	TableLayout tlMain = (TableLayout) findViewById(R.id.tableLayoutMarketPastExpiriesOpen);
    	tlMain.removeAllViews();
    	final LayoutInflater inflater = getLayoutInflater();
    	for (Opportunity opportunity : oppResult) {
    		Log.d(getPackageName(), String.valueOf(opportunity.getId()));
    		TableRow tr = (TableRow) inflater.inflate(R.layout.past_expiries_row, tlMain, false);
    		((TextView) tr.findViewById(R.id.textViewExpTime)).setText(opportunity.getTimeEstClosingTxt().split(" ")[0] + "  " + opportunity.getTimeEstClosingTxt().split(" ")[1]);
    		((TextView) tr.findViewById(R.id.textViewExpAmount)).setText(opportunity.getCloseLevelTxt());
    		tlMain.addView(tr);
	    }
    	TableLayout tl = new TableLayout(context);
		TableRow tr = (TableRow) inflater.inflate(R.layout.more_last_row_menu, tlMain, false);
		((TextView) tr.findViewById(R.id.textViewOther)).setText(getResources().getString(R.string.menuStrip_past_expiries_other));
    	tr.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				lastPassedExpiriesRowClickForPopUp(inflater);
			}
		});
    	makeNoDataRow(tlMain);
    	tl.addView(tr);
    	tlMain.addView(tl);
    }
    
    public void makeNoDataRow(TableLayout tlMain) {
    	if (tlMain.getChildCount() == 0) {
		    TableRow trFirst = (TableRow) getLayoutInflater().inflate(R.layout.first_dates_row, tlMain, false);
			trFirst.setTag(MyOptionView.NO_DATA_ROW);
			TextView tvFirst = (TextView) trFirst.findViewById(R.id.textViewFromAndToDates);
			tvFirst.setText(getString(R.string.general_emptylist));
			tvFirst.setTextColor(0xFFFFFFFF);
			tlMain.addView(trFirst);
    	}
    }
    
    public void lastPassedExpiriesRowClickForPopUp(LayoutInflater inflater) {
    	// get the current date 
        final Calendar c = Calendar.getInstance(); 
        int mYear = c.get(Calendar.YEAR); 
        int mMonth = c.get(Calendar.MONTH); 
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        
        LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.more_date_pop_up, null);
        TextView header = (TextView) ll.findViewById(R.id.textViewDateHeader);
        header.setText(getResources().getString(R.string.menuStrip_past_expiries));
        final DatePicker date = (DatePicker) ll.findViewById(R.id.datePicker);
        date.updateDate(mYear, mMonth, mDay);
        final Button close = (Button) ll.findViewById(R.id.buttonMoreSettledInvestmentPopUpClose);
        Button submit = (Button) ll.findViewById(R.id.buttonMoreSettledInvestmentPopUpSubmit);
        submit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//set the calender's (from and to) that the client choosed
				Calendar dateCal = Calendar.getInstance();
				
				//init the calender FROM to start of the day and TO to the end of the day
				dateCal.set(date.getYear(), date.getMonth(), date.getDayOfMonth());
				
				PastExpiresMethodRequest pastExpRequest = new PastExpiresMethodRequest();
	            pastExpRequest.setMarketId(market.getId());
	            pastExpRequest.setPageSize(0);
	            pastExpRequest.setStartRow(0);
	            pastExpRequest.setDate(dateCal);
				Utils.requestService(context, responseH, "pastExpireCallBack", pastExpRequest, new OpportunitiesMethodResult(), "getPastExpiries");
				close.performClick();
				
			}
		});
        Utils.showCustomAlert(this, ll, close);
	}

    public void assetInfoCallBack(Object resultObj) {
		Log.d(getPackageName(), "assetIndexCallBack");
		LinearLayout llOpen = (LinearLayout) findViewById(R.id.linearLayoutMraketAssetInfoOpen);
		LayoutInflater inflater = getLayoutInflater();
    	LinearLayout llOpenContent =  (LinearLayout) inflater.inflate(R.layout.asset_info, llOpen, false);
		AssetIndexMethodResult result = (AssetIndexMethodResult) resultObj;
		if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {			
	        TextView tradingTimeValue   = (TextView) llOpenContent.findViewById(R.id.textViewTradingTimeValue);
	        tradingTimeValue.setText(result.getAssetIndex().getTradingDaysFormat());
	        TextView symbolValue   = (TextView) llOpenContent.findViewById(R.id.textViewSymbolValue);
	        symbolValue.setText(result.getAssetIndex().getFeedName());
	        TextView expTypeValues   = (TextView) llOpenContent.findViewById(R.id.textViewExpTypeValues);	        
	        if (Utils.isEtraderProject(this) && result.getAssetIndex().getMarketGroupId() == Constants.MARKET_GROUP_ID_TA) {
	        	expTypeValues.setText(getString(R.string.investment_header_close_now_daily));
			} else {
	        	expTypeValues.setText(getString(R.string.investment_header_close_now));
	        }	        
	        TextView reutersFieldValue   = (TextView) llOpenContent.findViewById(R.id.textViewReutersFieldValue);
	        reutersFieldValue.setText(Html.fromHtml(result.getAssetIndex().getReutersField()));	
	        TextView expiryFormula   = (TextView) llOpenContent.findViewById(R.id.textViewExpiryFormula);
			String[] params = new String[1];
			params[0] = result.getAssetIndex().getExpiryFormula();			
	        expiryFormula.setText(Html.fromHtml(getString(R.string.assetIndex_expiryFormula, params)));
	        
			// Create DB and load lists
			DataBaseHelper db = null;
			Cursor c = null;
			try {
				db = new DataBaseHelper(getApplicationContext());
				db.openDataBase();
				AnyoptionApplication ap = (AnyoptionApplication) getApplication();					
				c = db.getMarketInfo(ap.getSkins().get(ap.getSkinId()).getDefaultLanguageId(), market.getId()); 
				while (c.moveToNext()) {
			        TextView marketDetails  = (TextView) llOpenContent.findViewById(R.id.textViewMarketDetails);
			        marketDetails.setText(Html.fromHtml(c.getString(c.getColumnIndex("value"))));             					
				}
			} finally {
				if (null != c) {
					c.close();
				}
				if (null != db) {
					db.close();
				}							
			}
		}
		llOpen.addView(llOpenContent);           	
    }
    
    @Override
	public void openGroup(View target) {
		ImageView iv = (ImageView) target.findViewById(R.id.imageViewMarketGroupArrow);
    	iv.setImageResource(R.drawable.list_arrow_on);        	
    	if (target.getId() == R.id.linearLayoutMraketAssetInfo) {
    		LinearLayout llOpen =  (LinearLayout) findViewById(R.id.linearLayoutMraketAssetInfoOpen); 
        	llOpen.setVisibility(View.VISIBLE);            	
    	} else if (target.getId() == R.id.linearLayoutMraketPastExpiries) {
    		TableLayout tlMain = (TableLayout) findViewById(R.id.tableLayoutMarketPastExpiriesOpen);
    		tlMain.setVisibility(View.VISIBLE);
    	}
    }
    
    @Override
	public void closeGroup(View group) {
    	View tl = null;
    	ImageView iv = (ImageView) group.findViewById(R.id.imageViewMarketGroupArrow);
    	iv.setImageResource(R.drawable.list_arrow_off);
     	if (R.id.linearLayoutMraketPastExpiries == group.getId()) {
     		tl = findViewById(R.id.tableLayoutMarketPastExpiriesOpen);
     	} else if (R.id.linearLayoutMraketAssetInfo == group.getId()) {
     		tl = findViewById(R.id.linearLayoutMraketAssetInfoOpen);
     	} 
     	tl.setVisibility(View.GONE);
	}
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        
        if (null != addedInvestment) {
            outState.putLong(INVESTMENT_SUCCESS_POPUP_ID, addedInvestment.getId());
        }
    }

    @Override
    public void onResume() {
        Log.v("anyoption", "AssetPageActivity.onResume");
        super.onResume();
        if (null != myOptions) {
            myOptions.removeAllRows();
        }
    }

    @Override
    public void onPause() {
        Log.v("anyoption", "AssetPageActivity.onPause");
        super.onPause();
    }

    public void myClickHandler(View target) {
        finish();
    }
    
    @Override
    public void addInvestment(Investment inv) {
        chart.addInvestment(inv);
        if (null != myOptions) {
            myOptions.makeRowForInvestment(inv);
        }
        addedInvestment = inv;
        showInvestmentSuccessPopup();
    }
    
    private void showInvestmentSuccessPopup() {
        AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        User user = ap.getUser();
        Utils.showInsertInvestmentSuccessAlert(context, addedInvestment, user.isCurrencyLeftSymbol(), user.getCurrencySymbol(), new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
                addedInvestment = null;
            }
        });
    }
    
    @Override
    public void removeInvestment(long invId) {
        chart.removeInvestment(invId);
        if (null != myOptions) {
            myOptions.removeInvestment(invId);
        }
    }

    @Override
    protected void showInsurancesButton() {
        Log.v(getLocalClassName(), "Showing insurance button - type: " + insurancesType + " close: " + insuranceCloseTime);
        super.showInsurancesButton();
        if (portrait) {
            if (null != myOptions) {
                myOptions.showInsurancesRow(insurancesType, insuranceCloseTime);
            }
        } else {
            ((AssetPageChartView) chart).setHasInsurances(true);
            InsuranceView iv = (InsuranceView) findViewById(R.id.landscapeInsuranceView);
            responseH.post(new ShowInsuranceRunnable(iv, insurancesType, insuranceCloseTime));
        }
    }

    @Override
    protected void hideInsurancesButton() {
        Log.v("insruance", "Hide insurance button.");
        if (portrait) {
            if (null != myOptions) {
                myOptions.hideInsurancesRow();
            }
        } else {
			((AssetPageChartView) chart).setHasInsurances(false);
			InsuranceView iv = (InsuranceView) findViewById(R.id.landscapeInsuranceView);
			if (null != iv) {
				responseH.post(new SetVisibilityRunnable(iv, View.GONE));
				iv.stopTimeLeftUpdater();
				responseH.post(new SetVisibilityRunnable(findViewById(R.id.assetPageChartTimeLeft), View.VISIBLE));
				responseH.post(new SetVisibilityRunnable(findViewById(R.id.assetPageChartTimeLeftTime), View.VISIBLE));
				responseH.post(new SetVisibilityRunnable(findViewById(R.id.assetPageChartTimeLeftProgress), View.VISIBLE));
			}
        }
    }
    
    class ShowInsuranceRunnable implements Runnable {
        private InsuranceView iv;
        private int insurancesType;
        private long insuranceCloseTime;
        
        public ShowInsuranceRunnable(InsuranceView iv, int insurancesType, long insuranceCloseTime) {
            this.iv = iv;
            this.insurancesType = insurancesType;
            this.insuranceCloseTime = insuranceCloseTime;
        }
        
        public void run() {
            try {
                findViewById(R.id.assetPageChartTimeLeft).setVisibility(View.GONE);
                findViewById(R.id.assetPageChartTimeLeftTime).setVisibility(View.GONE);
                findViewById(R.id.assetPageChartTimeLeftProgress).setVisibility(View.GONE);
    
                iv.initAndStart(insurancesType, insuranceCloseTime);
                iv.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                Log.e("anyoption", "Problem show insurances button.", e);
            }
        }
    }
    
    public long getMarketId() {
    	if (null != market) {
    		return market.getId();
    	}
    	else return 0; // for all markets in case we dont have one
    }
    
    @Override
    public int getInfoPageId() {
    	AnyoptionApplication ap = (AnyoptionApplication) getApplication();
        User user = ap.getUser();
        return (market.isOptionPlusMarket()? R.layout.info_page_option_plus : (user == null  ? R.layout.info_page_binary_logoff: R.layout.info_page_binary_login));
    }
    
    public void navigateToTradePage(View target) { 
    	startActivity(new Intent(this, MarketsGroupActivity.class)); 
    }
}