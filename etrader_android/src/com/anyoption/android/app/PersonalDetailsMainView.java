/**
 * 
 */
package com.anyoption.android.app;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.questionnaire.QuestionnaireMenuItemView;

/**
 * @author AviadH
 * 
 */
public class PersonalDetailsMainView extends MenuViewBase implements Initializable {

	private final String TAG = "FaqView";

	private View lastOpenRow;
	private int arrowRightId;
	private int arrowDownId;

	private Context mContext;

	private PersonalDetailsView personalDetailsView;

	public PersonalDetailsMainView(Context context) {
		super(context, null);
	}

	public PersonalDetailsMainView(Context context, String type) {
		super(context, null);

		initComponent();
	}

	public PersonalDetailsMainView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		initComponent();
	}

	public void initComponent() {
		Log.d(TAG, "InitComponent");
		tlMain.setColumnShrinkable(1, true);

		arrowRightId = R.drawable.list_arrow_off;
		arrowDownId = R.drawable.list_arrow_on;

		TextView groupName = (TextView) menuLine
				.findViewById(R.id.textViewGroupName);

		groupName.setText(getResources().getString(R.string.header_personal));

		personalDetailsView = new PersonalDetailsInnerView(mContext);
		personalDetailsView.setOnMenuStripClickListener(
				new OnMenuStripClickListener() {
					public void onClickCloseAll(MenuViewBase menuViewBase) {
						personalDetailsView.toggleGroup();
					}
				});
		tlMain.addView(personalDetailsView);
		tlMain.addView(new QuestionnaireMenuItemView(mContext));
	}

	public void clickRowLogoutHandler(View target) {
		boolean shouldOpen = false;
		if (lastOpenRow != target) {
			shouldOpen = true;
			if (null != lastOpenRow) {
				closeRow(lastOpenRow);
			}
		} else {
			closeRow(lastOpenRow);
			lastOpenRow = null;
		}
		if (shouldOpen) {
			// Change image to on mode
			ImageView iv = (ImageView) target
					.findViewById(R.id.tradingFaqImage);
			iv.setImageResource(R.drawable.arrow_nav_bottom);
			// set answer visible
			TableRow answer = (TableRow) target.getTag();
			answer.setVisibility(View.VISIBLE);
			if (null != menuScrollView) {
				menuScrollView.post(new ScrollViewScrollRunnable(
						menuScrollView, target));
			}
			lastOpenRow = target;
		}
	}

	private void closeRow(View target) {
		ImageView iv = (ImageView) target.findViewById(R.id.tradingFaqImage);
		iv.setImageResource(R.drawable.arrow_nav_right);
		// set answer and white line visible
		TableRow answer = (TableRow) target.getTag();
		answer.setVisibility(View.GONE);
	}

	@Override
	public void closeGroup() {
		ImageView iv = (ImageView) findViewById(R.id.imageViewGroupArrow);
		iv.setImageResource(arrowRightId);
		tlMain.setVisibility(View.GONE);
	}

	@Override
	public void openGroup() {
		if (menuScrollView == null) {
			menuScrollView = ((BaseMenuActivity) context).getMenuScrollView();
		}
		ImageView iv = (ImageView) findViewById(R.id.imageViewGroupArrow);
		iv.setImageResource(arrowDownId);
		tlMain.setVisibility(View.VISIBLE);
	}

	@Override
	public void removeAllRows() {
		// TODO Auto-generated method stub

	}

	public PersonalDetailsView getPersonalDetailsView() {
		return personalDetailsView;
	}

	public void initialize() {
		personalDetailsView.initMyAccountPersonalDetails();
	}

}
