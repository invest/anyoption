package com.anyoption.android.app;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.SetTextRunnable;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Investment;
import com.anyoption.common.beans.base.MarketRate;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.json.requests.ChartDataMethodRequest;
import com.anyoption.json.results.ChartDataMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

public class ChartView extends View implements ChartDataListener {
//    public static final int chartOffsetLeft = 15;
//    public static final int chartOffsetTop = 15;
//    public static final int chartOffsetBottom = 20;
    
//    public static final int SIZE_TEXT = 11;
    public static final int SIZE_TEXT_CRR_LVL = 20;

    public static final int ALPHA_CLOSED = 0x66;
    public static final int ALPHA_WFE = 0xB2;
    public static final int ALPHA_OPENED = 0xFF;
    
    protected static final int COLOR_BACKGROUND = 0xFFFFFFFF;
    protected static final int COLOR_BORDER = 0xFF033150;
    protected static final int COLOR_TEXT = 0xFF9B9B9B;
    protected static final int COLOR_TEXT_LEVELS = 0xFF093755;
    protected static final int COLOR_CALL_PUT = 0xFFBBBBBB;
    protected static final int COLOR_CHART = 0xFF0284D6;
    protected static final int COLOR_CHART_ETRADER = 0xFF7FB4CB;
    protected static final int COLOR_CRR_LVL_LINES = 0xFF093755;
    protected static final int COLOR_CRR_LVL_SQUARE = 0xFF093755;

//    protected static final int COLOR_EXP_BORDER = 0xB2033150;
//    protected static final int COLOR_EXP_TEXT = 0xB29B9B9B;
//    protected static final int COLOR_EXP_TEXT_LEVELS = 0xB2093755;
//    protected static final int COLOR_EXP_CHART = 0xB20284D6;
//    protected static final int COLOR_EXP_CRR_LVL_LINES = 0xB2093755;
//    protected static final int COLOR_EXP_CRR_LVL_SQUARE = 0xB2093755;
//
    protected static final int COLOR_FADE_BORDER = 0xFFE6E6E6;
    protected static final int COLOR_FADE_TEXT = 0xFFE6E6E6;
    protected static final int COLOR_FADE_TEXT_LEVELS = 0xFFE6E6E6;
    protected static final int COLOR_FADE_CALL_PUT = 0xFFBBBBBB;
    protected static final int COLOR_FADE_CHART = 0xFFE5F2FB;
    protected static final int COLOR_FADE_CRR_LVL_LINES = 0xFFE5F2FB;
    
    protected static final int LENGTH_IN_SEC_REGULAR = 3600;
    protected static final int LENGTH_IN_SEC_OPTION_PLUS = 1800;
    
    protected float secPerPixel;
    protected boolean portrait;
    protected int textSize;
    protected int chartOffsetLeft;
    protected int chartOffsetTop;
    protected int chartOffsetBottom;
    
    protected long marketId;
    protected long oppId;
    protected long oppTypeId;
    protected int oppState;

    protected int timezoneOffset;
    protected Date timeEstClosing;
    protected int scheduled;
    protected int decimalPoint;
    protected ArrayList<MarketRate> rates;
    protected boolean ratesLoaded;
    protected boolean hasPreviousHour;
    protected boolean previousHourLoaded;

    protected Date displayStartTime;
    protected int firstSec;
    protected Date ratesStartTime;
    protected float yMinVal;
    protected float yMaxVal;
    protected double currentLevel;
    protected Date currentLevelTime;
    protected float displayOffset;
    protected boolean call;
    protected boolean put;
    protected Date timeFirstInvest;
    protected Date timeLastInvest;
    protected ArrayList<Investment> investments;
    protected boolean investmentsLoaded;

    protected int crrMin;
    protected int crrSec;
    protected double totalProfit;
    protected double totalAmount;
    
    protected ArrayList<ChartArea> chartAreas;

    private LinearLayout investmentPopup;
    private Handler handler;

    protected Bitmap callIcn;
    protected Bitmap putIcn;
    protected Bitmap zoom;
    protected Bitmap hourPrev;
    protected Bitmap hourNext;
    protected Bitmap bubble;

    protected int chartOffsetRight;

    protected float hourPrevTopPosition;
    protected float hourPrevLeftPosition;
    protected float hourNextTopPosition;
    protected float hourNextLeftPosition;
    
    protected boolean onAssetPage;
    
    protected SlipView slip;
    
    protected Context context;
    
    protected SkinGroup skinGroup;
    
    public ChartView(Context context) {
        this(context, null, 0);
        this.context = context;
    }

    public ChartView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        this.context = context;
    }

    public ChartView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        
        reset();
        
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int orientation = display.getOrientation();
        portrait = orientation == Surface.ROTATION_0 || orientation == Surface.ROTATION_180;
        
        zoom = BitmapFactory.decodeResource(context.getResources(), R.drawable.zoom);
        hourPrev = BitmapFactory.decodeResource(context.getResources(), R.drawable.hour_prev);
        hourNext = BitmapFactory.decodeResource(context.getResources(), R.drawable.hour_next);
        bubble = BitmapFactory.decodeResource(context.getResources(), R.drawable.bubble_option);

        textSize = getResources().getDimensionPixelSize(R.dimen.chartFontSize);
        chartOffsetTop = getResources().getDimensionPixelSize(R.dimen.chartOffsetTop);
        chartOffsetLeft = getResources().getDimensionPixelSize(R.dimen.chartOffsetLeft);
        chartOffsetBottom = getResources().getDimensionPixelSize(R.dimen.chartOffsetBottom);
        
        chartAreas = new ArrayList<ChartArea>();
        
        handler = new Handler();
        
        onAssetPage = false;
        
        AnyoptionApplication application = (AnyoptionApplication) context.getApplicationContext();
        skinGroup = application.getSkins().get(application.getSkinId()).getSkinGroup();
    }

    protected void reset() {
        ratesLoaded = false;
//        waitingForExpiry = false;
        call = false;
        put = false;
        yMinVal = -1;
        yMaxVal = -1;
        investmentsLoaded = false;
        hasPreviousHour = false;
        previousHourLoaded = false;
        oppState = Opportunity.STATE_OPENED;
        currentLevel = 0;
        currentLevelTime = null;
//        currentLevelColor = 0;
    }
    
//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        Log.v("event", "onMeasure - widthMeasureSpec: " + widthMeasureSpec + " heightMeasureSpec: " + heightMeasureSpec);
//        
//        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
//    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.v("event", "onSizeChanged - w: " + w + " h: " + h + " oldw: " + oldw + " oldh: " + oldh);
        
        calcSecPerPixel();
        cleanUnneededRates();
        initDisplay();
        initInvestments();
    }
    
    protected void calcSecPerPixel() {
        secPerPixel = ((float) getLengthInSec()) / (getWidth() - chartOffsetLeft - chartOffsetRight); 
    }

    private static int getUtcOffset() {
        int utcOffset = TimeZone.getDefault().getRawOffset();
        if (TimeZone.getDefault().inDaylightTime(new Date())) {
            // TODO: see if there is a better way to include the summer time
            utcOffset += 3600000;
        }
        return utcOffset;
    }
    
    public static void fixDates(ChartDataMethodResult data) {
        int utcOffset = getUtcOffset();
        if (null != data.getTimeFirstInvest()) {
            data.getTimeFirstInvest().setTime(data.getTimeFirstInvest().getTime() + utcOffset);
        }
        if (null != data.getTimeLastInvest()) {
            data.getTimeLastInvest().setTime(data.getTimeLastInvest().getTime() + utcOffset);
        }
        if (null != data.getTimeEstClosing()) {
            data.getTimeEstClosing().setTime(data.getTimeEstClosing().getTime() + utcOffset);
        }
        
        if (null != data.getInvestments()) {
            Investment[] inva = data.getInvestments();
            for (int i = 0; i < inva.length; i++) {
                inva[i].getTimeCreated().setTime(inva[i].getTimeCreated().getTime() + utcOffset);
            }
        }
        
//        if (null != data.getRates()) {
//            MarketRate[] ra = data.getRates();
//            for (int i = 0; i < ra.length; i++) {
//                ra[i].getRateTime().setTime(ra[i].getRateTime().getTime() + utcOffset);
//            }
//        }
    }
    
    public static void fixDate(Date date) {
        int utcOffset = getUtcOffset();
        date.setTime(date.getTime() + utcOffset);
    }

    public void setChartData(ChartDataMethodResult data) {
        Log.v("data", "setChartData");
        reset();
//        serverTimeOffset = calcPeriodInSec(new Date(), data.getServerTime());
        timezoneOffset = data.getTimezoneOffset();
        marketId = data.getMarketId();
        oppId = data.getOpportunityId();
        oppTypeId = data.getOpportunityTypeId();
        calcSecPerPixel();
        timeFirstInvest = data.getTimeFirstInvest();
        timeLastInvest = data.getTimeLastInvest();
        timeEstClosing = data.getTimeEstClosing();
        scheduled = data.getScheduled();
        decimalPoint = data.getDecimalPoint();

        callIcn = BitmapFactory.decodeResource(getContext().getResources(), oppTypeId == Opportunity.TYPE_REGULAR ? R.drawable.call_small : R.drawable.call_small_plus);
        putIcn = BitmapFactory.decodeResource(getContext().getResources(), oppTypeId == Opportunity.TYPE_REGULAR ? R.drawable.put_small : R.drawable.put_small_plus);
        
        rates = new ArrayList<MarketRate>();
        if (null != data.getRates()) {
            long[] rt = data.getRatesTimes();
            double[] rv = data.getRates();
            for (int i = 0; i < rv.length; i++) {
                rates.add(new MarketRate(new Date(rt[i]), new BigDecimal(rv[i]), new BigDecimal(rv[i]), 0));
            }
//            rates.addAll(Arrays.asList(data.getRates()));
        }
        if (rates.size() > 0) {
            currentLevel = getLevel(rates.get(rates.size() - 1));
        }
        ratesLoaded = true;
        hasPreviousHour = data.isHasPreviousHour();

        cleanUnneededRates();

        investments = new ArrayList<Investment>();
        if (null != data.getInvestments()) {
            Log.v("chart", "Got investments: " + data.getInvestments().length);
            investments.addAll(Arrays.asList(data.getInvestments()));
        } else {
            Log.v("chart", "Got no investments.");
        }
        investmentsLoaded = true;

        initDisplay();
        initInvestments();
        initChartAreas();
        postInvalidate();
    }
    
    protected void initChartAreas() {
        if (chartAreas.size() > 0) {
            chartAreas.clear();
        }

        int onCrrMarket = 0;
        for (int i = 0; i < investments.size(); i++) {
            if (investments.get(i).getMarketId() == marketId) {
                onCrrMarket++;
            }
        }
        if (onCrrMarket > 0) {
            double[] invLvls = new double[onCrrMarket];
            int j = 0;
            for (int i = 0; i < investments.size(); i++) {
                if (investments.get(i).getMarketId() == marketId) {
                    invLvls[j] = investments.get(i).getCurrentLevel();
                    j++;
                }
            }
            sortInvLevels(invLvls);
            
            // make sure the investments are between the lowest and highest values
            if (yMinVal == invLvls[0]) {
                yMinVal = yMinVal * 0.999f;
            }
            if (yMaxVal == invLvls[invLvls.length - 1]) {
                yMaxVal = yMaxVal * 1.001f;
            }

            double btm = yMinVal;
            for (int i = 0; i < invLvls.length; i++) {
                chartAreas.add(new ChartArea((float) btm, (float) invLvls[i], ChartArea.isProfitable(btm, invLvls[i], investments, marketId) ? ChartArea.TYPE_PROFITABLE : ChartArea.TYPE_NOT_PROFITABLE, false));
                btm = invLvls[i];
            }
            chartAreas.add(new ChartArea((float) btm, yMaxVal, ChartArea.isProfitable(btm, yMaxVal, investments, marketId) ? ChartArea.TYPE_PROFITABLE : ChartArea.TYPE_NOT_PROFITABLE, true));
        }
    }
    
    protected void initInvestments() {
        if (!investmentsLoaded) {
            return;
        }
        Investment inv = null;
        for (int i = 0; i < investments.size(); i++) {
            inv = investments.get(i);
            inv.setChartXPosition((calcPeriodInSec(ratesStartTime, inv.getTimeCreated()) - firstSec) / secPerPixel);
        }
    }
    
    protected void sortInvLevels(double[] lvls) {
        if (lvls.length < 2) {
            return;
        }
        
        double tmp;
        boolean swapped;
        do {
            swapped = false;
            for (int i = 0; i < lvls.length - 1; i++) {
                if (lvls[i] > lvls[i + 1]) {
                    tmp = lvls[i];
                    lvls[i] = lvls[i + 1];
                    lvls[i + 1] = tmp;
                    swapped = true;
                }
            }
        } while (swapped);
    }
    
    protected float getLevel(MarketRate rate) {
        return  scheduled == 1 ? rate.getGraphRate().floatValue() : rate.getDayRate().floatValue();
    }
    
    protected void initDisplay() {
        if (!ratesLoaded || oppId == 0) {
            return;
        }
        displayStartTime = new Date(timeFirstInvest.getTime());
        Date ct = new Date();
        Utils.fixTime(ct);
        int timePassed = calcPeriodInSec(displayStartTime, ct);
        int oppLength = calcPeriodInSec(displayStartTime, timeEstClosing);
        Log.v("init", "timePassed: " + timePassed + " oppLength: " + oppLength + " _root.oppState: " + oppState);
        if (timePassed > 3600 || oppLength < 3600 || oppState >= 3) {
            Log.v("init", "Scroll...");
            displayStartTime = new Date(timeEstClosing.getTime());
            do {
                displayStartTime.setTime(displayStartTime.getTime() - getLengthInSec() * 1000);
            } while (displayStartTime.getTime() > ct.getTime());
        } else {
            Calendar c = Calendar.getInstance();
            c.setTime(displayStartTime);
            if (c.get(Calendar.MINUTE) % 10 != 0) {
                displayStartTime.setTime(displayStartTime.getTime() - (c.get(Calendar.MINUTE) % 10) * 60000);
            }
        }
//        if (rates.size() > 0 && rates.get(0).getRateTime().getTime() > displayStartTime.getTime()) {
//            Log.v("init", "Calculated start time before history start.");
//            while (displayStartTime.getTime() + 600000 <= rates.get(0).getRateTime().getTime()) {
//                Log.v("chart", "displayStartTime: " + displayStartTime + " rates.get(0).getTime(): " + rates.get(0).getRateTime() + " adding 10 min...");
//                displayStartTime.setTime(displayStartTime.getTime() + 600000);
//            }
//        }
//        displayOffset = (calcPeriodInSec(timeFirstInvest, displayStartTime) - firstSec) / secPerPixel;
        displayOffset = (calcPeriodInSec(ratesStartTime, displayStartTime) - firstSec) / secPerPixel;
        Log.v("init", "ratesStartTime: " + ratesStartTime + " displayStartTime: " + displayStartTime + " displayOffset: " + displayOffset);
    }
    
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        
        int width = getWidth();
        int height = getHeight();
        Paint p = new Paint();
        
        // clear the chart
        p.setStyle(Paint.Style.FILL);
        p.setColor(getColor(COLOR_BACKGROUND));
        canvas.drawRect(0, 0, width, height, p);
        
        onDrawBackground(canvas, p, chartOffsetLeft, chartOffsetTop, width - chartOffsetLeft/* - chartOffsetRight*/, height - chartOffsetTop - chartOffsetBottom);
        
        // border
        p.setStyle(Paint.Style.STROKE);
        p.setColor(getColor(COLOR_BORDER));
        canvas.drawLine(chartOffsetLeft, chartOffsetTop, chartOffsetLeft, height - chartOffsetBottom, p);
        canvas.drawLine(chartOffsetLeft, height - chartOffsetBottom, width, height - chartOffsetBottom, p);

        // X axis
        float xLabelsSpacing = (float) (width - chartOffsetLeft - chartOffsetRight) / 3;
        for (int i = 0; i < 4; i++) {
            canvas.drawLine(chartOffsetLeft + i * xLabelsSpacing, height - chartOffsetBottom + 1, chartOffsetLeft + i * xLabelsSpacing, height - chartOffsetBottom + 6, p);
        }

        if (ratesLoaded && oppId != 0) {
            p.setColor(getColor(COLOR_TEXT));
            p.setTextAlign(Paint.Align.CENTER);
            p.setTextSize(textSize);
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(displayStartTime.getTime());
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            for (int i = 0; i < 4; i++) {
                canvas.drawText(sdf.format(c.getTime()), chartOffsetLeft + i * xLabelsSpacing, height - 5, p);
                c.add(Calendar.SECOND, getLengthInSec() / 3);
            }

            float ySpread = yMaxVal - yMinVal;
            
            Date ct = new Date();
            Utils.fixTime(ct);
            if (null != rates && rates.size() > 0 && ct.getTime() < rates.get(rates.size() - 1).getRateTime().getTime()) {
                // if it happens that current local time is before last time from the history adjust it so the
                // cursor is not before the end of the chart
                ct = rates.get(rates.size() - 1).getRateTime();
            }
            int secFromStart = calcPeriodInSec(ratesStartTime, ct) - firstSec;
            float crrX = chartOffsetLeft + secFromStart / secPerPixel - displayOffset;
        //  if (!getCanScrollForward() && crrX > paintArea._x + paintArea._width) {
        //      crrX = paintArea._x + paintArea._width;
        //  }
            float crrY = chartOffsetTop + (height - chartOffsetTop - chartOffsetBottom) * (1 - ((float) currentLevel - yMinVal) / ySpread);

            if (call || put) {
                p.setColor(getColor(COLOR_CALL_PUT));
                if (call) {
                    callPutRect(canvas, p, chartOffsetLeft + 1, chartOffsetTop, width - chartOffsetLeft - chartOffsetRight - 2, crrY - chartOffsetTop - 2, 5);
                    if (crrY - chartOffsetTop > 15) {
                        canvas.drawText("CALL", (width - chartOffsetLeft - chartOffsetRight) / 2, chartOffsetTop + (crrY - chartOffsetTop)/ 2 + 5, p);
                    }
                }
                if (put) {
                    callPutRect(canvas, p, chartOffsetLeft + 1, crrY + 1, width - chartOffsetLeft - chartOffsetRight - 2, height - chartOffsetBottom - crrY - 2, 5);
                    if (height - chartOffsetBottom - crrY > 15) {
                        canvas.drawText("PUT", (width - chartOffsetLeft - chartOffsetRight) / 2, crrY + (height - chartOffsetBottom - crrY) / 2 + 5, p);
                    }
                }
            }

            p.setColor(getColor(Utils.isEtraderProject(getContext()) ? COLOR_CHART_ETRADER : COLOR_CHART));
            p.setStrokeWidth(2);
            if (rates.size() > 0) {
                MarketRate prev = null;
                MarketRate crr = null;
                for (int i = 0; i < rates.size(); i++) {
                    crr = rates.get(i);
                    if (displayOffset <= crr.getChartXPosition() && crr.getChartXPosition() - displayOffset <= width - chartOffsetLeft - chartOffsetRight) {
                        if (null != prev) {
                            float x1 = chartOffsetLeft + prev.getChartXPosition() - displayOffset;
                            float y1 = chartOffsetTop + (height - chartOffsetTop - chartOffsetBottom) * (1 - (getLevel(prev) - yMinVal) / ySpread);
                            float x2 = chartOffsetLeft + crr.getChartXPosition() - displayOffset;
                            float y2 = chartOffsetTop + (height - chartOffsetTop - chartOffsetBottom) * (1 - (getLevel(crr) - yMinVal) / ySpread);
                            onDrawChartSegment(canvas, p, x1, y1, x2, y2, height - chartOffsetTop - chartOffsetBottom, ySpread);
                        }
                        prev = crr;
                    }
                }
                // connect the chart with the cursor
                if (null != prev && crrX < width - chartOffsetRight - 1) {
                    float x1 = chartOffsetLeft + prev.getChartXPosition() - displayOffset;
                    float y1 = chartOffsetTop + (height - chartOffsetTop - chartOffsetBottom) * (1 - (getLevel(prev) - yMinVal) / ySpread);
                    canvas.drawLine(x1, y1, crrX, y1, p);
                    canvas.drawLine(crrX, y1, crrX, crrY, p);
                }
            }
        
            // cursor
            p.setColor(getColor(COLOR_CRR_LVL_LINES));
            p.setStrokeWidth(1);
            dashTo(canvas, p, chartOffsetLeft, crrY, width - chartOffsetRight, crrY, 1, 4); // horizontal
            if (crrX < width - chartOffsetRight) {
                dashTo(canvas, p, crrX, chartOffsetTop + 1, crrX, height - chartOffsetBottom, 1, 4); // vertical

//                if (!portrait) {
                    p.setStyle(Paint.Style.FILL);
                    p.setColor(getColor(COLOR_CRR_LVL_SQUARE));
                    canvas.drawRect(crrX - 3, crrY - 3, crrX + 3, crrY + 3, p);
//                }
            }

            // back arrow
            if (!portrait && canScrollBack() && (oppState == Opportunity.STATE_OPENED || oppState == Opportunity.STATE_LAST_10_MIN)) {
                hourPrevLeftPosition = 5;
                hourPrevTopPosition = chartOffsetTop + (height - chartOffsetTop - chartOffsetBottom) / 2 - hourPrev.getHeight() / 2;
                canvas.drawBitmap(hourPrev, hourPrevLeftPosition, hourPrevTopPosition, null);
            }

            // forward arrow
            if (!portrait && canScrollForward()) {
                hourNextLeftPosition = width - chartOffsetRight + 10 - hourNext.getWidth();
                hourNextTopPosition = chartOffsetTop + (height - chartOffsetTop - chartOffsetBottom) / 2 - hourNext.getHeight() / 2;
                canvas.drawBitmap(hourNext, hourNextLeftPosition, hourNextTopPosition, null);
            }
            
            DecimalFormat df = new DecimalFormat(String.format(Locale.US, "#,##0.%1$0" + decimalPoint + "d", 0));
            p.setStyle(Paint.Style.FILL);
            p.setColor(getColor(COLOR_TEXT_LEVELS));
            p.setTextAlign(Paint.Align.LEFT);
            p.setTextSize(textSize);
            p.setStrokeWidth(1);
            canvas.drawText(df.format(yMaxVal), chartOffsetLeft + 5, chartOffsetTop + 10, p);
            canvas.drawText(df.format(yMinVal), chartOffsetLeft + 5, height - chartOffsetBottom - 3, p);
            if (!portrait) {
                Investment inv = null;
                for (int i = 0; i < investments.size(); i++) {
                    inv = investments.get(i);
                    float invX = chartOffsetLeft + inv.getChartXPosition() - displayOffset;
                    float invY = chartOffsetTop + (float) ((height - chartOffsetTop - chartOffsetBottom) * (yMaxVal - inv.getCurrentLevel()) / ySpread);
                    drawInvestmentIcon(canvas, p, invX, invY, inv.getTypeId());
                    if (oppTypeId == Opportunity.TYPE_OPTION_PLUS && oppState >= Opportunity.STATE_OPENED && oppState <= Opportunity.STATE_LAST_10_MIN) {
                        canvas.drawBitmap(bubble, invX - bubble.getWidth() / 2, invY - callIcn.getHeight() / 2 - bubble.getHeight(), null);
                    }
                    canvas.drawText(df.format(inv.getCurrentLevel()), hourPrevLeftPosition + hourPrev.getWidth() + 5, invY + 5, p);
                }
            }
        }
    }
    
    protected void onDrawBackground(Canvas canvas, Paint paint, int x, int y, int width, int height) {
        if (portrait) {
            if ((null == investments || investments.size() == 0) && chartOffsetRight > 0) {
                canvas.drawBitmap(zoom,
                        chartOffsetLeft + (width - chartOffsetRight - zoom.getWidth()) / 2,
                        chartOffsetTop + (height - zoom.getHeight()) / 2,
                        paint);
            }
        } else {
            for (int i = 0; i < chartAreas.size(); i++) {
                chartAreas.get(i).onDraw(canvas, paint, x, y, width, height, yMinVal, yMaxVal, currentLevel, decimalPoint, false, getChartAlpha() << 24 | 0x00FFFFFF);
            }
        }
    }
    
    protected void onDrawChartSegment(Canvas canvas, Paint paint, float x1, float y1, float x2, float y2, int height, float ySpread) {
//        Log.v("draw", "x1: " + x1 + " y1: " + y1 + " x2: " + x2 + " y2: " + y2);
        // TODO: this implementation assume there is no more than 1 investment on segment
        Investment inv = null;
        for (int i = 0; i < investments.size(); i++) {
            inv = investments.get(i);
            float invX = chartOffsetLeft + inv.getChartXPosition() - displayOffset;
            if (x1 < invX && invX <= x2) {
                float invY = (float) (height * (1 - (inv.getCurrentLevel() - yMinVal) / ySpread));
                canvas.drawLine(x1, y1, invX, y1, paint);
                canvas.drawLine(invX, y1, invX, invY, paint);
                canvas.drawLine(invX, invY, x2, invY, paint);
                canvas.drawLine(x2, invY, x2, y2, paint);
                return;
            }
        }
//        canvas.drawLine(x1, y1, x2, y1, paint);
//        canvas.drawLine(x2, y1, x2, y2, paint);
        canvas.drawLine(x1, y1, x2, y2, paint);
    }
    
    protected void drawInvestmentIcon(Canvas canvas, Paint paint, float x, float y, long type) {
        if (type == Investment.INVESTMENT_TYPE_CALL) {
            canvas.drawBitmap(callIcn, x - callIcn.getWidth() / 2, y - callIcn.getHeight() / 2, paint);
        } else {
            canvas.drawBitmap(putIcn, x - putIcn.getWidth() / 2, y - putIcn.getHeight() / 2, paint);
        }
    }

    public int getChartAlpha() {
        switch (oppState) {
        case Opportunity.STATE_OPENED:
        case Opportunity.STATE_LAST_10_MIN:
            return ALPHA_OPENED;
        case Opportunity.STATE_CLOSING_1_MIN:
        case Opportunity.STATE_CLOSING:
            return ALPHA_WFE;
        default:
            return ALPHA_CLOSED;
        }
    }
    
    private int getColor(int color) {
        if (oppState != Opportunity.STATE_OPENED && oppState != Opportunity.STATE_LAST_10_MIN) {
            return color & BaseChartView.CLEAR_ALPHA_MASK | (getChartAlpha() << 24);
        } else if (!portrait || null == investments || investments.size() == 0) {
            return color;
        }
        switch (color) {
        case COLOR_BORDER:
            return COLOR_FADE_BORDER;
        case COLOR_TEXT:
            return COLOR_FADE_TEXT;
        case COLOR_TEXT_LEVELS:
            return COLOR_FADE_TEXT_LEVELS;
        case COLOR_CHART:
            return COLOR_FADE_CHART;
        case COLOR_CHART_ETRADER:
            return COLOR_FADE_CHART;
        case COLOR_CALL_PUT:
            return COLOR_FADE_CALL_PUT;
//        case COLOR_CRR_LVL_LINES:
//            return COLOR_FADE_CRR_LVL_LINES;
        }
        return color;
    }

    protected void callPutRect(Canvas canvas, Paint p, float startx, float starty, float width, float height, int gap) {
        for (int i = 0; i < width + height; i += gap) {
            canvas.drawLine(startx + (i > height ? i - height : 0), starty + (i < height ? i : height), startx + (i < width ? i : width), starty + (i > width ? i - width : 0), p);
        }
    }
    
    public static void dashTo(Canvas canvas, Paint p, float startX, float startY, float endX, float endY, int dash, int gap) {
        p.setStyle(Paint.Style.STROKE);
        p.setPathEffect(new DashPathEffect(new float[] {1, 4}, 0));
        Path path = new Path();
        path.moveTo(startX, startY);
        path.lineTo(endX, endY);
        canvas.drawPath(path, p);
        p.setPathEffect(null);
    }
    
    protected void cleanUnneededRates() {
        if (!ratesLoaded || oppId == 0) {
            return;
        }
        Date crrt = new Date();
        Utils.fixTime(crrt);
        ratesStartTime = new Date(timeEstClosing.getTime());
        do {
            ratesStartTime.setTime(ratesStartTime.getTime() - getLengthInSec() * 1000); // 1 opp period
        } while (crrt.getTime() < ratesStartTime.getTime());
        if (rates.size() > 0 && rates.get(0).getRateTime().getTime() < ratesStartTime.getTime()) { // if there is previous hour in the history
            ratesStartTime.setTime(ratesStartTime.getTime() - getLengthInSec() * 1000); // 1 opp period
        }

        Log.v("init", "Cleaning old hour rates - length: " + rates.size() + " yMinVal: " + yMinVal + " yMaxVal: " + yMaxVal + " startTime: " + ratesStartTime);
        while (rates.size() > 0 && rates.get(0).getRateTime().getTime() < ratesStartTime.getTime()) {
            rates.remove(0);
        }
        MarketRate r = null;
//        for (int i = 0; i < rates.size(); i++) {
//            r = rates.get(i);
//            if (getLevel(r) == 0) {
//                rates.remove(i);
//                i--;
//            }
//        }
        for (int i = 0; i < rates.size(); i++) {
            r = rates.get(i);
            if (i == 0) {
                Calendar c = Calendar.getInstance();
                c.setTime(r.getRateTime());
                firstSec = c.get(Calendar.SECOND);
                yMinVal = yMaxVal = getLevel(r);
            }
            r.setChartXPosition((calcPeriodInSec(ratesStartTime, r.getRateTime()) - firstSec) / secPerPixel);
            if (getLevel(r) < yMinVal) {
                yMinVal = getLevel(r);
            }
            if (getLevel(r) > yMaxVal) {
                yMaxVal = getLevel(r);
            }
        }

        if (rates.size() > 0) {
            float offset = (yMaxVal - yMinVal) * 0.02f;
            if (offset == 0) {
                offset = yMaxVal * 0.001f;
            }            
            yMinVal = yMinVal - offset;
            yMaxVal = yMaxVal + offset;
        } else {
            yMinVal = -1;
            yMaxVal = -1;
        }
        Log.v("init", "After cleaning - length: " + rates.size() + " yMinVal: " + yMinVal + " yMaxVal: " + yMaxVal + " startTime: " + ratesStartTime);
    }

    public void updateItem(int itemPos, String itemName, UpdateInfo update) {
        if (!ratesLoaded) {
            return;
        }
        String level = update.getNewValue(skinGroup.getLevelUpdateKey());
        String state = update.getNewValue("ET_STATE");
        String time = update.getNewValue("AO_TS");
        Log.v("data", "level: " + level + " state: " + state + " time: " + time);

        if (update.isValueChanged("ET_STATE")) {
            oppState = Integer.parseInt(state);
        }
  
        double pLevel = Double.parseDouble(level.replaceAll(",", ""));
        if (pLevel == 0 || oppState < Opportunity.STATE_OPENED || oppState > Opportunity.STATE_CLOSED/* || (waitingForExpiry && oppState == Opportunity.STATE_CLOSED)*/) {
            postInvalidate();
            return;
        }
        currentLevel = pLevel;
        currentLevelTime = new Date(Long.parseLong(time));
        if (yMinVal == -1 || pLevel < yMinVal) {
            yMinVal = (float) currentLevel;
        }
        if (yMaxVal == -1 || pLevel > yMaxVal) {
            yMaxVal = (float) currentLevel;
        }
        
        updateCurrentReturn();
        
        if (null != investmentPopup) {
            try {
                Investment inv = (Investment) investmentPopup.getTag();
                handler.post(new SetTextRunnable((TextView) investmentPopup.findViewById(R.id.textViewInvestmentCurrerntLevelNum), level));
                handler.post(new SetTextRunnable((TextView) investmentPopup.findViewById(R.id.textViewInvestmentCurrerntReturnNum),
                        Utils.formatCurrencyAmount(Utils.getInvestmentReturnAt(inv, currentLevel), slip.getCurrencySymbol(), slip.isCurrencyLeftSymbol(), slip.getCurrencyId())));
            } catch (Exception e) {
                // do nothing. most likely NPE the popup can be closed at anytime.
            }
        }

        ensureYOffset();
        takeRateSample();
        postInvalidate();
    }

    protected void updateCurrentReturn() {
        totalProfit = 0;
        totalAmount = 0;
        if (null != investments) {
            for (int i = 0; i < investments.size(); i++) {
                if (investments.get(i).getMarketId() == marketId) {
                    totalProfit += Utils.getInvestmentReturnAt(investments.get(i), currentLevel);
                    totalAmount += investments.get(i).getAmount();
                }
            }
        }
    }
    
    protected void takeRateSample() {
        if (rates.size() == 0 || (currentLevelTime.getTime() - rates.get(rates.size() - 1).getRateTime().getTime()) / 1000 > secPerPixel) {
            int secFromStart = calcPeriodInSec(ratesStartTime, currentLevelTime) - firstSec;
            Date t = new Date();
            t.setTime(currentLevelTime.getTime());
            rates.add(new MarketRate(t, new BigDecimal(currentLevel), new BigDecimal(currentLevel), secFromStart / secPerPixel));
        }
    }

    protected void ensureYOffset() {
        // don't let the current level line go too close to the top/bottom of the chart
        float tooClose = (yMaxVal - yMinVal) * 0.07f;
        if (tooClose == 0) {
            tooClose = yMinVal * 0.0001f;
        }
        if (currentLevel < yMinVal + tooClose) {
            float newMinVal = Math.min(yMinVal, (float) currentLevel);
            yMinVal = newMinVal - (yMaxVal - newMinVal) * 0.07f;
            if (yMinVal == currentLevel) {
                yMinVal = yMinVal - tooClose;
            }
            if (chartAreas.size() > 0) {
                chartAreas.get(0).setBottomLevel(yMinVal);
            }
        }
        if (currentLevel > yMaxVal - tooClose) {
            float newMaxVal = Math.max(yMaxVal, (float) currentLevel);
            yMaxVal = newMaxVal + (newMaxVal - yMinVal) * 0.07f;
            if (yMaxVal == currentLevel) {
                yMaxVal = yMaxVal + tooClose;
            }
            if (chartAreas.size() > 0) {
                chartAreas.get(chartAreas.size() - 1).setTopLevel(yMaxVal);
            }
        }
    };
    
    protected boolean canScrollBack() {
        return hasPreviousHour && displayStartTime.getTime() >= timeFirstInvest.getTime();
    }

    protected boolean canScrollForward() {
        return hasPreviousHour && displayStartTime.getTime() < timeFirstInvest.getTime();
//        Date ct = new Date();
//        fixTime(ct);
//        if (ct.getTime() > timeEstClosing.getTime()) {
//            ct.setTime(timeEstClosing.getTime());
//        }
//        // if the display start time is more than an hour old and the closing is not late
//        // we must be showing the previous hour
//        ct.setTime(ct.getTime() - getLengthInSec() * 1000); // 1 opp period
//        if (displayStartTime.getTime() < ct.getTime()) {
//            return true;
//        }
//        return false;
    }

    protected void scrollBack() {
        displayStartTime.setTime(displayStartTime.getTime() - getLengthInSec() * 1000); // 1 opp period
        displayOffset = (calcPeriodInSec(ratesStartTime, displayStartTime) - firstSec) / secPerPixel;
        invalidate();
        if (!previousHourLoaded) {
            previousHourLoaded = true;
            ChartDataMethodRequest request = new ChartDataMethodRequest();
            request.setMarketId(marketId);
            request.setOpportunityId(oppId);
            Utils.requestService(this, handler, "chartDataPreviousHourCallBack", request, new ChartDataMethodResult(), "getChartDataPreviousHour", (Activity) getContext());
        }
    }

    protected void scrollForward() {
        displayStartTime.setTime(displayStartTime.getTime() + getLengthInSec() * 1000); // 1 opp period
        displayOffset = (calcPeriodInSec(ratesStartTime, displayStartTime) - firstSec) / secPerPixel;
        invalidate();
    }

    public void chartDataPreviousHourCallBack(Object resultObj) {
        Log.d("chart", "chartDataPreviousHourCallBack");
        ChartDataMethodResult result = (ChartDataMethodResult) resultObj;
        if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
            fixDates(result);
            long[] rt = result.getRatesTimes();
            double[] rv = result.getRates();
            for (int i = 0; i < rv.length; i++) {
                rates.add(i, new MarketRate(new Date(rt[i]), new BigDecimal(rv[i]), new BigDecimal(0), 0));
            }
//            rates.addAll(0, Arrays.asList(result.getRates()));
            cleanUnneededRates();
            initInvestments();
            initChartAreas();
            displayOffset = (calcPeriodInSec(ratesStartTime, displayStartTime) - firstSec) / secPerPixel;
            Log.v("chart", "displayOffset: " + displayOffset);
            postInvalidate();
        } else {
            previousHourLoaded = false;
        }
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	final float x = event.getX(); 
        final float y = event.getY(); 
        if(event.getAction() == MotionEvent.ACTION_DOWN) { 
            if (!portrait && investmentsLoaded) {
                int height = getHeight();
                float ySpread = yMaxVal - yMinVal;
                int icnhw = callIcn.getWidth()/* / 2*/;
                int icnhh = callIcn.getHeight()/* / 2*/;
                Investment inv = null;
                for (int i = 0; i < investments.size(); i++) {
                    inv = investments.get(i);
                    float invX = chartOffsetLeft + inv.getChartXPosition() - displayOffset;
                    float invY = (float) ((height - chartOffsetTop - chartOffsetBottom) * (1 - (inv.getCurrentLevel() - yMinVal) / ySpread));
                    int bubbleHeight = oppTypeId == Opportunity.TYPE_OPTION_PLUS && oppState >= Opportunity.STATE_OPENED && oppState <= Opportunity.STATE_LAST_10_MIN ? bubble.getHeight() : 0;
                    if (invX - icnhw <= x && x <= invX + icnhw && invY - icnhh - bubbleHeight <= y && y <= invY + icnhh) {
                        Log.v("chart", "Hit the icon of investment " + inv.getId());
                        if (oppTypeId == Opportunity.TYPE_REGULAR || bubbleHeight == 0) {
                            investmentPopup = MyOptionView.tableRowClickPopUp(inv, false, getContext(),
                                    (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE),
                                    new DialogInterface.OnDismissListener() {
                                        public void onDismiss(DialogInterface dialog) {
                                            investmentPopup = null;
                                        }
                                    });
                        } else {
                            Intent intent = new Intent(getContext(), OptionPlusQuoteActivity.class); 
                            intent.putExtra("Investment", inv);
                            ((Activity) getContext()).startActivityForResult(intent, Constants.REQUEST_CODE_GET_QUOTE);
                        }
                        return true;
                    }
                }
                if (hasPreviousHour) {
//                    Log.v("chart", "check for prev hour hit - " + prevHourLeftPosition + " " + prevHourTopPosition);
                    if (canScrollBack() && hourPrevLeftPosition <= x && x <= hourPrevLeftPosition + hourPrev.getWidth() && hourPrevTopPosition <= y && y <= hourPrevTopPosition + hourPrev.getHeight()) {
                        Log.v("chart", "Hit prev hour icon.");
                        scrollBack();
                        return true;
                    }
                    if (canScrollForward() && hourNextLeftPosition <= x && x <= hourNextLeftPosition + hourNext.getWidth() && hourNextTopPosition <= y && y <= hourNextTopPosition + hourNext.getHeight()) {
                        Log.v("chart", "Hit next hour icon.");
                        scrollForward();
                        return true;
                    }
                }
            }
        } 
        return super.onTouchEvent(event);
    }

    /**
     * Calculate period length in seconds.
     * 
     * @param periodStart
     * @param periodEnd
     * @return
     */
    protected int calcPeriodInSec(Date periodStart, Date periodEnd) {
        return Math.round((periodEnd.getTime() - periodStart.getTime()) / 1000);
    }

//    public boolean isWaitingForExpiry() {
//        return waitingForExpiry;
//    }

	public void setChartSlip(SlipView slip) {
	    this.slip = slip;
	}

    public double getTotalProfit() {
        return totalProfit;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    protected int getLengthInSec() {
        return oppTypeId == Opportunity.TYPE_REGULAR ? LENGTH_IN_SEC_REGULAR : LENGTH_IN_SEC_OPTION_PLUS;
    }
    
    public void addInvestment(Investment inv) {
        fixDate(inv.getTimeCreated());
        investments.add(inv);
        initInvestments();
        initChartAreas();
        updateCurrentReturn();
    }
    
    public void removeInvestment(long invId) {
        for (int i = 0; i < investments.size(); i++) {
            if (investments.get(i).getId() == invId) {
                investments.remove(i);
                break;
            }
        }
        updateCurrentReturn();
        initChartAreas();
    }
    
    public int getInvestmentsCount() {
        return investments.size();
    }
    
    public void closeSlip() {
        call = false;
        put = false;
        postInvalidate();
    }

    public long getMarketId() {
        return marketId;
    }

    public int getChartOffsetRight() {
        return chartOffsetRight;
    }

    public void setChartOffsetRight(int chartOffsetRight) {
        this.chartOffsetRight = chartOffsetRight;
    }

    public boolean isPortrait() {
        return portrait;
    }

    public boolean isOnAssetPage() {
        return onAssetPage;
    }

    public void setOnAssetPage(boolean onAssetPage) {
        this.onAssetPage = onAssetPage;
        zoom = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.rotate);
    }
    
    public int getOppState() {
        return oppState;
    }

    public long getOppTypeId() {
        return oppTypeId;
    }
    
    public boolean isCall() {
        return call;
    }

    public void setCall(boolean call) {
        this.call = call;
        postInvalidate();
    }

    public boolean isPut() {
        return put;
    }

    public void setPut(boolean put) {
        this.put = put;
        postInvalidate();
    }
}