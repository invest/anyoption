package com.anyoption.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;

/**
 * @author IdanZ
 * 
 */
public class DetailsView extends MenuViewBase {

	private WebView webView;
	String from;
	private TableRow content;

	public DetailsView(Context context) {
		this(context, null);
	}

	public DetailsView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initComponent(attrs);
	}

	public void initComponent(AttributeSet attrs) {
		Log.d("DetailsView", "init Component");
		content = (TableRow) inflater.inflate(R.layout.web_view, null,
				false);
		AnyoptionApplication ap = (AnyoptionApplication) ((Activity) context)
				.getApplication();
		webView = (WebView) content.findViewById(R.id.webViewGeneralTerms);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setPluginsEnabled(true);
		webView.setBackgroundColor(0);

		TypedArray attributes = context.obtainStyledAttributes(attrs,
				R.styleable.faqView);
		
		from = attributes.getString(R.styleable.faqView_menuType);
		TextView groupName = (TextView)menuLine.findViewById(R.id.textViewGroupName);
		groupName.setText(getMenuStripText());
		String web_site_link = Utils.getStringFromResource(context, Constants.WEB_SITE_LINK, null);
		if (from.equals(Constants.PUT_EXTRA_GENERAL_TERMS)) {
			tlMain.addView(new GeneralTermsInnerView(context, context.getString(R.string.general_terms_annex_generalterms), "#General"));
			tlMain.addView(new GeneralTermsInnerView(context, context.getString(R.string.general_terms_annex_clientsdeclaration), "#Declaration"));
			tlMain.addView(new GeneralTermsInnerView(context, context.getString(R.string.general_terms_annex_representative), "#Representative"));
			if(Utils.isRegulated(context)){
				tlMain.addView(new GeneralTermsInnerView(context, context.getString(R.string.general_terms_annex_compensation), "#Fund"));
				tlMain.addView(new GeneralTermsInnerView(context, context.getString(R.string.general_terms_annex_orderexecution), "#Execution"));
				tlMain.addView(new GeneralTermsInnerView(context, context.getString(R.string.general_terms_annex_conflictofinterest), "#Conflict"));
				tlMain.addView(new GeneralTermsInnerView(context, context.getString(R.string.general_terms_annex_clientcomplaint), "#Complaint"));
				tlMain.addView(new GeneralTermsInnerView(context, context.getString(R.string.general_terms_annex_clientcategorization), "#Categorization"));
			}
			tlMain.addView(new GeneralTermsInnerView(context, context.getString(R.string.general_terms_annex_expirylevels), "#Calculations"));

			tlMain.setVisibility(View.GONE);
		} else if (from.equals(Constants.PUT_EXTRA_ABOUTUS)) {
			if (Utils.isEtraderProject(context)){
				webView.loadUrl(web_site_link + "aboutus.jsf?fromApp=true");
			} else { 
				webView.loadUrl(web_site_link + "/aboutus_content.jsf?fromApp=true&s=" + ap.getSkinId());
			}			
		} else if (from.equals(Constants.PUT_EXTRA_SECURITY)) {
			if (Utils.isEtraderProject(context)){
				webView.loadUrl(web_site_link + "security.jsf?fromApp=true");
			} else {
				webView.loadUrl(web_site_link + "/security_content.jsf?fromApp=true&s=" + ap.getSkinId());
			}
		} else if (from.equals(Constants.PUT_EXTRA_PRIVACY)) {
			if (Utils.isEtraderProject(context)){
				webView.loadUrl(web_site_link + "privacy.jsf?fromApp=true");
			} else {
				webView.loadUrl(web_site_link + "/privacy_content.jsf?fromApp=true&s=" + ap.getSkinId());
			}
		} else if (from.equals(Constants.PUT_EXTRA_RISK_DISCLOSURE)){
			webView.loadUrl(web_site_link + "/risk_disclosure_content.jsf?fromApp=true&s=" + ap.getSkinId());
		}

		// set table params
		tlMain.setColumnStretchable(0, true);
		tlMain.setColumnShrinkable(1, true);

		if(!from.equals(Constants.PUT_EXTRA_GENERAL_TERMS)){
		tlMain.addView(content, new TableRow.LayoutParams(
				TableRow.LayoutParams.FILL_PARENT,
				TableRow.LayoutParams.FILL_PARENT));
			tlMain.setVisibility(View.VISIBLE);
		}
	}

	public String getMenuStripText() {
		if (from == null) {
			return "";
		}
		if (from.equals(Constants.PUT_EXTRA_GENERAL_TERMS)) {
			return getResources().getString(R.string.footer_general_terms);
		} else if (from.equals(Constants.PUT_EXTRA_ABOUTUS)) {
			return getResources().getString(R.string.about_us_header);
		} else if (from.equals(Constants.PUT_EXTRA_SECURITY)) {
			return getResources().getString(R.string.security_header);
		} else if (from.equals(Constants.PUT_EXTRA_RISK_DISCLOSURE)){
			return getResources().getString(R.string.general_terms_riskdisclosure);
		}
		return getResources().getString(R.string.footer_privacy); // put_extra_privacy
	}

	@Override
	public void openGroup() {
		ImageView iv = (ImageView) findViewById(R.id.imageViewGroupArrow);
		iv.setImageResource(R.drawable.list_arrow_on);
		if(webView != null){
			webView.setVisibility(View.VISIBLE);
		}
		tlMain.setVisibility(View.VISIBLE);
	}

	@Override
	public void closeGroup() {
		ImageView iv = (ImageView) findViewById(R.id.imageViewGroupArrow);
		iv.setImageResource(R.drawable.list_arrow_off);
		if (null != webView) {
			webView.stopLoading();
			webView.setVisibility(View.GONE);
		} 
		tlMain.setVisibility(View.GONE);
		
	}

	@Override
	public void removeAllRows() {
	}
}