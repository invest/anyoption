package com.anyoption.android.app;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.anyoption.android.app.util.Utils;

public class PageControl extends View {
    private static final int COLOR_SELECTED = 0xFFFFFFFF;
    private static final int COLOR_NOT_SELECTED = 0xB2FFFFFF;
    private static final int COLOR_SELECTED_ET = 0xFF91171f;
    private static final int COLOR_NOT_SELECTED_ET = 0xAAd95e67;
    
    protected int pageCount;
    protected int currentPage;
    protected PageControlListener listener;
    
    public PageControl(Context context) {
        this(context, null, 0);
    }

    public PageControl(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PageControl(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.v("ChartView", "onSizeChanged - w: " + w + " h: " + h + " oldw: " + oldw + " oldh: " + oldh);
    }
        
    @Override
    protected void onDraw(Canvas canvas) {
        int dot_radius = getHeight() / 2;
        int dot_width = 4 * dot_radius;
        int cx = getWidth() / 2 - (pageCount - 1) * dot_width / 2;
        int cy = getHeight() / 2;
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        for (int i = 0; i < pageCount; i++) {
        	if (Utils.isEtraderProject(getContext())){
        		paint.setColor(i == currentPage ? COLOR_SELECTED_ET : COLOR_NOT_SELECTED_ET);
        	} else {
        		paint.setColor(i == currentPage ? COLOR_SELECTED : COLOR_NOT_SELECTED);
        	}
            canvas.drawCircle(cx + i * dot_width, cy, dot_radius, paint);
        }
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (null != listener) {
            if(event.getAction() == MotionEvent.ACTION_DOWN) {
                if (event.getX() < getWidth() / 2) {
                    showPrevious();
                    listener.showPrevious();
                } else {
                    showNext();
                    listener.showNext();
                }
            }
        }
        return true;
    }

    public void showNext() {
        currentPage++;
        if (currentPage == pageCount) {
            currentPage = 0;
        }
        invalidate();
    }
    
    public void showPrevious() {
        if (currentPage == 0) {
            currentPage = pageCount;
        }
        currentPage--;
        invalidate();
    }
    
    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public PageControlListener getListener() {
        return listener;
    }

    public void setListener(PageControlListener listener) {
        this.listener = listener;
    }
}