package com.anyoption.android.app;

import android.os.Bundle;

import com.anyoption.android.app.util.Constants;
import com.anyoption.common.beans.base.PaymentMethod;

public class NotAvailableActivity extends BaseActivity
{
	 /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        int paymentTypeId = ((PaymentMethod) getIntent().getExtras().get(Constants.PUT_EXTRA_BANKING_PAYMENT)).getTypeId();
        setContentView(buildLayout(paymentTypeId));
        PaymentMethod payment = (PaymentMethod) getIntent().getExtras().get(Constants.PUT_EXTRA_BANKING_PAYMENT);
        findViewById(R.id.imageViewInfo).setTag(payment);
    }

}
