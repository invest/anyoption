package com.anyoption.web.proxy;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;

import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.SendMailTemplateMethodRequest;
import com.google.gson.Gson;

public class ProxyServlet extends HttpServlet {

	private static final long serialVersionUID = 2822188232105860478L;
	private static final String JSON_SERVOCE_URL_PARAM_NAME = "com.anyoption.web.proxy.SERVER_URL";
    private static String jsonServiceURL;
    
    private static final Logger log = Logger.getLogger(ProxyServlet.class);

    public void init() throws ServletException {
        jsonServiceURL = getServletContext().getInitParameter(JSON_SERVOCE_URL_PARAM_NAME);
        log.info("JSON Service URL: " + jsonServiceURL);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        // extract the requested service name
        String uri = request.getRequestURI();
        String serviceName = uri.substring(uri.lastIndexOf("/"));
        log.info("Proxy - URI: " + uri + " serviceName: " + serviceName);
        try {
            byte[] buffer = new byte[2048]; // 2K read/write buffer. This might need change for better performance
            URL u = new URL(jsonServiceURL + serviceName);
            HttpURLConnection httpCon = (HttpURLConnection)u.openConnection();
            httpCon.setDoOutput(true);

            log.debug("Forward request");
            if (serviceName.equals("/sendValidateTemplate")) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                forward(buffer, request.getInputStream(), baos);
                byte[] resp = baos.toByteArray();
                String json = new String(resp, "UTF-8");
                log.debug("json: " + json);
                Gson gson = new Gson();
                SendMailTemplateMethodRequest jsonRequest = gson.fromJson(json, SendMailTemplateMethodRequest.class);
                HttpSession session = request.getSession();
                jsonRequest.setUserName((String) session.getAttribute("userName"));
                jsonRequest.setPassword((String) session.getAttribute("password"));
                jsonRequest.setEncrypt(true);
                jsonRequest.setSkinId((Long) session.getAttribute("skinId"));
                httpCon.getOutputStream().write(gson.toJson(jsonRequest).getBytes("UTF-8"));
            } else {
                forward(buffer, request.getInputStream(), httpCon.getOutputStream());
            }
            
            log.debug("Forward response");
            if (serviceName.equals("/insertUser")) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                forward(buffer, httpCon.getInputStream(), baos);
                byte[] resp = baos.toByteArray();
                String json = new String(resp, "UTF-8");
                log.debug("json: " + json);
                Gson gson = new Gson();
                UserMethodResult result = gson.fromJson(json, UserMethodResult.class);
                if (result.getErrorCode() == 0) {
                    log.debug("Save userName: " + result.getUser().getUserName() +
                            " password: " + result.getUser().getEncryptedPassword() +
                            " skinId: " + result.getUser().getSkinId());
                    HttpSession session = request.getSession();
                    session.setAttribute("userName", result.getUser().getUserName());
                    session.setAttribute("password", result.getUser().getEncryptedPassword());
                    session.setAttribute("skinId", result.getUser().getSkinId());
                } else {
                    log.debug("insertUser errorCode: " + result.getErrorCode());
                }
                response.getOutputStream().write(resp);
            } else {
                forward(buffer, httpCon.getInputStream(), response.getOutputStream());
            }
        } catch (Exception e) {
            log.error("Error in processing request.", e);
        }
    }
    
    private void forward(byte[] buffer, InputStream is, OutputStream os) throws IOException {
        int readCount = -1;
        while ((readCount = is.read(buffer)) != -1) {
            log.trace("readCount: " + readCount);
            os.write(buffer, 0, readCount);
        }
        os.flush();
        os.close();
    }
}