/**
 * 
 */
package com.copyop.json.results;

import com.anyoption.common.service.results.MethodResult;


/**
 * @author pavelt
 *
 */
public class CopyStepsEncourageMethodResult extends MethodResult {

	private String message;
	private long fullyCopyStep;// currently hardcoded 2
	private String stepAmount;
	private String profitAmount;
	

	public CopyStepsEncourageMethodResult() {

	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getFullyCopyStep() {
		return fullyCopyStep;
	}

	public void setFullyCopyStep(long fullyCopyStep) {
		this.fullyCopyStep = fullyCopyStep;
	}

	public String getStepAmount() {
		return stepAmount;
	}

	public void setStepAmount(String stepAmount) {
		this.stepAmount = stepAmount;
	}

	public String getProfitAmount() {
		return profitAmount;
	}

	public void setProfitAmount(String profitAmount) {
		this.profitAmount = profitAmount;
	}
}
