package com.copyop.json.results;

import com.anyoption.common.beans.base.Message;
import com.anyoption.common.service.results.MethodResult;

public class HolidayMessageResult extends MethodResult {
	
	protected Message message;

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}
}