package com.copyop.json.results;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.anyoption.common.service.results.ErrorMessage;
import com.copyop.common.dto.base.Profile;

/**
 * @author kirilim
 *
 */
public class UserMethodResult extends com.anyoption.common.service.results.UserMethodResult {

	private Profile copyopProfile;
	private List<Long> copyAmounts;
	private double oddsWin;
	private double oddsLose;
	private int maxCopiersPerAccount;
	private int safeModeMaxTrades;
	private int safeModeMinCopyUsers;
	private int safeModePeriod;
	private boolean isSafeMode = false ;
	
	public UserMethodResult() {
		isSafeMode = false;
	}

	public UserMethodResult(com.anyoption.common.service.results.UserMethodResult aoResult, int maxCopiersPerAccount) {
		this.setErrorCode(aoResult.getErrorCode());
		if (aoResult.getErrorMessages() != null) {
			this.addErrorMessages(new ArrayList<ErrorMessage>(Arrays.asList(aoResult.getErrorMessages())));
		}
		if (aoResult.getUserMessages() != null) {
			this.setUserMessages(new ArrayList<ErrorMessage>(Arrays.asList(aoResult.getUserMessages())));
		}
		this.setUser(aoResult.getUser());
		this.setUserRegulation(aoResult.getUserRegulation());
		this.setmId(aoResult.getmId());
		this.setMarketingStaticPart(aoResult.getMarketingStaticPart());
		this.setEtsMid(aoResult.getEtsMid());
		this.maxCopiersPerAccount = maxCopiersPerAccount;
		this.setMinIvestmentAmount(aoResult.getMinIvestmentAmount());
	}

	/**
	 * @return the copyopUser
	 */
	public Profile getCopyopProfile() {
		return copyopProfile;
	}

	/**
	 * @param copyopUser the copyopUser to set
	 */
	public void setCopyopProfile(Profile copyopProfile) {
		this.copyopProfile = copyopProfile;
	}

	public List<Long> getCopyAmounts() {
		return copyAmounts;
	}

	public void setCopyAmounts(List<Long> copyAmounts) {
		this.copyAmounts = copyAmounts;
	}

	public double getOddsWin() {
		return oddsWin;
	}

	public void setOddsWin(double oddsWin) {
		this.oddsWin = oddsWin;
	}

	public double getOddsLose() {
		return oddsLose;
	}

	public void setOddsLose(double oddsLose) {
		this.oddsLose = oddsLose;
	}
	
	public int getMaxCopiersPerAccount() {
		return maxCopiersPerAccount;
	}

	public void setMaxCopiersPerAccount(int maxCopiersPerAccount) {
		this.maxCopiersPerAccount = maxCopiersPerAccount;
	}
	
	public int getSafeModeMaxTrades() {
		return safeModeMaxTrades;
	}

	public void setSafeModeMaxTrades(int safeModeMaxTrades) {
		this.safeModeMaxTrades = safeModeMaxTrades;
	}

	public int getSafeModeMinCopyUsers() {
		return safeModeMinCopyUsers;
	}

	public void setSafeModeMinCopyUsers(int safeModeMinCopyUsers) {
		this.safeModeMinCopyUsers = safeModeMinCopyUsers;
	}

	public int getSafeModePeriod() {
		return safeModePeriod;
	}

	public void setSafeModePeriod(int safeModePeriod) {
		this.safeModePeriod = safeModePeriod;
	}

	public boolean isSafeMode() {
		return isSafeMode;
	}

	public void setSafeMode(boolean isSafeMode) {
		this.isSafeMode = isSafeMode;
	}

	@Override
	public String toString() {
		return "UserMethodResult [copyopProfile=" + copyopProfile + ", copyAmounts=" + copyAmounts + ", oddsWin="
				+ oddsWin + ", oddsLose=" + oddsLose + ", maxCopiersPerAccount=" + maxCopiersPerAccount
				+ ", safeModeMaxTrades=" + safeModeMaxTrades + ", safeModeMinCopyUsers=" + safeModeMinCopyUsers
				+ ", safeModePeriod=" + safeModePeriod + ", isSafeMode=" + isSafeMode + ", minIvestmentAmount=" + minIvestmentAmount + "]";
	}
}