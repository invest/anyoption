package com.copyop.json.results;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

public class FavMarketsMethodResult extends MethodResult {

	/*
	 * top ten markets with most investments by the user 
	 */
	private ArrayList<Long> favoriteMarkets;

	public ArrayList<Long> getFavoriteMarkets() {
		return favoriteMarkets;
	}

	public void setFavoriteMarkets(ArrayList<Long> favoriteMarkets) {
		this.favoriteMarkets = favoriteMarkets;
	}

}
