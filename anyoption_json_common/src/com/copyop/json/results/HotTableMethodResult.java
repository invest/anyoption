package com.copyop.json.results;

import java.util.List;

import com.anyoption.common.service.results.MethodResult;
import com.copyop.common.dto.base.Profile;

/**
 * @author kirilim
 */
public class HotTableMethodResult extends MethodResult {

	private List<Profile> traders;
	private List<Profile> copiers;

	/**
	 * @return the traders
	 */
	public List<Profile> getTraders() {
		return traders;
	}

	/**
	 * @param traders the traders to set
	 */
	public void setTraders(List<Profile> traders) {
		this.traders = traders;
	}

	/**
	 * @return the copiers
	 */
	public List<Profile> getCopiers() {
		return copiers;
	}

	/**
	 * @param copiers the copiers to set
	 */
	public void setCopiers(List<Profile> copiers) {
		this.copiers = copiers;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "HotTableMethodResult: "
				+ super.toString()
				+ "traders: " + traders + ls
				+ "copiers: " + copiers + ls;
	}
}