package com.copyop.json.results;

import java.util.List;

import com.anyoption.common.service.results.MethodResult;
import com.copyop.common.dto.base.Profile;

/**
 * @author kirilim
 */
public class CopyListMethodResult extends MethodResult {

	private List<? extends Profile> copyList;

	public List<? extends Profile> getCopyList() {
		return copyList;
	}

	public void setCopyList(List<? extends Profile> copyList) {
		this.copyList = copyList;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "CopyListMethodResult: " + super.toString() + "copyList: " + copyList + ls;
	}
}