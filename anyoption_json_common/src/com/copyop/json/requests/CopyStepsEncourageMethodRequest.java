/**
 * 
 */
package com.copyop.json.requests;

import com.anyoption.common.service.requests.MethodRequest;


/**
 * @author pavelt
 *
 */
public class CopyStepsEncourageMethodRequest extends MethodRequest {

	private long copiedUserId;
	private long userId;
	private long stepAmount;

	public CopyStepsEncourageMethodRequest() {
	}

	public long getCopiedUserId() {
		return copiedUserId;
	}

	public void setCopiedUserId(long copiedUserId) {
		this.copiedUserId = copiedUserId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getStepAmount() {
		return stepAmount;
	}

	public void setStepAmount(long stepAmount) {
		this.stepAmount = stepAmount;
	}
	
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "CopyStepsEncourageMethodRequest: "
				+ super.toString()
				+ "copiedUserId: " + copiedUserId + ls 
				+ "userId: " + userId + ls
				+ "stepAmount: " + stepAmount + ls;
	}
}
