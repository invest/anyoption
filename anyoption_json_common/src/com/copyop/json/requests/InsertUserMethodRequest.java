/**
 * 
 */
package com.copyop.json.requests;

/**
 * @author kirilim
 */
public class InsertUserMethodRequest extends com.anyoption.common.service.requests.InsertUserMethodRequest {

	private String nickname;
	private String avatar;
	private String fbId;
	private boolean acceptedTermsAndConditions;
	private String gender;

	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * @return the avatar
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * @param avatar the avatar to set
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	/**
	 * @return the facebookId
	 */
	public String getFbId() {
		return fbId;
	}

	/**
	 * @param facebookId the facebookId to set
	 */
	public void setFbId(String fbId) {
		this.fbId = fbId;
	}
	
	public boolean isAcceptedTermsAndConditions() {
		return acceptedTermsAndConditions;
	}

	public void setAcceptedTermsAndConditions(boolean acceptedTermsAndConditions) {
		this.acceptedTermsAndConditions = acceptedTermsAndConditions;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "com.copyop.json.requests.InsertUserMethodRequest: "
				+ super.toString()
				+ "nickname: " + nickname + ls
				+ "avatar: " + avatar + ls
				+ "gender: " + gender + ls
				+ "acceptedTermsAndConditions: " + acceptedTermsAndConditions + ls;
	}
}