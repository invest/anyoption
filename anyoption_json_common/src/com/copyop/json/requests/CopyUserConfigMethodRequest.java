package com.copyop.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;
import com.copyop.common.dto.base.CopyConfig;

/**
 * @author kirilim
 */
public class CopyUserConfigMethodRequest extends UserMethodRequest {

	private CopyConfig config;
	
	public CopyConfig getConfig() {
		return config;
	}
	
	public void setConfig(CopyConfig config) {
		this.config = config;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "CopyUserConfigMethodRequest: " + ls
				+ super.toString()
				+ "config: " + config + ls;
	}
}