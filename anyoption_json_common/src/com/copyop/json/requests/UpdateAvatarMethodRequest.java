package com.copyop.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

public class UpdateAvatarMethodRequest extends UserMethodRequest {
	protected String avatar;

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "UpdateAvatarMethodRequest: "
            + super.toString()
            + "avatar: " + avatar + ls;
	}
}