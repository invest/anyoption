package com.anyoption.json.util;


import java.util.Locale;

/**
 * 
 * @author liors
 *
 */
public class CountryOptionItem extends OptionItem  {

	private String phoneCode;
	private String supportPhone;
	private String supportFax;

	public CountryOptionItem(long id, String value, Locale locale, String phoneCode,
									String supportPhone, String supportFax) {
		super(id, value, locale);
		this.phoneCode = phoneCode;
		this.supportPhone = supportPhone;
		this.supportFax = supportFax;
	}

	/**
	 * @return the phoneCode
	 */
	public String getPhoneCode() {
		return phoneCode;
	}

	/**
	 * @param phoneCode the phoneCode to set
	 */
	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	/**
	 * @return the supportFax
	 */
	public String getSupportFax() {
		return supportFax;
	}

	/**
	 * @param supportFax the supportFax to set
	 */
	public void setSupportFax(String supportFax) {
		this.supportFax = supportFax;
	}

	/**
	 * @return the supportPhone
	 */
	public String getSupportPhone() {
		return supportPhone;
	}

	/**
	 * @param supportPhone the supportPhone to set
	 */
	public void setSupportPhone(String supportPhone) {
		this.supportPhone = supportPhone;
	}

}