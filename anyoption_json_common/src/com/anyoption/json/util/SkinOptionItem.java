package com.anyoption.json.util;

import java.util.Locale;

/**
 * 
 * @author liors
 *
 */
public class SkinOptionItem extends OptionItem  {

	private int defaultCountryId;
	private int defaultLanguageId;
	private int defaultCurrencyId;

	/**
	 * @param id
	 * @param value
	 * @param locale
	 * @param defaultCountryId
	 * @param defaultLanguageId
	 * @param defaultCurrencyId
	 */
	public SkinOptionItem(long id, String value, Locale locale, int defaultCountryId,
			int defaultLanguageId, int defaultCurrencyId) {
		super(id, value, locale);
		this.defaultCountryId = defaultCountryId;
		this.defaultLanguageId = defaultLanguageId;
		this.defaultCurrencyId = defaultCurrencyId;
	}

	/**
	 * @return the defaultCountryId
	 */
	public int getDefaultCountryId() {
		return defaultCountryId;
	}

	/**
	 * @param defaultCountryId the defaultCountryId to set
	 */
	public void setDefaultCountryId(int defaultCountryId) {
		this.defaultCountryId = defaultCountryId;
	}

	/**
	 * @return the defaultCurrencyId
	 */
	public int getDefaultCurrencyId() {
		return defaultCurrencyId;
	}

	/**
	 * @param defaultCurrencyId the defaultCurrencyId to set
	 */
	public void setDefaultCurrencyId(int defaultCurrencyId) {
		this.defaultCurrencyId = defaultCurrencyId;
	}

	/**
	 * @return the defaultLanguageId
	 */
	public int getDefaultLanguageId() {
		return defaultLanguageId;
	}

	/**
	 * @param defaultLanguageId the defaultLanguageId to set
	 */
	public void setDefaultLanguageId(int defaultLanguageId) {
		this.defaultLanguageId = defaultLanguageId;
	}

}