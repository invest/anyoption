package com.anyoption.json.util;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

/**
 * 
 * @author liors
 *
 */
public class OptionItem  {
    protected long id;
    protected String value;
    protected Locale locale;

    /**
     * @param id
     * @param value
     * @param locale
     */
    public OptionItem(long id, String value, Locale locale) {
    	this.id = id;
    	this.value = value;
    	this.locale = locale;
    }

    public OptionItem(long id, String value) {
    	this.id = id;
    	this.value = value;
    }

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

    public static class AlphaComparator implements Comparator<OptionItem> {
        public int compare(OptionItem a, OptionItem b) {
            String aLabel = a.getValue();
            String bLabel = b.getValue();
            int returnVal = 0;
            try {
                Collator collator = Collator.getInstance(a.locale);
                returnVal = collator.compare(aLabel, bLabel);
            } catch (Exception e) {
            }
			return returnVal;
        }
    }
    
    public String toString() {
    	if (null !=  this.value) {
    		return this.value;
    	}
    	return "";
    }

}