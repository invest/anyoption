package com.anyoption.json.requests;

import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * get and set wire.
 * 
 * @author liors
 *
 */
public class WireMethodRequest extends UserMethodRequest {
    protected WireBase wire;
	private Long userAnswerId;
	private String textAnswer;

    public WireBase getWire() {
        return wire;
    }

    public void setWire(WireBase wire) {
        this.wire = wire;
    }
    
	public Long getUserAnswerId() {
		return userAnswerId;
	}
	
	public void setUserAnswerId(Long userAnswerId) {
		this.userAnswerId = userAnswerId;
	}
	
	public String getTextAnswer() {
		return textAnswer;
	}
	
	public void setTextAnswer(String textAnswer) {
		this.textAnswer = textAnswer;
	}
    
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "WireMethodRequest: "
            + super.toString()
            + "wire: " + wire + ls
            + ", userAnswerId=" + userAnswerId + ls
            + ", textAnswer=" + textAnswer + "]";
    }
}