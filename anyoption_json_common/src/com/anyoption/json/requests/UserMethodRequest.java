//package com.anyoption.json.requests;
//
//import com.anyoption.common.service.requests.MethodRequest;
//
//
///**
// * contain userName, password, encrypt (boolean).
// * 
// * extends: MethodRequest
// * @author liors
// *
// */
//public class UserMethodRequest extends MethodRequest {
//    protected String userName;
//    protected String password;
//    protected boolean encrypt;
//    protected boolean isLogin;
//    protected boolean afterDeposit;
//	private long fingerPrint;
//
//    public UserMethodRequest() {
//        encrypt = false;
//    }
//
//	/**
//	 * @return the password
//	 */
//	public String getPassword() {
//		return password;
//	}
//
//	/**
//	 * @param password the password to set
//	 */
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	/**
//	 * @return the userName
//	 */
//	public String getUserName() {
//		return userName;
//	}
//
//	/**
//	 * @param userName the userName to set
//	 */
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//
//    public boolean isEncrypt() {
//        return encrypt;
//    }
//
//    public void setEncrypt(boolean encrypt) {
//        this.encrypt = encrypt;
//    }
//    
//    public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "UserMethodRequest: "
//            + super.toString()
//            + "userName: " + userName + ls
//            + "isLogin: " + isLogin + ls
//            + "afterDeposit: " + afterDeposit + ls
//            + "encrypt: " + encrypt + ls;
//    }
//
//    /**
//     * @return the isLogin
//     */
//    public boolean isLogin() {
//        return isLogin;
//    }
//
//    /**
//     * @param isLogin the isLogin to set
//     */
//    public void setLogin(boolean isLogin) {
//        this.isLogin = isLogin;
//    }
//
//	/**
//	 * @return the afterDeposit
//	 */
//	public boolean isAfterDeposit() {
//		return afterDeposit;
//	}
//
//	/**
//	 * @param afterDeposit the afterDeposit to set
//	 */
//	public void setAfterDeposit(boolean afterDeposit) {
//		this.afterDeposit = afterDeposit;
//	}
//
//	public long getFingerPrint() {
//		return fingerPrint;
//	}
//
//	public void setFingerPrint(long fingerPrint) {
//		this.fingerPrint = fingerPrint;
//	}
//}