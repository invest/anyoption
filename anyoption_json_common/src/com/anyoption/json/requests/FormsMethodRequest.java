package com.anyoption.json.requests;

import java.util.Calendar;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * 
 * @author liors
 *
 */
public class FormsMethodRequest extends UserMethodRequest {

    protected Calendar from;
    protected Calendar to;
    protected Integer startRow;
    protected Integer pageSize;

	/**
	 * @return the startRow
	 */
	public Integer getStartRow() {
		return startRow;
	}

	/**
	 * @param startRow the startRow to set
	 */
	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}

	/**
	 * @return the pageSize
	 */
	public Integer getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the from
	 */
	public Calendar getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Calendar from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Calendar getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Calendar to) {
		this.to = to;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "FormsMethodRequest: "
            + super.toString()
            + "from: " + from + ls
            + "to: " + to + ls
            + "startRow: " + startRow + ls
            + "pageSize: " + pageSize + ls;
    }

}