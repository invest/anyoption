package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author kirilim
 */
public class InatecDepositMethodRequest extends UserMethodRequest {

	private String depositAmount;
	private String accountNum;
	private String bankSortCode;
	private String beneficiaryName;
	private long paymentTypeParam;

	public String getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}

	public String getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public String getBankSortCode() {
		return bankSortCode;
	}

	public void setBankSortCode(String bankSortCode) {
		this.bankSortCode = bankSortCode;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public long getPaymentTypeParam() {
		return paymentTypeParam;
	}

	public void setPaymentTypeParam(long paymentTypeParam) {
		this.paymentTypeParam = paymentTypeParam;
	}

	public String toString() {
		 String ls = System.getProperty("line.separator");
	        return ls + "InatecDepositMethodRequest: "
	            + super.toString()
	            + "depositAmount: " + depositAmount + ls
	            + "accountNum: " + accountNum + ls
	            + "bankSortCode: " + bankSortCode + ls
	            + "beneficiaryName: " + beneficiaryName + ls
	            + "paymentTypeParam: " + paymentTypeParam;
	}
}