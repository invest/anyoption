/**
 * 
 */
package com.anyoption.json.requests;

import com.anyoption.common.beans.base.Transaction;
import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author AviadH
 *
 */
public class TransactionMethodRequest extends UserMethodRequest {
	
	protected Transaction trans[];

	/**
	 * @return the trans
	 */
	public Transaction[] getTrans() {
		return trans;
	}

	/**
	 * @param trans the trans to set
	 */
	public void setTrans(Transaction[] trans) {
		this.trans = trans;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "TransactionMethodRequest: "
            + super.toString()
            + "trans: " + trans + ls;
    }
	
}
