package com.anyoption.json.requests;

import com.anyoption.common.service.requests.MethodRequest;


/**
 * 
 * @author liors
 *
 */
public class CurrencyMethodRequest extends MethodRequest {
    protected long currencyId;

    /**
     * @return currencyId
     */
    public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
}