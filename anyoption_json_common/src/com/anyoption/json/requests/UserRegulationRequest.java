package com.anyoption.json.requests;

import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.service.requests.UserMethodRequest;

public class UserRegulationRequest extends UserMethodRequest {
	
	private UserRegulationBase userRegulation;

	public UserRegulationBase getUserRegulation() {
		return userRegulation;
	}

	public void setUserRegulation(UserRegulationBase userRegulation) {
		this.userRegulation = userRegulation;
	}

}