/**
 * 
 */
package com.anyoption.json.requests;

import com.anyoption.common.service.requests.MethodRequest;


/**
 * @author pavelhe
 *
 */
public class FirstEverDepositCheckRequest extends MethodRequest {
	private long userId;

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "FirstEverDepositCheckRequest [userId=" + userId
				+ ", toString()=" + super.toString() + "]";
	}
}
