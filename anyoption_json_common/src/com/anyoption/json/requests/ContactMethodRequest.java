package com.anyoption.json.requests;

import com.anyoption.beans.base.Contact;
import com.anyoption.common.service.requests.MethodRequest;

/**
 * 
 * @author liors
 *
 */
public class ContactMethodRequest extends MethodRequest {

	private Contact contact;
	private String comments;
	private String issue;

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the issue
	 */
	public String getIssue() {
		return issue;
	}

	/**
	 * @param issue the issue to set
	 */
	public void setIssue(String issue) {
		this.issue = issue;
	}

	/**
	 * @return the contact
	 */
	public Contact getContact() {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Override
	public String toString() {
		return "ContactMethodRequest [contact=" + contact + ", comments=" + comments + ", issue=" + issue + "]";
	}
	
	

}