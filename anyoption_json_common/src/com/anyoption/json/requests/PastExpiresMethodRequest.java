package com.anyoption.json.requests;

import java.util.Calendar;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * 
 * @author liors
 *
 */
public class PastExpiresMethodRequest extends UserMethodRequest {

    protected Calendar date;
    protected long marketId;
    protected long marketGroupId;
    protected int startRow;
    protected int pageSize;


	/**
	 * @return the date
	 */
	public Calendar getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Calendar date) {
		this.date = date;
	}

	/**
	 * @return the marketGroupId
	 */
	public long getMarketGroupId() {
		return marketGroupId;
	}

	/**
	 * @param marketGroupId the marketGroupId to set
	 */
	public void setMarketGroupId(long marketGroupId) {
		this.marketGroupId = marketGroupId;
	}

	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the startRow
	 */
	public int getStartRow() {
		return startRow;
	}

	/**
	 * @param startRow the startRow to set
	 */
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
}