package com.anyoption.json.requests;

public class SystemChecksRequest {
	private String service;

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}
}
