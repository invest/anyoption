/**
 * 
 */
package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;


/**
 * @author AviadH
 *
 */
public class FeeMethodRequest extends UserMethodRequest {
	
	protected long tranTypeId;

	public long getTranTypeId() {
		return tranTypeId;
	}

	public void setTranTypeId(long tranTypeId) {
		this.tranTypeId = tranTypeId;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "FeeMethodRequest: "
            + super.toString()
            + "tranTypeId: " + tranTypeId + ls;
    }

}
