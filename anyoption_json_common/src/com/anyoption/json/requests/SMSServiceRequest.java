package com.anyoption.json.requests;

public class SMSServiceRequest {

	String sender;
	String senderNumber;
	String phone;
	String message;
	
	long keyValue;
	long keyType;
	long providerId;
	long descriptionId;
	
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getSenderNumber() {
		return senderNumber;
	}
	public void setSenderNumber(String senderNumber) {
		this.senderNumber = senderNumber;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getKeyValue() {
		return keyValue;
	}
	public void setKeyValue(long keyValue) {
		this.keyValue = keyValue;
	}
	public long getKeyType() {
		return keyType;
	}
	public void setKeyType(long keyType) {
		this.keyType = keyType;
	}
	public long getProviderId() {
		return providerId;
	}
	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}
	public long getDescriptionId() {
		return descriptionId;
	}
	public void setDescriptionId(long descriptionId) {
		this.descriptionId = descriptionId;
	}
	
}
