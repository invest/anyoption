package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * contains box number, marketId, opportunityId and opportunityTypeId. 
 * @author liors
 *
 */
public class ChartDataMethodRequest extends UserMethodRequest {
    protected int box;
    protected long marketId;
    protected long opportunityId;
    protected long opportunityTypeId;
    private int scheduled;

    /**
     * @return box number.
     */
    public int getBox() {
        return box;
    }

    /**
     * @param box
     */
    public void setBox(int box) {
        this.box = box;
    }

    /**
     * @return marketId.
     */
    public long getMarketId() {
        return marketId;
    }

    /**
     * @param marketId
     */
    public void setMarketId(long marketId) {
        this.marketId = marketId;
    }

    /**
     * @return opportunityId.
     */
    public long getOpportunityId() {
        return opportunityId;
    }

    /**
     * @param opportunityId
     */
    public void setOpportunityId(long opportunityId) {
        this.opportunityId = opportunityId;
    }

    /**
     * @return opportunityTypeId
     */
    public long getOpportunityTypeId() {
        return opportunityTypeId;
    }

    /**
     * @param opportunityTypeId
     */
    public void setOpportunityTypeId(long opportunityTypeId) {
        this.opportunityTypeId = opportunityTypeId;
    }
    
    public int getScheduled() {
		return scheduled;
	}

	public void setScheduled(int scheduled) {
		this.scheduled = scheduled;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "ChartDataMethodRequest: "
            + super.toString()
            + "box: " + box + ls
            + "marketId: " + marketId + ls
            + "opportunityId: " + opportunityId + ls
            + "opportunityTypeId: " + opportunityTypeId + ls
            + "scheduled: " + scheduled + ls;
    }
}