package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

public class CUPInfoMethodRequest extends UserMethodRequest {
	private String params;
	private String amount;
	private long userId;
	private long clearingProviderId;

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public long getClearingProviderId() {
		return clearingProviderId;
	}

	public void setClearingProviderId(long clearingProviderId) {
		this.clearingProviderId = clearingProviderId;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "CUPInfoMethodRequest: "
            + super.toString()
            + "params: " + params + ls
            + "amount: " + amount + ls
            + "userId: " + userId + ls
            + "clearingProviderId: " + clearingProviderId + ls;
    }
}
