package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * contain email .
 *
 * extends: UserMethodRequest
 * @author oshikL
 *
 */
public class SendMailTemplateMethodRequest extends UserMethodRequest {
	protected String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
