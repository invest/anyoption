package com.anyoption.json.results;

import com.anyoption.common.beans.base.MarketingInfo;
import com.anyoption.common.service.results.MethodResult;
import com.google.gson.annotations.Expose;

/**
 * MarketingUsersMethodResult
 * @author Eyal O
 */
public class MarketingUsersMethodResult extends MethodResult {
	@Expose
	protected MarketingInfo marketingInfo;

	/**
	 * @return the marketingInfo
	 */
	public MarketingInfo getMarketingInfo() {
		return marketingInfo;
	}

	/**
	 * @param marketingInfo the marketingInfo to set
	 */
	public void setMarketingInfo(MarketingInfo marketingInfo) {
		this.marketingInfo = marketingInfo;
	}
}
