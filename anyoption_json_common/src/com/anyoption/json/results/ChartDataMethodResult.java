package com.anyoption.json.results;

import java.util.Date;

import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.service.results.UserMethodResult;
import com.google.gson.annotations.Expose;

/**
 * 
 * @author liors
 *
 */
public class ChartDataMethodResult extends UserMethodResult {
    protected int timezoneOffset;
    protected long marketId;
    @Expose
    protected long opportunityId;
    protected Date timeEstClosing;
    protected int scheduled;
    protected int decimalPoint;
//    protected MarketRate[] rates;
    @Expose
    protected long[] ratesTimes;
    @Expose
    protected double[] rates;
    protected double[] lasts;
    protected double[] asks;
    protected double[] bids;
    @Expose
    protected String marketName;
    protected Date timeFirstInvest;
    protected Date timeLastInvest;
    protected Investment[] investments;
    protected long opportunityTypeId;
    protected boolean hasPreviousHour;
    protected long commission;

    @Expose
    protected long timeLastInvestMillsec;
    @Expose
    protected long timeFirstInvestMillsec;
    @Expose
    protected long timeEstClosingMillsec;    
    
    /**
     * @return
     */
    public Date getTimeFirstInvest() {
		return timeFirstInvest;
	}

	/**
	 * @param timeFirstInvest
	 */
	public void setTimeFirstInvest(Date timeFirstInvest) {
		this.timeFirstInvest = timeFirstInvest;
	}

	/**
	 * @return timeLastInvest
	 */
	public Date getTimeLastInvest() {
		return timeLastInvest;
	}

	/**
	 * @param timeLastInvest
	 */
	public void setTimeLastInvest(Date timeLastInvest) {
		this.timeLastInvest = timeLastInvest;
	}

	/**
	 * @return
	 */
	public int getDecimalPoint() {
        return decimalPoint;
    }

    /**
     * @param decimalPoint
     */
    public void setDecimalPoint(int decimalPoint) {
        this.decimalPoint = decimalPoint;
    }

    /**
     * @return
     */
    public int getTimezoneOffset() {
        return timezoneOffset;
    }

    /**
     * @param timezoneOffset
     */
    public void setTimezoneOffset(int timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }

    /**
     * @return time Estimation Closing - The time when the opportunity is supposed to settle 
     */
    public Date getTimeEstClosing() {
        return timeEstClosing;
    }

    /**
     * @param timeEstClosing
     */
    public void setTimeEstClosing(Date timeEstClosing) {
        this.timeEstClosing = timeEstClosing;
    }

    /**
     * @return
     */
    public int getScheduled() {
        return scheduled;
    }

    /**
     * @param scheduled
     */
    public void setScheduled(int scheduled) {
        this.scheduled = scheduled;
    }

//    public MarketRate[] getRates() {
//        return rates;
//    }
//
//    public void setRates(MarketRate[] rates) {
//        this.rates = rates;
//    }

    /**
     * @return market Id
     */
    public long getMarketId() {
        return marketId;
    }

    /**
     * @return
     */
    public long[] getRatesTimes() {
        return ratesTimes;
    }

    /**
     * @param ratesTimes
     */
    public void setRatesTimes(long[] ratesTimes) {
        this.ratesTimes = ratesTimes;
    }

    /**
     * @return list of rates
     */
    public double[] getRates() {
        return rates;
    }

    /**
     * @param rates
     */
    public void setRates(double[] rates) {
        this.rates = rates;
    }

    public double[] getLasts() {
		return lasts;
	}

	public void setLasts(double[] lasts) {
		this.lasts = lasts;
	}

	public double[] getAsks() {
		return asks;
	}

	public void setAsks(double[] asks) {
		this.asks = asks;
	}

	public double[] getBids() {
		return bids;
	}

	public void setBids(double[] bids) {
		this.bids = bids;
	}

	/**
     * @param marketId
     */
    public void setMarketId(long marketId) {
        this.marketId = marketId;
    }

    /**
     * @return opportunity Id
     */
    public long getOpportunityId() {
        return opportunityId;
    }

    /**
     * @param opportunityId
     */
    public void setOpportunityId(long opportunityId) {
        this.opportunityId = opportunityId;
    }

	/**
	 * @return marketName
	 */
	public String getMarketName() {
		return marketName;
	}

	/**
	 * @param marketName
	 */
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

    /**
     * @return list of investments
     */
    public Investment[] getInvestments() {
        return investments;
    }

    /**
     * @param investments
     */
    public void setInvestments(Investment[] investments) {
        this.investments = investments;
    }

    /**
     * @return opportunity Type Id
     */
    public long getOpportunityTypeId() {
        return opportunityTypeId;
    }

    /**
     * @param opportunityTypeId
     */
    public void setOpportunityTypeId(long opportunityTypeId) {
        this.opportunityTypeId = opportunityTypeId;
    }

    /**
     * @return has Previous Hour
     */
    public boolean isHasPreviousHour() {
        return hasPreviousHour;
    }

    /**
     * @param hasPreviousHour
     */
    public void setHasPreviousHour(boolean hasPreviousHour) {
        this.hasPreviousHour = hasPreviousHour;
    }

	/**
	 * @return the commission
	 */
	public long getCommission() {
		return commission;
	}

	/**
	 * @param commission the commission to set
	 */
	public void setCommission(long commission) {
		this.commission = commission;
	}

	/**
	 * @return the timeEstClosingMillsec
	 */
	public long getTimeEstClosingMillsec() {
		return timeEstClosingMillsec;
	}

	/**
	 * @param timeEstClosingMillsec the timeEstClosingMillsec to set
	 */
	public void setTimeEstClosingMillsec(long timeEstClosingMillsec) {
		this.timeEstClosingMillsec = timeEstClosingMillsec;
	}

	/**
	 * @return the timeFirstInvestMillsec
	 */
	public long getTimeFirstInvestMillsec() {
		return timeFirstInvestMillsec;
	}

	/**
	 * @param timeFirstInvestMillsec the timeFirstInvestMillsec to set
	 */
	public void setTimeFirstInvestMillsec(long timeFirstInvestMillsec) {
		this.timeFirstInvestMillsec = timeFirstInvestMillsec;
	}

	/**
	 * @return the timeLastInvestMillsec
	 */
	public long getTimeLastInvestMillsec() {
		return timeLastInvestMillsec;
	}

	/**
	 * @param timeLastInvestMillsec the timeLastInvestMillsec to set
	 */
	public void setTimeLastInvestMillsec(long timeLastInvestMillsec) {
		this.timeLastInvestMillsec = timeLastInvestMillsec;
	}
	
}