package com.anyoption.json.results;

public class GeoTagResult {
	String countryCode;

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		return "GeoTagResult [countryCode=" + countryCode + "]";
	}
}
