package com.anyoption.json.results;

import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.service.results.MethodResult;
import com.google.gson.annotations.Expose;

/**
 * 
 * @author liors
 *
 */
public class OpportunityMethodResult extends MethodResult {
	@Expose
    protected Opportunity opportunity;

	/**
	 * @return the opportunity
	 */
	public Opportunity getOpportunity() {
		return opportunity;
	}

	/**
	 * @param opportunity the opportunity to set
	 */
	public void setOpportunity(Opportunity opportunity) {
		this.opportunity = opportunity;
	}
    
}