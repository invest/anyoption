package com.anyoption.json.results;

import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.service.results.MethodResult;

/**
 * 
 * @author liors
 *
 */
public class OpportunitiesMethodResult extends MethodResult {
    protected Opportunity[] opportunities;

    /**
     * @return list of Opportunities
     */
    public Opportunity[] getOpportunities() {
        return opportunities;
    }

    /**
     * @param opportunities
     */
    public void setOpportunities(Opportunity[] opportunities) {
        this.opportunities = opportunities;
    }
}