package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.util.OptionItem;

/**
 * 
 * @author liors
 *
 */
public class OptionsMethodResult extends MethodResult {
    protected OptionItem[] options;

	/**
	 * @return the options
	 */
	public OptionItem[] getOptions() {
		return options;
	}

	/**
	 * @param options the options to set
	 */
	public void setOptions(OptionItem[] options) {
		this.options = options;
	}

}