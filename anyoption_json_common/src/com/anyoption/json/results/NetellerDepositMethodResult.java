package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;

public class NetellerDepositMethodResult extends MethodResult {
	long transactionId;
	boolean success;

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	
}
