package com.anyoption.json.results;


import com.anyoption.common.beans.base.OpportunityOddsPair;
import com.anyoption.common.service.results.MethodResult;
import com.google.gson.annotations.Expose;

/**
 * @author EranL
 *
 */
public class OpportunityOddsPairResult extends MethodResult {
	
	@Expose
	protected OpportunityOddsPair[] opportunityOddsPair;

	/**
	 * @return the opportunityOddsPair
	 */
	public OpportunityOddsPair[] getOpportunityOddsPair() {
		return opportunityOddsPair;
	}

	/**
	 * @param opportunityOddsPair the opportunityOddsPair to set
	 */
	public void setOpportunityOddsPair(OpportunityOddsPair[] opportunityOddsPair) {
		this.opportunityOddsPair = opportunityOddsPair;
	}
	
	

}
