///**
// * 
// */
//package com.anyoption.json.results;
//
//import java.util.List;
//
//import com.anyoption.common.beans.base.QuestionnaireQuestion;
//import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
//import com.anyoption.common.service.results.UserMethodResult;
//
///**
// * @author pavelhe
// *
// */
//public class UserQuestionnaireResult extends UserMethodResult {
//	
//	private List<QuestionnaireUserAnswer> userAnswers;
//	
//	private List<QuestionnaireQuestion> questions;
//
//	/**
//	 * @return the userAnswers
//	 */
//	public List<QuestionnaireUserAnswer> getUserAnswers() {
//		return userAnswers;
//	}
//
//	/**
//	 * @param userAnswers the userAnswers to set
//	 */
//	public void setUserAnswers(List<QuestionnaireUserAnswer> userAnswers) {
//		this.userAnswers = userAnswers;
//	}
//	
//	/**
//	 * @return the questions
//	 */
//	public List<QuestionnaireQuestion> getQuestions() {
//		return questions;
//	}
//
//	/**
//	 * @param questions the questions to set
//	 */
//	public void setQuestions(List<QuestionnaireQuestion> questions) {
//		this.questions = questions;
//	}
//
//	public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "UserQuestionnaireResult: "
//            + super.toString()
//            + "questions: " + questions + ls
//            + "userAnswers: " + userAnswers + ls;
//    }
//
//}
