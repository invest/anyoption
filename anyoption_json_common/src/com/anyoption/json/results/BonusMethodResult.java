/**
 * 
 */
package com.anyoption.json.results;

import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.service.results.MethodResult;

/**
 * @author AviadH
 *
 */
public class BonusMethodResult extends MethodResult {

	BonusUsers[] bonuses;
	String[] turnoverFactor;
	
	public String[] getTurnoverFactor() {
		return turnoverFactor;
	}

	public void setTurnoverFactor(String[] turnoverFactor) {
		this.turnoverFactor = turnoverFactor;
	}

	/**
	 * @return the bonuses
	 */
	public BonusUsers[] getBonuses() {
		return bonuses;
	}

	/**
	 * @param bonuses the bonuses to set
	 */
	public void setBonuses(BonusUsers[] bonuses) {
		this.bonuses = bonuses;
	}
}
