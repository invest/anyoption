package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.util.CountryOptionItem;

/**
 * 
 * @author liors
 *
 */
public class CountriesOptionsMethodResult extends MethodResult {
    protected CountryOptionItem[] options;

	/**
	 * @return the options
	 */
	public CountryOptionItem[] getOptions() {
		return options;
	}

	/**
	 * @param options the options to set
	 */
	public void setOptions(CountryOptionItem[] options) {
		this.options = options;
	}

}