package com.anyoption.json.results;

import com.anyoption.beans.base.Contact;
import com.anyoption.common.service.results.MethodResult;
import com.google.gson.annotations.Expose;

/**
 * 
 * @author liors
 *
 */
public class ContactMethodResult extends MethodResult {

  @Expose
  private Contact contact;

  /**
   * @return the contact
   */
  public Contact getContact() {
	return contact;
  }

  /**
	* @param contact the contact to set
	*/
  public void setContact(Contact contact) {
	this.contact = contact;
  }

}