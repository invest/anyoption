package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;


/**
 * @author liors
 *
 */
public class PixelMethodResult extends MethodResult {
	private StringBuilder pixelHttpsCode;
	private StringBuilder pixelHttpCode;
	/**
	 * @return the pixelHttpsCode
	 */
	public StringBuilder getPixelHttpsCode() {
		return pixelHttpsCode;
	}
	/**
	 * @param pixelHttpsCode the pixelHttpsCode to set
	 */
	public void setPixelHttpsCode(StringBuilder pixelHttpsCode) {
		this.pixelHttpsCode = pixelHttpsCode;
	}
	/**
	 * @return the pixelHttpCode
	 */
	public StringBuilder getPixelHttpCode() {
		return pixelHttpCode;
	}
	/**
	 * @param pixelHttpCode the pixelHttpCode to set
	 */
	public void setPixelHttpCode(StringBuilder pixelHttpCode) {
		this.pixelHttpCode = pixelHttpCode;
	}
	
	@Override
	public String toString() {
		return "PixelMethodResult [pixelHttpsCode=" + pixelHttpsCode
				+ ", pixelHttpCode=" + pixelHttpCode + "]";
	}
}
