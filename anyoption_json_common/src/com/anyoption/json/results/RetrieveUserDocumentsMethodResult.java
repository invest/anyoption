package com.anyoption.json.results;

import java.util.Map;

import com.anyoption.common.beans.base.File;
import com.anyoption.common.service.results.UserMethodResult;

/**
 * @author kirilim
 */
public class RetrieveUserDocumentsMethodResult extends UserMethodResult {

	private Map<Long, File> userDocuments;
	private Map<Long, Map<Long, File>> ccFilesMap;

	public Map<Long, File> getUserDocuments() {
		return userDocuments;
	}

	public void setUserDocuments(Map<Long, File> userDocuments) {
		this.userDocuments = userDocuments;
	}

	public Map<Long, Map<Long, File>> getCcFilesMap() {
		return ccFilesMap;
	}

	public void setCcFilesMap(Map<Long, Map<Long, File>> ccFilesMap) {
		this.ccFilesMap = ccFilesMap;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "RetrieveUserDocumentsMethodResult: "
            + super.toString()
            + "userDocuments: " + userDocuments + ls
            + "ccFilesMap: " + ccFilesMap + ls;
    }
}