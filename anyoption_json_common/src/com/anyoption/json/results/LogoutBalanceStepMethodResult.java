/**
 * 
 */
package com.anyoption.json.results;

import java.util.List;

import com.anyoption.common.beans.base.BalanceStepPredefValue;
import com.anyoption.common.service.results.MethodResult;

/**
 * Contains the logout balance step information for given currency
 * 
 * @author kirilim
 */
public class LogoutBalanceStepMethodResult extends MethodResult {

	/**
	 * The balance step default amount value
	 */
	private long defaultAmountValue;

	/**
	 * The balance step predefined value suggestions
	 */
	private List<BalanceStepPredefValue> predefValues;
	/**
	 * The currency id for the balance step
	 */
	private long currencyId;

	/**
	 * @return the defaultAmountValue
	 */
	public long getDefaultAmountValue() {
		return defaultAmountValue;
	}

	/**
	 * @param defaultAmountValue the defaultAmountValue to set
	 */
	public void setDefaultAmountValue(long defaultAmountValue) {
		this.defaultAmountValue = defaultAmountValue;
	}

	/**
	 * @return the predefValues
	 */
	public List<BalanceStepPredefValue> getPredefValues() {
		return predefValues;
	}

	/**
	 * @param predefValues the predefValues to set
	 */
	public void setPredefValues(List<BalanceStepPredefValue> predefValues) {
		this.predefValues = predefValues;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
}