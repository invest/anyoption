package com.anyoption.json.results;

import com.anyoption.common.beans.base.LastCurrencyRate;
import com.anyoption.common.service.results.MethodResult;
import com.google.gson.annotations.Expose;

/**
 * @author EranL
 *
 */
public class LastCurrenciesRateMethodResult extends MethodResult {

	@Expose
	private LastCurrencyRate[] lastCurrenciesRate;

	/**
	 * @return the lastCurrenciesRate
	 */
	public LastCurrencyRate[] getLastCurrenciesRate() {
		return lastCurrenciesRate;
	}

	/**
	 * @param lastCurrenciesRate the lastCurrenciesRate to set
	 */
	public void setLastCurrenciesRate(LastCurrencyRate[] lastCurrenciesRate) {
		this.lastCurrenciesRate = lastCurrenciesRate;
	}


}
