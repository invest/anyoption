package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;


public class CUPInfoMethodResult extends MethodResult {
	private String params;
	private String postURL;
	private long userId;
	private long clearingProviderId;

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getPostURL() {
		return postURL;
	}

	public void setPostURL(String postURL) {
		this.postURL = postURL;
	}

	public long getClearingProviderId() {
		return clearingProviderId;
	}

	public void setClearingProviderId(long clearingProviderId) {
		this.clearingProviderId = clearingProviderId;
	}
}
